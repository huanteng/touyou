<?php
require '../library/controller.php';

class grab extends controller
{
	var $db = '';
	
	function __construct()
	{
		parent::__construct();
		$this->db = load( 'database' );
	}

	function do_get()
	{
		$function = isset( $this->input['type'] ) && $this->input['type'] != '' ? $this->input['type'] : '';

		if ( $function != 'do_post' && method_exists( $this, $function ) )
		{
			$data = $this->input;
			unset( $data[ $function ] );
			$result = $this->$function( $data  );
		}
		else
		{
			$result = array( 'code' => -999, 'data' => 'unknown method' );
		}

		$this->shows( json_encode( $result ) );
	}

	function do_post()
	{
		return $this->do_get();
	}
	
}
?>