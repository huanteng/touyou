<?php
	class cookie
	{
		var $key = '';
		var $data = '';
		var $domain = '';

		function cookie( $data, $key, $domain )
		{
			$this->data = $data;
			//$this->key = md5( $_SERVER['REMOTE_ADDR'] . $key . $_SERVER['HTTP_USER_AGENT'] );
			$this->key = md5( $key . $_SERVER['HTTP_USER_AGENT'] );
			$domain = explode( ':', $domain );
			$this->domain = $domain[0];
		}

		function get( $name, $code = false )
		{
			return isset( $this->data[$name] ) ? ( $code ? $this->code( $this->data[$name], 'decode' ) : $this->data[$name] ) : '';
		}

		function set( $name, $value, $code = false, $lifetime = 0, $secure = false, $httponly = true )
		{
			$value = $code ? $this->code( $value ) : $value;
			return setcookie( $name, $value, $lifetime, '/', $this->domain, $secure, $httponly );
		}

		function code( $text, $operate = 'encode' )
		{
			$data = '';
			$key = $this->key;
			$key_length = strlen( $key );
			$text = $operate == 'decode' ? base64_decode( $text ) : $text;
			$text_length = strlen( $text );
			for( $i = 0; $i < $text_length; $i += $key_length ) $data .= substr( $text, $i, $key_length ) ^ $key;
			$data = $operate == 'encode' ? str_replace( '=', '', base64_encode( $data ) ) : $data;
			return $data;
		}
	}
?>