<?php
	class template
	{
		var $data = array();
		var $auto = array();
		var $path = '';

		function template( $path )
		{
			$this->path = $path;
		}

		function assign( $key, $value )
		{
			$this->data[$key] = $value;
		}

		function appoint( $array )
		{
			foreach( $array as $key => $value ) $this->assign( $key, $value );
		}

		function parse( $_reserve_template_path_reserve_ )
		{
			extract( $this->auto );
			extract( $this->data );
			ob_start();
			require $this->path . $_reserve_template_path_reserve_ ;
			$_reserve_contents_reserve_ = ob_get_contents();
			ob_end_clean();
			return $_reserve_contents_reserve_;
		}

		function result( $content_file = '', $header_file = 'header.php', $footer_file = 'footer.php' )
		{
			return $this->parse( $header_file ) . $this->parse( $content_file ) . $this->parse( $footer_file );
		}
	}
?>