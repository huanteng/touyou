<?php
class controller
{
	function controller()
	{
		require_once dirname( __FILE__ ) . '/' . 'config.php';
		set_error_handler( array( & $this, 'mark' ) );
		register_shutdown_function( array( & $this, '_finish' ) );
		//set_magic_quotes_runtime( 0 );
		mb_internal_encoding( 'UTF-8' );
		$this->input = $_SERVER['REQUEST_METHOD'] == 'GET' ? $this->clean( $_GET ) : $this->clean( $_POST );
		if ( $this->output_handler != '' ) ob_start( $this->output_handler );
		if ( $this->content_type != '' ) header( $this->content_type );
		if ( ! $this->debug ) error_reporting( 0 );
	}

	function do_get()
	{
		echo __FUNCTION__;
	}

	function do_post()
	{
		echo __FUNCTION__;
	}

	function run()
	{
		$method = strtolower( $_SERVER['REQUEST_METHOD'] );
		$handle = true;

		foreach( $this->before[$method] as $function => $contine )
		{
			if ( ! $this->$function() )
			{
				$handle = $contine;
				break;
			}
		}

		if ( $handle ) $this->{'do_'.$method}();

		foreach( $this->after[$method] as $function ) if ( ! $this->$function() ) break;

		if ( ! $this->right() )
		{
			$this->error['other'] = array( 'data' => $_REQUEST, 'time' => date( 'Y-m-d H:i:s', time() ), 'url' => $_SERVER['SCRIPT_FILENAME'] );
			file_put_contents( config('library_dir') . '../cache/error.log', var_export( $this->error, true ) . "\n\n", FILE_APPEND );
			if ( $this->debug ) $this->alert();
		}
	}

	function prompt( $data, $url, $file = 'prompt.php', $time = 3 )
	{
		$default = '';

		foreach( $url as $index => $value )
		{
			if ( isset( $value['default'] ) )
			{
				$default = $index;
				break;
			}
		}

		$template = load( 'template' );
		$template->assign( 'data', $data );
		$template->assign( 'time', $time );
		$template->assign( 'url', $url );
		$template->assign( 'default', $default );
		echo $template->parse( $file );
	}

	function alert()
	{
		ob_end_clean();
		if ( $this->output_handler != '' ) ob_start( $this->output_handler );
		echo '<pre>';
		print_r( $this->error );
		echo '</pre>';
	}

	function right()
	{
		$database = load( 'database' );
		foreach( $database->sql as $index => $value ) foreach( $value as $sql => $error ) if ( $error != '' ) $this->error['sql'][$sql] = $error;
		return count( $this->error['php'] ) == 0 && count( $this->error['sql'] ) == 0;
	}

	function format( $value )
	{
		return addslashes( strip_tags( ( trim( $value ) ) ) );
	}

	function clean( & $array )
	{
		foreach( $array	as $key	=> $value ) $array[$key] = is_array( $array[$key] ) ? $this->clean( $value ) : $this->format( $value );
		return $array;
	}

	function mark( $no, $str, $file, $line )
	{
		if ( $no != 2048 ) $this->error['php'][] = array( 'no' => $no, 'file' => $file, 'line' => $line, 'str' => $str );
	}

	function _finish()
	{
		$error = error_get_last();

		if ( $error !== NULL )
		{
			$this->error['php'][] = array( 'no' => $error['type'], 'file' => $error['file'], 'line' => $error['line'], 'str' => $error['message'] );
			$this->error['other'] = array( 'data' => $_REQUEST, 'time' => date( 'Y-m-d H:i:s', time() ), 'url' => $_SERVER['SCRIPT_FILENAME'] );

			include dirname( __FILE__ ) . '/../log.conf.php';

			if ( isset( $log['error'] ) )
			{
				$data = array( 'uid' => 0, 'url' => $_SERVER['REQUEST_URI'] );
				$data['input'] = var_export( $_REQUEST, true );
				$data['response'] = var_export( $this->error, true );
				$data['sql'] = var_export( $this->db->sql, true );

				$log = load( 'biz.interface_log' );
				$log->add( $data );
			}
			else
			{
				file_put_contents( config('library_dir') . '../cache/error.log', var_export( $this->error, true ) . "\n\n", FILE_APPEND );
			}
		}
	}

	var $error = array( 'php' => array(), 'sql' => array() );
	var $before = array( 'get' => array(), 'post' => array() );
	var $after = array( 'get' => array(), 'post' => array() );
	var $input = array();
	var $debug = false;
	var $output_handler = 'ob_gzhandler';
	var $content_type = 'content-type: text/html; charset=utf-8';
}
?>