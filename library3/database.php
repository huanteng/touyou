<?php
class database
{
	var	$host = '';
	var	$name = '';
	var	$user = '';
	var	$pass = '';
	var $sql = array();
	var	$link = null;
	var $count = 0;	// 批事务的序号
	var $index = 0;	// 事务内sql序号，主要作用是用来区分同sql

	function database( $host, $name, $user, $pass )
	{
		$this->host = $host;
		$this->name = $name;
		$this->user = $user;
		$this->pass = $pass;
		register_shutdown_function( array( & $this, 'disconnect' ) );
	}

	function save_cache( $key, $data, $php = true )
	{
		$file = $this->cache_name( $key );
		$content = $php ? '<?php $__cache__ = ' . var_export( $data, true ) . ' ?>' : $data;
		return file_put_contents( $file, $content );
	}

	function load_cache( $key, $php = true )
	{
		$file = $this->cache_name( $key );

		if ( $php )
		{
			require $file;
			return $__cache__;
		}
		else
		{
			return file_get_contents( $file );
		}
	}

	function cache_name( $sql, $key = '' )
	{
		$base = $key != '' ? $key : md5( $sql );
		$dir = '/data/www/touyou/cache/' . $base[0] . $base[31] . '/';
		if ( ! is_dir( $dir ) ) mkdir( $dir, 0755, true );
		return $dir . $base;
	}

	function exist_cache( $key, $expire = 0, $other = '' )
	{
		$file = $this->cache_name( $key, $other );
		return is_file( $file ) && ( $expire < 1 || ( time() - filemtime( $file ) <= $expire ) );
	}

	function _connect()
	{
		$this->link = mysql_connect( $this->host, $this->user, $this->pass );
		mysql_select_db( $this->name, $this->link );
		mysql_unbuffered_query( "set names 'utf8'", $this->link );
	}

	function connect()
	{
		if ( ! is_resource( $this->link ) )
		{
			$this->_connect();
		}
		else
		{
			if ( ! mysql_ping( $this->link ) )
			{
				mysql_close( $this->link );
				$this->_connect();
			}
		}
	}

	function disconnect()
	{
		if ( is_resource( $this->link ) )
		{
			mysql_close( $this->link );
			$this->link = null;
		}
	}

	function start()
	{
		$this->count++;
		$this->index = 0;
		$this->sql[$this->count] = array();
		$this->command( 'begin' );
	}

	function end( $command = '' )
	{
		$right = true;

		if ( isset( $this->sql[$this->count] ) )
		{
			foreach( $this->sql[$this->count] as $sql => $error )
			{
				if ( $error != '' )
				{
					$right = false;
					break;
				}
			}
		}

		$this->command( $command != '' ? $command : ( $right ? 'commit' : 'rollback' ) );
		$this->count++;
		return $right;
	}

	function command( $sql )
	{
		$this->connect();
		
		mysql_unbuffered_query( $sql, $this->link );
		
		$result = mysql_error( $this->link );
		$affected = $result == '' ? mysql_affected_rows( $this->link ) : -1;

		$this->index++;
		$this->sql[$this->count][$this->index . '.' . $sql] = $result;

		return $result == '' ? ( $affected > 0 ? $affected : 0 ) : -1;
	}

	function select( $sql, $time = 0, $key = '' )
	{
		//if ( isset( $_REQUEST['sid'] ) && $_REQUEST['sid'] == 'c3d72bda25e6a362ac32a77115bcbc4f6ae63d86' ) file_put_contents( 'sql.txt', $sql . "\n", FILE_APPEND );
		$data = array();

		if ( $time > 0 && $this->exist_cache( $sql, $time, $key ) )
		{
			$data = $this->load_cache( $sql, $key );
		}
		else
		{
			$this->connect();
			$result = mysql_unbuffered_query( $sql, $this->link );
			$error = mysql_error( $this->link );
			$this->index++;
			$this->sql[$this->count][$this->index . '.' . $sql] = $error;
			if ( $error == '' ) while( $row = mysql_fetch_assoc( $result ) ) $data[] = $row;
			if ( is_resource( $result ) ) mysql_free_result( $result );
			if ( $time > 0 ) $this->save_cache( $sql, $data, $key );
		}

		return $data;
	}

	function unique( $sql )
	{
		$data = $this->select( $sql );
		return isset( $data[1] ) ? array() : ( isset( $data[0] ) ? $data[0] : array() );
	}

	function total( $sql )
	{
		$total = $this->select( 'select count( 1 ) as total ' . $sql );
		return isset( $total[0]['total'] ) ? $total[0]['total'] : 0;
	}

	function split( $count, $list, $size, $current )
	{
		$total = $this->total( $count );
		$page = ceil( $total / $size );
		$current = is_numeric( $current ) && $current >= 1 ? $current : 1;
		//20130109，不必，并且这将导致客户端难以控制$current = $current > $page ? $page : $current;
		$offset = $current == 1 ? 0 : ( $current - 1 ) * $size;
		$offset = $offset < 1 ? 0 : $offset;
		$result = array( 'total' => $total, 'page' => $page, 'current' => $current, 'size' => $size, 'offset' => $offset, 'data' => array() );
		if ( $total > 0 ) $result['data'] = $this->select( $list . ' limit ' . $offset . ', ' . $size );
		return $result;
	}

	function id()
	{
		return mysql_insert_id( $this->link );
	}

	function add( $table, $array )
	{
		$this->command( 'insert into ' . $table . ' ( `' . join( '`, `', array_keys( $array ) ) . "` ) values ( '" . join( "', '", $array ) . "' )" );
		$id = mysql_error( $this->link ) != '' ? 0 : $this->id();
		return $id;
	}

	function set( $table, $array, $term )
	{
		$sql = '';
		foreach( $array as $key => $value ) $sql .= ( is_numeric( $key ) ? $value : '`' . $key . "` = '" . $value . "'" ) .', ';
		$sql = 'update ' . $table . ' set ' . substr( $sql, 0, -2 ) . ' where ' . $this->term( $term );
		return $this->command( $sql );
	}

	/* 删除数据
	 * 参数：
	 *	table：表名
	 *	term：条件，为数组形式
	 *	id：默认为空，如存在值，则以这为key逐条删除，以防止被锁
	 */
	function del( $table, $term, $id = '' )
	{
		if( $id == '' )
		{
			return $this->command( 'delete from ' . $table . ' where ' . $this->term( $term ) );
		}
		else
		{
			$data = $this->get( $id, $table, $term );
			foreach ( $data as $v )
			{
				$this->del( $table, array( $id => $v[ $id ] ) );
			}
			return ;
		}
	}

	function term( $array, $join = 'and' )
	{
		$sql = '';
		foreach( $array as $key => $value ) $sql .= is_numeric( $key ) ? ( ' ( ' . $value . ' ) ' . $join . ' ' ) : '`' . $key . "` = '" . $value . "' " . $join . " ";
		return ( $sql == '' ) ? '' : ' ( ' . substr( $sql, 0, '-' . ( strlen( $join ) + 1 ) ) . ' ) ';
	}

	// 返回记录集
	function get( $field, $table, $term = array(), $orderby = '', $limit = '' )
	{
		$sql = "select $field from $table";
		$where = $this->term( $term );
		if( $where != '' ) $sql .= " where $where";
		if( $orderby != '' ) $sql .= " order by $orderby";
		if( $limit != '' ) $sql .= " limit $limit";

		return $this->select( $sql );
	}
      
  // 返回单条数组。注意：1、不作单条检测；2、仅返回一维数组
	function get1( $field, $table, $term = array(), $orderby = '' )
	{
		$data = $this->get( $field, $table, $term, $orderby, 1 );
		return isset( $data[0] ) ? $data[0] : array();
	}

	function replace( $table, $array )
	{
		return $this->command( 'replace into ' . $table . ' ( `' . join( '`, `', array_keys( $array ) ) . "` ) values ( '" . join( "', '", $array ) . "' )" );
	}
	
	// 是否存在符合条件的数据，返回true|false
	function exists( $table, $term = array() )
	{
		$sql = "select 1 from $table";
		$where = $this->term( $term );
		if( $where != '' ) $sql .= " where $where";
		$data = $this->select( $sql );
		return isset( $data[0] );
	}
}
?>