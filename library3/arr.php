<?php
class arr
{
	/*返回新数组，元素为原数组的随机一部分
     * $arr:原数组
     * $keep_count：保留的数量
	 */
	function part( $arr, $keep_count )
	{
        $key = array_keys( $arr );
        shuffle( $key );
        $count = count( $arr ) - $keep_count;
        
		for( $i = 0; $i < $count; ++$i )
		{
			unset( $arr[ array_pop( $key ) ] );
		}
		return $arr;
	}
	
	// <editor-fold defaultstate="collapsed" desc="make_int，返回一个数组，元素由整数组成">
	function make_int( $min, $max )
	{
		$arr = array();
		for( $i = $min; $i <= $max; ++$i ) $arr[] = $i;
		return $arr;
	}
	// </editor-fold>
 
	// <editor-fold defaultstate="collapsed" desc="make_rand，返回一个数组，元素由随机整数组成">
	/* 
	 * 参数：length，返回数组的长度
	 *	    min, max，每个元素下上限
	 */	
	function make_rand( $length, $min, $max )
	{
		$arr = array();
		for( $i = 0; $i < $length; ++$i ) $arr[] = mt_rand($min, $max);
		return $arr;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="del，删除不要的元素，返回新数组">
	function del( $source, $no )
	{
		foreach( $source as $k => $v )
		{
			if( in_array( $v, $no ) ) unset( $source[$k] );
		}
		return $source;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="keep，仅保留必要的元素，返回新数组">
	function keep( $source, $yes )
	{
		$out = array();
		foreach( $yes as $k )
		{
			if( isset($source[$k]) )
			{
				$out[$k] = $source[$k];
			}
		}
		return $out;
	}
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="json_decode，将记录集的其中一列json_decode">
	/* 参数：
	 *	data
	 *	key
	 * 返回：
	 *	转换后后记录集
	 */
	function json_decode( $data, $key )
	{
		foreach( $data as $k => $v )
		{
			if( $v[ $key ] == '' ) continue;
			$data[ $k ][ $key ] = json_decode( $v[ $key ] );
		}
		return $data;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="rename_key，将其中一列改名">
	/* 参数：
	 *	data，数组
	 *	old：旧key名
	 *	new：新key名
	 */
	function rename_key( $data, $old, $new )
	{
		foreach( $data as $k => $v )
		{
			$v[ $new ] = $v[ $old ];
			unset( $v[ $old ] );
			$data[$k] = $v;
		}
		return $data;
	}
	// </editor-fold>

	/* 如果不存在，则设置默认值
	 * 参数：
	 *	in：原数组
	 *	default：默认值数组
	 * 返回值：
	 *	新数组，如果原数组中没对应值，则以对应的默认值填充
	 */
	function set_default( $in, $default )
	{
		foreach( $default as $k => $v )
		{
			if( !isset($in[$k]) )
			{
				$in[$k] = $v;
			}
		}
		return $in;
	}
}
?>