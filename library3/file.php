<?php
class file
{
	/*
	 * 删除文件
	 */
	function del( $filename )
	{
		return is_file( $filename ) && unlink( $filename );
	}

	/** 删除旧文件
	 * @param $path：完整目录
	 * @param $day_ago：删除几天前？
	 */
	function del_old( $dir, $day_ago )
	{
		$time = strtotime( "-$day_ago days" );
		if( !empty($dir) && is_dir( $dir ))
		{
			$handle = opendir($dir);
			while(($file = readdir($handle)) !== false)
			{
				if( $file != '.' && $file != '..' )
				{
					$full = $dir . '/' . $file;
					if( $time > filemtime( $full ) )
					{
						unlink( $full );
					}
				}
			}
		}
	}
}
?>