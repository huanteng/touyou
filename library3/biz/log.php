<?php
require_once dirname( __FILE__ ) .'/base.php';

class log extends base
{
	var $level = 0;

	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,module,type,relate_id,code,data,content,ip,valid_time,url,time';
		$this->time_field = 'time';
	}
	function type_dict( $key = '' )
	{
		$data = array(
			0 => '信息',
			1 => '警告',
			2 => '错误',
		);

		if( $key != '' )
		{
			if( isset( $data[ $key ] ) )
			{
				$data = $data[ $key ];
			}
			else
			{
				$this->create( array( 'type' => 2, 'content' => '类型不存在，type=' . $key ) );

				$data = '错误';
			}
		}

		return $data;
	}

	/** 增加记录
	 * @param $info
	 * allow_repeat：是否允许重复log，默认为true。如不允许重复且数据重复时，则更新旧有数据的时间
	 * 注：其中参数中的valid_time单位为天，表示多少天后过期，默认为7。
	 */
	function create( $info, $allow_repeat = true )
	{
		if( isset( $info[ 'data' ] ) )
		{
			$info[ 'data' ] = load('arr')->php_json_encode( $info[ 'data' ] );
		}

		if( !$allow_repeat )
		{
			$info2 = $this->get1( 'id', $info );
			if( !empty( $info2 ) )
			{
				// 注：仅更新一条，其余忽略
				$this->set( $info2 );
				return;
			}
		}

		if( !isset( $info[ 'ip' ] ) )
		{
			$info[ 'ip' ] = $_SERVER['REMOTE_ADDR'];
		}
		$info[ 'valid_time' ] = time() + ( value( $info, 'valid_time', 7 ) * 86400 );

		return $this->add( $info );
	}

	// 返回模块列表
	function module_dict()
	{
		$data = $this->get( 'distinct module', array() );

		$out = array();
		foreach( $data as $v )
		{
			$out[ $v[ 'module' ] ] = $v[ 'module' ];
		}

		return $out;
	}

	// 设置调试级别
	function set_level( $level )
	{
		$this->level = $level;
	}
}
?>