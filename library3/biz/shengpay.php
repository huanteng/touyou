﻿<?php
require_once dirname( __FILE__ ) . '/base.php';

class shengpay extends base
{
	var $MsgSender = '146390';
	var $Key = 'huantengtouyou';

	function __construct()
	{
		$host = $_SERVER['HTTP_HOST'];

		// 给对方跳转的url 给浏览器用
		$this->PageUrl = 'http://' . $host . '/payment.php?method=shengpay_page';

		// 给对方post的url 给服务端用
		$this->NotifyUrl = 'http://' . $host . '/payment.php?method=shengpay_finish';
	}

	function post( $in )
	{
		$array=array(
			'Name'=>'B2CPayment',
			'Version'=>'V4.1.1.1.1',
			'Charset'=>'UTF-8',
			'MsgSender' => $this->MsgSender,
			'SendTime'=>date('YmdHis'),
			'OrderTime'=>date('YmdHis'),
			'PayType'=>'',
			'InstCode'=>'',
			'PageUrl'=> $this->PageUrl,
			'NotifyUrl' => $this->NotifyUrl,
			'ProductName'=>'金币',
			'BuyerContact'=>'',
			'BuyerIp'=>'',
			'Ext1'=>'',
			'Ext2'=>'',
			'SignType'=>'MD5',
		);

		$this->init( $array );
		$this->setKey( $this->Key );

		$this->takeOrder( $in[ 'billno' ], $in[ 'money' ] );
	}

	function finish( $in )
	{
		// 参考旧写法，两者无差别对待
		return $this->page( $in );
	}

	/** page跳转检查
	 * @param $in
	 * 返回：json，具体提示信息在memo属性
	 */
	function page( $in )
	{
		$this->log( '', 0, 'page', array( 'data' => $in ) );

		$this->setKey( $this->Key );

		if ( $this->returnSign() )
		{
			$billno = $in[ 'OrderNo' ];
			$money = $in[ 'money' ];

			$info = biz( 'payment' )->get1( '*', array( 'billno' => $billno ) );

			//校验通过开始处理订单
			if( $info[ 'money' ] == $money )
			{
				// todo，状态是否正确的判断呢？
				biz( 'payment' )->finish( $billno, 1 );

				return $this->out( 1, 'OK' );
			} else {
				return $this->out( -1, 'Error' );
			}
		}
		else
		{
			$this->log( '', 0, 'MD5校验失败' );

			return $this->out( -2, 'Error' );
		}
	}

	// 以下为原内容
	private $payHost;
	private $debug=false;
	private $params=array(
		'Name'=>'B2CPayment',
		'Version'=>'V4.1.1.1.1',
		'Charset'=>'UTF-8',
		'MsgSender'=>'100894',
		'SendTime'=>'',
		'OrderNo'=>'',
		'OrderAmount'=>'',
		'OrderTime'=>'',
		'PayType'=>'',
		'InstCode'=>'',
		'PageUrl'=>'',
		'NotifyUrl'=>'',
		'ProductName'=>'',
		'BuyerContact'=>'',
		'BuyerIp'=>'',
		'Ext1'=>'',
		'Ext2'=>'',
		'SignType'=>'MD5',
		'SignMsg'=>'',
	);

	/**
	 * old url
	 * sanbox url: $this->payHost='http://mer.mas.sdo.com/web-acquire-channel/cashier.htm';
	 * product url: $this->payHost='http://mas.sdo.com/web-acquire-channel/cashier.htm';
	 *
	 * https://mas.shengpay.com/web-acquire-channel/cashier.htm
	 */
	function init($array=array()){
		if($this->debug)
			$this->payHost='https://mer.mas.shengpay.com/web-acquire-channel/cashier.htm';
		else
			$this->payHost='https://mas.shengpay.com/web-acquire-channel/cashier.htm';
		foreach($array as $key=>$value){
			$this->params[$key]=$value;
		}
	}

	function setKey($key){
		$this->Key=$key;
	}
	function setParam($key,$value){
		$this->params[$key]=$value;
	}

	function takeOrder($oid,$fee){
		$this->params['OrderNo']=$oid;
		$this->params['OrderAmount']=$fee;
		$origin='';
		foreach($this->params as $key=>$value){
			if(!empty($value))
				$origin.=$value;
		}
		$SignMsg=strtoupper(md5($origin.$this->Key));
		$this->params['SignMsg']=$SignMsg;
		echo '<meta http-equiv = "content-Type" content = "text/html; charset = '.$this->params['Charset'].'"/>
			<form  method="post" action="'.$this->payHost.'">';
		foreach($this->params as $key=>$value){
			echo '<input type="hidden" name="'.$key.'" value="'.$value.'"/>';
		}
		echo '<input type="submit" name="submit" value="提交到盛付通" id="dh">
				<script>var a=document.getElementById("dh");a.click();</script>
			</form>';
	}

	function returnSign(){
		$params=array(
			'Name'=>'',
			'Version'=>'',
			'Charset'=>'',
			'TraceNo'=>'',
			'MsgSender'=>'',
			'SendTime'=>'',
			'InstCode'=>'',
			'OrderNo'=>'',
			'OrderAmount'=>'',
			'TransNo'=>'',
			'TransAmount'=>'',
			'TransStatus'=>'',
			'TransType'=>'',
			'TransTime'=>'',
			'MerchantNo'=>'',
			'ErrorCode'=>'',
			'ErrorMsg'=>'',
			'Ext1'=>'',
			'Ext2'=>'',
			'SignType'=>'MD5',
		);
		foreach($_POST as $key=>$value){
			if(isset($params[$key])){
				$params[$key]=$value;
			}
		}
		$TransStatus=(int)$_POST['TransStatus'];
		$origin='';
		foreach($params as $key=>$value){
			if(!empty($value))
				$origin.=$value;
		}
		$SignMsg=strtoupper(md5($origin.$this->Key));
		if($SignMsg==$_POST['SignMsg'] and $TransStatus==1){
			return true;
		}else{
			return false;
		}
	}



}
?>