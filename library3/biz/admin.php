<?php

require_once dirname( __FILE__ ) . '/base.php';

class admin extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,username,password,created,last_login_time,remark,status';
	}

	function status()
	{
		return array( 0 => '关闭', 1 => '正常' );
	}

	/*
	 * 这个是后台用的，要和前台区别开
	 */
	function hash( $data )
	{
		return md5( $data . 'jdf32#w44LK' );
	}

	function login_info()
	{
		$cookie = load( 'cookie' );
		return unserialize( $cookie->get( 'admin_info', true ) );
	}

	function password( $data )
	{
		$self_data = $this->get1( '*', array('id'=>$data['id']) );

		if ( $self_data['password'] != $this->hash( $data['old_password'] ) ) {
			return -1;	//旧密码不正确
		} else {
			unset($data['old_password']);
			$data['password'] = $this->hash( $data['password'] );

			parent::set( $data );
			return 1;
		}
	}
	
	function set($data)
	{
		if( value( $data, 'password' )!='') {
			$data['password'] = $this->hash( $data['password'] );
		} else {
			unset($data['password']);
		}
		parent::set( $data );
		return 1;
	}
	
	function del( $id )
	{
		parent::del( $id );
		biz( 'admin_privilege' )->del( $id );
		return 1;
	}

	function login( $in )
	{
		$cookie = load( 'cookie' );

		$login_code = $cookie->get( 'login_code', true );
		if( $login_code != $in['code'] )
		{
			return $this->out( -1, '验证码错误' );
		}

		$info = $this->get1( '*', array( 'username' => $in[ 'username' ] ) );
		if( empty( $info ) )
		{
			return $this->out( -2, '不存在用户' );
		}

		if( $info[ 'status' ] != 1 )
		{
			return $this->out( -3, '帐号停止使用' );
		}

		if( $info[ 'password' ] != $this->hash( $in[ 'password' ] ) )
		{
			return $this->out( -4, '密码错误' );
		}

		$this->set( array( 'last_login_time' => time(), 'id' => $info[ 'id' ] ) );

		$cookie->set( 'admin_info', serialize( $info ), true );

		$data = biz( 'admin_privilege' )->get( 'privilege', array( 'admin' => $info[ 'id' ] ) );

		$privilege = array( '0' => '' );
		foreach( $data as $v )
		{
			$privilege[ $v[ 'privilege' ] ] = '';
		}
		$cookie->set( 'privilege', serialize( $privilege ), true );

		$this->log( '', 0, $in[ 'username' ] . ' 登录' );

		return $this->out( 1, 'ok' );
	}

	function logout()
	{
		$cookie = load( 'cookie' );
		$cookie->set( 'admin_info', '' );
		$cookie->set( 'privilege', '' );
	}

	function menu()
	{
		$admin = $this->login_info();
		$data = biz( 'admin_privilege' )->get( 'privilege', array( 'admin' => $admin[ 'id' ] ) );
		$menu = array();
		if( !empty( $data ) )
		{
			$pids = $this->implode2( $data, '' );

			$admin_function = biz('admin_function');
			$menu_parent = $admin_function->get('*', array('display =1 and parent_id = 0 and id in ('.$pids.')'), '`order` desc' );
			$menu_child = $admin_function->get('*', array('display =1 and parent_id != 0 and id in ('.$pids.')'), '`order` desc' );

			foreach( $menu_parent as $p )
			{
				$p[ 'child' ] = array();
				foreach ( $menu_child as $k => $c )
				{
					if( $p['id'] == $c['parent_id'] )
					{
						$p['child'][] = $c;
						unset( $menu_child[ $k ] );
					}
				}
				$menu[$p['id']] = $p;
			}
		}

		return $menu;
	}

	/*
	 * 复制
	 */

	function copy( $id )
	{
		$data = $this->get_from_id( $id );
		unset( $data[ 'id' ] );
		$newid = $this->add( $data );

		$admin_privilege = biz( 'admin_privilege' );
		$p = $admin_privilege->get( '*', array( 'admin' => $id ) );
		foreach( $p as $k => $v )
		{
			$admin_privilege->add( array( 'admin' => $newid, 'privilege' => $v[ 'privilege' ] ) );
		}

		return $newid;
	}

}

?>
