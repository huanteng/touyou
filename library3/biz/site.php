<?php
// 网站之间通讯
require_once dirname( __FILE__ ) . '/base.php';

class site extends base
{
	function info( $id )
	{
		$data = array(
			1 => array( 'name' => '推介网', 'host' => 'b.tuijie.cc', 'interface' => 'tool', 'password' => 'tuijs#*3' ),
			2 => array( 'name' => '推球网', 'host' => 'b.tuiqiu.net', 'interface' => 'tool', 'password' => '12NKabc' ),
			3 => array( 'name' => '999足球网', 'host' => 'www.999zq.com', 'interface' => 'tool', 'password' => '!24445ssdf' ),
			4 => array( 'name' => '推球网商城', 'host' => 'www.tuiqiu2.net', 'interface' => 'tool', 'password' => 'tq_Sos123f' ),
			5 => array( 'name' => '99goad', 'host' => 'www.99goad.com', 'interface' => 'tool', 'password' => '919EW438ab' ),
			6 => array( 'name' => '推介网', 'host' => 'www.tuijie.cc', 'interface' => 'tool', 'password' => 'bs#jie*3' ),
			7 => array( 'name' => '推球网', 'host' => 'www.tuiqiu.net', 'interface' => 'tool', 'password' => 'ad5#67Kee' ),
			9 => array( 'name' => '骰友', 'host' => 'www.touyou.mobi', 'interface' => 'tool', 'password' => 'oyeyJ6THhmU4bq' ),
		);

		if( config( 'develop' ) )
		{
			$data[ 1 ][ 'host' ] = 'tuijie.local.com';
			$data[ 2 ][ 'host' ] = 'tuiqiu.local.com';
			$data[ 3 ][ 'host' ] = '999zq.local.com';
			$data[ 4 ][ 'host' ] = 'tuiqiu2.local.com';
			$data[ 5 ][ 'host' ] = '99goad.local.com';
			$data[ 6 ][ 'host' ] = 'tuijie.local.com';
			$data[ 7 ][ 'host' ] = 'tuiqiu.local.com';
			$data[ 9 ][ 'host' ] = 'touyou.local.com';
		}

		return $this->dict( $data, $id, null, '网站标识未定义' ) ;
	}

	/** 加签名
	 * @param $string：要签名的字符串
	 * @return string：加签名后的字符串
	 */
	function add_sign( $string )
	{
		$info = $this->info( config( 'site.id' ) );
		return $string . '&sign=' . $this->get_sign( $string, $info[ 'password' ] );
	}

	/** 获得签名
	 * @param $string
	 * @param $password
	 * @return string
	 */
	function get_sign( $string, $password )
	{
		return md5( $string . '&' . $password );
	}

	/** 检查签名是否正确
	 * @param $in：传入的get参数
	 * @return bool：是否正确
	 */
	function check_sign( $in )
	{
		$info = $this->info( $in[ 'site' ] );

		if( empty( $info) )
		{
			return $this->out( -1, '网站标识未定义' );
		}

		$s = array();
		foreach( $in as $k => $v )
		{
			if( $k != 'sign' )
			{
				$s[] = $k . '=' . $v;
			}
		}

		$s = implode( '&', $s );

		if( $this->get_sign( $s, $info[ 'password' ] ) != value( $in, 'sign' ) )
		{
			return $this->out( -2, '签名错误' );
		}

		return $this->out( 'ok' );
	}

	/** 获得同行站url，
	 * @param $id：对方id
	 * @param $file
	 * @param $method
	 * @param $data: array
	 * @param $is_interface: true|false，是否接口目录？如true，返回接口目录，如false，返回根目录。默认为true
	 * @return url
	 */
	function get_url( $id, $file, $method, $data, $is_interface = true )
	{
		$info = $this->info( $id );

		$url = array();
		foreach( $data as $k => $v )
		{
			$url[] = $k . '=' . $v;
		}
		$url = implode( '&', $url );

		return 'http://' . $info[ 'host' ] . '/' . ( $is_interface ? $info[ 'interface' ] : '' ) . '/' . $file .
			'.php?method=' . $method . '&' . $this->add_sign( $url );
	}

	/** 通过post形式，将数据发给同行站，
	 * @param $id：对方id
	 * @param $file
	 * @param $method
	 * @param $data: array
	 * @param $is_interface: true|false，是否接口目录？如true，返回接口目录，如false，返回根目录。默认为true
	 * @return 用于post的页面内容
	 */
	function post( $id, $file, $method, $data, $is_interface = true )
	{
		$data[ 'site' ] = config( 'site.id' );

		$url = array();
		foreach( $data as $k => $v )
		{
			$url[] = $k . '=' . $v;
		}
		$url = implode( '&', $url );

		$self = $this->info( $data[ 'site' ] );
		$sign = $this->get_sign( $url, $self[ 'password' ] );

		$info = $this->info( $id );
		$content = '<html><body><form name="frm" action="http://' . $info[ 'host' ] . '/' .
			( $is_interface ? $info[ 'interface' ] . '/' : '' ) . $file . '.php" method="post">';

		foreach( $data as $k => $v )
		{
			$content .= '<input type="hidden" name="' . $k . '" value="' . $v . '">';
		}

		$content .= '<input type="hidden" name="method" value="' . $method. '">'.
			'<input type="hidden" name="sign" value="' . $sign . '">'.
			//'<input type="submit" value="提交">'.
		 	'</form>正在处理，请稍候...'.
			'<script language="javascript">document.frm.submit();</script>'.
			'</body></html>';
		return $content;
	}


	/** 用get形式，请求同行站，并获得json结果。
	 * @param $id：对方id
	 * @param $file
	 * @param $method
	 * @param $data: array
	 * @param $is_interface: true|false，是否接口目录？如true，返回接口目录，如false，返回根目录。默认为true
	 * @return json结果
	 *
	 * 注意：
	 * get_json和post两种方式完全不同。前者是在服务器端直接获得数据，可用于敏感数据的处理；后者是引导浏览器post。
	 */
	function get_json( $id, $file, $method, $data, $is_interface = true )
	{
		$data[ 'site' ] = config( 'site.id' );
		$info = $this->info( $id );

		$url = array();
		foreach( $data as $k => $v )
		{
			$url[] = $k . '=' . $v;
		}
		$url = implode( '&', $url );

		$url = 'http://' . $info[ 'host' ] . '/' . ( $is_interface ? $info[ 'interface' ]. '/' : '' )  . $file .
		'.php?method=' . $method . '&' . $this->add_sign( $url );

		$this->log( '', 0, 'url is' . $url );

		return load('http')->get_json( $url );
	}

}
?>