<?php 
require_once dirname(__FILE__) . '/base.php';

class error extends base
{
	var $file = '';

	function __construct()
	{
		$this->file = config('dir.cache') . 'error.php';
	}

	/*
	 * 之所以加几个无用参数，是为了和base兼容，避免报错
	 */
	function get( $in = array(), $a = '', $b = '', $c = '' )
	{
		$file = $this->file;

		$out = array();

		if ( is_file( $file ) )
		{
			if( filesize( $file ) > 2048000 )
			{
				$out[] = array( 'notice' => '文件过大，请到服务器查看，或清空。当前文件大小为：' . filesize( $file ) );
			}
			else
			{
				$line_array = explode( "\n\n", file_get_contents( $file ) );

				foreach( $line_array as $line )
				{
					if ( $line != '' )
					{
						$content = '$_data_ = ' . $line . ';';
						eval( $content );
						$out[] = $_data_;
					}
				}
			}
		}

		return $out;
	}

	function clear()
	{
		return is_file( $this->file ) && unlink( $this->file );
	}
	
}
?>