<?php
require_once dirname( __FILE__ ) . '/base.php';

class alert extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,task_id,content,url,color,time';
		$this->time_field = 'time';
	}

	/* 创建任务
	 * 参数：
	 *	content
	 *	url
	 */
	function create( $in )
	{
		$info = $this->get1( '*', array( 'content' => $in[ 'content' ] ) );
		if( empty( $info ) )
		{
			return $this->add( $in );
		}
		else
		{
			return $this->set( array( 'id' => $info[ 'id' ] ) );
		}
	}

}
?>
