<?php
require_once dirname( __FILE__ ) . '/base.php';

class admin_function extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,parent_id,code,name,url,remark,order,display';
	}

	function display_dict( $i = '' )
	{
		$data = array( '0' => '不显示', '1' => '正常' );
		return $this->dict( $data, $i, '错误', 'admin_function::display_dict' );
	}

	/**
	 * 返回父菜单数据，每行为id=>name
	 */
	function parent()
	{
		$data = $this->get( 'id,name', array( 'parent_id=0' ), '`order` desc' );
		return load('dataset')->key_by_id( $data, 'id', 'name' );
	}

	/** 修改菜单顺序
	 * @param $in： {id, direct}
	 * @return bool
	 */
	function updown( $in )
	{
		$id = 0;
		$direct = 'up';
		extract( $in );

		$info = $this->get_from_id( $id );

		// 当前排序号
		$current_order = $info[ 'order' ];

		if( $direct == 'up' )
		{
			$compare = '>';
			$compare_direct = '';
		}
		else
		{
			$compare = '<';
			$compare_direct = 'desc';
		}

		// 要对调的行信息
		$other_info = $this->get1( '*', array( 'parent_id' => $info[ 'parent_id' ], '`order`' . $compare . $current_order ),
			'`order`' . $compare_direct
		);

		if( !isset( $other_info[ 'id' ] ) )
		{
			return false;
		}

		$this->set( array( 'id' => $id, 'order' => $other_info[ 'order' ] ) );
		$this->set( array( 'id' => $other_info[ 'id' ], 'order' => $current_order ) );

		return true;
	}
}
?>