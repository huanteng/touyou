<?php
require_once dirname( __FILE__ ) . '/base.php';

class admin_privilege extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'admin,privilege';
		$this->id = 'admin';
	}
}
?>