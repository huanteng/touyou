<?php
	//数据级缓存非模版页面级缓存，应用在数据字典方面或某些数据列表结果，主要以php数组形式保存，亦兼容纯文本方式
	class file_cache
	{
		var $dir = '';
		var $tag = 'oop';
/*
		function cache( $dir )
		{
			$this->dir = $dir;
		}
*/
		// key 为cache标识符，如果带有|，则会以此符号分开，前面的参数为目录名，最后一个参数为标识符。
		// 标识符经过加密处理后，作为文件名命名
		function save( $key, $data, $php = true )
		{
			$file = $this->name( $key );
			$content = $php ? '<?php $__cache__ = ' . var_export( $data, true ) . ' ?>' : $data;
			return file_put_contents( $file, $content );
		}

		function load( $key, $php = true )
		{
			$file = $this->name( $key );

			if( !is_file( $file ) )
			{
				return false;
			}

			if ( $php )
			{
				require $file;
				return $__cache__;
			}
			else
			{
				return file_get_contents( $file );
			}
		}

		function exist( $key, $expire = 0 )
		{
			$file = $this->name( $key );
			return is_file( $file ) && ( $expire < 1 || ( time() - filemtime( $file ) <= $expire ) );
		}

		function clear( $key )
		{
			$file = $this->name( $key );
			return is_file( $file ) && unlink( $file );
		}

		function name( $key ) {
			$keys = explode( "|", $key );
			$dir = config( 'cache.base' );
			
			$count_keys = count($keys);
			if( $count_keys == 1) {
				$dir .= 'data/';
			} else {
				for( $i=0;$i<$count_keys-1;++$i)
					$dir .= $keys[$i] . '/';
				$key = $keys[$count_keys-1];
			}
			
			$key = md5( $key . $this->tag );
			$dir .= $key[0] . DIRECTORY_SEPARATOR . $key[15] . DIRECTORY_SEPARATOR . $key[31];
			if ( ! is_dir( $dir ) ) mkdir( $dir, 0777, true );
			return $dir . DIRECTORY_SEPARATOR . $key . '.php';
		}
	}
?>