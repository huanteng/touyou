<?php
class time
{
	// <editor-fold defaultstate="collapsed" desc="in，判断是否处于两个时间中">
	/* 参数：
	 *	s_start_time：开始时间，字符串形式，如 '2012-11-10 0:00:01'
	 *	s_end_time：结束时间，字符串形式
	 * 返回值：true|false
	 */
	function in( $s_start_time, $s_end_time )
	{
		$now = time();

		return $now >= strtotime( $s_start_time ) && $now < strtotime( $s_end_time );
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="before，检测当前时间是否早于参数">
	/* 参数：
	 *	time：时间，字符串形式，如 '2012-11-10 0:00:01'
	 * 返回值：true|false
	 */
	function before( $time )
	{
		return time() <= strtotime( $time );
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="after，检测当前时间是否晚于参数">
	/* 参数：
	 *	time：时间，字符串形式，如 '2012-11-10 0:00:01'
	 * 返回值：true|false
	 */
	function after( $time )
	{
		return time() >= strtotime( $time );
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="format，格式化时间">
	function format( $timestamp )
	{
		$today = strtotime( date( 'Y-m-d' ) );
		if ( $timestamp >= $today && $timestamp <= $today + 86400 )
		{
			return date( 'H:i', $timestamp );
		}
		else if ( $timestamp >= ( $today - 86400 ) && $timestamp < $today )
		{
			return date( '昨天 H:i', $timestamp );
		}
		else
		{
			return date( 'm-d H:i', $timestamp );
		}
	}
	// </editor-fold>

	/* 返回当天0：00的时间值
	 *
	 */
	function today()
	{
		return strtotime(date('Y-m-d 0:00:00',time()));
	}

	/* 返回第二天0:00的时间值
	 *
	 */
	function tomorrow()
	{
		return $this->today() + 86400;
	}
}
?>