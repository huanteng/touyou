<?php
class cache
{
	var $memcache = null;
	var $debug = false;

	function __construct()
	{
		return;
		$this->memcache = new Memcache;
		//$this->memcache->addServer( '127.0.0.1', 11211 );
		$this->memcache->addServer( '10.211.55.4', 11211 );
		register_shutdown_function( array( & $this, 'disconnect' ) );
	}

	function set( $key, $data, $expire = 900, $zip = 0 )
	{
		$cache = load( 'file_cache' );
		$cache->save( $key, $data );
		
		return;
		if( $this->debug )
		{
			biz('base')->debug( "set $key = " . print_r( $data, true ) . " , $expire" );
		}
		return $this->memcache->set( $key, $data, $zip, $expire );
	}

	function get( $key, $default = FALSE )
	{
		$cache = load( 'file_cache' );
		return $cache->load( $key );
		
		$data = $this->memcache->get( $key );
		if( !$data ) $data = $default;

		if( $this->debug )
		{
			biz('base')->debug( "get $key = " . print_r( $data, true ) );
		}

		return $data;
	}

	function del( $key )
	{
		$cache = load( 'file_cache' );
		return $cache->clear( $key );
		
		return;
		if( $this->debug )
		{
			biz('base')->debug( 'del ' . $key );
		}
		return $this->memcache->delete( $key );
	}

	function clear()
	{
		return;
		return $this->memcache->flush();
	}

	function disconnect()
	{
		return;
		$this->memcache->close();
	}
}
?>