<?php
	$_index_ = __FILE__;

	//make sure config only once
	if ( ! isset( $GLOBALS[$_index_] ) )
	{
		$GLOBALS[$_index_] = array();
		$GLOBALS[$_index_]['database'] = array();
		$GLOBALS[$_index_]['template'] = array();
		$GLOBALS[$_index_]['domain'] = array();
		$GLOBALS[$_index_]['header'] = array();
		$GLOBALS[$_index_]['dir'] = array();
		$GLOBALS[$_index_]['cache'] = array();

		$GLOBALS[$_index_]['host'] = isset( $_SERVER['HTTP_HOST'] ) ? $_SERVER['HTTP_HOST'] : '';
		$GLOBALS[$_index_]['key'] = 'c]5';
		$GLOBALS[$_index_]['develop'] = false;

		$GLOBALS[$_index_]['frontend'] = 'http://saiyou.mobi/frontend/';
		$GLOBALS[$_index_]['interface'] = 'http://saiyou.mobi/interface/';
		$GLOBALS[$_index_]['logo_base_dir'] = '/data/web/dice/frontend/logo/';

		//database config
		$GLOBALS[$_index_]['database']['host'] = '127.0.0.1';
		$GLOBALS[$_index_]['database']['name'] = 'iron';
		$GLOBALS[$_index_]['database']['user'] = 'iron';
		$GLOBALS[$_index_]['database']['pass'] = 'iron';

		//get patch info
		$data['base'] = dirname( __FILE__ ) . '/';
		$data['path'] = pathinfo( $data['base'] );
		$data['directory'] = isset( $_SERVER['SCRIPT_NAME'] ) ? dirname( $_SERVER['SCRIPT_NAME'] ) . '/' : $data['base'];

		//domain config
		if ( $GLOBALS[$_index_]['develop'] )
		{
			$GLOBALS[$_index_]['domain']['root'] = $GLOBALS[$_index_]['host'];
			$GLOBALS[$_index_]['domain']['www'] = 'http://' . $GLOBALS[$_index_]['domain']['root'] . '/';
		}
		else
		{
			$temp = explode( '.', strtolower( $GLOBALS[$_index_]['host'] ) );
			krsort( $temp );$new = array();foreach( $temp as $value ) $new[] = $value;
			$domain = $new[1] == 'com' || $new[1] == 'gov' || $new[1] == 'net' || $new[1] == 'co' || $new[1] == 'me' || $new[1] == 'idv' ? $new[2] . '.' . $new[1] . '.' . $new[0] : $new[1] . '.' . $new[0];

			$GLOBALS[$_index_]['domain']['root'] = '.' . $domain;
			$GLOBALS[$_index_]['domain']['www'] = 'http://www' . $GLOBALS[$_index_]['domain']['root'] . '/';
		}

		//base config
		$GLOBALS[$_index_]['library_dir'] = $data['base'];
		$GLOBALS[$_index_]['project_dir'] = $data['path'];
		$GLOBALS[$_index_]['directory'] = $data['directory'];
		$GLOBALS[$_index_]['server'] = & $_SERVER;
		$GLOBALS[$_index_]['cookie'] = & $_COOKIE;

		if ( isset( $GLOBALS[$_index_]['server']['HTTP_HOST'] ) )
		{
			$GLOBALS[$_index_]['host'] = 'http://' . $GLOBALS[$_index_]['server']['HTTP_HOST'];
			$GLOBALS[$_index_]['current_url'] = $GLOBALS[$_index_]['host'] . $GLOBALS[$_index_]['server']['REQUEST_URI'];
			$GLOBALS[$_index_]['current_directory_url'] = $GLOBALS[$_index_]['host'] . $GLOBALS[$_index_]['directory'];
		}

		//dir config
		$GLOBALS[$_index_]['dir']['root'] = $GLOBALS[$_index_]['project_dir']['dirname'] . '/';
		$GLOBALS[$_index_]['dir']['www'] = $GLOBALS[$_index_]['project_dir']['dirname'] . '/';
		$GLOBALS[$_index_]['dir']['template'] = $GLOBALS[$_index_]['project_dir']['dirname'] . '/template/';

		//cache config
		$GLOBALS[$_index_]['cache']['base'] = $GLOBALS[$_index_]['project_dir']['dirname'] . '/cache/';
		$GLOBALS[$_index_]['cache']['data'] = $GLOBALS[$_index_]['cache']['base'] . 'data/';
		$GLOBALS[$_index_]['cache']['template'] = $GLOBALS[$_index_]['cache']['base'] . 'template/';

		//template config
		$GLOBALS[$_index_]['template']['img'] = $GLOBALS[$_index_]['domain']['www'] . 'template/';
		$GLOBALS[$_index_]['template']['js'] = $GLOBALS[$_index_]['domain']['www'] . 'template/';
		$GLOBALS[$_index_]['template']['css'] = $GLOBALS[$_index_]['domain']['www'] . 'template/';

		function biz( $name )
		{
			return load( 'biz.' . $name );
		}

		//config function return var
		function config( $name = '' )
		{
			$_index_ = __FILE__;

			if ( $name == '' )
			{
				$result = $GLOBALS[$_index_];
			}
			else
			{
				$temp = explode( '.', $name );
				$size = count( $temp );

				if ( $size == 1 )
				{
					$result = isset( $GLOBALS[$_index_][$name] ) ? $GLOBALS[$_index_][$name] : '';
				}
				else if ( $size == 2 )
				{
					$result = isset( $GLOBALS[$_index_][$temp[0]][$temp[1]] ) ? $GLOBALS[$_index_][$temp[0]][$temp[1]] : 0;
				}
				else if ( $size == 3 )
				{
					$result = isset( $GLOBALS[$_index_][$temp[0]][$temp[1]][$temp[2]] ) ? $GLOBALS[$_index_][$temp[0]][$temp[1]][$temp[2]] : 0;
				}
				else if ( $size == 4 )
				{
					$result = isset( $GLOBALS[$_index_][$temp[0]][$temp[1]][$temp[2]][$temp[3]] ) ? $GLOBALS[$_index_][$temp[0]][$temp[1]][$temp[2]][$temp[3]] : 0;
				}
				else
				{
					trigger_error( 'config function argument wrong.', E_USER_ERROR );
				}
			}

			return $result;
		}

		function load( $class, $config = array(), $unique = true )
		{
			static $object = array();

			if ( ! isset( $object[$class] ) || ! $unique )
			{
				require_once config( 'library_dir' ) . str_replace( '.', '/', $class ) . '.php';
				$name = explode( '.', $class );
				$size = count( $name );
				$name = $size > 0 ? $name[$size-1] : $name;

				if ( $name == 'template' )
				{
					$style = isset( $config['style'] ) ? $config['style'] : 1;
					$dir = isset( $config['dir'] ) ? $config['dir'] : '';

					$template_config = config();

					if ( $dir != '' )
					{
						$template_config['dir']['template'] = $dir;
						$template_config['template']['img'] = $dir;
						$template_config['template']['js'] = $dir;
						$template_config['template']['css'] = $dir;
					}

					$template_config['dir']['template'] .= $style . '/';
					$template_config['template']['img'] .= $style . '/images/';
					$template_config['template']['js'] .= $style . '/javascript/';
					$template_config['template']['css'] .= $style . '/';
					$temp = new $name( $template_config['dir']['template'], $template_config['cache']['template'] . $style . '/' );
					$temp->auto = array( '_header' => $template_config['header'], '_template' => $template_config['template'], '_dir' => $template_config['dir'], '_domain' => $template_config['domain'] );
				}
				else if ( $name == 'database' )
				{
					$temp = new $name( config( 'database.host' ), config( 'database.name' ), config( 'database.user' ), config( 'database.pass' ) );
				}
				else if ( $name == 'cache' )
				{
					$temp = new $name( config( 'cache.data' ) );
				}
				else if ( $name == "cookie" )
				{
					$temp = new $name( config( 'cookie' ), config( 'key' ), config( 'domain.root' ) );
				}
				else
				{
					$size = count( $config );
					$count = 1;
					foreach( $config as $value ) ${'arg_'.$count++} = $value;

					if ( $size == 0 )
					{
						$temp = new $name();
					}
					else if ( $size == 1 )
					{
						$temp = new $name( $arg_1 );
					}
					else if ( $size == 2 )
					{
						$temp = new $name( $arg_1, $arg_2 );
					}
					else if ( $size == 3 )
					{
						$temp = new $name( $arg_1, $arg_2, $arg_3 );
					}
					else if ( $size == 4 )
					{
						$temp = new $name( $arg_1, $arg_2, $arg_3, $arg_4 );
					}
					else if ( $size == 5 )
					{
						$temp = new $name( $arg_1, $arg_2, $arg_3, $arg_4, $arg_5 );
					}
					else if ( $size == 6 )
					{
						$temp = new $name( $arg_1, $arg_2, $arg_3, $arg_4, $arg_5, $arg_6 );
					}
					else
					{
						trigger_error( 'load function miss ' . $class . ' construct argument.', E_USER_ERROR );
					}
				}

				if ( $name != 'template' ) $object[$class] = & $temp;
			}
			else
			{
				$temp = $object[$class];
			}

			if ( ! is_object( $temp ) ) trigger_error( 'can not load' . $class . ' object.', E_USER_ERROR );
			return $temp;
		}

		function url( $data )
		{
		}
	}
?>