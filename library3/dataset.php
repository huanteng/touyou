<?php
// 记录集（二维数组）对象
class dataset
{
	/** 对记录集里面的时间字段作格式化
	 * @param $source：源记录集
	 * @param $field：时间字段
	 * 	方式一：只改单个字段，可以只传入单个字段的字符串，如 format_time( $source, 'time' )
	 * 	方式二：改多个字段，则传入字段集，形式为：旧字段 => 新字段（如不变可略）
	 * 		如：format_time( $source, array( 'time1' => 'time1A', 'time2' ) )
	 * @return new dataset
	 */
	function format_time( $source, $field )
	{
		if( !is_array( $field ) )
		{
			$field = array( $field );
		}

		$time = load( 'time' );

		$out = array();
		foreach( $source as $k => $v )
		{
			foreach( $field as $k1 => $v1 )
			{
				if( is_numeric( $k1 ) ) $k1 = $v1;
				$v[ $v1 ] = $time->format( $v[ $k1 ] );
			}

			$out[ $k ] = $v;
		}

		return $out;
	}

	/** 给记录集中填充名字字段
	 * @param $source：源记录集
	 * @param $user：从哪个对象中取名字字段？例如： biz('user')。本函数自动用到这个对象的get_name_from_id方法
	 * @param $id_field：id字段名，默认为id
	 * @param $name_field：添加的字段名，默认为name
	 * @return mixed
	 */
	function fill_name( $source, $user, $id_field = 'id', $name_field = 'name' )
	{
		$out = array();
		foreach( $source as $k => $v )
		{
			$v[ $name_field ] = $user->get_name_from_id( $v[ $id_field ] );
			$out[ $k ] = $v;
		}
		return $out;
	}

	/** 改变$source表现形式，改为使用id字段为key
	 * @param $source：源
	 * @param $id：id字段，默认为id
	 * @param $value：每行value要列的字段名，一般为单个字段名（如name）或空。如果为空，则以整行作为value
	 * @return 新记录集，以id为key
	 */
	function key_by_id( $source, $id = 'id', $value = '' )
	{
		$out = array();
		foreach( $source as $v )
		{
			$key = $v[ $id ];
			if( $value != '' )
			{
				$v = $v[ $value ];
			}
			$out[ $key ] = $v;
		}

		return $out;
	}

	/** 截断字段长度，便于列表显示（如截断时，后追加 ...）
	 * @param $source：源
	 * @param array $len：字段说明，为 field => length串
	 * @return mixed：新记录集
	 */
	function cut( $source, $len = array() )
	{
		foreach( $source as &$v )
		{
			foreach( $len as $k1 => $v1 )
			{
				if( strlen( $v[ $k1 ] ) > $v1 )
				{
					$v[ $k1 ] = mb_substr( $v[ $k1 ], 0, $v1 - 3,'utf-8' ) . '...';
				}
			}
		}

		return $source;
	}
}
?>