<?php require 'head.php'; ?>
<div id="loginWindow" class="mini-window" title="用户登录" style="width:350px;height:165px;" 
   showModal="true" showCloseButton="false"
    >

    <div id="loginForm" style="padding:15px;padding-top:10px;">
        <table >
            <tr>
                <td style="width:60px;"><label for="name$text">帐号：</label></td>
                <td>
                    <input id="name" name="name" class="mini-textbox" required="true" style="width:150px;"/>
                </td>    
            </tr>
            <tr>
                <td style="width:60px;"><label for="pass$text">密码：</label></td>
                <td>
                    <input id="pass" name="pass" onvalidation="onPwdValidation" class="mini-password" requiredErrorText="密码不能为空" required="true" style="width:150px;" onenter="onLoginClick"/>
                    &nbsp;&nbsp;<a href="#" >忘记密码?</a>
                </td>
            </tr>            
            <tr>
                <td></td>
                <td style="padding-top:5px;">
                    <a onclick="onLoginClick" class="mini-button" style="width:60px;">登录</a>
                    <a onclick="onResetClick" class="mini-button" style="width:60px;">重置</a>
                </td>
            </tr>
        </table>
    </div>

</div>

    
<script type="text/javascript">
mini.parse();

var loginWindow = mini.get("loginWindow");
loginWindow.show();

//window.onresize = function () {
//    loginWindow.show();
//}

function onLoginClick(e) {
    var form = new mini.Form("#loginWindow");

    form.validate();
    if (form.isValid() == false) return;

    //loginWindow.hide();
    mini.loading("正在登录，请稍候...", "正在登录");
    
    //提交数据
    var data = form.getData();
    var value = mini.encode(data);
    
    post( "login", "login", value, function (data) {
			if( data.code > 0 )
			{
				location = 'frame.php';
			}
			else
			{
				alert(data.message);
				location=location;
			}
        },function (jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			}
			);
}
function onResetClick(e) {
    var form = new mini.Form("#loginWindow");
    form.clear();
}

/////////////////////////////////////
function onUserNameValidation(e) {
    if (e.isValid) {
        if (isEmail(e.value) == false) {
            e.errorText = "必须输入邮件地址";
            e.isValid = false;
        }
    }
}
function onPwdValidation(e) {
    if (e.isValid) {
        /*
        if (e.value.length < 2) {
            e.errorText = "密码不能少于2个字符";
            e.isValid = false;
        }
        */
    }
}
var t = mini.get("name");
t.focus ()	;
</script>

<?php require 'bottom.php'; ?>