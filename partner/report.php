<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php// check_privilege(94); ?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="0" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div field="date" width="100" headerAlign="center" allowSort="true">日期</div>
            <div field="reg_count" width="100" headerAlign="center" allowSort="true">注册用户</div>
            <div field="user_count" width="100" headerAlign="center" allowSort="true">充值人数</div>
			<div field="pay_count" width="100" headerAlign="center" allowSort="true">充值总数</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'report';}

$('#toolbar .mini-button:lt(3),#toolbar .separator,#search').hide();
</script>