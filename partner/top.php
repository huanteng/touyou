<?php require 'head.php'; ?>
<?php check_privilege(0); ?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.url = 'data/' + module() +'.php?method=search';
	var data={};
	<?php
	$s = '';
	foreach( $_GET as $k => $v )
	{
		$s .= "data.{$k} = '{$v}';\n";
	}
	echo $s;
	?>
	grid.load(data);
	
	grid.on( "drawcell", onDrawcell );
}

function search() {
    var q = mini.get("q").getValue();
    grid.load({ q: q });
}
function onKeyEnter(e) {
    search();
}

// 按需覆盖
function onDrawcell (e) {
	/*var record = e.record,
		column = e.column,
		field = e.field,
		value = e.value;

	var html = "";
		switch( field )
		{
			case "user":
				html = '<img src="'+record.logo+'" style="width:40px;height:40px;"/>&nbsp;&nbsp;<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail' + record.id+'", text:"'+record.name+'", url: "user_detail.php?uid='+record.id+'"})\'>'+record.name+'</a>';
				break;
	    }
		if( html != "" ) e.cellHtml = html;
	*/
}

function add() {
	// 此函数最好在具体实现中覆盖，以实现默认值
    var newRow = {};
    grid.addRow(newRow, 0);
}
function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}
function truncate() {
    grid.loading("清空中，请稍候...");
	post( module(), "truncate", '', function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-add" onclick="add()" plain="true">增加</a>
                        <a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
                        <input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:150px;" onenter="onKeyEnter"/>   
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>
