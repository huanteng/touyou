<?php
// 大体抄自backend
function check_privilege( $function_id )
{
	// 不想因cookie而调用复杂的MVC，如果在MVC修改了key和cookie参数，必须在此同步
	require_once dirname( __FILE__ ) . '/../library/cookie.php';
	$cookie = new cookie( $_COOKIE, 'c]5', 'http://' . $_SERVER['HTTP_HOST']);
	
	$account = $cookie->get( 'account_name', true );
	if( $function_id == 0 ) 
	{
		if ( $account == '' ) die( '未登录。' );
		return;
	}
	
	$privilege = unserialize($cookie->get( 'privilege',true));

	if( !isset( $privilege[ $function_id ] ) ) die( "用户名：$account ，功能号：$function_id ，结果：权限不足。");
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>骰友管理后台</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" /><link href="css/css.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
    body
    {
        width:100%;height:100%;margin:0;
    }
//    body{
//        margin:0;padding:0;border:0;width:100%;height:100%;overflow:hidden;
//    }    
//    .header
//    {
//        background:url(../header.gif) repeat-x 0 -1px;
//    }
    </style>
    <script src="js/boot.js" type="text/javascript"></script>
    <script src="js/common.js" type="text/javascript"></script>
    
</head>
<body >
