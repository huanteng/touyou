<?php
require '../../library/controller.php';

class backend extends controller
{
	var $debug = true;
	var $before = array( 'get' => array( 'no_html_cache' => true, 'check_account' => false ), 'post' => array( 'check_account' => false ) );
//	var $account = '';
	var $db = '';
	
	function table() {
		// 需重载
		return 'tablename';
	}

	function id() {
		// 按需重载
		return 'id';
	}

	// 20121127新增，有可能导致后台部分功能出错
	function format( $value )
	{
		return $value;
	}

	function check_account()
	{
		$result = true;
		$cookie = load( 'cookie' );
//			$this->account = $cookie->get( 'account', true );
//
//			if ( ! is_numeric( $this->account ) )
//			{
//				$result = false;
////
////				$template = load( 'template', array( 'dir' => 'template/' ) );
////				echo $template->parse( 'login.php' );
//			}

		return $result;
	}

//		function pagebar( $data, $style = 1 )
//		{
//			$template = load( 'template', array( 'style' => $style, 'dir' => 'template/' ) );
//			$template->assign( 'bar', $data );
//			return $template->parse( 'pagebar.php' );
//		}
//
		function no_html_cache()
		{
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0', false );
			header('Pragma: no-cache');

			return true;
		}

	//检查 $name是否存在，如存在则返回，否则返回$default
	function value( $data, $key, $default = '' )
	{
		return isset( $data[$key] ) ? $data[$key] : $default;
	}
	
	//检查 $name是否存在并且是数字，如存在则返回，否则返回$default
	function number( $data, $key, $default = '' )
	{
		return isset( $data[$key] ) && is_numeric( $data[$key] ) ? $data[$key] : $default;
	}
	
	function out( $json )
	{
		if( strpos( $_SERVER['REQUEST_URI'], 'interface_log.php' ) === false && strpos( $_SERVER['REQUEST_URI'], 'login.php' ) === false )
		{
			include '../../log.conf.php';
			if ( isset( $log['back'] ) )
			{
				$is_log = false;
				$log = $log['back'];

				$uid = 0;
				if( isset( $log['key']['uid'] ) )
				{
					// 2do，完成当前帐号的判断
				}
				else
				{
					$is_log = true;
				}

				if( $is_log && isset( $log['key']['url'] ) )
				{
					$is_log = $this->log_check_in( $log['key']['url'], $_SERVER['REQUEST_URI'] );
				}
				if( $is_log && isset( $log['key']['input'] ) )
				{
					$is_log = $this->log_check_in( $log['key']['input'], var_export( $_REQUEST, true ) );
				}
				if( $is_log && isset( $log['key']['response'] ) )
				{
					$is_log = $this->log_check_in( $log['key']['response'], json_encode( $data ) );
				}
				if( $is_log && isset( $log['key']['sql'] ) )
				{
					$is_log = $this->log_check_in( $log['key']['sql'], var_export( $this->db->sql, true ) );
				}

				if ( $is_log )
				{
					$data = array( 'uid' => $uid, 'url' => $_SERVER['REQUEST_URI'] );
					if(  isset( $log['save']['input'] ) ) $data['input'] = var_export( $_REQUEST, true );
					if(  isset( $log['save']['response'] ) ) $data['response'] = json_encode( $data );
					if(  isset( $log['save']['sql'] ) ) $data['sql'] = var_export( $this->db->sql, true );

					$log = load( 'biz.interface_log' );
					$log->add( $data );
				}
			}
		}
		
		echo json_encode( $json );
	}
	
	function do_get()
	{
		$function = $this->value( $this->input, 'method', 'search' );
		$this->db = load( 'database' );

		if ( $function != 'do_post' && method_exists( $this, $function ) )
		{
			unset( $this->input['method'] );
			$result = $this->$function( $this->input );
		}
		else
		{
			$result = array( 'code' => -999, 'data' => 'unknown method' );
		}

		$this->out( $result );
	}
	
	function do_post()
	{
		// 2do，注意，这里没做防注入检测，隐含漏洞
		$this->input = array_merge( $_POST , $_GET );
			
		$this->do_get();
	}
	
	// 搜索表格，返回记录集，常用于列表数据：
	// $data：数据组
	// $field：要查询记录集，如*
	// $table：要查询的数据表，如user
	// $equal：$data中，使用＝作为比较符的字段名列表，分别和form及数据表的字段对应
	// $like：同理，只是和like作为比较符
	// $q：q关键字要检查的字段名列表，均使用like为比较符，字段间用or作为处理
	// $join：条件间关系
	// $data中特殊参数：pageSize, pageIndex, sortField, sortOrder, q
	// $other: 其它条件,如 time > 12323423
	function search( $data, $field, $table, $equal = array(), $like = array(), $q = array(), $join = 'and', $other = '' )
	{
		$where = array();
		foreach ( $equal as $column ) {
			if ( $this->value( $data, $column ) != '' )
			//$where[] = "$column = '$data[$column]'";
			$where[$column] = $data[$column];	
		}

		foreach ( $like as $column ) {
			if ( $this->value( $data, $column ) != '' )
			$where[] = "$column like '%" . $data[$column] . "%'";
		}
		if ( $this->value($data, 'q') != '' ){
			$s = '';
			foreach( $q as $column ) $s .= "( $column like '%" . $data['q'] . "%' ) or ";
			$where[] = substr( $s, 0, -4 );
		}
		if( $other != '' ){
			$where[] = $other;
		}
		
		$where = $this->db->term( $where, $join );
		
		if( $this->value( $data, 'sortField', '') != '' )
		{
			$orderby = $data['sortField'];
			if( isset( $data['sortOrder'] ) ) $orderby .= ' ' . $data['sortOrder'];
		}
		
		if( isset( $data['pageIndex'] ) && isset( $data['pageSize'] ) )
		{
			$limit = ( $data['pageIndex'] * $data['pageSize'] ) . ', ' . $data['pageSize'];
		}

		$sql = "select count(1) as total from $table";
		if( $where != '' ) $sql .= ' where ' . $where;
		$data = $this->db->unique( $sql );
		$result['total'] = $data['total'];
		
		if( $result['total'] == 0 )
		{
			$result['data'] = array();
		}
		else
		{
			$sql = "select $field from $table";
			if( $where != '' ) $sql .= ' where ' . $where;
			if( isset( $orderby ) ) $sql .= ' order by ' . $orderby;
			if( isset( $limit ) ) $sql .= ' limit ' . $limit;
			
			$result['data'] = $this->db->select( $sql );
		}
		
		return $result;
	}

	function php_json_decode($str){
		// 本句为dda所加，不知为何需要调用多一次才正常,20121004
		// 20121127取消一行，证实这是因为mvc::format函数引起。
		// 即日起在后台框重载format函数，正式取消该功能过滤功能，改在base实现防注入（以后有可能改在database中）
		//$str = stripslashes($str);
		// 201211271448，不知为何，因magic双引号问题，连这个也要取消作用，否则回车符会变为n
		// $stripStr = stripslashes($str);
		$stripStr = $str;
		$json = json_decode($stripStr,true);
		return $json;
	}
	
	function save( $data )
	{
		$rows = $this->php_json_decode( $data['data'] );
       
		$file = 'biz.' . $this->table();
		$obj = load( $file );
		
		$field = array_flip( $obj->field() );
		$id = $field[ 'id' ];

		foreach ($rows as $row)
		{
		    $state = $this->value( $row, '_state', '' );
			if($state == "added" || $id == ""){ //新增：id为空，或_state为added
				$obj->add( $this->row( $row ) );
			}
			else if ($state == "removed" || $state == "deleted")
		    {
				$obj->del($row[$id]);
		    }
			else if ($state == "modified" || $state == "")  //更新：_state为空或modified
		    {
				$obj->set( $this->row( $row ) );
		    }
		}
		return array( 'code' => 1, 'message' => '成功操作' );
	}

	/* 本函数仅用于save方法。
	 * 有时要对每行的数据做修改或增加，这时需要重载本函数实现需求
	 * 如需更复杂的处理，需要重载整个save方法
	 * 返回值：经处理后的数组
	 */
	function row( $data )
	{
		return $data;
	}

	// 将$data中，完善用户名字段的值，来源为$data['uid'],如果用户名不存在，则采用默认值
	// 调用方法： $new_data = fill_name_from_uid( $old_data, 'uid', 'name', '' )
	function fill_name_from_uid( $data, $uid, $name, $default = '' )
	{
		$result = array();
		foreach( $data as $k => $v )
		{
			if( $v[$uid] == 0 )
			{
				$v[$name] = $default;
			}
			else
			{
				$sql = "select name from user where id = $v[$uid]";
				$user = $this->db->unique( $sql );
				$v[$name] = isset( $user['name'] ) ?  $user['name'] : $default;
			}
			$result[ $k ] = $v;
		}
		return $result;
	}
	
	function fill_user( $data, $uid, $field=array('field'=>'rename') )
	{
		$result = array();
		foreach( $data as $k => $v )
		{
			$f = array();
			foreach($field as $key => $value)
			{
				$f[] = '`'.$key.'`';
			}
			$field_text = join(',',$f);
			
			$sql = "select $field_text from user where id = $v[$uid]";

			$user = $this->db->unique( $sql );
			
			foreach($field as $key => $value)
			{
				$name = ($value=='') ? $key : $value;
				$v[$name] = isset( $user[$key] ) ?  $user[$key] : '';
			}
			
			$result[ $k ] = $v;
		}
		return $result;
	}
	
    function fill_field( $data, $id, $name='', $value )
    {
        foreach ($data as $key=>$row)
        {
            $field = ($name == '') ? $id : $name;
            if( isset($row[$id]) )
            {
                $data[$key][$field] = isset($value[$row[$id]]) ? $value[$row[$id]] : '';
            }
        }
        return $data;
    }
	
	/*
	 * 用户的道具数量；用于查询列表。
	 * 当需要添加如：用户余额，骰友币余额等列 时使用
	 * 默认是金币
	 */
	function fill_props( $data, $uid='user', $field=array(1=>'gold_rename'), $default=0 )
	{
		$db = $this->db;
		$props_id = implode( ',', array_keys( $field ) );
		foreach ($data as $key=>$row)
        {
            $result = $db->get('props,quantity', 'user_props', array( 'user' => $row[$uid], '0' => "props in($props_id)" ));
			foreach( $result as $p )
			{
				$row[ $field[ $p['props'] ] ] = isset($p['quantity']) ? $p['quantity'] : $default;
			}
			$data[$key] = $row;
        }
		return $data;
	}

    // 将$data中时间字段格式化，
	// 参数：
	//   $data：记录集
	//   $time：时间字段
	//   $format：格式化，如：date，或time，或具体的 Y-m-d H:i:s
	function format_datetime( $data, $time, $format = 'm-d H:i' )
	{
		if( $format == 'date' ) $format = 'Y-m-d';
		if( $format == 'time' ) $format = 'H:i:s';
		if( $format == 'datetime' ) $format = 'Y-m-d H:i:s';
		if( $format == 'normal' ) $format = 'm-d H:i';
		
		foreach( $data as $k => $v )
		{
			$data[$k][$time] = $data[$k][$time] > 0 ? date( $format, $data[$k][$time] ) : '';
		}
		return $data;
	}
	
	// 检查当前身份，是否拥有该功能权限，当前身份来自cookie
	// 参数：$function_id：功能id，对应于 admin_function.id；0代表仅判断是否登录
	// 返回值：无
	function check_privilege( $privilege )
	{
		$cookie = load('cookie');
		
		$have = array();
		$temp = $cookie->get( 'privilege',true);
		if( $temp ) $have = unserialize( $temp );
		
		$privilege = explode( ',', $privilege );
		
		$result = array();
		foreach( $privilege as $p )
		{
			if( $p == 0 )
			{
				if( $cookie->get( 'privilege' ) == '' )
				{
					$this->out( array( 'code' => '-1000', 'message' => '未登录' ) );
					die;
				}
			}
			else
			{
				if( !isset( $have[ $p ] ) )
				{
					$this->out( array( 'code' => '-1001', 'message' => '无权限' ) );
					die;
				}
			}
		}
	}

	// 主要用户log场景。$keys中设定多个过滤条件（用逗号分开），逐一检查是否处于$content中
	function log_check_in( $keys, $content )
	{
		if( $keys == '' ) return true;
		foreach( explode( ',', $keys ) as $key )
		{
			if( strpos( $content, $key ) !== false )
			{
				return true;
			}
		}
		return false;
	}

}

?>