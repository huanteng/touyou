<?php
require 'backend.php';

class action extends backend
{
	function login( $data )
	{
		$data = $this->php_json_decode( $data['data'] );
		$result = array( 'code' => -1, 'message' => '发生错误' );

		$user = load( 'biz.partner' );
		$info = $user->get_by_name( $data['name'] );

		if ( !isset( $info['name'] ) )
			return array( 'code' => -2, 'message' => '用户不存在' );
		
		if ( $info['status'] != 0 )
			return array( 'code' => -3, 'message' => '用户被暂停' );

		if ( $info['pass'] != $user->password( $data['pass'] ) )
			return array( 'code' => -4, 'message' => '密码不正确' );

		$user->db->command( 'update partner set last = ' . time() . ' where id = ' . $info['id'] );

		$cookie = load( 'cookie' );
		$cookie->set( 'account', $info['id'], true );
		$cookie->set( 'account_name', $info['name'], true );
		$cookie->set( 'account_key', $info['key'], true );
		
		$result = array( 'code' => $info['id'], 'message' => '欢迎进入系统' );
		
		return $result;
	}
}

$action = new action();
$action->run();
?>