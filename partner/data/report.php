<?php

require 'backend.php';

class action extends backend
{

	function search($in)
	{
		$cookie = load('cookie');
		$account_key = $cookie->get('account_key', true);
		
		$date = $this->value($in, 'date', '');
		$time = ($date == '') ? time() : strtotime($date);

		for ($day = date('d', $time); $day >= 1; $day--) {
			$start_time = strtotime(date('Y-m-' . $day, $time));
			$end_time = $start_time + 86399;

			$reg_count = $this->db->unique('select count(1) as total from user where channel=\'' . $account_key . '\' and register between ' . $start_time . ' and ' . $end_time);
			$pay_count = $this->db->unique('select sum(binary(p.money)) as total from payment p,user u where p.user=u.id and u.channel=\'' . $account_key . '\' and p.`status`=1 and p.`time` between ' . $start_time . ' and ' . $end_time);
			$user_count = $this->db->unique('select count(distinct p.user) as total from payment p,user u where p.user=u.id and u.channel=\'' . $account_key . '\' and p.`status`=1 and p.`time` between ' . $start_time . ' and ' . $end_time);

			$reg_count = isset($reg_count['total']) ? $reg_count['total'] : 0;
			$pay_count = isset($pay_count['total']) ? round($pay_count['total'], 2) : 0;
			$user_count = isset($user_count['total']) ? $user_count['total'] : 0;
			$result['data'][] = array('date' => date('Y-m-' . $day, $time), 'reg_count' => $reg_count, 'pay_count' => $pay_count, 'user_count' => $user_count);
		}
		$result['total'] = 0;
		return $result;
	}

	// <editor-fold defaultstate="collapsed" desc="以月统计">
	function month($in)
	{
		$cookie = load('cookie');
		$account_key = $cookie->get('account_key', true);

		$date = $this->value($in, 'date', '');
		$time = ($date == '') ? time() : strtotime($date);

		$temp = $this->db->select('SELECT COUNT(1) AS total, FROM_UNIXTIME(register,\'%Y-%m\') AS month FROM `user` where channel=\'' . $account_key . '\' and register > ' . strtotime(date('Y-01-01'), $time) . ' GROUP BY month order by register desc');

		foreach ($temp as $value) {
			$start_time = strtotime($value['month']);
			$end_time = $start_time + date('t', $time) * 86400;

			$pay_count = $this->db->unique('select sum(binary(p.money)) as total from payment p,user u where p.user=u.id and u.channel=\'' . $account_key . '\' and p.status=1 and p.time between ' . $start_time . ' and ' . $end_time);
			$user_count = $this->db->unique('select count(distinct p.user) as total from payment p,user u where p.user=u.id and u.channel=\'' . $account_key . '\' and p.status=1 and p.time between ' . $start_time . ' and ' . $end_time);

			$reg_count = isset($value['total']) ? $value['total'] : 0;
			$pay_count = isset($pay_count['total']) ? round($pay_count['total'], 2) : 0;
			$user_count = isset($user_count['total']) ? $user_count['total'] : 0;
			$result['data'][] = array('month' => $value['month'], 'reg_count' => $reg_count, 'pay_count' => $pay_count, 'user_count' => $user_count);
		}
		$result['total'] = 0;
		return $result;
	}
	// </editor-fold>
}

$action = new action();
$action->run();
?>