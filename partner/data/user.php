<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'user';
	}

	function search($data)
	{
		$cookie = load('cookie');
		$account_key = $cookie->get('account_key', true);

		$data['channel'] = $account_key;

		$field = '*';
		$table = $this->table();
		$equal = array('channel');
		$like = array();
		$q = array('name');
		$other = '';

		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}

		$result = parent::search($data, $field, $table, $equal, $like, $q, 'and', $other);
		$result['data'] = $this->format_datetime($result['data'], 'register');
		$result['data'] = $this->format_datetime($result['data'], 'login');
		
		return $result;
	}
}

$action = new action();
$action->run();
?>