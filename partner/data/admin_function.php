<?php
require 'backend.php';

class action extends backend
{
	// 用于左树，只显示有权限的及可视的
	function tree( $data )
	{
		$cookie = load('cookie');
		$account_key = $cookie->get('account_key', true);
		
		$data = array();
		$data[] = array('id'=>1,'name'=>'个人设置','url'=>'','parent_id'=>'0');
	//	$data[] = array('id'=>2,'name'=>'修改密码','url'=>'pass_edit.php','parent_id'=>'1');
		$data[] = array('id'=>3,'name'=>'退出系统','url'=>'logout.php','parent_id'=>'1');
		
		$data[] = array('id'=>4,'name'=>'报表','url'=>'','parent_id'=>'0');
		$data[] = array('id'=>5,'name'=>'日报表','url'=>'report.php','parent_id'=>'4');
		$data[] = array('id'=>6,'name'=>'月报表','url'=>'report_month.php','parent_id'=>'4');
		$data[] = array('id'=>7,'name'=>'用户列表','url'=>'user.php','parent_id'=>'4');
		return $data;
	}
	
}

$action = new action();
$action->run();
?>