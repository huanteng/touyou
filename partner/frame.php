<?php require 'head.php'; ?>

<style type="text/css">
body{
    margin:0;padding:0;border:0;width:100%;height:100%;overflow:hidden;
}    
.header
{
    background:url(images/header.gif) repeat-x 0 -1px;
}
</style>

<!--Layout-->
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
    <?php /* <div class="header" region="north" height="70" showSplit="true" showHeader="false">
        <h1 style="margin:0;padding:15px;cursor:default;font-family:微软雅黑,黑体,宋体;">骰友 管理系统 V2</h1>
        <div style="position:absolute;top:18px;right:10px;">
            <a class="mini-button mini-button-iconTop" iconCls="icon-add" onclick="onQuickClick" plain="true">快捷</a>    
            <a class="mini-button mini-button-iconTop" iconCls="icon-edit" onclick="onClick"  plain="true" >首页</a>        
            <a class="mini-button mini-button-iconTop" iconCls="icon-date" onclick="onClick"  plain="true" >消息</a>        
            <a class="mini-button mini-button-iconTop" iconCls="icon-edit" onclick="onClick"  plain="true" >设置</a>        
            <a class="mini-button mini-button-iconTop" iconCls="icon-close" onclick="onClick"  plain="true" >关闭</a>        
            
        </div>
        
    </div> */?>
    <div title="south" region="south" showSplit="false" showHeader="false" height="30">
        <div style="line-height:28px;text-align:center;cursor:default">Copyright © 广州市欢腾网络科技有限公司版权所有 </div>
    </div>
    <div title="center" region="center" style="border:0;" bodyStyle="overflow:hidden;">
        <!--Splitter-->
        <div class="mini-splitter" style="width:100%;height:100%;" borderStyle="border:0;">
            <div size="180" maxSize="250" minSize="100" showCollapseButton="true" style="border:0;">
                <!--OutlookTree-->
                <div id="leftTree" class="mini-outlooktree" url="data/admin_function.php?method=tree" onnodeselect="onNodeSelect"
                    textField="name" idField="id" parentField="parent_id"  
                >
                </div>
                
            </div>
            <div showCollapseButton="false" style="border:0;">
                <!--Tabs-->
                <div id="mainTabs" class="mini-tabs" activeIndex="0" style="width:100%;height:100%;">
                    
                </div>
            </div>        
        </div>
    </div>
</div>

<script type="text/javascript">
mini.parse();

var tree = mini.get("leftTree");

function showTab(node) {
	if( node.is_new_window == '1' )
	{
		open(node.url);
	}
	else
	{
        var tabs = mini.get("mainTabs");

        var id = "tab$" + node.id;
        var tab = tabs.getTab(id);
        if (!tab) {
            tab = {};
            tab.name = id;
            tab.title = node.text + '<a href="' + node.url + '" target="_blank"><img src="js/miniui/themes/icons/zoomin.gif" border="0" height="13"></a><a href="#" onclick="reload(\'' + id + '\')"><img src="js/miniui/themes/icons/reload.png" border="0" height="13"></a>';
            tab.showCloseButton = true;

            tab.url = node.url;

            tabs.addTab(tab);
        }
        tabs.activeTab(tab);
    }
}

function onNodeSelect(e) {
    var node = e.node;
    var isLeaf = e.isLeaf;

    //if (isLeaf) {
    if (node.url!='') {
        showTab(node);
    }
}

function onClick(e) {
    var text = this.getText();
    alert(text);
}
function onQuickClick(e) {
    tree.expandPath("datagrid");
    tree.selectNode("datagrid");
}
function reload( id )
{
	var tabs = mini.get("mainTabs");
	var tab = tabs.getTab(id);
	tabs.reloadTab(tab);
	return false;
}

showTab({id:'-1', text:'用户列表', url: 'user.php?method=search'});

</script>

<?php require 'bottom.php'; ?>