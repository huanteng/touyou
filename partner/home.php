<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(94); 
require_once dirname( __FILE__ ) . '/../partner/data/backend.php';

$db = $this->db;

$reg_count = $db->unique('select id as total from user order by id desc');
$reg_count_today = $db->unique('select count(1) total from user where register between '.date('Y-m-d').' and '.date('Y-m-d 23:59:59'));
$user_count = $db->unique('select count(distinct user) as total from payment where `status`=1');
$user_count_today = $db->unique('select count(distinct user) as total from payment where `status`=1 and `time` between '.date('Y-m-d').' and '.date('Y-m-d 23:59:59'));
$pay_count = $db->unique('select sum(binary(money)) as total from payment where `status`=1');
$pay_count_today = $db->unique('select sum(binary(money)) as total from payment where `status`=1 and `time` between '.date('Y-m-d').' and '.date('Y-m-d 23:59:59'));

$reg_count = isset($reg_count['total'])?$reg_count['total']:0;
$reg_count_today = isset($reg_count_today['total'])?$reg_count_today['total']:0;
$user_count = isset($user_count['total'])?$user_count['total']:0;
$user_count_today = isset($user_count_today['total'])?$user_count_today['total']:0;
$pay_count = isset($pay_count['total'])?$pay_count['total']:0;
$pay_count_today = isset($pay_count_today['total'])?$pay_count_today['total']:0;
?>

    <div style="width:98%;height:80%;" >
        
		<table>
			<tr><td>注册人数： <?php echo $reg_count;?></td><td>充值人数：<?php echo $user_count;?></td><td>充值总数：<?php echo $pay_count;?></td></tr>
			<tr><td></td><td></td><td></td></tr>
			<tr><td>当天注册人数：<?php echo $reg_count_today;?></td><td>当天充值人数：<?php echo $user_count_today;?></td><td>当天充值总数：<?php echo $pay_count_today;?></td></tr>
		</table>
		
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'report';}

$('#toolbar .mini-button:lt(3),#toolbar .separator,#search').hide();
</script>