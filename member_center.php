<?php
	require 'library/controller.php';

	class action extends controller
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'frontend/template/' ) );
			echo $template->parse( 'member_center.php' );
		}

		function do_post()
		{
			//应付盛付通商务
			if( $this->input['name'] == 'dda' && $this->input['pass'] == '1234' )
			{
				$data = array('id'=>1, 'name'=>'dda');
				echo json_encode( array( 'code' => 1, 'data'=>$data, 'sid'=>md5('dda') ) );
			}
			else
			{
				$this->input['props'] = 2;
				$this->input['types'] = 2;
				$this->input['count'] *= 100;

				$payment = load( 'biz.payment' );
			//	echo json_encode( array( 'no' => $payment->get_no( $this->input ) ) );
				echo json_encode( array( 'no' => date('YmdHis') ) );//应付盛付通商务
			}
		}
	}

	$action = new action();
	$action->run();
?>