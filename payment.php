<?php
require 'frontend.php';

class action extends frontend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'payment';
	}

	/** 创建订单
	 * @param $in
	 * @return bool
	 */
	function home( $in )
	{
		if( $in[ 'money' ] == 0 )
		{
			$in[ 'money' ] = $in[ 'money_custom' ];
		}

		if( !isset( $in[ 'site' ] ) )
		{
			$uid = $this->login_uid();

			if( $uid == 0 )
			{
				die( '未登录' );
			}

			$in[ 'uid' ] = $uid;
		}

		$payment = biz( 'payment' );

		$id = $payment->create( $in );

		$info = $payment->get_from_id( $id );

		//支付方式处理
		$type = 'type_' . $info[ 'type' ];
		if( !method_exists( $this, $type ) )
		{
			$type = 'type_site';
		}

		$out = $this->$type( $info );

		return $out;
	}

	/**创建订单，被同行站调用入口
	 * @param $in
	 */
	function create( $in )
	{
		/* 测试数据
		$in = array( 'site' => '1', 'no' => '2014122921519', 'money' => '1.00', 'name' => '金币', 'sign' => '9bc4cec14dabd22cc13ab11469463cf5' );
		*/

		biz( 'payment' )->log( '', 0, '收到跨站支付请求', array( 'data' => $in ) );

		$this->check_sign( $in );
		
		$in[ 'billno' ] = $in[ 'no' ];

		return $this->home( $in );
	}

	// 内部支付
	function type_site( $in )
	{
		$site = biz('site');

		$site_id = $site->get_id_by_payment_type( $in[ 'type' ] );
		$url = $site->get_url( $site_id, 'payment', 'create', array( 'no' => $in[ 'billno' ],
			'money' => $in[ 'money' ], 'name' => '金币' ), false );
		header( 'location:' . $url );
	}

	// 盛付通
	function type_7( $in )
	{
		$data = biz('shengpay')->post( $in );
		echo $data;
	}

	/** 盛付通页面通知
	 * @param $in
	 */
	function shengpay_page( $in )
	{
		biz('shengpay')->log( '', 0, 'page', array( 'data' => $in ) );

		/* 测试数据
		$in = array( 'TransNo' => '20150110111819363165', 'PaymentNo' => '20150110111837706687', 'OrderAmount' => '0.01','TransTime'=>'20150110111819','SendTime'=>'20150110112639','TraceNo'=>'7fe79ee1-e77b-4607-bd00-65ec4bb80791','TransAmount'=>'0.01','BankSerialNo'=>'011501100048010','Charset'=>'UTF-8','OrderNo'=>'2015011022437','TransType'=>'PT001','PayableFee'=>'0.00','Version'=>'V4.1.2.1.1','Ext1'=>'','Ext2'=>'','TransStatus'=>'01','PayChannel'=>'19','SignType'=>'MD5','ReceivableFee'=>'0.00','Name'=>'REP_B2CPAYMENT','InstCode'=>'ICBC','MerchantNo'=>'146390','MsgSender'=>'SFT','SignMsg'=>'91D1CBE7C3EFC58D259FE920D7930D5F','ErrorMsg'=>'','ErrorCode'=>'' );
		*/

		$data = biz('shengpay')->page( $in );

		if( $data[ 'code' ] < 0 )
		{
			echo $data[ 'memo' ];
		}
		else
		{
			$billno = $in[ 'OrderNo' ];
			$info = biz('payment')->get1( '*', array( 'billno' => $billno ) );

			$site = biz('site');	
			if( $info[ 'site' ] != '' )
			{
				if( $info['memo'] =='m.tuijie.cc')
				{
					$info[ 'site' ]='m.tuijie.cc';
				}
				$url = $site->get_url( $info[ 'site' ], 'payment', 'back', array( 'memo' => $data[ 'memo' ], 'no' => $billno ),
					false );
				header( 'location:' . $url );
			}
			else
			{
				biz('payment')->log( '', 0, '发现异常本地page信息，', array( 'data' => $in, 'type' => 2 ) );
			}
		}
	}

	/** 盛付通页面返回
	 * @param $in
	 */
	function shengpay_finish( $in )
	{
		if( config( 'develop' ) )
		{

		}

		biz('shengpay')->log( 9, 0, 'finish', array( 'data' => $in ) );
		echo biz('shengpay')->finish( $in );
	}

}

$action = new action();
$action->run();
?>