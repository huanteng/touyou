<?php
	if ( isset( $_GET['u'] ) )
	{
		$data = '';
		$url = urldecode( $_GET['u'] );
		$referer = isset( $_GET['referer'] ) ? $_GET['referer'] : '';

		$config = parse_url( $url );
		$fp = fsockopen( $config['host'], 80, $errno, $errstr, 4 );

		if ( $fp )
		{
		   $out = "GET " . $config['path'] . ( isset( $config['query'] ) && $config['query'] != '' ? '?' . $config['query'] : '' ) . " HTTP/1.1\r\n";
		   if ( $referer != '' ) $out .= "Referer: " . $referer . "\r\n";
		   $out .= "Host: " . $config['host'] . "\r\n";
		   $out .= "Connection: Close\r\n\r\n";

		   fwrite( $fp, $out );
		   while( ! feof( $fp ) ) $data .= fgets( $fp, 512 );
		   fclose( $fp );

			$data = stristr( $data, "\r\n\r\n" );
			$data = substr( $data, 4, strlen( $data ) );
		}

		echo $data;
	}
?>