<?php
	function get_or_post( $url, $post = array(), $referer = 'http://www.bet007.com/', $cookie = '', $port = 80, $useragent = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.63 Safari/535.7' )
	{
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_PORT, $port );
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_HEADER, 0 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
		curl_setopt( $ch, CURLOPT_USERAGENT, $useragent );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 0 );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 0 );

		if ( $referer != '' ) curl_setopt( $ch, CURLOPT_REFERER, $referer );
		if ( $cookie != '' ) curl_setopt( $ch, CURLOPT_COOKIE, $cookie );

		if ( count( $post ) > 0 )
		{
			$field = '';
			foreach( $post as $key => $value ) $field .= $key . '=' . urlencode( $value ) . '&';
			$field = substr( $field, 0, -1 );
			curl_setopt( $ch, CURLOPT_POST, 1 );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $field );
		}

		$data = curl_exec( $ch );
		curl_close( $ch );
		return $data;
	}

	if ( isset( $_GET['u'] ) )
	{
		$u = urldecode( $_GET['u'] );
		$info = parse_url( $u );
		$port = isset( $info['port'] ) ? $info['port'] : 80;
		$referer = isset( $_GET['referer'] ) ? $_GET['referer'] : '';
		echo get_or_post( $u, array(), $referer, '', $port );
	}
?>