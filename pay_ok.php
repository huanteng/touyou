<?php /* 供 tuijie 客户端充值成功后用 */
	require 'library/controller.php';

	class action extends controller
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'frontend/template/' ) );
			echo $template->parse( 'pay_ok.php' );
		}
		
		function do_post()
		{
			$this->do_get();
		}
	}

	$action = new action();
	$action->run();
?>