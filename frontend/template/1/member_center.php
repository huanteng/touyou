<html>
<head>
<title>会员中心</title>
</head>
<body>
<div id="before_login_div">
	<table>
		<tr><td>帐号</td><td><input type="text" id="name"></td></tr>
		<tr><td>密码</td><td><input type="password" id="pass"></td></tr>
		<tr><td colspan="4" align="right"><input type="button" onclick="login()" value="登录" id="login_button"></td></tr>
	</table>
</div>
<div id="after_login_div" style="display:none;">
	<p>欢迎 <span id="name_span"></span> 回到骰友！<input type="button" onclick="logout()" value="退出"></p>
	<form method="post" id="charge_form" action="/interface/shengpay/takeorder.php">
		<input type="hidden" id="fee" name="fee" value="0">
		<input type="hidden" id="oid" name="oid" value="0">
		<p>你可以在此快速充值：<input type="text" id="rmb" size="4"> 元 <input type="button" onclick="charge()" value="提交" id="charge_button"></p>
	</form>
</div>
<script type="text/javascript" src="http://lib.sinaapp.com/js/jquery/1.2.6/jquery.min.js"></script>
<script type="text/javascript">
var sid = '';
var uid = '';
var name = '';
var prefix = '/interface/'
$(document).ready
(
	function()
	{
		$( '#name' ).focus();
	}
);
function charge()
{
	if ( sid != '' )
	{
		$.ajax
		(
			{
				type : "POST",
				url : 'member_center.php',
				dataType : 'json',
				data : 'sid=' + sid + '&count=' + $( '#rmb' ).val() + '&target_uid=' + uid,
				success : function( result )
					{
						if ( result.no > 0 )
						{
							$( '#oid' ).val( result.no );
							$( '#fee' ).val( $( '#rmb' ).val() );

							$( '#login_button' ).attr( 'disabled', true );
							$( '#charge_form' ).attr( 'action', prefix + 'shengpay/takeorder.php' );

							$( '#charge_form' ).submit();
						}
					}
			}
		);
	}
}
function login()
{
	$( '#login_button' ).attr( 'disabled', true );

	$.ajax
	(
		{
			type : "POST",
		//	url : prefix + 'user.php',
			url : 'member_center.php',
			dataType : 'json',
			data : 'type=logon&name=' + $( '#name' ).val() + '&pass=' + $( '#pass' ).val(),
			success : function( result )
				{
					if ( result.code > 0 )
					{
						$( '#name' ).val( '' );
						$( '#pass' ).val( '' );
						$( '#login_button' ).attr( 'disabled', false );

						sid = result.sid;
						uid = result.data.id;
						name = result.data.name;

						$( '#name_span' ).text( name );

						$( '#before_login_div' ).hide();
						$( '#after_login_div' ).show();

						$( '#rmb' ).focus();
					}
				}
		}
	);
}
function logout()
{
	sid = '';
	uid = '';
	name = '';

	$( '#after_login_div' ).hide();
	$( '#before_login_div' ).show();

	$( '#name' ).focus();
}
</script>
</body>
</html>