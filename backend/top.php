<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{			
			$template = load( 'template', array( 'dir' => 'template/' ) );

			$user = load( 'biz.administrator' );
			$info = $user->get_by_id( $this->account );

			$template->assign( 'name', $info['name'] );
			echo $template->parse( 'top.php' );
		}
	}

	$action = new action();
	$action->run();
?>