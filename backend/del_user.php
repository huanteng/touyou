<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$database = load( 'database' );
			$user_array = array( 9971, 9972, 9967, 9956, 9955 );

			foreach( $user_array as $id )
			{
				$database->command( 'delete from user where id = ' . $id );
				$database->command( 'delete from moving where user = ' . $id );
				$database->command( 'delete from friend where user = ' . $id );
				$database->command( 'delete from friend where friend = ' . $id );
				$database->command( 'delete from vote_album_log where user = ' . $id );
				$database->command( 'delete from user_props where user = ' . $id );
				$database->command( 'delete from user_props_log where user = ' . $id );
			}
		}
	}

	$action = new action();
	$action->run();
?>