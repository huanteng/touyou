<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$database = load( 'database' );
			$template = load( 'template', array( 'dir' => 'template/' ) );
		
			$_start = isset( $this->input['_start'] ) ? $this->input['_start'] : '';
			$_end = isset( $this->input['_end'] ) ? $this->input['_end'] : '';

			$start = ( $_start != '' ) ? strtotime($_start) : strtotime(date('Y-m-d'));
			$end = ( $_end != '' ) ? strtotime($_end.' 23:59:59') : $start+86399;

			$data = $result = array();
			$result = $database->select( "select * from online_report where date between ".$start." and ".$end );	
				
			for($i=0;$i<24;$i++)
			{
				$data['hour'.$i] = array(0=>0,1=>0);;
				$data['total_0'] = 0;
				$data['total_1'] = 0;
			}	

			foreach( $result as $value )
			{				
				$value['data'] = isset($value['data']) ? unserialize($value['data']) : array();

				for($i=0;$i<24;$i++)
				{
					$data['hour'.$i][0] += isset( $value['data']['hour'.$i][0] ) ? $value['data']['hour'.$i][0] : 0;
					$data['hour'.$i][1] += isset( $value['data']['hour'.$i][1] ) ? $value['data']['hour'.$i][1] : 0;
					
					$data['total_0'] += isset( $value['data']['hour'.$i][0] ) ? $value['data']['hour'.$i][0] : 0;
					$data['total_1'] += isset( $value['data']['hour'.$i][1] ) ? $value['data']['hour'.$i][1] : 0;
				}								
			}

			$template->assign( 'data', $data );
			$template->assign( '_start', $_start );
			$template->assign( '_end', $_end );
			$template->assign( 'start', $start );
			$template->assign( 'end', $end );
			$template->appoint( $this->input );
			echo $template->parse( 'online_report.php' );
		}
	}

	$action = new action();
	$action->run();
?>