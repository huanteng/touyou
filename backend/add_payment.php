<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'add_payment.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入用户' );

			if ( isset( $this->input['user'] ) && $this->input['user'] != '' )
			{
				if ( isset( $this->input['money'] ) && is_numeric( $this->input['money'] ) )
				{
					$payment = load( 'biz.payment' );
					$this->input['type'] = 1;
					$this->input['status'] = 1;
					$code = $payment->add( $this->input );

					if ( $code == 1 )
					{
						$result = array( 'status' => 0, 'message' => '成功添加充值' );
					}
					else
					{
						$result = array( 'status' => -1, 'message' => '添加充值失败' . $code );
					}
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>