<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$this->input['_start'] = isset( $this->input['_start'] ) ? $this->input['_start'] : '';
			$this->input['_end'] = isset( $this->input['_end'] ) ? $this->input['_end'] : '';
			$this->input['_status'] = isset( $this->input['_status'] ) ? $this->input['_status'] : '';
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;
			$where = '';
			
			if ( $this->input['_name'] != '' )
			{
				$database = load( 'database' );
				$user = $database->unique( "select id from user where name = '" . $this->input['_name'] . "'" );
				if ( ! isset( $user['id'] ) ) $user['id'] = 0;
				$where .= ' and user = ' . $user['id'];
			}
			
			if( $this->input['_start'] != '' )
			{
				$where .= ' and time between ' . strtotime( $this->input['_start'] ) . ' and ' . ( strtotime( $this->input['_start'] ) + 86399 );
			}
			
			if( $this->input['_status'] != '' )
			{
				$where .= ' and status = ' . $this->input['_status'];
			}

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$payment = load( 'biz.payment' );
			$result = $payment->lists( $page, $where );

			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->assign( 'status_dict', $payment->get_status_dict() );
			$template->assign( 'type_dict', $payment->get_type_dict() );
			$template->appoint( $this->input );
			echo $template->parse( 'payment.php' );
		}
	}

	$action = new action();
	$action->run();
?>