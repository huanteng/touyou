<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;
			$where = '';

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$intervene_real = load( 'biz.intervene_real' );

			$template->appoint( $intervene_real->lists( $page, $where ) );
			$template->appoint( $this->input );
			echo $template->parse( 'intervene_real.php' );
		}
	}

	$action = new action();
	$action->run();
?>