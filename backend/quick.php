<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'quick.php' );
		}

		function do_post()
		{
			if ( isset( $this->input['type'] ) && is_numeric( $this->input['type'] ) )
			{
				$database = load( 'database' );

				if ( $this->input['type'] == '1' )
				{
					$match = load( 'biz.match' );
					$vs_ing_array = $this->database->select( 'select * from vs' );
					// 20130426，担心误触发
					// foreach( $vs_ing_array as $vs ) $match->breakoff( $vs );
				}
				else if ( $this->input['type'] == '2' )
				{
					$user = load( 'biz.user' );
					$user_array = $database->select( 'select user from online where npc = 1' );	
					foreach( $user_array as $u ) $user->_logout( $u['user'] );
				}
				else if ( $this->input['type'] == '3' )
				{
					$database->command( "update configure set data = 'no' where code = 'allow_login'" );
				}
				else if ( $this->input['type'] == '4' )
				{
					$database->command( "update configure set data = 'yes' where code = 'allow_login'" );
				}
				else if ( $this->input['type'] == '5' )
				{
					$database->command( "delete from announce" );
				}
				else if ( $this->input['type'] == '6' )
				{
					$database->command( "delete from message" );
					$database->command( "delete from notification" );
				}
				else if ( $this->input['type'] == '7' )
				{
					$system = load( 'biz.system' );
					$system->reset();
				}
				else if ( $this->input['type'] == '8' )
				{
					$user = load( 'biz.user' );
					$user_array = $database->select( 'select user from online where npc = 0' );	
					foreach( $user_array as $u ) $user->_logout( $u['user'] );
				}
			}
		}
	}

	$action = new action();
	$action->run();
?>