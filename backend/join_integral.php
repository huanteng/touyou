<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'join_integral.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入用户名' );

			if ( isset( $this->input['name'] ) && $this->input['name'] != '' )
			{
				$database = load( 'database' );
				$temp = $database->unique( "select id from user where name = '" . $this->input['name'] . "'" );
				$user = isset( $temp['id'] ) ? $temp['id'] : 0;

				if ( $user > 0 && isset( $this->input['time'] ) && $this->input['time'] != '' )
				{
					$time = strtotime( $this->input['time'] );

					$id = $database->add( 'integral_rank', array( 'user' => $user, 'time' => $time, 'ready' => 0 ) );
					if ( $id > 0 ) $database->command( 'update integral_prize_pool set person = person + 1 where time = ' . $time );

					$result = array( 'status' => 0, 'message' => '成功报名骰魔大赛' );
				}
				else
				{
					$result = array( 'status' => -2, 'message' => '请选择比赛时间' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>