<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$match = load( 'biz.match' );
				$info = $match->detail( array( 'id' => $this->input['id'], 'type' => 'pair' ) );

				if ( isset( $info['id'] ) )
				{
					$template = load( 'template', array( 'dir' => 'template/' ) );
					$template->appoint( $info );
					echo $template->parse( 'get_pair.php' );
				}
			}
		}
	}

	$action = new action();
	$action->run();
?>