<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$database = load( 'database' );
			$navigation = load( 'navigation' );
			$template = load( 'template', array( 'dir' => 'template/' ) );

			$id = isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) ? $this->input['id'] : 0 ;
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;
			$type = isset( $this->input['type'] ) ? $this->input['type'] : '';

			if( $type == 'task_queue5' )
			{
				//预发布的主题
				$temp = $database->unique("select * from task_queue where id = ".$id);
				if( isset($temp['data']) )
				{
					$temp['data'] = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $temp['data'] );
					$data = unserialize($temp['data']);
				}
				else
				{
					$data = '';
				}
				
				$info['content'] = isset($data['content']) ? $data['content'] : '';
				$info['sender'] = isset($temp['user']) ? $temp['user'] : 0;
				$info['time'] = isset($temp['time']) ? $temp['time'] : time();
				$id = 999999;
			}
			else
			{
				$info = $database->unique("select * from comment where id = ".$id);
			}

			$result = array();
			$count = 'from comment,user where comment.sender=user.id and comment.parent = '.$id;
			$list = 'select comment.*,user.name,user.npc ' . $count . ' order by id desc';
			$result = $database->split( $count, $list, 20, $page );

			foreach( $result['data'] as $index => $value )
			{
				if( $value['sub_parent'] != 0 )
				{
					$temp = $database->select("select * from comment where sub_parent = '".$value['id']."'");
					foreach( $temp as $t )
					{
						$value[$index]['sub_comment'] = $t['content'];
					}
				}
				$temp = $database->unique("select name,npc from user where id =".$value['receiver']);
				$result['data'][$index]['receiver_name'] = isset($temp['name']) ? $temp['name'] : '';
				$result['data'][$index]['receiver_npc'] = isset($temp['npc']) ? $temp['npc'] : '';
			}

			$result['bar'] = $navigation->compute( $result['total'], 20 );

			//任务队列内容
			$task_queue = $database->select("select * from task_queue where target = ".$id." and status=1 and type=2");
			$task_list = array();

			foreach($task_queue as $value)
			{
				if(isset($value['data']))
				{
					$data_list = unserialize($value['data']);
					foreach($data_list as $item)
					{
						$name = '随机未定';
						if( $item['sender'] > 0 )
						{
							$temp = $database->unique("select name from user where id = ".$item['sender']);
							$name = isset($temp['name']) ? $temp['name'] : '随机未定';
						}
						$task_list[] = array( 'id'=>$value['id'],'content'=>$item['content'],'name'=>$name,'time'=>isset($item['time'])?$item['time']:time() );
					}
				}
			}

			$template->assign( 'type', $type );
			$template->assign( 'info', $info );
			$template->assign( 'total', $result['total'] );
			$template->assign( 'data', $result['data'] );
			$template->assign( 'task_list', $task_list );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'auto_publish_comment2.php' );
		}

		function format( $value )
		{
			return strip_tags( ( trim( $value ) ) );
		}

		function do_post()
		{
			$database = load( 'database' );

			$result = array( 'status' => -1, 'message' => '参数错误' );

			if ( isset( $this->input['op'] ) && $this->input['op'] == 'publish' )
			{
				if ( isset( $this->input['target'] ) && is_numeric( $this->input['target'] ) && isset( $this->input['user'] ) && is_numeric( $this->input['user'] ) && isset( $this->input['name'] ) && isset( $this->input['content'] ) && $this->input['content'] != '' && isset( $this->input['time'] ) && $this->input['time'] != '' && isset( $this->input['sex'] ) )
				{

					$input = array();
					foreach( array('name','time','content','sex') as $field )
					{
						$temp = explode('|||,',$this->input[$field]);
						foreach( $temp as $index => $value ) $input[$index][$field] = str_replace( '|||', '', $value );
					}

					$data = array();
					$first_time = 0;
					foreach( $input as $index => $value )
					{
						if( isset($value['content']) && $value['content'] == '' )
						{
							$result = array( 'status' => -1, 'message' => '第'.($index+1).'个回复内容为空' );
							break;
						}

						$temp =	$database->unique("select id from user where name = '".$value['name']."' and npc = 1");
						if( isset($temp['id']) || $value['name'] == '' || $value['name'] == '骰妖子' )//骰妖子特殊
						{
							if( $first_time == 0 ) $first_time = strtotime($value['time']);
							
							if($value['name'] == '骰妖子') $value['sender'] = 850 ;//骰妖子特殊
							else
								$value['sender'] = ($value['name'] == '') ? '' : $temp['id'];

							$value['content'] = str_replace( array( "\'", '\"' ), '', $value['content'] );

							$data[] = array( 'complete'=>0, 'sender' => $value['sender'], 'content' => $value['content'], 'sex'=>$value['sex'], 'time'=>strtotime($value['time']) );
						}
						else
						{
							$result = array( 'status' => -1, 'message' => '第'.($index+1).'个回复用户名不存在或者不是NPC' );
							break;//一个回复验证不通过就跳出
						}
					}

					if( count($input) == count($data) )
					{
						$status = 0;
						$target = $this->input['target'];
						$user = $this->input['user'];

						//预发布说两句营销回复的时候
						if( $this->input['type'] == 'task_queue5' )
						{
							$status = 5;
						}
						else
						{
							$status = 1;
						}

						$queue = array( 'type' => 2, 'create' => time(),'time' => $first_time, 'status' => $status, 'target' => $target, 'data' => serialize( $data ), 'user'=>$user,'creater'=>$this->account  );
						// 20121228，防单引号错误
						// $database->add( 'task_queue', $queue );
						$task_queue = load( 'biz.task_queue' );
						$task_queue->add( $queue );

						$result = array( 'status' => 0, 'message' => '保存成功' );
					}

				}

				echo json_encode( $result );

			}

		}
	}

	$action = new action();
	$action->run();
?>