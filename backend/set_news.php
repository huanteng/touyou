<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$news = load( 'biz.news' );
				$info = $news->get_by_id( $this->input['id'] );

				if ( isset( $info['id'] ) )
				{
					$template = load( 'template', array( 'dir' => 'template/' ) );
					$template->appoint( $info );
					$template->assign( 'cate_dict', $news->get_cate_dict() );
					echo $template->parse( 'set_news.php' );
				}
			}
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入标题' );

			if ( isset( $this->input['title'] ) && $this->input['title'] != '' )
			{
				if ( isset( $this->input['content'] ) && $this->input['content'] != '' )
				{
					if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
					{
						$this->input['content'] = $_REQUEST['content'];

						$news = load( 'biz.news' );
						$news->set( $this->input );
						
						$result = array( 'status' => 0, 'message' => '成功设置文章' );
					}
					else
					{
						$result = array( 'status' => -2, 'message' => '请传输id参数' );
					}
				}
				else
				{
					$result = array( 'status' => -1, 'message' => '请输入内容' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>