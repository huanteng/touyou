<?php
	require '../library/config.php';
	$final = array( 'code' => 0, 'data' => '参数错误' );
	$user = load( 'biz.user' );
	$name = 'logo_file';

	if ( isset( $_FILES[$name] ) && $_FILES[$name]['error'] == 0 && isset( $_REQUEST['user'] ) && is_numeric( $_REQUEST['user'] ) )
	{
		$album = load( 'biz.album' );
		$album->_upload( array( 'name' => $name, 'user' => $_REQUEST['user'] ) );
		$new = $album->database->unique( 'select id from album where user = ' . $_REQUEST['user'] . ' order by id desc limit 1' );

		if ( isset( $new['id'] ) )
		{
			$album->set_logo( array( 'id' => $new['id'], 'uid' => $_REQUEST['user'] ) );
		}
	}

	echo json_encode( $final );
?>