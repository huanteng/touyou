<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$administrator = load( 'biz.administrator' );
				$administrator->del( $this->input['id'] );
			}

			$this->prompt( '操作已成功', array( array( 'url' => 'administrator.php', 'name' => '用户管理', 'default' => true ) ) );
		}
	}

	$action = new action();
	$action->run();
?>