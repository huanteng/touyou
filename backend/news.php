<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;

			$where = $this->input['_name'] != '' ? " and title like '%" . $this->input['_name'] . "%'" : '';

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$news = load( 'biz.news' );
			$result = $news->lists( $page, $where );

			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->assign( 'cate_dict', $news->get_cate_dict() );
			$template->appoint( $this->input );
			echo $template->parse( 'news.php' );
		}
	}

	$action = new action();
	$action->run();
?>