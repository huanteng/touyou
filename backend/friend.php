<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;
			
			$this->input['_user'] = isset( $this->input['_user'] ) ? $this->input['_user'] : '';
			$this->input['_friend'] = isset( $this->input['_friend'] ) ? $this->input['_friend'] : '';
			
			$database = load( 'database' );
			
			$user_id = $friend_id = '';
			if( $this->input['_user'] != '' )
			{
				$temp = $database->unique( "select id from user where name = '" . $this->input['_user'] . "'" );
				$user_id = isset($temp['id']) ? $temp['id'] : '';
			}
			
			if( $this->input['_friend'] != '' )
			{
				$temp = $database->unique( "select id from user where name = '" . $this->input['_friend'] . "'" );
				$friend_id = isset($temp['id']) ? $temp['id'] : '';
			}

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$friend = load( 'biz.friend' );
			$result = $friend->_lists( array('user'=>$user_id,'friend'=>$friend_id,'page'=>$page) );
			
			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'friend.php' );
		}
				
	}

	$action = new action();
	$action->run();
?>