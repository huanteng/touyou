<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$database = load( 'database' );
			$navigation = load( 'navigation' );
			$template = load( 'template', array( 'dir' => 'template/' ) );

			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$this->input['_npc'] = isset( $this->input['_npc'] ) ? $this->input['_npc'] : '0';
			$this->input['_sex'] = isset( $this->input['_sex'] ) ? $this->input['_sex'] : '';

			$where = '';
			$where .= $this->input['_name'] != '' ? " and user.name like '%" . $this->input['_name'] . "%'" : '';
			$where .= $this->input['_npc'] != '' ? " and online.npc = " . $this->input['_npc'] : '';
			$where .= $this->input['_sex'] != '' ? " and online.sex = " . $this->input['_sex'] : '';

			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;
			$size = isset( $this->input['size'] ) && is_numeric( $this->input['size'] ) ? $this->input['size'] : 24;
			$count = 'from user, online where user.id = online.user' . $where;
			$list = 'select user.name, user.logout, user.sex, online.user, online.last, online.login, online.npc, online.forever ' . $count . ' order by online.login asc';
			$result = $database->split( $count, $list, $size, $page );
			$result['bar'] = $navigation->compute( $result['total'], $result['size'] );


			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'online.php' );
		}
	}

	$action = new action();
	$action->run();
?>