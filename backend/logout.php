<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$database = load( 'database' );
			$user = load( 'biz.administrator' );

			$info = $user->get_by_id( $this->account );
			$database->add( 'backend_log', array( 'administrator' => $info['name'], 'time' => time(), 'content' => '退出' ) );

			$cookie = load( 'cookie' );
			$cookie->set( 'account', '' );

			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'login.php' );
		}
	}

	$action = new action();
	$action->run();
?>