<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$database = load( 'database' );
			$template = load( 'template', array( 'dir' => 'template/' ) );

			$template->assign( 'props_array', $database->select( 'select * from props where kind != 0' ) );
			echo $template->parse( 'send_props.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入用户' );

			if ( isset( $this->input['user'] ) && $this->input['user'] != '' )
			{
				if ( isset( $this->input['quantity'] ) && is_numeric( $this->input['quantity'] ) )
				{
					if ( isset( $this->input['props'] ) && $this->input['props'] != '')
					{
						$props = load( 'biz.props' );
						$exisit = $props->database->unique( "select id from user where name = '" . $this->input['user'] . "'" );

						if ( isset( $exisit['id'] ) )
						{
							$memo = isset( $this->input['memo'] ) && $this->input['memo'] != '' ? $this->input['memo'] : '手工赠送';
							$code = $props->gain( array( 'user' => $exisit['id'], 'props' => $this->input['props'], 'quantity' => $this->input['quantity'], 'type' => 11, 'memo' => $memo ) );

							if ( $code )
							{
								$result = array( 'status' => 0, 'message' => '成功赠送道具' );
							}
							else
							{
								$result = array( 'status' => -5, 'message' => '赠送道具失败' );
							}
						}
						else
						{
							$result = array( 'status' => -4, 'message' => '用户不存在' );
						}
					}
					else
					{
						$result = array( 'status' => -3, 'message' => '请选择道具' );
					}
				}
				else
				{
					$result = array( 'status' => -2, 'message' => '请输入正确数量' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>