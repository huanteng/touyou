<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$uid = isset( $this->input['uid'] ) && is_numeric( $this->input['uid'] ) ? $this->input['uid'] : 0;
			$name = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$content = isset( $this->input['_content'] ) ? $this->input['_content'] : '';
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$comment = load( 'biz.comment' );
			$result = $comment->lists2( array('uid'=>$uid,'types'=>0,'name'=>$name,'content'=>$content,'page'=>$page) );

			$template->assign( '_name', $name );
			$template->assign( '_content', $content );
			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'comment1.php' );
		}
		
		function do_post()
		{
			if ( isset( $this->input['id'] ) && is_array( $this->input['id'] ) && count( $this->input['id'] ) )
			{
				$comment = load( 'biz.comment' );
				
				foreach( $this->input['id'] as $id )
				{
					$comment->delete_comment( $id );
				}	
			}

			$this->prompt( '操作已成功', array( array( 'url' => 'comment1.php', 'name' => '说两句管理', 'default' => true ) ) );
		}
				
	}

	$action = new action();
	$action->run();
?>