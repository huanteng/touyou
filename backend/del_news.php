<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$news = load( 'biz.news' );
				$news->del( $this->input['id'] );
			}

			$this->prompt( '操作已成功', array( array( 'url' => 'news.php', 'name' => '文章管理', 'default' => true ) ) );
		}
	}

	$action = new action();
	$action->run();
?>