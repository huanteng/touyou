<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['user'] ) && count( $this->input['user'] ) )
			{
				$user_array = array();
				$database = load( 'database' );
				$props = load( 'biz.props' );

				foreach( $this->input['user'] as $id )
				{
					$info = $database->unique( 'select name from user where id = ' . $id );
					//$gold = $props->free( array( 'user' => $id, 'props' => 'gold' ) );
					$gold = $props->gold( $id );
					$profit = $database->unique( 'select sum( `change` ) as total from user_props_log where type = 5 and user = ' . $id . ' and props = 1 and time >= ' . strtotime( date( 'Y-m-d' ) ) );
					$profit = isset( $profit['total'] ) && is_numeric( $profit['total'] ) ? $profit['total'] : 0;

					$user_array[] = array( 'id' => $id, 'name' => $info['name'], 'gold' => $gold, 'profit' => $profit );
				}

				$template = load( 'template', array( 'dir' => 'template/' ) );
				$template->assign( 'user_array', $user_array );
				echo $template->parse( 'set_npc_challenge.php' );
			}
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请选择目标用户' );

			if ( isset( $this->input['user'] ) && is_numeric( $this->input['user'] ) )
			{
				if ( isset( $this->input['times_1'] ) && is_numeric( $this->input['times_1'] ) && isset( $this->input['times_2'] ) && is_numeric( $this->input['times_2'] ) )
				{
					if ( isset( $this->input['bet_1'] ) && is_numeric( $this->input['bet_1'] ) && isset( $this->input['bet_2'] ) && is_numeric( $this->input['bet_2'] ) )
					{
						if ( isset( $this->input['accept'] ) && is_numeric( $this->input['accept'] ) )
						{
							if ( isset( $this->input['try'] ) && is_numeric( $this->input['try'] ) )
							{
								if ( isset( $this->input['interval_1'] ) && is_numeric( $this->input['interval_1'] ) && isset( $this->input['interval_2'] ) && is_numeric( $this->input['interval_2'] ) )
								{
									if ( isset( $this->input['start'] ) && strtotime( $this->input['start'] ) )
									{
										$database = load( 'database' );
										$exisit = $database->unique( 'select id from task_queue where type = 5 and status != 3 and user = ' . $this->input['user'] );

										if ( ! isset( $exisit['id'] ) )
										{
											if ( isset( $this->input['npc'] ) && $this->input['npc'] != '' )
											{
												$uids = array();
												$this->input['npc'] = str_replace( '，', ',', $this->input['npc'] );
												$npc = explode( ',', $this->input['npc'] );

												foreach( $npc as $temp )
												{
													if ( is_numeric( $temp ) )
													{
														$uids[] = $temp;
													}
													else
													{
														$temp = $database->unique( "select id from user where name = '" . $temp . "'" );
														if ( isset( $temp['id'] ) ) $uids[] = $temp['id'];
													}
												}

												$this->input['npc'] = $uids;
											}
											else
											{
												$this->input['npc'] = array();
											}

											$this->input['times'] = mt_rand( $this->input['times_1'], $this->input['times_2'] );
											if ( count( $this->input['npc'] ) ) $this->input['times'] = count( $this->input['npc'] );

											$queue = array( 'user' => $this->input['user'], 'create' => time(),'creater'=>$this->account, 'type' => 5, 'time' => strtotime( $this->input['start'] ), 'status' => 1, 'target' => $this->input['user'], 'data' => serialize( $this->input ) );
											$database->add( 'task_queue', $queue );

											$result = array( 'status' => 0, 'message' => '成功设置挑战' );
										}
										else
										{
											$result = array( 'status' => -8, 'message' => '设置失败，此用户还有未完的挑战队列' );
										}
									}
									else
									{
										$result = array( 'status' => -7, 'message' => '请输入正确的开始时间' );
									}
								}
								else
								{
									$result = array( 'status' => -6, 'message' => '请输入正确的发起间隔时间' );
								}
							}
							else
							{
								$result = array( 'status' => -5, 'message' => '请输入正确的重复挑战概率' );
							}
						}
						else
						{
							$result = array( 'status' => -4, 'message' => '请输入正确的重复应战概率' );
						}
					}
					else
					{
						$result = array( 'status' => -3, 'message' => '请输入正确的挑战金' );
					}
				}
				else
				{
					$result = array( 'status' => -2, 'message' => '请输入正确的挑战次数' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>