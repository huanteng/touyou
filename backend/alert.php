<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$database = load( 'database' );
			$template = load( 'template', array( 'dir' => 'template/' ) );
			$alert_array = $database->select( 'select alert.*, task_queue.time from alert, task_queue where alert.id = task_queue.type and alert.id not in ( 1000 )' );

			foreach( $alert_array as $key => $value )
			{
				$ids = $value['data'] != '' ? unserialize( $value['data'] ) : array();
				$alert_array[$key]['num'] = count( $ids );
			}
			
			$current_start_time = strtotime( date( 'Y-m-01' ) );
			$current_end_time = strtotime( date( 'Y-m-d' ) ) + 86399;

			$current_day = date( 'd' );
			$prev_month = date( 'm' ) - 1;
			$prev_year = date( 'Y' );

			if ( $prev_month == 0 )
			{
				$prev_month = 12;
				$prev_year = $prev_year - 1;
			}

			if ( $current_day > 28 )
			{
				$prev_standard_day = $this->days_in_month( $prev_month, $prev_year );
				if ( $current_day > $prev_standard_day ) $prev_day = $prev_standard_day;
				else $prev_day = $current_day;
			}
			else
			{
				$prev_day = $current_day;
			}

			$prev_start_time = strtotime( $prev_year . '-' . $prev_month . '-01' );
			$prev_end_time = strtotime( $prev_year . '-' . $prev_month . '-' . $prev_day ) + 86399;

			$current_sql = "select sum( money ) as money from payment where status = 1 and time between " . $current_start_time . " and " . $current_end_time;

			$prev_sql = "select sum( money ) as money from payment where status = 1 and time between " . $prev_start_time . " and " . $prev_end_time;

			$current = $database->unique( $current_sql );
			$prev = $database->unique( $prev_sql );

			$current_real = $database->unique( 'select sum(quantity) as money from user_props p, user u where u.id = p.user and u.npc = 0 and (p.expire=0 or p.expire<unix_timestamp())' );

			$template->assign( 'alert_array', $alert_array );
			$template->assign( 'current', round( $current['money'], 2 ) );
			$template->assign( 'plan', round( $current['money'] * date( 't' ) / date( 'd' ), 2 ) );
			$template->assign( 'percent', $prev['money'] == 0 ? '-' : ( ( round( ( $current['money'] - $prev['money'] ) * 100 / $prev['money'] ) ) . '%' ) );
			$template->assign( 'current_real', $current_real['money'] );
			echo $template->parse( 'alert.php' );
		}
		function days_in_month($a_month, $a_year) {
	  		return date('t', strtotime($a_year . '-' . $a_month . '-01'));
		}
	}

	$action = new action();
	$action->run();
?>