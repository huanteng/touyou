-- pos layout
local ReplyConfig = {
	[LIGHT_UI.SCREEN_480_800] = { -- 整个背景
		["desc_config"] = {
			["x"] = 13,
			["y"] = 720,
		},
		["edit_config"] = {
			["x"] = 20,
			["y"] = 585,
			["width"] = 450,
			["height"] = 125,
		},
		["reply_btn_config"] = { -- 顶部右上角按钮
			["x"] = 405,
			["y"] = 738,
			["normal_res"] = "oktosend.png",
			["click_res"] = "oktosend.png",
		},
		["move_page_config"] = { -- 滑动页
			["x"] = 0, -- 滑动页左下角点x
			["y"] = 0, -- 滑动页左下角点y
			["view_height"] = 698, -- 滑动页可视区域高度
			["inner_y"] = 688, -- 滑动页内部列表的起始y坐标
		},
		["reply_content"] = { -- 回复内容
			["x"] = 15,
			["y"] = 0,
			["margin"] = 15,
		},		
	},
}
--------------------------------------------------------------

function getConfigData()
	return ReplyConfig 
end

