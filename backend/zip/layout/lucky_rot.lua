-- pos layout
local LuckyConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["reward_list_btn_config"] = { -- 获奖列表按钮 
			["x"] = 402,
			["y"] = 738,
			["res"] = "dial_explain_pic.png",
		},
		["bg_pic_config"] = { -- 背景图 左上角点对齐
			["x"] = 0,
			["y"] = 738,
			["res"] = "dial_window_bg.png",
		},
		["left_money_config"] = { -- 剩余幸运星
			["x"] = 0,
			["y"] = 65,
			["res"] = "dial_count_bg.png",
		},
		["lucky_sound"] = { -- 转轮声音
			sound = 'sound/openbox.mp3',
			ms = 1000,
		},
		
		alert = {
			bg = { --黑色背景
				id = 'bg_btn',
				ax = 0.5,
				ay = 0.5,						
				x = 240,
				y = 400,
				sx = 480,
				sy = 800,
				res = 'res/bj.png',				
			},
			window = { --窗口背景
				ax = 0.5,
				ay = 0.5,
				x = 240,
				y = 400,
				res = 'res/xxp.png',
			},
			title = { --标题
				ax = 0.5,
				ay = 0.5,
				x = 200 - 68,
				y = 500 - 10,
				css = 'c4',
				text = '提示',
			},
			content = { --内容
				ax = 0,
				ay = 0.5,
				sx = 275,
				sy = 65,
				x = 105,
				y = 450,
				css = 'c4',
				text = '测试内容',
			},
			ok_btn = { --确定按钮
				ax = 0.5,
				ay = 0.5,
				tx = 0,
				ty = 0,
				x = 100 + 65,
				y = 100 + 255,
				sx = 100,
				sy = 59,
				css = 'blue_btn',
				text = '查看背包',
				text_css = 'c4',
			},
			cancel_btn = { --取消按钮
				ax = 0.5,
				ay = 0.5,
				tx = 0,
				ty = 0,
				x = 250 + 65,
				y = 100 + 255,
				sx = 100,
				sy = 59,
				css = 'gray_btn',
				text = '继续抽奖',
				text_css = 'c4',
			},				
		},
		
		alert2 = {
			bg = { --黑色背景
				id = 'bg_btn',
				ax = 0.5,
				ay = 0.5,						
				x = 240,
				y = 400,
				sx = 480,
				sy = 800,
				res = 'res/bj.png',				
			},
			window = { --窗口背景
				ax = 0.5,
				ay = 0.5,
				x = 240,
				y = 400,
				res = 'res/xxp.png',
			},
			title = { --标题
				ax = 0.5,
				ay = 0.5,
				x = 200 - 68,
				y = 500 - 10,
				css = 'c4',
				text = '提示',
			},
			content = { --内容
				ax = 0,
				ay = 0.5,
				sx = 275,
				sy = 65,
				x = 105,
				y = 450,
				css = 'c4',
				text = 'sorry，你没有幸运星，无法进行抽奖，可通过任务或购买方式获得',
			},
			ok_btn = { --确定按钮
				ax = 0.5,
				ay = 0.5,
				tx = 0,
				ty = 0,
				x = 100 + 65 + 80,
				y = 100 + 235,
				sx = 200,
				sy = 59,
				css = 'blue_btn',
				text = '确定',
				text_css = 'c4',
			},				
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return LuckyConfig 
end

