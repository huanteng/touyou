-- pos layout
local UploadConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		send_btn = {
			to = 'head_bg',
			x = 200,
			res = 'oktosend.png',
		},
		
		take_btn = {
			id = 'take_btn',
			x = 130,
			y = 700,
			sx = 180,
			sy = 45,
			css = 'blue_btn',
			text = '拍照上传',
		},
		
		file_btn = {
			to = 'take_btn',
			x = 220,
			sx = 180,
			sy = 45,
			css = 'blue_btn',
			text = '相册上传',
		},
		
		
	},
}
--------------------------------------------------------------

function getConfigData()
	return UploadConfig 
end

