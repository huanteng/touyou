-- pos layout
local VideoAuthConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		desc_txt = {
			id = 'desc_txt',
			to = 'xyz',
			tx = 0,
			ty = 1,
			sx = 450,
			sy = 10,
			x = 15,
			y = -10,
			css = 'b2',
			text = '真实的你，可以让更多的朋友了解你，更能为你吸引关注的目光，统计显示，交友网络中90%的人更愿意与有视频认证的用户进行深层次的交流互动。\n20秒！点亮认证标记，展现最真实的你',
		},
		video_btn = {
			id = 'video_btn',
			to = 'xyz',
			y = 150,
			sx = 360,
			sy = 50,
			ax = 0.5,
			ay = 0.5,
			css = 'blue_btn',
			text = '点击开始视频认证',
			text_css = 'c4',
		},
		video_txt = {
			id = 'video_txt',
			to = 'xyz',
			y = 110,
			sx = 100,
			sy = 40,
			ax = 0.5,
			ay = 0.5,
			css = 'b9',
			text = '',
		},
		success_txt = {
			id = 'success_txt',
			to = 'xyz',
			tx = 0,
			ty = 1,
			sx = 450,
			sy = 10,
			ax = 0,
			ay = 1,
			x = 15,
			y = -230,
			css = 'b2',
			text = ' '
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return VideoAuthConfig 
end