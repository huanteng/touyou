
local RoomFaceConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		room_face_bg = { --
			id = 'face_bg',
			x = 240,
			y = 350,
			sy = 460,
			res = 'res/bqk.png',
		},
		face_btn = {            --表情按钮
            id = 'face_btn', 
			x = 440,
			y = 596,
			res = 'res/bq_off.png',
			on = 'res/bq_on.png',
		},
		face_info = {
			count = 12,
			stay_time = 3, 						
		},			
		face_sel_1 = {                --第一行表情主参
			id ='face_sel_1',
		    to = 'face_bg',
			x = -149,
			y = 110,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		face_sel_2 = {
			id ='face_sel_2',
		    to = 'face_sel_1',
			x = 100,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		face_sel_3 = {
			id ='face_sel_3',
		    to = 'face_sel_2',
			x = 100,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		face_sel_4 = {
			id ='face_sel_4',
		    to = 'face_sel_3',
			x = 100,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		face_sel_5 = {                  --第二行表情主参
			id ='face_sel_5',
		    to = 'face_sel_1',
			y = -110,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		face_sel_6 = {
			id ='face_sel_6',
		    to = 'face_sel_5',
			x = 100,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		face_sel_7 = {
			id ='face_sel_7',
		    to = 'face_sel_6',
			x = 100,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		face_sel_8 = {
			id ='face_sel_8',
		    to = 'face_sel_7',
			x = 100,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		face_sel_9 = {          --第三行表情主参
			id ='face_sel_9',
		    to = 'face_sel_5',
			y = -110,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		face_sel_10 = {
			id ='face_sel_10',
		    to = 'face_sel_9',
			x = 100,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		face_sel_11 = {
			id ='face_sel_11',
		    to = 'face_sel_10',
			x = 100,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		face_sel_12 = {
			id ='face_sel_12',
		    to = 'face_sel_11',
			x = 100,
			res = 'res/bq/bqan.png',
			on = 'res/bq/bqan.png',
		},
		action_face_1 = {    --表情动画
			res = 'res/bq/12.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		action_face_2 = {    --表情动画
			res = 'res/bq/2.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		action_face_3 = {    --表情动画
			res = 'res/bq/3.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		action_face_4 = {    --表情动画
			res = 'res/bq/4.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		action_face_5 = {    --表情动画
			res = 'res/bq/5.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		action_face_6 = {    --表情动画
			res = 'res/bq/6.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		action_face_7 = {    --表情动画
			res = 'res/bq/7.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		action_face_8 = {    --表情动画
			res = 'res/bq/8.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		action_face_9= {    --表情动画
			res = 'res/bq/9.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		action_face_10 = {    --表情动画
			res = 'res/bq/10.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		action_face_11 = {    --表情动画
			res = 'res/bq/11.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		action_face_12 = {    --表情动画
			res = 'res/bq/1.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		face_show_1 = { 		
			x = 62,
			y = 80,
			res = 'res/bq/bqan.png',
		},
		face_show_2 = { 		
			x = 413,
			y = 500,
			res = 'res/bq/bqan.png',
		},
		face_show_3 = { 		
			x = 240,
			y = 660,
			res = 'res/bq/bqan.png',
		},
		face_show_4 = { 		
			x = 68,
			y = 500,
			res = 'res/bq/bqan.png',
		},
		close_btn = {  --关闭按钮
			id = 'close_btn',
			to = 'face_bg',
			x = 192,
			y = 172,
			sx = 60,
			sy = 60,
			res = 'res/gb_off.png',
			on = 'res/gb_on.png',
		},	
	},
}

function getConfigData()
	return RoomFaceConfig
end