-- setpage
-- pos layout
local Config = {
	[LIGHT_UI.SCREEN_480_800] = {
		--[[ 按钮
		声音 震动 
		挑战权限 - 接受VIP用户挑战 接受视频认证用户挑战 接受其他用户挑战
		聊天权限 - 接受vip用户聊天 接受视频认证用户聊天 接受其他用户聊天
		]]--
		btn = {
			res = 'btn_off.png',
			on = 'btn_on.png',
		},		
		btn_1 = {
			x = 380,
			y = 689,
		},
		btn_2 = {
			x = 380,
			y = 629,
		},
		btn_3 = {
			x = 380,
			y = 509,
		},
		btn_4 = {
			x = 380,
			y = 449,
		},
		btn_5 = {
			x = 380,
			y = 389,
		},
		bg = {	--底图通用设置
			sx = 420,
		},
		bg_1 = {
			id = 'bg_1',
			to = 'head_bg',
			y = -110,
			row = 2,
			sy = 120,
		},
		bg_2 = {
			id = 'bg_2',
			to = 'bg_1',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -60,
			row = 3,
			sy = 180,
		},		
		-- 说明文字	
		label = {	-- label通用配置
			res = 'small_purewhite_bg.png',
			sy = 20,
			css = 'c3',
		},
		label_1 = {
			id = 'label_1',
			to = 'bg_1',
			x = -160,
			y = 30,
			sx = 80,
			text = '声音',
			text_css = 'd3',
		},
		label_2 = {
			id = 'label_2',
			to = 'label_1',
			y = -60,
			sx = 80,
			text = '震动',
			text_css = 'd3',
		},
		label_3 = {
			id = 'label_3',
			to = 'bg_2',
			ty = 1,
			tx = 0,
			ax = 0,
			ay = 0,
			y = 20,
			x = 50,
			text = '聊天权限',
			text_css = 'b3',
			res = 'small_white_bg.png',
		},
		label_4 = {
			id = 'label_4',
			to = 'bg_2',
			ax = 0,
			x = -100,
			y = 60,
			text = '接受VIP用户聊天',
			text_css = 'd3',
		},
		label_5 = {
			id = 'label_5',
			to = 'label_4',
			ax =0,
			x = 24,			
			y = -60,
			text = '接受视频认证用户聊天',
			text_css = 'd3',
		},
		label_6 = {
			id = 'label_6',
			to = 'label_4',
			ax =0,
			x = 0,			
			y = -120,
			text = '接受其他用户聊天',
			text_css = 'd3',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return Config
end
