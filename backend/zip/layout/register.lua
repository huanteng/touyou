-- reg panel
-- pos layout
local RegConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		rect = {	-- 背景框
			id = 'rect',
			to = 'bg_pic',
			y = 270,
			row = 2,
			sx = 420,
			sy = 120,
		},
		username = {
			id = 'username',
			--to = 'rect',--其他控件也不要依赖这个
			--tx = 0,
			--ty = 1,
			x = 40,
			y = 646,
			ax = 0,
			ay = 1,
			sx = 400,
			sy = 40,
			css = 'b3',
			text = '设定用户名：不超10个字符中文或英文',
		},
		password = {
			id = 'password',
			--to = 'username',--其他控件也不要依赖这个
			--tx = .5,
			--ty = .5,
			x = 40,
			y = 585,
			sx = 400,
			sy = 40,
			css = 'b3',
			text = '设定密码：大于3小于10位的数字或字母',
		},
		view = {
			to = 'rect',
			tx = .5,
			ty = .5,
			x = 0,
			y = -260,
			sx = 200,
			sy = 100,
			text = '显示密码',
			res = 'small_white_bg.png',
			on = 'small_white_bg.png',
		},
		btn = {
			id = 'btn',
			to = 'rect',
			tx = .5,
			ty = .5,
			x = 0,
			y = -240,
			sx = 400,
			sy = 59,
			css = 'green_btn',
			text = '完成',
		},
		
		tip_txt = {
			to = 'btn',
			ax = 0,
			tx = 0.5,
			y = 70,
			text = '性别设定后不可改变，请慎重选择',
			css = 'b9',
		},

	},
}
--------------------------------------------------------------

function getConfigData()
	return RegConfig 
end

