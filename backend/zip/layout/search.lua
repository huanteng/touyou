-- pos layout
local SearchConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		
		move_page_config = { -- 滑动页
			x = 0, -- 滑动页左下角点x
			y = 0, -- 滑动页左下角点y
			view_height = 668, -- 滑动页可视区域高度
			inner_y = 668, -- 滑动页内部列表的起始y坐标
		},
		
		search_rect = {
			id = 'search_rect',
			x = 11,
			y = 720,
			sx = 350,
			sy = 40,
			ax = 0,
			ay = 1,
			key = 'ic_single',
		},
		
		search_btn = {
			to = 'search_rect',
			x = 230,-- offset
			sx = 100,--拉伸
			sy = 40,
			css = 'blue_btn',
			text = '搜索',
		},
		tr = { -- 竖直列表的小格子配置
			split = 23, --每行间隔
		},
		
		logo = {
			id = 'logo',
			sx = 90,
			sy = 90,
			x = 55,
			y = -50,
		},
		online = {
			to = 'logo',
			tx = 1,
			ty = 0,
			x = -5,
			y = 5,
			res = 'user_online_pic.png',
		},
		name = {
			id = 'name',
			to = 'logo',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 15,
			css = 'c2',
		},
		vip = {
			to = 'name',
			tx = 1,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 12,
		},
		sex = {
			id = 'sex',
			to = 'name',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -8,
		},
		address = {
			id = 'address',
			to = 'sex',
			ax = 0,
			ay = 0,
			x = 22,
			y = -12,
			css = 'b2',
		},
		real = {
			to = 'address',
			tx = 1,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 13,
			res = 'vedioauth_pic.png',
		},
		mood = {
			id = 'mood',
			to = 'sex',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -10,
			sx = 358,
			sy = 1,
			css = 'b2',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return SearchConfig 
end

