-- pos layout
local RewardConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		move_page_config = { -- 滑动页
			x = 0, -- 滑动页左下角点x
			y = 0, -- 滑动页左下角点y
			view_height = 688, -- 滑动页可视区域高度
			inner_y = 663, -- 滑动页内部列表的起始y坐标
		},
		title_config = {
			y = 689,
			gap = 115,
			text = '',
			css = 'c1',
		},
		time = {
			id = 'time',
			ax = 0,
			ay = 0,
			x = 30,
			y = -10,
			text = '',
			css = 'c2',
		},
		name = {
			to = 'time',
			x = 160,
			ax = 0.5,
			text = '',
			css = 'c2',
		},
		desc = {
			to = 'time',
			x = 280,
			ax = 0,
			text = '',
			css = 'c2',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return RewardConfig 
end

