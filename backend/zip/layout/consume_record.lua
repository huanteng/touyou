-- pos layout
local ConsumeConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["move_page_config"] = { -- 滑动页
			["x"] = 0, -- 滑动页左下角点x
			["y"] = 0, -- 滑动页左下角点y
			["view_height"] = 698, -- 滑动页可视区域高度
			["inner_y"] = 688, -- 滑动页内部列表的起始y坐标
		},
		["move_hint_config"] = { -- 滑动页码显示控件
			["x"] = 0,
			["y"] = 718,
			["res"] = "tab_cursor_image.png", -- 滑动的条资源
		},
		["bottom_part_config"] = { -- 底部
			["money_x"] = 20, -- 底部金钱持有信息
			["money_y"] = 26,
		},
		["consume_rec_config"] = { -- 消费记录格子
			["time_x"] = 20, -- 时间
			["time_y"] = 0,
			["type_x"] = 210, -- 类型
			["type_y"] = 0,
			["change_x"] = 286, -- 变化
			["change_y"] = 0,
			["balance_x"] = 410, -- 剩余
			["balance_y"] = 0,
		},
		
		buy_btn = {
			x = 410,
			y = 40,
			sx = 100,
			sy = 50,
			css = 'blue_btn',
			text = '购买',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return ConsumeConfig 
end

