-- pos layout
local FriendConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		add_btn = { -- 添加好友
			to = 'head_bg',
			x = 200,
			res = 'addfriend_btn.png',
		},
		find_btn_config = { -- 顶部右上角按钮
			["x"] = 400,
			["y"] = 742,
			["normal_res"] = "addfriend_bt_bg.png",
			["click_res"] = "addfriend_bt_bg.png",
		},
		move_page_config = { -- 滑动页
			x = 0, -- 滑动页左下角点x
			y = 0, -- 滑动页左下角点y
			view_height = 690, -- 滑动页可视区域高度
			inner_y = 690, -- 滑动页内部列表的起始y坐标
		},
		move_hint_config = { -- 滑动页码显示控件
			x = 0,
			y = 720,
			res = 'tab_cursor_image.png', -- 滑动的条资源
		},
		tr = { -- 竖直列表的小格子配置
			split = 20, --每行间隔
		},
		
		logo = {
			id = 'logo',
			sx = 90,
			sy = 90,
			x = 55,
			y = -50,
		},
		online = {
			to = 'logo',
			tx = 1,
			ty = 0,
			x = -5,
			y = 5,
			res = 'user_online_pic.png',
		},
		name = {
			id = 'name',
			to = 'logo',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 15,
			y = 5,
			css = 'c1',
		},
		vip = {
			to = 'name',
			tx = 1,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 12,
		},
		sex = {
			id = 'sex',
			to = 'name',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -8,
		},
		address = {
			id = 'address',
			to = 'sex',
			ax = 0,
			ay = 0,
			x = 22,
			y = -12,
			css = 'b2',
		},
		real = {
			to = 'address',
			tx = 1,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 13,
			res = 'vedioauth_pic.png',
		},
		mood = {
			id = 'mood',
			to = 'sex',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -7,
			sx = 358,
			sy = 1,
			css = 'b2',
		},
		empty_tips = {
			sx = 400,
			sy = 10,
			x = 40,
			y = -30,
			css = 'c2',
			text = "哎呀~你还没有添加任何好友，这样玩骰会很寂寞哦，赶紧找个吧",
		},
		friend_btn = {
			x = 240,
			y = -150,
			sx = 400,
			sy = 50,
			css = 'blue_btn',
			text = '找朋友',
			text_css = 'c4',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return FriendConfig 
end

