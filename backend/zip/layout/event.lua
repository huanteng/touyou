-- event layout
local MainPageConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		ad1 = {
			x = 240,
			y = 620,
		},
		ad2 = {
			x = 240,
			y = 393,
		},
		ad3 = {
			x = 240,
			y = 161,
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return MainPageConfig 
end

