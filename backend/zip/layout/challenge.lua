-- pos layout
local ChaConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["style_btn_config"] = { -- 顶部右上角切换样式按钮
			["x"] = 408,
			["y"] = 740,
			["grid_res"] = "challengehall_list_bt.png",
			["list_res"] = "challengehall_grid_bt.png",
		},
		["move_page_config"] = { -- 滑动页
			["x"] = 0, -- 滑动页左下角点x
			["y"] = 0, -- 滑动页左下角点y
			["view_height"] = 698, -- 滑动页可视区域高度
			["inner_y"] = 688, -- 滑动页内部列表的起始y坐标
		},
		["move_hint_config"] = { -- 滑动页码显示控件
			["x"] = 0,
			["y"] = 713,
			["res"] = "tab_cursor_image.png", -- 滑动的条资源
		},
		["small_item_config"] = { -- 竖直列表的小格子配置
			["name_x"] = 110, -- 小格子名字
			["name_y"] = -5,
			["sex_pic_x"] = 110, -- 小格子性别
			["sex_pic_y"] = -35,
			["address_x"] = 152, -- 小格子地址
			["address_y"] = -38,
			["real_x"] = 215, -- 小格子视频认证
			["real_y"] = -34,
			["vip_x"] = 10, -- 跟用户名之间的间隔距离
			["vip_y"] = -4,
			["mood_x"] = 110, -- 小格子心情语言
			["mood_y"] = -71,
			["divider_x"] = 0, -- 小格子分隔条
			["divider_y"] = 0,
			["min_height_split"] = 25, --每行之间相隔的最小高度1
			["min_height_split2"] = 80, --每行之间相隔的最小高度2
			["vip_1_pic"] = "vip1.png", --vip1的图
			["vip_2_pic"] = "vip2.png", --vip2的图
			["vip_3_pic"] = "vip3.png", --vip3的图
		},
		["big_item_config"] = { -- 大照片列表格子配置
			["real_x"] = 70,
			["real_y"] = -135,
			["vip_x"] = 112,
			["vip_y"] = -8,
		},
		logo1 = {
			id = 'logo',
			x = 82,
			y = -84,
			sx = 152,
			sy = 152,
		},
		logo2 = {
			id = 'logo',
			sx = 90,
			sy = 90,
			x = 55,
			y = -53,
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return ChaConfig 
end

