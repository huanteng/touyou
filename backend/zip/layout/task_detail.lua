-- pos layout
local TaskConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["big_item_config"] = { -- 大照片列表格子配置
			["real_x"] = 35,
			["real_y"] = -96,
			["vip_x"] = 71,
			["vip_y"] = 0,
		},
		
		name = {	-- 任务名称
			id = 'name',
			x = 30,
			y = 690,
			ax = 0,
			ay = 0,
			css = 'd2',
		},
		
		introduce = {	
			id = 'introduce',
			to = 'name',
			tx = 0,
			ty = 0,
			y = -20,
			ax = 0,
			ay = 1,
			sx = 410,
			sy = 1,
			css = 'b2',
		},
		
		btn = {
            to = 'introduce',			
			x = 0,
			y = -100,
			sx = 400,
			sy = 50,
			css = 'blue_btn',
			text = '',
			text_css = 'c9',
			text = '',
			text_css = 'c4',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return TaskConfig 
end

