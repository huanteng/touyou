-- pos layout
local CityConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["select_config"] = {
			["x"] = 135,
			["y"] = 687,
		},
		["reply_btn_config"] = { -- 顶部右上角切换样式按钮
			["x"] = 405,
			["y"] = 738,
			["normal_res"] = "oktosend.png",
			["click_res"] = "oktosend.png",
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return CityConfig 
end

