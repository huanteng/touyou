-- pos layout
local PanelContainerConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		
		title_text = {	-- 
			id = 'title',
			x = 0,
			y = 770,
			ax = 0,
			ay = 0.5,					
			text = '骰友',
			css = 'e4',
		},
		
		["bg_config"] = {		--容器的标题栏背景
			["x"] = 240,
			["y"] = 763,	
			["sx"] = 480,
			["sy"] = 75,				
			["res"] = 'head.png',
		},
		
		buy_btn = {
			x = 310,
			y = 40,
			sx = 100,
			sy = 50,
			css = 'blue_btn',
			text = '免费获取',
		},
		
		["v_line"] = {
			["x"] = -1,
			["y"] = 693,	
			["sx"] = 2,
			["sy"] = 1540,				
			["res"] = 'x-s.png',
		},
		
		bg_pic = { -- 背景图 左上角对齐				
			x = 240,
			y = 367,
			res = 'white_dot.png',
			sx = 480,
			sy = 737,
		},		
		
		panel_container_property = {		--面板容器的属性
			layer_pos_x = 100,
			speed = 120,	
			right_difference = 75,		--距离右边的长度	
			screen_width = 480,
			screen_height = 800,	
		},
		
		scale_container_btn = {		--伸缩容器按钮
			x = 0,
			y = 735,
			res = "cd.png",
			on = "cd-on.png",
		},
		
		msg_style_pic = {		--消息底图
			x = 55,
			y = 775,
		},
		
		msg_style_num = {		--消息数字
			x = 55,
			y = 775,
		},
		
		["music_sound"] = { -- 发送消息的声音
			sound = 'sound/message.mp3',
			ms = 1000,
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return PanelContainerConfig 
end