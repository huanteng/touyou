-- setpage
-- pos layout
local Config = {
	[LIGHT_UI.SCREEN_480_800] = {
		bg = {	--底图通用设置
			sx = 420,
		},
		bg_1 = {
			id = 'bg_1',
			to = 'head_bg',
			y = -155,
			row = 2,
			sy = 120,
		},
		bg_2 = {
			id = 'bg_2',
			to = 'bg_1',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -150,
			row = 1,
			sy = 50,
		},
		
		label = {	-- label通用配置
			res = 'small_purewhite_bg.png',
			sy = 20,
			css = 'c3',
		},
		label_1 = {
			id = 'label_1',
			to = 'bg_1',
			tx = 0,
			ty = 1,
			ax = 0,
			ay = 0,
			y = 13,
			x = 4,
			text = '修改密码',
			css = 'b3',
		},
		label_2 = {
			id = 'label_2',
			to = 'bg_1',
			tx = 0,
			ty = 1,
			ax = 0,
			ay = 0,
			x = 10,
			y = -45,
			text = '旧密码：',
			css = 'c3',
		},

		label_3 = {
			id = 'label_3',
			to = 'label_2',
			y = -60,
			text = '新密码：',
			css = 'c3',
		},
		label_4 = {
			id = 'label_4',
			to = 'label_2',
			tx = 0,
			ty = 1,
			ax = 0,
			ay = 0,
			y = -170,
			x = -6,
			text = '邮箱验证',
			css = 'b3',
		},
		label_5 = {
			id = 'label_5',
			to = 'label_4',
			tx = 0,
			ty = 1,
			ax = 0,
			ay = 1,
			y = -35,
			sx = 415,
			sy = 1,
			text = '邮箱是找回密码的凭证，验证邮箱才能实现重置密码，更好地保护你的帐号。',
			css = 'b9',
		},
		label_6 = {
			id = 'label_6',
			to = 'bg_2',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			x = 10,
			y = 38,
			text = '邮箱：',
		},
		
		
		-- 输入框
		old_pass = {
			to = 'label_2',
			tx = 1,
			ty = 0,
			ax = 0,
			ay = 0.5,
			x = 0,
			y = -68,
			text = '',
			css = 'd1',
		},
		new_pass = {
			to = 'label_3',
			tx = 1,
			ty = 0,
			ax = 0,
			ay = 0.5,
			x = 0,
			y = -68,
			text = '',
			css = 'd1',	
		},
		email = {
			to = 'label_6',
			tx = 1,
			ty =0,
			ax = 0,
			ay = 1,
			x = 0,
			y = -75,
			text = '',
			css = 'd1',	
		},
			
	
		
		-- 按钮
		pass_btn = {
			to = 'label_1',
			tx = 1,
			ty = 0,
			ax = 0.5,
			ay = 0.5,
			x = 290,
			y = 12,
			sx = 80,
			sy = 35,
			css = 'green_btn',
			text = '确定',
			text_css = 'c4',
		},
		email_btn = {
			to = 'label_4',
			tx = 1,
			ty = 0,
			ax = 0.5,
			ay = 0.5,
			x = 290,
			y = 12,
			sx = 80,
			sy = 35,
			css = 'green_btn',
			text = '确定',
			text_css = 'c4',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return Config
end
