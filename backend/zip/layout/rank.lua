-- pos layout
local RankConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["move_page_config"] = { -- 滑动页
			["x"] = 0, -- 滑动页左下角点x
			["y"] = 0, -- 滑动页左下角点y
			["view_height"] = 690, -- 滑动页可视区域高度
			["inner_y"] = 690, -- 滑动页内部列表的起始y坐标			
		},
		["move_hint_config"] = { -- 滑动页码显示控件
			["x"] = 0,
			["y"] = 720,
			["res"] = "tab_cursor_image.png", -- 滑动的条资源
			["text_size"] = 22, -- 文字大小
		},
		
		logo = {
			id = 'logo',
			sx = 90,
			sy = 90,
			x = 58,
			y = -54,
		},
		online = {
			to = 'logo',
			tx = 1,
			ty = 0,
			x = -5,
			y = 5,
			res = 'user_online_pic.png',
		},
		rank_bg = {
			id = 'rank_bg',
			to = 'logo',
			tx = 0,
			ty = 1,
			x = -1,
			y = -7,
		},
		rank = {
			to = 'rank_bg',
			x = 0,
			y = 6,
		},
		name = {
			id = 'name',
			to = 'logo',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 13,
			y = -5,
			css = 'c1',
		},
		win_hint = {
			to = 'name',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -30,
			css = 'c2',
		},
		gold_pic = {
			id = 'gold_pic',
			to = 'logo',
			ax = 1,
			x = 320,
			res = 'gold.png',
		},
		win_gold = {
			to = 'gold_pic',
			ax = 0,
			x = 22,	
			text = '',
			css = 'c2',		
		},
		
	},
}
--------------------------------------------------------------

function getConfigData()
	return RankConfig 
end

