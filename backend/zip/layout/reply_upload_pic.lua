-- pos layout
local UploadConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		send_btn = {
			to = 'head_bg',
			x = 200,
			res = 'oktosend.png',
		},
		
		take_btn = {
			id = 'take_btn',
			to = 'head_bg',
			x = -100,
			y = -60,
			sx = 140,
			sy = 40,
			css = 'blue_btn',
			text = '拍照上传',
		},
		
		file_btn = {
			to = 'take_btn',
			x = 200,
			sx = 140,
			sy = 40,
			css = 'blue_btn',
			text = '相册上传',
		},
		
		
	},
}
--------------------------------------------------------------

function getConfigData()
	return UploadConfig 
end

