-- pos layout
local newGameHallConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		
		title_text = {	-- 
			id = 'title',
			x = 0,
			y = 770,						
			text = '骰友',
			css = 'c3',
		},
		
		["bg_config"] = {		--背景
			["x"] = 0,
			["y"] = 0,
			[ "ax" ] = 0,
			[ "ay" ] = 0,	
			["sx"] = 480,
			["sy"] = 800,				
			["res"] = 'di.png',
		},	
		
		["select_config"] = { -- 选中框		
			["x"] = 250,
			["y"] = 677,	
			["sx"] = 403,
			["sy"] = 60,				
			["res"] = 'xz-on.png',
		},			
		
		["good_friend"] = { --  好友		
			res = 'hy.png',
		},
		
		["good_friend_rect"] = { -- 好友		
			res = 'goodfriend_clickrect.png',
			on = 'xz-on.png',
		},
		
		["chat"] = { -- 说两句		
			res = 'slj.png',
		},
		
		["chat_rect"] = { -- 说两句		
			res = 'chat_clickrect.png',
			on = 'xz-on.png',
		},
		
		["dynamic"] = { -- 动态		
			res = 'dt.png',
		},
		
		["dynamic_rect"] = { -- 动态		
			res = 'dynamic_clickrect.png',
			on = 'xz-on.png',
		},
		
		["discuss"] = { -- 评头论足		
			res = 'ptlz.png',
		},
		
		["discuss_rect"] = { -- 评头论足		
			res = 'discuss_clickrect.png',
			on = 'xz-on.png',
		},
		
		["meetByChance"] = { -- 偶遇		
			res = 'oy.png',
		},
		
		["meetByChance_rect"] = { -- 偶遇		
			res = 'meetbychance_clickrect.png',
			on = 'xz-on.png',
		},
		
		["goodMall"] = { -- 道具商城
			res = 'djsc.png',
		},
		
		["goodMall_rect"] = { -- 道具商城
			res = 'goodmall_clickrect.png',
			on = 'xz-on.png',
		},
		
		["getGold"] = { -- 获得金币		
			res = 'hdjb.png',
		},
		
		["getGold_rect"] = { -- 获得金币		
			res = 'getgold_clickrect.png',
			on = 'xz-on.png',
		},
		
		["luckyTurnplate"] = { -- 幸运转盘	
			res = 'xyzp.png',
		},
		
		["luckyTurnplate_rect"] = { -- 幸运转盘	
			res = 'luckyplate_clickrect.png',
			on = 'xz-on.png',
		},
		
		["game"] = { -- 游戏		
			res = 'yx.png',
		},
		
		["game_rect"] = { -- 游戏		
			res = 'gamehall_clickrect.png',
			on = 'xz-on.png',
		},
		
		["message"] = { -- 消息		
			res = 'xx.png',
		},
		
		["message_rect"] = { -- 消息		
			res = 'message_clickrect.png',
			on = 'xz-on.png',
		},
		
		setting_button = {	-- 设置按钮
			x = 345,
			y = 738,
			sx = 110,
			sy = 110,		
			res = 'sz-off.png',
			on = 'sz-on.png',
		},
		
		[ "user_button" ] = {	-- 用户头像按钮
			[ "id" ] = 'userBtn',
			[ "x" ] = 65,
			[ "y" ] = 740,			
			[ "res" ] = 'tx.png',
			[ "on" ] = 'tx.png',
		},
		
		user_name = {	-- 用户名字
			to = 'userBtn',
			x = 60,
			y = 20,
			ax = 0,
			ay = 0.5,
			css = 'e4',			
		},
		
		["gold_pic"] = {		--背景
			[ "id" ] = 'gold_pic',
			[ "to" ] = 'userBtn',
			["x"] = 75,
			["y"] = -30,				
			["res"] = 'gold.png',
		},
		
		gold_text = {	-- 金币数量
			to = 'gold_pic',
			x = 30,
			y = 2,
			css = 'd4',
			ax = 0,
			ay = 0.5,
			--size = '18',
		},		
		
		_user_pic = {		--用户头像
			x = 65,
			y = 740,
			sx = 100,
			sy = 100,
		},	
		
		game_list_item = {		--游戏栏目
			x = 0,
			y = 589,
			text = "",
		},
		
		makefriend_list_item = {		--交友栏目
			x = 0,
			y = 428,
			text = "交友",
			--css = 'c4',
		},
		
		riches_list_item = {		--财富栏目
			x = 0,
			y = 102,
			text = "财富",
			--text = "交友",
		},
		
		h_line = {		--横线
			x = 203,
			y = 671,
			--sx = 1,
			--sy = 2,
			res = 'hx_03.png',
		},
		
		online_num = {		--在线人数
			x = 398,
			y = 645,
			ax = 1,
			ay = 0.5,			
			text = "数据加载中...",
			css = 'c4',
		},
		
		new_pic1 = {		--消息条数
			to = 'msg_purper_bg',
			x = 0,
			y = 1,
			ax = 0.5,
			ay = 0.5,			
			text = "+100",
			css = 'a4',
		},
		
		new_pic2 = {		--评头论足条数
			to = 'discuss_purper_bg',
			x = 25,
			y = 1,
			ax = 1,
			ay = 0.5,			
			text = "+100",
			css = 'a4',
		},
		
		new_pic3 = {		--偶遇条数
			to = 'meet_by_chance_purper_bg',
			x = 25,
			y = 1,
			ax = 1,
			ay = 0.5,			
			text = "+100",
			css = 'a4',
		},
		
		new_user1 = {		--新用户
			to = 'chat_purper_bg',
			x = 24,
			y = 1,
			ax = 1,
			ay = 0.5,			
			text = "NEW",
			css = 'a4',
		},
		
		new_user2 = {		--新用户
			to = 'dynamic_purper_bg',
			x = 24,
			y = 1,
			ax = 1,
			ay = 0.5,			
			text = "NEW",
			css = 'a4',
		},
		
		msg_purper_bg = {		--消息紫色背景
			id = "msg_purper_bg",
			x = 370,
			y = 586,			
			res = 'bq.png',
		},
		
		chat_purper_bg = {		--说两句紫色背景
			id = "chat_purper_bg",
			x = 370,
			y = 425,			
			res = 'bq.png',
		},
		
		dynamic_purper_bg = {		--动态紫色背景
			id = "dynamic_purper_bg",
			x = 370,
			y = 370,			
			res = 'bq.png',
		},
		
		discuss_purper_bg = {		--评头论足紫色背景
			id = "discuss_purper_bg",
			x = 370,
			y = 321,			
			res = 'bq.png',
		},
		
		meet_by_chance_purper_bg = {		--偶遇紫色背景
			id = "meet_by_chance_purper_bg",
			x = 370,
			y = 261,			
			res = 'bq.png',
		},
		
		white_line_up = {		--白线，用来盖在下面那2条线上面的
			x=0,
			y=232,
			ax=0,
			ay=0.5,
			res='white_line.png',
		},
		
		white_line_down = {		--白线，用来盖在下面那2条线上面的
			x=0,
			y=184,
			ax=0,
			ay=0.5,
			res='white_line.png',
		},
		
		hot_bg = {		--热度背景
			x = 208,
			y = 643,
			ax = 1,
			ay = 0.5,			
			res = 'hot.png',
		},
		
		hot_animation = {		--热度动画		
			x = 0, 
			y = 0,
			width = 63, 
			height = 33, 
			count = 2, 
			across = 1, 
			row = 2, 
			res = 'hot.png', 
			time = 0.1,
		},

	},
}
--------------------------------------------------------------

function getConfigData()
	return newGameHallConfig 
end