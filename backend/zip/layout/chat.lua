-- pos layout
local ChatConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["say_btn_config"] = { -- 顶部右上角按钮
			["x"] = 415,
			["y"] = 742,
			["normal_res"] = "send_say.png",
			["click_res"] = "send_say.png",
		},
		["move_page_config"] = { -- 滑动页
			["x"] = 0, -- 滑动页左下角点x
			["y"] = 0, -- 滑动页左下角点y
			["view_height"] = 698, -- 滑动页可视区域高度
			["inner_y"] = 688, -- 滑动页内部列表的起始y坐标
		},
		["move_hint_config"] = { -- 滑动页码显示控件
			["x"] = 0,
			["y"] = 718,
			["res"] = "tab_cursor_image.png", -- 滑动的条资源
			["text_size"] = 22, -- 文字大小
		},
		["say_item_config"] = { -- 竖直列表的小格子配置
			["photo_w"] = 120, -- 中间附件图宽
			["photo_h"] = 120, -- 中间附件图高
			["frame_buttom"] = 10, --大边框离最下的距离
			["frame_buttom2"] = -8, --大边框离最下的距离2
			["reply_pic_margin"] = 15, --回复数图标与中间附件图的间隔距离
		},
		logo_btn_c = { --logo按钮
			id = 'logo_btn',
			to = 'xyz',
			tx = 0,
			ty = 1,
			ax = 0,
			ay = 1,
			sx = 90,
			sy = 90,
			x = 10,
			y = -5,
			res = 'portrait2.png',
		},
		logo_pic_c = { --logo
			id = 'logo_pic',
			to = 'logo_btn',
			tx = 0,
			ty = 1,
			ax = 0,
			ay = 1,
			sx = 90,
			sy = 90,
			x = 0,
			y = 0,
			res = '',
		},
		frame_bg_c = { --右边内容大框的底按钮
			id = 'frame_bg',
			to = 'logo_pic',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			sx = 343,
			sy = 0,
			x = 20,
			y = -2,
			text = ' ',
			text_css = 'c2',			
			res = 'tiny_bg_white_pixel.png',
			on = 'tiny_yellow_pixel.png',
		},
		text_frame_c = { --右边内容大框
			id = 'frame_btn',
			to = 'logo_pic',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			sx = 360,
			sy = 0,
			x = 5,
			y = 0,
			key = 'say_content',
		},
		username_c = { --用户名
			id = 'username',
			to = 'frame_btn',
			tx = 0,
			ty = 1,
			x = 25,
			y = -10,
			ax = 0,
			ay = 1,
			text = '',
			css = 'c1',
		},
		vip_c = { --vip图
			to = 'username',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 10,
			y = -3,
			res = '',
		},
		say_time_c = { --说两句时间
			id = 'say_time',
			to = 'frame_btn',
			tx = 1,
			ty = 1,
			x = -10,
			y = -10,
			ax = 1,
			ay = 1,
			text = '',
			css = 'a3',
		},
		sex_pic_c = { --用户性别图
			id = 'sex_pic',
			to = 'username',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -10,
			res = '',
		},
		video_auth_c = { --视频验证图
			id = 'video_auth',
			to = 'sex_pic',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 10,
			res = '',
		},
		say_content_c = { --说两句内容
			id = 'say_content',
			to = 'sex_pic',
			tx = 0,
			ty = 0,
			x = 0,
			y = -10,
			ax = 0,
			ay = 1,
			sx = 330,
			sy = 1,
			text = '',
			css = 'c2',
		},
		say_pic_c = { --说两句附图
			id = 'say_pic',
			to = 'say_content',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			sx = 120,
			sy = 120,
			x = 0,
			y = -15,
			res = '',
		},
		reply_pic_c = { --回复数图标
			id = "reply_pic",
			to = 'say_time',
			tx = 1,
			ty = 0,
			x = 0,
			y = 20,
			ax = 1,
			ay = 1,
			res = "saysum_pic.png",
		},	
		reply_txt_c = { --回复数文字
			to = 'reply_pic',
			tx = 0,
			ty = 0,
			x = -5,
			y = 0,
			ax = 1,
			ay = 0,
			text = '',
			css = 'a3',
		},		
	},
}
--------------------------------------------------------------

function getConfigData()
	return ChatConfig 
end

