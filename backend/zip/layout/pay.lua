-- pos layout
local PayConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		
		["style_btn_config"] = { -- 顶部右上角切换样式按钮
			["x"] = 405,
			["y"] = 748,
			["grid_res"] = "challengehall_grid_bt.png",
			["list_res"] = "challengehall_list_bt.png",
		},
		["move_page_config"] = { -- 滑动页
			["x"] = 0, -- 滑动页左下角点x
			["y"] = 0, -- 滑动页左下角点y
			["view_height"] = 698, -- 滑动页可视区域高度
			["inner_y"] = 688, -- 滑动页内部列表的起始y坐标
		},
		["move_hint_config"] = { -- 滑动页码显示控件
			["x"] = 0,
			["y"] = 718,
			["res"] = "tab_cursor_image.png", -- 滑动的条资源
		},
		["small_item_config"] = { -- 竖直列表的小格子配置
			["name_x"] = 84, -- 小格子名字
			["name_y"] = 0,
			["sex_pic_x"] = 66, -- 小格子性别
			["sex_pic_y"] = -30,
			["address_x"] = 101, -- 小格子地址
			["address_y"] = -35,
			["real_x"] = 196, -- 小格子视频认证
			["real_y"] = -28,
			["vip_x"] = 160, -- 小格子vip
			["vip_y"] = 0,
			["mood_x"] = 100, -- 小格子心情语言
			["mood_y"] = -65,
			["divider_x"] = -5, -- 小格子分隔条
			["divider_y"] = -110,
		},
		["big_item_config"] = { -- 大照片列表格子配置
			["real_x"] = 35,
			["real_y"] = -96,
			["vip_x"] = 71,
			["vip_y"] = 0,
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return PayConfig 
end

