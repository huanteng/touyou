-- question_select_layout
local HobbyConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		btn = {
			to = 'head_bg',
			x = 180,
			res = 'oktosend.png',
		},
		content = {
			to = 'head_bg',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			x = 10,
			y = -20,
			sx = 448,
			sy = 1,
			text = '',
			css = 'c2',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return HobbyConfig 
end

