-- one to one chat layout
local OneToOneChatConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		sendbtn = {
			x = 310,
			y = -87,
			sx = 40,
			sy = 1,
			res = 'btn_default.png',
			on = 'btn_default.png',
		},
		input_bg = {
			id = 'input_bg',
			x = 183,
			y = -67,
			row = 1,
			sx = 340,
			sy = 45,
		},			
		send_btn = {
			to = 'input_bg',
			x = 57,
			sx = 400,
			sy = 45,
			css = 'blue_btn',
			text = '点我聊天',
			text_css = 'c4',
		},
		say_btn_config = { -- 顶部右上角按钮
			x = 415,
			y = 742,
			normal_res = 'send_say.png',
			click_res = 'send_say.png',
		},
		send_page_config = { -- 滑动页
			x = 0, -- 滑动页左下角点x
			y = 100, -- 滑动页左下角点y
		},
		move_page_config = { -- 滑动页
			x = 0, -- 滑动页左下角点x
			y = 65, -- 滑动页左下角点y
			view_height = 675, -- 滑动页可视区域高度
			inner_y = 675, -- 滑动页内部列表的起始y坐标
		},
		move_hint_config = { -- 滑动页码显示控件
			x = 0,
			y = 675,
			res = 'tab_cursor_image.png', -- 滑动的条资源
			text_size = 22, -- 文字大小
		},
		say_item_config = { -- 竖直列表的小格子配置
			pic_w = 90, -- logo宽
			pic_h = 90, -- logo高
			pic_x = 10, -- logo离左边的距离
			pic_y = -5, -- logo离上边的距离
			name_x = 10, -- 用户名离大边框左边的距离
			name_y = 10, -- 用户名离最上的距离
			name_size = 22, -- 用户名字体大小
			sex_pic_y = 8, -- 性别图离用户名的距离
			say_x = 10, -- 正文离大边框左或大边框右的距离
			say_y = 8, -- 正文离性别图的距离
			time_x = 10, -- 时间离大边框右边的距离
			time_y = 10, -- 时间离大边框上边的距离
			time_size = 16, -- 时间字体大小
			photo_w = 120, -- 中间附件图宽
			photo_h = 120, -- 中间附件图高
			photo_split = 10, -- 中间附件图离正文的距离
			reply_x = 5, -- 回复数的字离回复数的图的距离
			reply_y = 10, -- 回复数的字和图所在的行离大边框下边的距离
			reply_size = 16, -- 回复数的字体大小
			reply_pic = 'saysum_pic.png', -- 回复数的图片
			reply_pic_x = 10, -- 回复数的图离大边框右边的距离
			video_auth_x = 5, --视频认证离性别图的距离
			video_auth_file = 'vedioauth_pic.png',
			vip_x_split = 5, --vip标志离用户名的间隔
			frame_buttom = 30, --大边框离最下的距离
			frame_right_split = 10, --大边框距离最右的距离
			vip_1_pic = 'vip1.png', --vip1的图
			vip_2_pic = 'vip2.png', --vip2的图
			vip_3_pic = 'vip3.png', --vip3的图
		},
		chat_desc_info = {   --聊天列表元素属性
			item_width = 480,
			item_height = 125,
			column_cnt = 1,
			x_space = 0,
			y_space = 0,
			chat_send_height =40,  --输入框高度						
		},
		tr = { -- 竖直列表的小格子配置
			split = 24, --每行间隔
		},			
		logo = {
			id = 'logo',
			sx = 90,
			sy = 90,
			x = 55,
			y = -55,
		},
		online = {
			to = 'logo',
			tx = 1,
			ty = 0,
			x = -5,
			y = 5,
			res = 'user_online_pic.png',
		},
		name = {
			id = 'name',
			to = 'logo',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 15,
			css = 'c2',
		},			
		sex = {
			id = 'sex',
			to = 'name',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -8,
		},			
		content = {
			id = 'content',
			to = 'sex',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -10,
			sx = 358,
			sy = 1,
			css = 'b2',
		},	
		time = {
			id = 'time',
			to = 'logo',
			ax = 1,
			ay = 0,
			x = 410,
			y = 20,
			css = 'a3',
		},
		--[[message = {
			id = 'message',
			to = 'input_bg',
			x = -155,
			y = -15,
			sx = 290,
			sy = 30,
			css = 'd1',
		},--]]
		message = {
			id = 'message',
			to = 'input_bg',
			tx = 0,
			ty = 0,
			x = 170,
			y = 21,
			sx = 350,
			sy = 48,			
			css = 'ic_single',
		},
		input_label = {
			id = 'input_label',
			to = 'input_bg',
			tx = 1,
			ty = 1,
			sx = 330,
			sy = 45,
			x = -170,
			y = -25,
			css = 'c2',		
			text = '设置输入字数限制测试一下而没其他意思而已',			
		},
		bg = {
		    id = 'bg',
			ax = 0,
			ay = 0,
			x = 0,
			y = -100,
			sx = 480,
			sy = 65,
			res = 'mmfooter_bg.png',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return OneToOneChatConfig
end