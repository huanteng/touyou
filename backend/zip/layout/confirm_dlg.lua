-- pos layout
local LuckyConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["confirm_config"] = {
			["x"] = 92,
			["y"] = 490,
		},
		["bg_config"] = {
			["x"] = 0,
			["y"] = 0,
			["res"] = "small_white_bg.png",
			["sx"] = 31,
			["sy"] = 19,
		},
		["top_pic_config"] = {
			["x"] = 0,
			["y"] = 0,
			["sx"] = 312,
			["sy"] = 0.8,
			["res"] = "window_title_bg.png",
		},
		["ok_btn_config"] = {
			["x"] = 20,
			["y"] = -179,
			["sx"] = 95,
			["sy"] = 1,
			["normal_res"] = "btn_default.png",
			["click_res"] = "btn_default.png",
		},
		["cancel_btn_config"] = {
			["x"] = 182,
			["y"] = -179,
			["sx"] = 95,
			["sy"] = 1,
			["normal_res"] = "btn_default.png",
			["click_res"] = "btn_default.png",
		},
		["top_title_config"] = { -- 顶部标题文字
			["x"] = 20,
			["y"] = -15,
		},
		["hint_txt_config"] = { -- 顶部标题文字
			["x"] = 20,
			["y"] = -70,
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return LuckyConfig 
end

