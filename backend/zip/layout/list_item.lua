-- pos layout
local listItemConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
			
		list_title_text = {	--栏目标题名字 		
			x = 43,
			y = 106,						
			text = '骰友',
			css = 'c2',
		},
		
		h_line_up = {	--栏目标题上面的横线 		
			x = 203,
			y = 130,			
			--sx = 403,
			--sy = 2,						
			res = 'hx_03.png',
		},
		
		h_line_down = {	--栏目标题下面的横线 		
			x = 203,
			y = 82,
			--sx = 480,
			--sy = 2,						
			res = 'hx_03.png',
		},
		
		click_target_offset_x = {	--栏目标题下面的横线 		
			x = 140,
		},
		
		btn_bg = {		--栏目按钮背景
			offset_x = 100,
			offset_y = 50,
			space_y = 55,
		},
		
		btn_rect = {		--栏目按钮点击范围
			offset_x = 110 + 94,
			offset_y = 53,
			space_y = 55,
			scale_x = 403,
			scale_y = 54,
		},			

	},
}
--------------------------------------------------------------

function getConfigData()
	return listItemConfig 
end