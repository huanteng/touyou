local MainBankPageConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["move_page_config"] = { -- 滑动页
			["x"] = 0, -- 滑动页左下角点x
			["y"] = -10, -- 滑动页左下角点y
			["view_height"] = 738, -- 滑动页可视区域高度
			["inner_y"] = 728, -- 滑动页内部列表的起始y坐标
		},
		pay_info = {
			count = 2,
		},
		pay_text = {
			id = 'pay_text',
			ax = 0,
			x = 20,
			y = 595,
			css = 'b2',
			text2 = "其他支付方式",
			text = "选择支付方式",
		},			
		pay1 = {
			x = 240,
			y = 525,
			res = 'alipay_icon.png',				
		},
		pay2 = {
			id = 'pay2',
			x = 240,
			y = 415,
			res = 'shengpay_icon.png',						
		},	
		pay_tips = {
			id = 'pay_tips',
			to = 'pay2',
			y = -75,
			css = 'a2',
			text = "盛付通支持联通/电信/移动手机充值卡充值\n使用此方式时，请务必选择正确充值面额",
		},	
		logo = {
			id = 'logo',
			x = 60,
			y = 680,
			sx = 90,
			sy = 90,
		},	
		name = {    --产品名称
			id = 'name',
			to = 'logo',
			tx = 1,
			ax = 0,
			x = 16,
			y = 30,
			css = 'c2',
		},
		count = {   --产品数量
			id = 'count',
			to = 'name',
			ax = 0,
			tx = 0,
			x = 0,
			y = -30,
			css = 'b3',
		},
		money = {   --支付金钱
			id = 'money',
			to = 'count',
			ax = 0,
			tx = 0,
			x = 0,
			y = -30,
			css = 'b9',
		},
		gold_dicretion_bg = {		--元宝说明背景
			id = 'gold_dicretion_bg',
			to = 'money',
			x = 119,
			y = -80,
			sx = 480,
			sy = 100,
			res = 'tiny_bg_white_pixel.png',
		},
		can_pay_gold_text = {		--能够支付元宝文本
			id = 'can_pay_gold_text',
			to = 'gold_dicretion_bg',
			x = -110,
			y = 25,
			css = 'b2',
			text = '账户可支付元宝：',
		},
		gold_pay_dicretion_text = {		--元宝支付说明文本
			id = 'gold_pay_dicretion_text',
			to = 'gold_dicretion_bg',
			y = -15,
			css = 'b9',
			text = '您的余额不足当前支付请选择其它支付方式',
		},
		confirm_pay = {		--确认支付按钮
			id = 'confirm_pay',
			to = 'gold_dicretion_bg',			
			y = -18,
			sx = 240,
			sy = 40,
			css = 'blue_btn',
			text = '确认支付',
		},
		
		contentMoveDown = {		--需要往下移的显示对象的下移距离
			distance = 120,
		},
	},
}	

function getConfigData()
	return MainBankPageConfig 
end