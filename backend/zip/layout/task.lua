-- pos layout
local TaskConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["move_page_config"] = { -- 滑动页
			["x"] = 0, -- 滑动页左下角点x
			["y"] = 0, -- 滑动页左下角点y
			["view_height"] = 735, -- 滑动页可视区域高度
			["inner_y"] = 735, -- 滑动页内部列表的起始y坐标
		},
		
		logo = {
			id = 'logo',
			x = 40,
			y = -45,
			sx = 59,
			sy = 24,
		},
		
		name = {	-- 任务名称
			id = 'name',
			to = 'logo',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 15,
			y = 20,
			css = 'c1',
		},
		
		introduce = {
			to = 'name',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -25,
			sx = 280,
			sy = 20,
			css = 'c2',
		},
		
		btn = {
			to = 'logo',
			x = 385,
			y = 0,
			sx = 80,
			sy = 40,
			css = 'blue_btn',
			text_css = 'b4',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return TaskConfig 
end

