-- user package layout
local PackConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		mall = {
			to = 'head_bg',
			x = 200,
			res = 'market_bt_bg.png',
		},
		["move_page_config"] = { -- 滑动页
			["x"] = 0, -- 滑动页左下角点x
			["y"] = 0, -- 滑动页左下角点y
			["view_height"] = 738, -- 滑动页可视区域高度
			["inner_y"] = 730, -- 滑动页内部列表的起始y坐标
		},
		logo = {
			id = 'logo',
			sx = 72,
			sy = 72,
			x = 50,
			y = -53,
		},
		name = {
			id = 'name',
			to = 'logo',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 15,
			y = 12,
			css = 'c1',
		},
		
		desc = {
			id = 'desc',
			to = 'name',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -8,
			sx = 355,
			sy = 1,
			css = 'b2',
		},
		day = {
			id = 'day',
		    to = 'name',
			tx = 0,
			ax = 0,
			x = 0,
			y = -68,
			css = 'a3',
		},
		count = {
			to = 'name',
			ax = 1,
			tx = 0,
			x = 353,
			y = 0,
			css = 'b9',
		},			
		alert = {
			bg = { --黑色背景
				id = 'bg_btn',
				ax = 0.5,
				ay = 0.5,						
				x = 240,
				y = 400,
				sx = 480,
				sy = 800,
				res = 'res/bj.png',				
			},
			window = { --窗口背景
				id = 'window',
				ax = 0.5,
				ay = 0.5,
				x = 240,
				y = 400,
				res = 'hintbg.png',
			},
			title = { --标题
				to = 'window',
				tx = 0,
				ty = 1,
				ax = 0,
				ay = 0.5,
				x = 15,
				y = -26,
				css = 'c4',
				text = '　提示',
			},
			content = { --内容
				to = 'window',	
				ax = 0.5,
				ay = 0.5,
				x =0,
				y = 30,
				css = 'c3',
				text = '确定使用',
			},
			content2 = { --内容  道具名
				to = 'window',	
				ax = 0.5,
				ay = 0.5,
				x = 0,
				y = 0,
				css = 'c9',
				text = '',
			},
			icon = { -- icon
			    to = 'window',
				x = -50,
				y = 10,
				res = 'res/icon.png',
			},
			ok_btn = { --确定按钮
				to = 'window',
				ax = 0.5,
				ay = 0.5,
				tx = 0,
				ty = 0,
				x = 120,
				y = 50,
				sx = 140,
				sy = 45,
				css = 'blue_btn',
				text = '确 定',
				text_css = 'c4',
			},
			cancel_btn = { --取消按钮
				to = 'window',
				ax = 0.5,
				ay = 0.5,
				tx = 0,
				ty = 0,
				x = 310,
				y = 50,
				sx = 140,
				sy = 45,
				css = 'gray_btn',
				text = '取 消',
				text_css = 'c4',
			},				
		},	
		nothing_tip = {
			x =0,
			y = 650,
			ax = 0,
			text = "空空如也，快去道具商城进货吧",
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return PackConfig 
end

