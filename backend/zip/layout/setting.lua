-- setpage
-- pos layout
local Config = {
	[LIGHT_UI.SCREEN_480_800] = {
		-- 底图
		bg = {	--底图通用设置
			sx = 420,
		},
		bg_1 = {
			id = 'bg_1',
			to = 'head_bg',
			y = -110,
			row = 2,
			sy = 120,
		},
		bg_2 = {
			id = 'bg_2',
			to = 'bg_1',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -20,
			row = 3,
			sy = 180,
		},
		bg_3 = {
			id = 'bg_3',
			to = 'bg_2',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -20,
			row = 2,
			sy = 120,
		},
			
		label = {	-- label通用配置
			res = 'small_purewhite_bg.png',
			sy = 20,
		},
		
		label_1 = {
			id = 'label_1',
			to = 'bg_1',
			x = -150,
			y = 30,
			sx = 80,
			text = '系统设置',
			text_css = 'd2',
		},
		label_2 = {
			id = 'label_2',
			to = 'label_1',
			y = -60,
			sx = 80,
			text = '安全设置',
			text_css = 'd2',
		},
		label_3 = {
			id = 'label_3',
			to = 'bg_2',
			x = -150,
			y = 60,
			sx = 80,
			text = '应用帮助',
			text_css = 'd2',			
		},
		label_4 = {
			id = 'label_4',
			to = 'label_3',
			y = -60,
			sx = 80,
			text = '关于软件',
			text_css = 'd2',
		},
		label_5 = {
			id = 'label_5',
			to = 'label_4',
			y = -60,
			sx = 80,
			text = '联系我们',
			text_css = 'd2',
		},
		label_6 = {
			id = 'label_6',
			to = 'bg_3',
			x = -150,
			y = 30,
			sx = 80,
			text = '意见反馈',
			text_css = 'd2',
		},
		label_7 = {
			id = 'label_7',
			to = 'label_6',
			y = -60,
			sx = 80,
			text = '清除缓存',
			text_css = 'd2',
		},
		
		------------------------------------------------
		label_8 = {
			id = 'label_8',
			to = 'label_1',
			ax = 0,
			x = 130,
			sx = 200,
			sy = 40,
			text = '声音/震动/聊天',
			text_css = 'b3',
		},
		label_9 = {
			id = 'label_9',	
			to = 'label_8',
			tx = 0,
			ax = 0,
			x = 20,
			y = -60,
			sx = 200,
			sy = 40,
			text = '修改密码、绑定邮箱',
			text_css = 'b3',
		},
		label_10 = {
			id = 'label_10',	
			to = 'label_3',
			ax = 0,
			x = 140,
			sx = 210,
			sy = 40,
			text = '游戏帮助说明文档',
			text_css = 'b3',
		},
		label_11 = {
			id = 'label_11',	
			to = 'label_10',
			tx = 0,
			ax = 0,
			x = -10,
			y = -60,
			sx = 210,
			sy = 40,
			text = '本应用信息查看',
			text_css = 'b3',
		},
		label_12 = {
			id = 'label_12',	
			to = 'label_11',
			tx = 0,
			ax = 0,
			x = -7,
			y = -60,
			sx = 200,
			sy = 40,
			text = '欢迎合作洽谈',
			text_css = 'b3',
		},
		label_13 = {
			id = 'label_13',	
			to = 'label_6',
			ax = 0,
			x = 130,
			sx = 210,
			sy = 40,
			text = '有意见和我说吧',
			text_css = 'b3',
		},
		label_14 = {
			id = 'label_14',	
			to = 'label_13',
			tx = 0,
			ax = 0,
			x = 18,
			y = -60,
			sx = 210,
			sy = 40,
			text = '清除所有的数据缓存',
			text_css = 'b3',
		},
		
		------------------------------------------------
		logout_btn = {
			to = 'bg_3',
			y = -120,
			sx = 400,
			sy = 50,
			css = 'green_btn',
			text = '注 销',
			text_css = 'd4',
		},
		
		btn_cannel = {
			x = 0,
			sx = 480,
			sy = 1,
			res = 'btn_default.png',
			on = 'btn_default.png',
			text = '注销',
			text_css = 'd9'
		},
		
		

	},
}
--------------------------------------------------------------

function getConfigData()
	return Config
end
