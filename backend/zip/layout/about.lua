-- about layout
local Config = {
	[LIGHT_UI.SCREEN_480_800] = {
		["text_config"] = {
			["x"] = 10,
			["y"] = 720,
			["text"] = "    《骰友》由广州市欢腾网络科技有限公司（以下简称本公司）全权开发发行，发布版本以本公司公示版本为准，任何未经授权对本软件进行修改发布的衍生软件，均属非法，本公司对制作者保留追究法律责任的权利；并用户因下载、安装、使用此类软件导致的不可预知的风险，由此产生的一切法律责任及纠纷，一概与本公司无关。\n    下载、安装、使用《骰友》皆免费，而为《骰友》的趣味性考虑，本公司将提供一定的付费增值服务，但下载和使用过程中产生的数据流量费用，则由运营商收取，与本公司无关。\n    《骰友》只是一个交友平台，用户必须为自己注册账号下的一切行为负责，包括内容以及由此产生的后果。此外，用户不得利用本软件发表、传送、传播及存储任何危害国家安全、祖国统一、社会稳定，或任意针对群体或个人的侮辱诽谤、淫秽、暴力及任何违反国家法律法规政策的言论及内容；不得利用本软件发布任何误导、欺骗的信息，或利用本软件批量发表、传送、传播广告信息。否则本公司有权终止用户帐号的使用。\n    本公司对用户发表内容是否侵权，有一定的监管机制，但由于内容太多，审查未必周全，如任何人发现本平台用户发表的任何内容存在侵权行为，请及时通知本公司，如情况属实，我们将会第一时间作出处理。\n声明：本协议的版权由本公司所有，本公司保留一切解释和修改的权利。",
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return Config
end
