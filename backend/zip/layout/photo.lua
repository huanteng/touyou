-- pos layout
local ChaConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		info_bg = {
			id = 'info_bg',
			to = 'head_bg',
			y = -62,
			sx = 480,
			sy = 60,
			res = 'buttom_clarity_line.png',
		},
		info = {
			id = 'info',
			to = 'info_bg',
			x = 30,
			y = 0,
			css = 'c4',
		},
		
		sex = {
			id = 'sex',
			to = 'info',
			tx = 0,
			ty = 0,
			ax = 1,
			ay = 0,
			x = -5,
		},
		big_pic = {
		    ax = 0.5,
			ay = 0.5,
			x = 240,
			y = 410,
			sx = 420,
			sy = 540,
		},
		
		link_bg = {
			to = 'score_bg',
			y = 73,
			sx = 480,
			sy = 60,
			res = 'buttom_clarity_line.png',
		},		
		info_btn = {
			id = 'info_btn',
			x = 104,
			y = 116,
			res = 'userinfo_pic.png',
		},
		photos_btn = {
			id = 'photos_btn',
			to = 'info_btn',
			tx = 1,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 100,
			res = 'photos_pic.png',
		},
		focus_btn = {
			to = 'photos_btn',
			tx = 1,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 100,
			res = 'focus_pic_1.png',
			on = 'focus_pic_2.png',
		},
		score_bg = {
			id = 'score_bg',
			x = 240,
			y = 43,
			sx = 480,
			sy = 86,
			res = 'bottom_line_dgary.png',
		},	
		
		ready_action = {
			x = 240,
			y = 400,
		    res = 'ready.png',
			width = 180,
			height = 60,
			count = 6,
			across = 1,
			row = 6,
			time = 0.2,
		},		
		
	},
}
--------------------------------------------------------------

function getConfigData()
	return ChaConfig 
end

