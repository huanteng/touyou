local RoomLuckConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		luck_btn = {
            id = 'luck_btn', 
			x = 60,
			y = 700,
			res = 'res/li_btn.png',
			on = 'res/li_btn.png',
		},
		luck_btnbg = {		
			x = 60,
			y = 700,
			res = 'res/li.png',			
		},
		action_luck = {    --彩蛋动画
			res = 'res/li_open.png',
			width = 100,
			height = 100,
			x = 0,
			y = 0,
			count = 2,
			across = 1,--横
			row = 2, --列
			time = 0.2, --以秒为单位,每0.1秒播放一帧
		},
		luck_text = {     --彩蛋文字
			x = 68,
			y = 710,
			text = '',
			css = 'b7',
		},	
		luck_text_move = { --文字移动
			height = 20,
			speed = 3,
		},
		luck_delay = { --彩蛋最后10秒开始隐藏
			time = 10,		
		},
		luck_showtime = {  --彩蛋显示完毕几秒后消失
		    time = 5,
		},
		luck_sound = { --开盒子声音
			sound = 'sound/openbox.mp3',
			ms = 1000,
		},	
		luck_show = { --宝箱出现声音
			sound = 'sound/box_show.mp3',
			ms = 1000,
		}	
	},
}	

function getConfigData()
	return RoomLuckConfig
end