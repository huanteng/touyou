-- pos layout
local MainPanelConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		
		title_text = {	-- 
			id = 'title',
			x = 200,
			y = 770,						
			text = '骰友',
			css = 'c3',
		},
		
		bg_pic = { -- 背景图 左上角对齐					
			x = 240,
			y = 362,
			res = 'white_dot.png',
			sx = 480,
			sy = 745,
		},			
		
		["bg_config"] = {
			["x"] = 240,
			["y"] = 630,			
			--["res"] = 'ggt.jpg',
			["res"] = '201311242042.jpg',			
		},
		
		buy_btn = {
			x = 395,
			y = 35,
			sx = 150,
			sy = 48,
			css = 'blue_btn',
			text = '免费获取',
			text_css='b4',
		},		
		
		scroll_text = {	-- 滚动条文字
			x = 0,
			y = 30,
			--text = '惊险、刺激，骰友伴你每天好心情，宝石升级就送！！！',
			text = '',
			css = 'c4',
		},	
		
		scroll_bg = { -- 滚动文字背景					
			x = 240,
			y = 500,
			res = 'gg.png',
			sx = 480,
			sy = 50,
		},	
		
		game_hall_button = {	-- 
			x = 240,
			y = 395,			
			res = 'yxdt-off.png',
			on = 'yxdt-on.png',
		},
		
		rank_button = {	-- 
			x = 240,
			y = 290,			
			res = 'pxb-off.png',
			on = 'phb-on.png',
		},
		
		dice_button = {	-- 
			x = 240,
			y = 185,			
			res = 'ytz-off.png',
			on = 'ytz-on.png',
		},
		
		message_button = {	-- 
			x = 440,
			y = 770,			
			res = 'xiaoxi.png',
			on = 'xiaoxi-on.png',
		},
		
		question_button = {	-- 
			x = 440,
			y = 105,			
			res = 'bz.png',
			on = 'bz-on.png',
		},
		
		["bg_bottom"] = {		--背景
			["x"] = 240,
			["y"] = 35,	
			["sx"] = 480,
			["sy"] = 70,				
			["res"] = 'di.png',
		},
		
		["gold_pic"] = {		--背景
			[ "id" ] = 'gold_pic',
			["x"] = 35,
			["y"] = 35,				
			["res"] = 'gold.png',
		},
		
		gold_text = {	-- 顶部金币数量
			to = 'gold_pic',
			x = 60,
			y = 2,
			css = 'c4',
		},
		
		_scroll_text = {
			x = 10,
			y = 490,
			width = 460,
			height = 30,
		},
		
		game_hall_text = {
			x = 170,
			y = 370,
			text = "千人同区 男女混战",
			ax = 0,
			ay = 0.5,
		},
		
		rank_text = {
			x = 170,
			y = 265,
			text = "骰魔争霸，谁与争锋",
			ax = 0,
			ay = 0.5,
		},
		
		dice_text = {
			x = 170,
			y = 160,
			text = "摇一摇，有惊喜哦",
			ax = 0,
			ay = 0.5,
		},			
		alert = {
			bg = { --黑色背景
				id = 'bg_btn',
				ax = 0.5,
				ay = 0.5,						
				x = 240,
				y = 400,
				sx = 480,
				sy = 800,
				res = 'res/bj.png',								
			},
			window = { --窗口背景
				ax = 0.5,
				ay = 0.5,
				x = 240,
				y = 400,
				sx = 380,
				sy = 500,
				res = 'res/zis.jpg',		
			},
			title = { --标题
				--ax = 0.5,
				--ay = 0.5,
				x = 135,
				y = 615,
				css = 'c16',
				text = '更新提示',
			},
			content3 = { --内容
				ax = 0.5,
				ay = 0.5,
				sx = 330,
				sy = 60,
				x = 90,
				y = 550,
				css = 'f17',
				text = '测试内容',
			},
			ok_btn = { --确定按钮
				ax = 0.5,
				ay = 0.5,
				tx = 0,
				ty = 0,
				x = 100 + 65,
				y = 190,
				sx = 110,
				sy = 50,
				css = 'blue_btn',
				text = '现在更新',
				text_css = 'c4',
			},
			cancel_btn = { --取消按钮
				ax = 0.5,
				ay = 0.5,
				tx = 0,
				ty = 0,
				x = 250 + 65,
				y = 190,
				sx = 110,
				sy = 50,
				css = 'gray_btn',
				text = '稍候再说',
				text_css = 'c4',
			},				
		},			
	},
}
--------------------------------------------------------------

function getConfigData()
	return MainPanelConfig 
end