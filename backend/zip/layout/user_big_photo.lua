-- pos layout
local ChaConfig = {
	[LIGHT_UI.SCREEN_480_800] = {		
		big_pic = {
		    ax = 0.5,
			ay = 0.5,
			x = 240,
			y = 400,
			--sx = 420,
			--sy = 650,
		},
		info_bg = {
			id = 'info_bg',
			to = 'head_bg',
			y = -62,
			sx = 480,
			sy = 60,
			res = 'buttom_clarity_line.png',
		},
		link_bg = {
			id = 'link_bg',
			to = 'xyz',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 0,
			y = 0,
			sx = 480,
			sy = 60,
			res = 'buttom_clarity_line.png',
		},		
		info_btn = {
			to = 'xyz',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 0,
			id = 'info_btn',
			x = 150,
			y = 10,
			res = 'userinfo_pic.png',
		},
		photos_btn = {
			id = 'photos_btn',
			to = 'info_btn',
			tx = 1,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 100,
			res = 'photos_pic.png',
		},
		focus_btn = {
			to = 'photos_btn',
			tx = 1,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 100,
			res = 'focus_pic_1.png',
			on = 'focus_pic_2.png',
		},
		score_bg = {
			id = 'score_bg',
			x = 240,
			y = 43,
			sx = 480,
			sy = 86,
			res = 'bottom_line_dgary.png',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return ChaConfig 
end

