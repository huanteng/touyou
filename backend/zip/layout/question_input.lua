-- question input layout
local HobbyConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		btn = {
			to = 'head_bg',
			x = 200,
			res = 'oktosend.png',
		},
		content = {
			id = 'content',
			to = 'head_bg',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			x = 20,
			y = -10,
			sx = 440,
			sy = 1,
			text = '',
			css = 'b2',
		},
		multi_text_btn_c = {			
			to = 'content',
			tx = 0,
			ty = 1,
			x = 0,
			y = -65,
			sx = 440,
			sy = 125,
			ax = 0,
			ay = 1,
			css = 'ic_single',
			text = '',
			text_css = 'b2',
		},
		user_input_label_c = {
			to = 'content',
			tx = 0,
			ty = 1,
			x = 5,
			y = -70,
			sx = 420,
			sy = 125,
			ax = 0,
			ay = 1,
			text = '',
			css = 'b2',
		}
	},
}
--------------------------------------------------------------

function getConfigData()
	return HobbyConfig 
end

