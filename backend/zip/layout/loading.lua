local LoadingPageConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["loading_bg"] = {
			id = 'loading_bg',
			x = 240,
			y = 400,
			res = 'res/loading_bg.png',
		},
		["loading"] = {	
			id = 'loading',
			to = 'loading_bg',
			res = 'res/loading.png',
			width = 156,
			height = 156,
			x = 0,
			y = 0,
			count = 12,
			across = 3,--横
			row = 4, --列
			time = 0.1, --每0.1秒跑帧
		},
		delay = {
			time = 3,
		},			
	},
}	

function getConfigData()
	return LoadingPageConfig 
end