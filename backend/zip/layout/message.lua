-- pos layout
local MsgConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		btn = {
			to = 'head_bg',
			x = 200,
			res = 'message_box_clear_bt.png',
		},
		move_page_config = { -- 滑动页
			x = 0, -- 滑动页左下角点x
			y = 0, -- 滑动页左下角点y
			view_height = 695, -- 滑动页可视区域高度
			inner_y = 695, -- 滑动页内部列表的起始y坐标
		},
		move_hint_config = { -- 滑动页码显示控件
			x = 0,
			y = 720,
			res = 'tab_cursor_image.png', -- 滑动的条资源
		},
		other = {
			margin = 20,
		},
		logo = {
			to = 'xyz',
			tx = 0,
			ty = 1,
			ax = 0,
			ay = 1,
			id = 'logo',
			sx = 90,
			sy = 90,
			x = 10,
			y = -10,
		},
		name = {
			id = 'name',
			to = 'logo',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 15,
			css = 'c1',
		},
		content = {
			id = 'content',
			to = 'name',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -10,
			sx = 340,
			sy = 1,
			css = 'b2',
		},
		time = {
			to = 'xyz2',
			tx = 1,
			ty = 1,
			x = -10,
			y = -15,
			ax = 1,
			ay = 1,
			text = '',
			css = 'a3',
		},
		whole_bg = {
			to = 'xyz',
			tx = 0,
			ty = 0,
			x = 0,
			y = 0,
			sx = 480,
			sy = 0,
			text = ' ',
			text_css = 'c2',			
			res = 'tiny_bg_pixel.png',
			on = 'tiny_yellow_pixel.png',
		},
		split_line = { --分隔线
			ax = 0,
			ay = 1,
			y = -50,
			res = 'divider.png',
			margin = 5,
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return MsgConfig 
end

