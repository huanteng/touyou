-- pos layout
local MallConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["pack_btn_config"] = { -- 顶部右上角切换样式按钮
			["x"] = 423,
			["y"] = 743,
			["res"] = "myprofile_pack_pic_sc.png",
		},
		["move_page_config"] = { -- 滑动页
			["x"] = 0, -- 滑动页左下角点x
			["y"] = 0, -- 滑动页左下角点y
			["view_height"] = 646, -- 滑动页可视区域高度
			["inner_y"] = 646, -- 滑动页内部列表的起始y坐标
		},
		["move_hint_config"] = { -- 滑动页码显示控件
			["x"] = 0,
			["y"] = 672,
			["res"] = "tab_cursor_image.png", -- 滑动的条资源
		},
		["mall_grid_config"] = {
			["name_x"] = 90,
			["name_y"] = -20,
			["price_x"] = 465,
			["price_y"] = -10,
			["desc_x"] = 90,
			["desc_y"] = -60,
		},
		head_scroll = {	-- 滚动条
			x = 0,
			y = 720,
			res = 'top_scroll_bg.png',
		},
		logo = {
			id = 'logo',
			sx = 80,
			sy = 80,
			x = 51,
			y = -55,
		},
		name = {
			id = 'name',
			to = 'logo',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 15,
			y = 0,
			css = 'c2',
		},
		hot = {
			to = 'hot',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 10,
			res = 'prop_hot_pic.png',
		},
		money_type = {
			id = 'money_type',
			to = 'name',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 265,
			y = -5,
		},
		price = {
			to = 'money_type',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 40,
			y = 5,
			text = "",
			css = "b9",
		},
		desc = {
			id = 'desc',
			to = 'name',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -30,
			sx = 358,
			sy = 1,
			css = 'b2',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return MallConfig 
end

