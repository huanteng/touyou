-- pos layout
local NewsConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["move_page_config"] = { -- 滑动页
			["x"] = 0, -- 滑动页左下角点x
			["y"] = 0, -- 滑动页左下角点y
			["view_height"] = 700, -- 滑动页可视区域高度
			["inner_y"] = 690, -- 滑动页内部列表的起始y坐标
		},
		["move_hint_config"] = { -- 滑动页码显示控件
			["x"] = 0,
			["y"] = 720,
			["res"] = "tab_cursor_image.png", -- 滑动的条资源
		},
		logo = {
			id = 'logo',
			sx = 90,
			sy = 90,
			x = 55,
			y = -58,
		},
		name = {
			id = 'name',
			to = 'logo',
			tx = 1,
			ty = 1,
			ax = 0,
			ay = 1,
			x = 10,
			y = 2,
			css = 'c1',
		},
		sex = {
			id = 'sex',
			to = 'name',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -8,
		},
		time = {
			to = 'logo',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 400,
			y = 65,
			text = '',
			css = 'a3',
		},
		vip = {
			to = 'name',
			tx = 1,
			ty = 0,
			ax = 0,
			ay = 0,
			x = 12,
		},
		content = {
			id = 'content',
			to = 'sex',
			tx = 0,
			ty = 0,
			ax = 0,
			ay = 1,
			y = -5,
			sx = 358,
			sy = 1,
			css = 'b2',
		},
		split_line = { --分隔线
			id = 'split_line',
			ax = 0,
			ay = 1,
			y = -100,
			res = 'divider.png',
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return NewsConfig 
end

