-- pos layout
local MainPageConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		["top_scroll_config"] = {
			["x"] = 0,
			["y"] = 708,
		},
		["move_config"] = {
			["x"] = 0,
			["y"] = 120,
			["view_height"] = 588,
			["inner_y"] = 538,
		},
		["move_hint_config"] = {
			["x"] = 0,
			["y"] = 120,
			["normal_res"] = "indicator_default.png",
			["select_res"] = "indicator_selected.png",
		},
		
		scroll_text = {	-- 滚动条文字
			x = 0,
			y = 30,
			text = '惊险、刺激，骰友伴你每天好心情，宝石升级就送！！！',
			css = 'c2',
		},
		
		desc_info = {	--每个9宫格的设置
			item_width = 100,
			item_height = 100,
			column_cnt = 3,
			x_space = 40,
			y_space = 80,
		},
		["music_sound"] = { -- 发送消息的声音
			sound = 'sound/message.mp3',
			ms = 1000,
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return MainPageConfig 
end

