-- pos layout
local DayConfig = {
	[LIGHT_UI.SCREEN_480_800] = {
		
		["select_config"] = {
			["x"] = 75,
			["y"] = 680,
		},
		["reply_btn_config"] = { -- 顶部右上角切换样式按钮
			["x"] = 405,
			["y"] = 739,
			["normal_res"] = "oktosend.png",
			["click_res"] = "oktosend.png",
		},
	},
}
--------------------------------------------------------------

function getConfigData()
	return DayConfig 
end

