--room_match_layout
local config = {
	room_label = {  -- 顶部右上角房间号
		x = display.sx - 50,
		y = display.sy-25,
		css = 'a4',
	},
	gold_pic = {	-- 顶部金币图标
		id = 'gold_pic',
		x = 140,
		y = 30,
		res = 'res/coin_s.png',
	},
	gold_text = {	-- 顶部金币数量
		to = 'gold_pic',
		x = 60,
		css = 'c4',
	},
	desk_bg = {
		x = 0,  -- 个人底板 相对于logo
		y = -9,
		res = 'res/xxp_08.png',
	},
	head_bg = { -- 背景图 左下角点对齐
		--x = 0,
		--y = 0,
		x = display.x,
		y = display.y,
		sx = display.sx,
		sy = display.sy,			
		res = 'bg_08.jpg',
	},
	head_text = { -- 顶部标题文字
		x = 120,
		y = display.sy-37,
		css = 'c4',
	},
	head_gold = { -- 挑战金
		x = 352,
		y = display.sy-37,
		css = 'c4',
	},
	head_back = { -- 顶部左上角返回按钮
		x = 35,
		y = display.sy-25,
		res = 'touzi.png',
	},
	control_btn = { -- 
		x = display.sx- 40,
		y = display.sy-90,
		sx = 48,
		sy = 48,
		res = 'yxcd_2.png',		
		--res = 'touzi.png',
	},
	head_scroll = {
		x = 0,
		y = display.sy-92,
		res = 'top_scroll_bg.png',
	},
	zzch = { -- 正在撮合，请稍候
		x = 200,
		y = display.y,
		res = 'tsk_ch.png',
	},
	desk1 = { -- 身份牌位置
		ay = 0,
		x = display.x,
		y = 148,		
	},
	desk2 = {
		x = display.sx - 51,
		y = display.y + 70,
	},
	desk3 = {
		id = 'desk3',
		x = display.x,
		y = display.sy-130,
	},
	desk4 = {
		x = 50,
		y = display.y + 70,
	},
	desk5 = { --自己的位置
		x =  'display.x0 + 10',
		y =  'display.y0 + 20',
	},
	--2do，男女换用不同的颜色
	name = {	-- 相对logo
		x = -40,
		y = -53,
		ax = 0,
		css = 'a4',
	},
	logo1 = {
		x = -178,
		y = -60,
	},
	logo2 = {
		x = -18,
		y = 40,
	},			
	logo3 = {
		x = 0,
		y = -10,
	},
	logo4 = {
		x = 18,
		y = 40,
	},
	logo5 = { --自己的logo
		x = 52,
		y = 68,
	},
	zhong1 = {	-- 相对中心点
		id = 'zhong1',
		x = -157,
		y = 70,
		res = 'sz-l.png',
	},
	zhong2 = {
		x = -22,
		y = -80,
		res = 'sz-s.png',
	},
	zhong3 = {
		x = -90,
		y = -20,
		res = 'sz-s.png',
	},
	zhong4 = {
		x = 25,
		y = -80,
		res = 'sz-s.png',			
	},
	zhong5 = {
		x = 80,
		y = 200,
		res = 'sz-l.png',
	},
	sitdown1 = {
		x = 0,
		y = -10,
		res = 'res/btn_zx_1.png',
		on = 'res/btn_zx_2.png',
		
	},
	sitdown2 = {
		x = -10,
		y = -20,
		res = 'res/btn_zx_1.png',
		on = 'res/btn_zx_2.png',
	},
	sitdown3 = {
		x = 0,
		y = 0,
		res = 'res/btn_zx_1.png',
		on = 'res/btn_zx_2.png',
	},
	sitdown4 = {
		x = 12,
		y = -20,
		res = 'res/btn_zx_1.png',
		on = 'res/btn_zx_2.png',
	},
	sitdown5 = {
		x = -23,
		y = 180,
		res = 'res/btn_zx_1.png',
		on = 'res/btn_zx_2.png',
		
	},
	a_sitdown1 = { --坐下动画位置
		x = 0,
		y = 0,
		res = 'res/btn_zx_1.png',
		on = 'res/btn_zx_2.png',
		
	},
	a_sitdown2 = {
		x = -10,
		y = -10,
		res = 'res/btn_zx_1.png',
		on = 'res/btn_zx_2.png',
	},
	a_sitdown3 = {
		x = 0,
		y = 10,
		res = 'res/btn_zx_1.png',
		on = 'res/btn_zx_2.png',
	},
	a_sitdown4 = {
		x = 12,
		y = -10,
		res = 'res/btn_zx_1.png',
		on = 'res/btn_zx_2.png',
	},
	a_sitdown5 = {
		x = -23,
		y = 180,
		res = 'res/btn_zx_1.png',
		on = 'res/btn_zx_2.png',
		
	},
	ready = {  -- 位置相对于盅
		x = 0,
		y = 0,
		res = 'ready.png',
	},
	-- 喊号提示框，相对于logo
	shout = {
		x = 0,
		y = 180,
		res = 'ts-ying.png',
		sound = 'sound/shout.mp3',
	},
	selfway = {  --自己骰，左下角
		id = 'selfway',
		to = 'zhong1',
		ax = 0,
		tx = 1,
		x = -50,
		y = 120,
		--x = -150,
		--y = 155,
	},
	way1 = {  --开骰双行叠骰效果
		x = display.x - 20,
		y = 220,
	},
	way2 = {
		x = display.sx - 100,
		y = display.y + 5,		
	},
	way3 = {	
		x = display.x - 30,
		y = display.sy - 235,
	},
	way4 = {
		x = 35,
		y = display.y + 5,
	},
	way5 = {
		x = display.sx - 200,
		y = display.sy - 610,		
	},
	gold1 = {  --赢输金币，相对于中心点
		ax = 0,
		x = 75,
		y = 135,
	},
	gold2 = {
		ax = 1,		
		x = 402,
		y = 490,			
	},
	gold3 = {
		ax = 0.5,
		x = 239,
		y = display.sy-140,
	},
	gold4 = {
		ax = 0,
		x = 75,
		y = 490,			
	},
	gold5 = {
		ax = 0,
		x = 240,
		y = 145,
	},
	timeout1 = {    --倒计时
		x = 60,
		y = 80,
	},
	timeout2 = {
		x = display.sx - 70,
		y = display.y + 105,
	},
	timeout3 = {
		x = display.x,
		y = display.sy - 146,
	},
	timeout4 = {
		x = 69,
		y = display.y + 105,
	},
	shout_item = {	--选号框
		x = 0,
		y = 0,
		sx = 300,
		sy = 59,		
		left_btn_x = 35,    --左移动按钮
		left_btn_y = -55,
		left_btn_res = 'cz-zjt.png',
		right_btn_x = 395,      --右移动按钮
		right_btn_y = -55,
		right_btn_res = 'cz-yjt.png',
		shout_view_x = 64,    --个数位置
		shout_view_y = -86,    --向上值越大
		sel_dice_startx = 65,   --骰子调整
		sel_dice_starty = -125,
		plus_btn_x = 85,   --加1调整
		plus_btn_y = -205,
		zhai_btn_x = 213,  --斋调整
		zhai_btn_y = -205,
		ok_btn_x = 344, --确定调整
		ok_btn_y = -205,
		ok_btn_sx = 80,
		ok_btn_sy = 1,
		count_str = '请选择数量',
		number_str = '请选择骰子',
		way_str = '你所喊的骰子太小哦',
		cut_zhai_str = '亲，破斋要至少两倍哦',
		cnt_move_delta = 55,	-- 数字向左、向右时移动速度
		cnt_move_x = 55,     --骰子点击向左向右移动的步长
	},
	shout_close = {	-- 选号框关闭
		x = 420,
		y = -8,
		res = 'btn_close.png',
	},
	win = {	--赢，使用对应的open的位置
		res = 'ts-ying.png',
		sound = 'sound/win.mp3',
		ms = 4000,
	},
	lose = {	--输
		res = 'ts-shu.png',
		sound = 'sound/lose.mp3',
		ms = 3000,
	},
	open = {	-- 开标志
		res = 'ts-kai.png',	
		sound = 'sound/jiang_show.mp3',
		ms = 5000,
	},
	open1 = {	--开	
		x = 110,
		y = 140,
	},
	open2 = {
		x = 375,
		y = display.y + 160,
	},
	open3 = {
		x = 280,
		y = display.sy-90,
	},
	open4 = {
		x = 120,
		y = display.y + 160,
		--y = 550,
	},
	open5 = {
		x = 115,
		y = 130,
	},
	hhk1 = {	--喊号框
		x = 175,
		y = 185,
	},
	hhk2 = {
		x = -10,
		y = -105,
	},
	hhk3 = {
		x = 0,
		y = -105,
	},
	hhk4 = {
		x = 10,
		y = -105,
	},
	hhk5 = {
		x = 150,
		y = 150,
	},
	hhxs = {      --叫号的斋显示
		x = 38,
		y = 0,
		text = '斋',
		css = 'c4',
	},
	shout_btn = {	-- 喊按钮
		x = display.sx - 260,
		y = 80,
		res = 'han.png',
		on = 'han_on.png',
	},
	open_btn = {	-- 开按钮
		x = display.sx - 90,
		y = 80,
		res = 'kai.png',
		on = 'kai_on.png',
	},
	delay = {	-- 延时，秒
		open = 0.5,	-- 显示“开”标志
		count = 1.5,	-- 每人有2秒时间,显示各自骰数量			
		win = 0.5,		-- 显示赢标示
		lose = 0.5,		-- 显示输标示
		gold = 2,	-- 显示赢输金币
		drunkenness = 3,		--显示醉酒度动画
		drunkHandler=0.5,   --更新自己的筹码
	},
	rock = {	--摇的声音
		sound = 'sound/rock.wav',
		ms = 3000,
	},
	result_number = {	-- 结果，骰子，相对于中心点
		id = 'result_number',
		y = display.y - 20,
		x = display.x - 35,	
		res = 'szm%d.png',
	},
	result_count = {	-- 结果，数量，相对于游戏
		to = 'result_number',
		ax = 0,				
		x = 30,
		y = 0,
		css = 'g4',
	},
	center = {    --火星轰击的位置		
		id = 'center',
		y = display.y - 20,
		x = display.x - 35,	
	},			
	agent_btn = {       -- 代理按钮
		x = 440,
		y = 714,
		res = 'res/tg1_off.png',
		on = 'res/tg1_on.png',
	},
	clear_wine = {		--解酒按钮
		id = 'clear_wine',
		x = display.sx - 90,
		y = display.sy- 90,
		res = 'jj_off.png',
		on = 'jj_on.png',
	},
	drunk_good_num_txt = {		--解酒药数量
		to = 'clear_wine',
		tx = 1,
		ty = 0,
		ax = 1,
		ay = 0,
		x = -3,
		y = 3, 
		text = '0',
		css = 'b4',
	},
	no_agent_btn = {    -- 取消代理按钮
		x = 440,
		y = display.sy-86,
		res = 'res/tg2_off.png',
		on = 'res/tg2_on.png',
	},
	act_mask = {  -- 托管提示层
		ax = 0,
		x = 0,
		y = display.sy * 0.5,		
		res = 'res/jrtg.png',			
	},
	act_robot = { --托管机器人		
		x = 100,
		y = 30,
		res = 'res/tuguan.png',
	},
	action_sitdown = {    --坐下动画
		res = 'res/sitdown.png',
		width = 40,
		height = 40,
		x = 0,
		y = 0,
		count = 2,
		across = 1,--横
		row = 2, --列
		time = 0.4, --以秒为单位,每0.2秒播放一帧
	},
	action_timeout = {     --玩家读秒倒计时
		res = 'res/timer.png',
		--width = 114,
		width = 114,
		height = 150,
		count = 75,
		across = 5,--横
		row = 15, --列
		time = 0.2, --以秒为单位
	},
	action_countdown = {    --开始倒计时
		path = 'res/countdown',
		x = 218,
		y = 380,
		width = 113,
		height = 110,
		count = 5,
		across = 1,--横
		row = 5, --列
		scale = 0.5,--缩放倍数
		sound = 'sound/clock_s.mp3',--倒计时声音
		ms = '5000',--声音时间(毫秒)
	},
   timeout_text1 = {	-- 倒计时文字，相对于游戏
		x = 162,
		y = 365,
		text = '游戏',
		css = 'c4',
	},
	timeout_text2 = {	-- 倒计时文字，相对于游戏
		x = 295,
		y = 365,
		text = '秒后开始',
		css = 'c4',
	},
	action_win_score = {      --赢积分
		res = 'res/suzi_20.png',
		width = 30,
		height = 40,
		startCharMap = '*',
	},
	action_loss_score = {      --输积分
		res = 'res/suzi_16.png',
		width = 30,
		height = 40,
		startCharMap = '*',
	},
	action_star = {      --火星
	   res = 'res/stars.png',
		width = 225,
		height = 200,
		count = 4,
		across = 1,--横
		row = 4, --列
		time = 0.1, --以秒为单位,每0.1秒播放一帧
		sound = 'sound/flash.mp3',
	},
	message_tips = {  --等待消息提示
		x = 100,
		y = 200,
	},
	add_coin = {  --增加金币
		x = 0,
		y = 0,
		sound = 'sound/addgold.mp3',
		res = 'res/coin.png',
	},
	double = { 		 --双倍
		id = 'double',
		y = display.y + 30,
		x = display.x - 43,	
		res = 'sb.png',
	},
	double_text = { --双倍×2，3，4
		to = 'double',
		x = 75,
		y = 0,
		css = 'g4',
	},	
	ready_action = {
		x = 240,
		y = 400,
		res = 'ready.png',
		width = 180,
		height = 60,
		count = 6,
		across = 1,
		row = 6,
		time = 0.2,
	},	
	lookontips = {
		x = 235,
		y = 80,
		css = 'b4',
		text = '已坐下，等待下局开始中',
	},	
	_scroll_text = {
		x = 10,
		y = 495,
		width = 310,
		height = 30,
	},
	
	scroll_bg = { -- 滚动文字背景
		id = 'scroll_bg',
		x = 240,
		y = display.sy-500,
		res = 'gg.png',
		sx = display.sx,
		sy = 50,
	},
	scroll_text = {	-- 滚动条文字
		x = 0,
		y = 30,
		--text = '惊险、刺激，骰友伴你每天好心情，宝石升级就送！！！',
		text = '醉酒度达到上限将被踢出房间，请及时解酒',
		css = 'c4',
	},
	scroll_node = {
		x = 60,
		y = display.sy - 530,
		--y = 320,
	},
	drunkenness_bg_pic = {		--醉酒背景图标		
		x = 110,
		y = 50,
		sx = 20,
		sy = 20,
		res = 'jzd-h.png',
	},
	drunkenness_animation = {		--醉酒踢出动画		
		x = 0, 
		y = 0,
		sx = 50,
		sy = 50, 
		width =130,
		height = 100, 
		count = 6,
		across = 1, 
		row = 6,
		res = 'gzcj.png', 
		time =0.3
	},
	drunkenness_animation2 = {		--醉酒动画		
		x = 0, 
		y = 0,
		sx = 50,
		sy = 50, 
		width =100, 
		height = 100, 
		count = 2,
		across = 1, 
		row = 2,
		res = 'zj60.png', 
		time = 1, 
	},
	drunkenness_btn_state_pic = {		--醉酒按钮状态背景图标		
		x = display.sx - 90,
		y = display.sy- 90,
		--x = display.sx - 40,
		--y = display.sy- 80,		
		res = 'jzd-h.png',
	},
	drunkenness_btn_state_animation = {		--醉酒按钮状态动画		
		x = 0, 
		y = 0,
		width = 60, 
		height = 60, 
		count = 2, 
		across = 1, 
		row = 2, 
		res = 'fg-tu.png', 
		time = 0.5,
	},
	
	drunkenness_up_pic = {		--酒水上升背景图标		
		x = 440,
		y = display.sy-86,			
		res = 'zj004.png',
	},
	drunkenness_up_animation = {		--酒水上升动画		
		x = 0, 
		y = 0,
		width = 76, 
		height = 76, 
		count = 10, 
		across = 1, 
		row = 10, 
		res = 'hjdh.png', 
		time = 0.3,
	},
	
	drunkenness_flash_pic = {		--解酒闪亮背景图标		
		x = display.sx - 40,
		y = display.sy- 80,			
		res = 'zj004.png',
	},
	drunkenness_flash_animation = {		--解酒闪亮动画		
		x = 0, 
		y = 0,
		width = 200, 
		height = 200, 
		count = 6, 
		across = 1, 
		row = 6, 
		res = 'jjdh.png', 
		time = 0.2,
	},
	wake_up_drunk_good_pic = {		--解酒药图标		
		x = 440,
		y = display.sy-86,	
		sx = 80,
		sy = 80,		
		res = 'wake_up_drunk_good.png',
	},
	changetable = {
		x = 405,
		y = 72,
		res = 'ksht-off.png',
		on = 'ksht-on.png',
	},
	vip_coming_pic = {		--vip进场背景图标		
		x = 440,
		y = display.sy-86,			
		res = 'vip_coming_bg.png',
	},
	vip_coming_animation = {		--vip进场动画		
		x = 0, 
		y = 0,
		width = 200, 
		height = 220, 
		count = 11, 
		across = 1, 
		row = 11, 
		res = 'vip_coming.png', 
		time = 0.2,
	},
	
	qkaiSound = {
		girlSound = "sound/dice/girl/qkai.mp3",
		boySound = "sound/dice/boy/qkai.mp3",
		delay = 0,
	},
	
	doubleSound = {
		girlSound = "sound/dice/girl/fb.wav",
		boySound = "sound/dice/boy/fb.wav",
		pureDelay = 3,
		delay = 2,
	},
}
--------------------------------------------------------------

function getData()
	return config
end

