-- room

local RoomConfig = Import("layout/room.lua").getData()
local COMPONENT_BASE = Import( "lua/component_base.lua" )
local RoomPage = nil

--local RoomListView = nil --房间列表

local PageData = {}
local cur_page = 0
local room_page = 12

local level_name = {["1"] = "屌丝",["2"] = "土豪",["3"] = "富豪",}

local mall = Import("lua/mall.lua")

--级别
local level = 1

local click_delta = 10
--自己的底金
local MYGold = 0
local basePanel = nil
------------------------------------------------------------------------
--显示空桌
local showNullChair = nil
--返回到商城
local backtoMall = nil
--返回
local back_func = nil
--创建列表节点
local createItemByData = nil
--刷新指定页面
local refreshPage = nil
------------------------------------------------------------------------

local currentDrunkenness = 0
local totalDrunkenness = 5
local popWindow = false
local winerName = nil
local exitWindow = nil
local quickChangetable = nil
local letMoney = nil
local isQuickChange = false
local RoomNunber = nil


local firstEnter = true

local info_count = 0

function setPopWindow( value, name )
	popWindow = value
	winerName = name	
end

--金币不足被弹出的的标志
function setExitWindow(value)
	exitWindow = value
	end	


function back_func(toMall)
	if toMall then
		--add_back(RoomPage ,backtoMall )
		mall.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0,backtoMall,nil,true)
	else
		--closePanel()
		if basePanel then	
			showPanel( HTTP_CLIENT.getRootNode(), 0, 0, true )			
		end
	end	
end	
	
function backtoMall()	
	back_func()
	mall.closeMallPagePanel()			
end

function showPage()
	local c = RoomConfig.page
	if RoomPage then
		if not RoomPage.page then
			RoomPage.page = createLabel(RoomPage,c)
		end
		local str = string.format("-[ %d/%d ]-",cur_page,room_page)
		RoomPage.page:setString(str)
	end			
end

local function getBaseGold(level)
	local ret = 100
	if(level == 1) then
		ret = 100	
	elseif(level == 2) then
		ret = 500
	else
		ret = 1000
	end
	return ret
end


local function showAlert()
	local function onOk()			
		back_func(true)
		closePanel()
		setExsitAlert(false)
	end
	
	local config = RoomConfig.alert2
	config.title.text = "温馨提示"
	
	config.content3.text = "您的金币不满足入场要求,最低需"..getBaseGold(level)*4 .."金币哟"	
	createAlert(RoomPage,config,onOk)	
	
	
end	

local function showExitAlert()	
	local room_match = Import("lua/room_game.lua")
	local exitLevel = room_match.getLevel()
	local function onOk()			
		back_func(true)
		closePanel()
		setExsitAlert(false)
	end
	
	local config = RoomConfig.alert2
	config.title.text = "温馨提示"
	config.content3.text = "您的金币不满足入场要求,最低需" .. getBaseGold(exitLevel)*4 .. "金币哟"			
	createAlert(RoomPage,config,onOk)
end	

function createGetRewardAlert(parent,c,onok,oncancel, hasReward )
	if getExsitAlert() == false then
		setExsitAlert(true)
	else		
		clear({p})
		return
	end
	local order = 0
	if parent then
		order = parent:getCOObj():getChildrenCount() + 120
	end		
	local BASE_LAYOUT = Import("layout/base.lua")
	local config = c or BASE_LAYOUT.getData().alert
	local p = createNode(parent,{x = 0,y = 0},nil,order)
	p:setAnchorPoint(0.5,0.5 )
	--[[local function receiveMsg()
		print('onclick')
	end--]]
	if config.bg then
		createButton(p,config.bg,nil)	
	end
	if config.window then
		createSprite(p,config.window)
	end	
	--[[if config.icon then
		createSprite(p,config.icon)
	end--]]
	if config.title then	
		createLabel(p,config.title)
	end
	if config.content then
		createLabel(p,config.content)
	end
	
	if config.content2 then
		createLabel(p,config.content2)
	end
	
	if config.content3 then
		createMultiLabel(p,config.content3)
	end
	
	local function onClickOk()
		setExsitAlert(false)
		clear({p})		
		if onok then
			onok()
		end			
	end
	
	local function onClickCancel()	
		setExsitAlert(false)
		if hasReward == 2 then
			local function getReward( data )
				if data.code == 1 then
					showUpFloorMessage( parent, "又有本钱啦，骰魔重现江湖！", {ms=3000} )
				elseif data.code == -1 then			
					showUpFloorMultiMessage( parent, "今天限额用完了哦，需要更多弹药请到商城补充吧", {ms=3000} )
				elseif data.code == -2 then			
					showUpFloorMultiMessage( parent, "低于400才需要领救济，您这样的高富帅就不用了吧？", {ms=3000} )
				end
			end
			doCommand( 'user', 'day_gold', {}, getReward )	
		end
	
		if oncancel then
			oncancel()
		end
		clear({p})
	end
	
	if config.cancel_btn then
		p.cancelBtn = create9Button(p,config.cancel_btn,onClickCancel)
	end
	if config.ok_btn then
		create9Button(p,config.ok_btn,onClickOk)
	end
	
	return p
end


--启动游戏进入比赛房间
function enterMatch2( room_id,ip,port,no , nLevel)
	--room_id = 50
	--ip = "192.168.1.20"
	--port = 5555
	--no = 5
	--nLevel = 5
	if nLevel then
		level = nLevel
	end
	--no = 10	
	d("currentDrunkenness" .. currentDrunkenness)
	--CCMessageBox(tostring(currentDrunkenness),"当前醉酒度")
	local function startGame()		
		--回调用户信息
		
		if tonumber(NEW_GAME_HALL.myCoin) < 400 then
				local function onOk()			
					back_func(true)
					closePanel()
				end
				local function initRewardBtn( data )
					if data.code and data.code < 0 then
						if data.memo then
							d( "data.memo:" .. data.memo )
						end
						return
					end						
					if tonumber( data.data.day_gold ) <= 0 then
							RoomConfig.get_reward_alert.cancel_btn.text = "知道了"
							createGetRewardAlert(RoomPage,RoomConfig.get_reward_alert,onOk, nil, 1 )	
					else
							RoomConfig.get_reward_alert.cancel_btn.text = "领取救济 (" .. data.data.day_gold .. "/3)"
							createGetRewardAlert(RoomPage,RoomConfig.get_reward_alert,onOk, nil, 2 )	
					end											
				end	
				--请求用户资料
			doCommand( 'user', 'money', {}, initRewardBtn )
			return
		elseif tonumber(NEW_GAME_HALL.myCoin) < getBaseGold(level)*4 then
				showAlert()
				
				return														
		end
			
		closePanel()	
		local room_match = Import("lua/room_game.lua")	
		--room_match.init( { ip = '192.168.1.254',port = 1111,room_id = room_id, chair_id = 1,back_func = back_func } )
		room_match.init( { level = level,ip = ip,port = port,
		room_id = room_id, chair_id = 1,
		room_no = no,room_page = cur_page,
		back_func = back_func } )
		room_match.showPanel(HTTP_CLIENT.getRootNode(), 0, 0 )					
		
	end
	
	local ROOM_MATCH = Import("lua/room_game.lua")
	local quserDrenness = ROOM_MATCH.getuserCurrentDrunkenness()	
	
	if quserDrenness >= 3 then
		currentDrunkenness = quserDrenness		
	end
	
	if currentDrunkenness >= 3 then	
		local function okHandler()
			setExsitAlert( false )
			local function onPropsUses( data )
				if data.code == 1 then
					ROOM_MATCH.setuserCurrentDrunkenness(0)	
					quserDrenness = 0
					showMessage( RoomPage, data.memo )														
					startGame()
				else
					showMessage( RoomPage, data.memo )
				end			
			end
			doCommand( 'props', 'uses', { props = 50, quantity = 1 }, onPropsUses )
		end
		createAlert( RoomPage, RoomConfig.enter_game_alert, okHandler )
	else
		startGame()
	end		
			
end			



--1,进入房间
local function doRoomEnter( id,pwd )
    local function onRoomEnter(data)
        if data.code < 0 then
            showMessage( RoomPage, data.memo )
        else
            enterMatch( id )
        end
    end
    doCommand( 'room', 'enter', { ['id'] = id, ['password'] = pwd },onRoomEnter )
end

--坐下
local function doSit( id )
	local function onSit( data )
		if data.code < 0 then
			showMessage( RoomPage, data.memo )
		else
			enterMatch( id )
		end		
	end
	local roomID = (level - 1)* 200 + id
	doCommand( 'room', 'sit', { ['id'] = roomID, ['password'] = '' }, onSit )
end

--快速坐下
local bQuickSit = false
function doQuickSit()
	if bQuickSit then
		return
	end
	
	bQuickSit = true
	
	local roomData = getData( 'room' )
	local recent = nil
	
	local function onQuickSit( data )
		if data.code < 0 then
			showMessage( RoomPage, data.memo )
			return
		end
		
		data = data.data
		
		if recent then
			local recent_array = split( recent, ',' )
			local i = 0
			recent = data.no
			for _,v in ipairs( recent_array ) do
				if i == 4 then
					break
				end
				
				recent = recent .. ',' .. v
				i = i + 1
			end
		else
			recent = data.no
		end
		
		setData( 'room', 'recent', recent )
		
		dailyCount('QuickStartGame') -- 快速开始
		enterMatch2( data.id,data.ip,data.port,data.no )
	end
	
	if roomData then
		recent = roomData.recent
	end
	
	doCommand( 'room', 'quick_start', { level = level, no = recent }, onQuickSit,nil,{priority = true} )	
	local function sitClickLimitHandler()
		if sitTimer then			
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(sitTimer)
			sitTimer = nil					
		end
		bQuickSit = false
	end
	if not sitTimer then
		sitTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( sitClickLimitHandler, 2, false )	
	end
end	

local function createRoom(idx,parent,info)
	local c, c2 = RoomConfig, nil
	local config = RoomConfig["big_item_config"]
	local nodeObj = createNode( parent, {} )
	
	local o = nil
	
	local function onRoomClick()
		if not info then 
			return 
		end		
		if string.len(tostring(info.ip)) > 0 and
			 tonumber(info.port) > 0 then		
			enterMatch2( info.id,info.ip,info.port,info.no )
		end							
	end
		
	c2 = c['desk' .. idx]	
	--createButton( nodeObj, c2,onRoomClick)
	local c4 = c['desk_center']
	c4.x = c2.x - 4 --配合美术资源设置位置偏移
	c4.y = c2.y
	createButton( nodeObj, c4,onRoomClick)	
	createSprite( nodeObj, c2)
	
	if not info then
		return 
	end 
	
	local center_x, center_y = c2.x, c2.y
	local desk_x, desk_y = c.desk_logo.x, c.desk_logo.y
	
	for i = 1, 4 do
		c2 = {}
	
		--[[
		位置布局为： 		
		4   3
		1   2
		]]
		if i == 4 then
		--if i == 1 then
			c2.x = center_x - desk_x
			c2.y = center_y + desk_y
		--elseif i == 2 then
		elseif i == 3 then
			c2.x = center_x + desk_x
			c2.y = center_y + desk_y
		--elseif i == 3 then
		elseif i == 1 then
			c2.x = center_x - desk_x
			c2.y = center_y - desk_y
		else
			c2.x = center_x + desk_x
			c2.y = center_y - desk_y
		end
		
		local name = nil
		local c3 = RoomConfig.logo
		local o = nil
		if info[ 'desk' .. i ] then
			c2.x, c2.y = c2.x - 0, c2.y + 0
			c2.sx = c3.sx
			c2.sy = c3.sy
			--c2.res = info[ 'desk' .. i ].logo
			--o = createSprite( nodeObj, c2 )
			local uid_info = info[ 'desk' .. i ]
			setUsersData( uid_info, 'uid', {'name',logo='slogo','sex'} )
			setUserLogo( nodeObj, c2, uid_info.uid, 's' )
			
			name = uid_info.name			
		end
		
		if i == 1 or i == 2 then
			c2.y = c2.y - 45
			c2.x = c2.x + 0
		else
			c2.y = c2.y + 45
			c2.x = c2.x + 0
		end
		local o = createLabel( nodeObj, text( c2.x, c2.y, name, 'b4') )
		
		--local o = createLabel( nodeObj, text( c2.x, c2.y, getCharString(tostring(name),5), 'b4') )
		if name then
			--print('name：'..name)
			AdjustString(o,tostring(name),80)
			--AdjustString(o,tostring(info.uid),80)
		end
	end
	
	c2 = c.desk_no	
	local no = string.format( '-%s%03d-', level_name[tostring(level)],info.no )
	createLabel( nodeObj, text( center_x + c2.x, center_y + c2.y, no, c2.css ) )
	
	--底金
	local gold = string.format( '金币: %d', info.gold )
	createLabel( nodeObj, text( center_x, center_y , gold, c2.css ) )
	
	local function getContentSize(node)
		return {["width"] = 140, ["height"] = 140,}
	end
	nodeObj.getContentSize = getContentSize	
	
	return nodeObj
end	
--请求列表定时器（3秒请求一次）
--[[local function doRefreshPage()
	TIMER[ "lists" ] = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( refreshPage, 3, false)
end--]]

local function createMoveBigPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local c, c2 = RoomConfig, RoomConfig.move_page
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_big_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width + 100, c2.view_height, 0 )
	--ret_panel._move_big_grp:setVMovable(true)
	
	local function get_y_limit(obj)
		return c2.inner_y 
	end		

	ret_panel._page = {}
	local desk_info = c.desk_info
	for i = 1, room_page do
		local girl_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, desk_info)
		girl_page:refresh_view()
		
		girl_page:setPosition(0, c2.inner_y)
		girl_page.getYLimit = get_y_limit
		ret_panel._move_big_grp:appendItem(girl_page)
	
		PageData[i] = {}
		ret_panel._page[i] = girl_page
	end
	
	return ret_panel
end	



local function createPanel(parent, x, y, isBasePanel ) 
	local ret_panel = createNode(parent, point( x, y ) )

	local c,c2 = RoomConfig, nil
	--MYGold = 0
	local o = nil
	
	c2 = c.top_pic
	ret_panel.top_pic = createSprite( ret_panel, c2 )
	
	local function on_back(btn)
		closePanel()
		--if not isBasePanel then		
			if PANEL_CONTAINER.closeChildPanel( nil, 10 ) then					
				PANEL_CONTAINER.addChild( MAIN_PANEL.createMainPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
				--PANEL_CONTAINER.addChild( showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
			end	
		--end
		--MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
	local function onClick()
	end
	o = createButton( ret_panel, c.bg_pic, onClick )
	createSprite( ret_panel, c.head_bg_config )
	ret_panel.tab_bar_bg = create9Sprite( ret_panel, c.tab_bar_bg )
	
	createLabel( ret_panel, c.room_title )
	--[[if isBasePanel then
		
	else		
		o = createButton( ret_panel, c.bg_pic2, onClick )
	end--]]
	--o:setAnchorPoint(0, 0)
	
	c2 = c.name
	c2.text = getCharString(tostring(GLOBAL.name),4)
	--print(c2.text)
	o = createLabel( ret_panel, c2 )
	--AdjustString(o,tostring(GLOBAL.name),80)
	ret_panel._name = o
	
	createSprite( ret_panel, c.gold_pic )
	
	o = createLabel( ret_panel, c.gold_text )
	ret_panel._gold = o
	
	local function clickTabBar( index )
		info_count = 0
		level = index + 1
		cur_page = 1			
		if level == 1 then  --友盟统计
			dailyCount('RoomDiaoSi')--初级场
		elseif level == 2 then
			dailyCount('RoomTuHao')--中级场
		elseif level == 3 then 
			dailyCount('RoomFuHao')--高级场
		end
		PANEL_CONTAINER.curLevel = level
		RoomPage.list:clear()
		RoomPage.list:reload()
		refreshPage()		
		--RoomListView:reload()
	end	
	local locX, locY = ret_panel.tab_bar_bg:getPosition()
	--TAB 按钮
	COMPONENT_BASE.clsSelectStateSlideTabBar:New(ret_panel, locX - 152, locY - 28, c.tab_bar.groups, c.tab_bar.width, c.tab_bar.space, c.tab_bar.button_num, 80, PANEL_CONTAINER.curLevel, clickTabBar )
		
	local function okHandler()
		setExsitAlert( false )
	end
	
	local function clickWineCupBtn()
		if ret_panel.arrow_bg_pic then
			clear( {ret_panel.arrow_bg_pic} )
			ret_panel.arrow_bg_pic = nil
		end
		createAlert( RoomPage, c.alert, okHandler )
	end
    	
	--回调用户信息
	local function onUserMoney( data )
		if data.code < 0 then
			return 
		end
		
		if RoomPage then
			ret_panel._gold:setString( data.data.gold )
			MYGold = tonumber(data.data.gold)
			NEW_GAME_HALL.myCoin = tonumber(data.data.gold)
			
			if exitWindow == true then
				exitWindow  = false
				if MYGold < 400 then
					--回调用户信息					
					local function onOk()			
						back_func(true)
						closePanel()
					end
					if tonumber( data.data.day_gold ) <= 0 then
						RoomConfig.get_reward_alert.cancel_btn.text = "知道了"
						createGetRewardAlert(ret_panel,RoomConfig.get_reward_alert,onOk, nil, 1 )	
					else
						RoomConfig.get_reward_alert.cancel_btn.text = "领取救济 (" .. data.data.day_gold .. "/3)"
						createGetRewardAlert(ret_panel,RoomConfig.get_reward_alert,onOk, nil, 2 )	
					end													
				else
					showExitAlert()
				end	
			end		
			
			currentDrunkenness = data.data.drunk
			if currentDrunkenness < 3 then
				c.wine_cup_pic.res = 'jzd-h.png'
			else
				c.wine_cup_pic.res = 'jzd-r.png'
			end	
			if not ret_panel.drunk_btn_responce_rect then
				if isBasePanel then
					c.drunk_btn_responce_rect.res = 'head.png'
				else
					c.drunk_btn_responce_rect.res = 'head.png'
				end
				--ret_panel.drunk_btn_responce_rect = createButton( ret_panel, c.drunk_btn_responce_rect, clickWineCupBtn )
				--ret_panel.drunk_btn_responce_rect:setVisible( false )
			end					
			
			if not ret_panel.wine_cup_pic then
				ret_panel.wine_cup_pic = createSprite( ret_panel, c.wine_cup_pic )
			else
				ret_panel.wine_cup_pic:setImage( c.wine_cup_pic.res )
			end
			
--[[			local roomData = getData( 'room' )
			if roomData then
				if roomData.firstClick and roomData.firstClick == true then
					firstEnter = false
					setData( 'room', 'firstClick', firstEnter )
					ret_panel.arrow_bg_pic = createSprite( ret_panel, c.arrow_bg_pic )
					a_play( ret_panel.arrow_bg_pic:getSprite(),c.arrow_animation,true)
				end
			else
				firstEnter = false
				setData( 'room', 'firstClick', firstEnter )
				ret_panel.arrow_bg_pic = createSprite( ret_panel, c.arrow_bg_pic )
				a_play( ret_panel.arrow_bg_pic:getSprite(),c.arrow_animation,true)
			end		--]]		
			
			c.wine_degree_txt.text = currentDrunkenness .. "/" .. totalDrunkenness
			if not ret_panel.wine_degree_txt then
				ret_panel.wine_degree_txt = createLabel( ret_panel, c.wine_degree_txt )
			end
			ret_panel.wine_degree_txt:setString( data.data.drunk .. "/" .. data.data.max_drunk )	
		end
	end
	
	local function updateDrunkTxt()
		doCommand( 'user', 'money', {}, onUserMoney, 5 )
	end
	
	if not drunkTimer then
		drunkTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( updateDrunkTxt, 5, false )
	end
	--请求用户资料
	updateDrunkTxt()
	--doCommand( 'user', 'money', {}, onUserMoney, 5 )
	
	if isBasePanel then
		if isBasePanel == true then	
			o = createButton( ret_panel, c.head_back )
			o.onTouchEnd = on_back
		end
	end	
	
	
	
			
	
	--clickTabBar( PANEL_CONTAINER.curLevel - 1 )
						
	o = create9Button( ret_panel, c.quick_start,doQuickSit )

	local function onSelectPage(RoomListView,page)
		cur_page = page		
		refreshPage()
	end
	
	local function onListBottom()
		cur_page = cur_page + 1
		refreshPage()
	end
	ret_panel.list = createListView( ret_panel, nil, nil, nil, nil, nil, nil, c.list )
	ret_panel.list.OnBottomRefresh = onListBottom
	
	--创建列表
	--[[RoomListView = createListView(ret_panel,0,0,480,display.sy-110,kCCScrollViewDirectionHorizontal,true)			
	local function onClick()
	end
	for i = 1,room_page do
		RoomListView:addItem(i,createItemByData,onClick)
	end
	RoomListView.onSelectPage = onSelectPage
	RoomListView:reload()--]]
	
	cur_page = 1
	
	ret_panel.closePanel = closePanel
	return ret_panel
end

function createItemByData(page)
	local nodeObj = createNode( nil, {} )
	local c, c2 = RoomConfig, nil		
	for i = 1,4 do	
		c2 = c["desk" .. i]
		n = createNode( nodeObj, {} )
		local info = nil
		if PageData[page] and PageData[page][i] then
			info = PageData[page][i]
		end
		createRoom(i,n,info)		
	end
	--d("createItemByData : " .. page)			
	return nodeObj,480,display.sy-110
end

local function createRoomListItem( info )
	local n = createNode( nil, { x = 0, y = 0 } )
	create9Sprite( n, RoomConfig.room_list_item_bg )	
	local str = info.no
	local len = string.len( str )
	if len >= 3 then
		RoomConfig.room_number_txt.text = str .. "房"
	elseif len == 2 then
		RoomConfig.room_number_txt.text = "0" .. str .. "房"
	else
		RoomConfig.room_number_txt.text = "00" .. str .. "房"
	end
	createLabel( n, RoomConfig.room_number_txt )
	RoomConfig.gold_txt.text = info.gold .. "金币"
	--createLabel( n, RoomConfig.gold_txt )
	local i = 1
	for i = 1, 4 do
		n.room_position = createSprite( n, RoomConfig.room_position )
		n.room_position:setPosition( RoomConfig.room_position.x + RoomConfig.room_position.xspace * ( i - 1 ), RoomConfig.room_position.y )
		if info[ "desk" .. i ] then
			local t = {}
			t.x = RoomConfig.room_position.x
			t.y = RoomConfig.room_position.y
			t.sx = RoomConfig.room_position.sx 
			t.sy = RoomConfig.room_position.sy
			if info[ "desk" .. i ][ "sex" ] == 0 then
				t.res = "sex0.png"
			else
				t.res = "sex1.png"
			end
			n.default = createSprite( n, t )
			n.default:setPosition( RoomConfig.room_position.x + RoomConfig.room_position.xspace * ( i - 1 ), RoomConfig.room_position.y)
			t.res = info[ "desk" .. i ].logo			
			n.logo = createSprite( n, t )
			n.logo:setPosition( RoomConfig.room_position.x + RoomConfig.room_position.xspace * ( i - 1 ), RoomConfig.room_position.y)
		end
		local t2 = {}
		t2.x = RoomConfig.room_position.x
		t2.y = RoomConfig.room_position.y
		t2.sx = RoomConfig.room_position.sx
		t2.sy = RoomConfig.room_position.sy
		t2.res = "kuangkuang-zd.png"
		n.headFrame = createSprite( n, t2 )
		n.headFrame:setPosition( RoomConfig.room_position.x + RoomConfig.room_position.xspace * ( i - 1 ), RoomConfig.room_position.y )
	end
	return n
end

function refreshPage()
	local function onRoomLists(data, refresh )
		if not RoomPage then 
			return
		end
		
		local function createItemInfo( idx, info )
			local n = createRoomListItem( info )
			return n, RoomConfig.list_item.width, RoomConfig.list_item.height
		end
		function onClick( idx, info )
			if string.len(tostring(info.ip)) > 0 and
				 tonumber(info.port) > 0 then		
				enterMatch2( info.id,info.ip,info.port,info.no )
			end
		end
		if refresh then
			info_count = info_count - #data.data
		end

		PageData[cur_page] = {}
		for k, info in ipairs(data.data) do
			RoomPage.list:updateItemAtIndex( info_count + k, info, createItemInfo, onClick)
			table.insert(PageData[cur_page],info)
		end
		--[[if RoomListView and PageData[cur_page] then
			RoomListView:updateCellAtIndex(cur_page,true)
		end--]]
		if cur_page == 1 then
			RoomPage.list:reload()
		else
			RoomPage.list:refresh()
		end
		info_count = info_count + #data.data
		--showPage()					
	end
	HTTP_REQUEST.cancel_download_pic() --清除未成功下载的图片
	doCommand( 'room', 'lists', { level = level, page = cur_page, size = 6 }, onRoomLists,1)
end

function showPanel(parent, x, y, isBasePanel )
	info_count = 0
	basePanel = isBasePanel
	if not RoomPage then
		RoomPage = createPanel(parent, x, y, isBasePanel)
		--RoomPage._move_big_page._move_big_grp:selectPage(1)		
	else
		RoomPage:setVisible(true)
	end
	
	--local COMPONENTBASE = Import( "lua/component_base.lua" )
	--RoomPage.list = COMPONENTBASE.clsHList:New( RoomPage, -10, 690, doRoomLists )
	refreshPage()
	local function okHandler()
		setExsitAlert( false )
		local function onPropsUses( data )	
			if data.code == 1 then
				currentDrunkenness = 0
				local function onUserMoney( data )
					currentDrunkenness = data.data.drunk
					if currentDrunkenness < 3 then
						RoomConfig.wine_cup_pic.res = 'jzd-h.png'
					else
						RoomConfig.wine_cup_pic.res = 'jzd-r.png'
					end	
					RoomPage.wine_cup_pic:setImage( RoomConfig.wine_cup_pic.res )
					RoomPage.wine_degree_txt:setString( data.data.drunk .. "/" .. data.data.max_drunk )
				end
				--请求用户资料
				doCommand( 'user', 'money', {}, onUserMoney )
			end
			showMessage( RoomPage, data.memo )
		end
		doCommand( 'props', 'uses', { props = 50, quantity = 1 }, onPropsUses )
	end		
	--createAlert( RoomPage, RoomConfig.drunk_alert, okHandler )
	
	local function drunkAlertHandler()
		if drunkAlertTimer then			
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(drunkAlertTimer)
			drunkAlertTimer = nil					
		end
		if popWindow == true then
			popWindow = false
			RoomConfig.drunk_alert.content3.text = "oh，北鼻，你被" .. winerName .. "灌醉了，赶紧喝解酒药解酒吧，不然要被人抱走咯，菊花残..满腚伤..."
			createAlert( RoomPage, RoomConfig.drunk_alert, okHandler )
		end	
	end
	if not drunkAlertTimer then
		drunkAlertTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( drunkAlertHandler, 4, false )	
	end
	
	--定时刷新列表
	--doRefresh()	
	
	local function onUserData( data )
		RoomConfig.my_head.res = data.data.logo
		createSprite( RoomPage, RoomConfig.my_head )
	end
	doCommand( 'user', 'detail', { id = GLOBAL.uid }, onUserData )	
	return RoomPage
end

function closePanel()
	if drunkTimer then	
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(drunkTimer)
		drunkTimer = nil
	end
	if RoomPage then

		--delete_back(RoomPage)

		HTTP_REQUEST.cancel_download_pic()
		
		setExsitAlert( false )
		clearTimer( "lists" )
		clear({RoomPage})
		RoomPage = nil
		--RoomListView = nil
	end
	--RoomPage:setVisible(false)
end

--快速游戏
function doQuickGame(mygold,ip,port,id,no,level)	
	local needGold = getBaseGold(level) * 4
	if mygold < needGold then
		CCMessageBox("你的金币不足入场要求，请充值",'')		
		back_func(true)
		--showAlert()
		return
	end
	local room_match = Import("lua/room_game.lua")		
	room_match.init( { level = level,ip = ip,port = port,
	room_id = id, chair_id = 1,
	room_no = no,back_func = back_func} )
	room_match.showPanel(HTTP_CLIENT.getRootNode(), 0, 0 )									
end
