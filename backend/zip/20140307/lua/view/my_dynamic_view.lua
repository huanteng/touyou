myDynamicView = {}

local obj = Import( "lua/util/cls_view.lua" )
myDynamicView = obj.clsView:Inherit()

local o = Import("layout/my_dynamic.lua")
local c = o.getConfigData()[LIGHT_UI.CurScreenSize]

--local MYRESULTINFO = Import( "lua/model/my_result_info.lua" )
--local MYRESULTCTRL = Import( "lua/ctrl/my_result_ctrl.lua" )

local myDynamicList = nil --列表

local id = 0

function myDynamicView:New( ... )
	obj.Super(myDynamicView).__init__( self, ... )
	return self
end	

local function createDynamicItemByInfo( idx, info, size ) --创建动态
	
	local ret_panel = LIGHT_UI.clsNode:New( nil, 0, 0 )

	local height = 0
	local content_obj = nil

	c.dynamic_time_c.text = info.time
	--c.dynamic_time_c.y = -1 * size.height
	local time_obj = createLabel( ret_panel, c.dynamic_time_c )

	if type( info.data) == "string" then
		c.dynamic_content_c.text = clearHTML( info.data )
		content_obj = createMultiLabel( ret_panel, c.dynamic_content_c )
	end
	
	if info.type == "7" then
		c.dynamic_content_c.text = "给" .. info.data.name .. "打了" .. info.data.score .. "分"
		content_obj = createMultiLabel( ret_panel, c.dynamic_content_c )

		c.pic_c.res = info.data.url
		createSprite( ret_panel, c.pic_c )
		height = 150
	elseif info.type == "1" then
		c.dynamic_content_c.text = "上传了新图片"
		content_obj = createMultiLabel( ret_panel, c.dynamic_content_c )
		
		c.pic_c.res = info.data.url
		createSprite( ret_panel, c.pic_c )
		height = 150
	end

	if not content_obj then
		c.dynamic_content_c.text = ''
		content_obj = createMultiLabel( ret_panel, c.dynamic_content_c )
	end
	local size = content_obj:getContentSize()	

	if height == 0 then
		height = math.abs( c.dynamic_content_c.y ) + size.height
	end

	height = height + c["simple_info_config"]["dynamic_info_margin"]

	c.dynamic_bg_c.sy = height
	create9Sprite( ret_panel, c.dynamic_bg_c )

	local function getContentSize( node )
		return { ["width"] = 480, ["height"] = height, }
	end

	ret_panel.getContentSize = getContentSize

	return ret_panel, 480, height
end

local function configDynamicInfo()
	for _, dynamic_info in pairs(dynamic_data) do
		local function onClick( idx, info )
			--onClickSmallItem(user_info["id"])	
			print( 11111111111111111 )
		end
		myDynamicList:addItem(dynamic_info,createDynamicItemByInfo,onClick)
	end
	if myDynamicList then
		--[[if cur_boy_page ~= 1 then		
			ChallengeList:refresh()
		else--]]
			myDynamicList:reload()
		--end
	end	
end

local function on_get_dynamic_info(data)
	dynamic_data = data.data
	if table.getn( dynamic_data ) > 0 then configDynamicInfo() end
end

local function request_dynamic_info(uid, page)
	if not page then local page = 1 end
	doCommand( 'user', 'moving', { kind = 3, page = page, size = 20, uid = uid }, on_get_dynamic_info )
end

local function OnTopRefresh()
			
end			
local function OnBottomRefresh()
	
end

function myDynamicView:initData()
	
	--MYRESULTINFO.myResultInfo:getInstance()
end

function myDynamicView:initView( parentPage, x, y, callBack )
	local ret_panel = createBasePanel( parentPage, x, y )
	local function on_back()
		if tonumber( id ) == tonumber( GLOBAL.uid ) then
			local path, pageId, className, childPath, childClassName, node, nodeAttribute = decodeBackTable()
				
			if PANEL_CONTAINER.closeChildPanel( nil, pageId ) then	--随意传个非空的table，该参数如果为空，就表明需要清除子面板，否则，就会考虑第2个参数							
				local o = Import( path )
				local f = o[ className ]:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)
				PANEL_CONTAINER.addChild( f:getMyMsgPage() )
				local g = Import( childPath )
				local h = g[ childClassName ]:getInstance( f[ node ][ nodeAttribute ] )
				h:secondInit()	
			end
		else
			local o = Import( "lua/view/other_msg_view.lua" )
			local f = o.otherMsgView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, id, callBack )
			f:secondInit()
		end
		self:closeMyDynamicPage()
	end
	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '动态' )
	
	myDynamicList = createListView(ret_panel,0,0,display.sx,display.sy-65)
	myDynamicList.OnTopRefresh = OnTopRefresh
	myDynamicList.OnBottomRefresh = OnBottomRefresh
	
	
	self.page = ret_panel
end

function myDynamicView:secondInit( uid )
	id = uid
	request_dynamic_info( uid )
end

function myDynamicView:initListener()
	
	--MYRESULTCTRL.myResultCtrl:getInstance()
end	

function myDynamicView:closeMyDynamicPage()
	--以下一句代码的删除写法是为了兼容panel_container的删除方式
	local o = myDynamicView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)	
	
	obj.Super(myDynamicView).close(o)	
end

function myDynamicView:disposeListener()
	--MYRESULTCTRL.myResultCtrl:getInstance():closeMyResultCtrl()	
end

function myDynamicView:disposeView()
	local o = myDynamicView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)
	clear( {o.page} )
	o.page = nil
	o = nil
end

function myDynamicView:disposeData()
	--MYRESULTINFO.myResultInfo:getInstance():closeMyResultInfo()	
end