myDetailView = {}

local obj = Import( "lua/util/cls_view.lua" )
myDetailView = obj.clsView:Inherit()

local o = Import("layout/my_detail.lua")
local c = o.getConfigData()[LIGHT_UI.CurScreenSize]

local MYDETAILINFO = Import( "lua/model/my_detail_info.lua" )
local MYDETAILCTRL = Import( "lua/ctrl/my_detail_ctrl.lua" )
local CHOOSE_CITY = Import("lua/choose_city.lua")
local CHOOSE_DAY = Import("lua/choose_day.lua")

--该页所需参数组
local paramArr = {}

local id = 0

function myDetailView:New( ... )
	obj.Super(myDetailView).__init__( self, ... )
	return self
end	

function myDetailView:initData()
	MYDETAILINFO.myDetailInfo:getInstance()
end

function myDetailView:initView( parentPage, x, y, callBack, uid )
	--local user_info = LOGIN_PANEL.getUserData()
	id = uid
	local ret_panel = createBasePanel( parentPage, x, y )
	paramArr = { parentPage = parentPage, x = x, y = y }
	local function on_back()
		if tonumber( id ) == tonumber( GLOBAL.uid ) then
			if PANEL_CONTAINER.closeChildPanel( nil, 16 ) then	--随意传个非空的table，该参数如果为空，就表明需要清除子面板，否则，就会考虑第2个参数
				local MYMSGVIEW = Import( "lua/view/my_msg_view.lua" )
				local f = MYMSGVIEW.myMsgView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)
				PANEL_CONTAINER.addChild( f:getMyMsgPage() )			
				local MYDATAVIEW = Import( "lua/view/my_data_view.lua" )
				local o = MYDATAVIEW.myDataView:getInstance( f.myMsgPage.myDataPage )
				o:secondInit()
			end
		else
			local o = Import( "lua/view/other_msg_view.lua" )
			local f = o.otherMsgView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, id, callBack )
			f:secondInit()
		end
		self:closeMyDetailPage()
	end
	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '详细资料' )
	
	for i=1,1 do
		local bg = replaceTable( c.bg, c[ 'bg_'..i] )
		createGrid( ret_panel, bg )
	end
	
	--[[local function loginForUpdate()
		local function onUserLogon( data )
			if data.code > 0 then
				--TODO:setData( 'login', 'user_data', data.data )
			end
		end
		local loginData = getData( 
		if loginData then
			doCommand( 'user', 'logon', { name = loginData.username, pass = loginData.password,version = CCUserDefault:sharedUserDefault():getStringForKey("engine"),imei=xymodule.get_imei() , channel = xymodule.getchannel() }, onUserLogon )
		end
	end--]]
	
	local function chooseCityHandler( value )
		--LOGIN_PANEL.setCity( value )
		--loginForUpdate()
	end
	
	local function openChooseCityPage()
		CHOOSE_CITY.showCityPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		CHOOSE_CITY.init( { ok_func = chooseCityHandler } )
		self:closeMyDetailPage()
	end
	
	local function modifyBirthdayHandler( value )
		--LOGIN_PANEL.setBirthday( value )
		--loginForUpdate()
	end
	
	local function openModifyBirthdayPage()
		CHOOSE_DAY.showDayPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		CHOOSE_DAY.init( { ok_func = modifyBirthdayHandler } )	
		self:closeMyDetailPage()
	end
	
	createLabel( ret_panel, c.base_text )
	createLabel( ret_panel, c.sex_text )	
	
	--c.birthday_btn.text = user_info["birthday"]
	local rc = replaceTable( c.label, c.birthday_btn )
	
	if tonumber( id ) == tonumber( GLOBAL.uid ) then
		createButton( ret_panel, c.birthday_responce_rect, openModifyBirthdayPage )
		ret_panel.birthday_btn = createButton( ret_panel, rc, openModifyBirthdayPage )
	else
		createButton( ret_panel, c.birthday_responce_rect )
		ret_panel.birthday_btn = createButton( ret_panel, rc )
	end
	--c.city_btn.text = user_info["province"] .. "," .. user_info["city"]
	rc = replaceTable( c.label, c.city_btn )
	
	if tonumber( id ) == tonumber( GLOBAL.uid ) then
		createButton( ret_panel, c.city_responce_rect, openChooseCityPage )
		createButton( ret_panel, rc, openChooseCityPage )
	else
		createButton( ret_panel, c.city_responce_rect )
		createButton( ret_panel, rc )
	end
	ret_panel.city_btn = createLabel( ret_panel, c.city_content )
	
	createLabel( ret_panel, c.birthday_text )
	createLabel( ret_panel, c.city_text )
	
	--[[if user_info.sex == "0" then
		c.sex_pic.res = "man.png"
	else
		c.sex_pic.res = "woman.png"
	end--]]
	ret_panel.sex_pic = createSprite( ret_panel, c.sex_pic )
	local i = 0
	for i = 1, 2 do		
		ret_panel.arrow_pic = createSprite( ret_panel, c.arrow_pic )
		locX, locY = ret_panel.arrow_pic:getPosition()
		ret_panel.arrow_pic:setPosition( locX, locY + ( ( i - 1 ) * c.arrow_pic.space ) )			
	end	
	
	self.page = ret_panel
end

function myDetailView:initListener()
	MYDETAILCTRL.myDetailCtrl:getInstance()
end	

function myDetailView:onUserData()
	local data = MYDETAILINFO.myDetailInfo:getInstance().myUserData
	local user_info = data.data
	myDetailView.page.birthday_btn:setString( user_info["birthday"] )
	myDetailView.page.city_btn:setString( user_info["province"] .. "," .. user_info["city"] )
	local s = ""
	if user_info.sex == "0" then
		s = "man.png"
	else
		s = "woman.png"
	end
	myDetailView.page.sex_pic:setImage( s )
end

function myDetailView:secondInit()
	
	local ctrl = MYDETAILCTRL.myDetailCtrl:getInstance()
	ctrl:requestUserDetail( self.onUserData, id )
end

function myDetailView:closeMyDetailPage()
	--以下一句代码的删除写法是为了兼容panel_container的删除方式
	local o = myDetailView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)	
	
	obj.Super(myDetailView).close(o)	
end

function myDetailView:disposeListener()
	MYDETAILCTRL.myDetailCtrl:getInstance():closeCtrl()	
end

function myDetailView:disposeView()
	local o = myDetailView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)
	clear( {o.page} )
	o.page = nil
	o = nil
end

function myDetailView:disposeData()
	MYDETAILINFO.myDetailInfo:getInstance():closeInfo()	
end