otherMsgView = {}

local obj = Import( "lua/util/cls_view.lua" )
otherMsgView = obj.clsView:Inherit()

local OTHERMSG_LAYOUT = Import("layout/other_msg.lua")
local otherMsgConfig = OTHERMSG_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local OTHERMSGINFO = Import( "lua/model/other_msg_info.lua" )
local OTHERMSGCTRL = Import( "lua/ctrl/other_msg_ctrl.lua" )
local MALL_PAGE = Import("lua/mall.lua")
--一对一聊天
local ONETOONE_CHAT_PANEL = Import("lua/one_to_one_chat.lua")

--该页所需参数组
local paramArr = {}

local OTHERDATAVIEW = Import( "lua/view/other_data_view.lua" )
local MYRESULTVIEW = Import( "lua/view/my_result_view.lua" )

local searching = false
local challenge_data = nil --挑战房间数据

function otherMsgView:New( ... )
	obj.Super(otherMsgView).__init__( self, ... )
	return self
end	

local exsitAlert = false
local function setExsitAlert( value )
	exsitAlert = value
end

local function getExsitAlert()
	return exsitAlert
end



function createAlertWith3Button(parent,c,onok,oncancel,onSecondOk, okState, secondOkState)
	if exsitAlert == false then
		exsitAlert = true
	else		
		clear({p})
		return
	end
	local order = 0
	if parent then
		order = parent:getCOObj():getChildrenCount() + 120
	end		
	local config = c or BASE_LAYOUT.getData().alert
	local p = createNode(parent,{x = 0,y = 0},nil,order)
	p:setAnchorPoint(0.5,0.5 )
	--[[local function receiveMsg()
		print('onclick')
	end--]]
	if config.bg then
		createButton(p,config.bg,nil)	
	end
	if config.window_head then
		p.windowHead = createSprite(p,config.window_head)
		local size = p.windowHead:getContentSize()
		config.window_body.y = config.window_head.y - size.height
		p.windowBody = createSprite(p,config.window_body)	
		config.window_tail.y = config.window_body.y - config.window_body.sy
		createSprite(p,config.window_tail)
	end	
	--[[if config.icon then
		createSprite(p,config.icon)
	end--]]
	if config.title then	
		createLabel(p,config.title)
	end
	if config.content then
		createLabel(p,config.content)
	end
	
	if config.content2 then
		createLabel(p,config.content2)
	end
	
	if config.content3 then
		createMultiLabel(p,config.content3)
	end
	
	local function onClickOk()
		exsitAlert = false
		if okState then
			clear({p})		
			if onok then
				onok()
			end		
		end	
	end
	
	local function onClickSecondOk()
		exsitAlert = false
		if secondOkState then
			clear({p})		
			if onSecondOk then
				onSecondOk()
			end		
		end	
	end
	
	local function onClickCancel()	
		
		exsitAlert = false
		if oncancel then
			oncancel()
		end
		clear({p})
	end
	
	if config.cancel_btn then
		create9Button(p,config.cancel_btn,onClickCancel)
	end
	if config.ok_btn then
		if okState then
			config.ok_btn.css = "blue_btn"
			create9Button(p,config.ok_btn,onClickOk)
		else
			config.ok_btn.css = "gray_btn"
			create9Button(p,config.ok_btn,onClickOk)
		end
	end
	if config.second_ok_btn then
		if secondOkState then
			config.second_ok_btn.css = "blue_btn"
			create9Button(p,config.second_ok_btn,onClickSecondOk)
		else
			config.second_ok_btn.css = "gray_btn"
			create9Button(p,config.second_ok_btn,onClickSecondOk)
		end
	end
	
	return p
end	

local function configSimpleInfo(user_id)
	local all_player_info = OTHERMSGINFO.otherMsgInfo:getInstance().all_player_info
	local player_info = all_player_info[user_id]
	--[[local simple_info_panel = playerInfoPage._move_info_page._player_simple_info
	if not LIGHT_UI.isPNGURL(player_info["logo"]) then	
		simple_info_panel._user_pic:setImage(player_info["logo"])
		simple_info_panel._user_pic:setVisible(true)
	end

	local vip_pic_x = 128

	if player_info["vip_id"] ~= "0" then
		local vip_logo = string.format( 'vip%i.png', player_info["vip_id"] )
		simple_info_panel._vip_pic = LIGHT_UI.clsSprite:New(simple_info_panel, vip_pic_x, 0, vip_logo)
		simple_info_panel._vip_pic:setAnchorPoint(0, 1)
	else
		if simple_info_panel._vip_pic then simple_info_panel._vip_pic:setVisible(false) end
	end

	if player_info["is_real"] == "0" then
		simple_info_panel._photo_hint:setString( "还未视频认证" )
		if simple_info_panel._video_auth then simple_info_panel._video_auth:setVisible(false) end
	else
		simple_info_panel._photo_hint:setString( " " )
		
		if player_info["vip_id"] ~= "0" then
			vip_pic_x = vip_pic_x + 60			
		end

		simple_info_panel._video_auth = LIGHT_UI.clsSprite:New( simple_info_panel, vip_pic_x, 5, "vedioauth_pic.png" )
		simple_info_panel._video_auth:setAnchorPoint( 0, 1 )
	end					
	playerInfoPage.name:setString( player_info["name"] )
	
	
	--控制显示用户心情显示字数
	local str = getCharString(player_info["mood"],200);	
	local count = (#str);	
	if count > 167 then
	str = getCharString(player_info["mood"],46) .. "..."
	end		
	simple_info_panel._remark_txt:setString(str)
	simple_info_panel._remark_txt:setVisible(true)
	
	if player_info["sex"] == "1" then 
		simple_info_panel._sex_value:setImage("woman.png")
		else
		simple_info_panel._sex_value:setImage("man.png")
	 end
	
	simple_info_panel._sex_value:setVisible(true)
	
	simple_info_panel._age_value:setString(player_info["age"])
	simple_info_panel._age_value:setVisible(true)

	simple_info_panel._charm_value:setString(player_info["charm"])
	simple_info_panel._charm_value:setVisible(true)

	simple_info_panel._city_value:setString(player_info["city"])
	simple_info_panel._city_value:setVisible(true)

	simple_info_panel._height_value:setString(player_info["height"])
	simple_info_panel._height_value:setVisible(true)

	simple_info_panel._birthday_value:setString(player_info["birthday"])
	simple_info_panel._birthday_value:setVisible(true)

	local incomeTxt = getCharString( player_info["income"], 10 )
	simple_info_panel._income_value:setString( incomeTxt )
	simple_info_panel._income_value:setVisible(true)

	simple_info_panel._hobby_value:setString(player_info["interest"])
	simple_info_panel._hobby_value:setVisible(true)
	
	local careerTxt = getCharString( player_info["profession"], 10 )
	simple_info_panel._profession_value:setString( careerTxt )
	simple_info_panel._profession_value:setVisible(true)

	local barTxt = getCharString( player_info["bar"], 10 )
	simple_info_panel._club_value:setString( barTxt )
	simple_info_panel._club_value:setVisible(true)

	if tonumber(player_info["frendship"]) == 1 then
		is_friend = true
		playerInfoPage._bottom_panel._focus_btn:setClickStatus()
	else
		is_friend = false
		playerInfoPage._bottom_panel._focus_btn:setUnClickStatus()
	end--]]
	
	--挑战
	if player_info['roomstate'] then
		challenge_data = {}
		challenge_data.ip = player_info['roomstate'].ip
		challenge_data.port = player_info['roomstate'].port
		challenge_data.id = player_info['roomstate'].id
		challenge_data.no = player_info['roomstate'].no
		challenge_data.level = player_info['roomstate'].level
		
		is_challenge = true
		--[[if playerInfoPage._bottom_panel._challenge_btn then
			playerInfoPage._bottom_panel._challenge_btn:setUnClickStatus()
		end--]]
	else
		challenge_data = nil
		is_challenge = false
		--[[if playerInfoPage._bottom_panel._challenge_btn then
			playerInfoPage._bottom_panel._challenge_btn:setClickStatus()
		end--]]
	end

	--[[local player_id = player_info["id"]
	if not small_pic_info[player_id] then
		request_small_pic(player_id)
	else
		configSmallPic(player_id)
	end--]]
end



function createAlertWithTime(parent,c,onok,oncancel)
	if exsitAlert == false then
		exsitAlert = true
	else		
		clear({p})
		return
	end
	PANEL_CONTAINER.timeAlertExist = true
	local order = 0
	if parent then
		order = parent:getCOObj():getChildrenCount() + 120
	end		
	local BASE_LAYOUT = Import("layout/base.lua")
	local config = c or BASE_LAYOUT.getData().time_alert
	local p = createNode(parent,{x = 0,y = 0},nil,order)
	p:setAnchorPoint(0.5,0.5 )
	--[[local function receiveMsg()
		print('onclick')
	end--]]
	if config.bg then
		createButton(p,config.bg,nil)	
	end
	if config.window_head then
		p.windowHead = createSprite(p,config.window_head)
		local size = p.windowHead:getContentSize()
		config.window_body.y = config.window_head.y - size.height
		p.windowBody = createSprite(p,config.window_body)	
		config.window_tail.y = config.window_body.y - config.window_body.sy
		createSprite(p,config.window_tail)
	end	
	--[[if config.icon then
		createSprite(p,config.icon)
	end--]]
	if config.title then	
		createLabel(p,config.title)
	end
	if config.content then
		createLabel(p,config.content)
	end
	
	if config.content2 then
		createLabel(p,config.content2)
	end
	
	if config.content3 then
		createMultiLabel(p,config.content3)
	end
	
	local function onClickOk()	
			
		if onok then
			onok()
		end			
	end
	
	local function onClickCancel()	
		if challengeTimer then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(challengeTimer)
			challengeTimer = nil
		end
		exsitAlert = false
		if oncancel then
			oncancel()
		end
		clear({p})
	end
	
	if config.cancel_btn then
		create9Button(p,config.cancel_btn,onClickCancel)
	end
	if config.ok_btn then
		create9Button(p,config.ok_btn,onClickOk)
	end
	
	p.timeLb = createLabel(p,config.time)	
	
	local count = tonumber( config.time.text ) - 1
	local function timeHandler()
		p.timeLb:setString( tostring( count ) )
		if count <= 0 then
			if challengeTimer then
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(challengeTimer)
				challengeTimer = nil
			end
			onClickCancel()
		else
			if PANEL_CONTAINER.timeAlertExist == false then
				if challengeTimer then
					CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(challengeTimer)
					challengeTimer = nil
				end
				onClickCancel()
			end
		end	
		count = count - 1
	end
	if not challengeTimer then
		challengeTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( timeHandler, 1, false )
	end
	
	return p
end

--发起挑战(发起者，应战者，挑战金类型，挑战金数量)
function doChallenge(uidFrom,nameFrom,uidTo,nType,nCount,szLogo)
	local arg = "{\"from\":" .. uidFrom .. ",\"sender_name\":\"" .. nameFrom .. "\",\"to\":" .. uidTo .. ",\"challenge_money_type\":" .. nType .. ",\"count\":" .. nCount .. ",\"sender_logo\":\"" .. szLogo .. "\",\"kind\":\"1\",\"type\":\"1\",\"time\":\"[" .. tostring(os.time()) .. "]\"}"
	tcp_pop(msg_type["challenge"],arg)
end

function createQuickPayCoinAlert(parent,c,onok,oncancel)
	if exsitAlert == false then
		exsitAlert = true
	else		
		clear({p})
		return
	end
	local user_info = getUserData()	
	local ownGold = NEW_GAME_HALL.myGold
	local order = 0
	if parent then
		order = parent:getCOObj():getChildrenCount() + 120
	end		
	local BASE_LAYOUT = Import("layout/base.lua")
	local config = c or BASE_LAYOUT.getData().alert
	local p = createNode(parent,{x = 0,y = 0},nil,order)
	p:setAnchorPoint(0.5,0.5 )
	--[[local function receiveMsg()
		print('onclick')
	end--]]
	if config.bg then
		createButton(p,config.bg,nil)	
	end
	if config.window_head then
		p.windowHead = createSprite(p,config.window_head)
		local size = p.windowHead:getContentSize()
		config.window_body.y = config.window_head.y - size.height
		p.windowBody = createSprite(p,config.window_body)	
		config.window_tail.y = config.window_body.y - config.window_body.sy
		createSprite(p,config.window_tail)
	end	
	--[[if config.icon then
		createSprite(p,config.icon)
	end--]]
	if config.title then	
		createLabel(p,config.title)
	end
	if config.content then
		createLabel(p,config.content)
	end
	
	if config.content2 then
		p.needPay = createLabel(p,config.content2)
	end
	
	if config.content3 then
		p.vipTip = createLabel(p,config.content3)
	end
	
	if config.content4 then
		config.content4.text = "账户可支付元宝:" .. ownGold
		createLabel(p,config.content4)
	end
	
	local discount
	if user_info["vip_id"] == "1" then
		discount = 0.9		
		p.vipTip:setString( "尊敬的vip用户，您已享受" .. discount * 10 .. "折优惠" )
		p.needPay:setString( "需要支付：" .. discount * 100 .. "元宝" )
			
	elseif user_info["vip_id"] == "2" then
		discount = 0.85
		p.vipTip:setString( "尊敬的vip用户，您已享受" .. discount * 10 .. "折优惠" )
		p.needPay:setString( "需要支付：" .. discount * 100 .. "元宝" )
			
	elseif user_info["vip_id"] == "3" then
		discount = 0.8
		p.vipTip:setString( "尊敬的vip用户，您已享受" .. discount * 10 .. "折优惠" )
		p.needPay:setString( "需要支付：" .. discount * 100 .. "元宝" )
		
	else
		discount = 1
		p.vipTip:setString( "开通vip可享受折扣优惠哦" )
		p.needPay:setString( "需要支付：" .. discount * 100 .. "元宝" )
	end	
	
	local function on_other_pay_cb(data)
		--[[local item_info = item_info_list[itemPage._item_id]
		local kind = tonumber(item_info["kind"])		--]]
		
		--[[local function onMsg( data )	
			if kind == 5 then
				USER_PACKAGE_PANEL.showPackageInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, nil, nil, 1 )				
			else
				ItemConfig.let_yb.text = data.data.bullion
			end					
			itemPage._move_big_page.letYbTxt:setString( ItemConfig.let_yb.text )
			closeItemPagePanel()
		end	--]]
		if data.code == 1 then
			showMessage( otherMsgView:getInstance().page, data.memo, {ms = 2000,y=300,})
			local function onPropsUses( data )
				NEW_GAME_HALL.myCoin = NEW_GAME_HALL.myCoin + 1200
				showUpFloorMessage( otherMsgView:getInstance().page, data.memo,{ms = 3000} )
			end
			doCommand( 'props', 'uses', { props = 46, quantity = 1 }, onPropsUses )
			--[[if kind ~= 3 then
				doCommand( 'user', 'money', {}, onMsg )
			end--]]
		else
			showMessage( otherMsgView:getInstance().page, data.memo .. "，请及时充值！", {ms = 3000,y=300,})
		end	
	end
	
	local function onClickOk()
		doCommand( 'props', 'buy', 
				{ target_uid = user_info.id, count = 1, props_id = 46 }, on_other_pay_cb )
		exsitAlert = false
		clear({p})		
		if onok then
			onok()
		end			
	end
	
	local function onClickCancel()	
		exsitAlert = false
		if oncancel then
			oncancel()
		end
		clear({p})
	end
	
	if config.cancel_btn then
		create9Button(p,config.cancel_btn,onClickCancel)
	end
	if config.ok_btn then
		create9Button(p,config.ok_btn,onClickOk)
	end
	
	return p
end	

function createQuickPayGoldAlert(parent,c,onok,oncancel)
	if exsitAlert == false then
		exsitAlert = true
	else		
		clear({p})
		return
	end
	local user_info = getUserData()
	local order = 0
	if parent then
		order = parent:getCOObj():getChildrenCount() + 120
	end		
	local BASE_LAYOUT = Import("layout/base.lua")
	local config = c or BASE_LAYOUT.getData().alert
	local p = createNode(parent,{x = 0,y = 0},nil,order)
	p:setAnchorPoint(0.5,0.5 )
	--[[local function receiveMsg()
		print('onclick')
	end--]]
	if config.bg then
		createButton(p,config.bg,nil)	
	end
	if config.window_head then
		p.windowHead = createSprite(p,config.window_head)
		local size = p.windowHead:getContentSize()
		config.window_body.y = config.window_head.y - size.height
		p.windowBody = createSprite(p,config.window_body)	
		config.window_tail.y = config.window_body.y - config.window_body.sy
		createSprite(p,config.window_tail)
	end	
	--[[if config.icon then
		createSprite(p,config.icon)
	end--]]
	if config.title then	
		createLabel(p,config.title)
	end
	if config.content then
		createLabel(p,config.content)
	end
	
	if config.content2 then
		createLabel(p,config.content2)
	end
	
	if config.content3 then
		createLabel(p,config.content3)
	end		
	
	local function onClickOk()
		exsitAlert = false
		clear({p})		
		if onok then
			onok()
		end			
	end
	
	local function onClickCancel()	
		exsitAlert = false
		if oncancel then
			oncancel()
		end
		clear({p})
	end
	
	if config.cancel_btn then
		create9Button(p,config.cancel_btn,onClickCancel)
	end
	if config.ok_btn then
		create9Button(p,config.ok_btn,onClickOk)
	end
	
	local function onPropsUses( data )
		NEW_GAME_HALL.myGold = NEW_GAME_HALL.myGold + 1000
		showUpFloorMessage( parent, data.memo, {ms=3000} )
	end
			
	--支付结果
	local function onPayFinish(data)
		if data == "ok" then		
			doCommand( 'props', 'uses', { props = 45, quantity = 1 }, onPropsUses )							
		else
			showUpFloorMessage( parent, data )
		end
	end
	local function onDelayPayFinish(data)
		f_delay_do(parent,onPayFinish,data,0.1)
	end
	--支付宝
	local function doAlipay()
		onClickCancel()	
		--doCommand( 'props', 'uses', { props = 45, quantity = 1 }, onPropsUses )
		dailyCount('ApplayForZhifubao')--支付宝
		local function cb(data)	
			--if g_order then
				local order_number = data.no
				local order_name = "小袋元宝"
				local order_price = 10
				--onPayFinish( "ok" )			
				xymodule.aliPay("buy", order_name,order_price,order_number, onDelayPayFinish)
			--end
		end		
		
		doCommand( 'props', 'payment_no', 
		{ props = 45, target_uid = tonumber(user_info.id), 
		count = 1, types = 0 }, cb )
	end
	
	--盛付通
	local function doShengpay()
		onClickCancel()	
		dailyCount('ApplayForChengfu')--盛付通
		local function cb(data)
			--if g_orde then 
				local order_number = data.no
				local order_name = "小袋元宝"
				local order_price = 10		
				xymodule.shengPay("buy",order_name,order_price,order_number, onDelayPayFinish)
			--end
		end
		
		doCommand( 'props', 'payment_no', 
		{ props = 45, target_uid = tonumber(user_info.id), 
		count = 1, types = 2 }, cb )
	end	
	
	createButton(p,config.zhifubao_btn,doAlipay)	
	createButton(p,config.chengfutong_btn,doShengpay)	
	
	return p
end	

function createChallengeSetttingAlert(parent,c,onok,oncancel, npc, hasReward )
	if exsitAlert == false then
		exsitAlert = true
	else		
		clear({p})
		return
	end
	local order = 0
	local tipExsit = false
	local ownCoin = NEW_GAME_HALL.myCoin
	local ownGold = NEW_GAME_HALL.myGold
	local page = nil
	local uid = OTHERMSGINFO.otherMsgInfo:getInstance().uid
	
	if parent then
		order = parent:getCOObj():getChildrenCount() + 120
	end		
	local BASE_LAYOUT = Import("layout/base.lua")
	local config = c or BASE_LAYOUT.getData().challenge_settting_alert
	
	local money_percent = config.slide.default_percent / 100
	
	local p = createNode(parent,{x = 0,y = 0},nil,order)
	p:setAnchorPoint(0.5,0.5 )
	--[[local function receiveMsg()
		print('onclick')
	end--]]
	if config.bg then
		createButton(p,config.bg,nil)	
	end

	if config.window_head then
		p.windowHead = createSprite(p,config.window_head)
		local size = p.windowHead:getContentSize()
		config.window_body.y = config.window_head.y - size.height
		p.windowBody = createSprite(p,config.window_body)	
		config.window_tail.y = config.window_body.y - config.window_body.sy
		createSprite(p,config.window_tail)
	end	
	createSprite(p,config.white_bg)
	--[[if config.icon then
		createSprite(p,config.icon)
	end--]]
	if config.title then	
		createLabel(p,config.title)
	end			
	if config.content then
		p.percentLb = createLabel(p,config.content)
		p.percentLb:setVisible( false )
	end
	
	if config.content2 then
		p.contentLb = createLabel(p,config.content2)
		p.contentLb:setVisible( false )
	end
	
	if config.content3 then
		createMultiLabel(p,config.content3)
	end
	
	local function onClickOk()
		exsitAlert = false
		if onok then
			onok()
		end	
		clear({p})	
		if tipExsit == true then
			if page == 1 then
--[[				local user_info = getUserData()
				local MALL_PANEL = Import("lua/mall.lua")
				MALL_PANEL.showMallPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, nil, user_info.id, true )
				MALL_PANEL.switchTabBar( 2 )--]]
				--createQuickPayCoinPanel( parent, ownGold )
				createQuickPayCoinAlert( parent, otherMsgConfig.quick_pay_coin_alert )
			end
			if page == 2 then			
				--[[local MALL_PANEL = Import("lua/mall.lua")
				if PANEL_CONTAINER.closeChildPanel( nil, 6 ) then
					PANEL_CONTAINER.addChild( MALL_PANEL.showMallPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
				end
				MALL_PANEL.switchTabBar( 1 )
				closePlayerInfoPagePanel()--]]
				createQuickPayGoldAlert( parent, otherMsgConfig.quick_pay_gold_alert )
			end
			
		else
			local function onCancel()
				--if tonumber( npc ) ~= 1 then
					local user_info = getUserData()
					local arg = "{\"from\":" .. tonumber( user_info.id ) .. ",\"to\":" .. tonumber( uid ) .. "}"
					tcp_pop(msg_type["cancelchallenge"],arg)
				--end
			end
			createAlertWithTime(parent,otherMsgConfig.time_alert, nil, onCancel )
			if page == 1 then
				local user_info = getUserData()
				--[[local MALL_PANEL = Import("lua/mall.lua")
				MALL_PANEL.showMallPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, nil, user_info.id, true )
				MALL_PANEL.switchTabBar( 2 )--]]
				--createQuickPayCoinPanel( parent, ownGold )
				PANEL_CONTAINER.challengeFrom = 1
				--if tonumber( npc ) ~= 1 then				
					doChallenge( tonumber( GLOBAL.uid ), GLOBAL.name, tonumber( uid ), 1,
					tonumber( math.floor( money_percent * ( ownCoin / 4  - 100 ) + 100 ) ), user_info.logo )
				--end
				
			end
			if page == 2 then
				local user_info = getUserData()
				PANEL_CONTAINER.challengeFrom = 1
				if tonumber( npc ) ~= 1 then				
					doChallenge( tonumber( GLOBAL.uid ), GLOBAL.name, tonumber( uid ), 2,
					tonumber( math.floor( money_percent * ( ownGold / 4  - 10 ) + 10 ) ), user_info.logo )
				end
			end
			--doQuickSit()
			
		end
	end
	
	local function onClickCancel()	
		print( "page:" .. page )
		print( "hasReward:" .. hasReward )
		if page == 1 and hasReward == 2 then
			local function getReward( data )
				if data.code == 1 then
					local function initRewardBtn( data )
						--p.cancelBtn:setString( "领取金币 (" .. data.data.day_gold .. "/3)" )
						NEW_GAME_HALL.myCoin = tonumber( data.data.gold )
					end
					doCommand( 'user', 'money', {}, initRewardBtn )
					--updateGoldText()
					showUpFloorMessage( parent, "又有本钱啦，骰魔重现江湖！", {ms=3000} )
				elseif data.code == -1 then			
					showUpFloorMultiMessage( parent, "今天限额用完了哦，需要更多弹药请到商城补充吧", {ms=3000} )
				elseif data.code == -2 then			
					showUpFloorMultiMessage( parent, "低于400才需要领救济，您这样的高富帅就不用了吧？", {ms=3000} )
				end
			end
			doCommand( 'user', 'day_gold', {}, getReward )	
		end
		exsitAlert = false
		if oncancel then
			oncancel()
		end
		clear({p})
	end
	
	if config.cancel_btn then
		p.cancelBtn = create9Button(p,config.cancel_btn,onClickCancel)
		--p.cancelBtn:setVisible( false )
	end
	if config.ok_btn then
		p.okBtn = create9Button(p,config.ok_btn,onClickOk)
		--p.okBtn:setVisible( false )
	end
	
		
	
	local function switchHandle( moneyType )
		if moneyType == 1 then
			if ownCoin then
				if tonumber( ownCoin ) >= 4 * 100 then
					p.slide:setVisible( true )
					p.tip:setVisible( false )
					p.percentLb:setVisible( true )
					p.contentLb:setVisible( true )
					p.okBtn:setVisible( true )
					p.okBtn:setString( "确定" )
					tipExsit = false
					p.slide:switchSlideBlockImage( {res='res/tzdjb.png',on = 'res/tzdjb.png'} )
					p.percentLb:setString( tostring( math.floor( config.slide.default_percent / 100 * ( ownCoin / 4  - 100 ) + 100 ) ) )	
					
					p.cancelBtn:setString( "取消" )
				else
					p.slide:setVisible( false )
					p.tip:setVisible( true )
					p.tip:setString( "你的金币不足400，已濒临破产，你可通过以下方式逆转人生；或选择其他类型挑战" )
					p.percentLb:setVisible( false )
					p.contentLb:setVisible( false )
					p.okBtn:setVisible( true )
					p.okBtn:setString( "补充弹药" )
					tipExsit = true
					p.cancelBtn:setString( otherMsgConfig.challenge_settting_alert.cancel_btn.text )
				end
			end
			p.cancelBtn:setPosition( 325, 212 )			
		end
		if moneyType == 2 then
			if ownGold then
				if tonumber( ownGold ) >= 4 * 10 then
					p.slide:setVisible( true )
					p.tip:setVisible( false )
					p.percentLb:setVisible( true )
					p.contentLb:setVisible( true )
					p.okBtn:setVisible( true )
					p.okBtn:setString( "确定" )					
					tipExsit = false
					p.slide:switchSlideBlockImage( {res='res/tzdyb.png',on = 'res/tzdyb.png'} )
					p.percentLb:setString( tostring( math.floor( config.slide.default_percent / 100 * ( ownGold / 4 - 10 ) + 10 ) ) )
					p.cancelBtn:setString( "取消" )
				else
					p.slide:setVisible( false )
					p.tip:setVisible( true )
					p.tip:setString( "元宝不足，请选择其他类型的挑战金" )
					p.percentLb:setVisible( false )
					p.contentLb:setVisible( false )
					p.okBtn:setVisible( true )
					p.okBtn:setString( "补充弹药" )					
					tipExsit = true
					p.cancelBtn:setString( "取消" )
				end
			end
			p.cancelBtn:setPosition( 325, 212 )	
		end
		if moneyType == 3 then
			p.slide:setVisible( false )
			p.tip:setVisible( true )
			p.tip:setString( "该功能暂未开放" )
			p.percentLb:setVisible( false )
			p.contentLb:setVisible( false )
			p.okBtn:setVisible( false )	
			p.cancelBtn:setPosition( 235, 212 )		
			p.cancelBtn:setString( "取消" )
		end
	end	
	
	local function clickTabBar( index )	
		if index == 0 then		--	金币	
			page = 1
			switchHandle( 1 )	
					
		end
		if index == 1 then		--	元宝		
			page = 2
			switchHandle( 2 )
			
		end
		if index == 2 then		--宝石
			page = 3
			switchHandle( 3 )			
		end
	end
	local COMPONENT_BASE = Import( "lua/component_base.lua" )
	p.tabBar = COMPONENT_BASE.clsSelectStateSlideTabBar:New(p, config.tab_bar.x, config.tab_bar.y, config.tab_bar.groups, config.tab_bar.width, config.tab_bar.space, 4, 80, 1, clickTabBar )
	--p.tabBar:setVisible( false )
	
	local function slideHandler( percent )	
		money_percent = percent
		if page == 1 then		
			p.percentLb:setString( tostring( math.floor( percent * ( ownCoin / 4  - 100 ) + 100 ) ) )
		end
		if page == 2 then
			p.percentLb:setString( tostring( math.floor( percent * ( ownGold / 4 - 10 ) + 10 ) ) )
		end
	end
	p.slide = COMPONENT_BASE.clsSlide:New(p, config.slide.x, config.slide.y, {res="res/tzdjb.png",on="res/tzdjb.png"}, config.slide.width, config.slide.default_percent, 

slideHandler )
	p.slide:setVisible( false )
	
	p.tip = createMultiLabel(p,config.tip)
	p.tip:setVisible( false )
	
	if ownCoin and ownGold then
		clickTabBar( 0 )
	else
		p.tabBar:setVisible( false )
		p.okBtn:setVisible( false )
		p.cancelBtn:setVisible( false )
		--回调用户信息
		local function onUserMoney( data )
			if data.data.gold then
				ownCoin = data.data.gold
				MYGold = data.data.gold
			end
			if data.data.bullion then
				ownGold = data.data.bullion
			end
			clickTabBar( 0 )
			p.tabBar:setVisible( true )
			p.okBtn:setVisible( true )
			p.cancelBtn:setVisible( true )
		end
		doCommand( 'user', 'money', {}, onUserMoney, nil, { loading = 1 } )
	end	
	
	return p
end

local function on_add_friend(data)
	dailyCount('PlayerInfoCare')--关注
	if not otherMsgView:getInstance().page then
		return
	end
	otherMsgView:getInstance().page._bottom_panel._focus_btn:setClickStatus()
	showMessage( otherMsgView:getInstance().page, '成功关注' )
	is_friend = true
end

local function on_del_friend(data)
	dailyCount('PlayerInfoUnCare')--取消关注
	otherMsgView:getInstance().page._bottom_panel._focus_btn:setUnClickStatus()
	showMessage( otherMsgView:getInstance().page, '取消关注' )
	is_friend = false
end

local function createBottomPanel(parent, x, y)
	local uid = OTHERMSGINFO.otherMsgInfo:getInstance().uid
	local all_player_info = OTHERMSGINFO.otherMsgInfo:getInstance().all_player_info[ uid ]
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	ret_panel._bottom_pic = LIGHT_UI.clsSprite:New(ret_panel, 0, 0, "mmfooter_bg.png")
	ret_panel._bottom_pic:setAnchorPoint(0, 0)

	local align_tbl = {}
	local function onGift(btn, x, y)
		btn:setClickStatus()
		if uid then
			
			local function back_func()
				MALL_PAGE.closeMallPagePanel()
				local path, params, className = decodeBackTable()
				local parentPage, x, y, uid
				if params.parentPage then
					parentPage = params.parentPage
				end
				if params.x then
					x = params.x
				end
				if params.y then
					y = params.y
				end
				if params.uid then
					uid = params.uid
				end
				local o = Import( path )
				o[ className ]:getInstance( parentPage, x, y, uid )
			end
			dailyCount('PlayerInfoGift')--送礼			

			playerId = uid
			MALL_PAGE.showMallPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, back_func, uid, true )
			otherMsgView:closeOtherMsgPage()
			add_back( 2, "lua/view/other_msg_view.lua", paramArr, nil, nil, "otherMsgView" )
		end
	end
	ret_panel._gift_page_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, "toosbar_gift_bt_bg.png", "toosbar_gift_bt_bg.png") 
	ret_panel._gift_page_btn:setAnchorPoint(0, 0)
	ret_panel._gift_page_btn:setString("")  --送礼修改地方
	ret_panel._gift_page_btn.onTouchEnd = onGift
	table.insert(align_tbl, ret_panel._gift_page_btn)

	local function onChat(btn, x, y)
		dailyCount('PlayerInfoChat')--一对一聊天
		btn:setClickStatus()
		--增加一对一聊天功能实现
		if uid then
			--local player_info = getPlayerInfo(playerInfoPage._user_id)
			uid = OTHERMSGINFO.otherMsgInfo:getInstance().uid
			all_player_info = OTHERMSGINFO.otherMsgInfo:getInstance().all_player_info[ uid ]
			local player_info = all_player_info
			--if player_info then

				local function back_func()
					--ONETOONE_CHAT_PANEL.closeOneToOneChatPagePanel()
					local path, params, className = decodeBackTable()
					local parentPage, x, y, uid
					if params.parentPage then
						parentPage = params.parentPage
					end
					if params.x then
						x = params.x
					end
					if params.y then
						y = params.y
					end
					if params.uid then
						uid = params.uid
					end
					local o = Import( path )
					o[ className ]:getInstance( parentPage, x, y, uid )
				end
				
				playerId = uid
			    if player_info then	
					ONETOONE_CHAT_PANEL.init( { uid2 = uid, name2 = player_info.name, back_func = back_func } )
	            end
				ONETOONE_CHAT_PANEL.showPanel(HTTP_CLIENT.getRootNode(), 0, 0 )
				otherMsgView:closeOtherMsgPage()
				add_back( 2, "lua/view/other_msg_view.lua", paramArr, nil, nil, "otherMsgView" )
			--end
		end			
	end
	ret_panel._chat_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, "toosbar_chat_bt_bg.png", "toosbar_chat_bt_bg.png") 
	ret_panel._chat_btn:setAnchorPoint(0, 0)
	ret_panel._chat_btn:setString("")  --聊天修改地方	
	ret_panel._chat_btn.onTouchEnd = onChat
	table.insert(align_tbl, ret_panel._chat_btn)
	
	local function onChallenge(btn, x, y)	
		if is_challenge == false then
			--btn:setClickStatus()
		else
			--btn:setUnClickStatus()
			local ROOMMatch = Import("lua/room.lua")
			ROOMMatch.doQuickGame(tonumber(NEW_GAME_HALL.myCoin),challenge_data.ip,challenge_data.port,
			                      challenge_data.id,challenge_data.no,tonumber( challenge_data.level ))
			otherMsgView:closeOtherMsgPage()
		end
		--[[
		if playerInfoPage._user_id then
			local ROOM_PANEL = Import("lua/room.lua")
			ROOM_PANEL.showPanel(HTTP_CLIENT.getRootNode(), 0, 0, true)		--这个面板不销毁，到时返回时只需要将现在这个新建的面板销毁即可
		end
		]]
	end		
	
	local function challengeHandler( btn, x, y )	
		local function onUserMoney( data )
			searching = false
			if data.data.gold then
				NEW_GAME_HALL.myCoin = tonumber( data.data.gold )
			end
			if data.data.drunk >= 3 then
				local function okHandler()
					setExsitAlert( false )
					local function onPropsUses( data )
						if data.code == 1 then
							showMessage( ret_panel, data.memo )								
							onChallenge( btn, x, y )
						else
							showMessage( ret_panel, data.memo )
						end			
					end
					doCommand( 'props', 'uses', { props = 50, quantity = 1 }, onPropsUses )
				end
				createAlert( ret_panel, otherMsgConfig.enter_game_alert, okHandler )
			else
				onChallenge( btn, x, y )
			end
		end
		if searching == false then
			searching = true
			if NEW_GAME_HALL.myDrunk and NEW_GAME_HALL.myCoin then
				searching = false
				if NEW_GAME_HALL.myDrunk >= 3 then
					local function okHandler()
						setExsitAlert( false )
						local function onPropsUses( data )
							if data.code == 1 then
								showMessage( ret_panel, data.memo )								
								onChallenge( btn, x, y )
							else
								showMessage( ret_panel, data.memo )
							end			
						end
						doCommand( 'props', 'uses', { props = 50, quantity = 1 }, onPropsUses )
					end
					createAlert( ret_panel, otherMsgConfig.enter_game_alert, okHandler )
				else
					onChallenge( btn, x, y )
				end
			else
				doCommand( 'user', 'money', {}, onUserMoney )
			end
			
		else
			showMessage( otherMsgView:getInstance().page, '搜索中', {ms=2000} )
		end
	end
	
	
	
	local function showAlert( btn, x, y )
		local user_info = getUserData()
		if uid == user_info.id then
			showMessage( otherMsgView:getInstance().page, "请不要挑战自己!", {ms=3000} )
			return
		end
		
		--local function onGetOnline( data )			
			--playerInfoPage:unadvise( serv_type["on_getonline"] )
			--print("onGetOnline" .. data.data.online)
			--[[local isOnline = true
			if data.data.online == 1 then
				isOnline = true
			else
				isOnline = false
			end--]]
			
			local function onOk()		--单挑
				setExsitAlert( false )
				local function confirmHandler()
					setExsitAlert( false )
					
				end
				--回调用户信息
				local function initRewardBtn( data )
					if data.code and data.code < 0 then
						if data.memo then
							print( "data.memo:" .. data.memo )
						end
						return
					end
					if tonumber( data.data.day_gold ) <= 0 then
						otherMsgConfig.challenge_settting_alert.cancel_btn.text = "取消"
						local player_info = all_player_info			
						createChallengeSetttingAlert( ret_panel,otherMsgConfig.challenge_settting_alert,confirmHandler, nil, player_info[ "npc" ], 1 )
					elseif tonumber( data.data.gold ) < 400 then
						otherMsgConfig.challenge_settting_alert.cancel_btn.text = "领取救济 (" .. data.data.day_gold .. "/3)"
						local player_info = all_player_info	
						createChallengeSetttingAlert( ret_panel,otherMsgConfig.challenge_settting_alert,confirmHandler, nil, player_info[ "npc" ], 2 )
					else
						otherMsgConfig.challenge_settting_alert.cancel_btn.text = "取消"
						local player_info = all_player_info			
						createChallengeSetttingAlert( ret_panel,otherMsgConfig.challenge_settting_alert,confirmHandler, nil, player_info[ "npc" ], 1 )
					end
					
					
				end
				
				--请求用户资料
				doCommand( 'user', 'money', {}, initRewardBtn )
				
				
			end
			local function onSecondOk()		--去他的游戏桌
				setExsitAlert( false )
				challengeHandler( btn, x, y )
			end
			uid = OTHERMSGINFO.otherMsgInfo:getInstance().uid
			all_player_info = OTHERMSGINFO.otherMsgInfo:getInstance().all_player_info[ uid ]
			local player_info = all_player_info
			if not player_info then
				return
			end
			
			if player_info['roomstate'] then
				--createAlertWith3Button(ret_panel,otherMsgConfig.alert,onOk, nil, onSecondOk, isOnline, true )
				createAlertWith3Button(ret_panel,otherMsgConfig.alert,onOk, nil, onSecondOk, true, true )
			else
			--createAlertWith3Button(ret_panel,otherMsgConfig.alert,onOk, nil, onSecondOk, isOnline, false )
				createAlertWith3Button(ret_panel,otherMsgConfig.alert,onOk, nil, onSecondOk, true, false )
			end
		--end
		--[[local function delayOnline(data)
			f_delay_do(ret_panel,onGetOnline,data,1)
		end--]]
		--otherMsgView.page:advise(serv_type["on_getonline"],delayOnline )
		--获取在线状态
		--[[local function getOnline(uid)			
			local arg = "{\"uid\":"..uid.."}"	
			tcp_pop(msg_type["getonline"],arg)
		end--]]
				
		--getOnline( tonumber( uid ) )
		ret_panel._challenge_btn:setNormalStatus()
				
	end
	--兼容1.0版
	if isFileExists(getResPath()..'dis_toosbar_challenge_bt_bg.png') == true then
		
		ret_panel._challenge_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, "toosbar_challenge_bt_bg.png", "dis_toosbar_challenge_bt_bg.png") 
		ret_panel._challenge_btn:setAnchorPoint(0, 0)
		ret_panel._challenge_btn:setString("") --斗骰修改地方
		ret_panel._challenge_btn.onTouchEnd = showAlert
		table.insert(align_tbl, ret_panel._challenge_btn)
	end		

	local function onFocus(btn, x, y)
		if is_friend then
		doCommand( 'user', 'delfriend', { friend = uid }, on_del_friend )	
		else
			doCommand( 'user', 'addfriend', { friend = uid }, on_add_friend )			
		end
	    
		btn:setClickStatus()
	--发送三条消息给它- - 
	--1.邀请视频认证
	--2.邀请上传照片
	--3.邀请完善资料
			
	end

	local focus_pic_1 = "focus_pic.png"
	local focus_pic_2 = "focus_already_pic.png"
	ret_panel._focus_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, focus_pic_1, focus_pic_2) 
	ret_panel._focus_btn:setAnchorPoint(0, 0)
	ret_panel._focus_btn:setString("")  --关注修改的地方
	ret_panel._focus_btn.onTouchEnd = onFocus
	table.insert(align_tbl, ret_panel._focus_btn)

	local function onMore(btn, x, y)
		btn:setClickStatus()
	end
	ret_panel._more_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, "toosbar_more_bt_bg.png", "toosbar_more_bt_bg.png") 
	ret_panel._more_btn:setAnchorPoint(0, 0)
	ret_panel._more_btn:setString("") --更多修改的地方
	--ret_panel._more_btn.onTouchEnd = onFocus
	ret_panel._more_btn.onTouchEnd = onFocus
	ret_panel._more_btn:setVisible( false )
	--table.insert(align_tbl, ret_panel._more_btn)

	LIGHT_UI.alignXCenterForObjList(align_tbl, 60)

	return ret_panel
end

function onChallengeFail( dataType )
	PANEL_CONTAINER.timeAlertExist = false
	if dataType == 1 then
		showUpFloorMessage( playerInfoPage, '对方挑战金不足', {ms=3000} )
	end
	if dataType == 2 then
		showUpFloorMessage( playerInfoPage, '对方拒绝', {ms=3000} )
	end
	if dataType == 3 then
		showUpFloorMessage( playerInfoPage, '对方不在线', {ms=3000} )
	end
	if dataType == 4 then
		showUpFloorMessage( playerInfoPage, '挑战已过期', {ms=3000} )
	end
	if dataType == 5 then
		showUpFloorMessage( playerInfoPage, '挑战场已关闭', {ms=3000} )
	end
	if dataType == 6 then
		showUpFloorMessage( playerInfoPage, '对方游戏中', {ms=3000} )
	end
end

function otherMsgView:initData()
	OTHERMSGINFO.otherMsgInfo:getInstance()
end

function otherMsgView:initView( parentPage, x, y, uid, callBack )
	paramArr = { parentPage = parentPage, x = x, y = y, uid = uid }
	OTHERMSGINFO.otherMsgInfo:getInstance().uid = uid
	local ret_panel = createBasePanel( parentPage, x, y )
	ret_panel.otherDataPage = createNode( ret_panel, {x=0,y=0} )
	local function on_back()
		--[[local MEET_BYCHANCE = Import("lua/challenge.lua")	
		MEET_BYCHANCE.showChallengePagePanel(HTTP_CLIENT.getRootNode(), 0, 0, true, 2)	--]]
		self:closeOtherMsgPage()
		if callBack then
			callBack()
		end
	end
	ret_panel._onBack = on_back	
	--ret_panel._user_id = uid
	ret_panel.otherDataPage = createNode( ret_panel, {x=0,y=0} )
	createSprite( ret_panel, otherMsgConfig.tab_bar_bg )
	local function clickTabBar( index )
		if index == 0 then		--资料
			MYRESULTVIEW.myResultView:getInstance():closeMyResultPage()
			OTHERDATAVIEW.otherDataView:getInstance( ret_panel.otherDataPage, 0, 0, uid, callBack )
		end
		if index == 1 then		--战绩
			OTHERDATAVIEW.otherDataView:getInstance():closePage()
			MYRESULTVIEW.myResultView:getInstance( ret_panel.otherDataPage, 0, 0, uid )
		end
	end
	local COMPONENT_BASE = Import( "lua/component_base.lua" )	
	COMPONENT_BASE.clsSelectStateSlideTabBar:New(ret_panel, otherMsgConfig.tab_bar.x, otherMsgConfig.tab_bar.y, otherMsgConfig.tab_bar.groups, otherMsgConfig.tab_bar.width, otherMsgConfig.tab_bar.space, 4, 80, 1, 
clickTabBar )
	
	local o = OTHERDATAVIEW.otherDataView:getInstance( ret_panel.otherDataPage, 0, 0, uid, callBack )
			
	ret_panel._bottom_panel = createBottomPanel(ret_panel, 0, 0)
	
	self.page = ret_panel
end

function otherMsgView:initListener()
	OTHERMSGCTRL.otherMsgCtrl:getInstance()
end	

function otherMsgView:secondInit()
	local function on_get_user_info(data)
		local uid = data.id
		if self.page then
			self.page._head_text:setString( data.name )
		end
		OTHERMSGINFO.otherMsgInfo:getInstance().uid = uid
		OTHERMSGINFO.otherMsgInfo:getInstance().all_player_info[uid] = data
		configSimpleInfo(uid)
	end
	local o = OTHERMSGCTRL.otherMsgCtrl:getInstance()
	o:request_player_info( OTHERMSGINFO.otherMsgInfo:getInstance().uid, on_get_user_info )
end

function otherMsgView:closeOtherMsgPage()
	--
	local o = otherMsgView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)	
	
	obj.Super(otherMsgView).close(o)	
end

function otherMsgView:disposeListener()
	otherMsgView.page:unadvise( serv_type["on_getonline"] )
	OTHERMSGCTRL.otherMsgCtrl:getInstance():closeOtherMsgCtrl()	
end

function otherMsgView:disposeView()
	local o = otherMsgView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)
	OTHERDATAVIEW.otherDataView:getInstance(  o.page.otherDataPage, 0, 0, nil ):closePage()
	MYRESULTVIEW.myResultView:getInstance():closeMyResultPage()
	clear( {o.page} )
	o.page = nil
	o = nil
end

function otherMsgView:disposeData()
	OTHERMSGINFO.otherMsgInfo:getInstance():closeOtherMsgInfo()	
end