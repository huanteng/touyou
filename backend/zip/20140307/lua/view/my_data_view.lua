myDataView = {}

local obj = Import( "lua/util/cls_view.lua" )
myDataView = obj.clsView:Inherit()

local MYMSG_LAYOUT = Import("layout/my_msg.lua")
local myMsgConfig = MYMSG_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local MYDATAINFO = Import( "lua/model/my_data_info.lua" )
local MYDATACTRL = Import( "lua/ctrl/my_data_ctrl.lua" )
local MYMSGVIEW = Import( "lua/view/my_msg_view.lua" )
local USER_ALBUM_PAGE = Import("lua/user_album.lua")
local UPLOAD_PAGE = Import("lua/upload.lua")

local album_data = nil

function getAlbumData()
	return album_data
end

function myDataView:New( parentPage, x, y )
	obj.Super(myDataView).__init__( self, parentPage, x, y )
	return self
end	

function myDataView:on_get_small_album()
	local data = MYDATAINFO.myDataInfo:getInstance().data
	local myMsgViewObj = MYMSGVIEW.myMsgView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)	
	local myDataViewObj = myDataView:getInstance( myMsgViewObj.myMsgPage.myDataPage )
	
	if data.code < 0 then
		showMessage( myDataViewObj.myDataPage, data.data )
		return
	end
	
	album_data = data.data.data
	
	local idx = 0
	for _, album_info in ipairs(album_data) do	
		if idx >= 2 then
			break
		end
		myMsgConfig.add_photo_pic.res = album_info["url"]
		
		myDataViewObj.myDataPage.photo = createSprite( myDataViewObj.myDataPage, myMsgConfig.add_photo_pic )
		local locX, locY = myDataViewObj.myDataPage.photo:getPosition()
		myDataViewObj.myDataPage.photo:setPosition( locX + ( ( _ - 1 ) * myMsgConfig.add_photo_pic.xspace ), locY )
		idx = idx + 1
	end	
	myDataViewObj.myDataPage.photoReponseRect:setContentSize( ( idx - 1 ) * ( myMsgConfig.add_photo_pic.xspace - 65 ) + idx * 65, 65 )
	
	myMsgConfig.add_photo_pic.res = "add_photo_bg.png"
	myDataViewObj.myDataPage.photo = createSprite( myDataViewObj.myDataPage, myMsgConfig.add_photo_pic )
	local locX, locY = myDataViewObj.myDataPage.photo:getPosition()
	myDataViewObj.myDataPage.photo:setPosition( locX + ( idx * myMsgConfig.add_photo_pic.xspace ), locY )
	myDataViewObj.myDataPage.photoReponseRect2:setPosition( locX + ( idx * myMsgConfig.add_photo_pic.xspace ), locY )

end

function myDataView:initData()
	MYDATAINFO.myDataInfo:getInstance()
end

function myDataView:initView( parentPage )
	local clipPanel = LIGHT_UI.clsClipLayer:New( parentPage, 0, 85 )
	clipPanel:set_msg_rect(0, 0, display.sx, display.sy-195 )
	local ret_panel =  createNode( clipPanel, {x=0,y=-85} )	
	local t = {}
	t.ax = 0
	t.ay = 0
	t.x = 0
	t.y = -120
	t.sx = display.sx
	t.sy = 810
	t.res = "white_dot.png"
	ret_panel.moveBg = createButton( ret_panel, t )
	myMsgConfig.no_pic_no_real.res = "white_dot.png"
	ret_panel.upload_pic_button = createButton( ret_panel, myMsgConfig.no_pic_no_real )	
	ret_panel.no_pic_no_real = createSprite( ret_panel, myMsgConfig.no_pic_no_real )		
	createSprite( ret_panel, myMsgConfig.simple_instruction )
	createSprite( ret_panel, myMsgConfig.frame )
	createLabel( ret_panel, myMsgConfig.sex_text )
	ret_panel.sex_pic = createSprite( ret_panel, myMsgConfig.sex_pic )
	ret_panel.age_text = createLabel( ret_panel, myMsgConfig.age_text )	
	ret_panel.address_text = createLabel( ret_panel, myMsgConfig.address_text )	
		local vip_logo = string.format( 'vip%i.png', "1" )
		ret_panel.vip_logo_pic = createSprite(ret_panel,myMsgConfig.video_vip_pos )--VIP标志调整				
		--ret_panel.vip_logo_pic = LIGHT_UI.clsSprite:New(ret_panel, myMsgConfig.video_vip_pos["x"], myMsgConfig.video_vip_pos["y"], vip_logo)--VIP标志调整				
		ret_panel.vip_logo_pic:setVisible( false )		
	createSprite( ret_panel, myMsgConfig.beautiful_value_pic )	
	ret_panel.beautiful_value_text = createLabel( ret_panel, myMsgConfig.beautiful_value_text )
	
	local lastY = 0	
	local distance = nil
	local isMove = false
	local function beganHandler( t, x, y )	
		lastY = y			
	end
	local function moveHandler( t, x, y )	
		--[[if not lastY then
			print( tostring( lastY ) )
			return
		end--]]
		if not distance then
			distance = y
		end	
		local locX, locY = ret_panel:getPosition()		
		if locY + y - lastY < 120 and locY + y - lastY > -90 then
			ret_panel:setPosition( locX, locY + y - lastY )	
		end	
		lastY = y
	end
	--[[local function stopHandler( t, x, y )
		if isMove == false then
			showMessage( ret_panel, "你点到我了", {ms=3000} )
		end
	end--]]
	local function buttonStopHandler( t, x, y )
		if not distance then
			distance = y
		end
		if math.abs( y - distance ) > 40 then
			isMove = true
			distance = nil
		end
		if isMove == false then
			myDataView.closeMyDataPage()
			local VIDEO_AUTH_PAGE = Import("lua/video_auth.lua")
			VIDEO_AUTH_PAGE.showVideoAuthPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
			add_back( 5, "lua/view/my_msg_view.lua", nil, 16, nil, "myMsgView", "lua/view/my_data_view.lua", "myDataView", "myMsgPage", "myDataPage" )
		end
		isMove = false	
		lastY = y
	end
	
	local function on_pic_click(msg_obj, x, y)
		--closeUserInfoPagePanel()	
		if not distance then
			distance = y
		end
		if math.abs( y - distance ) > 40 then
			isMove = true
			distance = nil
		end			
		if isMove == false then	
			myDataView.closeMyDataPage()		
			USER_ALBUM_PAGE.showAlbumPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
			add_back( 5, "lua/view/my_msg_view.lua", nil, 16, nil, "myMsgView", "lua/view/my_data_view.lua", "myDataView", "myMsgPage", "myDataPage" )
		end
		isMove = false
		lastY = y
		
	end

	local function on_no_pic_click(msg_obj, x, y)
		--closeUserInfoPagePanel()	
		if not distance then
			distance = y
		end
		if math.abs( y - distance ) > 40 then
			isMove = true
			distance = nil
		end	
		if isMove == false then	
			myDataView.closeMyDataPage()
			UPLOAD_PAGE.showUploadPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
			add_back( 5, "lua/view/my_msg_view.lua", nil, 16, nil, "myMsgView", "lua/view/my_data_view.lua", "myDataView", "myMsgPage", "myDataPage" )
		end
		isMove = false
		lastY = y	
	end
	
	local function onMyDetail(t, x, y)	
		if not distance then
			distance = y
		end
		if math.abs( y - distance ) > 40 then
			isMove = true
			distance = nil
		end
		if isMove == false then	
			myDataView.closeMyDataPage()
			local o = Import( "lua/view/my_detail_view.lua" )
			local obj = o.myDetailView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, nil, GLOBAL.uid )
			obj:secondInit()
		end
		isMove = false
		lastY = y			
	end
	
	local function onMyWord(t, x, y)	
		if not distance then
			distance = y
		end
		if math.abs( y - distance ) > 40 then
			isMove = true
			distance = nil
		end
		if isMove == false then	
			myDataView.closeMyDataPage()
			local o = Import( "lua/view/my_word_view.lua" )
			local obj = o.myWordView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0 )
			obj:secondInit( GLOBAL.uid )
			add_back( 5, "lua/view/my_msg_view.lua", nil, 16, nil, "myMsgView", "lua/view/my_data_view.lua", "myDataView", "myMsgPage", "myDataPage" )
		end
		isMove = false
		lastY = y	
	end	
	
	local function onMyDynamic(t, x, y)	
		if not distance then
			distance = y
		end
		if math.abs( y - distance ) > 40 then
			isMove = true
			distance = nil
		end
		if isMove == false then
			myDataView.closeMyDataPage()
			local o = Import( "lua/view/my_dynamic_view.lua" )
			local obj = o.myDynamicView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0 )
			obj:secondInit( GLOBAL.uid )
			add_back( 5, "lua/view/my_msg_view.lua", nil, 16, nil, "myMsgView", "lua/view/my_data_view.lua", "myDataView", "myMsgPage", "myDataPage" )
		end
		isMove = false
		lastY = y	
	end	
	
	local function on_select_cb(data)
		showMessage( ret_panel, data.memo, { ms=3000 } )
	end
	--用户心情显示
	local function for_show_bug( data )
		local dataCount =string.len(data)
		if dataCount > 0 then
			if dataCount < 46 then
				local str = getCharString(data,46)
				ret_panel.sign_text:setString( str )
				doCommand( 'question', 'answer', { id = 10, value = str }, on_select_cb )
			else
				local str = getCharString(data,46) .. ".."
				ret_panel.sign_text:setString( str )
				doCommand( 'question', 'answer', { id = 10, value = str }, on_select_cb )
			end
		end	
	end

	local function after_submit_input( data )
		f_delay_do( ret_panel, for_show_bug, data, 1 )
	end		
	
	local function modifyMood( t, x, y )
		if not distance then
			distance = y
		end
		if math.abs( y - distance ) > 40 then
			isMove = true
			distance = nil
		end
		if isMove == false then
			xymodule.mulitiInput( "", after_submit_input )
		end
		isMove = false
		lastY = y
	end
	ret_panel.sign_button = createButton( ret_panel, myMsgConfig.sign_button )
	ret_panel.sign_button.onTouchMove = moveHandler
	ret_panel.sign_button.onTouchEnd = modifyMood
	--[[if not user_info["mood"] or #user_info["mood"] <= 0 then
		str = "这家伙真懒，啥东西也没留下！"
	else
		if #user_info["mood"] > 22 then
			str = getCharString( user_info["mood"], 22 )
			str = str .. "..."
		else
			str = user_info["mood"]
		end
	end
	
	myMsgConfig.sign_text.text = str--]]
	ret_panel.sign_text = createLabel( ret_panel, myMsgConfig.sign_text )
	
	createSprite( ret_panel, myMsgConfig.first_white_line )
	createSprite( ret_panel, myMsgConfig.first_arrow_pic )
	ret_panel.photoReponseRect = createButton( ret_panel, myMsgConfig.add_photo_pic2 )
	--ret_panel.photoReponseRect:set_msg_rect(0, 0, 200, 60 )
	ret_panel.photoReponseRect2 = createButton( ret_panel, myMsgConfig.add_photo_pic3 )		
	
	ret_panel.responce_rect = {}
	local i = 1
	for i = 1, 4 do	
		ret_panel.white_line = createSprite( ret_panel, myMsgConfig.white_line )
		local locX, locY = ret_panel.white_line:getPosition()
		ret_panel.white_line:setPosition( locX, locY + ( ( i - 1 ) * myMsgConfig.white_line.space ) )
		if i ~= 4 then
			--myMsgConfig.arrow_pic.y = myMsgConfig.arrow_pic.y - myMsgConfig.arrow_pic.space
			
			ret_panel.responce_rect[ i ] = createButton( ret_panel, myMsgConfig.responce_rect )
			locX, locY = ret_panel.responce_rect[ i ]:getPosition()
			ret_panel.responce_rect[ i ]:setPosition( locX, locY + ( ( i - 1 ) * myMsgConfig.responce_rect.yspace ) )
			ret_panel.responce_rect[ i ].onTouchMove = moveHandler
			
			if i == 1 then
				ret_panel.responce_rect[ i ].onTouchEnd = onMyDetail
			end
			if i == 2 then
				ret_panel.responce_rect[ i ].onTouchEnd = onMyWord
			end
			if i == 3 then
				ret_panel.responce_rect[ i ].onTouchEnd = onMyDynamic
			end
			
			ret_panel.arrow_pic = createSprite( ret_panel, myMsgConfig.arrow_pic )
			locX, locY = ret_panel.arrow_pic:getPosition()
			ret_panel.arrow_pic:setPosition( locX, locY + ( ( i - 1 ) * myMsgConfig.arrow_pic.space ) )
		end
	end	
	local function freshVideoBtn()
		if tonumber(user_info["is_real"]) == 1 then
			ret_panel.videoPic:setVisible( true )
			ret_panel.videoBtn:setVisible( false )
			ret_panel.reward_coin_text:setVisible( false )
		end
	end
	
	ret_panel.videoBtn = createButton( ret_panel, myMsgConfig.video_btn )
	ret_panel.videoPic = createSprite( ret_panel, myMsgConfig.video_pic )
	ret_panel.reward_coin_text = createLabel( ret_panel, myMsgConfig.reward_coin_text )
	ret_panel.videoPic:setVisible( false )
	ret_panel.videoBtn:setVisible( false )
	ret_panel.reward_coin_text:setVisible( false )
	--[[if tonumber(user_info["is_real"]) == 0 then
		ret_panel.videoPic:setVisible( false )
	else
		ret_panel.videoBtn:setVisible( false )
		ret_panel.reward_coin_text:setVisible( false )
	end--]]
	
	--createButton( ret_panel, myMsgConfig.detail_data_pic, onMyDetail )
	createSprite( ret_panel, myMsgConfig.detail_data_pic )
	--createSprite( ret_panel, myMsgConfig.my_word_pic )
	
	--createButton( ret_panel, myMsgConfig.my_word_pic, onMyWord )
	createSprite( ret_panel, myMsgConfig.my_word_pic )
	--createButton( ret_panel, myMsgConfig.my_word_pic )
	
	--createButton( ret_panel, myMsgConfig.dynamic_pic, onMyDynamic )
	createSprite( ret_panel, myMsgConfig.dynamic_pic )
	createLabel( ret_panel, myMsgConfig.present_text )
	createSprite( ret_panel, myMsgConfig.last_arrow_pic )
	
	--[[createLabel( ret_panel, myMsgConfig.tip_text1 )
	createLabel( ret_panel, myMsgConfig.tip_text2 )
	createLabel( ret_panel, myMsgConfig.tip_text3 )--]]
	ret_panel.tip2 = createLabel( ret_panel, myMsgConfig.tip_text2 )
	ret_panel.tip3 = createLabel( ret_panel, myMsgConfig.tip_text3 )
	createLabel( ret_panel, myMsgConfig.tip_text4 )
	--createSprite( ret_panel, myMsgConfig.no_present_pic )
	
	--[[local t = { "my_msg/a.png", "my_msg/b.png", "my_msg/c.png", "my_msg/d.png", }
	for k, v in ipairs( t ) do
		myMsgConfig.present_pic.res = v		
		ret_panel.present_pic = createSprite( ret_panel, myMsgConfig.present_pic )
		--myMsgConfig.present_pic.x = myMsgConfig.present_pic.x + myMsgConfig.present_pic.xspace
		local locX, locY = ret_panel.present_pic:getPosition()
		ret_panel.present_pic:setPosition( locX + ( ( k - 1 ) * myMsgConfig.present_pic.xspace ), locY )
	end--]]
	
	--createSprite( ret_panel, myMsgConfig.add_photo_pic )
	
	--parentPage.mask = LIGHT_UI.clsClipLayer:New( parentPage, 0, 85 )
	--parentPage.mask = LIGHT_UI.clsClipLayer:New( ret_panel, 0, 85 )
	--parentPage.mask:set_msg_rect(0, 0, 480, 600 )
	
	
	clipPanel.onTouchBegan = beganHandler
	clipPanel.onTouchMove = moveHandler
	ret_panel.moveBg.onTouchMove = moveHandler
	--clipPanel.onTouchEnd = stopHandler	
	
	--ret_panel.upload_pic_button.onTouchBegan = beganHandler
	ret_panel.upload_pic_button.onTouchMove = moveHandler
	ret_panel.upload_pic_button.onTouchEnd = on_pic_click	
	
	ret_panel.photoReponseRect.onTouchMove = moveHandler
	ret_panel.photoReponseRect.onTouchEnd = on_pic_click
	
	ret_panel.photoReponseRect2.onTouchMove = moveHandler
	ret_panel.photoReponseRect2.onTouchEnd = on_no_pic_click
	
	ret_panel.videoBtn.onTouchMove = moveHandler
	ret_panel.videoBtn.onTouchEnd = buttonStopHandler
	
	self.clip = clipPanel
	self.myDataPage = ret_panel
	--ret_panel.closePanel = self.closeMyMsgPage	
end

function myDataView:initListener()
	MYDATACTRL.myDataCtrl:getInstance()	
end	

function myDataView:doCommentLists()
	local data = MYDATAINFO.myDataInfo:getInstance().myWord
	local myMsgViewObj = MYMSGVIEW.myMsgView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)	
	local o = myDataView:getInstance( myMsgViewObj.myMsgPage.myDataPage )
	if table.getn( data.data ) > 0 then
		local str = data.data[ 1 ].content
		if #str > 12 then
			str = getCharString( str, 12 )
			str = str .. "..."			
		end
		o.myDataPage.tip2:setString( str )
	else
		o.myDataPage.tip2:setString( "暂无发言" )
	end	
end

function myDataView:on_get_dynamic_info()
	dynamic_data = MYDATAINFO.myDataInfo:getInstance().dynamic.data
	if type( dynamic_data ) == "string" then
		print( dynamic_data )
		return
	end
	local myMsgViewObj = MYMSGVIEW.myMsgView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)
	local o = myDataView:getInstance( myMsgViewObj.myMsgPage.myDataPage )
	if table.getn( dynamic_data ) > 0 then
		
		local str
		local info = dynamic_data[ 1 ]
		if info.type == "7" then
			str = "给" .. info.data.name .. "打了" .. info.data.score .. "分"
		elseif info.type == "1" then
			str = "上传了新图片"
		elseif info.type == "9" then
			str = clearHTML( info.data )
		else
			str = info.data
		end
		if #str > 12 then
			str = getCharString( str, 12 )
			str = str .. "..."
		end
		o.myDataPage.tip3:setString( str )
	else
		o.myDataPage.tip3:setString( "暂无动态" )
	end
end	

function myDataView:onUserData()
	local data = MYDATAINFO.myDataInfo:getInstance().myUserData
	local user_info = data.data
	myDataView.myDataPage.no_pic_no_real:setImage( user_info.llogo )	
	local sexFile = ""
	if user_info.sex == "0" then
		sexFile = "man.png"
	else
		sexFile = "woman.png"
	end
	myDataView.myDataPage.sex_pic:setImage( sexFile )
	local s = ""
	local age = ""
	if user_info["birthday"] then
		local t = split( user_info["birthday"], "-" )
		age = tonumber( os.date("%Y") ) - tonumber( t[ 1 ] )
	end	
	s = "年龄：" .. age
	myDataView.myDataPage.age_text:setString( s )
	s = getCharString( user_info["city"], 3 )
	s = "城市：" .. s
	myDataView.myDataPage.address_text:setString( s )
	if user_info["vip_id"] ~= "0" then
		myDataView.myDataPage.vip_logo_pic:setVisible( true )
		s = string.format( 'vip%i.png', user_info["vip_id"] or "1" )
		myDataView.myDataPage.vip_logo_pic:setImage( s )
	end
	myDataView.myDataPage.beautiful_value_text:setString( tostring( user_info["charm"] ) )
	if not user_info["mood"] or #user_info["mood"] <= 0 then
		s = "这家伙真懒，啥东西也没留下！"
	else
		if #user_info["mood"] > 22 then
			s = getCharString( user_info["mood"], 22 )
			s = s .. "..."
		else
			s = user_info["mood"]
		end
	end	
	myDataView.myDataPage.sign_text:setString( s )
	if tonumber(user_info["is_real"]) == 0 then
		myDataView.myDataPage.videoBtn:setVisible( true )		
		myDataView.myDataPage.reward_coin_text:setVisible( true )
	else
		myDataView.myDataPage.videoPic:setVisible( true )
	end
end

function myDataView:secondInit()
	local ctrl = MYDATACTRL.myDataCtrl:getInstance()
	ctrl:requestPhotoInfo( self.on_get_small_album )
	ctrl:requestMyWordInfo( 3, 1, self.doCommentLists )	
	ctrl:requestDynamicInfo( GLOBAL.uid, nil, self.on_get_dynamic_info )
	ctrl:requestUserDetail( self.onUserData )
end

function myDataView:getMyDataPage()
	return self.myDataPage
end

function myDataView:closeMyDataPage()
	local myMsgViewObj = MYMSGVIEW.myMsgView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)	
	local myDataViewObj = myDataView:getInstance( myMsgViewObj.myMsgPage.myDataPage )
	obj.Super(myDataView).close(myDataViewObj)		
end

function myDataView:disposeListener()
	MYDATACTRL.myDataCtrl:getInstance():closeMyDataCtrl()
end

function myDataView:disposeView()
	--[[local MYMSGVIEW = Import( "view/my_msg_view.lua" )
	local myMsgViewObj = MYMSGVIEW.myMsgView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)	
	local myDataViewObj = myDataView:getInstance( myMsgViewObj.myMsgPage.myDataPage )--]]
	
	--[[if myDataViewObj.myDataPage then
		clear( {myDataViewObj.myDataPage} )
		myDataViewObj.myDataPage = nil
	end
	myDataViewObj = nil--]]
		
	local myMsgViewObj = MYMSGVIEW.myMsgView:getInstance(PANEL_CONTAINER.ret_panel.bg, 0, 0)	
	local myDataViewObj = myDataView:getInstance( myMsgViewObj.myMsgPage.myDataPage )
	
	--obj.Super(myDataView).close(myDataViewObj)	
	if myDataViewObj.myDataPage then
		clear( {myDataViewObj.myDataPage} )
		myDataViewObj.myDataPage = nil
		myDataViewObj.clip:setTouchEnabled( false )
		myDataViewObj.clip = nil
	end
	myDataViewObj = nil
end

function myDataView:disposeData()
	MYDATAINFO.myDataInfo:getInstance():closeMyDataInfo()
	--album_data = nil
end