otherDataView = {}

local obj = Import( "lua/util/cls_view.lua" )
otherDataView = obj.clsView:Inherit()

local l= Import("layout/other_msg.lua")
local c = l.getConfigData()[ LIGHT_UI.SCREEN_480_800 ]

--local info = Import( "lua/model/my_result_info.lua" )
--local infoObj = nil
local ctrl = Import( "lua/ctrl/other_data_ctrl.lua" )
local ctrlObj = nil

local id = nil

local USER_ALBUM_PAGE = Import("lua/user_album.lua")
local album_data = nil

function getAlbumData()
	return album_data
end

function otherDataView:New( ... )
	obj.Super(otherDataView).__init__( self, ... )
	return self
end	

function otherDataView:initData()
	--infoObj = info.myResultInfo:getInstance()
end

function otherDataView:initView( parentPage, x, y, uid, callBack )
	
	local function createMySelf()
		local o = Import( "lua/view/other_msg_view.lua" )
		local f = o.otherMsgView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, uid, callBack )
		f:secondInit()
	end
	id = uid
	local clipPanel = LIGHT_UI.clsClipLayer:New( parentPage, 0, 85 )
	clipPanel:set_msg_rect(0, 0, display.sx, display.sy-195 )
	local ret_panel =  createNode( clipPanel, {x=0,y=-85} )
	local t = {}
	t.ax = 0
	t.ay = 0
	t.x = 0
	t.y = -120
	t.sx = display.sx
	t.sy = 810
	t.res = "white_dot.png"
	ret_panel.moveBg = createButton( ret_panel, t )
	
	c.no_pic_no_real.res = "white_dot.png"
	ret_panel.upload_pic_button = createButton( ret_panel, c.no_pic_no_real )	
	ret_panel.no_pic_no_real = createSprite( ret_panel, c.no_pic_no_real )
	createSprite( ret_panel, c.frame, nil, 5 )
	createSprite( ret_panel, c.simple_instruction )	
	createLabel( ret_panel, c.sex_text )
	ret_panel.sex_pic = createSprite( ret_panel, c.sex_pic )
	ret_panel.age_text = createLabel( ret_panel, c.age_text )	
	ret_panel.address_text = createLabel( ret_panel, c.address_text )
	local vip_logo = string.format( 'vip%i.png', "1" )
	ret_panel.vip_logo_pic = createSprite(ret_panel,c.video_vip_pos )--VIP标志调整
	createSprite( ret_panel, c.beautiful_value_pic )	
	ret_panel.beautiful_value_text = createLabel( ret_panel, c.beautiful_value_text )
	
	local lastY = 0	
	local distance = nil
	local isMove = false
	local function beganHandler( t, x, y )	
		lastY = y			
	end
	local function moveHandler( t, x, y )	
		--[[if not lastY then
			print( tostring( lastY ) )
			return
		end--]]
		if not distance then
			distance = y
		end	
		local locX, locY = ret_panel:getPosition()		
		if locY + y - lastY < 120 and locY + y - lastY > -90 then
			ret_panel:setPosition( locX, locY + y - lastY )	
		end	
		lastY = y
	end
	
	local function on_pic_click(msg_obj, x, y)
		--closeUserInfoPagePanel()	
		if not distance then
			distance = y
		end
		if math.abs( y - distance ) > 40 then
			isMove = true
			distance = nil
		end			
		if isMove == false then	
			local p = Import( "lua/view/other_msg_view.lua" )
			p.otherMsgView:getInstance():closeOtherMsgPage()	
			dailyCount('PlayerInfoPhoto')--玩家相册
			local PLAYER_ALBUM_PAGE = Import("lua/player_album.lua")
			PLAYER_ALBUM_PAGE.showAlbumPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, id, createMySelf )
			--add_back( 5, "lua/view/my_msg_view.lua", nil, 16, nil, "myMsgView", "lua/view/my_data_view.lua", "myDataView", "myMsgPage", "myDataPage" )
		end
		isMove = false
		lastY = y
		
	end
	
	local function onMyDetail(t, x, y)
		if not distance then
			distance = y
		end
		if math.abs( y - distance ) > 40 then
			isMove = true
			distance = nil
		end
		if isMove == false then
			local p = Import( "lua/view/other_msg_view.lua" )
			p.otherMsgView:getInstance():closeOtherMsgPage()
			local o = Import( "lua/view/my_detail_view.lua" )
			local obj = o.myDetailView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, callBack, id )
			obj:secondInit()
			--add_back( 5, "lua/view/my_msg_view.lua", nil, 16, nil, "myMsgView", "lua/view/my_data_view.lua", "myDataView", "myMsgPage", "myDataPage" )
		end
		isMove = false
		lastY = y			
	end
	
	local function onMyWord(t, x, y)	
		if not distance then
			distance = y
		end
		if math.abs( y - distance ) > 40 then
			isMove = true
			distance = nil
		end
		if isMove == false then
			if not self.name then
				return
			end
			local p = Import( "lua/view/other_msg_view.lua" )
			p.otherMsgView:getInstance():closeOtherMsgPage()
			local o = Import( "lua/view/my_word_view.lua" )
			local obj = o.myWordView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, callBack, self.name )
			obj:secondInit( id )
			--add_back( 5, "lua/view/my_msg_view.lua", nil, 16, nil, "myMsgView", "lua/view/my_data_view.lua", "myDataView", "myMsgPage", "myDataPage" )
		end
		isMove = false
		lastY = y	
	end	
	
	local function onMyDynamic(t, x, y)	
		if not distance then
			distance = y
		end
		if math.abs( y - distance ) > 40 then
			isMove = true
			distance = nil
		end
		if isMove == false then
			local p = Import( "lua/view/other_msg_view.lua" )
			p.otherMsgView:getInstance():closeOtherMsgPage()
			local o = Import( "lua/view/my_dynamic_view.lua" )
			local obj = o.myDynamicView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, callBack )
			obj:secondInit( id )
			--add_back( 5, "lua/view/my_msg_view.lua", nil, 16, nil, "myMsgView", "lua/view/my_data_view.lua", "myDataView", "myMsgPage", "myDataPage" )
		end
		isMove = false
		lastY = y	
	end	
	
	ret_panel.sign_button = createButton( ret_panel, c.sign_button )
	ret_panel.sign_text = createLabel( ret_panel, c.sign_text )
	createSprite( ret_panel, c.first_white_line )
	--createSprite( ret_panel, c.first_arrow_pic )
	ret_panel.photoReponseRect = createButton( ret_panel, c.add_photo_pic2 )
	--ret_panel.photoReponseRect:set_msg_rect(0, 0, 200, 60 )
	--ret_panel.photoReponseRect2 = createButton( ret_panel, c.add_photo_pic3 )		
	
	ret_panel.responce_rect = {}
	local i = 1
	for i = 1, 4 do	
		ret_panel.white_line = createSprite( ret_panel, c.white_line )
		local locX, locY = ret_panel.white_line:getPosition()
		ret_panel.white_line:setPosition( locX, locY + ( ( i - 1 ) * c.white_line.space ) )
		if i ~= 4 then
			--myMsgConfig.arrow_pic.y = myMsgConfig.arrow_pic.y - myMsgConfig.arrow_pic.space
			
			ret_panel.responce_rect[ i ] = createButton( ret_panel, c.responce_rect )
			locX, locY = ret_panel.responce_rect[ i ]:getPosition()
			ret_panel.responce_rect[ i ]:setPosition( locX, locY + ( ( i - 1 ) * c.responce_rect.yspace ) )
			ret_panel.responce_rect[ i ].onTouchMove = moveHandler
			
			if i == 1 then
				ret_panel.responce_rect[ i ].onTouchEnd = onMyDetail
			end
			if i == 2 then
				ret_panel.responce_rect[ i ].onTouchEnd = onMyWord
			end
			if i == 3 then
				ret_panel.responce_rect[ i ].onTouchEnd = onMyDynamic
			end
			
			ret_panel.arrow_pic = createSprite( ret_panel, c.arrow_pic )
			locX, locY = ret_panel.arrow_pic:getPosition()
			ret_panel.arrow_pic:setPosition( locX, locY + ( ( i - 1 ) * c.arrow_pic.space ) )
		end
	end	
	
	ret_panel.videoBtn = createButton( ret_panel, c.video_btn )
	ret_panel.videoPic = createSprite( ret_panel, c.video_pic )
	ret_panel.reward_coin_text = createLabel( ret_panel, c.reward_coin_text )
	
	createSprite( ret_panel, c.detail_data_pic )
	createSprite( ret_panel, c.my_word_pic )
	createSprite( ret_panel, c.dynamic_pic )
	createLabel( ret_panel, c.present_text )
	createSprite( ret_panel, c.last_arrow_pic )
	ret_panel.tip2 = createLabel( ret_panel, c.tip_text2 )
	ret_panel.tip3 = createLabel( ret_panel, c.tip_text3 )
	createLabel( ret_panel, c.tip_text4 )
	
	--clipPanel.onTouchBegan = beganHandler
	clipPanel.onTouchMove = moveHandler	
	ret_panel.moveBg.onTouchMove = moveHandler
	
	ret_panel.photoReponseRect.onTouchMove = moveHandler
	ret_panel.photoReponseRect.onTouchEnd = on_pic_click
	
	ret_panel.upload_pic_button.onTouchMove = moveHandler
	ret_panel.upload_pic_button.onTouchEnd = on_pic_click	
	
	self.page = ret_panel
end

function otherDataView:on_get_small_album( data )
	
	if data.code < 0 then
		showMessage( self.page, data.data )
		return
	end
	
	album_data = data.data.data
	
	local startX = 0
	local startY = 0
	local idx = 0
	for _, album_info in ipairs(album_data) do	
		if idx >= 3 then
			break
		end
		c.add_photo_pic.res = album_info["url"]
		
		self.page.photo = createSprite( self.page, c.add_photo_pic )
		local locX, locY = self.page.photo:getPosition()
		self.page.photo:setPosition( locX + ( ( _ - 1 ) * c.add_photo_pic.xspace ), locY )
		if idx == 0 then
			startX = locX
			startY = locY
		end
		idx = idx + 1
	end	
	if idx ~= 0 then
		self.page.photoReponseRect:setContentSize( ( idx - 1 ) * ( c.add_photo_pic.xspace - 75 ) + idx * 75, 75 )
		self.page.photoReponseRect:setPosition( startX, startY )
	end	
	
	--[[c.add_photo_pic.res = "add_photo_bg.png"
	self.page.photo = createSprite( self.page, c.add_photo_pic )
	local locX, locY = self.page.photo:getPosition()
	self.page.photo:setPosition( locX + ( idx * c.add_photo_pic.xspace ), locY )
	self.page.photoReponseRect2:setPosition( locX + ( idx * c.add_photo_pic.xspace ), locY )--]]

end

function otherDataView:on_get_dynamic_info( dynamic_data )
	if type( dynamic_data ) == "string" then
		print( dynamic_data )
		return
	end

	if table.getn( dynamic_data ) > 0 then
		
		local str
		local info = dynamic_data[ 1 ]
		if info.type == "7" then
			str = "给" .. info.data.name .. "打了" .. info.data.score .. "分"
		elseif info.type == "1" then
			str = "上传了新图片"
		elseif info.type == "9" then
			str = clearHTML( info.data )
		else
			str = info.data
		end
		if #str > 12 then
			str = getCharString( str, 12 )
			str = str .. "..."
		end
		self.page.tip3:setString( str )
	else
		self.page.tip3:setString( "暂无动态" )
	end
end	

function otherDataView:onUserData( data )
	local user_info = data.data
	self.name = user_info.name
	self.page.no_pic_no_real:setImage( user_info.llogo )	
	local sexFile = ""
	if user_info.sex == "0" then
		sexFile = "man.png"
	else
		sexFile = "woman.png"
	end
	self.page.sex_pic:setImage( sexFile )
	local s = ""
	local age = ""
	if user_info["birthday"] then
		local t = split( user_info["birthday"], "-" )
		if t[ 2 ] then
			age = tonumber( os.date("%Y") ) - tonumber( t[ 1 ] )
		else
			age = t[ 1 ]
		end
	end	
	s = "年龄：" .. age
	self.page.age_text:setString( s )
	s = getCharString( user_info["city"], 3 )
	s = "城市：" .. s
	self.page.address_text:setString( s )
	if user_info["vip_id"] ~= "0" then
		self.page.vip_logo_pic:setVisible( true )
		s = string.format( 'vip%i.png', user_info["vip_id"] or "1" )
		self.page.vip_logo_pic:setImage( s )
	end
	self.page.beautiful_value_text:setString( tostring( user_info["charm"] ) )
	if not user_info["mood"] or #user_info["mood"] <= 0 then
		s = "这家伙真懒，啥东西也没留下！"
	else
		if #user_info["mood"] > 22 then
			s = getCharString( user_info["mood"], 22 )
			s = s .. "..."
		else
			s = user_info["mood"]
		end
	end	
	self.page.sign_text:setString( s )
	if tonumber(user_info["is_real"]) == 0 then
		self.page.videoBtn:setVisible( true )		
		self.page.reward_coin_text:setVisible( true )
	else
		self.page.videoPic:setVisible( true )
	end
end

function otherDataView:on_get_other_word_info( data )
	if table.getn( data.data ) > 0 then
		local str = data.data[ 1 ].content
		if #str > 12 then
			str = getCharString( str, 12 )
			str = str .. "..."			
		end
		self.page.tip2:setString( str )
	else
		self.page.tip2:setString( "暂无发言" )
	end	
end

function otherDataView:initListener()
	ctrlObj = ctrl.otherDataCtrl:getInstance()
	if id then
		ctrlObj:requestUserDetail( self, self.onUserData, id )
		ctrlObj:requestPhotoInfo( self, self.on_get_small_album, id )
		ctrlObj:requestDynamicInfo( self, self.on_get_dynamic_info, id, nil )
		ctrlObj:requestOtherWordInfo( self, self.on_get_other_word_info, 3, 1, id )
	end
end	

function otherDataView:closePage()
	local o = otherDataView:getInstance()	
	
	obj.Super(otherDataView).close(o)	
end

function otherDataView:disposeListener()
	ctrlObj:closeCtrl()	
end

function otherDataView:disposeView()
	local o = otherDataView:getInstance()
	clear( {o.page} )
	o.page = nil
	o = nil
end

function otherDataView:disposeData()
	--infoObj:closeInfo()	
end