local DetailConfig = Import("layout/chat_detail.lua").getData()
local PLAYER_INFO_PANEL = Import("lua/player_info.lua")

local main_say_panel = nil
local self = nil
--local detail_data = {}
local user_info = nil

local total_width = 480
local main_height_1 = 0
local main_height_2 = 0
local divider_pic2 = nil

local reply_user_id = nil

local user_chat_id = nil		--用于临时保存聊天记录id

local page = 1

local freshing = nil
local doListData = nil

local function setAllCommentExpires()
	local i = 1
	local name = string.format( "%s_comment_view_%s", user_chat_id, i )
	if getCache( name ) then
		setCacheExpires( name )
		i = i + 1
	end
end

local function on_reply_cb(data)
	setAllCommentExpires()
	onDetailTouchEnd()
	setCacheExpires( 'comment.lists.2.1' )
	setCacheExpires( 'comment.lists.3.1' )
	
end

local function onReply(content)
	local function filterChar( msg )
		local i = 1
		local str = " "
		while(i <= string.len(msg)) do
			local b = string.sub(msg,1,i)		
			if b ~= str then
				return true
			end
			str = str .. " "			
			i = i + 1
		end
		return false
	end	
	local sayContent = filterChar( content )
	if sayContent == true then		
		if string.len( content ) > 0 then
			local url = GLOBAL.interface .. "comment.php?sid=" .. GLOBAL.sid

			if tonumber( self._chat_id ) > 0 then
				--local post_data = { id = self._chat_id, content = content, kind = 3 }
				local post_data = { id = reply_user_id, content = content, kind = 3 }			
				if pic_path then post_data.file = pic_path end
				if post_data.file then
					post_data.type = 'answer'
					post_data.sid = GLOBAL.sid
					HTTP_REQUEST.http_upload( url, post_data, on_reply_cb )
				else
					doCommand( 'comment', 'answer', post_data, on_reply_cb )
				end
			else
				local post_data = { content = content }
				if pic_path then post_data.file = pic_path end

				if post_data.file then
					post_data.type = 'say'
					post_data.sid = GLOBAL.sid
					HTTP_REQUEST.http_upload( url, post_data, on_reply_cb )
				else
					doCommand( 'comment', 'say', post_data, on_reply_cb )
				end
			end				
		end	
	else
		showMessage( self, "输入不能为空...", {ms = '3000'})
	end	
				
end

local function onDelayreply(data)
	f_delay_do( self, onReply, data,1 )
end
--local tryConfigPageByData
function onDetailTouchEnd()	
	if not self then
		return
	end
	
	freshing = true
	--tryConfigPageByData(self._chat_id)
	loadedPage = 1	
	doListData()		
end

function doListData()
	local cachename = string.format( "%s_comment_view_%s", user_chat_id, page )
	doCommand( 'comment', 'view', { id = user_chat_id, page = page, size = 10 }, onCommentView, 60, { cachename = cachename } )
end

local function onListTop()
	--doListData()
end

local function onListBottom()
	page = page + 1
	doListData()
end

local CHAT_PAGE = Import("lua/chat.lua")
local REPLY_PAGE = Import("lua/reply.lua")

local back_func = nil

function init( data )
	uid = GLOBAL.uid
	
	if data.back_func then
		back_func = data.back_func
	end
end	

local function okHandler( content )
	dailyCount('SayWordsReply')--回复说两句
	local function filterChar( content )
		local i = 1
		local str = " "
		while(i <= string.len(content)) do
			local b = string.sub(content,1,i)		
			if b ~= str then
				return true
			end
			str = str .. " "
			i = i + 1
		end
		return false
	end		
	local sayContent = filterChar( content )	
	if sayContent == true then
		if string.len( content ) > 0 then
			doCommand( 'comment', 'answer', { id = self._chat_id, content = content, kind = 1 }, on_reply_cb )
		end			
	else
		showMessage( self, "输入不能为空...", {ms = '3000'})
	end		
end

local function createDetailPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )
	
	self = ret_panel

	local c, c2 = DetailConfig, nil
	local o = nil

	local function on_back(btn)
		closeDetailPagePanel()

		if back_func then
			back_func()
		end
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '详情' )
	
	c2 = c["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	local o = createListView( ret_panel, 0, 0, winSize.width, c2["view_height"] )
	o.OnTopRefresh = onListTop
	o.OnBottomRefresh = onListBottom
	ret_panel.list = o
	
	local function on_ok(content)
		f_delay_do( ret_panel, okHandler, content, 0.1 )		
	end

	local function on_cancel()
		showDetailPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, self._chat_id)
	end

	local function on_reply_click(obj, x, y)
		local page_info = {
			title = "回复",
			desc = "",
			id = self._chat_id,
			nophoto = 1,
		}
		--REPLY_PAGE.showReplyPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, on_cancel, page_info, nil)
		--closeDetailPagePanel()
		xymodule.mulitiInput( "", on_ok )	
		--on_ok( "大家好" )
		--doCommand( 'comment', 'answer', { id = self._chat_id, content = "hao", kind = 1 }, on_reply_cb )		
	end
	config = DetailConfig["replybtn_config"]
	ret_panel._reply_btn = LIGHT_UI.clsButton:New(ret_panel, config.x, config.y, config.normal_res, config.click_res)
	ret_panel._reply_btn.onTouchEnd = on_reply_click

	--add_back(ret_panel ,on_back)

	return ret_panel
end

local function after_get_http_pic( parent, obj )
	local size = obj:getContentSize()		
	local size_old = parent.getContentSize()
	local pos_old_x, pos_old_y = obj:getPosition()
	local pic_new_width = 440
	local pic_new_height = 0

	if size.width > 440 then
		pic_new_width = 440
	else
		pic_new_width = size.width
	end

	pic_new_height = pic_new_width * size.height / size.width
	
	obj:setContentSize( pic_new_width, pic_new_height )
	obj:setPosition( ( total_width - pic_new_width ) / 2, pos_old_y )
	
	local height = pic_new_height + 10
	
	local list = { '_reply_num_pic_obj', '_reply_num_obj', '_bottom_split_line', 'bg' }
	
	for _,v in pairs( list ) do
		local x, y = parent[v]:getPosition()
		parent[v]:setPosition( x, y - height )
	end
	local bx, by = parent[ 'bg' ]:getPosition()
	parent[ 'bg' ]:setScaleY( -by + 90 )
	
	self._move_detail_page._detail_page:refresh_view()
end

local function onClickLogo( uid )
	user_chat_id = chat_id
	closeDetailPagePanel()

	local function back_func()
		showDetailPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, user_chat_id )
	end

	--[[PLAYER_INFO_PANEL.showByUserId( uid )
	PLAYER_INFO_PANEL.init( { back_func = back_func })--]]
	local o = Import( "lua/view/other_msg_view.lua" )
	local f = o.otherMsgView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, uid, back_func )
	f:secondInit()
end

local function createItemByFirst( idx, info, size )
	local function onClick(data)
		onClickLogo( info.user )
	end
	
	local c, c2 = DetailConfig, nil
	
	local tr = LIGHT_UI.clsTr:New( c )
	local ret_panel = tr._RootNode
	
	createButton( ret_panel, c.chat_pic_bg, onClick )
	
	setUsersData( info, 'user', {'is_real','vip_id','logo','sex','name'} )
	setUserLogo( ret_panel, c.logo, info.user, '' )
	
	if info.vip_id ~= '0' then
		local vip_logo = string.format( 'vip%i.png', info.vip_id )
		
		tr:addSprite( 'vip', vip_logo )
	end
	
	tr:addLabel( 'name', info.name )
	
	tr:addLabel( 'time', info.time )	
	
	local content = tr:addMultiLabel( 'content', info.content )
	
	o = tr:addSprite( 'split_line' )
	
	local function onLoad( obj )
		if not self then
			return
		end
		
		if size then
			return
		end
		
		local List = self.list
		List:updateCellAtIndexBySize(idx)
	end
	
	if string.len( info.large ) > 0 then
		o = tr:addSprite( 'say_pic_c', info.large, onLoad )
		
		-- 以下代码写法不一定合理。但当第二次时，因图片已下载，就合理了。
		local pos_old_x, pos_old_y = o:getPosition()
		local size = o:getContentSize()
		if size.width > 0 and size.height > 0 then
			local pic_new_width = 440
			local pic_new_height = 0

			if size.width > 440 then
				pic_new_width = 440
			else
				pic_new_width = size.width
			end

			pic_new_height = (pic_new_width * size.height) / size.width
			
			o:setContentSize( pic_new_width, pic_new_height )
			o:setPosition( ( total_width - pic_new_width ) / 2, pos_old_y )
			
			local y = c.reply_pic.y
			c.reply_pic.y = y + o:getY0()
			tr:addSprite( 'reply_pic' )
			c.reply_pic.y = y
		else
			o = tr:addSprite( 'say_pic_c', info.large .. '.s.jpg' )
			local y = c.reply_pic.y
			c.reply_pic.y = y + o:getY0()
			tr:addSprite( 'reply_pic' )
			c.reply_pic.y = y
			
		end
	else
		local y = c.reply_pic.y
		c.reply_pic.y = y + content:getY0()
		d(c.reply_pic.y)
		tr:addSprite( 'reply_pic' )
		c.reply_pic.y = y
	end		
	
	tr:addLabel( 'replys', info.replys )
	
	local line = tr:addSprite( 'bottom_line' )
	
	sx = 480
	sy = 10 - line:getY0()
	
	return ret_panel, sx, sy
end	

local function createItemByInfo( idx, info )
	local c, c2 = DetailConfig, nil
	local tr = LIGHT_UI.clsTr:New( c )
	local ret_panel = tr._RootNode

	local function on_ok( content )		
		doCommand( 'comment', 'answer', { id = reply_info["id"], content = content, kind = 1 }, on_reply_cb )
		showDetailPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, self._chat_id)
	end

	local function on_cancel()
		showDetailPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, self._chat_id )
	end

	local function click_reply( obj, x, y )
		local page_info = {
			title = "回复",
			desc = "",
			id = self._chat_id,
			nophoto = 1,
		}
		REPLY_PAGE.showReplyPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, on_cancel, page_info, reply_info )
		closeDetailPagePanel()
	end
	
	local function click_logo()
		user_chat_id = self._chat_id
		closeDetailPagePanel()

		local function back_func()
			showDetailPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, user_chat_id )
		end

		--[[PLAYER_INFO_PANEL.showByUserId( info["user"] )
		PLAYER_INFO_PANEL.init( { back_func = back_func })--]]
		local o = Import( "lua/view/other_msg_view.lua" )
		local f = o.otherMsgView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, info["user"], back_func )
		f:secondInit()
	end		
	
	setUsersData( info, 'user', {'vip_id','logo','sex','name'} )
	local logo = setUserLogo( ret_panel, c.logo_pic_c2, info.user, '' )
	
	tr:addLabel( 'username_c2', info["name"] )
	tr:addLabel( 'say_time_c2', info["time"] )
	local content = tr:addMultiLabel( 'say_content_c2', info["content"] )
		
	local line = nil
	c2 = deepcopy( c.line )
	local pre_content_height = 0
	if info["prev_content"] then
		reply_bg = create9Sprite( ret_panel, DetailConfig.replg_bg_c )

		local text = string.format( "%s说: %s", info["prev_name"], info["prev_content"] )
		local content2_obj = tr:addMultiLabel( 'say_content_c3', text )
		local content2_x, content2_y = content2_obj:getPosition()
		local content2_size = content2_obj:getContentSize()
		height = math.abs( content2_y ) + content2_size.height
		reply_bg:setSpriteContentSize( content2_size.width + 20, content2_size.height )
		
		pre_content_height = reply_bg:getY0()
	end
	c2.y = math.min( logo:getY0(), content:getY0(), pre_content_height ) - 8
	line = createSprite( ret_panel, c2 )
	
	sx = 480
	sy = 10 - line:getY0()
	
	return ret_panel, sx, sy
end

function onCommentView(data)
	--d(type(data))
	local function doNothing()
		d(a)
	end
	local function reply(idx, info)
		reply_user_id = info[ "id" ]
		xymodule.mulitiInput( "", onDelayreply )
		--onDelayreply( " " )	
		--onReply( "Hello" )	
	end
	
	if not self then
		return
	end
	
	if data.code == -1 then
		showMessage( self, "帖子不存在", {3000} )
		return
	elseif data.code < 0 then
		showMessage( self, data.memo or "出错", {3000} )
		return
	end
	
	local List = self.list
	
	data = data.data
	if page == 1 then				
		List:clear()
		
		List:addItem( data.first, createItemByFirst, doNothing)
	end
	
	data = data.lists.data
	for _, info in ipairs(data) do
		List:addItem( info, createItemByInfo,reply)
	end
	
	if page ~= 1 then
		List:refresh()
	else
		List:reload()
	end
	
	freshing = nil --刷新完毕
end

function init( data )
	detail_data = data.data
end

function showDetailPagePanel(parent, x, y, chat_id)
	page = 1
	self = createDetailPagePanel(parent, x, y)
	
	if detail_data then
		local data = { code = 1, data = { first = detail_data, lists = { data = {} } } }
		onCommentView( data )
	end
	
	user_chat_id = chat_id
	self._chat_id = chat_id
	doListData()
end

function closeDetailPagePanel()
	clear({self})
	self = nil
end