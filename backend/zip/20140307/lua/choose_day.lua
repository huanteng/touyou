DAY_LAYOUT = Import("layout/choose_day.lua")
local USER_PAGE = Import("lua/user_info.lua") 
local DayConfig = DAY_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
local dayPage = nil
local gchat_id = nil
local gday_id = nil
local year_panel = nil
local month_panel = nil
local day_panel = nil

local day_list = {
	[1] = "01",
	[2] = "02",
	[3] = "03",
	[4] = "04",
	[5] = "05",
	[6] = "06",
	[7] = "07",
	[8] = "08",
	[9] = "09",
	[10] = "10",
	[11] = "11",
	[12] = "12",
	[13] = "13",
	[14] = "14",
	[15] = "15",
	[16] = "16",
	[17] = "17",
	[18] = "18",
	[19] = "19",
	[20] = "20",
	[21] = "21",
	[22] = "22",
	[23] = "23",
	[24] = "24",
	[25] = "25",
	[26] = "26",
	[27] = "27",
	[28] = "28",
	[29] = "29",
	[30] = "30",
	[31] = "31",
}

local month_list = {
	[1] = "01",
	[2] = "02",
	[3] = "03",
	[4] = "04",
	[5] = "05",
	[6] = "06",
	[7] = "07",
	[8] = "08",
	[9] = "09",
	[10] = "10",
	[11] = "11",
	[12] = "12",
}

local year_list = {
	[1] = "1970",
	[2] = "1971",
	[3] = "1972",
	[4] = "1973",
	[5] = "1974",
	[6] = "1975",
	[7] = "1976",
	[8] = "1977",
	[9] = "1978",
	[10] = "1979",
	[11] = "1980",
	[12] = "1981",
	[13] = "1982",
	[14] = "1983",
	[15] = "1984",
	[16] = "1985",
	[17] = "1986",
	[18] = "1987",
	[19] = "1988",
	[20] = "1989",
	[21] = "1990",
	[22] = "1991",
	[23] = "1992",
	[24] = "1993",
	[25] = "1994",
	[26] = "1995",
	[27] = "1996",
	[28] = "1997",
	[29] = "1998",
	[30] = "1999",
	[31] = "2000",
}

local row_desc_info = {
	item_width = 90,
	item_height = 40,
	column_cnt = 4,
	x_space = 20,
	y_space = 30,
}

local function click_day_btn( btn, x, y )
	for _, btn2 in pairs( dayPage._day_btns ) do
		btn2:setStatus( LIGHT_UI.NORMAL_STATUS )
	end
	btn:setStatus( LIGHT_UI.DOWN_STATUS )
	dayPage._day_btn:setString( btn.text )
end

local function click_month_btn( btn, x, y )
	for _, btn2 in pairs( dayPage._month_btns ) do
		btn2:setStatus( LIGHT_UI.NORMAL_STATUS )
	end
	btn:setStatus( LIGHT_UI.DOWN_STATUS )
	dayPage._day_btn:setString( day_list[1] )
	dayPage._month_btn:setString( btn.text )
end

local function click_year_btn( btn, x, y )
	for _, btn2 in pairs( dayPage._year_btns ) do
		btn2:setStatus( LIGHT_UI.NORMAL_STATUS )
	end
	btn:setStatus( LIGHT_UI.DOWN_STATUS )
	dayPage._month_btn:setString( month_list[1] )
	dayPage._year_btn:setString( btn.text )
end

local function create_item( text, cb, selected )
	
	local config = {
		sx = row_desc_info.item_width,
		sy = row_desc_info.item_height,
		css = 'checkbox',
		text = '',
		text_css = 'b2',
	}

	local btn = create9Button( nil, config, cb )
	btn:setString( text )
	btn.text = text
	if selected then btn:setStatus( LIGHT_UI.DOWN_STATUS ) end
	
	return btn
end

local function show_day_panel( parent )
	if month_panel then month_panel:setVisible( false ) end
	if year_panel then year_panel:setVisible( false ) end

	if day_panel == nil then
		day_panel = LIGHT_UI.clsNode:New( parent, 0, 0 )
		local row_page = LIGHT_UI.clsRowLayout:New( day_panel, 30, 620, row_desc_info )

		parent._day_btns = {}
		for i = 1, table.getn( day_list ) do
			local item = create_item( day_list[i], click_day_btn, i == 1 )
			row_page:append_item( item )
			table.insert( parent._day_btns, item )
		end

		row_page:refresh_view()
	end
	
	parent._bg2:setVisible( true )
	day_panel:setVisible( true )
end

local function show_month_panel( parent )
	if day_panel then day_panel:setVisible( false ) end
	if year_panel then year_panel:setVisible( false ) end

	if month_panel == nil then
		month_panel = LIGHT_UI.clsNode:New( parent, 0, 0 )
		local row_page = LIGHT_UI.clsRowLayout:New( month_panel, 30, 620, row_desc_info )

		parent._month_btns = {}
		for i = 1, table.getn( month_list ) do
			local item = create_item( month_list[i], click_month_btn, i == 1 )
			row_page:append_item( item )
			table.insert( parent._month_btns, item )
		end

		row_page:refresh_view()
	end
	
	parent._bg2:setVisible( true )
	month_panel:setVisible( true )
end

local function show_year_panel( parent )
	if day_panel then day_panel:setVisible( false ) end
	if month_panel then month_panel:setVisible( false ) end

	if year_panel == nil then
		year_panel = LIGHT_UI.clsNode:New( parent, 0, 0 )
		local row_page = LIGHT_UI.clsRowLayout:New( year_panel, 30, 620, row_desc_info )

		parent._year_btns = {}
		for i = 1, table.getn( year_list ) do
			local item = create_item( year_list[i], click_year_btn, i == 1 )
			row_page:append_item( item )
			table.insert( parent._year_btns, item )
		end

		row_page:refresh_view()
	end
	
	parent._bg2:setVisible( true )
	year_panel:setVisible( true )
end

local function hide_select_panel()
	if dayPage then
		dayPage._bg2:setVisible( false )
	end
	if year_panel then year_panel:setVisible( false ) end
	if month_panel then month_panel:setVisible( false ) end
	if day_panel then day_panel:setVisible( false ) end
end

local function createDayPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = DayConfig, nil
	local o = nil
	local USER_INFO_PANEL = Import("lua/user_info.lua")

	local function on_back(btn)
		hide_select_panel()
		closeDayPagePanel()
		local o = Import( "lua/view/my_detail_view.lua" )
		local obj = o.myDetailView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, nil, GLOBAL.uid )
		obj:secondInit()
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '设置生日' )
	
	local function onOk(obj, x, y)
		
		local tip
		local txt = string.format("%s-%s-%s", ret_panel._year_btn._str_obj:getString(), ret_panel._month_btn._str_obj:getString(), ret_panel._day_btn._str_obj:getString())		

		local function on_select_cb(data)
			clear{ tip }
			tip = nil
			--[[local page = USER_PAGE.get_page_obj()
			page._move_info_page._user_simple_info._birthday_value:setString( ret_panel._year_btn._str_obj:getString() .. '-' .. ret_panel._month_btn._str_obj:getString() .. '-' .. ret_panel._day_btn._str_obj:getString() )--]]
			if data.code > 0 then
				ok_func( txt )
				closeDayPagePanel()
				local o = Import( "lua/view/my_detail_view.lua" )
				local obj = o.myDetailView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, nil, GLOBAL.uid )
				obj:secondInit()
			else
				showMessage( dayPage, "操作失败，请联系客服!", { ms = 3000 } )
			end
		end

		doCommand( 'question', 'answer', { id = 8, value = txt }, on_select_cb )
		tip = showMessageX( dayPage, "信息已提交，请稍候" )
		
		hide_select_panel()
		
	end

	config = DayConfig["reply_btn_config"]
	ret_panel._ok_btn = LIGHT_UI.clsButton:New(ret_panel, config["x"], config["y"], config["normal_res"], config["click_res"])
	ret_panel._ok_btn.onTouchEnd = onOk

	config = DayConfig["select_config"]
	local startx = config.x
	local starty = config.y
	local x_step = 166
	local config = {
		x = startx,
		y = starty,
		sx = 120,
		sy = 50,
		css = 'checkbox',
	}
	local year_btn = create9Button( ret_panel, config, nil )

	local function on_year_click(btn, x, y)
		show_year_panel( ret_panel )
	end
	year_btn:setString("1990")
	year_btn:setTextColor(102, 102, 102)
	year_btn.onTouchEnd = on_year_click

	local size = year_btn:getContentSize()
	local year_txt = LIGHT_UI.clsLabel:New(ret_panel, startx + size.width - 30, starty, "年", GLOBAL_FONT, 22) 
	year_txt:setTextColor(102, 102, 102)

	startx = startx + x_step
	local config = {
		x = startx,
		y = starty,
		sx = 120,
		sy = 50,
		css = 'checkbox',
	}
	local month_btn = create9Button( ret_panel, config, nil )

	local function on_month_click(btn, x, y)
		show_month_panel( ret_panel )
	end
	month_btn:setString("01")
	month_btn:setTextColor(102, 102, 102)
	month_btn.onTouchEnd = on_month_click

	size = month_btn:getContentSize()
	local month_txt = LIGHT_UI.clsLabel:New(ret_panel, startx + size.width - 30, starty, "月", GLOBAL_FONT, 22) 
	month_txt:setTextColor(102, 102, 102)

	startx = startx + x_step
	local config = {
		x = startx,
		y = starty,
		sx = 120,
		sy = 50,
		css = 'checkbox',
	}
	local day_btn = create9Button( ret_panel, config, nil )

	local function on_day_click(btn, x, y)
		show_day_panel( ret_panel )
	end
	day_btn:setString("01")
	day_btn:setTextColor(102, 102, 102)
	day_btn.onTouchEnd = on_day_click

	ret_panel._year_btn = year_btn
	ret_panel._month_btn = month_btn 
	ret_panel._day_btn = day_btn 

	ret_panel._bg2 = LIGHT_UI.clsSprite:New( ret_panel, 0, 0, "yellow_bg_p.png" )
	ret_panel._bg2:setAnchorPoint( 0, 0 )
	ret_panel._bg2:setContentSize( 480, 630 )
	ret_panel._bg2:setVisible( false )

	--add_back(ret_panel ,on_back)
	
	return ret_panel
end

function showDayPagePanel(parent, x, y)
	year_panel = nil
	month_panel = nil
	day_panel = nil
	if not dayPage then
		dayPage = createDayPagePanel(parent, x, y)
	else
		dayPage:setVisible(true)
	end
end

function init( data )
	ok_func = data.ok_func
end

function closeDayPagePanel()
	--dayPage:setVisible(false)	
	if dayPage then
		--delete_back(dayPage)
		clear({dayPage})
		dayPage = nil
	end		
end

