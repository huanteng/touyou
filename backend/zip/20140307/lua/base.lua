--base.lua
PAGE = {}
local net_error = false
--GLOBAL_FONT = 'Verdana'
--GLOBAL_FONT = 'DroidSans'
--默认图片
--DEFALUT_PIC = "xiao.png"
local ON_BACK = {} --记录返回函数parent
local isExitGame = false --是否退出游戏

HTTPPicCache = {
	["add_photo_bg.png"] = "add_photo_bg.png",
}	

function tcp_connect(ip,port,name,pwd,version,cb)
	if not pwd then
		pwd = "******"
	end
	if tcp.connect then
		tcp.connect(ip,port,name,pwd,version,cb)
	end
end

function tcp_pop(ntype,content)
	if tcp.pop then
		tcp.pop(ntype,content)
	end
end
--(ip，端口，UID，比赛类型(多人为0,单人为1)，挑战金类型，挑战金数量)
function tcp_login(ip,port,name,matchtype,challengegoldtype,challengegold,cb)
	tcp.login(ip,port,name,matchtype,challengegoldtype,challengegold,cb)
end

function tcp_sitdown(tableid,chairid)
	tcp.sitdown(tableid,chairid)
end

function tcp_leave()
	tcp.leave()
end

function tcp_shout(count,number,fast)
	tcp.shout(count,number,fast)
end

function tcp_open()
	tcp.open()
end

function tcp_award()
	tcp.award()
end

function tcp_message(ntype,msg)
	tcp.message(ntype,msg)
end

function tcp_heartbeat()
	tcp.heartbeat()
end

function tcp_useprops(uid,propid,count)
	tcp.useprops(uid,propid,count)
end

function Log(data)
	local file = WRITE_PATH.."log.txt"
	local f = io.open(file,'a+')
	if f then
		local tab=os.date("*t",time);
		local str = tab.hour..":"..tab.min..":"..tab.sec.."   "..tostring(data)
		f:write(str)
		f:write("\r\n")
		f:close()
	end		
end

function proc_http_request()
	HTTP_REQUEST.proc_single_request()
end

function __G__TRACKBACK__(msg)
	--xymodule.print_android_str(msg)
	--xymodule.print_android_str(debug.traceback())
	print("----------------------------------------")
	print("LUA ERROR: " .. tostring(msg) .. "\n")
	print(debug.traceback())
	print("----------------------------------------")
end

--[[
返回位置描述，结构为{x,y,{ax,ay,},{sx,sy,}}
参数：
	x：x坐标
	y
	ax：AuchorPoint的x值，可选
	ay：AuchorPoint的y值，可选
	sx：x向拉伸值，可选
	sy：y向拉伸值，可选
]]
function point( x, y, ax, ay, sx, sy )
	return { x = x, y = y, ax = ax, ay = ay, sx = sx, sy = sy }
end

function text(x, y, text1, css )
	local o = { ["x"] = x, ["y"] = y, ["text"] = text1 }
	if css then
		o.css = css
	end
	return o
end

--返回结构体{x,y,res, on}，以简化程序
function res(x, y, res, on )
	local o = { ["x"] = x, ["y"] = y, ["res"] = res }
	if on then
		o.on = on
	end
	return o
end

--删除对象组
function clear( obj )
	for _, item in pairs( obj ) do
		if item then
		    item:removeFromParentAndCleanup( true )
		end
	end
end

--隐藏对象组
function hide(obj)
	for _, item in pairs( obj ) do
		item:setVisible(false)
	end
end

function d(s)
	print(LIGHT_UI.db_fmt(s))
end

--检查table中是否有值，如无则采用默认值，一般用于简化函数默认值处理
function setTableDefault( table, default_table )
	for key, value in pairs( default_table ) do
		if not table[key] then
			table[key] = value
		end
	end
end

--[[
传入样式表，如e4
返回结构
{ size, color ={r, g, b}, font }
]]
function css( p )
	local size = { ['a'] = 18, ['b'] = 20, ['c'] = 22, ['d'] = 24, ['e'] = 28, ['f'] = 16, ['g'] = 32,  }
	local color = {
		['1'] = {['r'] = 51, ['g'] = 51, ['b'] = 51},
		['2'] = {['r'] = 102, ['g'] = 102, ['b'] = 102},
		['3'] = {['r'] = 153, ['g'] = 153, ['b'] = 153},
		['4'] = {['r'] = 255, ['g'] = 255, ['b'] = 255},
		['5'] = {['r'] = 240, ['g'] = 240, ['b'] = 240},
		['7'] = {['r'] = 255, ['g'] = 204, ['b'] = 0},
		['8'] = {['r'] = 196, ['g'] = 196, ['b'] = 196},
		['9'] = {['r'] = 255, ['g'] = 0, ['b'] = 0},
		['10'] = {['r'] = 3, ['g'] = 131, ['b'] = 240},
		['11'] = {['r'] = 241, ['g'] = 55, ['b'] = 138},
		['12'] = {['r'] = 137, ['g'] = 67, ['b'] = 159},
		['13'] = {['r'] = 216, ['g'] = 177, ['b'] = 90},
		['14'] = {['r'] = 246, ['g'] = 159, ['b'] = 211},
		['15'] = {['r'] = 159, ['g'] = 225, ['b'] = 246},
		['16'] = {['r'] = 255, ['g'] = 255, ['b'] = 255},
		['17'] = {['r'] = 0, ['g'] = 0, ['b'] = 0},
		['18'] = {['r'] = 255, ['g'] = 198, ['b'] = 42},		
		}
	
	local size_key = string.sub( p, 1, 1 )
	local color_key = string.sub( p, 2, 3 )
	
	return {
		['size'] = size[size_key],
		['color'] = color[color_key],
		['font'] = GLOBAL_FONT,
	}
end

-- 因多个控件均要操作样式表的需求，将本部分实现抽离出来
function doSetTextColor( o, my_css )
	o:setTextColor( my_css.color.r, my_css.color.g, my_css.color.b)
end
	
function createLabel(parent, c)
	setTableDefault( c, {text='', css='a8'} )
	
	local my_css = css( c.css )
		
	local o = LIGHT_UI.clsLabel:New(parent, nil, nil, c.text, my_css.font, my_css.size, nZOrder )
	doSetTextColor( o, my_css )
	
	doConfig( o, c )
	
	return o
end

function createMultiLabel(parent, c)
	setTableDefault( c, {text='', css='a8', sx = 0, sy = 0 } )
	
	local my_css = css( c.css )
		
	local o = LIGHT_UI.clsMultiLabel:New( parent, nil, nil, c.text, my_css.font, my_css.size, 1, 1 )
	doSetTextColor( o, my_css )
	
	doConfig( o, c )
	
	return o
end	

TIMER = {}

--延时调用
function delayCall( op, para, ms )
	local function fn()
		return function()
			op( para )
			
			clearTimer( para )
		end
	end
	
	TIMER[ para ] = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( fn(), ms / 1000, false)
end


function clearTimer( id )
	if TIMER[ id ] then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry( TIMER[ id ] )
		TIMER[ id ] = nil
	end
end

function clearAllTimer()
	for k,_ in pairs(TIMER) do
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry( TIMER[ k ] )
		TIMER[ k ] = nil
	end
	TIMER = {}
end

function createSprite(parent, c, onload, nZOrder )
	local runDoConfig = false
	
	local function cb(o)
		if not runDoConfig then
			doConfig( o, c )
			runDoConfig = true
		else
			if c.sx then
				o:setContentSize( c.sx, c.sy )
			end
			
			if c.ax then
				o:setAnchorPoint(c.ax, c.ay)
			end
		end
		
		if onload then
			onload(o)
		end
	end
	--d(c.res)
	if not (isFileExists(c.res) == false) and c.res then	
		return nil
	end
	local o = LIGHT_UI.clsSprite:New(parent, nil, nil, c.res, cb, nZOrder )
	
	if not runDoConfig then
		doConfig( o, c )
		runDoConfig = true
	end
	
	return o
end

function createButton(parent, c, onclick, ms, stopMessage, zOrder )
	local btn_res = {
		[LIGHT_UI.NORMAL_STATUS] = c.res,
		[LIGHT_UI.DOWN_STATUS] = c.on or c.res,
	}
	local o = LIGHT_UI.clsBaseButton:New(parent, nil, nil, btn_res, stopMessage, zOrder )
	
	if c.text then
		local my_css = css( c.text_css or 'c4' )
		
		o:setString( c.text, my_css.font, my_css.size )
		doSetTextColor( o, my_css )
	end
	
	if onclick then
		o.onTouchUp = onclick
	end
	
	doConfig( o, c )
	
	if ms then
		delayCall( clear, {o}, ms )
	end
	
	return o
end

local exsitAlert = false
function setExsitAlert( value )
	exsitAlert = value
end

function getExsitAlert()
	return exsitAlert
end

function createAlert(parent,c,onok,oncancel)
	if exsitAlert == false then
		exsitAlert = true
	else		
		clear({p})
		return
	end
	local order = 0
	if parent then
		order = parent:getCOObj():getChildrenCount() + 120
	end		
	local config = c
	if not c then
		local BASE_LAYOUT = Import("layout/base.lua")
		config = BASE_LAYOUT.getData().alert
	end
	local p = createNode(parent,{x = 0,y = 0},nil,order)
	p:setAnchorPoint(0.5,0.5 )
	--[[local function receiveMsg()
		print('onclick')
	end--]]
	if config.bg then
		createButton(p,config.bg,nil)	
	end
	if config.window then
		createSprite(p,config.window)
	end	
	--[[if config.icon then
		createSprite(p,config.icon)
	end--]]
	if config.title then	
		createLabel(p,config.title)
	end
	if config.content then
		createLabel(p,config.content)
	end
	
	if config.content2 then
		createLabel(p,config.content2)
	end
	
	if config.content3 then
		createMultiLabel(p,config.content3)
	end
	
	local function onClickOk()
		clear({p})		
		if onok then
			onok()
		end			
	end
	
	local function onClickCancel()	
		exsitAlert = false
		if oncancel then
			oncancel()
		end
		clear({p})
	end
	
	if config.cancel_btn then
		create9Button(p,config.cancel_btn,onClickCancel)
	end
	if config.ok_btn then
		create9Button(p,config.ok_btn,onClickOk)
	end
	
	return p
end

function createCommonAlert( parent, content, onOk, onCancel, options )
	local BASE_LAYOUT = Import("layout/base.lua")
	local config = BASE_LAYOUT.getData().common_alert
	
	options = options or {}
	
	if options.config then
		config = deepcopy( config )
		local new_config = options.config
		
		-- 二层替换新值
		for _, section in pairs( new_config ) do
			for key, value in pairs( section ) do
				config[ section ][ key ] = value
			end
		end
	end
	
	config.content3.text = content
	
	return createAlert( parent, config, onOk, onCancel )
end

function createFrameButton( parent, c )
	local o = LIGHT_UI.clsFrameButton:New( parent, 0, 0, c.sx, c.sy, c.res, c.on or c.res )
	o:hideLine(0)
	
	doConfig( o, c )
	
	return o
end
--------------------------------------------------------------------
clsListButton = clsObject:Inherit()

--[[
创建通用表格按钮，以响应事件
参数：
	parent
	move_obj：不知什么作用，看着是继承父事件似的
	onclick：点击回调
返回值：
	表格按钮
	线
]]
function clsListButton:__init__( parent, move_obj, onclick )
	local BASE_LAYOUT = Import("layout/base.lua")
	local c = BASE_LAYOUT.getData()
	
	local o = createFrameButton( parent, c.list_button )
	o:hideLine(0)
	
	setMsgDelegate( o, move_obj, onclick )
	
	self._btn = o
	
	self._line = createSprite( parent, c.list_line )
end

--[[
设置表格高
]]
function clsListButton:setHeight( height )
	local BASE_LAYOUT = Import("layout/base.lua")
	local c = BASE_LAYOUT.getData()
	
	self._btn:setPixelScale( c.list_button.sx, height )
end
--------------------------------------------------------------------

--[[
创建表格分隔线
]]
function createListLine( parent )
	local BASE_LAYOUT = Import("layout/base.lua")
	local c = BASE_LAYOUT.getData()
	
	return createSprite( parent, c.list_line )
end

--显示一个对象，ms毫秒后隐藏
function view( obj, ms )
	obj:setVisible(true)
	
	if ms then
		delayCall( hide, {obj}, ms )
	end
end

--[[
命令处理
 file：接口文件名
 method: 类别
 data：参数，json形式
 callback：成功回调
 TTL：缓存有效期，数字（秒），被clear_expired删除过期缓存时用。nil表示不缓存。默认不需要缓存
 options：其它参数
--    need_login：1：（默认）本命令需要登录状态，如不在登录，则自动登录。 0：本命令不需判断登录与否
 	cachename: 如果有值，则指定缓存名字，否则由系统生成
 	priority:请求优先级
]]
function doCommand( file, method, data, callback, TTL, options)
	local url = ''
	local filename = nil		
	
	options = options or {}
--	options = options or { need_login = 1}
--	options.need_login = options.need_login or 1
	
	data.type = method
	
	for k, v in pairs(data) do	
		url = string.format( "%s&%s=%s", url, k, string.urlencode(v) )
	end
	url = string.sub( url, 2 )
	
	url = string.format("%s%s.php?%s",  GLOBAL.interface, file, url )
		
	if method ~= 'pop' then
		print( "requesting ", url )
	end
	
	local function cb( data )
		if data and tonumber(data) == 408 then
			if net_error == false then
				CCMessageBox('网络错误,连接失败','')
				net_error = true
			end
			return ""
		end
		net_error = false
		if TTL then
			delCache( filename )
			saveCache( filename, data )
		end			
		data = json.decode( data )
		data.code = tonumber(data.code)
		
		if TTL and data.code and data.code < 0 then
			delCache( filename )
		end			
		hideLoading()
		if TTL then
			callback( data, "refresh" )
		else
			callback( data )
		end
	end
	
	if GLOBAL.sid then
		url = url .. '&sid=' .. GLOBAL.sid
	end
	if options.loading then
		showLoading()
	end
	
	--d(url)	
	
	if TTL then
		filename = options.cachename or GetMD5( url )
		
		local content = getCache( filename )
		
		if content then
			--d( 'use cache, filename is :' .. filename..' \nurl: '..url )
			content = json.decode( content )
			if content.code then
				content.code = tonumber(content.code)
			end
			callback( content )
		
			if not isCacheExpires( filename, TTL ) then
				return	""			
			end
		end
	end
		
	HTTP_REQUEST.http_request(url, cb,options.priority)
	
	return url
end

--[[
比对文件是否失效
参数：
filename：完整文件名
second：秒
]]
function isFileExpires( filename, second )
	local curTime = tonumber(xymodule.get_curTime())
		
	local c,m,a = getFiletime( filename )
	
	return (curTime - a) > tonumber(second)
end

--复制table，以达到不改变原table的目的，
--抄自http://blog.sina.com.cn/s/blog_49bdd36d0100fdc1.html
function deepcopy(object)
	-- 20140212，弃用，如果一个月不报错，则此函数不再要了
	do return clone(object) end
	
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end  -- if
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end  -- for
        return setmetatable(new_table, getmetatable(object))
    end  -- function _copy
    return _copy(object)
end  -- function deepcopy

function empty_function()
end

function createNode( parent, config, ms,nZOrder )
	local o = LIGHT_UI.clsNode:New( parent, nil, nil, nZOrder)
	
	doConfig( o, config )
	
	if ms then
		o:getCOObj():retain()
		delayCall( clear, {o}, ms )
	end
	
	return o
end

function createLayer(parent)
	local o = LIGHT_UI.clsLayer:New( parent, 0, 0)
	return o
end

-- 注：Edit控件有bug，y方向会正向偏移了。暂不解决，暂通过对应改y值实现
function createEdit( parent, config )
	setTableDefault( config, { css = 'e8', sx = 400, sy = 100 } )
	
	local my_css = css( config.css )
	
	--通过曲折方法获得大小及位置值
	local data = nil
	if true then
		data = deepcopy( config )
		
		data.id = 'temp'
		doConfig( nil, data )
		
		local obj = PAGE.objtemp
		local c = config	-- 简化代码
		c.x0, c.x1, c.y0, c.y1 = nil, nil, nil, nil
		c.ax = 0
		c.ay = 1
		c.x = obj:getX0()
		c.y = obj:getY1()
		c.sx = obj:getSx()
		c.sy = obj:getSy()
	end
	
	local o = LIGHT_UI.clsEdit:New( parent, 0, 0, my_css.font, my_css.size, config.sx, config.sy )
	o:setTextColor( my_css.color.r, my_css.color.g, my_css.color.b)
	
	if config.text then
		o:setString( config.text )
	end
	
	if config.MaxInputChar then
		o:setMaxInputChar(32)
	end
	
	-- 暂时忽略拉伸
	o.getContentSize = function ()
		return { width = config.sx, height = config.sy }
	end
	config.sx = nil
	config.sy = nil
	
	doConfig( o, config )
	
	return o
end
--解决原来Edit销毁后位置偏移问题
function createEdit2( parent, config, limitSign )
	setTableDefault( config, { css = 'e8', sx = 400, sy = 100 } )
	
	local my_css = css( config.css )
	
	local o = LIGHT_UI.clsEdit:New( parent, 0, 0, my_css.font, my_css.size, config.sx, config.sy, limitSign )
	o:setTextColor( my_css.color.r, my_css.color.g, my_css.color.b)
	
	if config.text then
		o:setString( config.text )
	end		
	
	o:setPosition(config.x,config.y)
	
	return o
end

function createCheckButton( parent, config, onclick )
	config.on = config.on or config.res
	
	local o = LIGHT_UI.clsCheckButton:New( parent, 0, 0, config.res, config.on )
	
	--暂时忽略sx、sy参数
	o.getContentSize = empty_function
	config.sx = nil
	config.sy = nil
	
	doConfig( o, config )
	
	o.onTouchEnd = onclick
	
	return o
end

-- 播放背景音乐
function playMp3( file, ms )
	file = getResFilename( file )
	
	local function stopMp3()
		SimpleAudioEngine:sharedEngine():stopBackgroundMusic()
	end
	if isFileExists(file) == true then
		if getUserString( 'sound','1' ) ~= '0' then	
			--SimpleAudioEngine:sharedEngine():playBackgroundMusic( file)
			SimpleAudioEngine:sharedEngine():playBackgroundMusic( file, true)		
		end
	end
end

function preLoadMp3( file )
	--if CCApplication:sharedApplication():getTargetPlatform() == kTargetAndroid then
	file = getResFilename( file )	
	if isFileExists(file) == true then
		if getUserString( 'sound','1' ) ~= '0' then		
			SimpleAudioEngine:sharedEngine():preloadBackgroundMusic( file)	
		end	
	end
end

-- 播放音效
function playWav( file, ms )
	file = getResFilename( file )
	
	local effectID = nil
	local function stopWav()
		SimpleAudioEngine:sharedEngine():stopEffect(effectID)
	end
	if isFileExists(file) == true then
		if getUserString( 'sound','1' ) ~= '0' then	
			effectID = SimpleAudioEngine:sharedEngine():playEffect( file )		
		end
	end
	return effectID
end

function preLoadWav(file)
	file = getResFilename( file )
	if isFileExists(file) == true then	
		if getUserString( 'sound','1' ) ~= '0' then	
			SimpleAudioEngine:sharedEngine():preloadEffect( file )		
		end	
	end		
end

--x 中间对齐
function xCenter( o )
	LIGHT_UI.alignXCenter( o )
	--o:setAnchorPoint(0.5, 0.5)
end

--[[短暂显示
option参数包括：
ms：持续时间，默认为1000（毫秒）
]]
function showMessage( parent, content, option )
	option = option or {}
	setTableDefault( option, {ms=1000, y = 200} )
	local o = createNode( parent, point( 0, option.y ), option.ms,parent:getCOObj():getChildrenCount() + 30 )
	
	local bg = createSprite( o, res(0, 0, 'tiny_black_pixel.png') )
	local content = createLabel( o, text(0, 0, content, "c4" ) )
	
	local size = content:getContentSize()
	
	bg:setScaleX( size.width )
	bg:setScaleY( size.height )
	
	xCenter( o )
	
	return o
end

function showMultiMessage( parent, content, option )
	option = option or {}
	setTableDefault( option, {ms=option[ 1 ], y = 200} )
	local o = createNode( parent, point( 0, option.y ), option.ms )
	
	local bg = createSprite( o, {x = 0, y = 35, ax = 0.5, ay = 1, res = 'tiny_black_pixel.png'} )
	local contentTable = {		
			x = -165,
			y = 38,
			ax = 0.5,
			ay = 1,
			sx = 410,
			sy = 60,
			text = '',
			css = 'd4',
		}
		contentTable.text = content
	local content = createMultiLabel( o, contentTable )
	
	local size = content:getContentSize()
	
	bg:setScaleX( size.width )
	bg:setScaleY( size.height )
	local locX, locY = bg:getPosition()
	content:setPosition( locX - size.width / 2, locY )
	
	xCenter( o )
	
	return o
end

--可以显示到快捷支付上面
function showUpFloorMessage( parent, content, option )
	option = option or {}
	setTableDefault( option, {ms=1000, y = 200} )
	local o = createNode( parent, point( 0, option.y ), option.ms,parent:getCOObj():getChildrenCount() + 320 )
	
	local bg = createSprite( o, res(0, 0, 'tiny_black_pixel.png') )
	local content = createLabel( o, text(0, 0, content, "c4" ) )
	
	local size = content:getContentSize()
	
	bg:setScaleX( size.width )
	bg:setScaleY( size.height )
	
	xCenter( o )
	
	return o
end

function showUpFloorMultiMessage( parent, content, option )
	option = option or {}
	setTableDefault( option, {ms=1000, y = 200} )
	local o = createNode( parent, point( 0, option.y ), option.ms,parent:getCOObj():getChildrenCount() + 320 )
	
	local bg = createSprite( o, res(0, 0, 'tiny_black_pixel.png') )
	local contentTable = {		
			x = -165,
			y = 38,
			ax = 0.5,
			ay = 1,
			sx = 410,
			sy = 60,
			text = '',
			css = 'd4',
		}
	local content = createMultiLabel( o, contentTable )
	
	local size = content:getContentSize()
	
	bg:setScaleX( size.width )
	bg:setScaleY( size.height )
	
	xCenter( o )
	
	return o
end

--不自动销毁的MessageBox
function showMessageX( parent, content,option )
	option = option or {}
	setTableDefault( option, {x=100, y = 200} )
	local o = createNode( parent, point( option.x, option.y ) )
	
	local bg = createSprite( o, res(0, 0, 'tiny_black_pixel.png') )
	local content = createLabel( o, text(0, 0, content ) )
	
	local size = content:getContentSize()
	
	bg:setScaleX( size.width )
	bg:setScaleY( size.height )
	
	xCenter( o )
	
	return o
end

--单页面初始化，一个页面仅允许调用一次
function head()
	-- 默认一个页面只能调用一次本函数，否则此处会清空位置值
	PAGE = {}
end

--[[
创建基础界面，包括头、左上角返回键、标题、背景图
常用属性：
_head_text：标题
_onBack：返回键事件
edit:响应Edit事件
]]
function createBasePanel(parent, x, y)
	local BASE_LAYOUT = Import("layout/base.lua")
	local c = deepcopy( BASE_LAYOUT.getData() )

	local c2 = nil
	local o = nil
	
	head()

	local ret_panel = createNode( parent, point( x, y ) )
	
	--local ret_panel = createLayer( parent )
	
	ret_panel._head_bg = createSprite( ret_panel, c.head_bg )
	
	c2 = c.head_text
	local _head_text = createLabel( ret_panel, c2 )
	ret_panel._head_text = _head_text
	
	local function on_back(btn)
		if ret_panel._onBack then ret_panel._onBack() end
	end

	ret_panel._head_back = createButton( ret_panel, c.head_back, on_back )
	
	c2 = c.bg_pic
	--o = createSprite( ret_panel, c2 )
	--local function onClick()	
	--end
	--local c_button = {x = 0 ,y = 0,res = c2.res}
	
	-- 用于屏蔽穿透		
	o = createButton(ret_panel,c.bg_pic)	
	--ret_panel._back_pic = o
	
	return ret_panel
end

function createContainerChildPanel(parent, x, y)
	local BASE_LAYOUT = Import("layout/base.lua")
	local c = deepcopy( BASE_LAYOUT.getData() )

	local c2 = nil
	local o = nil
	
	head()

	local ret_panel = createNode( parent, point( x, y ) )
	
	ret_panel._head_bg = createNode( parent, c.head_bg )
	
	--c2 = c.head_text
	--local _head_text = createNode( parent, c.head_text )
	--ret_panel._head_text = _head_text
	
	local function on_back(btn)
		if ret_panel._onBack then ret_panel._onBack() end
	end

	--ret_panel._head_back = createNode( parent, point( x, y ) )
	
	c2 = c.bg_pic
	c2.y1 = display.sy - c.head_bg.sy
	o = createButton(ret_panel,c2)
	
	--ret_panel._back_pic = o
	
	return ret_panel
end

function getUserString( sKey, sDefault )
	local gUserData = CCUserDefault:sharedUserDefault()	
	local str = gUserData:getStringForKey( sKey )
	if str == '' then
		str = sDefault
	end
	return str
end

function setUserString( sKey, sValue )
	local gUserData = CCUserDefault:sharedUserDefault()
	local sResult = gUserData:setStringForKey( sKey, sValue )
	gUserData:flush()
	return sResult
end

local startx, starty
--抄自challenge
function setMsgDelegate(msg_obj, move_obj, click_func)
	local click_delta = 10
	
	
	local endx, endy
	local is_stop = false
	local function onTouchBegan(btn, x, y)
		if move_obj:isAutoMove() then
			is_stop = true
		end
		startx, starty = x, y
		--print( "startx:" .. startx )
		if btn.convertToWorldSpace then
			move_obj:onTouchBegan(btn:convertToWorldSpace(x, y))
		else
			move_obj:onTouchBegan(x,y)
		end
	end
	local function onTouchMove(btn, x, y)
		if btn.convertToWorldSpace then
			move_obj:onTouchMove(btn:convertToWorldSpace(x, y))
		else
			move_obj:onTouchMove(x,y)
		end			
	end
	local function onTouchEnd(btn, x, y)
		if not startx then
			return
		end
		endx, endy = x, y
		if btn.convertToWorldSpace then
			move_obj:onTouchEnd(btn:convertToWorldSpace(x, y))
		else
			move_obj:onTouchEnd(x,y)
		end
		--move_obj:onTouchEnd(btn:convertToWorldSpace(x, y))

		
		if ((math.abs(startx - endx) + math.abs(starty - endy)) <= click_delta) and (not is_stop) then
			click_func(btn, x, y)
		end
		is_stop = false
	end
	msg_obj.onTouchBegan = onTouchBegan
	msg_obj.onTouchMove = onTouchMove
	msg_obj.onTouchEnd = onTouchEnd
end

function setMsgDelegate2(msg_obj, move_obj, click_func)
	local click_delta = 10
	
	
	local endx, endy
	local is_stop = false
	local function onTouchBegan(btn, x, y)
		if move_obj:isAutoMove() then
			is_stop = true
		end
		startx, starty = x, y
		--print( "startx:" .. startx )
		if btn.convertToWorldSpace then
			move_obj:onTouchBegan(btn:convertToWorldSpace(x, y))
		else
			move_obj:onTouchBegan(x,y)
		end
	end
	local function onTouchMove(btn, x, y)
		if btn.convertToWorldSpace then
			move_obj:onTouchMove(btn:convertToWorldSpace(x, y))
		else
			move_obj:onTouchMove(x,y)
		end			
	end
	local function onTouchEnd(btn, x, y)
		--[[if not startx then
			return
		end--]]
		endx, endy = x, y
		if btn.convertToWorldSpace then
			move_obj:onTouchEnd(btn:convertToWorldSpace(x, y))
		else
			move_obj:onTouchEnd(x,y)
		end
		--move_obj:onTouchEnd(btn:convertToWorldSpace(x, y))

		
		--if ((math.abs(startx - endx) + math.abs(starty - endy)) <= click_delta) and (not is_stop) then
			click_func(btn, x, y)
		--end
		is_stop = false
	end
	msg_obj.onTouchBegan = onTouchBegan
	msg_obj.onTouchMove = onTouchMove
	msg_obj.onTouchEnd = onTouchEnd
end

function clearHTML( str )
	str = string.gsub( str, "<\/?[^>]*>",  '' )
	str = string.gsub( str, "&nbsp;", " ")
	return str
end

--[[
返回点九图所需的两个参数：原图尺寸、切割尺寸
参数：图片名字，不包括.9.png的部分
返回：返回两个rect，分别为原图尺寸，切割尺寸

本函数暂只用于create9Button
]]
local function get9Rect( file )
	local config = Import( 'lua/9.lua' ).getConfigData()
	
	if config[ file ].same then
		file = config[ file ].same
	end
	
	local size = config[ file ]
	local cut = config[ file .. '_cut' ]
	return CCRectMake( size.x, size.y, size.width, size.height ),CCRectMake( cut.x, cut.y, cut.width, cut.height )
end

--[[
处理相对位置
]]
function doConfig( obj, c )
	local function cal( x )
		if not x then
			return x
		end
		
		if tonumber(x ) then
			return x
		end
		
		x, _ = string.gsub( x, "%s*([_a-zA-Z0-9]*)%.([_a-zA-Z0-9]*)", function(name, p)
			if name == 'display' then
				if p == 'x' then
					return display.c_right
				elseif p == 'y' then
					return display.c_top
				elseif p == 'x0' then
					return display.left
				elseif p == 'x1' then
					return display.right
				elseif p == 'y0' then
					return display.bottom
				elseif p == 'y1' then
					return display.top
				elseif p == 'sx' then
					return display.width
				elseif p == 'sy' then
					return display.height
				end
			else
				local o = PAGE[ 'obj'..name ]
				
				if p == 'x0' then
					return o:getX0()
				elseif p == 'x1' then
					return o:getX1()
				elseif p == 'x' then
					return o:getX()
				elseif p == 'y0' then
					return o:getY0()
				elseif p == 'y1' then
					return o:getY1()
				elseif p == 'y' then
					return o:getY()
				elseif p == 'sx' then
					return o:getSx()
				elseif p == 'sy' then
					return o:getSy()
				end
			end
		end )
		return eval( x )
	end

	--[[
	分别处理x或y
	参数：
	k：x|y
	]]
	local function doXY( x )
		local x0, x1 = x .. '0', x .. '1'
		local ax = 'a' .. x
		local tx = 't' .. x
		local sx = 's' .. x
		
		if c[x0] and c[x1] then
			c[ax] = 0
			c[x] = c[x0]
			c[sx] = c[x1] ..  '-(' .. c[x0] .. ')'
		else
			if c.to then
				local ctx = tonumber(c[tx]) or 0.5
				c[x] = string.format( '%s.%s%s+%s', c.to, x, ( ctx == 0.5 ) and '' or c[tx], c[x] or 0 )
			end
		end
		
		c[x] = cal( c[x] )
		c[sx] = cal( c[sx] )
	end
	
	c = clone( c )
	
	doXY( 'x' )
	doXY( 'y' )
	
	local cx = tonum( c.x )
	local cy = tonum( c.y )
	
	if obj then
		if c.sx then
			obj:setContentSize( math.abs(c.sx), math.abs(c.sy) )
		end
	
		--默认居中对齐
		obj:setAnchorPoint( c.ax or .5, c.ay or .5 )
		
		obj:setPosition( cx, cy )
	end
	
	if c.id then
		if not obj then
			obj = LIGHT_UI.clsObject:New()
			obj.getPosition = function ()
				return cx, cy
			end
			obj.getAnchorPoint = function ()
				return { x = c.ax or .5, y = c.ay or .5 }
			end
			obj.getContentSize = function ()
				return { width = c.sx, height = c.sy }
			end
		end
		PAGE[ 'obj'.. c.id ] = obj
	end
end

--[[
创建9切图
参数：
parent：父
config：{}，按钮设置项，包括x,y,sx,sy,ax,ay,key
]]
function create9Sprite( parent, config )
	local key = config.key
	local res = key .. '.9.png'
	
	o = LIGHT_UI.clsScaleSprite:New( parent, nil, nil, res, get9Rect( key ) )
	o.setContentSize = o.setSpriteContentSize
	
	doConfig( o, config )
	
	return o
end

--[[
创建9切图按钮
参数：
parent：父
config：{}，按钮设置项，包括x,y,sx,sy,ax,ay,css,text
onclick：按钮响应
]]
function create9Button( parent, config, onclick )
	local key = config.css
	local btn_list = {
		[LIGHT_UI.NORMAL_STATUS] = key .. '.9.png',
		[LIGHT_UI.DOWN_STATUS] = key .. '_on.9.png',
	}
	
	o = LIGHT_UI.clsBaseSprite9Button:New(parent, nil, nil, btn_list, get9Rect( key ) )		
	--o.setContentSize = o.setSpriteContentSize
	--o.onTouchDown = onclick
	o.onTouchUp = onclick
	
	if config.text then
		local my_css = css( config.text_css or 'c4' )
		
		o:setString( config.text, my_css.font, my_css.size )
		o:setTextColor( my_css.color.r, my_css.color.g, my_css.color.b)
	end
	
	doConfig( o, config )
	
	return o
end

function createCustomStatusButton(parent, c, onclick)
	local btn_res = {
		[LIGHT_UI.NORMAL_STATUS] = c.res,
		[LIGHT_UI.DOWN_STATUS] = c.on or c.res,
	}
	local o = LIGHT_UI.clsCustomStatusButton:New(parent, 0, 0, btn_res)
	
	if c.text then
		local my_css = css( c.text_css or 'c4' )
		
		o:setString( c.text, my_css.font, my_css.size )
		doSetTextColor( o, my_css )
	end
	
	if onclick then
		o.onTouchUp = onclick
	end
	
	doConfig( o, c )
	
	return o
end

function createCustom9Button( parent, config, onclick )
	local key = config.css
	local btn_list = {
		[LIGHT_UI.NORMAL_STATUS] = key .. '.9.png',
		[LIGHT_UI.DOWN_STATUS] = key .. '_on.9.png',
	}
	
	o = LIGHT_UI.clsCustomBaseSprite9Button:New(parent, nil, nil, btn_list, get9Rect( key ) )		
	--o.setContentSize = o.setSpriteContentSize
	--o.onTouchDown = onclick
	o.onTouchUp = onclick
	
	if config.text then
		local my_css = css( config.text_css or 'c4' )
		
		o:setString( config.text, my_css.font, my_css.size )
		o:setTextColor( my_css.color.r, my_css.color.g, my_css.color.b)
	end
	
	doConfig( o, config )
	
	return o
end	

--[[
创建等高背景框
参数：
parent：父
config：设置，包括 row（行数）, sx, sy, css
]]
function createGrid( parent, config )
	local res = {
		white = {
			first = 'ic_first',
			normal = 'ic_normal',
			last = 'ic_last',
			single = 'ic_single',
		},
		yellow = {
			first = 'ic_first_on',
			normal = 'ic_normal_on',
			last = 'ic_last_on',
			single = 'ic_single_on.9.png',
		},
	}
	
	res = res[ config.css or 'white' ]
	
	local config1 = clone( config )
	
	local y1 = string.format( '%s+%s*(%s)', config.y or 0, 1-(config.ay or .5), config.sy or 1 )
	
	config1.ay = 1
	config1.y = y1
	config1.sy = string.format( '%s/%s', config.sy, config.row )
		
	for i = 1, config.row do
		if i == 1 then
			config1.key = config.row == 1 and res.single or res.first
		elseif i == config.row then
			config1.key = res.last
		else
			config1.key = res.normal
		end
		create9Sprite( parent, config1 )
		
		config1.y = string.format( '%s-%s', config1.y, config1.sy )
	end
	
	if config.id then
		doConfig( nil, config )
	end
end

--[[
将table转换为json形式的字符串
]]
function table2json(t)
	local function serialize(tbl)
		local tmp = {}
		for k, v in pairs(tbl) do
			if type(v) == 'string' then
				v = string.gsub(v, "\\", "\\\\")
				v = string.gsub(v, "\"", "\\\"")
			end
			local k_type = type(k)
			local v_type = type(v)
			local key = (k_type == "string" and "\"" .. k .. "\":")
				or (k_type == "number" and "")
			local value = (v_type == "table" and serialize(v))
				or (v_type == "boolean" and tostring(v))
				or (v_type == "string" and "\"" .. v .. "\"")
				or (v_type == "number" and v)
			tmp[#tmp + 1] = key and value and tostring(key) .. tostring(value) or nil
		end
		if table.maxn(tbl) == 0 then
			return "{" .. table.concat(tmp, ",") .. "}"
		else
			return "[" .. table.concat(tmp, ",") .. "]"
		end
	end
	assert(type(t) == "table")
	return serialize(t)
end

function createLabelAtlas( parent, config )
	local width = config.width
	local height = config.height
	local file = config.res
	local startCharMap = config.startCharMap
	
	o = LIGHT_UI.clsLabelAtlas:New(parent, 0, 0, file,"",width,height,string.byte(startCharMap))
	--o.setContentSize = o.setSpriteContentSize
	
	doConfig( o, config )
	
	return o
end

--[[
从配置值中获取信息，并修改某个key，然后返回
因多次使用，故抽取出来
参数：
	config：整个配置值，对应于layout
	section：section name
	key：key name
	value：new value
返回值：
	修改后的新section
]]
function replaceConfigValue( config, section, key, value )
	local c = deepcopy( config[section] )
	c[ key ] = value
	return c
end

--[[
实现通用跳转
参数：
	code：跳转代码
	data：跳转参数
	back_func：返回函数（除执行原页面关闭后，执行显示本页）
返回值：无
]]
function jump( code, data, back_func )
	local PLAYER_PAGE = Import("lua/player_info.lua")
	
	local function go_player_info( data )
		--[[PLAYER_PAGE.showByUserId( data.uid )
		PLAYER_PAGE.init( { back_func = back_func } )--]]
		local o = Import( "lua/view/other_msg_view.lua" )
		local f = o.otherMsgView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, data.uid, back_func )
		f:secondInit()
	end
	
	local function go_user_info( data )
		local USER_PAGE = Import("lua/user_info.lua")
		if PANEL_CONTAINER.closeChildPanel( nil, 16 ) then
			local USER_INFO_PANEL = Import("lua/user_info.lua")
			PANEL_CONTAINER.addChild( USER_INFO_PANEL.showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )							
		end
		--USER_PAGE.showUserInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		--USER_PAGE.set_back_func(back_func)
	end

	local function go_challenge( data )
		local CHALLENGE_PANEL = Import("lua/challenge.lua")
		--CHALLENGE_PANEL.showChallengePagePanel( HTTP_CLIENT.getRootNode(), 0, 0)
		if PANEL_CONTAINER.closeChildPanel( nil, 5 ) then
			PANEL_CONTAINER.addChild( CHALLENGE_PANEL.showChallengePagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
		end
	end

	local function go_shopping( data )
		local MALL_PAGE = Import("lua/mall.lua")

		MALL_PAGE.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	local function go_dice( data )
	end

	local function go_chat( data )
		local ONETOONE_CHAT_PANEL = Import("lua/one_to_one_chat.lua")
		ONETOONE_CHAT_PANEL.init( { uid2 = data.uid, name2 = data.name } )
		ONETOONE_CHAT_PANEL.showPanel( HTTP_CLIENT.getRootNode(), 0, 0 )
	end

	local function go_say( data )
		local CHAT_DETAIL_PAGE = Import("lua/chat_detail.lua")
		CHAT_DETAIL_PAGE.showDetailPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, data.id )
		CHAT_DETAIL_PAGE.init( { back_func = back_func } )
	end

	local function go_my_package( data )
		local USER_PACKAGE_PANEL = Import("lua/user_package.lua") 
		USER_PACKAGE_PANEL.showPackageInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0 , nil , nil , 5)
	end
	
	local function go_video_auth( data )
		local USER_PACKAGE_PANEL = Import("lua/video_auth.lua") 
		USER_PACKAGE_PANEL.showVideoAuthPagePanel(HTTP_CLIENT.getRootNode(), 0, 0,back_func)
	end

	local do_func_list = {
		--[[
		1800
		2003
		2000

		1400
		2300
		]]
		[0] = empty_function,
		[1100] = go_player_info,
		[1300] = go_video_auth,
		[1600] = go_challenge,
		--[2300] = go_dice,
		[2400] = go_challenge,
		--[2400] = go_shopping,
		[3502] = go_user_info,	-- 暂时跳到个人主页
		[3503] = go_user_info,	-- 暂时跳到个人主页
		[3504] = go_user_info,	-- 暂时跳到个人主页
		[1201] = go_chat,
		[1802] = go_say,
		[3400] = go_my_package,
		--
		[2300] = go_user_info,
	}

	do_func_list[ code ]( data )
end

-- 将new中的值覆盖default
function replaceTable( default, new )
	local ret = {}
	for k,v in pairs( default ) do
		ret[ k ] = v
	end
	for k,v in pairs( new ) do
		ret[ k ] = v
	end
	return ret
end

--设置指定长度的字符串
function AdjustString(label,txt,width)
	if label and txt and tonumber(width) > 0 then
		local i = 1	
		while(i <= string.len(txt)) do
			local str = ""
			local b = txt:byte(i)	
			if b > 128 then
				str = string.sub(txt,1,i+2)
				i = i + 3
			else
				str = string.sub(txt,1,i)
				i = i + 1
			end	
			label:setString(str)
			local orgSize = label:getContentSize()
			if orgSize.width >= width then			
				return 
			end
		end			
	end
end

--[[
根据原图尺寸和设定的尺寸，进行最大限度的等限度缩放
参数：
	old_x：原图宽
	old_y
	max_x：设定的最大宽
	max_y
返回值：
	宽，高
]]
function get_fit( old_x, old_y, max_x, max_y )
	if old_x <= max_x and old_y <= max_y then
		return old_x, old_y
	end
	
	local x, y = 0, 0
	if ( old_x / max_x ) > ( old_y / max_y ) then	-- 超宽
		x = max_x
		y = old_y * max_x / old_x
	else	-- 超高
		y = max_y
		x = old_x * max_y / old_y
	end

	return x, y
end

--通过URL获取文件名
function GetFileNameFromUrl(url)
	local reUrl = string.reverse(url)
	local index = string.find(reUrl,'\/')
	local file = string.sub(reUrl,1,index-1)
	return string.reverse(file)	
end

--修改后缀名
function ModifySuffix(filename,suffix)
	local str = string.reverse(filename)
	local index = string.find(str,'%p')
	if not index then 
		local i = 0
	end
	local str3 = string.sub(str,index + 1,string.len(str))
	return string.reverse(str3)..suffix	
end

--获取字符串md5
function GetMD5(str)
	return xymodule.get_md5(str)
end

--获取文件md5
function GetFileMD5(file)
	return xymodule.get_filemd5(file)
end

--获取cache路径
function GetCachePath()
	local cachepath = WRITE_PATH..'cache/'..tostring(GLOBAL.uid)..'/'
	return cachepath
end

--保存cache内容
function saveCache( name, data )
	WriteByte( GetCachePath() .. name,data )
end

--设置cache过期
function setCacheExpires( name )
	WriteByte( GetCachePath() .. name .. '.expires', '' )
end

--检查cache是否过期，返回true为过期，false为未过期
function isCacheExpires( name, TTL )
	if getFileContent( GetCachePath() .. name .. '.expires' ) then
		return true
	end
	
	local curTime = tonumber(xymodule.get_curTime())
	local aTime = tonumber(xymodule.get_atime(GetCachePath() .. name))
	if (curTime - aTime) <= tonumber(TTL) then
		return false
	end
	
	return true
end

--返回cache内容，如存在返回nil
function getCache( name )
	return getFileContent( GetCachePath() .. name )
end

--删除cache
function delCache( name )
	delFile( GetCachePath() .. name )
	delFile( GetCachePath() .. name .. '.expires' )
end

--获取data路径
function getDataFilename( name )
	return WRITE_PATH .. 'data/' .. name
end

--[[
使用data目录文件，如果不存在，则从远程获取
 file：文件名
 second：缓存时间，0表示不缓存但获得到的内容会更新缓存文件，-1为不使用缓存且不会更新缓存文件
 callback：成功回调，如果当然存在缓存但失效，则会触发两次
 url：请求的地址，如不传入，则约定取远程对应的 [interface]/data/[section]
]]
function downRemoteData( section, second, callback, url )
	local filename = nil
	local content = nil
	
	filename = getDataFilename( section )
	
	local function cb( data )
		if not data then
			return
		end
		if not content or ( GetMD5(content) ~= getMD5(data) ) then
			if second > -1 then
				WriteByte( filename, data )
			end
			
			data = json.decode( data )
			callback( data )
		end
	end
	
	if second > 0 then
		content = getFileContent( filename )
		
		if content then
			callback( json.decode( content ) )
		
			if not isFileExpires( filename, second ) then
				return
			end
		end
	end
	
	if not url then
		url = GLOBAL.interface .. 'data/' .. section
	end
	HTTP_REQUEST.http_request( url, cb )
end

function writeData( section, json )
	local filename = getDataFilename( section )
	WriteByte( filename, table2json( json ) )
end

--[[
删除整个data文件
]]
function delData( section )
	local filename = getDataFilename( section )

	delFile( filename )
end

--[[
设置data的key
]]
function setData( section, key, value )
	local filename = getDataFilename( section )
	
	local data = getData( section )
	if not data then 
		data = {}
	end
	data[ key ] = value
	
	local content = table2json(data)
	WriteByte( filename, content )
end

--返回data(json结构)，如不存在返回nil
function getData( section )
	local filename = nil
	filename = getDataFilename( section )
	content = getFileContent( filename )
		
	return content and json.decode( content ) or nil
end
-- 获得文件内容，如文件不存在，返回nil
function getFileContent( filename )
	local file,err = io.open( filename )
	
	local content = nil
	if file then
		content = file:read( '*a' )
		file:close()
	end
	
	return content
end

function delFile( filename )
	 os.remove( filename )
end

--写缓存(二进制数据)
function WriteByte(filename,data)
	assert(filename)
	local file = io.open(filename, "wb")
	if file then
		file:write(data)
		file:close()
		return true		
	end
	return false
end

function getResPath()
	return WRITE_PATH .. 'res/'
end

-- 加上前辍，获得资源文件名
function getResFilename( filename )
	return filename and getResPath() .. filename or nil
end	

--检测更新
function checkUpdate()
	
	local version = CCUserDefault:sharedUserDefault():getStringForKey("version")
	local engine = CCUserDefault:sharedUserDefault():getStringForKey("engine")	
	--local version = "1374542591"
	if string.len(version) <= 0 then
		version = tostring(TIME_VERSION)
	end
	local function onUpdate(data)
		if data.code < 0 then 
		    return
		end	
		--d(data.time)
		if(tonumber(data.time) > tonumber(version)) then		
			local z_path = WRITE_PATH..'temp/'..data.time..'.zip'			
			--local dec_data = base64Decode(data.data)
			local dec_data = xymodule.base64_decode(data.length,data.data)
			--print(dec_data)
			WriteByte(z_path,dec_data)	
			CCUserDefault:sharedUserDefault():setStringForKey("version",data.time);
			CCUserDefault:sharedUserDefault():flush()				
		end
	end
	local loginData = getData( 'current' )
	local uid = 0
	if loginData then 
		uid = loginData.uid
	end
	local size = CCDirector:sharedDirector():getWinSize()
	doCommand( 'system', 'update_lua', { time = version,version=engine,
	uid=uid,width=display.sx,height=display.sy},onUpdate )	
end

--base64解码
function base64Decode(data)
	local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
	data = string.gsub(data, '[^'..b..'=]', '')
    return (data:gsub('.', function(x)
        if (x == '=') then return '' end
        local r,f='',(b:find(x)-1)
        for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
        return r;
    end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
        if (#x ~= 8) then return '' end
        local c=0
        for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
        return string.char(c)
    end))
end

--base64编码
function base64Encode(data)
	local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    return ((data:gsub('.', function(x) 
        local r,b='',x:byte()
        for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
        return r;
    end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
        if (#x < 6) then return '' end
        local c=0
        for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
        return b:sub(c+1,c+1)
    end)..({ '', '==', '=' })[#data%3+1])
end

--获取文件时间
function getFiletime(file)
	local c = xymodule.get_ctime(file)
	local m = xymodule.get_mtime(file)
	local a = xymodule.get_atime(file)
	return c,m,a
end

--清空缓存
function clearCache()
	xymodule.delete_file(GetCachePath())
end

--显示Loading动画
local load_loading = nil
function showLoading()
	if load_loading == nil then
		safe_dofile("lua/loading.lua")
	end		
	load_loading = Loading(HTTP_CLIENT.getRootNode())	
end

function hideLoading()
	if load_loading then
		HLoading()
	end
end

--获取指定字符个数字符串
function getCharString(txt,count)
	local str = ""
	local cnt = 0
	local i = 1
	if not txt or type(txt) ~= 'string' then
		return
	else
		while(i <= string.len(txt)) do
		local len = string.len(txt)
		local b = txt:byte(i)				
			if b > 128 then
				str = string.sub(txt,1,i+2)
				i = i + 3
			else
				str = string.sub(txt,1,i)
				i = i + 1
			end
			cnt = cnt + 1
			if cnt >= count then
				break
			end
		end
	end
	
	return str
end

local readyParentNode
--显示等待动画
function ShowReady( node, c )
	HideReady()
	readyParentNode = node	
	readyParentNode.loading = createSprite( readyParentNode, c )
	--ready:setPosition(c.center.x + 15,c.center.y)	
    a_play(readyParentNode.loading:getSprite(),c,true)	
end

function HideReady()
	if readyParentNode and readyParentNode.loading then
		clear({readyParentNode.loading})
		readyParentNode.loading = nil
	end
end

function split(s, delim)  
	assert (type (delim) == "string" and string.len (delim) > 0,          "bad delimiter")  
	local start = 1  local t = {}  -- results table  -- find each instance of a string followed by the delimiter  
	while true do    
	local pos = string.find (s, delim, start, true) -- plain find    
	if not pos then      
		
	break    
	end    
	table.insert (t, string.sub (s, start, pos - 1	))    
	start = pos + string.len (delim)  
	end -- while  
	-- insert final one (after last delimiter	)  
	table.insert (t, string.sub (s, start))  
	return t
end -- function split	

--检查文件是否存在
function isFileExists(filename)
	--d(filename)
	if not filename then
		return false
	end
	local file,err = io.open( filename )
	if file then
		file:close()
		return true
	end
	return false
end

--友盟具体点击事件ID
function dailyCount(data,t)
	if xymodule.dailycount then
		if not t then
			t = 0
		end
		xymodule.dailycount(data,t)
	end
end

function escape( s )
	s = string.gsub( s, "[&=+%%%C]", function( c )
		return string.format( "%%%02X", string.byte( c ) )
		end )
	s = string.gsub( s, " ", "+" )
	return s		
end

--[[ by dda, 20140109，本函数已不用function urlencode( t )
	local b = {}
	for k,v in pairs( t ) do
		--b[ #b + 1 ]  = ( escape( k ) .. "=" .. escape( v ) )
		b[ #b + 1 ]  = escape( v )
	end
	return table.concat( b, "&" )	
end
]]

--Create an class.
function class(classname, super)
    local superType = type(super)
    local cls

    if superType ~= "function" and superType ~= "table" then
        superType = nil
        super = nil
    end

    if superType == "function" or (super and super.__ctype == 1) then
        -- inherited from native C++ Object
        cls = {}

        if superType == "table" then
            -- copy fields from super
            for k,v in pairs(super) do cls[k] = v end
            cls.__create = super.__create
            cls.super    = super
        else
            cls.__create = super
        end

        cls.ctor    = function() end
        cls.__cname = classname
        cls.__ctype = 1

        function cls.new(...)
            local instance = cls.__create(...)
            -- copy fields from class to native object
            for k,v in pairs(cls) do instance[k] = v end
            instance.class = cls
            instance:ctor(...)
            return instance
        end

    else
        -- inherited from Lua Object
        if super then
            cls = clone(super)
            cls.super = super
        else
            cls = {ctor = function() end}
        end

        cls.__cname = classname
        cls.__ctype = 2 -- lua
        cls.__index = cls

        function cls.new(...)
            local instance = setmetatable({}, cls)
            instance.class = cls
            instance:ctor(...)
            return instance
        end
    end

    return cls
end

--[[
20140121，好像没用
function createList()
	local o = LIGHT_UI.clsList:New( parent, 0, 0)
	o:create()
	return o
end	
]]

function playLinkSound( ... )
	playWav( select( 1, ... ) )
	local soundCount = select( "#", ... )
	local i
	soundTimer = {}
	soundArr = {}
	local count = 2
	
	local function playSoundHandler()
		if soundTimer[ count ] then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(soundTimer[ count ])
			soundTimer[ count ] = nil	
		end
		playWav( soundArr[ count ] )
		count = count + 2
	end
	for i = 2, soundCount, 2 do
		if not soundTimer[ i ] then
			soundTimer[ i ] = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( playSoundHandler, select( i + 1, ... ), false )
		end	
		soundArr[ i ] = select( i, ... )
	end
end

function createListView(parent, x, y, width, height,style,page, c)
	local o 
	if c then
		local x, y, width, height,style,page = c.x,c.y,c.sx,c.sy,c.style,c.page
		o = LIGHT_UI.clsListView:New(parent, 1, 1, 1, 1,style,page)
	else		
		o = LIGHT_UI.clsListView:New(parent, x, y, width, height,style,page)
	end
	
	function scrollViewDidScroll(view)
		return o:scrollViewDidScroll(view)
	end
	function cellSizeForTable(table,idx)
		return o:cellSizeForTable(table,idx)
	end
	function tableCellAtIndex(table, idx)
		return o:tableCellAtIndex(table, idx)
	end
	function numberOfCellsInTableView(table)
		return o:numberOfCellsInTableView(table)
	end
	function tableCellTouched(table,cell)
		return o:tableCellTouched(table,cell)
	end
	function scrollViewEndScroll(view,idx)
		return o:scrollViewEndScroll(view,idx)
	end
		
	o:registerScriptHandler(scrollViewDidScroll,CCTableView.kTableViewScroll)
	o:registerScriptHandler(scrollViewEndScroll,CCTableView.kTableViewEndScroll)
	o:registerScriptHandler(cellSizeForTable,CCTableView.kTableCellSizeForIndex)
	o:registerScriptHandler(tableCellAtIndex,CCTableView.kTableCellSizeAtIndex)
	o:registerScriptHandler(numberOfCellsInTableView,CCTableView.kNumberOfCellsInTableView)	
	o:registerScriptHandler(tableCellTouched,CCTableView.kTableCellTouched)	
	
	if c then
		doConfig( o, c )
	end

	return o
end

function createListCell()
	local o = LIGHT_UI.clsListCell:New()
	return o
end

function createEditBox(parent,config)
	setTableDefault( config, { css = 'e8', sx = 400, sy = 100 } )
	local my_css = css( config.css )
	local bg = getResPath() .. "tiny_bg_white_pixel.png"
	local o = LIGHT_UI.clsEditBox:New(parent, 0, 0, config.sx,config.sy,bg)
	o:setFontName(my_css.font)
	o:setFontSize(my_css.size)
    o:setFontColor(my_css.color.r, my_css.color.g, my_css.color.b)	
    --o:setPlaceholderFontColor(ccc3(0,0,0))	
	
	if config.MaxInputChar then
		o:setMaxInputChar( config.MaxInputChar )
	end
	if config.text then
		o:setPlaceHolder( config.text )
	end	
	doConfig( o, config )
	return o
end

--[[
获得用户数据，自己的用户数据保存在data目录，其它人的数据文件保存在缓存目录。
参数：
uid：要获取的uid，如果不传入，则取当前登录者
返回：
用户数据，table形式，如不存在，返回{}
]]
function getUserData( uid )
	if not uid then
		uid = GLOBAL.uid
	end
	
	local data = nil
	
	if uid == GLOBAL.uid then
		data = getData( 'uid_' .. uid )
	else
		local content = getCache( 'uid_' .. uid )
		
		if content then
			data = json.decode(content)
		end
	end
	
	return data or {}
end
function writeUserData( uid,table )
	if uid == GLOBAL.uid then
		writeData( 'uid_' .. uid, table )
	else
		local filename = GetCachePath() .. 'uid_' .. uid
		WriteByte( filename, table2json(table) )
	end
end
--[[
设置用户属性
参数：
data：数据，内含用户属性
uid_key：用户uid字段值
field：用户字段映射，key为data中的属性名，value为用户属性名
]]
function setUsersData( data, uid_key, field )
	local uid = data[ uid_key ]
	local userData = getUserData( uid )
	
	for k,v in pairs( field ) do
		if tonumber(k) == k then
			k = v
		end
		userData[ v ] = data[ k ]
	end
	
	writeUserData( uid, userData )
end

--[[
设置头像
参数：
obj：sprite对象
config：设置值对象
uid：
type：可选，类型，为 s||l，默认为空，显示中图
sex：可选，性别，当没头像时，先显示默认值
ms：可选，显示默认值多久后，才显示真正的图片，默认为500毫秒
]]
function setUserLogo( parent, config, uid, type, ms )
	local function findCache( name )
		local list = { 'jpg', 'png' }
		
		for _,v in pairs( list ) do
			local full_filename = name .. v
			local file,err = io.open( full_filename )
			if file then
				file:close()
				return full_filename
			end
		end
		
		return nil
	end
	
	local function findLogoCache( data, type )
		if not data[ type .. 'logo' ] then
			return nil, nil
		end
		
		local url = data[ type .. 'logo' ]
		
		local name = GetCachePath() .. GetMD5( url ) .. '.'
		return url, findCache( name )
	end
	
	local function changeRes( data )
		local o = data.obj
		
		o:setImage( data.url )
	end
	
	local data = getUserData( uid )
	
	type = type or ''
	
	local logo_url, logo_filename = findLogoCache( data, type )
	
	if not logo_filename then
		for _,v in ipairs( {'l', '', 's'} ) do
			if v ~= type then
				local logo_url2 = nil
				logo_url2, logo_filename = findLogoCache( data, v )
				
				if logo_filename then
					break
				end
				
				if logo_url2 and not logo_url then
					logo_url = logo_url2
				end
			end
		end
	end
	
	if logo_filename then
		--注意：此处改写了table对象，如后续还要有用，则必须复制而不是直接改写
		local o = createSprite( parent, {} )
		o:setImage( logo_filename, nil, true )
		doConfig( o, config )
		--o._RootNode:retain()
		return o
	end

	local c = deepcopy( config )
	c.res = 'sex' .. (data.sex or 2) .. '.png'
	
	local o = createSprite( parent, c )
	o._RootNode:retain()
	
	delayCall( changeRes, { obj = o, url = logo_url }, ms or 500 )
	return o
end

--[[
下载图片并保存
参数：
url：要下载的图片完整url
filename：保存文件名，可选，如无，则自动保存在缓存目录
]]
function downImg( url, filename )
	local function cb()
	
	end
	if not filename then
		filename = GetCachePath() .. GetMD5( url ) .. '.'
	end
	HTTP_REQUEST.http_download_pic( url,filename, cb )
end

--[[记录返回信息 sign 标志 1返回页面(旧) 2返回页面(新) 3非返回页面(旧) 4非返回页面(新)  5带tabbar的作为类看待的页面(新)
path 路径 params 参数组 pageId 页面id funName show行为的方法名（旧思路） className 类名（新思路）
接下来的参数针对带tabbar的作为类看的页面 childPath 子路径 childClassName 子类名 node 可视结点 nodeAttribute 结点属性--]]
--add_back( 1, "lua/challenge.lua", paramArr, nil, "show", nil )
--add_back( 2, "lua/view/other_msg_view.lua", paramArr, nil, nil, "otherMsgView" )
--add_back( 3, "lua/main_panel.lua", nil, 9, "createMainPanel", nil )
--add_back( 4, "lua/view/my_msg_view.lua", nil, 16, nil, "myMsgView" )
--add_back( 5, "lua/view/my_msg_view.lua", nil, 16, nil, "myMsgView", "lua/view/my_data_view.lua", "myDataView", "myMsgPage", "myDataPage" )
function add_back( sign, path, params, pageId, funName, className, childPath, childClassName, node, nodeAttribute )
	--isExitGame = false	
	if not ON_BACK then
		ON_BACK = {}
	end
		
	local t = {}
	t[ "sign" ] = sign
	t[ "path" ]= path
	if params then
		t[ "params" ] = params
	end
	if pageId then
		t[ "pageId" ] = pageId
	end
	if funName then
		t[ "funName" ] = funName
	end
	if className then
		t[ "className" ] = className
	end
	if childPath then
		t[ "childPath" ] = childPath
	end
	if childClassName then
		t[ "childClassName" ] = childClassName
	end
	if node then
		t[ "node" ] = node
	end
	if nodeAttribute then
		t[ "nodeAttribute" ] = nodeAttribute
	end
	table.insert( ON_BACK, t )
	--print( 'page count' .. #ON_BACK )

	--table.insert(ON_BACK_FUNCTION , mFunction)--]]
	--[[local function turnOnBack()
		showMessage(parent , "再按一次退出游戏")
	end
	if xymodule.advise_callback then
		xymodule.advise_callback(turnOnBack)
	end]]
end

--清除返回信息
local function delete_back()
	--isExitGame = false
	--[[local idx = 1
	for k,v in ipairs(ON_BACK) do
		if v["page"] == page then
			table.remove(ON_BACK,idx)
			break
			--v = nil
		end
		idx = idx + 1		
	end--]]
	if ON_BACK then
		if #ON_BACK > 0 then
			local o =  table.remove( ON_BACK )
			--print( 'page count' .. #ON_BACK )
			if #ON_BACK == 0 then
				ON_BACK = nil
			end	
			
			return o
		end	
	end		
	return nil
end

function decodeBackTable()
	local o = delete_back()
	local path, params, pageId, funName, className, childPath, childClassName, node, nodeAttribute
	if o then
		if o.sign then
			if o.sign < 1 and o.sign > 5 then
				error( "mode don't match" )
			end
			if o.sign == 1 then
				if o.path then
					path = o.path
				else
					error( "mode don't match" )
				end
				if o.params then
					params = o.params
				else
					error( "mode don't match" )
				end
				if o.funName then
					funName = o.funName
				else
					error( "mode don't match" )
				end	
				return path, params, funName
			end
			if o.sign == 2 then
				if o.path then
					path = o.path
				else
					error( "mode don't match" )
				end
				if o.params then
					params = o.params
				else
					error( "mode don't match" )
				end
				if o.className then
					className = o.className
				else
					error( "mode don't match" )
				end	
				return path, params, className
			end
			if o.sign == 3 then
				if o.path then
					path = o.path
				else
					error( "mode don't match" )
				end
				if o.pageId then
					pageId = o.pageId
				else
					error( "mode don't match" )
				end
				if o.funName then
					funName = o.funName
				else
					error( "mode don't match" )
				end	
				return path, pageId, funName
			end
			if o.sign == 4 then
				if o.path then
					path = o.path
				else
					error( "mode don't match" )
				end
				if o.pageId then
					pageId = o.pageId
				else
					error( "mode don't match" )
				end
				if o.className then
					className = o.className
				else
					error( "mode don't match" )
				end	
				return path, pageId, className
			end
			if o.sign == 5 then
				if o.path then
					path = o.path
				else
					error( "mode don't match" )
				end
				if o.pageId then
					pageId = o.pageId
				else
					error( "mode don't match" )
				end
				if o.className then
					className = o.className
				else
					error( "mode don't match" )
				end	
				if o.childPath then
					childPath = o.childPath
				else
					error( "mode don't match" )
				end	
				if o.childClassName then
					childClassName = o.childClassName
				else
					error( "mode don't match" )
				end	
				if o.node then
					node = o.node
				else
					error( "mode don't match" )
				end	
				if o.nodeAttribute then
					nodeAttribute = o.nodeAttribute
				else
					error( "mode don't match" )
				end	
				return path, pageId, className, childPath, childClassName, node, nodeAttribute
			end
		else
			error( "mode don't match" )
		end
	else
		error( "nothing for back" )
	end
end

--返回键执行返回函数
function turnOnBack()
	if #ON_BACK >= 0 then
		if not isExitGame then
			--if ON_BACK[#ON_BACK].base == true then
			if #ON_BACK <= 2 then
				--[[if #ON_BACK == 2 then
					PANEL_CONTAINER.clickButton()
				end--]]
				--xymodule.share()				
					--CCMessageBox("在按一次退出游戏","title")	
					--CCMessageBox("在按一次退出游戏","title")
					local function onBack()
						showMessage(ON_BACK[#ON_BACK].page , '再按一次退出游戏',{ms=2000})
						showMessage(ON_BACK[1].page , '再按一次退出游戏',{ms=2000})
					end	
					local function onReset()
						isExitGame = false
					end							
					isExitGame = true			
					f_delay_do(ON_BACK[#ON_BACK].page,onBack,nil,0)	
					f_delay_do(ON_BACK[#ON_BACK].page,onReset,nil,2)		
			else
				--CCMessageBox('返回','title')			
				f_delay_do(ON_BACK[#ON_BACK].page,ON_BACK[#ON_BACK].fun,nil,0)					
			end		
		else
			isExitGame = false
			xymodule.exit_process()
			--
		end			
	end	
end

--返回唯一key
function getUniverKey()
	return os.time() .. math.random()
end

--[[
从父中取出子的属性，如果不存在，则从默认值中取对应的子
参数：
parent：父对象
defautlt：默认父对象，当父对象中找不到子时，会改在这里取
son：子描述，字符串形式，如果为孙，则用半角句号隔开
]]
function getSon( parent, default_parent, son )
	local function g( parent, son )
		return parent and parent[son] or nil
	end
	local _, i = string.find( son, "[.]" )
	
	if i then
		local son_first = string.sub( son, 1, i - 1 )
		local son_second = string.sub( son, i + 1, -1 )
		return getSon( g(parent, son_first), g(default_parent, son_first), son_second )
	else
		return g(parent, son) or g(default_parent, son)
	end
end

--[[
传入完整日期，返回年月日时分秒
]]
function getPartByDate(r)
	local a = split(r, " ")
    local b = split(a[1], "-")
    local c = split(a[2], ":")
	
	return b[1],b[2],b[3],c[1],c[2],c[3]
end

---- 通过日期获取秒 yyyy-MM-dd HH:mm:ss
function getSecondByDate(r)
	local y,m,d,h,i,s = getPartByDate(r)
	local t = os.time({year=y,month=m,day=d, hour=h, min=i, sec=s})
    
	return t
end

--[[
根据传入的timestamp，返回友好的时间值
]]
function getStyleTime( strtimestamp )
	local timestamp = tonumber(strtimestamp)
	local today = os.date("*t")
	local secondOfToday = os.time({day=today.day, month=today.month,
		year=today.year, hour=0, minute=0, second=0})
	
	if timestamp > secondOfToday then
		return os.date( '%H:%M', timestamp )
	elseif timestamp > secondOfToday - 86400 then
		return os.date( '昨天 %H:%M', timestamp )
	else
		return os.date( '%Y-%m-%d %H:%M', timestamp )
	end
end

--[[
返回字符串的运算结果
]]
function eval(str)
    if type(str) == "string" then
        return loadstring("return " .. str)()
    elseif type(str) == "number" then
        return loadstring("return " .. tostring(str))()
    else
        error("is not a string")
    end
end