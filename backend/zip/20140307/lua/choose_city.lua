CITY_LAYOUT = Import("layout/choose_city.lua")
local CityConfig = CITY_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
local USER_PAGE = Import("lua/user_info.lua") 
local cityPage = nil
local gchat_id = nil
local gcity_id = nil
local province_panel = nil
local city_panel = nil

local city_list = {
	["北京"] = {
		[1] = "北京",
	},
	["天津"] = {
		[1] = "天津",
	},
	["河北"] = {
		[1] = "石家庄",
        [2] = "唐山",
        [3] = "秦皇岛",
        [4] = "邯郸",
        [5] = "邢台",
        [6] = "保定",
        [7] = "张家口",
        [8] = "承德",
        [9] = "沧州",
        [10] = "廊坊",
        [11] = "衡水",
	},
	["山西"] = {
		[1] = "太原",
        [2] = "大同",
        [3] = "阳泉",
        [4] = "长治",
        [5] = "晋城",
        [6] = "朔州",
        [7] = "晋中",
        [8] = "运城",
        [9] = "忻州",
        [10] = "临汾",
        [11] = "吕梁",
	},
	["内蒙古"] = {
		[1] = "呼和浩特",
        [2] = "包头",
        [3] = "乌海",
        [4] = "赤峰",
        [5] = "通辽",
        [6] = "鄂尔多斯",
        [7] = "呼伦贝尔",
        [8] = "巴彦淖尔",
        [9] = "乌兰察布",
        [10] = "兴安盟",
        [11] = "锡林郭勒盟",
        [12] = "阿拉善盟",
	},
	["辽宁"] = {
		[1] = "沈阳",
        [2] = "大连",
        [3] = "鞍山",
        [4] = "抚顺",
        [5] = "本溪",
        [6] = "丹东",
        [7] = "锦州",
        [8] = "营口",
        [9] = "阜新",
        [10] = "辽阳",
        [11] = "盘锦",
        [12] = "铁岭",
        [13] = "朝阳",
        [14] = "葫芦岛",
	},
	["吉林"] = {
		[1] = "长春",
        [2] = "吉林",
        [3] = "四平",
        [4] = "辽源",
        [5] = "通化",
        [6] = "白山",
        [7] = "松原",
        [8] = "白城",
        [9] = "延边",
	},
	["黑龙江"] = {
		[1] = "哈尔滨",
        [2] = "齐齐哈尔",
        [3] = "鸡西",
        [4] = "鹤岗",
        [5] = "双鸭山",
        [6] = "大庆",
        [7] = "伊春",
        [8] = "佳木斯",
        [9] = "七台河",
        [10] = "牡丹江",
        [11] = "黑河",
        [12] = "绥化",
        [13] = "大兴安岭",
	},
	["上海"] = {
		[1] = "上海",
	},
	["江苏"] = {
		[1] = "南京",
        [2] = "无锡",
        [3] = "徐州",
        [4] = "常州",
        [5] = "苏州",
        [6] = "南通",
        [7] = "连云港",
        [8] = "淮安",
        [9] = "盐城",
        [10] = "扬州",
        [11] = "镇江",
        [12] = "泰州",
        [13] = "宿迁",
	},
	["浙江"] = {
		[1] = "杭州",
        [2] = "宁波",
        [3] = "温州",
        [4] = "嘉兴",
        [5] = "湖州",
        [6] = "绍兴",
        [7] = "金华",
        [8] = "衢州",
        [9] = "舟山",
        [10] = "台州",
        [11] = "丽水",
	},
	["安徽"] = {
		[1] = "合肥",
        [2] = "芜湖",
        [3] = "蚌埠",
        [4] = "淮南",
        [5] = "马鞍山",
        [6] = "淮北",
        [7] = "铜陵",
        [8] = "安庆",
        [9] = "黄山",
        [10] = "滁州",
        [11] = "阜阳",
        [12] = "宿州",
        [13] = "巢湖",
        [14] = "六安",
        [15] = "亳州",
        [16] = "池州",
        [17] = "宣城",
	},
	["福建"] = {
		[1] = "福州",
        [2] = "厦门",
        [3] = "莆田",
        [4] = "三明",
        [5] = "泉州",
        [6] = "漳州",
        [7] = "南平",
        [8] = "龙岩",
        [9] = "宁德",
	},
	["江西"] = {
		[1] = "南昌",
        [2] = "景德镇",
        [3] = "萍乡",
        [4] = "九江",
        [5] = "新余",
        [6] = "鹰潭",
        [7] = "赣州",
        [8] = "吉安",
        [9] = "宜春",
        [10] = "抚州",
        [11] = "上饶",
	},
	["山东"] = {
		[1] = "济南",
        [2] = "青岛",
        [3] = "淄博",
        [4] = "枣庄",
        [5] = "东营",
        [6] = "烟台",
        [7] = "潍坊",
        [8] = "济宁",
        [9] = "泰安",
        [10] = "威海",
        [11] = "日照",
        [12] = "莱芜",
        [13] = "临沂",
        [14] = "德州",
        [15] = "聊城",
        [16] = "滨州",
        [17] = "荷泽",
	},
	["河南"] = {
		[1] = "郑州",
        [2] = "开封",
        [3] = "洛阳",
        [4] = "平顶山",
        [5] = "安阳",
        [6] = "鹤壁",
        [7] = "新乡",
        [8] = "焦作",
        [9] = "濮阳",
        [10] = "许昌",
        [11] = "漯河",
        [12] = "三门峡",
        [13] = "南阳",
        [14] = "商丘",
        [15] = "信阳",
        [16] = "周口",
        [17] = "驻马店",
	},
	["湖北"] = {
		[1] = "武汉",
        [2] = "黄石",
        [3] = "十堰",
        [4] = "宜昌",
        [5] = "襄樊",
        [6] = "鄂州",
        [7] = "荆门",
        [8] = "孝感",
        [9] = "荆州",
        [10] = "黄冈",
        [11] = "咸宁",
        [12] = "随州",
        [13] = "恩施",
        [14] = "神农架",
	},
	["湖南"] = {
		[1] = "长沙",
        [2] = "株洲",
        [3] = "湘潭",
        [4] = "衡阳",
        [5] = "邵阳",
        [6] = "岳阳",
        [7] = "常德",
        [8] = "张家界",
        [9] = "益阳",
        [10] = "郴州",
        [11] = "永州",
        [12] = "怀化",
        [13] = "娄底",
        [14] = "湘西",
	},
	["广东"] = {
		[1] = "广州",
        [2] = "韶关",
        [3] = "深圳",
        [4] = "珠海",
        [5] = "汕头",
        [6] = "佛山",
        [7] = "江门",
        [8] = "湛江",
        [9] = "茂名",
        [10] = "肇庆",
        [11] = "惠州",
        [12] = "梅州",
        [13] = "汕尾",
        [14] = "河源",
        [15] = "阳江",
        [16] = "清远",
        [17] = "东莞",
        [18] = "中山",
        [19] = "潮州",
        [20] = "揭阳",
        [21] = "云浮",
	},
	["广西"] = {
		[1] = "南宁",
        [2] = "柳州",
        [3] = "桂林",
        [4] = "梧州",
        [5] = "北海",
        [6] = "防城港",
        [7] = "钦州",
        [8] = "贵港",
        [9] = "玉林",
        [10] = "百色",
        [11] = "贺州",
        [12] = "河池",
        [13] = "来宾",
        [14] = "崇左",
	},
	["海南"] = {
		[1] = "海口",
        [2] = "三亚",
	},
	["重庆"] = {
		[1] = "重庆",
	},
	["四川"] = {
		[1] = "成都",
        [2] = "自贡",
        [3] = "攀枝花",
        [4] = "泸州",
        [5] = "德阳",
        [6] = "绵阳",
        [7] = "广元",
        [8] = "遂宁",
        [9] = "内江",
        [10] = "乐山",
        [11] = "南充",
        [12] = "眉山",
        [13] = "宜宾",
        [14] = "广安",
        [15] = "达州",
        [16] = "雅安",
        [17] = "巴中",
        [18] = "资阳",
        [19] = "阿坝",
        [20] = "甘孜",
        [21] = "凉山",
	},
	["贵州"] = {
		[1] = "贵阳",
        [2] = "六盘水",
        [3] = "遵义",
        [4] = "安顺",
        [5] = "铜仁",
        [6] = "黔西南",
        [7] = "毕节",
        [8] = "黔东南",
        [9] = "黔南",
	},
	["云南"] = {
		[1] = "昆明",
        [2] = "曲靖",
        [3] = "玉溪",
        [4] = "保山",
        [5] = "昭通",
        [6] = "丽江",
        [7] = "思茅",
        [8] = "临沧",
        [9] = "楚雄",
        [10] = "红河",
        [11] = "文山",
        [12] = "西双版纳",
        [13] = "大理",
        [14] = "德宏",
        [15] = "怒江",
        [16] = "迪庆",
	},
	["西藏"] = {
		[1] = "拉萨",
        [2] = "昌都",
        [3] = "山南",
        [4] = "日喀则",
        [5] = "那曲",
        [6] = "阿里",
        [7] = "林芝",
	},
	["陕西"] = {
		[1] = "西安",
        [2] = "铜川",
        [3] = "宝鸡",
        [4] = "咸阳",
        [5] = "渭南",
        [6] = "延安",
        [7] = "汉中",
        [8] = "榆林",
        [9] = "安康",
        [10] = "商洛",
	},
	["甘肃"] = {
		[1] = "兰州",
        [2] = "嘉峪关",
        [3] = "金昌",
        [4] = "白银",
        [5] = "天水",
        [6] = "武威",
        [7] = "张掖",
        [8] = "平凉",
        [9] = "酒泉",
        [10] = "庆阳",
        [11] = "定西",
        [12] = "陇南",
        [13] = "临夏",
        [14] = "甘南",
	},
	["青海"] = {
		[1] = "西宁",
        [2] = "海东",
        [3] = "海北",
        [4] = "黄南",
        [5] = "海南",
        [6] = "果洛",
        [7] = "玉树",
        [8] = "海西",
	},
	["宁夏"] = {
		[1] = "银川",
        [2] = "石嘴山",
        [3] = "吴忠",
        [4] = "固原",
        [5] = "中卫",
	},
	["新疆"] = {
		[1] = "乌鲁木齐",
        [2] = "克拉玛依",
        [3] = "吐鲁番",
        [4] = "哈密",
        [5] = "昌吉",
        [6] = "博尔塔拉",
        [7] = "巴音郭楞",
        [8] = "阿克苏",
        [9] = "克孜勒苏",
        [10] = "喀什",
        [11] = "和田",
        [12] = "伊犁",
        [13] = "塔城",
        [14] = "阿勒泰",
        [15] = "石河子",
        [16] = "阿拉尔",
        [17] = "图木舒克",
        [18] = "五家渠",
	},
	["香港"] = {
		[1] = "香港",
	},
	["澳门"] = {
		[1] = "澳门",
	},
	["台湾"] = {
		[1] = "台湾",
	},
}

local province_list = {
	[1] = "北京",
    [2] = "天津",
    [3] = "河北",
    [4] = "山西",
    [5] = "内蒙古",
    [6] = "辽宁",
    [7] = "吉林",
    [8] = "黑龙江",
    [9] = "上海",
    [10] = "江苏",
    [11] = "浙江",
    [12] = "安徽",
    [13] = "福建",
    [14] = "江西",
    [15] = "山东",
    [16] = "河南",
    [17] = "湖北",
    [18] = "湖南",
    [19] = "广东",
    [20] = "广西",
    [21] = "海南",
    [22] = "重庆",
    [23] = "四川",
    [24] = "贵州",
    [25] = "云南",
    [26] = "西藏",
    [27] = "陕西",
    [28] = "甘肃",
    [29] = "青海",
    [30] = "宁夏",
    [31] = "新疆",
    [32] = "香港",
    [33] = "澳门",
    [34] = "台湾",
}

local row_desc_info = {
	item_width = 90,
	item_height = 40,
	column_cnt = 4,
	x_space = 20,
	y_space = 30,
}

local function click_city_btn( btn, x, y )
	for _, btn2 in pairs( cityPage._city_btns ) do
		btn2:setStatus( LIGHT_UI.NORMAL_STATUS )
	end
	btn:setStatus( LIGHT_UI.DOWN_STATUS )
	cityPage._city_btn:setString( btn.text )
end

local function click_province_btn( btn, x, y )
	for _, btn2 in pairs( cityPage._province_btns ) do
		btn2:setStatus( LIGHT_UI.NORMAL_STATUS )
	end
	btn:setStatus( LIGHT_UI.DOWN_STATUS )
	cityPage._city_btn:setString( city_list[btn.text][1] )
	cityPage._province_btn:setString( btn.text )
end

local function create_item( text, cb, selected )
	
	local config = {
		sx = row_desc_info.item_width,
		sy = row_desc_info.item_height,
		css = 'checkbox',
		text = '',
		text_css = 'b2',
	}

	local btn = create9Button( nil, config, cb )
	btn:setString( text )
	btn.text = text
	if selected then btn:setStatus( LIGHT_UI.DOWN_STATUS ) end
	
	return btn
end

local function show_province_panel( parent )
	if city_panel then city_panel:setVisible( false ) end

	if province_panel == nil then
		province_panel = LIGHT_UI.clsNode:New( parent, 0, 0 )
		local row_page = LIGHT_UI.clsRowLayout:New( province_panel, 30, 620, row_desc_info )

		parent._province_btns = {}
		for i = 1, table.getn( province_list ) do
			local item = create_item( province_list[i], click_province_btn, i == 1 )
			row_page:append_item( item )
			table.insert( parent._province_btns, item )
		end

		row_page:refresh_view()
	end
	
	parent._bg2:setVisible( true )
	province_panel:setVisible( true )
end

local function show_city_panel( parent, province )
	if province_panel then province_panel:setVisible( false ) end

	if city_panel == nil then
		city_panel = LIGHT_UI.clsNode:New( parent, 0, 0 )
		city_panel_row_page = LIGHT_UI.clsRowLayout:New( city_panel, 30, 620, row_desc_info )
	end

	city_panel_row_page:clearAllItem()
	parent._city_btns = {}
	for i = 1, table.getn( city_list[province] ) do
		local item = create_item( city_list[province][i], click_city_btn, i == 1 )
		city_panel_row_page:append_item( item )
		table.insert( parent._city_btns, item )
	end

	city_panel_row_page:refresh_view()
	parent._bg2:setVisible( true )
	city_panel:setVisible( true )
end

local function createCityPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = CityConfig, nil
	local o = nil
	local USER_INFO_PANEL = Import("lua/user_info.lua")

	local function on_back(btn)
		closeCityPagePanel()
		local o = Import( "lua/view/my_detail_view.lua" )
		local obj = o.myDetailView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, nil, GLOBAL.uid )
		obj:secondInit()
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '选择城市' )
	
	local function onOk(obj, x, y)
		local tip
		local txt = string.format("%s,%s", ret_panel._province_btn._str_obj:getString(), ret_panel._city_btn._str_obj:getString())
		
		local function on_select_cb(data)
			clear{ tip }
			tip = nil
			if data.code > 0 then
				ok_func( txt )
				closeCityPagePanel()
				local o = Import( "lua/view/my_detail_view.lua" )
				local obj = o.myDetailView:getInstance( HTTP_CLIENT.getRootNode(), 0, 0, nil, GLOBAL.uid )
				obj:secondInit()
				
			else
				showMessage( cityPage, "操作失败，请联系客服!", { ms = 3000 } )
			end
		end
		
		doCommand( 'question', 'answer', { id = 7, value = txt }, on_select_cb, nil, {loading = 0})
		tip = showMessageX( cityPage, "信息已提交，请稍候" )
	end

	config = CityConfig["reply_btn_config"]
	ret_panel._ok_btn = LIGHT_UI.clsButton:New(ret_panel, config["x"], config["y"], config["normal_res"], config["click_res"])
	ret_panel._ok_btn.onTouchEnd = onOk

	config = CityConfig["select_config"]
	local startx = config.x
	local starty = config.y
	local x_step = 166
	local config = {
		x = startx,
		y = starty,
		sx = 120,
		sy = 50,
		css = 'checkbox',
	}
	local province_btn = create9Button( ret_panel, config, nil )

	local function on_province_click(btn, x, y)
		show_province_panel( ret_panel )
	end
	ret_panel._province_btn = province_btn
	province_btn:setString("北京")
	province_btn:setTextColor(102, 102, 102)
	province_btn.onTouchEnd = on_province_click

	local size = province_btn:getContentSize()
	local province_txt = LIGHT_UI.clsLabel:New(ret_panel, startx + size.width - 30, starty, "省", GLOBAL_FONT, 20) 
	province_txt:setTextColor(102, 102, 102)

	startx = startx + x_step
	local config = {
		x = startx,
		y = starty,
		sx = 120,
		sy = 50,
		css = 'checkbox',
	}
	local city_btn = create9Button( ret_panel, config, nil )

	local function on_city_click(btn, x, y)
		show_city_panel( ret_panel, province_btn._str_obj:getString() )
	end
	ret_panel._city_btn = city_btn
	city_btn:setString("北京")
	city_btn:setTextColor(102, 102, 102)
	city_btn.onTouchEnd = on_city_click
	ret_panel._city_btn = city_btn
	ret_panel._province_btn = province_btn

	size = city_btn:getContentSize()
	local city_txt = LIGHT_UI.clsLabel:New(ret_panel, startx + size.width - 30, starty, "市", GLOBAL_FONT, 20) 
	city_txt:setTextColor(102, 102, 102)


	ret_panel._bg2 = LIGHT_UI.clsSprite:New( ret_panel, 0, 0, "yellow_bg_p.png" )
	ret_panel._bg2:setAnchorPoint( 0, 0 )
	ret_panel._bg2:setContentSize( 480, 630 )
	ret_panel._bg2:setVisible( false )
	
	--add_back(ret_panel ,on_back)

	return ret_panel
end

function showCityPagePanel(parent, x, y)
	city_panel = nil
	province_panel = nil
	if not cityPage then
		cityPage = createCityPagePanel(parent, x, y)
	else
		cityPage:setVisible(true)
	end
end

function init( data )
	ok_func = data.ok_func
end

function closeCityPagePanel()
	--cityPage:setVisible(false)
	if cityPage then
		--delete_back(cityPage)
		clear({cityPage})
		cityPage = nil
	end		
end
