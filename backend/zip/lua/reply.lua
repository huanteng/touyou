-- reply page

REPLY_LAYOUT = Import("layout/reply.lua")
local UPLOAD_PAGE = Import("lua/reply_upload_pic.lua")
local ReplyConfig = REPLY_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local replyPage = nil
local screen_size = CCDirector:sharedDirector():getWinSize()
local total_width = screen_size.width
local MULTI_EDIT = Import("lua/light_multiline_edit.lua")
local MULTI_RICHEDIT = Import("lua/light_multiline_richedit.lua")

local gchat_id = nil
local greply_id = nil
local pic_path = nil
local submiting = false

local EditInfo = {
	Color = {
		[1] = 51,
		[2] = 51,
		[3] = 51,
	},
	Font = GLOBAL_FONT,
	Size = 22,
	MaxCharCount = 80,
	MaxTextWidth = 400,
	MinMsgHeight = 80,
	LineInterval = 0,
}

local DETAIL_PAGE = Import("lua/chat_detail.lua") 

local function createReplyPagePanel(parent, x, y, page_info, reply_info) -- 转换成世界坐标一定要结点的父结点调用	
	local ret_panel = createBasePanel( parent, x, y )

	PAGE[ 'xyz' ] = {
			x = 0,
			y = 0,
			sx = 480,
			sy = 720,
			ax = 0,
			ay = 0,
		}

	local c, c2 = ReplyConfig, nil
	local o = nil

	local function on_back(btn)
		replyPage._on_cancel()

		closeReplyPagePanel()
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '回复' )
	
	local desc_c = {
			id = 'desc_label',
			to = 'xyz',
			tx = 0,
			ty = 1,
			x = ReplyConfig["reply_content"]["x"],
			y = -ReplyConfig["reply_content"]["y"],
			ax = 0,
			ay = 1,
			sx = 480,
			sy = 1,
			text = ' ',
			text_css = 'c2',
		}
	
	ret_panel._desc_txt = createMultiLabel( ret_panel, desc_c )
		
	local function onClickReply(obj, x, y)		
		local function filterChar( content )
			local i = 1
			local str = " "
			while(i <= string.len(content)) do
				local b = string.sub(content,1,i)		
				if b ~= str then
					return true
				end
				str = str .. " "
				i = i + 1
			end
			return false
		end		
		--local sayContent = filterChar( "Good morning" )		
			
		local sayContent = filterChar( ret_panel._user_input_label:getText() )	
		if sayContent == true then				
			dailyCount('SayWordsReport')--发表说两句		
			replyPage._on_ok( encode({ret_panel._user_input_label:getText()}) )			
			--replyPage._on_ok( base64Encode("Good morning") )
		else
			showMessage( replyPage, "输入不能为空...", {ms = '3000'})
		end					
	end

	local y_offset = 10

	if reply_info then	
		ret_panel._desc_txt:setString( reply_info["name"] .. " 说 ：" .. reply_info["content"] )
		local size = ret_panel._desc_txt:getContentSize()
		y_offset = y_offset + size.height
	end

	local function fot_show_bug( data )
		local dataCount = string.len(data);	
		if dataCount > 0 then
			if dataCount < 80 then
			local str = getCharString(data,80)
			ret_panel._user_input_label:setString( str )
			else
			local str = getCharString(data,78) .. ".."
			ret_panel._user_input_label:setString( str )
			end
		end
		
		
	end

	local function after_submit_input( data )
		f_delay_do( ret_panel, fot_show_bug, data, 1 )
	end

	local function show_multi_text_input()
		xymodule.mulitiInput( "", after_submit_input )	
		--after_submit_input('现在的人怎么都玩游戏不发说说了，叫我八卦什么好呢？')
		--after_submit_input('y H')		
	end

	local multi_text_btn_c = {
			id = 'rect',
			to = 'desc_label',
			tx = 0,
			ty = 0,
			x = ( total_width - ReplyConfig["edit_config"]["width"] ) / 2 - ReplyConfig["reply_content"]["x"],
			y = - ReplyConfig["reply_content"]["margin"],
			sx = ReplyConfig["edit_config"]["width"],
			sy = ReplyConfig["edit_config"]["height"],
			ax = 0,
			ay = 1,
			css = 'ic_single',
		}
	create9Button( ret_panel, multi_text_btn_c, show_multi_text_input )

	local user_input_label_c = {
			id = 'user_input_label',
			to = 'desc_label',
			tx = 0,
			ty = 0,
			x = ( total_width - ReplyConfig["edit_config"]["width"] ) / 2 - ReplyConfig["reply_content"]["x"] + 10,
			y = - ReplyConfig["reply_content"]["margin"] - 5,
			sx = ReplyConfig["edit_config"]["width"] - 10,
			sy = ReplyConfig["edit_config"]["height"] - 10,
			ax = 0,
			ay = 1,
			text = '',
			css = 'c3',
		}
	
	ret_panel._user_input_label = createMultiLabel( ret_panel, user_input_label_c )

	config = ReplyConfig["reply_btn_config"]
	ret_panel._reply_btn = LIGHT_UI.clsButton:New(ret_panel, config["x"], config["y"], config["normal_res"], config["click_res"])
	ret_panel._reply_btn.onTouchEnd = onClickReply

	config = ReplyConfig["edit_config"]

	local tips_c = {
			id = 'tips_label',
			to = 'rect',
			tx = 0,
			ty = 0,
			x = 0,
			y = -35,
			ax = 0,
			ay = 1,
			text = '你还可以输入80个字',
			text_css = 'c2',
		}

	ret_panel._input_tips = createLabel(ret_panel, tips_c )
	ret_panel._input_tips:setTextColor(102, 102, 102)
	
	
	local function onTakePhoto(btn, x, y)
		UPLOAD_PAGE.showUploadPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, page_info, reply_info )
	end
		
	local photo_c = {
			id = 'take_pic_btn',
			to = 'rect',
			tx = 1,
			ty = 0,
			x = -45,
			y = -55,
			sx = 88,
			sy = 88,
			text = '',
			text_css = 'c2',			
			res = "takephoto_bt_bg.png",
		}

	ret_panel._photo_button = createButton( ret_panel, photo_c, onTakePhoto )
	ret_panel._photo_pic = LIGHT_UI.clsSprite:New(ret_panel, 100, 100, nil)

	return ret_panel
end

local function setByPageInfo(page_info)
	replyPage._head_text:setString(page_info.title)
	replyPage._desc_txt:setString(page_info.desc)
	if not page_info.notips then replyPage._input_tips:setString( "你还可以输入80个字" ) end
	if page_info.notips then
		replyPage._input_tips:setVisible( false )
		replyPage._photo_button:setVisible( false )
	elseif page_info.nophoto then
		replyPage._photo_button:setVisible( false )
	else
		replyPage._input_tips:setVisible( true )
		replyPage._photo_button:setVisible( true )
	end
end

local function hideTips()
    if uploadTips ~= nil then
       clear({uploadTips})
       uploadTips = nil
    end
end

local function showTips(txt,delay_sec)
    hideTips()
    if uploadTips == nil then
        local d_time = 0
        if delay_sec == 0 then
            d_time = 1000*3600
        else 
            d_time = 1000*delay_sec
        end
        local option = {ms=d_time}
        uploadTips = showMessage(replyPage,txt,option)
    end
end

function showReplyPagePanel(parent, x, y, on_cancel, page_info, reply_info)
	if page_info then
		if page_info.id then
			gchat_id = page_info.id
		else
			gchat_id = 0
		end
	end
	if not replyPage then	
		replyPage = createReplyPagePanel(parent, x, y, page_info, reply_info)
	else
		replyPage:setVisible(true)
	end

	local function on_reply_cb(data)
		submiting = false
		pic_path = nil
		local CHAT_PANEL = Import("lua/chat.lua")		
		if PANEL_CONTAINER.closeChildPanel( nil, 2 ) then
			PANEL_CONTAINER.addChild( CHAT_PANEL.showChatPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
		end	
		
		setCacheExpires( 'comment.lists.2.1' )
		setCacheExpires( 'comment.lists.3.1' )
		closeReplyPagePanel()			
	end
		
	local function on_ok2(content)		
		if string.len( content ) > 0 then
			local url = GLOBAL.interface .. "comment.php?sid=" .. GLOBAL.sid
						
			if tonumber( gchat_id ) > 0 then
				local post_data = { id = gchat_id, content = content, kind = 1 }
				if pic_path then post_data.file = pic_path end

				if post_data.file then
					post_data.type = 'answer'
					post_data.sid = GLOBAL.sid
					
					if submiting == false then
						submiting = true
						HTTP_REQUEST.http_upload( url, post_data, on_reply_cb )
					else
						showTips("请稍候，图片上传中..",0)
					end	
				else
					if submiting == false then
						submiting = true
						doCommand( 'comment', 'answer', post_data, on_reply_cb )
					else
						showTips("发布中，请稍后..",0)
					end
				end
			else
				local post_data = { content = content }
				if pic_path then post_data.file = pic_path end

				if post_data.file then
					post_data.type = 'say'
					post_data.sid = GLOBAL.sid	
					if submiting == false then
						submiting = true				
						HTTP_REQUEST.http_upload( url, post_data, on_reply_cb )
					else
						showTips("请稍候，图片上传中..",0)
					end
				else
					if submiting == false then
						submiting = true
						doCommand( 'comment', 'say', post_data, on_reply_cb )
					else
						showTips("发布中，请稍后..",0)
					end
				end
			end
		else
			replyPage._input_tips:setString( "请输入内容" )
		end
	end
	
	if on_cancel then
		replyPage._on_cancel = on_cancel
	end
	replyPage._on_ok = on_ok2
	
	setByPageInfo(page_info)
end

function closeReplyPagePanel()
	--replyPage:setVisible(false)
	clear({replyPage})
	replyPage = nil
end

local function initData()
	submiting = false		--防止发布过程中退出出问题
end

function showReplyPagePanel2(parent, x, y, on_ok, on_cancel, page_info, reply_info)
	initData()
	replyPage = createReplyPagePanel(parent, x, y, reply_info)
	if on_cancel then
		replyPage._on_cancel = on_cancel
	end
	replyPage._on_ok = on_ok
	
	setByPageInfo(page_info)	
end

function set_pic_path( path )
	pic_path = path
	local function cb()
		local size = replyPage._photo_pic:getContentSize()
		local m_width = 480*0.8
		local m_height = 800*0.5
		local r_w = size.width
		local r_h = size.height
		local scale = 1
		if size.width > m_width or size.height > m_height then		 	
			if size.width/m_width > size.height/m_height then
				scale = m_width/size.width
				r_w = m_width
				r_h = size.height * scale
			else
				scale = m_height/size.height
				r_w = size.width * scale
				r_h = m_height
			end
		end		
		replyPage._photo_pic:setScale(scale)
		replyPage._photo_pic:setAnchorPoint(0,0)
		local x = (480 - r_w) / 2 - 98
		local y = (680 - r_h)/2 - 190
		replyPage._photo_pic:getSprite():setPosition(x,y)			
	end
	
	replyPage._photo_pic:setImage( pic_path, cb, true )
	--replyPage._photo_pic:setScaleX( 0.3 )
	--replyPage._photo_pic:setScaleY( 0.3 )
end