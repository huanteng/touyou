-- confirm page panel

local confirmPage = nil

CONFIRM_LAYOUT = Import("layout/confirm_dlg.lua")
local ConfirmConfig = CONFIRM_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local function on_confirm_return(data)
	local data_tbl = json.decode(data)
	if data_tbl.code ~= 1 then
		return
	end
end

--[[
kind
type
content
jump
jump_data
time
sender_uid
sender_name
sender_logo
--]]

local function createConfirmPagePanel(parent, x, y, title, hint, ok_cb, cancel_cb, ok_txt, cancel_txt) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = ConfirmConfig["bg_config"]
	ret_panel._bg_pic = LIGHT_UI.clsSprite:New(ret_panel, config.x, config.y, config.res)
	ret_panel._bg_pic:setAnchorPoint(0, 1)
	ret_panel._bg_pic:setScaleX(config.sx)
	ret_panel._bg_pic:setScaleY(config.sy)

	config = ConfirmConfig["top_pic_config"]
	ret_panel._top_pic = LIGHT_UI.clsSprite:New(ret_panel, config.x, config.y, config.res)
	ret_panel._top_pic:setAnchorPoint(0, 1)
	ret_panel._top_pic:setScaleX(config.sx)
	ret_panel._top_pic:setScaleY(config.sy)

	config = ConfirmConfig.top_title_config
	ret_panel._top_title = LIGHT_UI.clsLabel:New(ret_panel, config.x, config.y, title, GLOBAL_FONT, 21)
	ret_panel._top_title:setAnchorPoint(0, 1)

	config = ConfirmConfig.hint_txt_config
	ret_panel._hint_txt = LIGHT_UI.clsLabel:New(ret_panel, config.x, config.y, hint, GLOBAL_FONT, 13)
	ret_panel._hint_txt:setTextColor(0, 0, 0)
	ret_panel._hint_txt:setAnchorPoint(0, 1)

	local function on_ok_click(btn_obj)
		closeConfirmPagePanel()
		ok_cb()
	end

	config = ConfirmConfig["ok_btn_config"]
	ret_panel._ok_btn = LIGHT_UI.clsButton:New(ret_panel, config.x, config.y, config.normal_res, config.click_res)
	ret_panel._ok_btn:setAnchorPoint(0, 0)
	if ok_txt then
		ret_panel._ok_btn:setString(ok_txt)
	else
		ret_panel._ok_btn:setString("确定")
	end
	ret_panel._ok_btn:setTextColor(183,22,4)
	ret_panel._ok_btn:setSpriteScaleX(config.sx)
	ret_panel._ok_btn.onTouchEnd = on_ok_click

	local function on_cancel_click(btn_obj)
		closeConfirmPagePanel()
		cancel_cb()
	end

	config = ConfirmConfig["cancel_btn_config"]
	ret_panel._cancel_btn = LIGHT_UI.clsButton:New(ret_panel, config.x, config.y, config.normal_res, config.click_res)
	ret_panel._cancel_btn:setAnchorPoint(0, 0)
	if cancel_txt then
		ret_panel._cancel_btn:setString(cancel_txt)
	else
		ret_panel._cancel_btn:setString("取消")
	end
	ret_panel._cancel_btn:setTextColor(183,22,4)
	ret_panel._cancel_btn:setSpriteScaleX(config.sx)
	ret_panel._cancel_btn.onTouchEnd = on_cancel_click

	return ret_panel
end

function showConfirmPagePanel(title, hint, ok_cb, cancel_cb, ok_txt, cancel_txt)
	local config = ConfirmConfig["confirm_config"]
	HTTP_CLIENT.showMask()
	if not confirmPage then
		confirmPage = createConfirmPagePanel(HTTP_CLIENT.getMaskNode(), config.x, config.y, title, hint, ok_cb, cancel_cb, ok_txt, cancel_txt)
	else
		confirmPage:setVisible(true)
	end

	confirmPage._top_title:setString(title)
	confirmPage._hint_txt:setString(hint)

	return confirmPage
end

function closeConfirmPagePanel()
	--confirmPage:setVisible(false)
	HTTP_CLIENT.hideMask()
	clear({confirmPage})
	confirmPage = nil
end

