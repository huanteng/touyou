-- user info

local userInfoPage = nil

USER_LAYOUT = Import("layout/user_info.lua")
LOGIN_PANEL = Import("lua/login.lua")
local UserConfig = USER_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
local cur_dynamic_page = 1
local max_dynamic_page = 10
local max_pic = 4
local total_width = 480
local battle_rec_data = nil
local question_id = 0
local question_val = ''
local album_data = nil
--[[
id
url
comment_times
vote_times
--]]

local dynamic_data = nil
--[[
type
user
id

data
"" type = 9
or
score type = 7
user
id
url
name

logo
item
vip_id
sex
name
is_real
time
--]]

function getAlbumData()
	return album_data
end

function removePhoto(id)
	local remove_idx = nil
	for idx, photo_info in pairs(album_data) do
		if id == photo_info["id"] then
			remove_idx = idx
			break
		end
	end

	if remove_idx then
		table.remove(album_data, remove_idx)
	end
end

local back_func = nil
function set_back_func(func)
	back_func = func
end

function init( data )
	if data.back_func then
		back_func = data.back_func
	end
end

local info_desc_info = {
	item_width = 480,
	item_height = 140,
	column_cnt = 1,
	x_space = 20,
	y_space = 40,
}

function setUserPic(logo_url)
	userInfoPage._move_info_page._user_simple_info._user_pic:setImage(logo_url)
end

local CHALLENGE_PAGE = Import("lua/challenge.lua")
local SELECT_PAGE = Import("lua/select.lua")
local REPLY_PAGE = Import("lua/reply.lua")
local CHOOSE_CITY = Import("lua/choose_city.lua")
local CHOOSE_DAY = Import("lua/choose_day.lua")

local function on_select_cb(data)
	showMessage( userInfoPage, data.memo )
end

local delay_cb = nil

local function video_cb(real_path)
end

local userValue = {}		--临时存储的个人资料

local function updateUserValue()
	if userValue.sex then 		-- 性别
		if userValue.sex == "男" then
			userInfoPage._move_info_page._user_simple_info._sex_value:setImage( "man.png" )
		elseif userValue.sex == "女" then
			userInfoPage._move_info_page._user_simple_info._sex_value:setImage( "woman.png" )
		end
	end
	if userValue.hobby then 		-- 兴趣
		userInfoPage._move_info_page._user_simple_info._hobby_value:setString( userValue.hobby )
		LOGIN_PANEL.setHobby( userValue.hobby )
		userValue.hobby = nil
	end
	if userValue.income then	--收入
		local incomeTxt = getCharString( userValue.income, 10 )
		userInfoPage._move_info_page._user_simple_info._income_value:setString( incomeTxt )
		LOGIN_PANEL.setIncome( incomeTxt )
		userValue.income = nil
	end
	if userValue.height then	--身高
		local heightTxt = getCharString( userValue.height, 10 )
		userInfoPage._move_info_page._user_simple_info._height_value:setString( heightTxt )
		LOGIN_PANEL.setHeight( userValue.height )
		userValue.height = nil
	end		
	if userValue.birthday then 		-- 生日
		userInfoPage._move_info_page._user_simple_info._birthday_value:setString( userValue.birthday )
		LOGIN_PANEL.setBirthday( userValue.birthday )
		userValue.birthday = nil
	end
	if userValue.city then		--城市
		userInfoPage._move_info_page._user_simple_info._city_value:setString( userValue.city )
		LOGIN_PANEL.setCity( userValue.city )
		userValue.city = nil
	end
	if userValue.career then		--职业
		local careerTxt = getCharString( userValue.career, 10 )
		userInfoPage._move_info_page._user_simple_info._profession_value:setString( careerTxt )
		LOGIN_PANEL.setProfession( careerTxt )
		userValue.career = nil
	end
	if userValue.bar then		--夜店主场
		local barTxt = getCharString( userValue.bar, 10 )
		userInfoPage._move_info_page._user_simple_info._club_value:setString( barTxt )
		LOGIN_PANEL.setBar( barTxt )
		userValue.bar = nil
	end
end

local function onQuestionAnswer( data )
	battle_rec_data = nil
	--showUserInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	userInfoPage:setVisible( true )
	showMessage( userInfoPage, data.memo )

	updateUserValue()
end	

local function userInfoModifyHandler( data, value, uValue )
	if not uValue then
		if value ~= "back" then		--设定一下返回的时候就传个back进来
			question_val = value
			uValue = value
			doCommand( 'question', 'answer', { id = data.id, value = value }, onQuestionAnswer )
		else
			
			return nil
		end								
	else
		if value == "back" then		--设定一下返回的时候就传个back进来
			question_val = uValue			
		else
			question_val = value
			uValue = value
			doCommand( 'question', 'answer', { id = data.id, value = value }, onQuestionAnswer )
		end
	end	
	return uValue
end

local function onQuestionDetail( data )
	local function onOk( value )
		if question_id == 1 then		--性别
			userValue.sex = userInfoModifyHandler( data, value, userValue.sex )
		elseif question_id == 2 then 		-- 兴趣
			userValue.hobby = userInfoModifyHandler( data, value, userValue.hobby )
		elseif question_id == 3 then	--收入
			userValue.income = userInfoModifyHandler( data, value, userValue.income )
		elseif question_id == 4 then	--职业
			userValue.career = userInfoModifyHandler( data, value, userValue.career )
		elseif question_id == 5 then	--夜店主场
			userValue.bar = userInfoModifyHandler( data, value, userValue.bar )
		elseif question_id == 6 then	--身高
			userValue.height = userInfoModifyHandler( data, value, userValue.height )		
		end		
		updateUserValue()
	end
	
	if data.code < 0 then
		showMessage( userInfoPage, data.memo )
		return
	end
	
	data = data.data
	
	-- type：类型，1：单选，2：复选， 3：问答
	data.type = tonumber( data.type )
	local type = data.type
	
	local panel = nil
	if type == 1 then
		panel = Import("lua/question_select.lua")
	elseif type == 2 then
		panel = Import("lua/question_select.lua")
	elseif type == 3 then
		panel = Import("lua/question_input.lua")
	else
		print( '不正确的问题类型，id=' .. tostring(data.id) .. '，type=' .. tostring(data.type) )
		return
	end
	
	closeUserInfoPagePanel()
	if data.id == '2' then data.max_select = 3 end
	if data.type == 1 then data.max_select = 1 end
	panel.init( { data = data, ok_func = onOk } )
	panel.showQuestionPagePanel( HTTP_CLIENT.getRootNode(), 0, 0 )
end

--[[
修改问题
]]
local function onEditQuestion( id )
	question_id = id
	doCommand( 'question', 'detail', { id = id }, onQuestionDetail )
end

local function delay_take()
	xymodule.take_video(video_cb)
	CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(delay_cb)
	delay_cb = nil
end

local function userInfoModifyHandler2( value, uValue )
	if not uValue then
		if value ~= "back" then		--设定一下返回的时候就传个back进来
			question_val = value
			uValue = value			
		else			
			return nil
		end								
	else
		if value == "back" then		--设定一下返回的时候就传个back进来
			question_val = uValue
			
		else
			question_val = value
			uValue = value			
		end
	end	
	
	return uValue
end

local function confirmHandler( value )
	if question_id == 8 then 		-- 生日
		userValue.birthday = userInfoModifyHandler2( value, userValue.birthday )	
	elseif	question_id == 7 then		--城市
		userValue.city = userInfoModifyHandler2( value, userValue.city )
	end				
	updateUserValue()
end

local function createUserSimpleInfo(parent, x, y, move_grp)
	local c, c2 = UserConfig, nil
	local o = nil
	
	local user_info = LOGIN_PANEL.getUserData()
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	ret_panel._top_bg_pic = LIGHT_UI.clsSprite:New(ret_panel, -20, 15, "tiny_black_pixel.png")
	ret_panel._top_bg_pic:setAnchorPoint(0, 1)
	ret_panel._top_bg_pic:setScaleX(480)
	ret_panel._top_bg_pic:setScaleY(480)

	ret_panel._bg_pic = LIGHT_UI.clsSprite:New(ret_panel, -20, -140, "tiny_bg_pixel.png")
	ret_panel._bg_pic:setAnchorPoint(0, 1)
	ret_panel._bg_pic:setScaleX(480)
	ret_panel._bg_pic:setScaleY(1100)
	
	

	--create9Sprite( ret_panel, c.logo_rect )

	ret_panel._user_pic = createSprite( ret_panel, { x = 50, y = -70, res = user_info["logo"], sx = 120, sy = 120 } )--头像位置调整
	
	
	--用户心情显示
	local function for_show_bug( data )
		local dataCount =string.len(data)
		if dataCount > 0 then
			if dataCount < 46 then
			local str = getCharString(data,46)
			ret_panel._remark_txt:setString( str )
			doCommand( 'question', 'answer', { id = 10, value = str }, on_select_cb )
			else
			local str = getCharString(data,46) .. ".."
			ret_panel._remark_txt:setString( str )
			doCommand( 'question', 'answer', { id = 10, value = str }, on_select_cb )
			end
		end
		
		
	end

	local function after_submit_input( data )
		f_delay_do( ret_panel, for_show_bug, data, 1 )
	end		
	
	local function modifyMood( t, x, y )
		xymodule.mulitiInput( "", after_submit_input )		
	end
	ret_panel.click_rect =createButton( ret_panel, UserConfig.mood_click_rect, modifyMood )
	--ret_panel.click_rect:set_msg_rect( 0, 0, 325, 510 )
	--ret_panel.click_rect.onTouchEnd = modifyMood
	--控制显示用户心情显示字数
	local str = getCharString(user_info["mood"],200);
	local count = (#str);	
	if count > 165 then
	str = getCharString(user_info["mood"],46) .. "..."
	end	
	ret_panel._remark_txt = LIGHT_UI.clsMultiLabel:New(ret_panel, 125, -40, 
	str, GLOBAL_FONT, 20, 320, 1)
--	ret_panel._remark_txt = LIGHT_UI.clsMultiLabel:New(ret_panel, 125, -40, user_info["mood"], GLOBAL_FONT, 20, 320, 1)
	
	ret_panel._remark_txt:setAnchorPoint(0, 1)	
		

	if user_info["vip_id"] ~= "0" then
		local vip_logo = string.format( 'vip%i.png', user_info["vip_id"] )
		ret_panel._vip_pic = LIGHT_UI.clsSprite:New(ret_panel, UserConfig.video_vip_pos["real_x"], UserConfig.video_vip_pos["real_y"], vip_logo)--VIP标志调整				
	end
	
	if tonumber(user_info["is_real"]) == 1 then	
		local posX = UserConfig.video_vip_pos["real_x"]
		if user_info["vip_id"] ~= "0" then
			posX = UserConfig.video_vip_pos["real_x"] + UserConfig.video_vip_pos["space"]			
		end

		local list = { x = posX,y = UserConfig.video_vip_pos["real_y2"],res = "vedioauth_pic.png",ax = UserConfig.video_vip_pos["ax"] }
		--ret_panel.video_pic = LIGHT_UI.clsSprite:New(ret_panel, posX, UserConfig.video_vip_pos["real_y2"], "vedioauth_pic.png")	
		ret_panel.video_pic = createSprite(ret_panel,list)
	end			

	ret_panel._photo_txt = LIGHT_UI.clsLabel:New(ret_panel, -2, -151, "相册", GLOBAL_FONT, 16)
	ret_panel._photo_txt:setAnchorPoint(0, 1)
	ret_panel._photo_txt:setTextColor(153, 153, 153)
	
	
	if tonumber(user_info["is_real"]) == 0 then
		ret_panel._photo_hint = LIGHT_UI.clsLabel:New(ret_panel, 110, -151, "照片仍未通过真实性认证", GLOBAL_FONT, 16)
		ret_panel._photo_hint:setAnchorPoint(0, 1)
		ret_panel._photo_hint:setTextColor(153, 153, 153)
	end

	local function on_video_click(obj, x, y)
		closeUserInfoPagePanel()
		local VIDEO_AUTH_PAGE = Import("lua/video_auth.lua")
		VIDEO_AUTH_PAGE.showVideoAuthPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
		
	if tonumber(user_info["is_real"]) == 0 then
		ret_panel._photo_validate_btn = create9Button( ret_panel, c.vadio_btn, on_video_click )
	end
	

	ret_panel._album_pic_list = {}   --相册缩略图修改位置
	ret_panel.nodelist = {}
	local startx = 45
	local starty = -236
	local x_step = 115
	local small_logo_rect = deepcopy(c.small_logo_rect)
	--local small_logo_rect = c.small_logo_rect
	
	--记录着仅为了删除
	local small_album_list = {}	
	
	
		
	for i = 1, max_pic do		
		local node = {}
		o = createButton( ret_panel, small_logo_rect )
		table.insert( node, o )
		table.insert(ret_panel.nodelist, o)
		
		small_logo_rect.x = small_logo_rect.x + x_step
		
		--print("small pic x :" .. small_logo_rect.x)
		
		local pic_obj = createSprite( ret_panel, { x = startx, y = starty, sx = 100, sy = 100 } )
		table.insert(ret_panel._album_pic_list, pic_obj)
		startx = startx + x_step
		--pic_obj._msg_layer = LIGHT_UI.clsClipLayer:New(pic_obj, 0, -60)
		--pic_obj._msg_layer:set_msg_rect(0, 0, 140, 115)
		
		table.insert( node, pic_obj )
		small_album_list[ tonumber( i ) ] = node
	end
	
	ret_panel.small_album_list = small_album_list

	ret_panel._base_info_title = LIGHT_UI.clsLabel:New(ret_panel, -2, -302, "基本信息", GLOBAL_FONT, 16)
	ret_panel._base_info_title:setAnchorPoint(0, 1)
	ret_panel._base_info_title:setTextColor(153, 153, 153)

	startx = 20
	starty = -333
	local y_step = -60
	local x_gap = 400 

	ret_panel._base_info_bg = LIGHT_UI.clsSprite:New(ret_panel, startx -25, starty + 5, "grxxlb.png")
	ret_panel._base_info_bg:setAnchorPoint(0, 1)

	starty = starty - 20

	local function on_sel_sex(txt)
		doCommand( 'question', 'answer', { id = 1, value = txt }, on_select_cb )
	end
	local function on_sex_click(msg, x, y)
		--SELECT_PAGE.showSelectPagePanel("请选择性别", {[1]="男",[2]="女",}, on_sel_sex)
		--[[closeUserInfoPagePanel()
		QUESTION_SELECT.init( { id = 1 } )
		QUESTION_SELECT.showHobbyPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)--]]
		onEditQuestion( 1 )
	end
	--ret_panel._sex_msg = LIGHT_UI.clsFrameButton:New(ret_panel, startx - 24, starty + 12, 447, 50, "tiny_white_pixel.png", "tiny_yellow_pixel.png")
	--ret_panel._sex_msg:hideLine()
	--CHALLENGE_PAGE.setMsgDelegate(ret_panel._sex_msg, move_grp, on_sex_click)

	ret_panel._sex_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty + 7, "性别", GLOBAL_FONT, 22)
	ret_panel._sex_title:setAnchorPoint(0, 1)
	ret_panel._sex_title:setTextColor(153, 153, 153)
	
	if user_info.sex == "0" then
		ret_panel._sex_value = LIGHT_UI.clsSprite:New(ret_panel, startx + x_gap, starty + 7, "man.png")
	else
		ret_panel._sex_value = LIGHT_UI.clsSprite:New(ret_panel, startx + x_gap, starty + 7, "woman.png")
	end
	ret_panel._sex_value:setAnchorPoint(1, 1)
	starty = starty + y_step

	local function on_name_ok(txt)
		showUserInfoPagePanel()
		doCommand( 'question', 'answer', { id = 9, value = txt }, on_select_cb )
	end
	local function on_name_cancel()
		showUserInfoPagePanel()
	end
	local function on_name_click(msg, x, y)
--		closeUserInfoPagePanel()
--[[		local page_info = {
			title = "请输入用户名",
			desc = "请输入用户名",
			notips = true,
		}
		REPLY_PAGE.showReplyPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, on_name_ok, on_name_cancel, page_info)
--]]
--		QUESTION_INPUT.init( { id = 9 } )
		onEditQuestion( 9 )
	end
	--ret_panel._name_msg = LIGHT_UI.clsFrameButton:New(ret_panel, startx - 10, starty + 3, 425, 26, "tiny_bg_pixel.png", "tiny_yellow_pixel.png")
	--ret_panel._name_msg:hideLine()
	--CHALLENGE_PAGE.setMsgDelegate(ret_panel._name_msg, move_grp, on_name_click)
	ret_panel._name_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "名字", GLOBAL_FONT, 22)
	ret_panel._name_title:setAnchorPoint(0, 1)
	ret_panel._name_title:setTextColor(153, 153, 153)
	ret_panel._name_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, user_info["name"], GLOBAL_FONT, 22)
	ret_panel._name_value:setAnchorPoint(1, 1)
	ret_panel._name_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	ret_panel._charm_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "魅力", GLOBAL_FONT, 22)
	ret_panel._charm_title:setAnchorPoint(0, 1)
	ret_panel._charm_title:setTextColor(153, 153, 153)
	ret_panel._charm_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, user_info["charm"], GLOBAL_FONT, 22)
	ret_panel._charm_value:setAnchorPoint(1, 1)
	ret_panel._charm_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	local function on_city_click(msg, x, y)
		question_id = 7
		closeUserInfoPagePanel()
		CHOOSE_CITY.showCityPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		CHOOSE_CITY.init( { ok_func = confirmHandler } )
	end
	ret_panel._city_msg = LIGHT_UI.clsFrameButton:New(ret_panel, startx - 24, starty + 15, 447, 62, "tiny_white_pixel.png", "tiny_yellow_pixel.png")
	ret_panel._city_msg:hideLine()
	CHALLENGE_PAGE.setMsgDelegate(ret_panel._city_msg, move_grp, on_city_click)
	ret_panel._city_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "城市", GLOBAL_FONT, 22)
	ret_panel._city_title:setAnchorPoint(0, 1)
	ret_panel._city_title:setTextColor(153, 153, 153)
	ret_panel._city_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, user_info["province"] .. "," .. user_info["city"], GLOBAL_FONT, 22)
	ret_panel._city_value:setAnchorPoint(1, 1)
	ret_panel._city_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	local function on_height_ok(txt)
		showUserInfoPagePanel()
		doCommand( 'question', 'answer', { id = 6, value = txt }, on_select_cb )
	end
	local function on_height_cancel()
		showUserInfoPagePanel()
	end
	local function on_height_click(msg, x, y)
		--[[closeUserInfoPagePanel()
		local page_info = {
			title = "身高",
			desc = "你身高是多少",
			notips = true,
		}
		REPLY_PAGE.showReplyPagePanel2(HTTP_CLIENT.getRootNode(), 0, 0, on_height_ok, on_height_cancel, page_info)--]]
		onEditQuestion( 6 )
	end
	ret_panel._height_msg = LIGHT_UI.clsFrameButton:New(ret_panel, startx - 24, starty + 12, 447, 62, "tiny_white_pixel.png", "tiny_yellow_pixel.png")
	ret_panel._height_msg:hideLine()
	CHALLENGE_PAGE.setMsgDelegate(ret_panel._height_msg, move_grp, on_height_click)

	starty = starty - 10
	ret_panel._height_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "身高(cm)", GLOBAL_FONT, 22)
	ret_panel._height_title:setAnchorPoint(0, 1)
	ret_panel._height_title:setTextColor(153, 153, 153)
	local heightTxt = getCharString( user_info["height"], 10 )
	ret_panel._height_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, heightTxt, GLOBAL_FONT, 22)
	ret_panel._height_value:setAnchorPoint(1, 1)
	ret_panel._height_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	local function on_birth_click(msg, x, y)
		question_id = 8
		closeUserInfoPagePanel()
		CHOOSE_DAY.showDayPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		CHOOSE_DAY.init( { ok_func = confirmHandler } )
	end
	ret_panel._birth_msg = LIGHT_UI.clsFrameButton:New(ret_panel, startx - 24, starty + 19, 447, 62, "tiny_white_pixel.png", "tiny_yellow_pixel.png")
	ret_panel._birth_msg:hideLine()
	CHALLENGE_PAGE.setMsgDelegate(ret_panel._birth_msg, move_grp, on_birth_click)
	ret_panel._birthday_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "生日", GLOBAL_FONT, 22)
	ret_panel._birthday_title:setAnchorPoint(0, 1)
	ret_panel._birthday_title:setTextColor(153, 153, 153)
	ret_panel._birthday_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, user_info["birthday"], GLOBAL_FONT, 22)
	ret_panel._birthday_value:setAnchorPoint(1, 1)
	ret_panel._birthday_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	--[[local function on_income_ok(txt)
		showUserInfoPagePanel()
		doCommand( 'question', 'answer', { id = 3, value = txt }, on_select_cb )
	end
	local function on_income_cancel()
		showUserInfoPagePanel()
	end--]]
	local function on_income_click(msg, x, y)
		--[[closeUserInfoPagePanel()
		local o = QUESTION_INPUT
		o.init( { id = 3 } )
		o.showQuestionPagePanel( HTTP_CLIENT.getRootNode(), 0, 0 )--]]
		onEditQuestion( 3 )
	end
	ret_panel._income_msg = LIGHT_UI.clsFrameButton:New(ret_panel, startx - 24, starty + 16, 447, 62, "tiny_white_pixel.png", "tiny_yellow_pixel.png")
	ret_panel._income_msg:hideLine()
	CHALLENGE_PAGE.setMsgDelegate(ret_panel._income_msg, move_grp, on_income_click)
	ret_panel._income_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "收入(元/月)", GLOBAL_FONT, 22)
	ret_panel._income_title:setAnchorPoint(0, 1)
	ret_panel._income_title:setTextColor(153, 153, 153)
	local incomeTxt = getCharString( user_info["income"], 10 )
	ret_panel._income_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, incomeTxt, GLOBAL_FONT, 22)
	ret_panel._income_value:setAnchorPoint(1, 1)
	ret_panel._income_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	local function on_hobby_click(msg, x, y)
		--[[closeUserInfoPagePanel()
		QUESTION_SELECT.init( { id = 2 } )
		QUESTION_SELECT.showHobbyPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)--]]
		onEditQuestion( 2 )
	end
	ret_panel._hobby_msg = LIGHT_UI.clsFrameButton:New(ret_panel, startx - 24, starty + 13, 447, 62, "tiny_white_pixel.png", "tiny_yellow_pixel.png")
	ret_panel._hobby_msg:hideLine()
	CHALLENGE_PAGE.setMsgDelegate(ret_panel._hobby_msg, move_grp, on_hobby_click)
	ret_panel._hobby_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "兴趣", GLOBAL_FONT, 22)
	ret_panel._hobby_title:setAnchorPoint(0, 1)
	ret_panel._hobby_title:setTextColor(153, 153, 153)
	ret_panel._hobby_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, user_info["interest"], GLOBAL_FONT, 22)
	ret_panel._hobby_value:setAnchorPoint(1, 1)
	ret_panel._hobby_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	--[[local function on_pro_ok(txt)
		showUserInfoPagePanel()
		doCommand( 'question', 'answer', { id = 4, value = txt }, on_select_cb )
	end
	local function on_pro_cancel()
		showUserInfoPagePanel()
	end--]]
	local function on_pro_click(msg, x, y)
		--[[local page_info = {
			title = "请输入职业",
			desc = "你是做什么的?虽然不重要，但人家还是想知道嘛",
			notips = true,
		}
		REPLY_PAGE.showReplyPagePanel2(HTTP_CLIENT.getRootNode(), 0, 0, on_pro_ok, on_pro_cancel, page_info)--]]
		
		onEditQuestion( 4 )
	end
	ret_panel._pro_msg = LIGHT_UI.clsFrameButton:New(ret_panel, startx - 24, starty + 10, 447, 62, "tiny_white_pixel.png", "tiny_yellow_pixel.png")
	ret_panel._pro_msg:hideLine()
	CHALLENGE_PAGE.setMsgDelegate(ret_panel._pro_msg, move_grp, on_pro_click)
	
	starty = starty - 10
	ret_panel._profession_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "职业", GLOBAL_FONT, 22)
	ret_panel._profession_title:setAnchorPoint(0, 1)
	ret_panel._profession_title:setTextColor(153, 153, 153)
	local careerTxt = getCharString( user_info["profession"], 10 )
	ret_panel._profession_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, careerTxt, GLOBAL_FONT, 22)
	ret_panel._profession_value:setAnchorPoint(1, 1)
	ret_panel._profession_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	ret_panel._sina_bind_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "绑定微博", GLOBAL_FONT, 22)
	ret_panel._sina_bind_title:setAnchorPoint(0, 1)
	ret_panel._sina_bind_title:setTextColor(153, 153, 153)
	ret_panel._sina_bind_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, "", GLOBAL_FONT, 22)
	ret_panel._sina_bind_value:setAnchorPoint(1, 1)
	ret_panel._sina_bind_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	--[[local function on_club_ok(txt)
		showUserInfoPagePanel()
		doCommand( 'question', 'answer', { id = 5, value = txt }, on_select_cb )
	end
	local function on_club_cancel()
		showUserInfoPagePanel()
	end--]]
	local function on_club_click(msg, x, y)
		--[[closeUserInfoPagePanel()
		local page_info = {
			title = "夜店主场",
			desc = "约泡吧得找场子，你的主场在哪里?",
			notips = true,
		}
		REPLY_PAGE.showReplyPagePanel2(HTTP_CLIENT.getRootNode(), 0, 0, on_club_ok, on_club_cancel, page_info)--]]
		onEditQuestion( 5 )
	end
	ret_panel._club_msg = LIGHT_UI.clsFrameButton:New(ret_panel, startx - 24, starty + 13, 447, 50, "tiny_white_pixel.png", "tiny_yellow_pixel.png")
	ret_panel._club_msg:hideLine()
	CHALLENGE_PAGE.setMsgDelegate(ret_panel._club_msg, move_grp, on_club_click)
	ret_panel._club_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "夜店主场", GLOBAL_FONT, 22)
	ret_panel._club_title:setAnchorPoint(0, 1)
	ret_panel._club_title:setTextColor(153, 153, 153)
	local barTxt = getCharString( user_info["bar"], 10 )
	ret_panel._club_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, barTxt, GLOBAL_FONT, 22)
	ret_panel._club_value:setAnchorPoint(1, 1)
	ret_panel._club_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	local function getContentSize(panel)
		return { ["width"] = 440, ["height"] = 1040, }
	end

	ret_panel.getContentSize = getContentSize
	return ret_panel
end

local function createBattleRecord(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	createSprite( ret_panel, UserConfig.battle_bg )
	
	
	create9Sprite( ret_panel, UserConfig.today_rect )
	createLabel( ret_panel, UserConfig.today_left )
	ret_panel._today_right_txt = createLabel( ret_panel, UserConfig.today_right_txt )
	--createSprite( ret_panel, UserConfig.today_right_pic )

	create9Sprite( ret_panel, UserConfig.wealth_rect )
	createLabel( ret_panel, UserConfig.wealth_left )
	ret_panel._wealth_right_txt = createLabel( ret_panel, UserConfig.wealth_right_txt )


	create9Sprite( ret_panel, UserConfig.battle_rect )
	createLabel( ret_panel, UserConfig.battle_left )
	ret_panel._battle_right_txt = createLabel( ret_panel, UserConfig.battle_right_txt )


	local function getContentSize(panel)
		return { ["width"] = 480, ["height"] = 200, }
	end

	ret_panel.getContentSize = getContentSize

	return ret_panel
end

local function createDynamicItemByInfo( info ) --创建动态
	
	local ret_panel = LIGHT_UI.clsNode:New( nil, 0, 0 )

	PAGE[ 'xyz' ] = {
			x = 0,
			y = 0,
			sx = 480,
			sy = 0,
			ax = 0,
			ay = 1,
		}

	local height = 0
	local content_obj = nil

	UserConfig.dynamic_time_c.text = info.time
	--UserConfig.dynamic_time_c.y = -1 * size.height
	local time_obj = createLabel( ret_panel, UserConfig.dynamic_time_c )

	if type( info.data) == "string" then
		UserConfig.dynamic_content_c.text = clearHTML( info.data )
		content_obj = createMultiLabel( ret_panel, UserConfig.dynamic_content_c )
	end
	
	if info.type == "7" then
		UserConfig.dynamic_content_c.text = "给" .. info.data.name .. "打了" .. info.data.score .. "分"
		content_obj = createMultiLabel( ret_panel, UserConfig.dynamic_content_c )

		UserConfig.pic_c.res = info.data.url
		createSprite( ret_panel, UserConfig.pic_c )
		height = 150
	elseif info.type == "1" then
		UserConfig.dynamic_content_c.text = "上传了新图片"
		content_obj = createMultiLabel( ret_panel, UserConfig.dynamic_content_c )
		
		UserConfig.pic_c.res = info.data.url
		createSprite( ret_panel, UserConfig.pic_c )
		height = 150
	end

	if not content_obj then
		UserConfig.dynamic_content_c.text = ''
		content_obj = createMultiLabel( ret_panel, UserConfig.dynamic_content_c )
	end
	local size = content_obj:getContentSize()	

	if height == 0 then
		height = math.abs( UserConfig.dynamic_content_c.y ) + size.height
	end

	height = height + UserConfig["simple_info_config"]["dynamic_info_margin"]

	UserConfig.dynamic_bg_c.sy = height
	create9Sprite( ret_panel, UserConfig.dynamic_bg_c )

	local function getContentSize( node )
		return { ["width"] = 480, ["height"] = height, }
	end

	ret_panel.getContentSize = getContentSize

	return ret_panel
end

local function configDynamicInfo()
	if userInfoPage then   --判断
		local news_layout = userInfoPage._move_info_page._news_layout
		news_layout:clearAllItem()

		for _, dynamic_info in pairs(dynamic_data) do
			news_layout:append_item(createDynamicItemByInfo(dynamic_info))
		end

		news_layout:refresh_view()
	end                 --判断
end

local function on_get_dynamic_info(data)
	dynamic_data = data.data
	if table.getn( dynamic_data ) > 0 then configDynamicInfo() end
end

local function request_dynamic_info(user_id, page)
	if not page then local page = 1 end
	doCommand( 'user', 'moving', { kind = 3, page = page, size = 20, uid = user_id, version=1 }, on_get_dynamic_info )
end

local function createMoveInfoPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)
	config = UserConfig["move_page_config"]

	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_info_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config.view_height)
	ret_panel._move_info_grp:setVMovable(true)

	local user_simple_info = createUserSimpleInfo(nil, 0, 0, ret_panel._move_info_grp)

	local news_layout = LIGHT_UI.clsRowLayout:New(nil, 0, 0, info_desc_info)
	news_layout._use_dynamic_size = true 
	news_layout:refresh_view()

	local function onAOverTop(layout)
		news_layout:clearAllItem()
		cur_dynamic_page = 1
		request_dynamic_info(LOGIN_PANEL.getUserData()["id"],cur_dynamic_page)
	end

	local function onAOverBottom(layout)
		if cur_dynamic_page < max_dynamic_page then
			cur_dynamic_page = cur_dynamic_page + 1
			request_dynamic_info(LOGIN_PANEL.getUserData()["id"],cur_dynamic_page)
		end
	end

	news_layout.onHMoveOverTop = onAOverTop
	news_layout.onHMoveOverBottom = onAOverBottom

	local battle_record = createBattleRecord(nil, 0, 0)

	local function get_y_limit(obj)
		return UserConfig["move_page_config"].inner_y 
	end

	user_simple_info:setPosition(0, config.inner_y)
	ret_panel._move_info_grp:appendItem(user_simple_info)
	news_layout:setPosition(0, config.inner_y)
	ret_panel._move_info_grp:appendItem(news_layout)
	battle_record:setPosition(0, config.inner_y)
	ret_panel._move_info_grp:appendItem(battle_record)
	ret_panel._move_info_grp:selectPage(1)

	user_simple_info.getYLimit = get_y_limit
	news_layout.getYLimit = get_y_limit
	battle_record.getYLimit = get_y_limit

	ret_panel._user_simple_info = user_simple_info
	ret_panel._news_layout = news_layout 
	ret_panel._battle_record = battle_record 
	
	return ret_panel
end

local function createBottomPanel(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	ret_panel._bottom_pic = LIGHT_UI.clsSprite:New(ret_panel, 0, 0, "mmfooter_bg.png")
	ret_panel._bottom_pic:setAnchorPoint(0, 0)

	local align_tbl = {}
	local function onConsume(btn, x, y)
		btn:setClickStatus()
		closeUserInfoPagePanel()
		dailyCount('UserInfoCunsum')--消费记录
		CONSUME_RECORD_PANEL.showConsumeInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
	ret_panel._consume_rec_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, "myprofile_record_pic.png", "myprofile_record_pic.png") 
	ret_panel._consume_rec_btn:setAnchorPoint(0, 0)
	ret_panel._consume_rec_btn:setString("")  --消费记录导航修改处
	ret_panel._consume_rec_btn.onTouchEnd = onConsume
	table.insert(align_tbl, ret_panel._consume_rec_btn)

	local function back_user_panel()
		--showUserInfoPagePanel( HTTP_CLIENT.getRootNode(), 0, 0 )
		if PANEL_CONTAINER.closeChildPanel( nil, 16 ) then						
			PANEL_CONTAINER.addChild( showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )				
		end	
	end
	
	local function onPack(btn, x, y)
		btn:setClickStatus()
		closeUserInfoPagePanel()
		dailyCount('UserInfoBag')--背包
		USER_PACKAGE_PANEL.showPackageInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, back_user_panel, nil, 3 )
	end
	
	ret_panel._pack_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, "myprofile_pack_pic.png", "myprofile_pack_pic.png") 
	ret_panel._pack_btn:setAnchorPoint(0, 0)
	ret_panel._pack_btn:setString("")  --背包导航修改处
	ret_panel._pack_btn.onTouchEnd = onPack
	table.insert(align_tbl, ret_panel._pack_btn)

	local function onMore(btn, x, y)
		btn:setClickStatus()
	end
	--[[ret_panel._more_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, "pack_btn.png", "pack_btn.png") 
	ret_panel._more_btn:setAnchorPoint(0, 0)
	ret_panel._more_btn:setString("")  --更多导航修改处
	ret_panel._more_btn.onTouchEnd = onMore--]]
	--table.insert(align_tbl, ret_panel._more_btn)

	LIGHT_UI.alignXCenterForObjList(align_tbl, 130)

	return ret_panel
end

local function createUserInfoPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createContainerChildPanel( parent, x, y )	

	local c, c2 = UserConfig, nil
	local o = nil

	local function on_back(btn)
		closeUserInfoPagePanel()
		
		if back_func then
			back_func()
		else
			MAINPAGE_PANEL.showMainPagePanel()
		end
	end
	
	--ret_panel._onBack = on_back
	--ret_panel._head_text:setString( '我的资料' )
	
	config = UserConfig["move_page_config"]
	ret_panel._move_info_page = createMoveInfoPage(ret_panel, config.x, config.y)

	local function on_sel_page(move_obj)
		ret_panel._move_hint:selItem(move_obj:getCurPage())
	end

	ret_panel._move_info_page._move_info_grp.onSelPage = on_sel_page

	local item_list = {
		[1] = {
			["txt"] = "资料",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
		[2] = {
			["txt"] = "动态",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
		[3] = {
			["txt"] = "战绩",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
	}

	config = UserConfig["move_hint_config"]	
	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, config.x, config.y, "tab_bg_image.png", item_list, total_width, config.res, "tiny_black_pixel.png")
	local bg = ret_panel._move_hint:getBGObj()
	bg:setScaleX(total_width)
	ret_panel._move_hint:selItem(1)

	local function on_click( obj, idx )
		if idx == 2 then
			dailyCount('UserInfoDynamic')--动态
		else
			dailyCount('UserInfoScore')--战绩
		end
		userInfoPage._move_hint:selItem( idx )		
		userInfoPage._move_info_page._move_info_grp:selectPage( idx )
	end

	ret_panel._move_hint.onClick = on_click

	ret_panel._bottom_panel = createBottomPanel(ret_panel, 0, 0)
	
	
	--重新载入头部，防止“穿越”
	local BASE_LAYOUT = Import("layout/base.lua")
	local c = BASE_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
	
	local c2 = nil
	local o = nil
	
	--ret_panel._head_bg = createSprite( ret_panel, c.head_bg )

	--ret_panel._head_back = createButton( ret_panel, c.head_back, on_back )
	
	c2 = c.head_text
	c2.text = '我的资料'
	--createLabel( ret_panel, c2 )
	
	ret_panel.closePanel = closeUserInfoPagePanel
	
	return ret_panel
end

local USER_ALBUM_PAGE = Import("lua/user_album.lua")
local UPLOAD_PAGE = Import("lua/upload.lua")

local function showNode(o,tag)
	for _,v in ipairs(o) do
		v:setVisible(tag)
	end
end

local function on_get_small_album(data)
	
	local function on_pic_click(msg_obj, x, y)
		closeUserInfoPagePanel()
		USER_ALBUM_PAGE.showAlbumPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	local function on_no_pic_click(msg_obj, x, y)
		closeUserInfoPagePanel()
		UPLOAD_PAGE.showUploadPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
	
	if data.code < 0 then
		showMessage( userInfoPage, data.data )
		return
	end
	
	album_data = data.data.data
	--CCMessageBox("album_data:" .. tostring( #album_data),'')
	local idx = 1
	for _, album_info in ipairs(album_data) do
		if idx > max_pic - 1 then
			break
		end
		if userInfoPage then    --判断
			local pic_obj = userInfoPage._move_info_page._user_simple_info._album_pic_list[idx]
			pic_obj:setImage(album_info["url"])
			showNode(userInfoPage._move_info_page._user_simple_info.small_album_list[idx],true)
						
			local o = userInfoPage._move_info_page._user_simple_info.nodelist[idx]						
					
			setMsgDelegate2(o, userInfoPage._move_info_page._move_info_grp, on_pic_click)						
			--屏蔽o的onTouchBegan方法
			o.onTouchBegan = nil		
			idx = idx + 1
		end		
	end		
	if idx <= max_pic - 1  then
		if userInfoPage then    --判断
			local pic_obj = userInfoPage._move_info_page._user_simple_info._album_pic_list[idx]
			pic_obj:setImage("add_photo_bg.png")
			local o = userInfoPage._move_info_page._user_simple_info.nodelist[idx]		
			--o.onTouchBegan =	
			
			o.onTouchBegan =
			setMsgDelegate2(o, userInfoPage._move_info_page._move_info_grp, on_no_pic_click)
			
			if userInfoPage._movse_info_page then
			showNode(userInfoPage._movse_info_page._user_simple_info.small_album_list[idx],true)
			end
		--local delete = 0
			for i = idx + 1, max_pic do	
				showNode(userInfoPage._move_info_page._user_simple_info.small_album_list[i],false)							
			end
		end
	else
		if userInfoPage then   --判断
			local pic_obj = userInfoPage._move_info_page._user_simple_info._album_pic_list[4]
			pic_obj:setImage("add_photo_bg.png")
			local o = userInfoPage._move_info_page._user_simple_info.nodelist[4]	
											
			setMsgDelegate2(o, userInfoPage._move_info_page._move_info_grp, on_no_pic_click)
			o.onTouchBegan = nil
			showNode(userInfoPage._move_info_page._user_simple_info.small_album_list[4],true)
			--local delete = 0
			for i = 4 + 1, max_pic do	
				showNode(userInfoPage._move_info_page._user_simple_info.small_album_list[i],false)							
			end
		end
	end
end

local function configBattleInfo()
	if userInfoPage then    --判断
		local p = userInfoPage._move_info_page._battle_record
		local c = UserConfig

		p._today_right_txt:setString( ( battle_rec_data["profit"] ) )	
		local size = p._today_right_txt:getContentSize()
		c.today_right_pic.x = c.today_right_pic.x - size.width
		createSprite( p, c.today_right_pic )
		p._wealth_right_txt:setString( clearHTML( battle_rec_data["rich"] ) )
		p._battle_right_txt:setString( ( battle_rec_data["win"] ) .. '胜 / ' .. ( battle_rec_data["lose"] ) .. '负' )
	
		return p
	end         --
end

local function on_get_battle_rec(data)
	battle_rec_data = data.data
	configBattleInfo()
end

function showUserInfoPagePanel(parent, x, y)
	dailyCount('UserInfo')--个人页面
	if not userInfoPage then
		userInfoPage = createUserInfoPagePanel(parent, x, y )
	else
		userInfoPage:setVisible(true)
	end	

	album_data = nil
	if not album_data then
		doCommand( 'album', 'small', { user = LOGIN_PANEL.getUserData()["id"], page = 1, size = 20, kind = 1 }, on_get_small_album )
	end

	request_dynamic_info( LOGIN_PANEL.getUserData()["id"] )

--	if not battle_rec_data then
		doCommand( 'user', 'score', { uid = LOGIN_PANEL.getUserData()["id"] }, on_get_battle_rec )
--	end
	return userInfoPage
end

function closeUserInfoPagePanel()
	--userInfoPage:setVisible(false)
	clear({userInfoPage})
	userInfoPage = nil
	
end

function get_page_obj()
	return userInfoPage
end

function refreshData()
	doCommand( 'album', 'small', { user = LOGIN_PANEL.getUserData()["id"], page = 1, size = 20, kind = 1 }, on_get_small_album )
end