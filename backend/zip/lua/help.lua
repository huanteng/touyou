LAYOUT = Import("layout/help.lua")
local layoutConfig = LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local panel = nil
local parentPageType = nil

local function createTopHintPanel(parent, x, y)
	local ret_panel = createNode(parent, {x=x,y=y})
	
	ret_panel._hint_text = createLabel( ret_panel, text( 0,0, "应用帮助", "e4" ) )
	ret_panel._hint_text:setAnchorPoint(0, 1)
	local function get_size(panel)
		return panel._hint_text:getContentSize()
	end
	ret_panel.getContentSize = get_size
	LIGHT_UI.alignXCenter(ret_panel)

	return ret_panel
end

local function createPanel(parent, x, y)
	local ret_panel = createBasePanel( parent, x, y )
	--local ret_panel = createNode(parent, {x=x,y=y})
	local function onClick()
		print('------------------------- onclick ------------------')	
	end
	local c_button = {x = 0 ,y = 0,res = layoutConfig["bg_config"].res,
	sx = layoutConfig["bg_config"].sx,sy = layoutConfig["bg_config"].sy,
	ax = 0,ay = 0}
	local b = createButton(ret_panel,c_button,onClick)
	local o = createSprite( ret_panel, layoutConfig["bg_config"] )
	--[[local function onClick()
		print('------------------------- onclick ------------------')	
	end
	setMsgDelegate(o,nil,onClick)	--]]
	o:setAnchorPoint(0, 0)
	o:setScaleX(layoutConfig["bg_config"].sx)
	o:setScaleY(layoutConfig["bg_config"].sy)
	ret_panel._bg_pic = o

	local o = createSprite( ret_panel, layoutConfig["top_pic_config"] )
	o:setAnchorPoint(0, 1)
	o:setScaleX(layoutConfig["top_pic_config"].sx)
	o:setScaleY(layoutConfig["top_pic_config"].sy)
	ret_panel._top_pic = o

	ret_panel._top_hint = createTopHintPanel(ret_panel, layoutConfig["top_hint_config"].x, layoutConfig["top_hint_config"].y)

	local function on_back(btn)
		closePanel()
		if parentPageType == 1 then
			if PANEL_CONTAINER.closeChildPanel( nil, 15 ) then
				local SETTING = Import("lua/setting.lua")							
				PANEL_CONTAINER.addChild( SETTING.showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
			end	
		elseif parentPageType == 2 then
			if PANEL_CONTAINER.closeChildPanel( nil, 9 ) then
				PANEL_CONTAINER.addChild( MAIN_PANEL.createMainPanel( PANEL_CONTAINER.ret_panel.bg, 0, 0 ) )	
			end
			PANEL_CONTAINER.expandContainer()
		end
		--SETTING = Import("lua/setting.lua")
		--SETTING.showPanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
	ret_panel._back_btn = createButton( ret_panel, layoutConfig["back_btn_config"] )	
	ret_panel._back_btn.onTouchEnd = on_back

	--ret_panel._hint_text = createLabel( ret_panel, text( layoutConfig["back_text_config"]["x"], layoutConfig["back_text_config"]["y"], help_text, "f10" ) )
	--ret_panel._hint_text:setAnchorPoint(0, 0)
	
	local remark_txt = LIGHT_UI.clsMultiLabel:New(ret_panel, layoutConfig["text_config"]["x"], layoutConfig["text_config"]["y"], layoutConfig["text_config"]["text"], GLOBAL_FONT, 18, 450, 1)
	remark_txt:setTextColor(102, 102, 102)
	
	return ret_panel
end

function showPanel(parent, x, y, prePageType )
	dailyCount('SettingcHelp')--应用帮助
	parentPageType = prePageType
	if not panel then
		panel = createPanel(parent, x, y)
	else
		panel:setVisible(true)
	end
end

function closePanel()
	--panel:setVisible(false)
	clear({panel})
	panel = nil
end