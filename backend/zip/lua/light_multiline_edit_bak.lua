local UI_BASE = import("base/light_ui/ui_base.lua")
local UI_TEXT = import("base/light_ui/light_text.lua")
local UI_BOX = import("base/light_ui/light_box.lua")
local UI_CONTAINER = import("base/light_ui/light_container.lua")
local UI_SPRITE = import("base/light_ui/light_sprite.lua")
local OLD_UI_BASE = import("base/ui/ubase.lua")

local TestTag = false 

clsLightTextMsgHandler = UI_CONTAINER.clsLightContainer:Inherit()
function clsLightTextMsgHandler:OnMouseIn(MouseX, MouseY)
	g_cursor:set_style("编辑")
end

function clsLightTextMsgHandler:OnMouseOut(MouseX, MouseY)
	g_cursor:set_style()
	self:GetMainObj():OnTextMouseOut(MouseX, MouseY)
end

function clsLightTextMsgHandler:OnMouseMove(MouseX, MouseY)
	self:GetMainObj():OnTextMouseMove(MouseX, MouseY)
end

function clsLightTextMsgHandler:OnLeftDown(MouseX, MouseY)
	self:GetMainObj():OnLeftDown(MouseX, MouseY)
	self:GetMainObj():OnTextLeftDown(MouseX, MouseY)
end

function clsLightTextMsgHandler:OnLeftUp(MouseX, MouseY)
	self:GetMainObj():OnTextLeftUp(MouseX, MouseY)
end

function clsLightTextMsgHandler:SetMainObj(MainObj)
	self._MainObj = MainObj
end

function clsLightTextMsgHandler:GetMainObj()
	return self._MainObj
end

clsLightMultiEdit = UI_BASE.clsObject:Inherit()

local InitZ = 1
local BlankElement = ""
local CursorWidth = 3 
local SelectAlpha = 200

--[[
TextInfo = {
	Color = ,
	Font =,
	Width = ,
	MaxCharCount = ,
	MaxTextWidth = ,
	LineInterval = ,
	MinMsgHeight = ,
}
--]]

local HideTextZ = 1
local TextZ = 2 
local SelectZ = 3 
local CursorZ = 4

local function ReleaseExtraRes(EditObj)
	if EditObj._BlinkCursorTimer then
		EditObj._BlinkCursorTimer("cancel")
		EditObj._BlinkCursorTimer = nil
	end

	if OLD_UI_BASE.GetActiveEditObj() == EditObj then
		UI_BASE.TryFocusDefaultChat()
	end
end

-- Combination Class
function clsLightMultiEdit:InitCommon(ParentObj, BaseX, BaseY, TextInfo)
	assert(BaseX)
	assert(BaseY)
	assert(TextInfo.MaxCharCount)

	-- 控件所使用的变量初始化列表
	self._ElementRec = {} -- 记录每个字符的字节起点 StartIdx, EndIdx
	self._InsertIndex = 0 -- 插入下标 也是选择文本的起始下标
	self._StartIndex = nil -- 选择文本的起始下标
	self._StartLine = nil
	self._DesIndex = nil -- 选择文本的目标下标
	self._DesLine = nil
	self._CanCopy = true
	self._NewLineCharIndexList = {}
	self._TmpCharCnt = 0
	self._MaxCharCount = TextInfo.MaxCharCount
	self._MaxTextWidth = TextInfo.MaxTextWidth
	self._MinMsgHeight = TextInfo.MinMsgHeight
	self._Font = TextInfo.Font
	self._Color = TextInfo.Color
	self._LMouseBtnDown = false -- 记录鼠标左键是否处于按下状态
	self._BlinkCursorTimer = nil
	self._MouseMoveX = 0 -- 记录鼠标移动时的当前坐标
	self._MouseMoveY = 0
	self._LineInterval = TextInfo.LineInterval

	self._CurLine = 1
	self._LineRec = {}
	self._SegIndex = 1
	--
	self._BaseContainerObj = UI_CONTAINER.clsLightContainer:NewObj(ParentObj, BaseX, BaseY, 1, 1)
	self._BaseContainerObj:RegisterResRelease(ReleaseExtraRes, self)
	self._BaseContainerObj:SetZ(InitZ)
	self._BaseContainerObj:SetHandleMouseMove(true)
	self._BaseContainerObj:SetHandleMouseClick(true)

	self._TextMsgHandler = clsLightTextMsgHandler:NewObj(self._BaseContainerObj, 0, 0, TextInfo.Width, TextInfo.MinMsgHeight)
	self._TextMsgHandler:SetZ(InitZ)
	self._TextMsgHandler:SetHandleMouseMove(true)
	self._TextMsgHandler:SetHandleMouseClick(true)
	self._TextMsgHandler:SetMainObj(self)

	local OldGetFocus = self._TextMsgHandler.GetClickFocus
	self._TextMsgHandler.GetClickFocus = function(obj)
		OldGetFocus(obj)
		self:GetClickFocus()
	end

	local OldLostFocus = self._TextMsgHandler.LostClickFocus
	self._TextMsgHandler.LostClickFocus = function(obj)
		OldLostFocus(obj)
		self:LostClickFocus()
	end

	if TestTag then -- for test ???
		self._TestBGObj = UI_BOX.clsLightBox:NewObj(self._BaseContainerObj, 0, 0, 0, 0, 0x000000)
		self._TestBGObj:SetSize(TextInfo.Width, 0)
		self._TestBGObj:SetZ(InitZ)
		self._TextMsgHandler:TopIt(self._TestBGObj)
	end

	self._SelectBoxList = {}

	self._TextCursor = UI_BOX.clsLightBox:NewObj(self._TextMsgHandler, 0, 0, CursorWidth, 0)
	self._TextCursor:SetShow(false)
	self._TextCursor:SetZ(CursorZ)

	self._LineRec[self._CurLine] = {}
	self._LineRec[self._CurLine].StartIdx = 0
	self._LineRec[self._CurLine].EndIdx = 0
	self._LineRec[self._CurLine].Segment = self._SegIndex 

	self._HideTextObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, BlankElement, TextInfo.Color, TextInfo.Font)
	self._HideTextObj:SetZ(HideTextZ)
	self._HideTextObj:SetShow(false)
end

function clsLightMultiEdit:__init__(ParentObj, BaseX, BaseY, TextInfo)
	self:InitCommon(ParentObj, BaseX, BaseY, TextInfo)

	self._TextBuffer = BlankElement -- 特有数据成员

	self._TextObjList = {}
	local TempTextObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, BlankElement, TextInfo.Color, TextInfo.Font)
	TempTextObj:SetZ(TextZ)
	self._TextObjList[self._CurLine] = TempTextObj
	self._LineHeight = TempTextObj:GetFontHeight()

	self._TextCursor:SetSize(CursorWidth, self._LineHeight)

	if TestTag then
		self._TestBGObj:SetSize(self._MaxTextWidth, self._LineHeight)
	end
end

function clsLightMultiEdit:GetShow()
	return self._BaseContainerObj:GetShow()
end

local BlinkFre = 10 
local function DoBlinkCursor(EditObj)
	if EditObj._TextCursor:GetShow() then
		EditObj._TextCursor:SetShow(false)
	else
		EditObj._TextCursor:SetShow(true)
	end

	return BlinkFre
end

local function ClearBrush(EditObj)
	for LineIndex, SelectObj in pairs(EditObj._SelectBoxList) do
		SelectObj:Release()
		EditObj._SelectBoxList[LineIndex] = nil
	end
end

local function ClearSelection(EditObj)
	ClearBrush(EditObj)
	EditObj._DesIndex = nil
	EditObj._DesLine = nil
	EditObj._StartIndex = nil
	EditObj._StartLine = nil
end

function clsLightMultiEdit:TryNoticeListener(SizeChanged)
	if self._ViewEventListener then
		local CursorPosX, CursorPosY = self._TextCursor:GetPos()
		local CursorWidth, CursorHeight = self._TextCursor:GetSize()
		self._ViewEventFunc(self._ViewEventListener, CursorPosX, CursorPosY, CursorWidth, CursorHeight, SizeChanged)
	end
end

function clsLightMultiEdit:CalElementSize(ElementList)
	self._HideTextObj:SetText(ElementList)
	return self._HideTextObj:GetSize()
end

local function ShowCursor(EditObj)
	if not EditObj._BlinkCursorTimer then
		EditObj._BlinkCursorTimer = delay_call(BlinkFre, DoBlinkCursor, EditObj)
	end

	EditObj._TextCursor:SetShow(true)
end

local function HideCursor(EditObj)
	if EditObj._BlinkCursorTimer then
		EditObj._BlinkCursorTimer("cancel")
		EditObj._BlinkCursorTimer = nil
	end

	EditObj._TextCursor:SetShow(false)
end

function clsLightMultiEdit:LostFocus()
	HideCursor(self)

	set_global_edit(nil, nil)
end

function clsLightMultiEdit:GetSubElement(BeginIndex, EndIndex)
	if BeginIndex <= 0 then
		return ""
	else
		return string.sub(self._TextBuffer, self._ElementRec[BeginIndex].StartIdx, self._ElementRec[EndIndex].EndIdx)
	end
end

local function GetLineEleSize(EditObj, LineIndex)
	local EleList = EditObj:GetSubElement(EditObj._LineRec[LineIndex].StartIdx, EditObj._LineRec[LineIndex].EndIdx)
	return EditObj:CalElementSize(EleList)
end

local function CalRelaIndexByMouseX(EditObj, LineIndex, TextMouseX)
	if EditObj._LineRec[LineIndex].StartIdx <= 0 then
		return 0 
	end

	local TextWidth, _ = GetLineEleSize(EditObj, LineIndex)
	if TextMouseX >= TextWidth then
		return EditObj._LineRec[LineIndex].EndIdx - EditObj._LineRec[LineIndex].StartIdx + 1
	end

	if TextMouseX <= 0 then
		return 0
	end

	local ExpandStr = BlankElement 
	local CharIndex = 0 
	local LastTextWidth = 0
	local TextRec = EditObj._ElementRec
	for CharIndex = EditObj._LineRec[LineIndex].StartIdx, EditObj._LineRec[LineIndex].EndIdx do
		ExpandStr = EditObj:GetSubElement(EditObj._LineRec[LineIndex].StartIdx, CharIndex)

		local TextWidth, _ = EditObj:CalElementSize(ExpandStr)
		if TextWidth > TextMouseX then
			if ((TextWidth + LastTextWidth) / 2) > TextMouseX then
				return CharIndex - EditObj._LineRec[LineIndex].StartIdx

			else
				return CharIndex - EditObj._LineRec[LineIndex].StartIdx + 1

			end
		end

		LastTextWidth = TextWidth
	end
end

local UnicodeCharCode = 256
local function ParsChar(CharCode)
	if CharCode < UnicodeCharCode then
		return string.char(CharCode), 1
	else
		return string.char(math.mod(CharCode, UnicodeCharCode), math.floor(CharCode/UnicodeCharCode)), 2
	end
end

local ANSCOde = 128
local function UnParsChar(Char)
	if string.byte(Char) > ANSCOde then
		return 2
	else
		return 1
	end
end

function clsLightMultiEdit:CalTextObjY(LineIndex)
	return (self._LineHeight + self._LineInterval) * (LineIndex - 1)
end

function clsLightMultiEdit:InsertChar(Char, ElementSize)
	local InsertIndex = self._InsertIndex
	local TextRec = self._ElementRec

	local PreStr = BlankElement 
	local TailStr = BlankElement 

	if InsertIndex > 0 then
		PreStr = self:GetSubElement(1, InsertIndex)
	end

	local CharCount = #(TextRec)
	if InsertIndex < CharCount then
		TailStr = self:GetSubElement(InsertIndex + 1, CharCount)
	end

	self._TextBuffer = string.format("%s%s%s", PreStr, Char, TailStr)

	local TextRec = self._ElementRec
	local AfterIndex = InsertIndex + 1 
	for i = AfterIndex, #(TextRec) do
		local CharInfo = TextRec[i]
		CharInfo.StartIdx = CharInfo.StartIdx + ElementSize 
		CharInfo.EndIdx = CharInfo.EndIdx + ElementSize 
	end

	local InsertNode = {} 
	if InsertIndex <= 0 then
		InsertNode.StartIdx = 1
	else
		InsertNode.StartIdx = TextRec[InsertIndex].EndIdx + 1
	end

	InsertNode.EndIdx = InsertNode.StartIdx + ElementSize - 1

	table.insert(TextRec, AfterIndex, InsertNode)

	self._InsertIndex = self._InsertIndex + 1
end

function clsLightMultiEdit:DeleteSubElement(BeginIndex, EndIndex)
	local PreStr = BlankElement 
	local TailStr = BlankElement 

	local TextRec = self._ElementRec
	if BeginIndex > 1 then
		PreStr = self:GetSubElement(1, BeginIndex - 1)
	end

	local CharCount = #(TextRec)
	if EndIndex < CharCount then
		TailStr = self:GetSubElement(EndIndex + 1, CharCount)
	end

	self._TextBuffer = string.format("%s%s", PreStr, TailStr)

	local DeleteCharSize = 0
	for i = BeginIndex, EndIndex do
		local DeleteNode = TextRec[i]
		DeleteCharSize = DeleteCharSize + DeleteNode.EndIdx - DeleteNode.StartIdx + 1
	end

	local AfterIndex = EndIndex + 1
	if AfterIndex <= CharCount then
		for i = AfterIndex, CharCount do
			TextRec[i].StartIdx = TextRec[i].StartIdx - DeleteCharSize
			TextRec[i].EndIdx = TextRec[i].EndIdx - DeleteCharSize
		end
	end

	local RemoveCount = EndIndex - BeginIndex + 1
	for i = 1, RemoveCount do
		table.remove(TextRec, BeginIndex)
	end
end

function clsLightMultiEdit:CalIndexPixelPosX(LineIndex, RelaIndex)
	local DisplayBeginIdx = self._LineRec[LineIndex].StartIdx
	if DisplayBeginIdx <= 0 then
		return 0
	end

	local TextRec = self._ElementRec
	if RelaIndex < (DisplayBeginIdx - self._LineRec[LineIndex].StartIdx + 1) then
		return 0
	else
		local TempElementList = self:GetSubElement(DisplayBeginIdx, DisplayBeginIdx + RelaIndex - 1)
		local PixelWidth, _ = self:CalElementSize(TempElementList)

		return PixelWidth
	end
end

local function CalRelaIndex(EditObj, LineIndex, AbsIndex)
	if EditObj._LineRec[LineIndex].StartIdx == 0 then
		return 0
	else
		return (AbsIndex - EditObj._LineRec[LineIndex].StartIdx + 1)
	end
end

local function CalPosByIndex(EditObj, LineIndex, RelaIndex)
	local CursorPixelPosX = EditObj:CalIndexPixelPosX(LineIndex, RelaIndex)
	local CursorPixelPosY = EditObj:CalTextObjY(LineIndex)

	return CursorPixelPosX, CursorPixelPosY
end

local function DisplayCursorByIndex(EditObj, LineIndex, RelaIndex)
	local CursorPixelPosX, CursorPixelPosY = CalPosByIndex(EditObj, LineIndex, RelaIndex)

	EditObj._TextCursor:SetPos(CursorPixelPosX, CursorPixelPosY)
	EditObj._TextCursor:SetSize(CursorWidth, EditObj:GetLineHeight(LineIndex)) 
end

local function MoveCursorForwardLeft(EditObj)
	local CurLine = EditObj._CurLine
	if EditObj._LineRec[CurLine].StartIdx <= 0 then
		return
	end

	EditObj._InsertIndex = EditObj._InsertIndex - 1
	if EditObj._InsertIndex < 0 then
		EditObj._InsertIndex = 0
		return
	end

	if EditObj._InsertIndex < EditObj._LineRec[CurLine].StartIdx - 1 then
		EditObj._CurLine = EditObj._CurLine - 1
		EditObj._InsertIndex = EditObj._InsertIndex + 1
	end

	local RelaIndex = CalRelaIndex(EditObj, EditObj._CurLine, EditObj._InsertIndex)
	DisplayCursorByIndex(EditObj, EditObj._CurLine, RelaIndex)
end

function clsLightMultiEdit:DoLeftProc()
	ClearSelection(self)
	MoveCursorForwardLeft(self)
end

local function MoveCursorForwardRight(EditObj)
	local CurLine = EditObj._CurLine
	if EditObj._LineRec[CurLine].StartIdx <= 0 then
		return
	end

	EditObj._InsertIndex = EditObj._InsertIndex + 1
	if EditObj._InsertIndex > #(EditObj._ElementRec) then
		EditObj._InsertIndex = #(EditObj._ElementRec)
		return
	end

	if EditObj._InsertIndex > EditObj._LineRec[CurLine].EndIdx then
		EditObj._CurLine = EditObj._CurLine + 1
		EditObj._InsertIndex = EditObj._InsertIndex - 1
	end

	local RelaIndex = CalRelaIndex(EditObj, EditObj._CurLine, EditObj._InsertIndex)
	DisplayCursorByIndex(EditObj, EditObj._CurLine, RelaIndex)
end

function clsLightMultiEdit:DoRightProc()
	ClearSelection(self)
	MoveCursorForwardRight(self)
end

function clsLightMultiEdit:SetParent(ParentObj)
	self._BaseContainerObj:SetParent(ParentObj)
end

local function HasSelText(EditObj)
	if (EditObj._DesIndex) and (EditObj._DesIndex ~= EditObj._StartIndex) then
		return true
	else
		return false
	end
end

local function OnEditChangeHeight(EditObj, OldHeight, NewHeight)
	if OldHeight == NewHeight then
		return
	end

	local Width, Height = EditObj._TextMsgHandler:GetSize()
	if NewHeight > EditObj._MinMsgHeight then
		EditObj._TextMsgHandler:SetSize(Width, NewHeight)
	else
		EditObj._TextMsgHandler:SetSize(Width, EditObj._MinMsgHeight)
	end

	if TestTag then -- for test ???
		local Width, Height = EditObj._TextMsgHandler:GetSize()
		EditObj._TestBGObj:SetSize(Width, Height)
	end
end

function clsLightMultiEdit:UnDisplayLine(LineIndex)
	self._TextObjList[LineIndex]:SetText("")
end

local function UnDisplay(EditObj, BeginLine, EndLine)
	for i = BeginLine, EndLine do
		EditObj:UnDisplayLine(i)
	end
end

function clsLightMultiEdit:DisplayLine(LineIndex)
	local EleList = self:GetSubElement(self._LineRec[LineIndex].StartIdx, self._LineRec[LineIndex].EndIdx)
	local TextObj = self._TextObjList[LineIndex]
	TextObj:SetText(EleList)
	local PosX = 0
	local PosY = self:CalTextObjY(LineIndex)
	TextObj:SetPos(PosX, PosY)
end

function clsLightMultiEdit:SetPos(x, y)
	self._BaseContainerObj:SetPos(x, y)
end

function clsLightMultiEdit:GetPos()
	return self._BaseContainerObj:GetPos()
end

function clsLightMultiEdit:SetParent(ParentObj)
	self._BaseContainerObj:SetParent(ParentObj)
end

function clsLightMultiEdit:GetSize()
	local RetWidth, _ = self._TextMsgHandler:GetSize()
	local RetHeight = self:GetEditHeight()

	return RetWidth, RetHeight
end

local function Display(EditObj, BeginLine, EndLine)
	for i = BeginLine, EndLine do
		EditObj:DisplayLine(i)
	end
end

local function RefreshDisplay(EditObj, BeginLine, EndLine)
	UnDisplay(EditObj, BeginLine, EndLine)
	Display(EditObj, BeginLine, EndLine)
end

local function MergeSegment(EditObj, BeginIndex, OldSegment, NewSegment)
	while ((EditObj._LineRec[BeginIndex]) and (EditObj._LineRec[BeginIndex].Segment == OldSegment)) do
		EditObj._LineRec[BeginIndex].Segment = NewSegment
		BeginIndex = BeginIndex + 1
	end
end

local TryAdjustLayout
local function DoReturnEffect(EditObj, LineIndex, AbsIndex)
	EditObj._SegIndex = EditObj._SegIndex + 1
	local CurLine = LineIndex
	local TailCnt = 0
	if EditObj._LineRec[CurLine].StartIdx > 0 then
		TailCnt = EditObj._LineRec[CurLine].EndIdx - AbsIndex
	end

	EditObj._LineRec[CurLine].EndIdx = EditObj._LineRec[CurLine].EndIdx - TailCnt
	if EditObj._LineRec[CurLine].EndIdx < EditObj._LineRec[CurLine].StartIdx then
		EditObj._LineRec[CurLine].StartIdx = 0
		EditObj._LineRec[CurLine].EndIdx = 0
	end

	local InitStartIdx = 0
	local InitEndIdx = 0
	if TailCnt > 0 then
		InitStartIdx = AbsIndex + 1
		InitEndIdx = AbsIndex + TailCnt
	end

	local NewLineIndex = CurLine + 1
	EditObj:NewLine(NewLineIndex, InitStartIdx, InitEndIdx, EditObj._SegIndex)

	local OldSegment = EditObj._LineRec[CurLine].Segment
	local NewSegment = EditObj._LineRec[CurLine + 1].Segment
	MergeSegment(EditObj, CurLine + 2, OldSegment, NewSegment)

	TryAdjustLayout(EditObj, CurLine + 1, NewSegment)
	RefreshDisplay(EditObj, CurLine, #(EditObj._LineRec))

	return NewLineIndex
end

function clsLightMultiEdit:DoReturnProc()
	local OldHeight = self:GetEditHeight()
	self._CurLine = DoReturnEffect(self, self._CurLine, self._InsertIndex)
	local NewHeight = self:GetEditHeight()

	OnEditChangeHeight(self, OldHeight, NewHeight)

	local RelaIndex = CalRelaIndex(self, self._CurLine, self._InsertIndex)
	DisplayCursorByIndex(self, self._CurLine, RelaIndex)
end

local function CalSelTextIndexRange(EditObj)
	if (not EditObj._DesIndex) or (EditObj._DesIndex == EditObj._StartIndex) then
		return nil, nil 
	end

	if EditObj._DesIndex < EditObj._StartIndex then
		return EditObj._DesIndex + 1, EditObj._StartIndex
	else
		return EditObj._StartIndex + 1, EditObj._DesIndex
	end
end

local function GetDeleteRange(EditObj)
	local BeginIndex, EndIndex = CalSelTextIndexRange(EditObj)
	if not BeginIndex then
		return EditObj._InsertIndex, EditObj._InsertIndex
	else
		return BeginIndex, EndIndex
	end
end

function clsLightMultiEdit:TryReturnProc()
	if HasSelText(self) then
		local BeginIndex, EndIndex = GetDeleteRange(self)
		self:DoDelete(BeginIndex, EndIndex)
	end

	self:DoReturnProc()
end

function clsLightMultiEdit:ResetEdit()
	self._ElementRec = {}
	self._TextBuffer = BlankElement 
	self._InsertIndex = 0
	self._TextCursor:SetPos(0, 0)
	for _, TextObj in pairs(self._TextObjList) do
		TextObj:SetText(BlankElement)
	end

	ClearSelection(self)

	self._CurLine = 1
	self._LineRec = {}
	self._LineRec[self._CurLine] = {}
	self._LineRec[self._CurLine].StartIdx = 0
	self._LineRec[self._CurLine].EndIdx = 0
	self._SegIndex = 1
	self._LineRec[self._CurLine].Segment = self._SegIndex 

	self:TryNoticeListener(true)
end

function clsLightMultiEdit:ConvertElementToText(ElementList)
	return ElementList
end

local function CalLineByIndex(EditObj, Index)
	if Index <= 0 then
		return 1
	end

	for LineIndex, LineInfo in ipairs(EditObj._LineRec) do
		if Index >= LineInfo.StartIdx and Index <= LineInfo.EndIdx then
			return LineIndex
		end
	end

	return nil
end

local function GetOverrideRange(Begin1, End1, Begin2, End2)
	local RetBegin = 0
	local RetEnd = 0

	if Begin1 > Begin2 then
		RetBegin = Begin1
	else
		RetBegin = Begin2
	end

	if End1 > End2 then
		RetEnd = End2
	else
		RetEnd = End1
	end

	return RetBegin, RetEnd
end

local IgnoreChar = "\r"
local NewLineChar = "\n"

function clsLightMultiEdit:GetTextByRange(BeginIndex, EndIndex)
	local BeginLine = CalLineByIndex(self, BeginIndex)
	local EndLine = CalLineByIndex(self, EndIndex)
	local RetText = ""
	local PreSegment = nil 
	for i = BeginLine, EndLine do
		if self._LineRec[i].StartIdx > 0 then
			if PreSegment and PreSegment ~= self._LineRec[i].Segment then
				RetText = RetText .. IgnoreChar .. NewLineChar
			end
			PreSegment = self._LineRec[i].Segment
			local TempBeginIndex, TempEndIndex = GetOverrideRange(self._LineRec[i].StartIdx, self._LineRec[i].EndIdx, BeginIndex, EndIndex)
			local SubElement = self:GetSubElement(TempBeginIndex, TempEndIndex)
			local TempText = self:ConvertElementToText(SubElement)
			RetText = RetText .. TempText
		else
			RetText = RetText .. IgnoreChar .. NewLineChar
		end
	end

	return RetText
end

local function TryCopyProc(EditObj)
	if (not EditObj._CanCopy) then
		return
	end

	if IsCtrlDown() then
		local BeginIndex, EndIndex = CalSelTextIndexRange(EditObj)
		if BeginIndex and EditObj._ElementRec[BeginIndex] and EndIndex and EditObj._ElementRec[EndIndex] then
			local CopyText = EditObj:GetTextByRange(BeginIndex, EndIndex)
			if not CopyText then
				return
			end

			win32.SetClipboard(CopyText)
		end
	end
end

function clsLightMultiEdit:DisableCopy()
	self._CanCopy = false
end

function clsLightMultiEdit:GetText()
	local EleCount = #(self._ElementRec)
	if EleCount <= 0 then
		return ""
	end

	return self:GetTextByRange(1, EleCount)
end

function clsLightMultiEdit:NewLine(LineIndex, StartIdx, EndIdx, Segment)
	table.insert(self._LineRec, LineIndex, {})
	self._LineRec[LineIndex].StartIdx = StartIdx 
	self._LineRec[LineIndex].EndIdx = EndIdx 
	self._LineRec[LineIndex].Segment = Segment

	local TempTextObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, self:CalTextObjY(LineIndex), BlankElement, self._Color, self._Font)
	TempTextObj:SetZ(TextZ)
	table.insert(self._TextObjList, LineIndex, TempTextObj)
end

function clsLightMultiEdit:DeleteLine(LineIndex)
	table.remove(self._LineRec, LineIndex)
	self._TextObjList[LineIndex]:Release()
	table.remove(self._TextObjList, LineIndex)
end

local function GetTailOutEleCount(EditObj, LineIndex)
	local BeginIndex = EditObj._LineRec[LineIndex].StartIdx
	local TempEndIndex = BeginIndex
	local ElementList = EditObj:GetSubElement(BeginIndex, TempEndIndex)
	local SumWidth = 0
	local EleWidth, _ = EditObj:CalElementSize(ElementList)
	SumWidth = EleWidth
	while ((SumWidth + CursorWidth) <= EditObj._MaxTextWidth) do
		TempEndIndex = TempEndIndex + 1
		if TempEndIndex > EditObj._LineRec[LineIndex].EndIdx then
			return 0
		end

		ElementList = EditObj:GetSubElement(BeginIndex, TempEndIndex)
		EleWidth, _ = EditObj:CalElementSize(ElementList)
		SumWidth = EleWidth
	end

	return EditObj._LineRec[LineIndex].EndIdx - TempEndIndex + 1 
end

local function CalTailFreeSpace(EditObj, LineIndex)
	if EditObj._LineRec[LineIndex].StartIdx == -1 then
		return EditObj._MaxTextWidth
	end

	local ElementList = EditObj:GetSubElement(EditObj._LineRec[LineIndex].StartIdx, EditObj._LineRec[LineIndex].EndIdx)
	local TempWidth, _ = EditObj:CalElementSize(ElementList)

	return (EditObj._MaxTextWidth - TempWidth)
end

local function GetFitHeadEleCount(EditObj, Space, LineIndex)
	local TempEndIndex = EditObj._LineRec[LineIndex].StartIdx
	while (TempEndIndex <= EditObj._LineRec[LineIndex].EndIdx) do
		local ElementList = EditObj:GetSubElement(EditObj._LineRec[LineIndex].StartIdx, TempEndIndex)
		local TempWidth, _ = EditObj:CalElementSize(ElementList) + CursorWidth
		if TempWidth > Space then
			break
		end

		TempEndIndex = TempEndIndex + 1
	end

	return (TempEndIndex - EditObj._LineRec[LineIndex].StartIdx)
end

TryAdjustLayout = function(EditObj, LineIndex, Segment)
	if (not EditObj._LineRec[LineIndex]) then
		return
	end

	if (EditObj._LineRec[LineIndex].Segment ~= Segment) then
		return
	end

	if EditObj._LineRec[LineIndex].StartIdx == -1 and LineIndex > 1 then
		EditObj:DeleteLine(LineIndex)
		TryAdjustLayout(EditObj, LineIndex, Segment)
		return
	end

	local ElementList = EditObj:GetSubElement(EditObj._LineRec[LineIndex].StartIdx, EditObj._LineRec[LineIndex].EndIdx)
	local TempWidth, _ = EditObj:CalElementSize(ElementList)
	if ((TempWidth + CursorWidth) > EditObj._MaxTextWidth) then 
		local BackEleCount = GetTailOutEleCount(EditObj, LineIndex)
		if (not EditObj._LineRec[LineIndex + 1]) or (EditObj._LineRec[LineIndex + 1].Segment ~= Segment) then
			EditObj._LineRec[LineIndex].EndIdx = EditObj._LineRec[LineIndex].EndIdx - BackEleCount
			EditObj:NewLine(LineIndex + 1, EditObj._LineRec[LineIndex].EndIdx + 1, EditObj._LineRec[LineIndex].EndIdx + BackEleCount, EditObj._LineRec[LineIndex].Segment)

			TryAdjustLayout(EditObj, LineIndex + 1, Segment)

			return
		elseif EditObj._LineRec[LineIndex + 1].StartIdx == -1 then
			EditObj:DeleteLine(LineIndex + 1)
			TryAdjustLayout(EditObj, LineIndex, Segment)

			return
		else
			EditObj._LineRec[LineIndex].EndIdx = EditObj._LineRec[LineIndex].EndIdx - BackEleCount
			EditObj._LineRec[LineIndex + 1].StartIdx = EditObj._LineRec[LineIndex + 1].StartIdx - BackEleCount
		end
	else
		if (not EditObj._LineRec[LineIndex + 1]) then

			return
		end

		if (EditObj._LineRec[LineIndex + 1].StartIdx == -1) then
			EditObj:DeleteLine(LineIndex + 1)
			TryAdjustLayout(EditObj, LineIndex, Segment)

			return
		elseif (EditObj._LineRec[LineIndex + 1].Segment ~= Segment) then

			return
		end

		local LeftSpace = CalTailFreeSpace(EditObj, LineIndex)
		local FitEleCount = GetFitHeadEleCount(EditObj, LeftSpace, LineIndex + 1)
		if FitEleCount <= 0 then
			TryAdjustLayout(EditObj, LineIndex + 1, Segment)

			return
		end

		if EditObj._LineRec[LineIndex].StartIdx == 0 then
			EditObj._LineRec[LineIndex].StartIdx = EditObj._LineRec[LineIndex + 1].StartIdx
			EditObj._LineRec[LineIndex].EndIdx = EditObj._LineRec[LineIndex].StartIdx + FitEleCount - 1
		else
			EditObj._LineRec[LineIndex].EndIdx = EditObj._LineRec[LineIndex].EndIdx + FitEleCount
		end

		EditObj._LineRec[LineIndex + 1].StartIdx = EditObj._LineRec[LineIndex + 1].StartIdx + FitEleCount
		if EditObj._LineRec[LineIndex + 1].StartIdx > EditObj._LineRec[LineIndex + 1].EndIdx then
			EditObj:DeleteLine(LineIndex + 1)
			TryAdjustLayout(EditObj, LineIndex, Segment)

			return
		end
	end

	TryAdjustLayout(EditObj, LineIndex + 1, Segment)
end

function clsLightMultiEdit:GetEditHeight()
	local LineCount = #(self._LineRec)
	return LineCount * (self._LineHeight + self._LineInterval)
end

local function SetClearIndex(EditObj, LineIndex)
	if (EditObj._LineRec[LineIndex - 1] and EditObj._LineRec[LineIndex - 1].Segment == EditObj._LineRec[LineIndex].Segment) then
		EditObj._LineRec[LineIndex].StartIdx = -1 
		EditObj._LineRec[LineIndex].EndIdx = -1 
	else
		EditObj._LineRec[LineIndex].StartIdx = 0 
		EditObj._LineRec[LineIndex].EndIdx = 0 
	end
end

function clsLightMultiEdit:DoInsertNewLineChar()
	table.insert(self._NewLineCharIndexList, self._InsertIndex)
	self._TmpCharCnt = self._TmpCharCnt - 1
end

function clsLightMultiEdit:PreProcInputStr(InputStr)
	return InputStr
end

function clsLightMultiEdit:CalStrEleCnt(Str)
	local StringLen = string.len(Str)
	local StartIndex = 1

	local RetCnt = 0
	while StartIndex <= StringLen do
		RetCnt = RetCnt + 1
		local Char = string.sub(Str, StartIndex, StartIndex)
		local CharByteCount = UnParsChar(Char)
		StartIndex = StartIndex + CharByteCount
	end

	return RetCnt
end

local function FindFirstLineBySegment(EditObj, BeginLine, Segment)
	local SumLine = #EditObj._LineRec
	for i = BeginLine, SumLine do
		if EditObj._LineRec[i].Segment == Segment then
			return i
		end
	end
end

function clsLightMultiEdit:AddEleAdjustLine(EleCount)
	local BeginEditLine = self._CurLine
	local BeginRefreshLine = BeginEditLine
	local AdjustLineList = {}
	table.insert(AdjustLineList, self._LineRec[BeginRefreshLine].Segment)

	if self._LineRec[BeginEditLine].StartIdx == 0 then
		self._LineRec[BeginEditLine].StartIdx = self._InsertIndex - EleCount + 1
		self._LineRec[BeginEditLine].EndIdx = self._LineRec[BeginEditLine].StartIdx + EleCount - 1
	else
		self._LineRec[BeginEditLine].EndIdx = self._LineRec[BeginEditLine].EndIdx + EleCount 
	end

	local LineCount = #(self._LineRec)
	if BeginEditLine < LineCount then
		for i = BeginEditLine + 1, LineCount do
			if self._LineRec[i].StartIdx > 0 then
				self._LineRec[i].StartIdx = self._LineRec[i].StartIdx + EleCount 
				self._LineRec[i].EndIdx = self._LineRec[i].EndIdx + EleCount 
			end
		end
	end

	local TempBeginLine = BeginEditLine
	for _, TempIndex in ipairs(self._NewLineCharIndexList) do
		local TempBeginIdx = TempIndex + 1
		local TempEndIdx = self._LineRec[TempBeginLine].EndIdx
		if TempEndIdx < TempBeginIdx then
			TempBeginIdx = 0
			TempEndIdx = 0
		end
		self._SegIndex = self._SegIndex + 1
		self:NewLine(TempBeginLine + 1, TempBeginIdx, TempEndIdx, self._SegIndex)
		table.insert(AdjustLineList, self._SegIndex)

		if self._LineRec[TempBeginLine].StartIdx > 0 then
			self._LineRec[TempBeginLine].EndIdx = TempIndex
			if self._LineRec[TempBeginLine].EndIdx < self._LineRec[TempBeginLine].StartIdx then
				self._LineRec[TempBeginLine].StartIdx = 0
				self._LineRec[TempBeginLine].EndIdx = 0
			end
		end

		TempBeginLine = TempBeginLine + 1
	end
	self._NewLineCharIndexList = {}

	for _, AdjustSegment in ipairs(AdjustLineList) do
		local LineIndex = FindFirstLineBySegment(self, BeginEditLine, AdjustSegment)
		TryAdjustLayout(self, LineIndex, AdjustSegment)
	end

	RefreshDisplay(self, BeginRefreshLine, #(self._LineRec))
end

function clsLightMultiEdit:SubEleAdjustLine(EleCount)
        local BeginEditLine = self._CurLine
        local BeginRefreshLine = BeginEditLine

	local TempStartLine = BeginEditLine
	local LeftEleCount = self._LineRec[TempStartLine].EndIdx - self._LineRec[TempStartLine].StartIdx + 1  
	local TempEleCount = -EleCount
	while (LeftEleCount < TempEleCount) do
		SetClearIndex(self, TempStartLine)
		TempEleCount = TempEleCount - LeftEleCount
		TempStartLine = TempStartLine - 1
		LeftEleCount = self._LineRec[TempStartLine].EndIdx - self._LineRec[TempStartLine].StartIdx + 1  
	end  

	self._LineRec[TempStartLine].EndIdx = self._LineRec[TempStartLine].EndIdx - TempEleCount 
	BeginRefreshLine = TempStartLine
	if self._LineRec[TempStartLine].EndIdx < self._LineRec[TempStartLine].StartIdx then 
		SetClearIndex(self, TempStartLine)
		BeginRefreshLine = TempStartLine - 1
		if BeginRefreshLine <= 0 then
			BeginRefreshLine = 1
		end
	end  

	local LineCount = #(self._LineRec)
	if BeginEditLine < LineCount then 
		for i = BeginEditLine + 1, LineCount do
			if (self._LineRec[i].StartIdx > 0) then 
				self._LineRec[i].StartIdx = self._LineRec[i].StartIdx - (-EleCount)
				self._LineRec[i].EndIdx = self._LineRec[i].EndIdx - (-EleCount)
			end  
		end  
	end

	TryAdjustLayout(self, BeginRefreshLine, self._LineRec[BeginRefreshLine].Segment)
	RefreshDisplay(self, BeginRefreshLine, #(self._LineRec))
end

function clsLightMultiEdit:DoArrangeLine(EleCount)
	if EleCount > 0 then
		self:AddEleAdjustLine(EleCount)
	else
		self:SubEleAdjustLine(EleCount)
	end

	self._CurLine = CalLineByIndex(self, self._InsertIndex)
	local RelaIndex = CalRelaIndex(self, self._CurLine, self._InsertIndex)
	DisplayCursorByIndex(self, self._CurLine, RelaIndex)

	local NewHeight = self:GetEditHeight()
	if NewHeight ~= OldHeight then
		OnEditChangeHeight(self, OldHeight, NewHeight)
	end
end

local function DoInsertString(EditObj, Str)
	Str = EditObj:PreProcInputStr(Str)
	local StringLen = string.len(Str)
	local StartIndex = 1
	local PreInsertIndex = EditObj._InsertIndex
	EditObj._NewLineCharIndexList = {}
	EditObj._TmpCharCnt = EditObj._MaxCharCount
	while StartIndex <= StringLen do
		local Char = string.sub(Str, StartIndex, StartIndex)
		local CharByteCount = UnParsChar(Char)
		Char = string.sub(Str, StartIndex, StartIndex + CharByteCount - 1)
		if Char ~= IgnoreChar then
			if Char == NewLineChar then
				EditObj:DoInsertNewLineChar()
			else
				EditObj:InsertChar(Char, CharByteCount)
			end

			if #(EditObj._ElementRec) >= EditObj._TmpCharCnt then
				break
			end
		end
		StartIndex = StartIndex + CharByteCount
	end

	local AfterInsertIndex = EditObj._InsertIndex
	local EleCount = AfterInsertIndex - PreInsertIndex

	EditObj:DoArrangeLine(EleCount)
end

function clsLightMultiEdit:SetText(Text)
	self:ResetEdit()
	DoInsertString(self, Text)
	self:TryNoticeListener(true)
end

function clsLightMultiEdit:InsertText(Text)
	DoInsertString(self, Text)
	self:TryNoticeListener(true)
end

function clsLightMultiEdit:IsValid()
	return self._BaseContainerObj:IsValid()
end

function clsLightMultiEdit:AppendText(Text)
	self._InsertIndex = #(self._ElementRec)
	DoInsertString(self, Text)
	self:DoReturnProc()
	self:TryNoticeListener(true)
end

function clsLightMultiEdit:AddTextAtTail(Text)
	self._InsertIndex = #(self._ElementRec)
	DoInsertString(self, Text)
	self:TryNoticeListener(true)
end

local function DoDeleteSubElement(EditObj, BeginIndex, EndIndex, BeginLine, EndLine)
	EditObj:DeleteSubElement(BeginIndex, EndIndex)
	local EleCount = EndIndex - BeginIndex + 1

	if BeginLine == EndLine then
		EditObj._LineRec[BeginLine].EndIdx = EditObj._LineRec[BeginLine].EndIdx - EleCount
		if EditObj._LineRec[BeginLine].EndIdx < EditObj._LineRec[BeginLine].StartIdx then
			SetClearIndex(EditObj, BeginLine)
		end
	else
		local TempBeginLine = BeginLine
		local NewSegment = EditObj._LineRec[TempBeginLine].Segment
		for i = BeginLine, EndLine do
			EditObj._LineRec[i].Segment = NewSegment
		end

		while (EditObj._LineRec[TempBeginLine].StartIdx == 0) do
			SetClearIndex(EditObj, TempBeginLine)
			TempBeginLine = TempBeginLine + 1
		end

		EditObj._LineRec[TempBeginLine].EndIdx = BeginIndex - 1
		if EditObj._LineRec[TempBeginLine].EndIdx < EditObj._LineRec[TempBeginLine].StartIdx then
			SetClearIndex(EditObj, TempBeginLine)
		end

		local TempEndLine = EndLine
		while (EditObj._LineRec[TempEndLine].StartIdx == 0) do
			SetClearIndex(EditObj, TempEndLine)
			TempEndLine = TempEndLine - 1
		end

		if TempEndLine > TempBeginLine then
			if EndIndex == EditObj._LineRec[TempEndLine].EndIdx then
				EditObj._LineRec[TempEndLine].StartIdx = -1 
				EditObj._LineRec[TempEndLine].EndIdx = -1 
			else
				EditObj._LineRec[TempEndLine].StartIdx = BeginIndex
				EditObj._LineRec[TempEndLine].EndIdx = EditObj._LineRec[TempEndLine].EndIdx - EleCount
			end
		end

		local ClearBeginLine = TempBeginLine + 1
		local ClearEndLine = TempEndLine - 1
		if ClearEndLine >= ClearBeginLine then
			for i = ClearBeginLine, ClearEndLine do
				EditObj._LineRec[i].StartIdx = -1 
				EditObj._LineRec[i].EndIdx = -1 
			end
		end
	end

	local LineCount = #(EditObj._LineRec)
	if EndLine < LineCount then
		for i = EndLine + 1, LineCount do
			if EditObj._LineRec[i].StartIdx > 0 then
				EditObj._LineRec[i].StartIdx = EditObj._LineRec[i].StartIdx - EleCount 
				EditObj._LineRec[i].EndIdx = EditObj._LineRec[i].EndIdx - EleCount 
			end
		end
	end

	TryAdjustLayout(EditObj, BeginLine, EditObj._LineRec[BeginLine].Segment)
end

local function HasNextNonBlankLine(EditObj, LineIndex)
	return EditObj._LineRec[LineIndex + 1]
end

function clsLightMultiEdit:DoDelete(BeginIndex, EndIndex)
	if BeginIndex <= 0 then
		return
	end

	local OldHeight = self:GetEditHeight()

	local BeginLine = 0
	local EndLine = 0
	if not self._DesLine then
		BeginLine = CalLineByIndex(self, BeginIndex)
		EndLine = CalLineByIndex(self, EndIndex)
	else
		if self._CurLine > self._DesLine then
			BeginLine = self._DesLine
			EndLine = self._CurLine
		else
			BeginLine = self._CurLine
			EndLine = self._DesLine
		end
	end

	local RefreshBeginLine = BeginLine
	DoDeleteSubElement(self, BeginIndex, EndIndex, BeginLine, EndLine)

	RefreshDisplay(self, RefreshBeginLine, #(self._LineRec))

	self._InsertIndex = BeginIndex - 1
	self._CurLine = CalLineByIndex(self, self._InsertIndex)
	if self._InsertIndex == self._LineRec[self._CurLine].EndIdx and self._LineRec[self._CurLine + 1] then
		self._CurLine = BeginLine 
	end

	local RelaIndex = CalRelaIndex(self, self._CurLine, self._InsertIndex)
	DisplayCursorByIndex(self, self._CurLine, RelaIndex)

	if #(self._ElementRec) <= 0 then
		self:ResetEdit()
	end

	ClearSelection(self)

	local NewHeight = self:GetEditHeight()
	if NewHeight ~= OldHeight then
		OnEditChangeHeight(self, OldHeight, NewHeight)
	end
end

function clsLightMultiEdit:DoBackProc()
	if self._LMouseBtnDown then
		return
	end

	local CurLine = self._CurLine
	local RelaIndex = CalRelaIndex(self, CurLine, self._InsertIndex)
	local BeginSel, _ = CalSelTextIndexRange(self)
	if not BeginSel and RelaIndex <= 0 and self._LineRec[CurLine - 1] and self._LineRec[CurLine].Segment ~= self._LineRec[CurLine - 1].Segment then
		local OldHeight = self:GetEditHeight()
		if self._LineRec[CurLine].StartIdx == 0 then
			self:DeleteLine(CurLine)
			RefreshDisplay(self, CurLine, #(self._LineRec))
		else
			local OldSegment = self._LineRec[CurLine].Segment
			local NewSegment = self._LineRec[CurLine - 1].Segment
			MergeSegment(self, CurLine, OldSegment, NewSegment)
			TryAdjustLayout(self, CurLine - 1, NewSegment)
			RefreshDisplay(self, CurLine - 1, #(self._LineRec))
		end

		self._CurLine = self._CurLine - 1
		local RelaIndex = CalRelaIndex(self, self._CurLine, self._InsertIndex)
		DisplayCursorByIndex(self, self._CurLine, RelaIndex)

		local NewHeight = self:GetEditHeight()
		if NewHeight ~= OldHeight then
			OnEditChangeHeight(self, OldHeight, NewHeight)
		end
	else
		local BeginIndex, EndIndex = GetDeleteRange(self)
		self:DoDelete(BeginIndex, EndIndex)
	end
end

local function EditInsertStr(EditObj, Str)
	if HasSelText(EditObj) then
		local BeginIndex, EndIndex = GetDeleteRange(EditObj)
		EditObj:DoDelete(BeginIndex, EndIndex)
	end

	DoInsertString(EditObj, Str)
end

function clsLightMultiEdit:DoTryPasteProc()
	if IsCtrlDown() then
		if (#(self._ElementRec) >= (self._MaxCharCount)) then
			return
		end

		local PasteText = win32.GetClipboard()
		if not PasteText then
			return
		end
		EditInsertStr(self, PasteText)
	end
end

local function DoBrush(EditObj, LineIndex, PosX, PosY, Width, Height)
	if EditObj._SelectBoxList[LineIndex] then
		EditObj._SelectBoxList[LineIndex]:SetPos(PosX, PosY)
		EditObj._SelectBoxList[LineIndex]:SetSize(Width, Height)
		return
	end

	local TempSelectBox = UI_BOX.clsLightBox:NewObj(EditObj._TextMsgHandler, PosX, PosY, Width, Height)
	TempSelectBox:SetAlpha(SelectAlpha)
	TempSelectBox:SetZ(SelectZ)

	EditObj._SelectBoxList[LineIndex] = TempSelectBox
end

function clsLightMultiEdit:GetLineHeight(LineIndex)
	return self._LineHeight
end

local function BrushLine(EditObj, LineIndex, BeginAbsIndex, EndAbsIndex)
	local BeginRelaIndex = CalRelaIndex(EditObj, LineIndex, BeginAbsIndex)
	local BeginPosX, BeginPosY = CalPosByIndex(EditObj, LineIndex, BeginRelaIndex)
	local EndRelaIndex = CalRelaIndex(EditObj, LineIndex, EndAbsIndex)
	local EndPosX, EndPosY = CalPosByIndex(EditObj, LineIndex, EndRelaIndex)

	DoBrush(EditObj, LineIndex, BeginPosX, BeginPosY, EndPosX - BeginPosX, EditObj:GetLineHeight(LineIndex))
end

local function TryClearRedundantBrush(EditObj, BeginLine, EndLine)
	local i = BeginLine - 1
	while EditObj._SelectBoxList[i] do
		EditObj._SelectBoxList[i]:Release()
		EditObj._SelectBoxList[i] = nil
		i = i - 1
	end

	i = EndLine + 1
	while EditObj._SelectBoxList[i] do
		EditObj._SelectBoxList[i]:Release()
		EditObj._SelectBoxList[i] = nil
		i = i + 1
	end
end

local function BrushBetweenIndex(EditObj, Index1, Line1, Index2, Line2)
	local BeginIndex = Index1 
	local BeginLine = Line1
	local EndIndex = Index2 
	local EndLine = Line2
	if Index1 > Index2 then
		BeginIndex = Index2
		BeginLine = Line2
		EndIndex = Index1
		EndLine = Line1
	end

	TryClearRedundantBrush(EditObj, BeginLine, EndLine)

	if BeginLine == EndLine then
		BrushLine(EditObj, BeginLine, BeginIndex, EndIndex)
		return
	else
		BrushLine(EditObj, BeginLine, BeginIndex, EditObj._LineRec[BeginLine].EndIdx)
		BrushLine(EditObj, EndLine, EditObj._LineRec[EndLine].StartIdx - 1, EndIndex)
	end

	local TempStartLine = BeginLine + 1
	local TempEndLine = EndLine - 1
	if TempEndLine >= TempStartLine then
		for i = TempStartLine, TempEndLine do
			BrushLine(EditObj, i, EditObj._LineRec[i].StartIdx - 1, EditObj._LineRec[i].EndIdx)
		end
	end
end

function clsLightMultiEdit:TrySelectAll()
	if not IsCtrlDown() then
		return
	end

	if #(self._ElementRec) <= 0 then
		return
	end

	self._DesIndex = 0
	self._DesLine = 1
	self._InsertIndex = #(self._ElementRec)
	self._CurLine = CalLineByIndex(self, self._InsertIndex)
	self._StartIndex = self._InsertIndex
	self._StartLine = self._CurLine

	local RelaIndex = CalRelaIndex(self, self._CurLine, self._InsertIndex)
	DisplayCursorByIndex(self, self._CurLine, RelaIndex)

	BrushBetweenIndex(self, self._DesIndex, self._DesLine, self._StartIndex, self._StartLine)
end

local function DoBackToStart(EditObj)
	EditObj._InsertIndex = 0
	EditObj._CurLine = 1

	DisplayCursorByIndex(EditObj, 1, 0)
end

function clsLightMultiEdit:EditBackToStart()
	if #(self._ElementRec) <= 0 then
		return
	end

	ClearSelection(self)
	DoBackToStart(self)
end

function clsLightMultiEdit:EditDeleteAfter() -- ??? 支持删行
	--[[if self._LMouseBtnDown then
		return
	end

	ClearSelection(self)

	local BeginIndex = self._InsertIndex + 1
	if BeginIndex > #(self._ElementRec) then
		return
	end

	self:DoDelete(BeginIndex, BeginIndex)]]
end

local function InheritBackProc(EditObj) -- 为了索引到继承接口
	if EditObj._DisableInput then
		return
	end

	EditObj:DoBackProc()
	EditObj:TryNoticeListener(true)
end

local function InheritLeftProc(EditObj)
	EditObj:DoLeftProc()
	EditObj:TryNoticeListener(false)
end

local function InheritRightProc(EditObj)
	EditObj:DoRightProc()
	EditObj:TryNoticeListener(false)
end

local function InheritTryPasteProc(EditObj)
	if EditObj._DisableInput then
		return
	end

	EditObj:DoTryPasteProc()
	EditObj:TryNoticeListener(true)
end

local function InheritEditBackToStart(EditObj)
	EditObj:EditBackToStart()
	EditObj:TryNoticeListener(false)
end

local function InheritEditDeleteAfter(EditObj)
	if EditObj._DisableInput then
		return
	end

	EditObj:EditDeleteAfter()
	EditObj:TryNoticeListener(true)
end

local function InheritTrySelectAll(EditObj)
	EditObj:TrySelectAll()
	EditObj:TryNoticeListener(false)
end

function clsLightMultiEdit:OnReturnNotice()
end

local function InheritReturnProc(EditObj)
	if IsCtrlDown() then
		EditObj:TryReturnProc()
		EditObj:TryNoticeListener(true)
	else
		EditObj:OnReturnNotice()
	end
end

function clsLightMultiEdit:EscProc()
end

local function ESCProc(EditObj)
	EditObj:EscProc()
end

local CtrlKeyFuncList = {
	LEFT = InheritLeftProc,
	RIGHT = InheritRightProc,
	BACK = InheritBackProc,
	C = TryCopyProc,
	V = InheritTryPasteProc,
	A = InheritTrySelectAll,
	HOME = InheritEditBackToStart,
	DELETE = InheritEditDeleteAfter,
	RETURN = InheritReturnProc,
	ESCAPE = ESCProc,
}

local function DownEvent(HotKey, Status, Alt)
	return CtrlKeyFuncList[HotKey] and Status == "down"
end

local MinKeyCode = 31
local function DoNormalKeyProc(EditObj, Char)
	if Char <= MinKeyCode then
		return
	end

	if (#(EditObj._ElementRec) >= (EditObj._MaxCharCount)) then
		return
	end

	if (EditObj._DisableInput) then
		return
	end

	local CharStr, _ = ParsChar(Char)
	EditInsertStr(EditObj, CharStr)

	EditObj:TryNoticeListener(true)
end

function clsLightMultiEdit:CalLineByMouseY(MouseY)
	local TempLine = math.ceil(MouseY / (self._LineHeight + self._LineInterval))
	if TempLine <= 0 then
		TempLine = 1
	end

	while (not self._LineRec[TempLine]) do
		TempLine = TempLine - 1
	end

	return TempLine 
end

function clsLightMultiEdit:DisableInput()
	self._DisableInput = true
end

local function CalAbsIndex(EditObj, LineIndex, RelaIndex)
	return EditObj._LineRec[LineIndex].StartIdx + RelaIndex - 1
end

local function CalIndexByMousePos(EditObj, MouseX, MouseY)
	local LineIndex = EditObj:CalLineByMouseY(MouseY)
	if EditObj._LineRec[LineIndex].StartIdx == 0 then
		local AbsIndex = 0
		local TempLineIndex = LineIndex - 1
		while (TempLineIndex >= 1) do
			if EditObj._LineRec[TempLineIndex].EndIdx > 0 then
				AbsIndex = EditObj._LineRec[TempLineIndex].EndIdx
				break
			end

			TempLineIndex = TempLineIndex - 1
		end

		return LineIndex, 0, AbsIndex 
	end

	local RelaIndex = CalRelaIndexByMouseX(EditObj, LineIndex, MouseX)

	return LineIndex, RelaIndex, CalAbsIndex(EditObj, LineIndex, RelaIndex)
end

local function LocateCursorPos(EditObj, MouseX, MouseY)
	local LineIndex, RelaIndex, AbsIndex = CalIndexByMousePos(EditObj, MouseX, MouseY)

	EditObj._StartIndex = AbsIndex
	EditObj._StartLine = LineIndex

	if not EditObj._DisableInput then
		EditObj._InsertIndex = AbsIndex
		EditObj._CurLine = LineIndex

		DisplayCursorByIndex(EditObj, LineIndex, RelaIndex)
	end
end

local function MouseDownMoveProc(EditObj, MouseX, MouseY)
	if not EditObj._DesIndex then
		EditObj._DesIndex = EditObj._StartIndex
		EditObj._DesLine = EditObj._StartLine
	end

	LocateCursorPos(EditObj, MouseX, MouseY)
	BrushBetweenIndex(EditObj, EditObj._DesIndex, EditObj._DesLine, EditObj._StartIndex, EditObj._StartLine)
end

function clsLightMultiEdit:OnTextMouseMove(MouseX, MouseY)
	self._MouseMoveX = MouseX
	self._MouseMoveY = MouseY
	if self._LMouseBtnDown then
		MouseDownMoveProc(self, MouseX, MouseY)
	end
end

function clsLightMultiEdit:ShowFocus()
	ClearSelection(self)

	import("base/ui/ubase.lua").SetActiveEditObj(self)

	ShowCursor(self)
	DisplayCursorByIndex(self, self._CurLine, self._InsertIndex)

	local function OnHotKey(HotKey, Status, Alt)
		if DownEvent(HotKey, Status, Alt) then
			CtrlKeyFuncList[HotKey](self)
		end
	end

	local function OnNormalKey(Char)
		DoNormalKeyProc(self, Char)
	end

	set_global_edit(OnNormalKey, OnHotKey)
end

function clsLightMultiEdit:OnTextLeftDown(MouseX, MouseY)
	ClearSelection(self)

	self._LMouseBtnDown = true
	import("base/ui/ubase.lua").SetActiveEditObj(self)

	if (not self._DisableInput) then
		ShowCursor(self)
	end

	LocateCursorPos(self, MouseX, MouseY)

	local function OnHotKey(HotKey, Status, Alt)
		if DownEvent(HotKey, Status, Alt) then
			CtrlKeyFuncList[HotKey](self)
		end
	end

	local function OnNormalKey(Char)
		DoNormalKeyProc(self, Char)
	end

	set_global_edit(OnNormalKey, OnHotKey)

	if win32.GetClipboard() then
		self:OnLeftDownHasPaste()
	end
end

function clsLightMultiEdit:RegisterViewEvent(ListenerObj, Func) -- 为滚动控件提供的注册监听事件接口
	self._ViewEventListener = ListenerObj
	self._ViewEventFunc = Func
end

local function ClearScrollInfo(EditObj)
	EditObj._LMouseBtnDown = false
end

function clsLightMultiEdit:OnTextLeftUp(MouseX, MouseY)
	ClearScrollInfo(self)
	if HasSelText(self) then
		self:OnLeftUpHasSel()
	end
end

function clsLightMultiEdit:OnLeftUpHasSel()
end

function clsLightMultiEdit:OnLeftDownHasPaste()
end

function clsLightMultiEdit:GenCursorHint(HintText, OffX, OffY)
	self._TextCursor:GenHint(HintText, OffX, OffY)
end

function clsLightMultiEdit:OnTextMouseOut(MouseX, MouseY)
	ClearScrollInfo(self)
end

function clsLightMultiEdit:GetClickFocus()
end

function clsLightMultiEdit:LostClickFocus()
end

function clsLightMultiEdit:SetHandleClickFocus(HandleFlag)
	self._TextMsgHandler:SetHandleClickFocus(HandleFlag)
end

function clsLightMultiEdit:Release()
	self._BaseContainerObj:Release()
end

local MyText = nil
function TestFunc()
	local TextInfo = {}
	TextInfo.Width = 100 
	TextInfo.MaxCharCount = 100
	TextInfo.MaxTextWidth = 100 
	TextInfo.LineInterval = 3
	TextInfo.MinMsgHeight = 70
	TextInfo.Font = FONT_INFO["中标题"]

	MyText = clsLightMultiEdit:NewObj(nil, 100, 100, TextInfo)
end

function Destroy()
	MyText:Release()
end

