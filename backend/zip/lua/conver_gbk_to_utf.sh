#!/bin/sh

real_path() {
	perl -e 'use Cwd "abs_path";print abs_path(shift)' $1
}


SHELL_DIR=`real_path $(dirname $0)/..`
ROOT_DIR="$SHELL_DIR"

try_convert() {
	filename=$1
	echo $filename
	iconv -f GBK -t "UTF-8" "$filename" > "$filename""1"
	#mv "test_lua.lua" "$filename"
}

for f in `find . -name "*.lua" `; do 
	try_convert $f
done

