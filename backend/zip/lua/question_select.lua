-- question_select

QUESTION_SELECT_LAYOUT = Import("layout/question_select.lua")
local QuestionConfig = QUESTION_SELECT_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local hobbyPage = nil

local USER_PAGE = Import("lua/user_info.lua") 
--local SELECT_PAGE = Import("lua/select.lua")

local function onQuestionAnswer(data)
	showMessage( hobbyPage, data.memo )
end

local row_desc_info = {
	item_width = 115,
	item_height = 50,
	column_cnt = 3,
	x_space = 30,
	y_space = 30,
}

-- 问题数据
local question_data = nil
local max_select = 1
local alread_select = 0
local selectBtn = nil

local function singleChooseHandler( clickBtn )
	if selectBtn then
		selectBtn:setStatus( LIGHT_UI.NORMAL_STATUS )
	end
	clickBtn:setStatus( LIGHT_UI.DOWN_STATUS )
	selectBtn = clickBtn
	alread_select = 1
end

local function toggleStatus(btn, x, y)
	--[[if btn._cur_status == LIGHT_UI.DOWN_STATUS then
		btn:setStatus( LIGHT_UI.NORMAL_STATUS )	
		alread_select = alread_select - 1
	else
		if alread_select < max_select then
			alread_select = alread_select + 1
			btn:setStatus( LIGHT_UI.DOWN_STATUS )	
		end
	end--]]
	if btn.currentStatus == 1 then
		if alread_select < max_select then
			btn.currentStatus = 2
			btn:setStatus( LIGHT_UI.DOWN_STATUS )
			alread_select = alread_select + 1
		else
			btn:setStatus( LIGHT_UI.NORMAL_STATUS )
		end	
	else
		btn.currentStatus = 1
		btn:setStatus( LIGHT_UI.NORMAL_STATUS )
		alread_select = alread_select - 1
	end
end	

local function createItem( text, selected )
	local config = {                     --选项宽高调整
		sx = 120,
		sy = 50,
		css = 'checkbox',
		text = '',
		text_css = 'b2',
	}
	local btn
	if question_data.type == 1 then
		btn = createCustom9Button( nil, config, singleChooseHandler )
	else
		btn = createCustom9Button( nil, config, toggleStatus )
	end
	btn:setString( text )
	btn.text = text		
	btn.currentStatus = 1
	
	if selected then
		selectBtn = btn
		toggleStatus( btn )
	end		
	
	return btn
end

local function createHobbyPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = QuestionConfig, nil
	local o = nil

	local function on_back(btn)
		--USER_PAGE.showUserInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		if PANEL_CONTAINER.closeChildPanel( nil, 16 ) then	--随意传个非空的table，该参数如果为空，就表明需要清除子面板，否则，就会考虑第2个参数							
			PANEL_CONTAINER.addChild( USER_INFO_PANEL.showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
		end
		closeHobbyPagePanel()
		ok_func( "back" )
	end

	ret_panel._onBack = on_back

	ret_panel._desc_txt = createMultiLabel( ret_panel, c.content )
	
	local function onOk(obj, x, y)
		local txt = {}
		for _,v in ipairs( hobbyPage._btn ) do
			if v._cur_status == LIGHT_UI.DOWN_STATUS then
				table.insert( txt, v.text )
			end
		end
		
		txt = table.concat( txt,  ',' )
		if PANEL_CONTAINER.closeChildPanel( nil, 16 ) then	--随意传个非空的table，该参数如果为空，就表明需要清除子面板，否则，就会考虑第2个参数							
			PANEL_CONTAINER.addChild( USER_INFO_PANEL.showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
		end
		--USER_PAGE.showUserInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		closeHobbyPagePanel()
		ok_func( txt )
	end
	
	createButton( ret_panel, c.btn, onOk )

	ret_panel._sel_row = LIGHT_UI.clsRowLayout:New(ret_panel, 37, 650, row_desc_info)

	return ret_panel
end

function init( data )
	question_data = data.data
	ok_func = data.ok_func
	if question_data.max_select then max_select = question_data.max_select end
	alread_select = 0
end

--[[
获取选中状态
参数：
	key：当前项内容
	keys：所有项内容
返回值：true|false，表示选中与否
]]
local function getSelected( key, keys )
	for _,v in ipairs( keys ) do
		if key == v then
			return true
		end
	end
	
	return false
end

local function configByData()
	hobbyPage._head_text:setString( question_data.title )
	hobbyPage._desc_txt:setString( question_data.content )
	
	local row_page = hobbyPage._sel_row
	row_page:clearAllItem()
	
	local default = question_data.default
	hobbyPage._btn = {}
	for _, text in pairs(question_data.option) do
		local selected = getSelected( text, default )
		local item = createItem(text, selected)
		
		table.insert( hobbyPage._btn, item )
		row_page:append_item(item)
	end
	
	row_page:refresh_view()
end

function showQuestionPagePanel(parent, x, y)
	if not hobbyPage then
		hobbyPage = createHobbyPagePanel(parent, x, y)
	else
		hobbyPage:setVisible(true)
	end

	configByData()
end

function closeHobbyPagePanel()
	--hobbyPage:setVisible(false)
	clear({hobbyPage})
	hobbyPage = nil
end

