-- item page
ITEM_LAYOUT = Import("layout/item_detail.lua")
local ItemConfig = ITEM_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
local itemPage = nil
local item_info_list = {}
local BANK_PAGE = Import("lua/mall_bank.lua")
local target_uid = 0
local createBottom

local item_desc_info = {
	item_width = 480,
	item_height = 140,
	column_cnt = 1,
	x_space = 0,
	y_space = 0,
}

local PAY_PAGE = Import("lua/pay.lua")

local function createMoveBigPage(parent, x, y)
	local c, c2 = ItemConfig, nil
	local o = nil
	
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = ItemConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_big_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config["view_height"])
	ret_panel._move_big_grp:setVMovable(true)

	local detail_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, item_desc_info)
	detail_page._use_dynamic_size = true

	local top_node_obj = LIGHT_UI.clsNode:New(nil, 0, 0)
	
	top_node_obj._bg = create9Sprite( top_node_obj,c.bg )	
	--top_node_obj._item_pic = createHTTPSprite(top_node_obj, c.logo, nil)
	top_node_obj._item_pic = createSprite(top_node_obj, c.logo)
	top_node_obj._item_name = createLabel(top_node_obj,c.name)
	top_node_obj._money_tag = createSprite(top_node_obj,c.money)
	top_node_obj._price_txt = createLabel(top_node_obj,c.unit_price)	
	
	
		
	local function get_top_size()
		return {width= 480, height=85,}
	end
	top_node_obj.getContentSize = get_top_size
	detail_page:append_item(top_node_obj)
	ret_panel._top_node_obj = top_node_obj


	local mid_node_obj = LIGHT_UI.clsNode:New(nil, 0, 0)
	mid_node_obj._detail_txt = createMultiLabel( mid_node_obj, c.detail )	
	
	local function get_detail_size()
		local size = mid_node_obj._detail_txt:getContentSize()
		return {width = 480, height = size.height+50, }
	end
	mid_node_obj.getContentSize = get_detail_size
	
	detail_page:append_item(mid_node_obj)
	ret_panel._mid_node_obj = mid_node_obj 
		
	detail_page:refresh_view()

	detail_page:setPosition(0, config["inner_y"])
	ret_panel._move_big_grp:appendItem(detail_page)
	ret_panel._move_big_grp:selectPage(1)

	ret_panel._detail_page = detail_page 

	local function get_y_limit(obj)
		return config["inner_y"] 
	end

	detail_page.getYLimit = get_y_limit
	
	return ret_panel
end

local MALL_PAGE = Import("lua/mall.lua")

local function createItemPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = RegConfig, nil
	local o = nil

	local function on_back(btn)
		
		if itemPage.back_mall_fun then
			itemPage.back_mall_fun( itemPage.mall_back, target_uid )
		else
			if PANEL_CONTAINER.closeChildPanel( nil, 6 ) then
				PANEL_CONTAINER.addChild( MALL_PAGE.showMallPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			end
		end
		closeItemPagePanel()
		--MALL_PAGE.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, nil, target_uid)
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '详情' )
	
	config = ItemConfig["move_page_config"]
	ret_panel._move_big_page = createMoveBigPage(ret_panel, config["x"], config["y"])

	return ret_panel
end

local function configByData(item_id)
			
	local item_info = item_info_list[item_id]
	itemPage._move_big_page._top_node_obj._item_pic:setImage(item_info.logo)
	itemPage._move_big_page._top_node_obj._item_name:setString(item_info.name)
	
	local img_file = nil
	local kind = tonumber(item_info["kind"])
	if kind == 3 then
		img_file = "rmb.png"
	elseif kind == 4 then
		img_file = "ingot_pic.png"
	elseif kind ==2 then
		--img_file = "gold.png"
		img_file = "ingot_pic.png"
	else
		img_file = "bs.png"
	end
	itemPage._move_big_page._top_node_obj._money_tag:setImage(img_file)
	if kind == 2 then
		itemPage._move_big_page._top_node_obj._price_txt:setString(tostring(item_info.price ))
	else
		itemPage._move_big_page._top_node_obj._price_txt:setString(tostring(item_info.price))
	end
		
	item_info.description = string.gsub( item_info.description, "<\/?[^>]*>",  '\r\n' )
	item_info.description = string.gsub( item_info.description, "&nbsp;", "")		
	itemPage._move_big_page._mid_node_obj._detail_txt:setString(item_info.description)
	local discount	
	local user_info = LOGIN_PANEL.getUserData()	
	if user_info["vip_id"] == "1" and kind ~=5 then
		discount = 0.9				
	elseif user_info["vip_id"] == "2" and kind ~=5 then
		discount = 0.85		
	elseif user_info["vip_id"] == "3" and kind ~=5 then
		discount = 0.8	
	else
		discount = 1
	end		
	createBottom( discount )		
	if user_info["vip_id"] == "1" and kind ~=5 then
		discount = 0.9		
		ItemConfig.discount_direction.text = "尊敬的vip用户，您已享受" .. discount * 10 .. "折优惠"
		if kind ~= 3 then
			itemPage._move_big_page.vipTxt = createLabel(itemPage._move_big_page._bottom_node_obj,ItemConfig.discount_direction)
		end		
	elseif user_info["vip_id"] == "2" and kind ~=5 then
		discount = 0.85
		ItemConfig.discount_direction.text = "尊敬的vip用户，您已享受" .. discount * 10 .. "折优惠"
		if kind ~= 3 then
			itemPage._move_big_page.vipTxt = createLabel(itemPage._move_big_page._bottom_node_obj,ItemConfig.discount_direction)
		end		
	elseif user_info["vip_id"] == "3" and kind ~=5 then
		discount = 0.8
		ItemConfig.discount_direction.text = "尊敬的vip用户，您已享受" .. discount * 10 .. "折优惠"
		if kind ~= 3 then
			itemPage._move_big_page.vipTxt = createLabel(itemPage._move_big_page._bottom_node_obj,ItemConfig.discount_direction)
		end		
	else
		discount = 1
	end	
	
	
	--兑换提示
	if kind == 5 then	
		ItemConfig.let_yb_desc.text = "你拥有宝石："
		itemPage._move_big_page.letTxt = createLabel(itemPage._move_big_page._bottom_node_obj,ItemConfig.let_yb_desc)
		local function onMsg( data )	
		ItemConfig.let_yb.text = data.data.stone
		itemPage._move_big_page.letYbTxt = createLabel(itemPage._move_big_page._bottom_node_obj,ItemConfig.let_yb)
		end	
		doCommand( 'user', 'money', {}, onMsg )
	end
		
	
	if kind ~= 3 and kind ~= 5 then
		--购买道具提示信息	
		ItemConfig.let_yb_desc.text = "你拥有元宝："
		itemPage._move_big_page.letTxt = createLabel(itemPage._move_big_page._bottom_node_obj,ItemConfig.let_yb_desc)
		local function onMsg( data )	
		ItemConfig.let_yb.text = data.data.bullion
		itemPage._move_big_page.letYbTxt = createLabel(itemPage._move_big_page._bottom_node_obj,ItemConfig.let_yb)
		end		
		ItemConfig.warm_prompt.text = "温馨提示：成为VIP用户购买元宝道具会有额外的折扣喔!"	
		itemPage._move_big_page.warmTxt = createLabel(itemPage._move_big_page._bottom_node_obj,ItemConfig.warm_prompt)
		doCommand( 'user', 'money', {}, onMsg )
	end
	
	
	
	if kind == 3 then
		itemPage._move_big_page._bottom_node_obj._price_txt:setString(item_info.price )
	else
		itemPage._move_big_page._bottom_node_obj._price_txt:setString(tostring( item_info.price * discount ) )
	end
	
	--重新计算位置
	local visibleSize = CCDirector:sharedDirector():getVisibleSize()
	local c = ItemConfig
	--local h  = itemPage._move_big_page._top_node_obj:getContentSize()
	local bg_height = itemPage._move_big_page._top_node_obj:getContentSize().height + 
		              itemPage._move_big_page._mid_node_obj:getContentSize().height
	itemPage._move_big_page._top_node_obj._bg:setContentSize(c.bg.sx,bg_height)
	local bottom_height = visibleSize.height - bg_height
	--local orgX,orgY = itemPage._move_big_page._bottom_node_obj:getPosition()
	--itemPage._move_big_page._bottom_node_obj:setPosition(orgX,orgY + bottom_height)
	
end

function showItemPagePanel(parent, x, y, item_id, target_uid2, backMallFun, mallBack)
	
	target_uid = target_uid2
	if not itemPage then
		itemPage = createItemPagePanel(parent, x, y)
	else
		itemPage:setVisible(true)
	end
	
	if backMallFun then
		itemPage.back_mall_fun = backMallFun
	end
	if mallBack then
		itemPage.mall_back = mallBack
	end
	
	itemPage._item_id = item_id

	--[[if item_info_list[item_id] then
		configByData(item_id)
	else--]]
		local function on_get_detail(data)
			--if not item_info_list[item_id] then
				item_info_list[item_id] = data	
				configByData(item_id)				
			--end				
		end

		doCommand( 'props', 'detail', { id = item_id }, on_get_detail )
	--end
end

function closeItemPagePanel()
	clear({itemPage})
	itemPage = nil
	--itemPage:setVisible(false)
end

function createBottom( discount )
	local ret_panel = itemPage._move_big_page
	local c = ItemConfig
	local bottom_node_obj = LIGHT_UI.clsNode:New(nil, 0, 0)
	local function on_dec_click(obj, x, y)
		local item_id = itemPage._item_id
		local item_info = item_info_list[item_id]
		local cnt = tonumber(bottom_node_obj._cnt_txt:getString())
		cnt = cnt - 1
		if cnt < 1 then
			cnt = 1
		end
		bottom_node_obj._cnt_txt:setString(tostring(cnt))
		local sum_price = tonumber(item_info.price) * cnt
		if tonumber(item_info.kind) == 3 then
			bottom_node_obj._price_txt:setString(tostring(sum_price))
		else
			bottom_node_obj._price_txt:setString(tostring(sum_price * discount))
		end
	end
	
	bottom_node_obj._dec_btn = create9Button( bottom_node_obj, c.dec_btn, on_dec_click )
		
	bottom_node_obj._cnt_txt = createLabel(bottom_node_obj,c.count)
	
	local function on_inc_click(obj, x, y)
		local item_id = itemPage._item_id
		local item_info = item_info_list[item_id]
		
		print(bottom_node_obj._cnt_txt:getString())
		
		local cnt = tonumber(bottom_node_obj._cnt_txt:getString())
		cnt = cnt + 1
		bottom_node_obj._cnt_txt:setString(tostring(cnt))
		local sum_price = tonumber(item_info.price) * cnt
		if tonumber(item_info.kind) == 3  then
			bottom_node_obj._price_txt:setString(tostring(sum_price))
		else
			bottom_node_obj._price_txt:setString(tostring(sum_price * discount))
		end
	end
	
	bottom_node_obj._inc_btn = create9Button( bottom_node_obj, c.inc_btn, on_inc_click )
	
	bottom_node_obj._desc_txt = createLabel(bottom_node_obj,c.desc)
		
	bottom_node_obj._price_txt = createLabel(bottom_node_obj,c.price)


	--打开支付银行页面
	local function openPayBank(data, canPayByGold )			
		local cnt = tonumber(bottom_node_obj._cnt_txt:getString())
		local item_info = item_info_list[itemPage._item_id]
		local order = {}
		order.name = item_info["name"]
		if canPayByGold then
			order.price = item_info["price"] / 100 * cnt
		else
			order.price = item_info["price"] * cnt
		end
		
		order.count = cnt
		--order.number = data.data --订单号
		order.props = data
		order.logo = item_info["logo"]
		order.target_uid = target_uid
		closeItemPagePanel()
		BANK_PAGE.createBankPanel(HTTP_CLIENT.getRootNode(), 0, 0,order, canPayByGold)			
	end

	local function on_other_pay_cb(data)
		local item_info = item_info_list[itemPage._item_id]
		local kind = tonumber(item_info["kind"])		
		
		local function onMsg( data )	
			if kind == 5 then
				USER_PACKAGE_PANEL.showPackageInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, nil, nil, 1 )					
			else
				ItemConfig.let_yb.text = data.data.bullion
			end					
			itemPage._move_big_page.letYbTxt:setString( ItemConfig.let_yb.text )
			
			if kind == 5 then
				closeItemPagePanel()
			end
						
		end	
		
		if data.code == 1 then
			if kind == 5 then
				showMessage( itemPage, "兑换成功", {ms = 3000,y=300,})
			else
				showMessage( itemPage, data.memo, {ms = 3000,y=300,})
			end			
			if kind ~= 3 then
				doCommand( 'user', 'money', {}, onMsg )
			end
		else
			if kind == 5 then
				showMessage( itemPage, data.memo .. "!", {ms = 3000,y=300,})
			else
				showMessage( itemPage, data.memo .. "，请及时充值！", {ms = 3000,y=300,})
			end			
		end	
	end
	
	
	local function on_buy(btn, x, y)		
		local item_info = item_info_list[itemPage._item_id]
		local kind = tonumber(item_info["kind"])
		
		if kind == 3 or kind == 4 then
			--[[local cnt = tonumber(bottom_node_obj._cnt_txt:getString())
			doCommand( 'props', 'payment_no', 
				{ props = itemPage._item_id, target_uid = target_uid, count = cnt, types = 0 }, openPayBank )--]]
				if kind == 4 then
					openPayBank(itemPage._item_id, true)
				else
					openPayBank(itemPage._item_id)
				end
		else
			local cnt = tonumber(bottom_node_obj._cnt_txt:getString())
			doCommand( 'props', 'buy', 
				{ target_uid = target_uid, count = cnt, props_id = itemPage._item_id }, on_other_pay_cb )
		end									
	end

	if target_uid == GLOBAL.uid then
		local item_id = itemPage._item_id
		local item_info = item_info_list[item_id]
		if tonumber(item_info.kind) == 5 then
			c.buy_btn.text = '兑换'
		else
			c.buy_btn.text = '购买'	
		end		
	else
		c.buy_btn.text = '赠送'
	end

	bottom_node_obj._buy_btn = create9Button( bottom_node_obj, c.buy_btn, on_buy )
	
	local function get_bottom_size()
		return {width = 480, height = 200,}
	end
	bottom_node_obj.getContentSize = get_bottom_size
	ret_panel._detail_page:append_item(bottom_node_obj)
	ret_panel._bottom_node_obj = bottom_node_obj 

	ret_panel._detail_page:refresh_view()
end