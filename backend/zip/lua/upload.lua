-- upload page

UPLOAD_LAYOUT = Import("layout/upload.lua")
local UploadConfig = UPLOAD_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local uploadPage = nil

local USER_PAGE = Import("lua/user_info.lua")

local USER_ALBUM = Import("lua/user_album.lua")

local photo_real_path = nil 
local delay_cb = nil

--状态提示
local uploadTips = nil

local function hideTips()
    if uploadTips ~= nil then
       clear({uploadTips})
       uploadTips = nil
    end
end

local function showTips(txt,delay_sec)
    hideTips()
    if uploadTips == nil then
        local d_time = 0
        if delay_sec == 0 then
            d_time = 1000*3600
        else 
            d_time = 1000*delay_sec
        end
        local option = {ms=d_time}
        uploadTips = showMessage(uploadPage,txt,option)
    end
end

--上传回调
local function on_upload_cb(data)
		local result = json.decode(data).code	
		--showTips("上传成功",0)
		--ret_panel.tips:setString("上传成功")
		local function cb()
			--ret_panel._ok_btn:setTouchEnabled(true)
			closeUploadPagePanel()
			USER_ALBUM.showAlbumPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		end
		if tonumber( result ) > 0 then	
			USER_PAGE.refreshData()
			--CCMessageBox("上传成功,3秒后返回到相册",'')			
			showTips("上传成功,3秒后返回到相册",0)
		else
			--CCMessageBox("上传失败,3秒后返回到相册",'')	
			showTips("上传失败,3秒后返回到相册",0)
		end
		
		f_delay_do(uploadPage,cb,nil,3)
				
end

local function delay_dis()
	if delay_cb then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(delay_cb)
		delay_cb = nil	
	end
	local function cb()	
		local size = uploadPage._photo_pic:getContentSize()
		local m_width = 480*0.8
		local m_height = 800*0.8
		local r_w = size.width
		local r_h = size.height
		local scale = 1
		if size.width > m_width or size.height > m_height then		 	
			if size.width/m_width > size.height/m_height then
				scale = m_width/size.width
				r_w = m_width
				r_h = size.height * scale
			else
				scale = m_height/size.height
				r_w = size.width * scale
				r_h = m_height
			end
		end
		uploadPage._photo_pic:setScale(scale)
		uploadPage._photo_pic:setAnchorPoint(0,0)
		local x = (480 - r_w) / 2
		local y = (680 - r_h)/2
		uploadPage._photo_pic:getSprite():setPosition(x,y)
	end
	uploadPage._photo_pic:setImage(photo_real_path,cb,true)	
	--LIGHT_UI.refreshAllHttpSprite()	
end

local function photo_cb(real_path)
	photo_real_path = real_path
	if photo_real_path ~= "" then
	   --[[ local p = string.format("回调路径：%s",photo_real_path)
		showTips(p,0)	--]]	
		delay_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(delay_dis, 0.1, false)
	else
		--showTips("路径为空",0)
		--ret_panel.tips:setString("路径为空")
	end
	
end

local function delay_take()
	xymodule.take_photo(photo_cb)
	CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(delay_cb)
	delay_cb = nil
end

local function delay_sel()
	xymodule.sel_photo(photo_cb)
	CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(delay_cb)
	delay_cb = nil
end

local function createUploadPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = UploadConfig, nil
	local o = nil

	local function on_back(btn)
		closeUploadPagePanel()
		--USER_PAGE.showUserInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		if PANEL_CONTAINER.closeChildPanel( t, 16 ) then	--随意传个非空的table，该参数如果为空，就表明需要清除子面板，否则，就会考虑第2个参数							
			PANEL_CONTAINER.addChild( USER_INFO_PANEL.showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
		end
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '上传照片' )
	
	local function onOK(obj, x, y)
		--hideTips()
		--showTips("图片上传中，请稍候",0)
		--ret_panel.tips:setString("点击按钮")		
		--[[on_upload_cb()
		if uploadTips then		
			return
		end--]]
		--uploadTips:setVisible(true)
		--photo_real_path = "C:/Users/xp001/Desktop/MYXJ_20130826143041_org.jpg"
		if not photo_real_path or photo_real_path == '' then
		    --showTips("路径为空",0)
		    --ret_panel.tips:setString("路径为空")
			return
		end
		
		local url = string.format("http://www.touyou.mobi/interface/album.php")
		local param = {
			["type"] = "upload",
			["sid"] = GLOBAL.sid,
			["desc"] = "",
			["kind"] = "1",
			["file"] = photo_real_path,
		}
		HTTP_REQUEST.http_upload(url, param, on_upload_cb)
		showTips("图片上传中，请稍候",0)
		--ret_panel.tips:setString("图片上传中，请稍候")
		ret_panel._ok_btn:setTouchEnabled(false)
		
	end

	ret_panel._ok_btn = createButton( ret_panel, c.send_btn, onOK )
	
	local function onTakePhoto(btn, x, y)
		delay_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(delay_take, 0.1, false)
	end
		
	ret_panel._take_photo_btn = create9Button( ret_panel, c.take_btn, onTakePhoto )		

	local function onSelPhoto(btn, x, y)
		delay_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(delay_sel, 0.1, false)
	end
	ret_panel._sel_photo_btn = create9Button( ret_panel, c.file_btn, onSelPhoto )

	ret_panel._photo_pic = LIGHT_UI.clsSprite:New(ret_panel, 0, 0, nil)
		
		
	--local tips = LIGHT_UI.clsLabel:New(ret_panel,160,20,"测试一下","Arial",22)
	--tips:setTextColor(0, 0, 0)
    --ret_panel.tips = tips
    
	return ret_panel
end

function showUploadPagePanel(parent, x, y)
	dailyCount('UserInfoAddPicture')--上传照片
	if not uploadPage then
		uploadPage = createUploadPagePanel(parent, x, y)
	else
		uploadPage:setVisible(true)
	end
	--photo_real_path = "C:\\Users\\win7\\Desktop\\btn_zx_1.png"
	--delay_dis()
	--showTips("请选择图片进行上传",0)
end

function closeUploadPagePanel()
	hideTips()
	clear({uploadPage})
	uploadPage = nil
	--uploadPage:setVisible(false)
end

