BIG_LAYOUT = Import("layout/player_big_photo.lua")
local PLAYER_ALBUM_PAGE = Import("lua/player_album.lua")
local PhotoConfig = BIG_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
local info = nil
local pic_info = nil
local bigPage = nil
local big_data = {} 
local score_cnt = 6
local callBack

local function after_get_http_pic(obj)
	--隐藏loading图标，清除显示文字
	hideLoading();
	clear( { bigPage.tip } )
	bigPage.tip = nil	
	
	local c2 = PhotoConfig.big_pic	
	local size = obj:getContentSize()
	
	local m_width = 480*0.74
	local m_height = 800*0.74
	local r_w = size.width
	local r_h = size.height
	local scale = 1
	if size.width > m_width or size.height > m_height then		 	
		if size.width/m_width > size.height/m_height then
			scale = m_width/size.width
			r_w = m_width
			r_h = size.height * scale
		else
			scale = m_height/size.height
			r_w = size.width * scale
			r_h = m_height
		end
	end
		
	--local x, y = get_fit( size.width, size.height, c2.sx, c2.sy )
	
	--obj:setContentSize( x, y )
	if bigPage and bigPage._big_pic then
		bigPage._big_pic:setContentSize( r_w, r_h )
	end

	local pic_x, pic_y = obj:getPosition()	
	bigPage._pic_bg_btn:setPosition( pic_x, pic_y )
	--bigPage._pic_bg_btn:setContentSize( x, y )
	bigPage._pic_bg_btn:setContentSize( r_w, r_h )
	bigPage._pic_bg_btn:setVisible( true )
end

local function on_focus_click(obj, x, y)
	if tonumber(info.frendship) == 0 then
		local function on_add_friend(data)
			info.frendship = 1
			obj:setDownStatus()
			showMessage(bigPage, '成功关注')
		end
		doCommand( 'user', 'addfriend', { friend = bigPage._player_id }, on_add_friend )
	else			
		local function on_del_friend(data)
			info.frendship = 0
			obj:setNormalStatus()
			showMessage(bigPage, '取消关注')
		end
		doCommand( 'user', 'delfriend', { friend = bigPage._player_id }, on_del_friend )
	end
end

local function info_back_func(btn)
--	closeBigPagePanel()
	--PLAYER_INFO_PANEL.showByUserId( bigPage._player_id )
	local PLAYER_ALBUM_PAGE = Import("lua/player_album.lua")
	PLAYER_ALBUM_PAGE.showAlbumPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, bigPage._player_id,callBack)
	closeBigPagePanel()
end

local function on_info_click(obj, x, y)
	--closeBigPagePanel()
	PLAYER_INFO_PANEL.showByUserId( bigPage._player_id )
	closeBigPagePanel()
end

local function on_photo_click(obj, x, y)
	--closeBigPagePanel()
	PLAYER_ALBUM_PAGE.showAlbumPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, bigPage._player_id)
	closeBigPagePanel()
	--	
end

local function show_big_pic()
	bigPage._pic_bg_btn:setVisible( false )
	c2 = PhotoConfig.big_pic
	local count = 0
	for _, temp in ipairs( big_data[bigPage._player_id] ) do
		count = count + 1
		if count == bigPage._cur_sel_page then
			pic_info = temp
			c2.res = temp.url

			if bigPage._big_pic then
				bigPage._big_pic:setVisible( false )
				--bigPage._big_pic:removeFromParentAndCleanup( true )
			end				

			if not bigPage.tip then
				bigPage.tip = showMessageX( bigPage, "请稍候,正在努力为您加载图片...", {y=200} )
			end
			--显示loading图标
			showLoading();
			bigPage._big_pic = createSprite( bigPage, c2, after_get_http_pic )
			local size = bigPage._big_pic:getContentSize()
	
			local m_width = 480*0.74
			local m_height = 800*0.74
			local r_w = size.width
			local r_h = size.height
			local scale = 1
			if size.width > m_width or size.height > m_height then		 	
				if size.width/m_width > size.height/m_height then
					scale = m_width/size.width
					r_w = m_width
					r_h = size.height * scale
				else
					scale = m_height/size.height
					r_w = size.width * scale
					r_h = m_height
				end
			end
			if r_w ~= 0 then
				bigPage._big_pic:setContentSize( r_w, r_h )
			end
			bigPage._cur_sel_page = bigPage._cur_sel_page + 1
			if bigPage._cur_sel_page > table.getn( big_data[bigPage._player_id] ) then bigPage._cur_sel_page = 1 end
			break;
		end
	end
end

local function on_voted_score(data)
	show_big_pic()
end

local function createBigPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = PhotoConfig, nil
	local o = nil

	ret_panel._onBack = info_back_func
	ret_panel._head_text:setString( '相册' )
		
	local function on_score(btn, x, y)
		doCommand( 'album', 'vote_score', { id = pic_info.id, score = btn._score }, on_voted_score )
	end

	createSprite( ret_panel, c.score_bg )

	startx = 65 
	starty = 40
	x_step = 70
	for i = 5, 10 do
		local res = string.format( 'score%s.png', i )
		local on = string.format( 'score%s_on.png', i )
		local o = LIGHT_UI.clsSimpleButton:New(ret_panel, startx, starty, res, on)
		o._score = i
		o.onTouchEnd = on_score
		startx = startx + x_step
	end
	
	return ret_panel
end

local function configByBigData()
	local c, c2 = PhotoConfig, nil
	local node = createNode( bigPage, {} )
	local o = nil

	local function click_photo(obj, x, y)
		show_big_pic()
	end

	bigPage._pic_bg_btn = createButton( bigPage, { x = 0, y = 0, res = 'portrait2.png' }, click_photo )
	bigPage._pic_bg_btn:setVisible( false )
	show_big_pic()
		
	bigPage._link_bg = createSprite( bigPage, c.link_bg )	
	bigPage._photo_btn = createButton( bigPage, c.info_btn, on_info_click )
	bigPage._info_btn = createButton( bigPage, c.photos_btn, on_photo_click )
	bigPage._focus_btn = createButton( bigPage, c.focus_btn, on_focus_click )
	
	if tonumber(info.frendship) == 1 then
		bigPage._focus_btn:setStatus( LIGHT_UI.DOWN_STATUS )
	end
end

local function on_get_big_data(data)
	local player_id = bigPage._player_id

	if data.data.data then
		big_data[player_id] = data.data.data	
		configByBigData()
	else--'基于公平原则，查看视频认证用户照片，您需先进行视频认证（VIP不受此限制）'
		showMultiMessage( bigPage, '基于公平原则，查看视频认证用户照片，您需先进行视频认证（VIP不受此限制）', {4000} )
	end
end

local function request_big_data( data )
	info = data.data
	doCommand( 'album', 'large', { user = bigPage._player_id, page = 1, size = 20, kind = 1 }, on_get_big_data )
end

local function request_player_info()
	doCommand( 'user', 'detail', { id = bigPage._player_id }, request_big_data )
end

function showBigPagePanel( parent, x, y, player_id, sel_page, back_func )
	bigPage = nil
	if not bigPage then
		bigPage = createBigPagePanel( parent, x, y )
	else
		bigPage:setVisible(true)
	end

	bigPage._player_id = player_id
	bigPage._cur_sel_page = sel_page 
	callBack = back_func

	if not big_data[player_id] then
		request_player_info()
	else
		configByBigData()
	end
end

function closeBigPagePanel()
	--bigPage:setVisible(false)
	clear( { bigPage.tip } )
	bigPage.tip = nil
	clear({bigPage})
	bigPage = nil
end

