-- login
LOGIN_LAYOUT = Import("layout/login.lua")
local LoginConfig = LOGIN_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

loginPanel = nil
local createWin = nil
--local crateLogin = nil
local loadingPage = nil
local reConnect = 0 --网络监测次数
--启动时显示动画
local ShowReady = nil
--隐藏动画
local HideReady = nil

local photo_real_path = nil 
local function photo_cb(real_path)
	photo_real_path = real_path
	local a = LIGHT_UI.clsSprite:New(loginPanel, 200, 200, photo_real_path)
	a:setAnchorPoint(0, 0)
end

local function video_cb(real_path)
end

local user_data = nil


function getUserData()
	return user_data
end

function setLogo( logoUrl )
	user_data[ "logo" ] = logoUrl
end

function setHobby( hobbyStr )
	user_data[ "interest" ] = hobbyStr
end

function setCity( cityStr )
	local t = split(cityStr, ",")	
	user_data[ "province" ] = t[ 1 ]
	user_data["city"] = t[ 2 ]
end

function setHeight( heightStr )
	user_data[ "height" ] = heightStr
end

function setBirthday( birthdayStr )
	user_data[ "birthday" ] = birthdayStr
end

function setIncome( incomeStr )
	user_data[ "income" ] = incomeStr
end

function setProfession( professionStr )
	user_data[ "profession" ] = professionStr
end

function setBar( barStr )
	user_data[ "bar" ] = barStr
end

local userNameTxt = nil
local passwordTxt = nil
local loginComplete = true

local function onUserLogon(data)
	if loginPanel and loginPanel.tip then
		clear( {loginPanel.tip } )
		loginPanel.tip = nil
	end
	if data.code <= 0 then
		showMessage( loginPanel, data.memo, {ms="3000"} )
		
		setData( 'login', 'password' )
		loginComplete = true
		return
	else
		user_data = data.data
		
		--[[local city  = user_data[ "logo" ]
		print( city )
		loginPanel.city = LIGHT_UI.clsLabel:New(loginPanel, 100, 100, user_data[ "logo" ], GLOBAL_FONT, 22)--]]			

		GLOBAL.sid = data.sid
		GLOBAL.uid = user_data.id
		GLOBAL.name = user_data.name
		GLOBAL.gold = user_data.gold	
		--print(GLOBAL.gold .. '-'..user_data.gold)	
		local path = WRITE_PATH..'cache/'..tostring(GLOBAL.uid)
		xymodule.create_dir(path)
		setData( 'login', 'username', user_data.name )			
		setData( 'login', 'password', passwordTxt )
		setData( 'login', 'uid',user_data.id)
		--checkUpdate() --检查更新
		closeLoginPanel()
		dailyCount('Login')--登录
		loginComplete = true
		--MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	    NEW_GAME_HALL.showGameHall( HTTP_CLIENT.getRootNode(), 0, 0 )
		PANEL_CONTAINER.showPanelContainer( HTTP_CLIENT.getRootNode(), 0, 0 )
		if PANEL_CONTAINER.closeChildPanel( nil, 9 ) then
			PANEL_CONTAINER.addChild( MAIN_PANEL.createMainPanel( PANEL_CONTAINER.ret_panel.bg, 0, 0 ) )	
		end			
	end
end

local function onUserLogon2( txt )
	local data = json.decode( txt )
	onUserLogon( { code = 1, data = { id = data.code, name = '' }, sid = data.sid } )
end	

function doLogin(username, password)
	--checkVersion()	
	--为了兼容旧版本提示				
	if xymodule.dailycount then
		userNameTxt = username
		passwordTxt = password
		if loginComplete == true then
			loginComplete = false
			doCommand( 'user', 'logon', { name = username, pass = password,version = 102,imei=xymodule.get_imei() }, onUserLogon )
			if loginPanel then
				loginPanel.tip = showMessageX( loginPanel, "正在登录，请稍候...", {y=400} )
			end
		end
	else		
		if loginPanel then	
			loginPanel.tip = showMultiMessage( loginPanel, 
			"本应用已经升级，请到www.touyou.mobi下载最新版本。安装前请先卸载旧版，避免出错。", {y=500} )
		end
	end
end

local function createLoginPanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = LoginConfig, nil
	local o = nil	
	
	hide( {ret_panel._head_back} )
	ret_panel._head_text:setString( '登录' )
	
	createGrid( ret_panel, c.rect )
	
	--logo
	o = createSprite(ret_panel,c.logo)
	
	o = createEdit2( ret_panel, c.username )			
	o:setMaxInputChar(32)
	--o:setString('hello world')
	ret_panel._username_input = o	
		
	o = createEdit2( ret_panel, c.password )		
	o:setMaxInputChar(32)
	o:setPasswordChar("*")
	ret_panel._password_input = o
	
	local function on_register(btn)
		if loginComplete == true then
			closeLoginPanel()
			local REGISTER_PANEL = Import("lua/register.lua")	
			REGISTER_PANEL.showRegisterPanel(HTTP_CLIENT.getRootNode(), 0, 0)
		end
	end
	
	createButton( ret_panel, c.register, on_register )
	
	local function on_getpass(btn)
	end
	
	createButton( ret_panel, c.forgotpass, on_getpass )

	local function on_login_click(btn_obj)	
		
		--playWav('sound/jiang_show.mp3',5000)
		--CCMessageBox('click','')		
		ret_panel._username_input:active(false)
		ret_panel._password_input:active(false)
		local username = ret_panel._username_input:getText()
		local password = ret_panel._password_input:getText()
		doLogin(username, password)		
	end
	
	
	
	create9Button( ret_panel, c.login_btn, on_login_click )

	local function after_login_third( text1 )
		local data = json.decode( text1 )

		if data.platform then
			if data.platform == "sina" then
				local function after_request_sina( text2 )
					local url2 = GLOBAL.interface .. "user.php?type=open_login&kind=1&key=" .. data.uid .. "&token=" .. data.token .. "&data=" .. text2
					HTTP_REQUEST.http_request( url2, onUserLogon2 )
				end

				--local url1 = "https://api.weibo.com/2/users/show.json?" .. "uid=" .. data.uid .. "&access_token=" .. data.token
				--HTTP_REQUEST.http_request( url1, after_request_sina )
				local url1 = GLOBAL.interface .. "user.php?type=open_login&kind=1&key=" .. data.uid .. "&token=" .. data.token
				HTTP_REQUEST.http_request( url1, onUserLogon2 )				
			end
		end
	end
	
	local function on_weibo_delay(data)
		f_delay_do(ret_panel,after_login_third,data,0.1)
	end

	local function on_weibo_click(btn_obj)
		local data = { platform = 'weibo' };
		xymodule.third_login( table2json( data ), on_weibo_delay )
		--after_login_third( '{"uid":"2164005562","token":"2.00Whw83CAiW6nBe84f0eb6edtR6JJB","platform":"sina"}' )
	end

	--create9Button( ret_panel, c.weibo_btn, on_weibo_click )
	--createButton( ret_panel, c.weibo_btn, on_weibo_click )
	
	local o = createLabel(ret_panel,{x=60,y = 20})
	local version = CCUserDefault:sharedUserDefault():getStringForKey("version");
	o:setString(tostring(version))	
	
	return ret_panel
end



--启动时显示动画
function ShowReady()
	HideReady()
	local c = LoginConfig
	--为了兼容旧版本提示
	if xymodule.dailycount then	
		ready = createNode(loadingPage,c.ready_action)
		local o = createSprite( ready, res(0, 0, c["ready_action"].res) )
		ready:setPosition(235,180)	
		a_play(o:getSprite(),c["ready_action"],true)			
	end
end

function HideReady()
	if ready then
		clear({ready})
		ready = nil
	end
end

--进入登录页面时温馨提示
function WramMessage()
	math.randomseed(os.time())
	local number = math.random(1,12)
	warm = {'喝醉会被踢出房间，记得及时解酒哦',
			'想快速解酒？到商城购买解酒药吧',
			'VIP购买道具有折扣优惠哦',
			'开通VIP就可以查看认证用户靓照啦',
			'视频认证成功，即获1000金币',
			'没人理你？上传几张你的靓照试试',
			'看他人靓照记得打打分哦',
			'天天做任务，金币免费拿哟',
			'斋、破斋、抢开可加倍奖励',
			'点斗骰快速找到好友所在的游戏桌',
			'发布说两句，可分享身边趣事',
			'关注骰妖子，有问题可随时找她哦'}
	return warm[number]
end


function showLoginPanel(parent, x, y)
	loadingPage = createNode(parent,point(0,0))
	--loading图
	createSprite(loadingPage,LoginConfig.loading)	
	--版本号	
	local versionname = tostring(VERSION_NAME)	
	createLabel(loadingPage,{x = 137, y = 30, ax = 0, ay = 1, text = '骰友多人版beta ' .. versionname, css='c4'})
	wm = WramMessage()		
	showMessage(loadingPage,wm,{ms = 6000,y = 125})	
	ShowReady()
	math.randomseed(os.time())
	local delay = 1 --math.random(1,20)
	f_delay_do(loadingPage,createWin,parent,tonumber(delay))	
	checkUpdate() --检查更新
end

function createWin(parent)
	HideReady()
	local function ExitProcess()
		xymodule.exit_process()
	end
	if loadingPage then 
		local net = xymodule.check_network()		
		if tonumber(net) == 1 then
			clear({loadingPage})
			loadingPage = nil		
			creatLogin(parent)
			reConnect = 0
		else
			if tonumber(reConnect) < 3 then
				reConnect = reConnect + 1
				f_delay_do(loadingPage,createWin,parent,3)				
			else
				CCMessageBox('网络连接失败,10秒后退出应用','')
				f_delay_do(loadingPage,ExitProcess,parent,3)
			end
		end
	end		
end

function creatLogin(parent)
	loginPanel = createLoginPanel(parent, 0, 0)
	local function on_login_click(btn_obj)			
		loginPanel._username_input:active(false)
		loginPanel._password_input:active(false)
		local username = loginPanel._username_input:getText()
		local password = loginPanel._password_input:getText()
		doLogin(username, password)		
	end
		
	local loginData = getData( 'login' )

	if loginData then
		loginPanel._username_input:setString( loginData.username )
		loginPanel._password_input:setString( loginData.password )
	
		on_login_click()
	end	
end

function checkVersion()
	local function onBack(data)	
		local function download()
			xymodule.android_download('http://www.touyou.mobi/touyou.apk',WRITE_PATH,'touyou.apk')
			CCUserDefault:sharedUserDefault():setStringForKey("engine",data.vercode)
		end
		local function cancel()		
			xymodule.exit_process()
		end
		if data.code >= 1 then
			--local engine = CCUserDefault:sharedUserDefault():getStringForKey("engine")
			local engine = 10			
			if tonumber(engine) < tonumber(data.data.vercode) then
				if tonumber(data.data.need) == 0 then
					alert = showUpdateWindow('发现新版本',data.data.verinfo,download)				
				else
					alert =showUpdateWindow('发现新版本',data.data.verinfo,download,cancel)	
				end
			end	
		end		
	end
	local engine_version = CCUserDefault:sharedUserDefault():getStringForKey("engine")	
	engine_version = 10
	if string.len(tostring(engine_version)) <= 0 then
		engine_version = ENGINE_VERSION
		CCUserDefault:sharedUserDefault():setStringForKey("engine",ENGINE_VERSION)		
	end
	doCommand('system', 'version',{vercode=engine_version},onBack)				
end	

--快速进入
function QuickEnter()
	local username = '游客'..string.sub(os.time())
	local function onAutoRegist(data)
		local path = WRITE_PATH..'cache/'..tostring(GLOBAL.uid)
		xymodule.create_dir(path)
		setData( 'login', 'username', username )			
		setData( 'login', 'password', registerPanel._password_input:getText() )
		LOGIN_PANEL = Import("lua/login.lua")
		LOGIN_PANEL.creatLogin(HTTP_CLIENT.getRootNode())
		closeRegisterPanel()
	end
	
	local loginData = getData( 'login' )

	if loginData then
		loginPanel._username_input:setString( loginData.username )
		loginPanel._password_input:setString( loginData.password )
	
		doLogin(loginData.username, loginData.password)
	else
		
		doCommand( 'user', 'register', { name = username, pass = password, version = 100,
			imei = xymodule.get_imei(), channel = xymodule.getchannel(),sex = sexStr }, onAutoRegist )
	end	
end	

function closeLoginPanel()
	if loginPanel then
		loginPanel._username_input:active(false)
		loginPanel._password_input:active(false)
		LIGHT_UI.clearActEdit()
		loginPanel:setVisible(false)
		clear({loginPanel})		
	end
	loginPanel = nil
end

--更新提示框
function showUpdateWindow(title,content,cb,cancel)
	
	local config = LoginConfig.alert;
	config.title.text = title;	
	local str = "";		
	--config.content3.text = 	string.gsub(str,"<br>","\n")
	local t = split(content,"<br>")
	for k,v in ipairs(t) do
		
		if v ~= "" then
			str = str .. v .. "\n"
		end
	end
	config.content3.text = 	str
	return createAlert(loginPanel,config,cb,cancel);
end