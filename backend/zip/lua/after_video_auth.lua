local page = nil

local function createAfterVideoAuthPagePanel( parent, x, y )
	
	local ret_panel = createBasePanel( parent, x, y )
	
	hide( {ret_panel._head_back} )
	ret_panel._head_text:setString( '视频认证' )

	local config_1 = {
		x = 50,
		y = 500,
		sx = 400,
		sy = 40,
		css = 'c2',
		text = '视频上传需耗费数秒时间，请稍候...所录视频仅供系统审核用，其他任何人不可见',
	}

	local config_2 = {
		x = 250,
		y = 350,
		sx = 360,
		sy = 50,
		ax = 0.5,
		ay = 0.5,
		css = 'blue_btn',
		text = '确定',
		text_css = 'c4',
	}

	local function btn_click( obj, x, y )
		closeAfterVideoAuthPagePanel()
		--USER_INFO_PANEL.showUserInfoPagePanel( HTTP_CLIENT.getRootNode(), 0, 0 )
		if PANEL_CONTAINER.closeChildPanel( nil, 16 ) then	--随意传个非空的table，该参数如果为空，就表明需要清除子面板，否则，就会考虑第2个参数							
			PANEL_CONTAINER.addChild( USER_INFO_PANEL.showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )				
		end
	end

	ret_panel._memo = createMultiLabel( ret_panel, config_1 )
	ret_panel._btn = create9Button( ret_panel, config_2, btn_click )
	ret_panel._btn:setVisible( false )

	return ret_panel
end

local function on_upload_cb( data )
	data = json.decode( data )
	page._memo:setString( '认证视频已上传，并已进入审核队列，由于排队人数多，审核时间可能较长，请耐心等待...温馨提示：升级VIP可加快审核速度' )
	page._btn:setVisible( true )
end

local function uploadPic( pic_path )
	local url = GLOBAL.interface .. "album.php"
	local post_data = {
		["type"] = "upload",
		["sid"] = GLOBAL.sid,
		["desc"] = "",
		["kind"] = "2",
		["file"] = pic_path,
	}

	HTTP_REQUEST.http_upload( url, post_data, on_upload_cb )
end

function showAfterVideoAuthPagePanel( parent, x, y, pic_path )
	
	if not page then
		page = createAfterVideoAuthPagePanel( parent, x, y )
	else
		page:setVisible( true )
	end

	if pic_path then uploadPic( pic_path ) end
end

function closeAfterVideoAuthPagePanel()
	page:setVisible( false )
end