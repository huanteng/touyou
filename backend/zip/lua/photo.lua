-- photo panel

LSCORE_LAYOUT = Import("layout/photo.lua")
local PhotoConfig = LSCORE_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local PhotoPage = nil

local look_data = {}
--[[
frendship
comment
is_real
vote_times
city
comment_times
small
user
id
view_times
vote
name
sex
source
time
large
logo
--]]
-- 图片数据
local info = nil
local canOperate = false
local function on_voted_score(data)
	--print(LIGHT_UI.db_fmt(data))
	showMessage(PhotoPage,"评分成功")
	request_look_data()
end

local PLAYER_ALBUM_PAGE = Import("lua/player_album.lua")
local PLAYER_INFO_PANEL = Import("lua/player_info.lua")
local PHOTO_PAGE = Import("lua/photo.lua")

local function info_back_func()
	PLAYER_INFO_PANEL.closePlayerInfoPagePanel()
	--showPhotoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	if PANEL_CONTAINER.closeChildPanel( nil, 4 ) then
		PANEL_CONTAINER.addChild( showPhotoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
	end
end

local function photo_back_func()
	PLAYER_ALBUM_PAGE.closeAlbumPagePanel()
	--showPhotoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	PANEL_CONTAINER.addChild( showPhotoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
end

local function after_get_http_pic(obj)
	local c2 = PhotoConfig.big_pic
	
	local size = obj:getContentSize()
		
	local x, y = get_fit( size.width, size.height, c2.sx, c2.sy )
	print( x, y )
	
	obj:setContentSize( x, y )
	
	--HideReady()
	canOperate = true
end

local function on_photo_click(obj, x, y)
	if not info then
		return
	end
	local player_id = info.user
	local albumData = PLAYER_INFO_PANEL.getSmallPicInfo(player_id)
	if not albumData then
		local function on_get_small_pic(data)
			PLAYER_INFO_PANEL.getSmallPicData()[player_id] = data.data.data
			closePhotoPagePanel()
			PLAYER_ALBUM_PAGE.showAlbumPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, info.user,photo_back_func)
		end

		doCommand( 'album', 'small', { user = player_id, page = 1, kind = 1 }, on_get_small_pic )
	else
		closePhotoPagePanel()
		PLAYER_ALBUM_PAGE.showAlbumPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, info.user,photo_back_func)
	end
end

local function on_focus_click(obj, x, y)
	if tonumber(info.frendship) == 0 then
		local function on_add_friend(data)
			info.frendship = 1
			obj:setDownStatus()
			showMessage(PhotoPage, '成功关注')
		end
		local player_id = info.user
		doCommand( 'user', 'addfriend', { friend = player_id }, on_add_friend )
	else			
		local function on_del_friend(data)
			info.frendship = 0
			obj:setNormalStatus()
			showMessage(PhotoPage, '取消关注')
		end
		local player_id = info.user
		doCommand( 'user', 'delfriend', { friend = player_id }, on_del_friend )
	end
end
local function on_info_click(obj, x, y)
	local player_id = info.user
	PLAYER_INFO_PANEL.showByUserId(player_id)
	PLAYER_INFO_PANEL.init( { back_func = info_back_func } )
	closePhotoPagePanel()
end

local function createPhotoPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createContainerChildPanel( parent, x, y )

	local c, c2 = PhotoConfig, nil
	local o = nil

	local function on_back(btn)
		closePhotoPagePanel()
		MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	--ret_panel._onBack = on_back
	--ret_panel._head_text:setString( '最新上传' )
		
	local function on_score(btn, x, y)
		--showMessage(PhotoPage,"评分成功")
		
		--盟友看相打分点击事件ID
		if btn._score == 5 then		
			dailyCount('PictureForFive')
		elseif btn._score == 6 then
			dailyCount('PictureForSix')
		elseif btn._score == 7 then
			dailyCount('PictureForSevent')
		elseif btn._score == 8 then
			dailyCount('PictureForEight')
		elseif btn._score == 9 then
			dailyCount('PictureForNight')
		elseif btn._score == 10 then
			dailyCount('PictureForTen')
		end
		
		
		if canOperate == true then
			--HideReady()
			canOperate = false
			doCommand( 'album', 'vote_score', { id = info.id, score = btn._score }, on_voted_score )
			
		end
	end

	createSprite( ret_panel, c.score_bg )

	startx = 65 
	starty = 40
	x_step = 70
	for i = 5, 10 do
		local res = string.format( 'score%s.png', i )
		local on = string.format( 'score%s_on.png', i )
		local o = LIGHT_UI.clsSimpleButton:New(ret_panel, startx, starty, res, on)
		o._score = i
		o.onTouchEnd = on_score
		startx = startx + x_step
	end
	
	c2 = c.big_pic
	
	ret_panel._big_pic = createSprite( ret_panel, c2 )
	
	createSprite( ret_panel, c.info_bg )
	
	ret_panel._info_txt = createLabel( ret_panel, c.info ) --相片对应用户的信息	
	
	ret_panel._sex_pic = createSprite( ret_panel, c.sex )  --性别图片
	
	createSprite( ret_panel, c.link_bg )
	
	createButton( ret_panel, c.info_btn, on_info_click )
	createButton( ret_panel, c.photos_btn, on_photo_click )
	createButton( ret_panel, c.focus_btn, on_focus_click )
	
	ret_panel.closePanel = closePhotoPagePanel
	
	return ret_panel
end

local function configByData()
	if not PhotoPage then
		return
	end
	
	local o = PhotoPage._big_pic
	o._width = nil
	o._height = nil
	o:setImage( info.large, after_get_http_pic )
	
	local info_text = info.name .. '  ' .. info.city
	--local info_text = getCharString(info.name,5) .. '  ' .. info.city
	o = PhotoPage._info_txt
	o:setString( info_text )
	--AdjustString(o,info_text,100)	
	doConfig( o, PhotoConfig.info )
	
	local sex = tonumber(info.sex) == 1 and 'woman.png' or 'man.png'
	o = PhotoPage._sex_pic
	o:setImage( sex )
	doConfig( o, PhotoConfig.sex )
		
	if tonumber(info.friend) == 1 then
		PhotoPage._focus_btn:setStatus( LIGHT_UI.DOWN_STATUS )
	end		
end

local function onAlbumListallnew(data)
	info = data.data[1]
	configByData()
end

function request_look_data()
	--[[if PhotoPage then
		PHOTOCONFIG = Import("layout/photo.lua")
		local pConfig = PHOTOCONFIG.getConfigData()[LIGHT_UI.CurScreenSize]
		ShowReady( PhotoPage, pConfig.ready_action )
	end--]]
	--doCommand( 'album', 'list_all_new', { size = 1 }, onAlbumListallnew, 1 )
	
	doCommand( 'album', 'list_all_new', { size = 1 }, onAlbumListallnew )
	showLoading();
end

function showPhotoPagePanel(parent, x, y)
	dailyCount('PictureForaScore')--看相打分
	canOperate = false
	if not PhotoPage then
		PhotoPage = createPhotoPagePanel(parent, x, y)
	else
		PhotoPage:setVisible(true)
	end

	request_look_data()
	return PhotoPage
end

function closePhotoPagePanel()
	--HideReady()
	if PhotoPage then
		PhotoPage:removeFromParentAndCleanup(true)
	end
	PhotoPage = nil
--	o:setVisible(false)
end

