-- player album page

ALBUM_LAYOUT = Import("layout/player_album.lua")
local AlbumConfig = ALBUM_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local albumPage = nil

local back_func = nil

local spic_item_width = 105
local spic_item_height = 105
local photo_list_desc = {
	item_width = 105,
	item_height = 105,
	column_cnt = 4,
	x_space = 10,
	y_space = 10,
}

local function createAlbumPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = RegConfig, nil
	local o = nil

	local function on_back(btn)
		if back_func then
			back_func()
			return
		end
		PLAYER_INFO_PANEL.showByUserId(albumPage._player_id)
		closeAlbumPagePanel()
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '相册' )
	
	ret_panel._photo_list_txt = LIGHT_UI.clsLabel:New(ret_panel, 20, 720, "", GLOBAL_FONT, 22)
	ret_panel._photo_list_txt:setAnchorPoint(0, 1)
	ret_panel._photo_list_txt:setTextColor(102, 102, 102)

	ret_panel._photo_list = LIGHT_UI.clsRowLayout:New(ret_panel, 15, 680, photo_list_desc)
	
	return ret_panel
end

local PLAYER_BIG_PHOTO_PAGE = Import("lua/player_big_photo.lua")

local function configByAlbumData()

	local pic_c = {
			sx = 105,
			sy = 105,
			ax = 0,
			ay = 1,
			x = 0,
			y = 0,
			res = '',
		}

	local function on_pic_touch_end(msg_obj, x, y)
	--	closeAlbumPagePanel()
		PLAYER_BIG_PHOTO_PAGE.showBigPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, albumPage._player_id, msg_obj._photo_idx, back_func )
		closeAlbumPagePanel()
	end
	
	local albumData = PLAYER_INFO_PANEL.getSmallPicInfo(albumPage._player_id)
	albumPage._photo_list_txt:setString(string.format("相片列表", table.maxn(albumData)))
	albumPage._photo_list:clearAllItem()
	local photo_idx = 1
	for _, photo_info in pairs(albumData) do
		pic_c.res = photo_info["url"]
		local pic_item = createSprite( nil, pic_c )
		albumPage._photo_list:append_item(pic_item)
		pic_item._msg_layer = LIGHT_UI.clsClipLayer:New(pic_item, 0, -spic_item_width)
		pic_item._msg_layer:set_msg_rect(0, 0, spic_item_width, spic_item_height)
		pic_item._msg_layer.onTouchEnd = on_pic_touch_end
		pic_item._msg_layer._photo_info = photo_info
		pic_item._msg_layer._photo_idx = photo_idx
		photo_idx = photo_idx + 1
	end

	albumPage._photo_list:refresh_view()
end

function showAlbumPagePanel(parent, x, y, player_id,back_fun)
	if not albumPage then
		albumPage = createAlbumPagePanel(parent, x, y)
	else
		albumPage:setVisible(true)
	end
	back_func = back_fun
	albumPage._player_id = player_id
	configByAlbumData()
end

function closeAlbumPagePanel()
	--albumPage:setVisible(false)
	clear({albumPage})
	albumPage = nil
end

