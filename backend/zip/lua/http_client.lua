-- http client 
local BeginY = 536 
local item_width = 91
local item_height = 125 
local column_cnt = 3
local y_gap = 45 

local screen_width = 480
local screen_height = 640 

local dice_panel = nil

local login_txt = nil
local login_name = nil

local function on_back(obj, x, y)
	dice_panel._move_player_grp:setVisible(false)
	dice_panel._move_player_grp:setTouchEnabled(false)
	dice_panel._back_btn:setVisible(false)
	dice_panel._move_group:setVisible(true)
	dice_panel._move_group:setTouchEnabled(true)
	login_txt:setVisible(true)
end

local dice_scene = nil
local rootNode = nil
local maskPanel = nil
function getRootNode()
	return rootNode 
end

function resetNode()
	if rootNode then
		clearAllTimer()
		rootNode:removeAllChildrenWithCleanup(true)
	end		
end

function getMaskNode()
	return maskPanel
end

function showMask()
	maskPanel:setVisible(true)
end

function hideMask()
	maskPanel:setVisible(false)
end

local function createDiceScene()
	dice_scene = CCScene:create()
	local nodeObj = LIGHT_UI.clsNode:New(nil, 0, 0)
	rootNode = LIGHT_UI.clsNode:New(nil, 0, 0)
	rootNode:setPosition(0, 0)
	dice_scene:addChild(rootNode:getCOObj())

	maskPanel = LIGHT_UI.clsNode:New(nil, 0, 0)
	maskPanel._maskPic = LIGHT_UI.clsButton:New(maskPanel, 0, 0, "small_mask_bg.png", "small_mask_bg.png")
	maskPanel._maskPic:setAnchorPoint(0, 0)
	maskPanel._maskPic:setSpriteScaleX(48)
	maskPanel._maskPic:setSpriteScaleY(80)
	maskPanel._maskPic:setOpacity(100)
	dice_scene:addChild(maskPanel:getCOObj())
	maskPanel:setVisible(false)
	--local dice_scene = CCScene:create()
	--dice_scene:addChild(dice_panel:getCOObj())
	CCDirector:sharedDirector():runWithScene(dice_scene)
end

---------------------------------------------------------------------

local test_text = nil
local idx = 1

function http_main()
	--local url_str = "http://www.touyou.mobi/interface/user.php?type=logon&name=ddb&pass=1234&version=28"
	--createTestScene()
	createDiceScene()
	--GAMEHALL_PANEL.showGameHallPanel(getRootNode(), 0, 0)	
	--LUCKY_ROT_PANEL.showLuckyPagePanel(getRootNode(), 0, 0)
	--PLAYER_INFO_PANEL.showPlayerInfoPagePanel(getRootNode(), 0, 0)
	--CHALLENGE_PANEL.showChallengePagePanel(getRootNode(), 0, 0)
	--CHAT_PANEL.showChatPagePanel(getRootNode(), 0, 0)
	--MAINPAGE_PANEL.showMainPagePanel(getRootNode(), 0, 0)
	--REGISTER_PANEL.showRegisterPanel(getRootNode(), 0, 0)
	local LOGIN_PANEL = Import("lua/login.lua")
	LOGIN_PANEL.showLoginPanel(getRootNode(), 0, 0)

	--MALL_PANEL.showMallPagePanel(getRootNode(), 0, 0)
	--FARM_PANEL.create_scene()
end

