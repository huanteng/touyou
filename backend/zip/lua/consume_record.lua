-- game hall panel

local consumeInfoPage = nil
CONSUME_LAYOUT = Import("layout/consume_record.lua")
LOGIN_PANEL = Import("lua/login.lua")
local ConsumeConfig = CONSUME_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
-- gold 1 yb 2
local gold_consume_rec = {}
local yb_consume_rec = {}
--[[
balance
time
time2
type
change
content
momo
--]]

local cur_gold_page = 1
local max_gold_page = 5
local cur_yb_page = 0
local max_yb_page = 5

local info_desc_info = {
	item_width = 480,
	item_height = 40,
	column_cnt = 1,
	x_space = 10,
	y_space = 10,
}

local freshing = false

local function createBottomPanel(parent, x, y)
	local c, c2 = ConsumeConfig, nil
	local o = nil
	
	local config = ConsumeConfig["bottom_part_config"]
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	ret_panel._bg_pic = LIGHT_UI.clsSprite:New(ret_panel, 0, 80, "tiny_black_pixel.png")
	ret_panel._bg_pic:setAnchorPoint(0, 1)
	ret_panel._bg_pic:setScaleX(480)
	ret_panel._bg_pic:setScaleY(80)
	
	local function backConsumeRecord()
		local MALL_PANEL = Import("lua/mall.lua")
		MALL_PANEL.closeMallPagePanel()
		showConsumeInfoPagePanel( HTTP_CLIENT.getRootNode(), 0, 0 )
	end

	local function onBuy(btn, x, y)
		closeConsumeInfoPagePanel()
		local MALL_PANEL = Import("lua/mall.lua")
		MALL_PANEL.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, backConsumeRecord, nil, true )
	end
	
	--[[ret_panel._buy_btn = LIGHT_UI.clsFrameButton:New(ret_panel, config["buy_btn_x"], config["buy_btn_y"], 80, 1, "button_blue_line.png", "button_blue_line_on.png")
	ret_panel._buy_btn:hideLine()
	ret_panel._buy_btn:setString("购买")
	ret_panel._buy_btn.onTouchEnd = onBuy--]]
	
	ret_panel._buy_btn = create9Button( ret_panel, c.buy_btn, onBuy )

	local gold_txt = LIGHT_UI.clsLabel:New(ret_panel, config["money_x"], config["money_y"], "持有金币: " .. LOGIN_PANEL.getUserData()["gold"], GLOBAL_FONT, 24)
	gold_txt:setAnchorPoint(0, 0)
	ret_panel._gold_txt = gold_txt

	return ret_panel
end

local function createItemByConsumeInfo(consume_info)
	local config = ConsumeConfig.consume_rec_config
	local nodeObj = LIGHT_UI.clsNode:New(nil, 0, 0)

	local time_txt = LIGHT_UI.clsLabel:New(nodeObj, config["time_x"], config["time_y"], consume_info["time"], GLOBAL_FONT, 22)
	time_txt:setAnchorPoint(0, 1)
	time_txt:setTextColor(102, 102, 102)
	local type_name = LIGHT_UI.clsLabel:New(nodeObj, config["type_x"], config["type_y"], consume_info["content"], GLOBAL_FONT, 22)
	type_name:setAnchorPoint(0.5, 1)
	type_name:setTextColor(102, 102, 102)
	local change_txt = LIGHT_UI.clsLabel:New(nodeObj, config["change_x"], config["change_y"], consume_info["change"], GLOBAL_FONT, 22)
	change_txt:setAnchorPoint(0.5, 1)
	change_txt:setTextColor(102, 102, 102)
	local balance_txt = LIGHT_UI.clsLabel:New(nodeObj, config["balance_x"], config["balance_y"], consume_info["balance"], GLOBAL_FONT, 22)
	balance_txt:setAnchorPoint(0.5, 1)
	balance_txt:setTextColor(102, 102, 102)

	return nodeObj
end

local function on_get_gold_history(data)
	freshing = false
	if cur_gold_page == 1 then
		consumeInfoPage._move_info_page._gold_page:clearAllItem()
	end
	if consumeInfoPage.tip then
		clear( { consumeInfoPage.tip } )
		consumeInfoPage.tip = nil
	end
	
	if table.getn(data.data)>0 then
		for _, gold_consume_info in pairs(data.data) do
			local item = createItemByConsumeInfo(gold_consume_info)
			consumeInfoPage._move_info_page._gold_page:append_item(item)
			table.insert(gold_consume_rec, gold_consume_info)
		end		
	else
		consumeInfoPage.tip = createLabel(consumeInfoPage, {text="暂无记录...",x=200,y=400})
	end		
	consumeInfoPage._move_info_page._gold_page:refresh_view()
end

local function on_get_yb_history(data)
	freshing = false
	if cur_yb_page == 1 then
		consumeInfoPage._move_info_page._yb_money_page:clearAllItem()
	end
	
	if consumeInfoPage.tip then
		clear( { consumeInfoPage.tip } )
		consumeInfoPage.tip = nil
	end
	
	if table.getn(data.data)>0 then
		for _, yb_consume_info in pairs(data.data) do
		local item = createItemByConsumeInfo(yb_consume_info)
		consumeInfoPage._move_info_page._yb_money_page:append_item(item)
		table.insert(yb_consume_rec, yb_consume_info)
		end				
	else
		consumeInfoPage.tip = createLabel(consumeInfoPage, {text="暂无记录...",x=200,y=400})
	end		
	consumeInfoPage._move_info_page._yb_money_page:refresh_view()
end

local function request_gold_consume_data(page)
	doCommand( 'props', 'history', { page = page, size = 20, props = 1 }, on_get_gold_history )
end

local function request_yb_consume_data(page)
	doCommand( 'props', 'history', { page = page, size = 20, props = 2 }, on_get_yb_history )
end

local function createMoveInfoPage(parent, x, y)
	local config = ConsumeConfig["move_page_config"]
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_info_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config.view_height)
	ret_panel._move_info_grp:setVMovable(true)

	local gold_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, info_desc_info)
	gold_page:refresh_view()

	local function onGOverTop(layout)
		if freshing == false then
			freshing = true
			gold_consume_rec = {}
			--gold_page:clearAllItem()
			cur_gold_page = 1
			request_gold_consume_data(cur_gold_page)
		end
	end

	local function onGOverBottom(layout)
		if cur_gold_page < max_gold_page then
			cur_gold_page = cur_gold_page + 1
			request_gold_consume_data(cur_gold_page)
		end
	end

	gold_page.onHMoveOverTop = onGOverTop
	gold_page.onHMoveOverBottom = onGOverBottom

	local yb_money_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, info_desc_info)
	yb_money_page:refresh_view()

	local function onYOverTop(layout)
		if freshing == false then
			freshing = true
			yb_consume_rec = {}
			--yb_money_page:clearAllItem()
			cur_yb_page = 1
			request_yb_consume_data(cur_yb_page)
		end
	end

	local function onYOverBottom(layout)
		if cur_yb_page < max_yb_page then
			cur_yb_page = cur_yb_page + 1
			request_yb_consume_data(cur_yb_page)
		end
	end

	yb_money_page.onHMoveOverTop = onYOverTop
	yb_money_page.onHMoveOverBottom = onYOverBottom

	local function get_y_limit(obj)
		return config.inner_y 
	end

	gold_page:setPosition(0, config.inner_y)
	ret_panel._move_info_grp:appendItem(gold_page)
	yb_money_page:setPosition(0, config.inner_y)
	ret_panel._move_info_grp:appendItem(yb_money_page)
	ret_panel._move_info_grp:selectPage(1)

	gold_page.getYLimit = get_y_limit
	yb_money_page.getYLimit = get_y_limit

	ret_panel._gold_page = gold_page 
	ret_panel._yb_money_page = yb_money_page
	
	return ret_panel
end

local function createConsumeInfoPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = RegConfig, nil
	local o = nil

	local function on_back(btn)
		closeConsumeInfoPagePanel()
		--USER_INFO_PANEL.showUserInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		if PANEL_CONTAINER.closeChildPanel( nil, 16 ) then							
			PANEL_CONTAINER.addChild( USER_INFO_PANEL.showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )				
		end
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '消费记录' )
	
	config = ConsumeConfig["move_page_config"]
	ret_panel._move_info_page = createMoveInfoPage(ret_panel, config.x, config.y)

	local function on_sel_page(move_obj)
		
		if consumeInfoPage.tip then
			clear( { consumeInfoPage.tip } )
			consumeInfoPage.tip = nil
		end
		
		ret_panel._move_hint:selItem(move_obj:getCurPage())
		if move_obj:getCurPage() == 1 then
			cur_gold_page = 1
			request_gold_consume_data(cur_gold_page)
			
			local function onUserMoney( data )
				if ret_panel then
					if data.data.gold then
						--ret_panel._gold:setString( "안녕하세요" )
						--ret_panel._gold:setString( data.data.gold )
						consumeInfoPage._bottom_panel._gold_txt:setString( "持有金币：" .. data.data.gold )
					end
				end	
			end			
			--请求用户资料
			doCommand( 'user', 'money', {}, onUserMoney )
		end
		if move_obj:getCurPage() == 2 then
			cur_yb_page = 1
			request_yb_consume_data(cur_yb_page)			
			local function onMsg( data )			
				consumeInfoPage._bottom_panel._gold_txt:setString( "持有元宝：" .. data.data.bullion )
			end
			doCommand( 'user', 'money', {}, onMsg )			
		end
	end

	ret_panel._move_info_page._move_info_grp.onSelPage = on_sel_page

	local item_list = {
		[1] = {
			["txt"] = "金币",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
		[2] = {
			["txt"] = "元宝",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
	}

	config = ConsumeConfig["move_hint_config"]
	local total_width = 480
	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, config.x, config.y, "tab_bg_image.png", item_list, total_width, config.res, "tiny_black_pixel.png")
	local bg = ret_panel._move_hint:getBGObj()
	bg:setScaleX(total_width)
	ret_panel._move_hint:selItem(1)

	local function on_click( obj, idx )
		consumeInfoPage._move_hint:selItem( idx )		
		consumeInfoPage._move_info_page._move_info_grp:selectPage( idx )
	end

	ret_panel._move_hint.onClick = on_click

	ret_panel._bottom_panel = createBottomPanel(ret_panel, 0, 0)
	cur_gold_page = 1
	request_gold_consume_data(cur_gold_page)	

	return ret_panel
end

function showConsumeInfoPagePanel(parent, x, y)
	if not consumeInfoPage then
		consumeInfoPage = createConsumeInfoPagePanel(parent, x, y)
	else
		consumeInfoPage:setVisible(true)
	end
end

function closeConsumeInfoPagePanel()
	--consumeInfoPage:setVisible(false)
	clear({consumeInfoPage})
	consumeInfoPage = nil
end

