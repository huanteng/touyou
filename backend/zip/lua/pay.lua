-- pay page

PAY_LAYOUT = Import("layout/pay.lua")
local PayConfig = PAY_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local payPage = nil

local girl_list = {}
local cur_girl_page = 0
local max_girl_page = 5
local boy_list = {}
local cur_boy_page = 0
local max_boy_page = 5
local same_city_list = {}

local click_delta = 10

function setMsgDelegate(msg_obj, move_obj, click_func)
	local startx, starty
	local endx, endy
	local is_stop = false
	local function onTouchBegan(btn, x, y)
		if move_obj:isAutoMove() then
			is_stop = true
		end
		startx, starty = x, y
		move_obj:onTouchBegan(btn:convertToWorldSpace(x, y))
	end
	local function onTouchMove(btn, x, y)
		move_obj:onTouchMove(btn:convertToWorldSpace(x, y))
	end
	local function onTouchEnd(btn, x, y)
		endx, endy = x, y
		move_obj:onTouchEnd(btn:convertToWorldSpace(x, y))

		if ((math.abs(startx - endx) + math.abs(starty - endy)) <= click_delta) and (not is_stop) then
			click_func(btn, x, y)
		end
		is_stop = false
	end
	msg_obj.onTouchBegan = onTouchBegan
	msg_obj.onTouchMove = onTouchMove
	msg_obj.onTouchEnd = onTouchEnd
end

local function createBigItemByUserInfo(user_info)
	local config = PayConfig["big_item_config"]
	local nodeObj = LIGHT_UI.clsNode:New(nil, 0, 0)
	nodeObj._user_id = user_info["id"]

	local msg_obj = LIGHT_UI.clsSimpleButton:New(nodeObj, 0, 0, "portrait.png", "portrait.png")
	msg_obj:setButtonScale(0.8)
	msg_obj:setAnchorPoint(0, 1)
	
	local function on_click()
		GAMEHALL_PANEL.closeGameHallPanel()
		closePayPagePanel()
		PLAYER_INFO_PANEL.showByUserId(nodeObj._user_id)
	end

	setMsgDelegate(msg_obj, payPage._move_big_page._move_big_grp, on_click)

	local pic_obj = createSprite( nil, res( 0, 0, user_info["logo"] ) )
	pic_obj:setPosition(0, 0)
	nodeObj:addChild(pic_obj)
	if tonumber(user_info["is_real"]) == 1 then
		local video_pic = LIGHT_UI.clsSprite:New(nodeObj, config["real_x"], config["real_y"], "vedioauth_pic.png")
		video_pic:setAnchorPoint(0, 1)
	end
	if tonumber(user_info["vip_id"]) == 1 then
		local vip_pic = LIGHT_UI.clsSprite:New(nodeObj, config["vip_x"], config["vip_y"], "silver_vip_icon.png")
		vip_pic:setAnchorPoint(0, 1)
	end

	local function getContentSize(node)
		return {["width"] = 140, ["height"] = 140,}
	end
	nodeObj.getContentSize = getContentSize
	return nodeObj
end

local function createSmallItemByUserInfo(user_info)
	local config = PayConfig["small_item_config"]
	local nodeObj = LIGHT_UI.clsNode:New(nil, 0, 0)
	nodeObj._user_id = user_info["id"]

	local msg_obj = LIGHT_UI.clsSimpleButton:New(nodeObj, 0, 0, "small_white_bg.png", "small_white_bg.png")
	msg_obj:setAnchorPoint(0, 1)
	msg_obj:setButtonScaleX(48)
	msg_obj:setButtonScaleY(10)

	local function on_click()
		GAMEHALL_PANEL.closeGameHallPanel()
		closePayPagePanel()
		PLAYER_INFO_PANEL.showByUserId(nodeObj._user_id)
	end

	setMsgDelegate(msg_obj, payPage._move_small_page._move_small_grp, on_click)

	local pic_obj = createSprite( nil, res( 0, 0, user_info["logo"] ) ) 
	pic_obj:setPosition(0, 0)
	nodeObj:addChild(pic_obj)

	local name_obj = LIGHT_UI.clsLabel:New(nodeObj, config["name_x"], config["name_y"], user_info["name"], GLOBAL_FONT, 16)
	name_obj:setAnchorPoint(0, 1)
	name_obj:setTextColor(0, 0, 0)
	local sex_pic = LIGHT_UI.clsSprite:New(nodeObj, config["sex_pic_x"], config["sex_pic_y"], "man.png")
	sex_pic:setAnchorPoint(0, 1)
	local address = LIGHT_UI.clsLabel:New(nodeObj, config["address_x"], config["address_y"], user_info["city"], GLOBAL_FONT, 16)
	address:setAnchorPoint(0, 1)
	address:setTextColor(0, 0, 0)
	if tonumber(user_info["is_real"]) == 1 then
		local video_pic = LIGHT_UI.clsSprite:New(nodeObj, config["real_x"], config["real_y"], "vedioauth_pic.png")
		video_pic:setAnchorPoint(0, 1)
	end
	if tonumber(user_info["vip_id"]) == 1 then
		local vip_pic = LIGHT_UI.clsSprite:New(nodeObj, config["vip_x"], config["vip_y"], "silver_vip_icon.png")
		vip_pic:setAnchorPoint(0, 1)
	end

	local mood_txt = LIGHT_UI.clsMultiLabel:New(nodeObj, config["mood_x"], config["mood_y"], user_info["mood"], GLOBAL_FONT, 16, 364, 1)
	mood_txt:setTextColor(0, 0, 0)
	local size = { width = 0, height = 0, }
	if user_info["mood"] and string.len(user_info["mood"]) > 0 then
		size = mood_txt:getContentSize()
	end

	local function getContentSize(node)
		return {["width"] = 480, ["height"] = size.height + 90,}
	end
	nodeObj.getContentSize = getContentSize
	return nodeObj
end

local function on_get_girl_list(data)
	local real_data = data.data
	for _, user_info in ipairs(real_data) do
		table.insert(girl_list, user_info)
		payPage._move_big_page._girl_page:append_item(createBigItemByUserInfo(user_info))
		payPage._move_small_page._girl_page:append_item(createSmallItemByUserInfo(user_info))
	end

	payPage._move_big_page._girl_page:refresh_view()
	payPage._move_small_page._girl_page:refresh_view()
end

local function on_get_boy_list(data)
	local real_data = data.data
	for _, user_info in ipairs(real_data) do
		table.insert(boy_list, user_info)
		payPage._move_big_page._boy_page:append_item(createBigItemByUserInfo(user_info))
		payPage._move_small_page._boy_page:append_item(createSmallItemByUserInfo(user_info))
	end

	payPage._move_big_page._boy_page:refresh_view()
	payPage._move_small_page._boy_page:refresh_view()
end

local function request_girl_list(page)
	doCommand( 'pay', 'list_user', { sex = 1, page = page }, on_get_girl_list )
end

local function request_boy_list(page)
	doCommand( 'pay', 'list_user', { sex = 0, page = page }, on_get_boy_list )
end

--[[
[1] = {
	frendship = 0,
	id = *,
	profit = 188,
	is_real = 0,
	vip_id = 0,
	city = "",
	sex = 1,
	logo = "http ***",
	name = "**",
	mood = "**",
	province = "**",
}
--]]

local big_desc_info = {
	item_width = 140,
	item_height = 140,
	column_cnt = 3,
	x_space = 20,
	y_space = 40,
}

local small_desc_info = {
	item_width = 470,
	item_height = 140,
	column_cnt = 1,
	x_space = 0,
	y_space = 0,
}

local function createMoveBigPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = PayConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_big_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config["view_height"])
	ret_panel._move_big_grp:setVMovable(true)

	local girl_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, big_desc_info)
	girl_page:refresh_view()

	local function onGirlOverTop(layout)
		girl_list = {}
		girl_page:clearAllItem()
		cur_girl_page = 1
		request_girl_list(cur_girl_page)
	end
	local function onGirlOverBottom(layout)
		if cur_girl_page < max_girl_page then
			cur_girl_page = cur_girl_page + 1
			request_girl_list(cur_girl_page)
		end
	end

	girl_page.onHMoveOverTop = onGirlOverTop
	girl_page.onHMoveOverBottom = onGirlOverBottom

	local boy_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, big_desc_info)
	boy_page:refresh_view()

	local function onBoyOverTop(layout)
		boy_list = {}
		boy_page:clearAllItem()
		cur_boy_page = 1
		request_boy_list(cur_boy_page)
	end
	local function onBoyOverBottom(layout)
		if cur_boy_page < max_boy_page then
			cur_boy_page = cur_boy_page + 1
			request_boy_list(cur_boy_page)
		end
	end

	boy_page.onHMoveOverTop = onBoyOverTop
	boy_page.onHMoveOverBottom = onBoyOverBottom

	local same_city_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, big_desc_info)
	same_city_page:refresh_view()

	girl_page:setPosition(0, config["inner_y"])
	ret_panel._move_big_grp:appendItem(girl_page)
	boy_page:setPosition(0, config["inner_y"])
	ret_panel._move_big_grp:appendItem(boy_page)
	same_city_page:setPosition(0, config["inner_y"])
	ret_panel._move_big_grp:appendItem(same_city_page)
	ret_panel._move_big_grp:selectPage(1)

	ret_panel._girl_page = girl_page
	ret_panel._boy_page = boy_page 
	ret_panel._same_city_page = same_city_page 

	local function get_y_limit(obj)
		return config["inner_y"] 
	end

	girl_page.getYLimit = get_y_limit
	boy_page.getYLimit = get_y_limit
	same_city_page.getYLimit = get_y_limit
	
	return ret_panel
end

local function createMoveSmallPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = PayConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_small_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config["view_height"])
	ret_panel._move_small_grp:setVMovable(true)

	local girl_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	girl_page._use_dynamic_size = true
	girl_page:refresh_view()

	local function onGirlOverTop(layout)
		girl_list = {}
		girl_page:clearAllItem()
		cur_girl_page = 1
		request_girl_list(cur_girl_page)
	end
	local function onGirlOverBottom(layout)
		if cur_girl_page < max_girl_page then
			cur_girl_page = cur_girl_page + 1
			request_girl_list(cur_girl_page)
		end
	end

	girl_page.onHMoveOverTop = onGirlOverTop
	girl_page.onHMoveOverBottom = onGirlOverBottom

	local boy_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	boy_page._use_dynamic_size = true
	boy_page:refresh_view()

	local function onBoyOverTop(layout)
		boy_list = {}
		boy_page:clearAllItem()
		cur_boy_page = 1
		request_boy_list(cur_boy_page)
	end
	local function onBoyOverBottom(layout)
		if cur_boy_page < max_boy_page then
			cur_boy_page = cur_boy_page + 1
			request_boy_list(cur_boy_page)
		end
	end

	boy_page.onHMoveOverTop = onBoyOverTop
	boy_page.onHMoveOverBottom = onBoyOverBottom

	local same_city_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	same_city_page:refresh_view()

	girl_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(girl_page)
	boy_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(boy_page)
	same_city_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(same_city_page)
	ret_panel._move_small_grp:selectPage(1)

	ret_panel._girl_page = girl_page
	ret_panel._boy_page = boy_page 
	ret_panel._same_city_page = same_city_page

	local function get_y_limit(obj)
		return config["inner_y"] 
	end

	girl_page.getYLimit = get_y_limit
	boy_page.getYLimit = get_y_limit
	same_city_page.getYLimit = get_y_limit
	
	return ret_panel
end

local function createPayPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = PayConfig, nil
	local o = nil

	local function on_back(btn)
		closePayPagePanel()
		MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '挑战大厅' )
	
	local function onClickChangeStyle(obj, x, y)
		local big_is_visible = not ret_panel._move_big_page:isVisible()
		local small_is_visible = not ret_panel._move_small_page:isVisible()
		ret_panel._move_big_page:setVisible(big_is_visible)
		ret_panel._move_small_page:setVisible(small_is_visible)
		ret_panel._move_big_page._move_big_grp:selectPage(1)
		ret_panel._move_small_page._move_small_grp:selectPage(1)
	end

	config = PayConfig["style_btn_config"]
	ret_panel._change_style_btn = LIGHT_UI.clsCheckButton:New(ret_panel, config["x"], config["y"], config["grid_res"], config["list_res"])
	ret_panel._change_style_btn.onTouchEnd = onClickChangeStyle

	config = PayConfig["move_page_config"]
	ret_panel._move_big_page = createMoveBigPage(ret_panel, config["x"], config["y"]) -- 大照片列表
	ret_panel._move_small_page = createMoveSmallPage(ret_panel, config["x"], config["y"]) -- 小图标列表 
	ret_panel._move_small_page:setVisible(false)

	local function on_sel_big_page(move_obj)
		ret_panel._move_hint:selItem(move_obj:getCurPage())
		if move_obj:getCurPage() == 1 and table.maxn(girl_list) <= 0 then
			cur_girl_page = 1
			request_girl_list(cur_girl_page)
		end
		if move_obj:getCurPage() == 2 and table.maxn(boy_list) <= 0 then
			cur_boy_page = 1
			request_boy_list(cur_boy_page)
		end
	end

	local function on_sel_small_page(move_obj)
		ret_panel._move_hint:selItem(move_obj:getCurPage())
		if move_obj:getCurPage() == 1 and table.maxn(girl_list) <= 0 then
			cur_girl_page = 1
			request_girl_list(cur_girl_page)
		end
		if move_obj:getCurPage() == 2 and table.maxn(boy_list) <= 0 then
			cur_boy_page = 1
			request_boy_list(cur_boy_page)
		end
	end

	ret_panel._move_big_page._move_big_grp.onSelPage = on_sel_big_page
	ret_panel._move_small_page._move_small_grp.onSelPage = on_sel_small_page

	local item_list = {
		[1] = {
			["txt"] = "美女",
			["normal_color"] = { [1] = 127, [2] = 127, [3] = 127, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
		},
		[2] = {
			["txt"] = "帅哥",
			["normal_color"] = { [1] = 127, [2] = 127, [3] = 127, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
		},
		[3] = {
			["txt"] = "同城",
			["normal_color"] = { [1] = 127, [2] = 127, [3] = 127, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
		},
	}

	config = PayConfig["move_hint_config"]
	local total_width = 480
	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, config["x"], config["y"], "tab_bg_image.png", item_list, total_width, config["res"],"tiny_black_pixel.png")
	local bg = ret_panel._move_hint:getBGObj()
	bg:setScaleX(480)
	ret_panel._move_hint:selItem(1)
	LIGHT_UI.alignXCenter(ret_panel._move_hint)

	if table.maxn(girl_list) <= 0 then
		cur_girl_page = 1
		request_girl_list(cur_girl_page)
	end

	return ret_panel
end

function showPayPagePanel(parent, x, y, item_id, cnt)
	if not payPage then
		payPage = createPayPagePanel(parent, x, y)
	else
		payPage:setVisible(true)
	end
end

function closePayPagePanel()
	--payPage:setVisible(false)
	clear({payPage})
	payPage = nil
end

