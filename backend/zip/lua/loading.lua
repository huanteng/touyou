LOADING_LAYOUT = Import("layout/loading.lua")
local LoadingConfig = LOADING_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local parent = nil
--加载loading
function Loading(_root)
	assert(_root)
	
	--[[local function onTimeOut()
		if _root.Loading then
			clear({_root.Loading})
			_root.Loading = nil
		end
	end--]]
	
	parent = _root
	
	if _root.Loading then 
		return _root.Loading
	else
		local order = _root:getCOObj():getChildrenCount() + 80	
		_root.Loading = createNode(_root,{x = 0,y = 0},nil,order)
		local bg = createSprite(_root.Loading,LoadingConfig["loading_bg"])		
		--local a = createSprite(_root.Loading,LoadingConfig["loading_bg"])
		local obj = bg:getSprite()
		if obj then
			--[[obj.x = 100
			obj.y = 100--]]
			a_play(obj,LoadingConfig.loading,true)
		end
		--[[local d_sec = 0
		if not sec then
			d_sec = tonumber(LoadingConfig.delay.time)
		else
			d_sec = math.max(tonumber(sec),LoadingConfig.delay.time)
		end--]] 
		--f_delay_do(_root,onTimeOut,nil,d_sec)	
	end		
	return _root.Loading
end

function HLoading()
	if parent and parent.Loading then
		clear({parent.Loading})
		parent.Loading = nil
		parent = nil
	end
end