local chatPage = nil

CHAT_LAYOUT = Import("layout/chat.lua")
local ChatConfig = CHAT_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local cur_fsay_page = 0
local max_fsay_page = 5
local cur_asay_page = 0
local max_asay_page = 5
local cur_isay_page = 0
local max_isay_page = 5
local total_width = 480

local freshing = true

local chat_desc_info = {
	item_width = total_width,
	item_height = 140,
	column_cnt = 1,
	x_space = 20,
	y_space = 40,
}

local DETAIL_PAGE = Import("lua/chat_detail.lua") 
local CHALLENGE_PAGE = Import("lua/challenge.lua")
local PLAYER_INFO_PANEL = Import("lua/player_info.lua")

local function createItemBySayInfo( info )

	local ret_panel = LIGHT_UI.clsNode:New( nil, 0, 0 )
	local total_height = 300

	PAGE[ 'xyz' ] = {
			x = 0,
			y = 0,
			sx = 0,
			sy = 0,
			ax = 0,
			ay = 0,
		}
	
	local function back_func()
		PLAYER_INFO_PANEL.closePlayerInfoPagePanel()
		showChatPagePanel( HTTP_CLIENT.getRootNode(), 0, 0 )
	end

	local function click_logo( btn, x, y )
		

		local function back_func()
			PLAYER_INFO_PANEL.closePlayerInfoPagePanel()
			showChatPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)			
			if PANEL_CONTAINER.closeChildPanel( nil, 2 ) then
				PANEL_CONTAINER.addChild( showChatPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			end
		end

		local user_info = LOGIN_PANEL.getUserData()
		if user_info.id ~= info["user"] then	
			closeChatPagePanel()			
			PLAYER_INFO_PANEL.showByUserId( info["user"] )
			PLAYER_INFO_PANEL.init( { back_func = back_func })
		else
			showMessage( chatPage, "查看个人资料请到导航栏点击头像进入", {ms="4000", y="400"} )
		end	
		
	end
	
	createButton( ret_panel, ChatConfig.logo_btn_c, click_logo )
	

	ChatConfig.logo_pic_c.res = info.logo
	createSprite( ret_panel, ChatConfig.logo_pic_c )

	local function click_say_content( obj, x, y )
		--closeChatPagePanel()
		local a = info
		DETAIL_PAGE.showDetailPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, info["id"] )
	end

	
	local frame_btn = create9Sprite( ret_panel, ChatConfig.text_frame_c )	
	local frame_bg = createFrameButton( ret_panel, ChatConfig.frame_bg_c )
	
	setMsgDelegate( frame_bg, chatPage._move_chat_page._move_chat_grp, click_say_content )	
	

	ChatConfig.username_c.text = info["name"]
	local username_obj = createLabel( ret_panel, ChatConfig.username_c )
	AdjustString(username_obj,ChatConfig.username_c.text,100)
	doConfig(username_obj,ChatConfig.username_c)	
	if info["vip_id"] ~= "0" then
		local vip_logo = string.format( 'vip%i.png', info["vip_id"] )
		ChatConfig.vip_c.res = vip_logo
		createSprite( ret_panel, ChatConfig.vip_c )
	end

	ChatConfig.say_time_c.text = info["time"]
	--ChatConfig.say_time_c.text = info["content"]
	createLabel( ret_panel, ChatConfig.say_time_c )	

	local sex_pic_res = "man.png"
	if info["sex"] == "1" then sex_pic_res = "woman.png" end
	ChatConfig.sex_pic_c.res = sex_pic_res
	local sex_obj = createSprite( ret_panel, ChatConfig.sex_pic_c )

	if info["is_real"] == "1" then
		ChatConfig.video_auth_c.res = "vedioauth_pic.png" 
		createSprite( ret_panel, ChatConfig.video_auth_c )
	end
	
	if info["content"] then
		ChatConfig.say_content_c.text = info["content"]
	else
		ChatConfig.say_content_c.text = "label bug"
	end	
	--ChatConfig.say_content_c.text = "有问题"
	--content_obj = createMultiLabel( ret_panel, ChatConfig.say_content_c )
	
	local specialCode = { 128, 161, 225, 128, 187, 225, 128, 150, 225, 128, 180, 239, 187, 191, 225, 128, 177, 225, 128, 155, 225,
	128, 172, 225, 128, 132, 225, 128, 185, 239, 187, 191 }
	local len = string.len( info["content"] )
	local tmp_pos = 2	
	while (tmp_pos <= len) do
		local char_value = string.byte( string.sub(info["content"], tmp_pos, tmp_pos ) )
		if char_value ~= specialCode[ tmp_pos - 1 ] then		
			break
		end
		tmp_pos = tmp_pos + 1
	end		
	local content_obj
	if tmp_pos == 34 then
		content_obj = createLabel( ret_panel, ChatConfig.say_content_c )
	else		
	    content_obj = createMultiLabel( ret_panel, ChatConfig.say_content_c )	
	end
	

	local say_pic_obj = nil
	ChatConfig.say_pic_c.res = info["small"]
	if string.len( info["small"] ) > 0 then	say_pic_obj = createSprite( ret_panel, ChatConfig.say_pic_c ) end

	local reply_num_pic_obj = createSprite( ret_panel, ChatConfig.reply_pic_c )

	ChatConfig.reply_txt_c.text = info["replys"]
	local reply_num_obj = createLabel( ret_panel, ChatConfig.reply_txt_c )
	
	local reply_num_obj_size = reply_num_obj:getContentSize()
	local username_obj_size = username_obj:getContentSize()
	local sex_obj_size = sex_obj:getContentSize()
	local content_obj_size = content_obj:getContentSize()
	total_height = reply_num_obj_size.height + username_obj_size.height + sex_obj_size.height + content_obj_size.height + math.abs( ChatConfig.username_c.y + ChatConfig.sex_pic_c.y + ChatConfig.say_content_c.y ) + ChatConfig["say_item_config"]["frame_buttom"]
	if string.len( info["small"] ) > 0 then
		total_height = total_height + math.abs( ChatConfig.say_pic_c.y )
		total_height = total_height + ChatConfig["say_item_config"]["photo_h"]
		local x, y = say_pic_obj:getPosition()
		-- 19来自9.lua的say_content_cut.x
		x = x + ( ChatConfig.say_content_c.sx - 19 - ChatConfig["say_item_config"]["photo_w"] ) / 2
		say_pic_obj:setPosition( x, y )
	end
	local x, y = reply_num_pic_obj:getPosition()
	y = -total_height + ChatConfig["say_item_config"]["reply_pic_margin"] + ChatConfig["say_item_config"]["frame_buttom"]
	reply_num_pic_obj:setPosition( x, y )	
	local x, y = reply_num_obj:getPosition()
	y = -total_height + ChatConfig["say_item_config"]["reply_pic_margin"] + ChatConfig["say_item_config"]["frame_buttom2"]
	reply_num_obj:setPosition( x, y )


	frame_bg:setContentSize( ChatConfig.frame_bg_c.sx, total_height -5 )
	frame_btn:setContentSize( ChatConfig.text_frame_c.sx, total_height )

	local function getContentSize( node )		
		return { ["width"] = total_width, ["height"] = total_height + ChatConfig["say_item_config"]["frame_buttom"], }
	end

	ret_panel.getContentSize = getContentSize
	
	return ret_panel
end

--[[local function on_get_fsay_list(data)
	fsay_list = {}
	chatPage._move_chat_page._friend_say_page:clearAllItem()
	local real_data = data.data
	for _, say_info in ipairs(real_data) do
		table.insert(fsay_list, say_info)
		chatPage._move_chat_page._friend_say_page:append_item(createItemBySayInfo(say_info))
	end

	chatPage._move_chat_page._friend_say_page:refresh_view()
end

local function on_get_asay_list(data)
	asay_list = {}
	chatPage._move_chat_page._all_say_page:clearAllItem()
	local real_data = data.data
	for _, say_info in ipairs(real_data) do
		table.insert(asay_list, say_info)
		chatPage._move_chat_page._all_say_page:append_item(createItemBySayInfo(say_info))
	end

	chatPage._move_chat_page._all_say_page:refresh_view()
end

local function on_get_isay_list(data)
	isay_list = {}
	chatPage._move_chat_page._self_say_page:clearAllItem()
	local real_data = data.data
	for _, say_info in ipairs(real_data) do
		table.insert(isay_list, say_info)
		chatPage._move_chat_page._self_say_page:append_item(createItemBySayInfo(say_info))
	end

	chatPage._move_chat_page._self_say_page:refresh_view()
end
--]]

local function clearItemById( page, id, cache )
	local itemList = page:getItemList()
	for k,v in ipairs( itemList ) do
		if v.id == id then
			if not cache then
				page:clearItem( k, item )
			end				
		end
	end 
end	

local itemCount = 1
local cacheArr = {}
local function onCommentLists( kind, data, cache )
	local pagename = nil
	
	if kind == 1 then
		pagename = '_friend_say_page'
	elseif kind == 2 then
		pagename = '_all_say_page'
	else
		pagename = '_self_say_page'
	end		
	
	
	local page
	if chatPage then
		page = chatPage._move_chat_page[ pagename ]	
		--page:clearAllItem()	
		
		if freshing and freshing == true then			
			page:clearAllItem()
			if kind == 1 then
				resetFListPosition()
			elseif kind == 2 then
				resetAListPosition()
			else
				resetSListPosition()
			end						
		end		
		
		for k, v in pairs(cacheArr) do
			clearItemById( page, v, cache )
		end	
		
			
		local real_data = data.data			
		if type( real_data ) == "table" then
			
			itemCount = 1
			cacheArr = {}
			for _, say_info in ipairs(real_data) do						
					local ret_panel = createItemBySayInfo(say_info)
					if freshing and freshing == true then
						if not cache then
							freshing = false
						end
						if itemCount == 1 then
							ret_panel.tip = createLabel( ret_panel, {x = 100, y = 30, ax = 0, ay = 0.5, text = '等侯刷新', css='c1'} )
							xCenter( ret_panel.tip )
							local locX, locY = ret_panel.tip:getPosition()
							ret_panel.tip:setPosition( locX + 10, locY )
							locX, locY = ret_panel.tip:getPosition()
							ret_panel.arrow = createSprite( ret_panel, {x = locX - 20, y = 30, res = 'arrow_down.png'} )
							ret_panel.loading = createSprite(  ret_panel, {x = locX - 20, y = 30, res = 'white_dot.png'})
							ret_panel.loading:setVisible( false )
						end
					end
					
					if cache then
						table.insert( cacheArr, say_info.id )
					end
					ret_panel.id = say_info.id													
					page:append_item(ret_panel)						
				itemCount = itemCount + 1						
			end
		end
		
		page:refresh_view()
		
	end
end

--[[local function request_fsay_list(page)
	showLoading(chatPage)
	doCommand( 'comment', 'lists', { page = page, kind = 1 }, on_get_fsay_list, 10 )
end

local function request_asay_list(page)
	showLoading(chatPage)
	doCommand( 'comment', 'lists', { page = page, kind = 2 }, on_get_asay_list, 5 )
end

local function request_isay_list(page)
	showLoading(chatPage)
	doCommand( 'comment', 'lists', { page = page, kind = 3 }, on_get_isay_list, 5 )
end--]]

local function doCommentLists( kind, page, TTL )
	local function cb( data, cache )
		onCommentLists( kind, data, cache )
	end
	
	--showLoading(chatPage)
	
	local cachename = string.format( 'comment.lists.%d.%d', kind, page )
	--doCommand( 'comment', 'lists', { page = page, kind = kind, size = 10 }, cb, nil, { cachename = cachename,loading = 0 } )
	doCommand( 'comment', 'lists', { page = page, kind = kind, size = 10 }, cb, TTL, { cachename = cachename,loading = 0 } )
	--doCommand( 'comment', 'lists', { page = page, kind = kind }, cb, TTL, { loading = 0 } )
end

function onFChatTouchEnd()
	cur_fsay_page = 1
	freshing = true
	doCommentLists( 1, 1, 1 )	
end

function resetFListPosition()
	local config = ChatConfig["move_page_config"]
	chatPage._move_chat_page._friend_say_page:setPosition(0, config.inner_y)
end

function onAChatTouchEnd()
	cur_asay_page = 1
	freshing = true
	doCommentLists( 2, 1, 1 )	
end

function resetAListPosition()
	local config = ChatConfig["move_page_config"]
	if chatPage._move_chat_page._all_say_page then
		chatPage._move_chat_page._all_say_page:setPosition(0, config.inner_y)
	end
end

function onSChatTouchEnd()
	cur_isay_page = 1
	freshing = true
	doCommentLists( 3, 1, 1 )	
end

function resetSListPosition()
	local config = ChatConfig["move_page_config"]
	chatPage._move_chat_page._self_say_page:setPosition(0, config.inner_y)
end

local function createChatPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)
	local config = ChatConfig["move_page_config"]

	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_chat_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config.view_height)
	ret_panel._move_chat_grp:setVMovable(true)
	
	ret_panel._move_chat_grp.touchEnd = onAChatTouchEnd
	ret_panel._move_chat_grp.resetListPos = resetAListPosition
	ret_panel._move_chat_grp.tipHeight = 700

	local friend_say_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, chat_desc_info)
	friend_say_page._use_dynamic_size = true 
	friend_say_page:refresh_view()

	local function onFOverTop(layout)
		
--		request_fsay_list(cur_fsay_page)
	end

	local function onFOverBottom(layout)
		if cur_fsay_page < max_fsay_page then
			cur_fsay_page = cur_fsay_page + 1
			doCommentLists( 1, cur_fsay_page, 1)
--			request_fsay_list(cur_fsay_page)
		end
	end

	--friend_say_page.onHMoveOverTop = onFOverTop
	friend_say_page.onHMoveOverBottom = onFOverBottom

	local all_say_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, chat_desc_info)
	all_say_page._use_dynamic_size = true
	all_say_page:refresh_view()

	local function onAOverTop(layout)
		
--		request_asay_list(cur_asay_page)
	end

	local function onAOverBottom(layout)
		if cur_asay_page < max_asay_page then
			cur_asay_page = cur_asay_page + 1
			doCommentLists( 2, cur_asay_page, 1 )
--			request_asay_list(cur_asay_page)
		end
	end

	all_say_page.onHMoveOverTop = onAOverTop
	all_say_page.onHMoveOverBottom = onAOverBottom

	local self_say_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, chat_desc_info)
	self_say_page._use_dynamic_size = true
	self_say_page:refresh_view()

	local function onSelfOverTop(layout)
		
--		request_isay_list(cur_isay_page)
	end

	local function onSelfOverBottom(layout)
		if cur_isay_page < max_isay_page then
			cur_isay_page = cur_isay_page + 1
			doCommentLists( 3, cur_isay_page, 1 )
--			request_isay_list(cur_isay_page)
		end
	end

	self_say_page.onHMoveOverTop = onSelfOverTop
	self_say_page.onHMoveOverBottom = onSelfOverBottom

	local function get_y_limit(obj)
		return config.inner_y - 60
	end

	friend_say_page:setPosition(0, config.inner_y)
	ret_panel._move_chat_grp:appendItem(friend_say_page)
	all_say_page:setPosition(0, config.inner_y)
	ret_panel._move_chat_grp:appendItem(all_say_page)
	self_say_page:setPosition(0, config.inner_y)
	ret_panel._move_chat_grp:appendItem(self_say_page)
	ret_panel._move_chat_grp:selectPage(2)
	--ret_panel._move_chat_grp:selectPage(1)

	friend_say_page.getYLimit = get_y_limit
	all_say_page.getYLimit = get_y_limit
	self_say_page.getYLimit = get_y_limit

	ret_panel._friend_say_page = friend_say_page 
	ret_panel._all_say_page = all_say_page 
	ret_panel._self_say_page = self_say_page 

	return ret_panel
end

local REPLY_PAGE = Import("lua/reply.lua")

local function on_reply_cb(data)
	local data_tbl = json.decode(data)
	print(LIGHT_UI.db_fmt(data_tbl))
end

--local testPageCount = 1
local function createChatPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createContainerChildPanel( parent, x, y )
	chatPage = ret_panel

	local c, c2 = RegConfig, nil
	local o = nil

	local function on_back(btn)
		closeChatPagePanel()
		MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)		
	end

	ret_panel._onBack = on_back
	--ret_panel._head_text:setString( '说两句' )

	local function on_cancel()
		
		if PANEL_CONTAINER.closeChildPanel( nil, 2 ) then
			PANEL_CONTAINER.addChild( showChatPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
		end		
	end

	local function onClickSay(obj, x, y)
		
		--doCommentLists( 1, testPageCount, 1 )
		--testPageCount = testPageCount + 1
		local page_info = {
			title = "我来说",
			desc = "",
		}	
		REPLY_PAGE.showReplyPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, on_cancel, page_info, nil)
		closeChatPagePanel()
	end

	config = ChatConfig["say_btn_config"]
	ret_panel._say_btn = LIGHT_UI.clsCheckButton:New(ret_panel, config.x, config.y, config.normal_res, config.click_res)
	ret_panel._say_btn.onTouchEnd = onClickSay

	config = ChatConfig["move_page_config"]
	ret_panel._move_chat_page = createChatPage(ret_panel, config.x, config.y)

	local function on_sel_page(move_obj)
		freshing = true
		ret_panel._move_hint:selItem(move_obj:getCurPage())
		if move_obj:getCurPage() == 1 then
			cur_fsay_page = 1
			doCommentLists( 1, cur_fsay_page, 1 )
			chatPage._move_chat_page._move_chat_grp.touchEnd = onFChatTouchEnd
			chatPage._move_chat_page._move_chat_grp.resetListPos = resetFListPosition
--			request_fsay_list(cur_fsay_page)
		end
		if move_obj:getCurPage() == 2 then
			cur_asay_page = 1
			doCommentLists( 1, cur_asay_page, 1 )
			chatPage._move_chat_page._move_chat_grp.touchEnd = onAChatTouchEnd
			chatPage._move_chat_page._move_chat_grp.resetListPos = resetAListPosition
--			request_asay_list(cur_asay_page)
		end
		if move_obj:getCurPage() == 3 then
			cur_isay_page = 1
			doCommentLists( 3, cur_isay_page, 1 )
			chatPage._move_chat_page._move_chat_grp.touchEnd = onSChatTouchEnd
			chatPage._move_chat_page._move_chat_grp.resetListPos = resetSListPosition
--			request_isay_list(cur_isay_page)
		end
	end

	ret_panel._move_chat_page._move_chat_grp.onSelPage = on_sel_page

	local item_list = {
		[1] = {
			["txt"] = "关注",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = ChatConfig["move_hint_config"]["text_size"],
		},
		[2] = {
			["txt"] = "所有",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = ChatConfig["move_hint_config"]["text_size"],
		},
		[3] = {
			["txt"] = "自己",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = ChatConfig["move_hint_config"]["text_size"],
		},
	}

	config = ChatConfig["move_hint_config"]
	local total_width = 480
	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, config.x, config.y, "tab_bg_image.png", item_list, total_width, config.res, "tiny_black_pixel.png")
	local bg = ret_panel._move_hint:getBGObj()
	bg:setScaleX(total_width)
	ret_panel._move_hint:selItem(2)

	cur_asay_page = 1

	local function on_click( obj, idx )
		chatPage._move_hint:selItem( idx )		
		chatPage._move_chat_page._move_chat_grp:selectPage( idx )
	end

	ret_panel._move_hint.onClick = on_click
	
	ret_panel.closePanel = closeChatPagePanel

	return ret_panel
end

function showChatPagePanel(parent, x, y)
	freshing = true
	if not chatPage then
		chatPage = createChatPagePanel(parent, x, y)
		
--		request_asay_list(cur_asay_page)
		doCommentLists( 2, 1, 1 )
	else
		chatPage:setVisible(true)
	end
	return chatPage
end

function closeChatPagePanel()
	
	if chatPage and chatPage._move_chat_page._move_chat_grp._move_cb then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(chatPage._move_chat_page._move_chat_grp._move_cb)	
		chatPage._move_chat_page._move_chat_grp._move_cb = nil
	end
	clear({chatPage})
	chatPage = nil
	--chatPage:setVisible(false)
end

