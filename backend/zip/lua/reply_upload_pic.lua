-- upload page

local REPLY_PAGE = Import("lua/reply.lua")
UPLOAD_LAYOUT = Import("layout/reply_upload_pic.lua")
local UploadConfig = UPLOAD_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local uploadPage = nil

local photo_real_path = nil 
local delay_cb = nil

--状态提示
local uploadTips = nil

local function hideTips()
    if uploadTips ~= nil then
       clear({uploadTips})
       uploadTips = nil
    end
end

local function showTips(txt,delay_sec)
    hideTips()
    if uploadTips == nil then
        local d_time = 0
        if delay_sec == 0 then
            d_time = 1000*3600
        else 
            d_time = 1000*delay_sec
        end
        local option = {ms=d_time}
		if uploadPage then
			uploadTips = showMessage(uploadPage,txt,option)
		end
    end
end



--[[local function delay_dis()
	CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(delay_cb)
	delay_cb = nil

	uploadPage._photo_pic:setImage(photo_real_path)
	uploadPage._photo_pic:setAnchorPoint(0, 0)
	uploadPage._photo_pic:setScale(0.2)
	LIGHT_UI.refreshAllHttpSprite()
end--]]

local function delay_dis()
	if delay_cb then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(delay_cb)
		delay_cb = nil
	end
	
	local function cb()
		local size = uploadPage._photo_pic:getContentSize()
		local m_width = 480*0.8
		local m_height = 800*0.65
		local r_w = size.width
		local r_h = size.height
		local scale = 1
		if size.width > m_width or size.height > m_height then		 	
			if size.width/m_width > size.height/m_height then
				scale = m_width/size.width
				r_w = m_width
				r_h = size.height * scale
			else
				scale = m_height/size.height
				r_w = size.width * scale
				r_h = m_height
			end
		end		
		uploadPage._photo_pic:setScale(scale)
		uploadPage._photo_pic:setAnchorPoint(0,0)
		local x = (480 - r_w) / 2 - 98
		local y = (680 - r_h)/2 - 50
		uploadPage._photo_pic:getSprite():setPosition(x,y)			
	end
	uploadPage._photo_pic:setImage(photo_real_path,cb,true)	
	--LIGHT_UI.refreshAllHttpSprite()	
end

local function photo_cb(real_path)
	photo_real_path = real_path	
	if photo_real_path ~= "" then
	    --showTips(photo_real_path,0)
		--CCMessageBox("here",'')
		REPLY_PAGE.set_pic_path( photo_real_path )		
		delay_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(delay_dis, 0.1, false)
	else
		showTips("路径为空",0)
		--ret_panel.tips:setString("路径为空")
	end	
	
end

local function delay_take()
	xymodule.take_photo(photo_cb)
	
	CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(delay_cb)
	delay_cb = nil	
end

local function delay_sel()
	--[[photo_real_path = "C:/Users/xp001/Desktop/IMG_20130824_105722.jpg"
	delay_dis()--]]
	--CCMessageBox("here",'')
	xymodule.sel_photo(photo_cb)
	
	--CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(delay_cb)
	--delay_cb = nil

end

local function createUploadPagePanel(parent, x, y, page_info, reply_info) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = UploadConfig, nil
	local o = nil	

	--上传回调
	local function on_upload_cb()
		
		if uploadTips ~= nil then
			showTips("上传成功",0)
			--ret_panel.tips:setString("上传成功")
			--ret_panel._ok_btn:setTouchEnabled(true)
		end
		REPLY_PAGE.showReplyPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, nil, page_info, reply_info)
		closeUploadPagePanel()		
	end
	
	local function on_back(btn)
		
		REPLY_PAGE.showReplyPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, nil, page_info, reply_info)
		closeUploadPagePanel()
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '上传照片' )
	
	local function onOK(obj, x, y)
		--hideTips()
		--showTips("图片上传中，请稍候",0)
		--ret_panel.tips:setString("点击按钮")
		--showTips("点击按钮",1)
		--photo_real_path = "C:/Users/xp001/Desktop/IMG_20130824_105722.jpg"
		if not photo_real_path then
		    showTips("路径为空",0)
		   -- ret_panel.tips:setString("路径为空")
			return
		else
			REPLY_PAGE.set_pic_path( photo_real_path )
			photo_real_path = nil
			--REPLY_PAGE.showReplyPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, nil, page_info, reply_info)
			--closeUploadPagePanel()
		end				
		--[[local url = string.format("http://www.touyou.mobi/interface/album.php")
		local param = {
			["type"] = "upload",
			["sid"] = GLOBAL.sid,
			["desc"] = "",
			["kind"] = "1",
			["file"] = photo_real_path,
		}
		HTTP_REQUEST.http_upload(url, param, on_upload_cb)
		showTips("请稍候,图片上传中..",0)--]]
		REPLY_PAGE.showReplyPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, nil, page_info, reply_info)
		closeUploadPagePanel()
		
	end

	ret_panel._ok_btn = createButton( ret_panel, c.send_btn, onOK )
	
	local function onTakePhoto(btn, x, y)		
		delay_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(delay_take, 0.1, false)		
	end
	
	ret_panel._take_photo_btn = create9Button( ret_panel, c.take_btn, onTakePhoto )

	local function onSelPhoto(btn, x, y)
		
		delay_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(delay_sel, 0.1, false)
	end
	
	ret_panel._sel_photo_btn = create9Button( ret_panel, c.file_btn, delay_sel )

	ret_panel._photo_pic = LIGHT_UI.clsSprite:New(ret_panel, 100, 100, nil)
		
	
	return ret_panel
end

function showUploadPagePanel(parent, x, y, page_info, reply_info)
	--if not uploadPage then	
		uploadPage = createUploadPagePanel(parent, x, y, page_info, reply_info)
	--else
		uploadPage:setVisible(true)
	--end
	--showTips("请选择图片进行上传",0)
end

function closeUploadPagePanel()
	--uploadPage:setVisible(false)
	hideTips()
	clear({uploadPage})
	uploadPage = nil
end

