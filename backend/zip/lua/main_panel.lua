MAINPANEL_LAYOUT = Import("layout/main_panel.lua")
local mainPanelConfig = MAINPANEL_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
local ret_panel = nil

------------------
local Init = nil
local updateScorllInfo = nil
local checkVersion = nil
---------------------
local function closeMainPanel()
	
	ret_panel._top_scroll_panel._scroll_text:tryStopScroll()
	clear( { ret_panel } )
	ret_panel = nil
end



local function createTopScrollPanel(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)
	
	ret_panel.bg_pic = createSprite( ret_panel, mainPanelConfig.scroll_bg )		--公告栏底图图片

	ret_panel._scroll_text = LIGHT_UI.clsRecycleScroll:New(ret_panel, mainPanelConfig._scroll_text.x, 
	mainPanelConfig._scroll_text.y, mainPanelConfig._scroll_text.width, mainPanelConfig._scroll_text.height) --公告文字位置修改
	local txt1 = createLabel( nil, mainPanelConfig.scroll_text )
	txt1:setTextColor( 255, 255, 255 )
	ret_panel._scroll_text:addInnerObj(txt1)
	ret_panel._scroll_text:startScroll( 2 )
	
	return ret_panel
end

local function clickButtonHandler( t, x, y )
	if t._btn_res_list[ 1 ] == 'yxdt-off.png' then
		if PANEL_CONTAINER.closeChildPanel( nil, 10 ) then
			dailyCount('Gamehall')--游戏大厅
			local ROOM_PANEL = Import("lua/room.lua")			
			PANEL_CONTAINER.addChild( ROOM_PANEL.showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
		end	
	elseif t._btn_res_list[ 1 ] == 'pxb-off.png' then
		if PANEL_CONTAINER.closeChildPanel( nil, 11 ) then
			local RANK_PANEL = Import("lua/rank.lua")							
			PANEL_CONTAINER.addChild( RANK_PANEL.showRankPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
		end
		--12留给摇骰子
	--[[elseif t._btn_res_list[ 1 ] == 'xiaoxi.png' then
		if PANEL_CONTAINER.closeChildPanel( nil, 13 ) then
			local MSG_PAGE = Import("lua/message.lua")							
			PANEL_CONTAINER.addChild( MSG_PAGE.showMsgPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
		end--]]
	elseif t._btn_res_list[ 1 ] == 'bz.png' then
		local o = Import("lua/help.lua")
		o.showPanel(HTTP_CLIENT.getRootNode(), 0, 0, 2)
	end
end

local function goToGetGoldPanel()
	--[[local TASK_PAGE = Import("lua/task.lua")
	if PANEL_CONTAINER.closeChildPanel( t, 7 ) then
		PANEL_CONTAINER.addChild( TASK_PAGE.showTaskPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
	end--]]
	local function getReward( data )
		if data.code == 1 then
			local function initRewardBtn( data )
				ret_panel.btn:setString( "领取金币 (" .. data.data.day_gold .. "/3)" )
			end
			doCommand( 'user', 'money', {}, initRewardBtn )
			updateGoldText()
			showMessage( ret_panel, "又有本钱啦，骰魔重现江湖！", {ms=3000} )
		elseif data.code == -1 then			
			showMultiMessage( ret_panel, "今天的限额已用完了哦，明天请早吧，需要更多请到商城充值购买", {ms=3000} )
		elseif data.code == -2 then			
			showMultiMessage( ret_panel, "金币不低于400，不能领取免费金币哦", {ms=3000} )
		end
	end
	doCommand( 'user', 'day_gold', {}, getReward )
end

function updateGoldText()
	--回调用户信息
	local function onUserMoney( data )
		if ret_panel then
			if data.data.gold then
				--ret_panel._gold:setString( "안녕하세요" )
				ret_panel._gold:setString( data.data.gold )
			end
		end
	end
	--请求用户资料
	doCommand( 'user', 'money', {}, onUserMoney )
end

local function showGoodFriendRank( rank )
	if rank <= 10 then
		ret_panel.rank_lb:setString( "骰魔争霸，谁与争锋" )
		--ret_panel.rank_lb:setString( "好友排行：第" .. rank .. "名" )
	elseif rank <= 20 then
		ret_panel.rank_lb:setString( "骰魔争霸，谁与争锋" )
		--ret_panel.rank_lb:setString( "好友排行：" .. "还行，10+" )
	elseif rank <= 100 then
		ret_panel.rank_lb:setString( "骰魔争霸，谁与争锋" )
		--ret_panel.rank_lb:setString( "好友排行：" .. "囧，20+" )
	else
		ret_panel.rank_lb:setString( "骰魔争霸，谁与争锋" )
		--ret_panel.rank_lb:setString( "好友排行：" .. "排太远，看不见啊" )
	end
end

function createMainPanel( parent, x, y )
	
	ret_panel = createNode( parent, point( x, y ) )	
	
	--ret_panel.message = createButton( ret_panel, mainPanelConfig.message_button, clickButtonHandler )	
	
	ret_panel.bg_pic = createSprite( ret_panel, mainPanelConfig.bg_pic )
	
	ret_panel.pic = createSprite( ret_panel, mainPanelConfig.bg_config )
	
	ret_panel._top_scroll_panel = createTopScrollPanel( ret_panel, x, y )
	
	ret_panel.game_hall_btn = createButton( ret_panel, mainPanelConfig.game_hall_button, clickButtonHandler )
	ret_panel.game_hall_lb = createLabel( ret_panel, mainPanelConfig.game_hall_text )	
	ret_panel.rank_btn = createButton( ret_panel, mainPanelConfig.rank_button, clickButtonHandler )
	ret_panel.rank_lb = createLabel( ret_panel, mainPanelConfig.rank_text )
	ret_panel.dice_btn = createButton( ret_panel, mainPanelConfig.dice_button, clickButtonHandler )
	ret_panel.dice_lb = createLabel( ret_panel, mainPanelConfig.dice_text )
	
	ret_panel.question_btn = createButton( ret_panel, mainPanelConfig.question_button, clickButtonHandler )
	
	ret_panel.bottom_bg_pic = createSprite( ret_panel, mainPanelConfig.bg_bottom )
	
	local function initRewardBtn( data )
		mainPanelConfig.buy_btn.text = "领取金币 (" .. data.data.day_gold .. "/3)"
		if not ret_panel then
			return
		end
		ret_panel.btn = create9Button( ret_panel, mainPanelConfig.buy_btn, goToGetGoldPanel )	
	end
	doCommand( 'user', 'money', {}, initRewardBtn )
	
	ret_panel.gold_pic = createSprite( ret_panel, mainPanelConfig.gold_pic )
	
	ret_panel._gold = createLabel( ret_panel, mainPanelConfig.gold_text )
	ret_panel._gold:setTextColor( 197, 118, 28 )
	updateGoldText()
	local function onMsg( data )	
		if ret_panel then
			if data.data.person then
				ret_panel.game_hall_lb:setString( "千人同区 男女混战" )
			end
			if data.data.rank then
				showGoodFriendRank( data.data.rank )
				--showGoodFriendRank( 21 )
			end
		end
	end
	doCommand( 'room', 'info', {}, onMsg, 1 )
	--NEW_GAME_HALL.updateOnlineNum()
	
	ret_panel.closePanel = closeMainPanel	
	updateScorllInfo()
	checkVersion()
	return ret_panel
end		

function updateScorllInfo()
	local function onInfo(data)
		local content = data.data
		for k,v in ipairs(content) do
			if v.section == 'home' and v.key == 'activity_words' then
				local vale = tostring(v.value)
				if ret_panel then
					ret_panel._top_scroll_panel._scroll_text:setString(vale)
					return
				end
			end
		end
	end
	doCommand( 'config', 'get',{time=0}, onInfo )
end

function checkVersion()
	local function onBack(data)	
		local function download()
			showMessageX( ret_panel, "正努力为您下载新版，请先不要退出哦...", {y=400} )
			xymodule.android_download('http://www.touyou.mobi/update/touyou.apk',WRITE_PATH,'touyou.apk')
			CCUserDefault:sharedUserDefault():setStringForKey("engine",data.vercode)
		end
		local function cancel()		
			xymodule.exit_process()
		end
		if data.code >= 1 then
			local engine = CCUserDefault:sharedUserDefault():getStringForKey("engine")
			--local engine = 10			
			if tonumber(engine) < tonumber(data.data.vercode) then
				if tonumber(data.data.need) == 0 then
					alert = showUpdateWindow('发现新版本',data.data.verinfo,download)				
				else
					alert =showUpdateWindow('发现新版本',data.data.verinfo,download,cancel)	
				end
			end	
		end		
	end
	local engine_version = CCUserDefault:sharedUserDefault():getStringForKey("engine")	
	--engine_version = 10
	if string.len(tostring(engine_version)) <= 0 then
		engine_version = ENGINE_VERSION
		CCUserDefault:sharedUserDefault():setStringForKey("engine",ENGINE_VERSION)		
	end
	doCommand('system', 'version',{vercode=engine_version},onBack)				
end	


--更新提示框
function showUpdateWindow(title,content,cb,cancel)
	
	local config = mainPanelConfig.alert;
	config.title.text = title;	
	local str = "";		
	--config.content3.text = 	string.gsub(str,"<br>","\n")
	local t = split(content,"<br>")
	for k,v in ipairs(t) do
		
		if v ~= "" then
			str = str .. v .. "\n"
		end
	end
	config.content3.text = 	str
	return createAlert(ret_panel,config,cb,cancel);
end