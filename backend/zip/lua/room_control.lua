ROOM_MATCH_LAYOUT = Import("layout/room_match.lua")
local RoomMatchConfig = ROOM_MATCH_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local isAgent = false --默认为非代理状态
--------------------------
local showActMask  --显示托管提示层
--------------------------
local currentDrunkenness = 3

--获取代理状态
function IsAgent()
	return isAgent
end

local function doAgent(_root)
	_root.isAct = true
	--local function onAgent()	
	--end
	--doCommand( 'room', 'act', { room_id = id }, onAgent)	
end


local function doCancelAgent(_root)
	_root.isAct = false
	--_root:setAct(false)
	--local function onCancelAgent()	
	--end
	--doCommand( 'room', 'cancel_act', { room_id = id }, onCancelAgent)
end

function doAct(_root)
	if _root:IsWatch() == false and IsAgent() == false then
	--if IsAgent() == false then
		doAgent(_root)
		--showMessage(_root,'你已经启用托管模式')
		showActMask(_root,true)
		_root.AgentBtn:setVisible(false)
		showNoActBtn(_root,true)
		isAgent = true
	end
end

function doCancelAct(_root)
	if _root:IsWatch() == false and IsAgent() == true then
	--if IsAgent() == true then
		doCancelAgent(_root)
		--showMessage(_root,'你已经取消托管模式')
		showActMask(_root,false)
		showActBtn(_root,true)
		_root.NoAgentBtn:setVisible(false)
		isAgent = false		
	end
end

function updateDrunkGoodNum( _root )
	local function on_get_pack_data( data )	
		local ROOMGAME = Import( "lua/room_game.lua" )
		local exsitResolveDrunkGood = false
		local count = 0
		for k, v in ipairs( data.data ) do
			if v.id == "50" then		--存在解酒药
				exsitResolveDrunkGood = true					
				if ROOMGAME.getRoomGamePanelExsit() == true then
					count = count + v.quantity
				end			
			end
		end	
		if exsitResolveDrunkGood == false then
			if ROOMGAME.getRoomGamePanelExsit() == true then
				_root.drunkGoodNumTxt:setString( "0" )
			end
		else
			if count > 99 then
				_root.drunkGoodNumTxt:setString( "99+" )
			else
				_root.drunkGoodNumTxt:setString( tostring( count ) )
			end
		end
	end
	doCommand( 'props', 'my', {  }, on_get_pack_data )
end

--显示托管按钮
function showActBtn(_root,bShow)
	
	local user_info = LOGIN_PANEL.getUserData()
	local function createTopScrollPanel(parent, x, y)
		if parent.scrollTxt then
			parent.scrollTxt._scroll_text:Destroy()
			clear( { parent.scrollTxt } )
			parent.scrollTxt = nil
		end
		local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)
		
		--ret_panel.bg_pic = createSprite( ret_panel, RoomMatchConfig.scroll_bg )		--公告栏底图图片

		ret_panel._scroll_text = LIGHT_UI.clsRecycleScroll:New(ret_panel, RoomMatchConfig._scroll_text.x, 
		RoomMatchConfig._scroll_text.y, RoomMatchConfig._scroll_text.width, RoomMatchConfig._scroll_text.height, true ) --公告文字位置修改
		local txt1 = createLabel( nil, RoomMatchConfig.scroll_text )
		txt1:setTextColor( 255, 255, 255 )
		ret_panel._scroll_text:addInnerObj(txt1, 260 )
		ret_panel._scroll_text:startScroll( 1, 1 )
		
		return ret_panel
	end
	if bShow then	
		local function on_get_pack_data( data )	
			local exsitResolveDrunkGood = false
			for k, v in ipairs( data.data ) do
				if v.id == "50" then		--存在解酒药
					exsitResolveDrunkGood = true
					showMessage( _root, "操作成功", {ms = 3000} )
					--[[if _root.AgentBtn then
						_root.AgentBtn:setTouchEnabled( false )
					end--]]
					tcp_useprops(user_info.id,50,1)
					break
				end
			end
			if exsitResolveDrunkGood == false then
				showMessage( _root, "亲，缺少解酒药，到商城可补充!", {ms = 3000} )
			end
		end
		local function onClick()
			if _root:IsWatch() == false then	
				doCommand( 'props', 'my', {  }, on_get_pack_data )
			else
				showMessage( _root, "旁观状态下不允许操作", {ms = 3000} )
			end
			--doAct(_root)
				
			--_root.scrollTxt = createTopScrollPanel( _root, 50, 270 )
		end
		
		if not _root.AgentBtn then
			local c = RoomMatchConfig
			--_root.AgentBtn = createButton(_root,c["agent_btn"],onClick)
			_root.AgentBtn = createButton(_root,c["clear_wine"],onClick)
			
		else
			_root.AgentBtn:setVisible(true)
		end
		_root.drunkGoodNumTxt = createLabel( _root, RoomMatchConfig.drunk_good_num_txt )
		updateDrunkGoodNum( _root )
		isAgent = false			
	else
		if _root.AgentBtn then
			_root.AgentBtn:setVisible(false)
		end
	end		
end	

function createDrunkState( currentDrunkenness, _root, c )
	if tonumber( currentDrunkenness ) >= 3 then
		if not _root.drunkenness_btn_state_pic then
			_root.drunkenness_btn_state_pic = createSprite( _root, c.drunkenness_btn_state_pic )	
			a_play( _root.drunkenness_btn_state_pic:getSprite(),c.drunkenness_btn_state_animation,true)
		end
	end
end

--显示非托管按钮
function showNoActBtn(_root,bShow)
	if bShow then	
		local function onClick()	
			doCancelAct(_root)
		end
		if not _root.NoAgentBtn then
			local c = RoomMatchConfig
			_root.NoAgentBtn = createButton(_root,c["no_agent_btn"],onClick)
		else
			_root.NoAgentBtn:setVisible(true)
		end
		isAgent = true
	else
		if _root.NoAgentBtn then
			_root.NoAgentBtn:setVisible(false)
		end
	end
end



--退出提示窗口
function showExtTips()
		
end

function showActMask(_root,bShow)
	if bShow == true then
		if not _root.ActMask then 		
			local function onClick()
				doCancelAct(_root)
			end
			
			local order = _root:getCOObj():getChildrenCount() + 80
			local c = { x = 0,y = 0}
			local o = createNode(_root,c,nil,order)		
			_root.ActMask = createButton(o,RoomMatchConfig.act_mask,onClick)		
			_root.ActRobot = createSprite(o,RoomMatchConfig.act_robot)
		end	
		_root.ActMask:setVisible(true)
		_root.ActRobot:setVisible(true)
	else
		if _root.ActMask then
			_root.ActMask:setVisible(false)
		end
		if _root.ActRobot then
			_root.ActRobot:setVisible(false)
		end
	end
end