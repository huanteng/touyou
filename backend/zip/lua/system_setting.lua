LAYOUT = Import('layout/system_setting.lua')
local layoutConfig = LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local panel = nil
local btn_status = {}

local function on_btn_detail(data)
	showMessage( panel, data.memo )
end

local function setBtnStatus( obj )
	if obj.status == '0' then
		obj:setStatus(LIGHT_UI.NORMAL_STATUS)
	else
		obj:setStatus(LIGHT_UI.DOWN_STATUS)
	end
end

local function changeBtnStatus( obj )
	if obj.status == '1' then
		obj.status = '0'
	else
		obj.status = '1'
	end
	
	setBtnStatus( obj )
end

--更新本地设置
local function updateUserString( obj )
	changeBtnStatus( obj )
	setUserString( obj.key, obj.status )
end

--更新远程设置
local function updateRemote( obj )
	changeBtnStatus( obj )

	doCommand( 'user', 'set_config', { name = obj.key, data = obj.status }, on_btn_detail)
end

local function createPanel(parent, x, y)
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = layoutConfig, nil
	local o = nil

	local function on_back(btn)
		closePanel()
		--[[SETTING = Import('lua/setting.lua')
		SETTING.showPanel(HTTP_CLIENT.getRootNode(), 0, 0)--]]
		if PANEL_CONTAINER.closeChildPanel( nil, 15 ) then
			local SETTING = Import("lua/setting.lua")							
			PANEL_CONTAINER.addChild( SETTING.showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
		end	
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '系统设置' )
	
	--说明文字 及底图
	for i=1,2 do
		local bg = replaceTable( c.bg, c[ 'bg_'..i] )
		if i == 1 then
			createGrid( ret_panel, bg )
		end
	end

	for i=1,6 do
		local label = replaceTable( c.label, c[ 'label_'..i] )
		if i == 1 or i == 2 then
			createButton( ret_panel, label )
		end
	end		
	--说明文字 及底图 end

	--按钮
	ret_panel._back_btn = {}
	local btn = c.btn
	for i=1,5 do
		c2 = c['btn_'..i]
		btn.x, btn.y = c2.x, c2.y
		
		o = createButton( ret_panel, btn )
		o.status = 1
		ret_panel._back_btn[i] = o
	end
	for i=3,5 do
		ret_panel._back_btn[i]:setVisible( false )
	end
	--按钮 end
	
	return ret_panel
end

function showPanel(parent, x, y)
	dailyCount('SettingaSystem')--系统设置
	if not panel then
		panel = createPanel(parent, x, y)
	else
		panel:setVisible(true)
	end
	
	local function init(data)
		if data.code == 1 then
			local config = {
				[3] = 'allow_chat_vip',
				[4] = 'allow_chat_real',
				[5] = 'allow_chat_other',
			}
			
			data = data.data
			for i, key in pairs( config ) do
				o = panel._back_btn[ i ]
				o.status = data[ key ]
				o.key = key
				o.onTouchEnd = updateRemote
				setBtnStatus( o )
			end
		end
	end
	
	local config = {
		[1] = 'sound',
		[2] = 'shake',
	}
	
	for i, key in pairs( config ) do
		o = panel._back_btn[ i ]
		o.status = getUserString( key, '1' )
		o.key = key
		panel._back_btn[ i ].onTouchEnd = updateUserString
		setBtnStatus( o )
	end
	
	doCommand( 'user', 'config', {}, init)
end

function closePanel()
	--panel:setVisible(false)
	clear({panel})
	panel = nil
end