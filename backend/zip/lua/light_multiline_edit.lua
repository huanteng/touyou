-- light multiline edit

clsLightMultiEdit = clsObject:Inherit()
local BlankElement = ""
local CursorWidth = nil 

--[[
TextInfo = {
	Color = ,
	Font =,
	Size = ,
	MaxCharCount = ,
	MaxTextWidth = ,
	LineInterval = ,
	MinMsgHeight = ,
}
--]]

local DoNormalKeyProc
-- Combination Class
function clsLightMultiEdit:InitCommon(ParentObj, BaseX, BaseY, TextInfo)
	-- 控件所使用的变量初始化列表
	self._ElementRec = {} -- 记录每个字符的字节起点 StartIdx, EndIdx
	self._InsertIndex = 0 -- 插入下标 也是选择文本的起始下标
	self._NewLineCharIndexList = {}
	self._TmpCharCnt = 0
	self._MaxCharCount = TextInfo.MaxCharCount
	self._MaxTextWidth = TextInfo.MaxTextWidth
	self._MinMsgHeight = TextInfo.MinMsgHeight
	self._Font = TextInfo.Font
	self._Color = TextInfo.Color
	self._FontSize = TextInfo.Size
	self._BlinkCursorTimer = nil
	self._LineInterval = TextInfo.LineInterval

	self._CurLine = 1
	self._LineRec = {}
	self._SegIndex = 1
	--
	local function onTextMsgClick(msg_obj, x, y)
		self:OnTextLeftDown(x, y)
	end
	local function onInput(txt)
		if string.byte(txt) == 10 then
			self:TryReturnProc()
		else
			DoNormalKeyProc(self, txt)
		end
	end
	local function onDelete()
		self:DoBackProc()
	end
	self._RootNode = LIGHT_UI.clsNode:New(ParentObj, BaseX, BaseY)
	self._BaseContainerObj = LIGHT_UI.clsSimpleButton:New(self._RootNode, 0, 0, "tiny_bg_pixel.png", "tiny_bg_pixel.png") -- 鼠标定位
	self._BaseContainerObj:setButtonScaleX(self._MaxTextWidth)
	self._BaseContainerObj:setButtonScaleY(self._MinMsgHeight)
	self._BaseContainerObj:setAnchorPoint(0, 0)
	--self._BaseContainerObj:setOpacity(0) ???
	self._BaseContainerObj.onTouchEnd = onTextMsgClick 

	self._MsgEdit = CCTextFieldTTF:textFieldWithPlaceHolder("", CCSizeMake(0, 0), kCCTextAlignmentLeft, self._Font, self._FontSize)
	self._MsgEdit:setPosition(0, 0)
	self._MsgEdit:setVisible(false)
	self._RootNode:getCOObj():addChild(self._MsgEdit)
	self._MsgEdit:registerScriptInputHandler(onInput, onDelete)
	self._MsgEdit:setVisible(false)

	self._TextCursor = CCSprite:create("cursor.png")
	self._TextCursor:setVisible(false)
	self._RootNode:getCOObj():addChild(self._TextCursor)
	self._TextCursor:setPosition(0, 0)
	self._TextCursor:setAnchorPoint(CCPointMake(0, 1))
	local size = self._TextCursor:getContentSize()
	CursorWidth = size.width

	self._LineRec[self._CurLine] = {}
	self._LineRec[self._CurLine].StartIdx = 0
	self._LineRec[self._CurLine].EndIdx = 0
	self._LineRec[self._CurLine].Segment = self._SegIndex 

	self._HideTextObj = CCLabelTTF:create(BlankElement, self._Font, self._FontSize, CCSizeMake(0, 0), kCCTextAlignmentLeft)
	self._HideTextObj:setVisible(false)
	self._RootNode:getCOObj():addChild(self._HideTextObj)
end

function clsLightMultiEdit:__init__(ParentObj, BaseX, BaseY, TextInfo)
	self:InitCommon(ParentObj, BaseX, BaseY, TextInfo)

	self._TextBuffer = BlankElement

	self._TextObjList = {}
	local TempTextObj = LIGHT_UI.clsLabel:New(self._RootNode, 0, self._MinMsgHeight, "", TextInfo.Font, TextInfo.Size)
	TempTextObj:setAnchorPoint(0, 1)
	TempTextObj:setTextColor(self._Color[1], self._Color[2], self._Color[3])
	self._TextObjList[self._CurLine] = TempTextObj
	local width, height = self:CalElementSize(" ")
	self._LineHeight = height
end

function clsLightMultiEdit:getVisible()
	return self._RootNode:getVisible()
end

function clsLightMultiEdit:getCOObj()
	return self._RootNode
end

local BlinkFre = 0.3 
function clsLightMultiEdit:CalElementSize(ElementList)
	self._HideTextObj:setString(ElementList)
	local size = self._HideTextObj:getContentSize()
	return size.width, size.height
end

local function ShowCursor(EditObj)

	local function DoBlinkCursor()
		if EditObj._TextCursor:isVisible() then
			EditObj._TextCursor:setVisible(false)
		else
			EditObj._TextCursor:setVisible(true)
		end
	end

	if not TIMER or not TIMER['_BlinkCursorTimer'] then
		--EditObj._BlinkCursorTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(DoBlinkCursor, BlinkFre, false)
		TIMER['_BlinkCursorTimer'] = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(DoBlinkCursor, BlinkFre, false)
	end

	EditObj._TextCursor:setVisible(true)
end

local function HideCursor(EditObj)
	if EditObj._BlinkCursorTimer then
		--CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(EditObj._BlinkCursorTimer)
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(TIMER['_BlinkCursorTimer'])
		EditObj._BlinkCursorTimer = nil
	end

	EditObj._TextCursor:setVisible(false)
end

function clsLightMultiEdit:GetSubElement(BeginIndex, EndIndex)
	if BeginIndex <= 0 then
		return ""
	else
		return string.sub(self._TextBuffer, self._ElementRec[BeginIndex].StartIdx, self._ElementRec[EndIndex].EndIdx)
	end
end

local function GetLineEleSize(EditObj, LineIndex)
	local EleList = EditObj:GetSubElement(EditObj._LineRec[LineIndex].StartIdx, EditObj._LineRec[LineIndex].EndIdx)
	return EditObj:CalElementSize(EleList)
end

local function CalRelaIndexByMouseX(EditObj, LineIndex, TextMouseX)
	if EditObj._LineRec[LineIndex].StartIdx <= 0 then
		return 0 
	end

	local TextWidth, _ = GetLineEleSize(EditObj, LineIndex)
	if TextMouseX >= TextWidth then
		return EditObj._LineRec[LineIndex].EndIdx - EditObj._LineRec[LineIndex].StartIdx + 1
	end

	if TextMouseX <= 0 then
		return 0
	end

	local ExpandStr = BlankElement 
	local CharIndex = 0 
	local LastTextWidth = 0
	local TextRec = EditObj._ElementRec
	for CharIndex = EditObj._LineRec[LineIndex].StartIdx, EditObj._LineRec[LineIndex].EndIdx do
		ExpandStr = EditObj:GetSubElement(EditObj._LineRec[LineIndex].StartIdx, CharIndex)

		local TextWidth, _ = EditObj:CalElementSize(ExpandStr)
		if TextWidth > TextMouseX then
			if ((TextWidth + LastTextWidth) / 2) > TextMouseX then
				return CharIndex - EditObj._LineRec[LineIndex].StartIdx

			else
				return CharIndex - EditObj._LineRec[LineIndex].StartIdx + 1

			end
		end

		LastTextWidth = TextWidth
	end
end

local UnicodeCharCode = 256
local function ParsChar(CharCode)
	if CharCode < UnicodeCharCode then
		return string.char(CharCode), 1
	else
		return string.char(math.mod(CharCode, UnicodeCharCode), math.floor(CharCode/UnicodeCharCode)), 2
	end
end

local ANSCOde = 128
local function UnParsChar(Char)
	if string.byte(Char) > ANSCOde then
		return 2
	else
		return 1
	end
end

function clsLightMultiEdit:CalTextObjY(LineIndex)
	return self._MinMsgHeight - (self._LineHeight + self._LineInterval) * (LineIndex - 1)
end

function clsLightMultiEdit:InsertChar(Char, ElementSize)
	local InsertIndex = self._InsertIndex
	local TextRec = self._ElementRec

	local PreStr = BlankElement 
	local TailStr = BlankElement 

	if InsertIndex > 0 then
		PreStr = self:GetSubElement(1, InsertIndex)
	end

	local CharCount = #(TextRec)
	if InsertIndex < CharCount then
		TailStr = self:GetSubElement(InsertIndex + 1, CharCount)
	end

	self._TextBuffer = string.format("%s%s%s", PreStr, Char, TailStr)

	local TextRec = self._ElementRec
	local AfterIndex = InsertIndex + 1 
	for i = AfterIndex, #(TextRec) do
		local CharInfo = TextRec[i]
		CharInfo.StartIdx = CharInfo.StartIdx + ElementSize 
		CharInfo.EndIdx = CharInfo.EndIdx + ElementSize 
	end

	local InsertNode = {} 
	if InsertIndex <= 0 then
		InsertNode.StartIdx = 1
	else
		InsertNode.StartIdx = TextRec[InsertIndex].EndIdx + 1
	end

	InsertNode.EndIdx = InsertNode.StartIdx + ElementSize - 1

	table.insert(TextRec, AfterIndex, InsertNode)

	self._InsertIndex = self._InsertIndex + 1
end

function clsLightMultiEdit:DeleteSubElement(BeginIndex, EndIndex)
	local PreStr = BlankElement 
	local TailStr = BlankElement 

	local TextRec = self._ElementRec
	if BeginIndex > 1 then
		PreStr = self:GetSubElement(1, BeginIndex - 1)
	end

	local CharCount = #(TextRec)
	if EndIndex < CharCount then
		TailStr = self:GetSubElement(EndIndex + 1, CharCount)
	end

	self._TextBuffer = string.format("%s%s", PreStr, TailStr)

	local DeleteCharSize = 0
	for i = BeginIndex, EndIndex do
		local DeleteNode = TextRec[i]
		DeleteCharSize = DeleteCharSize + DeleteNode.EndIdx - DeleteNode.StartIdx + 1
	end

	local AfterIndex = EndIndex + 1
	if AfterIndex <= CharCount then
		for i = AfterIndex, CharCount do
			TextRec[i].StartIdx = TextRec[i].StartIdx - DeleteCharSize
			TextRec[i].EndIdx = TextRec[i].EndIdx - DeleteCharSize
		end
	end

	local RemoveCount = EndIndex - BeginIndex + 1
	for i = 1, RemoveCount do
		table.remove(TextRec, BeginIndex)
	end
end

function clsLightMultiEdit:CalIndexPixelPosX(LineIndex, RelaIndex)
	local DisplayBeginIdx = self._LineRec[LineIndex].StartIdx
	if DisplayBeginIdx <= 0 then
		return 0
	end

	local TextRec = self._ElementRec
	if RelaIndex < (DisplayBeginIdx - self._LineRec[LineIndex].StartIdx + 1) then
		return 0
	else
		local TempElementList = self:GetSubElement(DisplayBeginIdx, DisplayBeginIdx + RelaIndex - 1)
		local PixelWidth, _ = self:CalElementSize(TempElementList)

		return PixelWidth
	end
end

local function CalRelaIndex(EditObj, LineIndex, AbsIndex)
	if EditObj._LineRec[LineIndex].StartIdx == 0 then
		return 0
	else
		return (AbsIndex - EditObj._LineRec[LineIndex].StartIdx + 1)
	end
end

local function CalPosByIndex(EditObj, LineIndex, RelaIndex)
	local CursorPixelPosX = EditObj:CalIndexPixelPosX(LineIndex, RelaIndex)
	local CursorPixelPosY = EditObj:CalTextObjY(LineIndex)

	return CursorPixelPosX, CursorPixelPosY
end

local function DisplayCursorByIndex(EditObj, LineIndex, RelaIndex)
	local CursorPixelPosX, CursorPixelPosY = CalPosByIndex(EditObj, LineIndex, RelaIndex)

	EditObj._TextCursor:setPosition(CursorPixelPosX, CursorPixelPosY)
	--EditObj._TextCursor:SetSize(CursorWidth, EditObj:GetLineHeight(LineIndex)) ???
end

function clsLightMultiEdit:UnDisplayLine(LineIndex)
	self._TextObjList[LineIndex]:setString("")
end

local function UnDisplay(EditObj, BeginLine, EndLine)
	for i = BeginLine, EndLine do
		EditObj:UnDisplayLine(i)
	end
end

function clsLightMultiEdit:DisplayLine(LineIndex)
	local EleList = self:GetSubElement(self._LineRec[LineIndex].StartIdx, self._LineRec[LineIndex].EndIdx)
	local TextObj = self._TextObjList[LineIndex]
	TextObj:setString(EleList)
	local PosX = 0
	local PosY = self:CalTextObjY(LineIndex)
	TextObj:setPosition(PosX, PosY)
end

function clsLightMultiEdit:setPosition(x, y)
	self._RootNode:setPosition(x, y)
end

function clsLightMultiEdit:getPosition()
	return self._RootNode:getPosition()
end

function clsLightMultiEdit:getContentSize()
	return { width = self.MaxTextWidth, height = self._MinMsgHeight, }
end

local function Display(EditObj, BeginLine, EndLine)
	for i = BeginLine, EndLine do
		EditObj:DisplayLine(i)
	end
end

local function RefreshDisplay(EditObj, BeginLine, EndLine)
	UnDisplay(EditObj, BeginLine, EndLine)
	Display(EditObj, BeginLine, EndLine)
end

local function MergeSegment(EditObj, BeginIndex, OldSegment, NewSegment)
	while ((EditObj._LineRec[BeginIndex]) and (EditObj._LineRec[BeginIndex].Segment == OldSegment)) do
		EditObj._LineRec[BeginIndex].Segment = NewSegment
		BeginIndex = BeginIndex + 1
	end
end

local TryAdjustLayout
local function DoReturnEffect(EditObj, LineIndex, AbsIndex)
	EditObj._SegIndex = EditObj._SegIndex + 1
	local CurLine = LineIndex
	local TailCnt = 0
	if EditObj._LineRec[CurLine].StartIdx > 0 then
		TailCnt = EditObj._LineRec[CurLine].EndIdx - AbsIndex
	end

	EditObj._LineRec[CurLine].EndIdx = EditObj._LineRec[CurLine].EndIdx - TailCnt
	if EditObj._LineRec[CurLine].EndIdx < EditObj._LineRec[CurLine].StartIdx then
		EditObj._LineRec[CurLine].StartIdx = 0
		EditObj._LineRec[CurLine].EndIdx = 0
	end

	local InitStartIdx = 0
	local InitEndIdx = 0
	if TailCnt > 0 then
		InitStartIdx = AbsIndex + 1
		InitEndIdx = AbsIndex + TailCnt
	end

	local NewLineIndex = CurLine + 1
	EditObj:NewLine(NewLineIndex, InitStartIdx, InitEndIdx, EditObj._SegIndex)

	local OldSegment = EditObj._LineRec[CurLine].Segment
	local NewSegment = EditObj._LineRec[CurLine + 1].Segment
	MergeSegment(EditObj, CurLine + 2, OldSegment, NewSegment)

	TryAdjustLayout(EditObj, CurLine + 1, NewSegment)
	RefreshDisplay(EditObj, CurLine, #(EditObj._LineRec))

	return NewLineIndex
end

function clsLightMultiEdit:DoReturnProc()
	self._CurLine = DoReturnEffect(self, self._CurLine, self._InsertIndex)

	local RelaIndex = CalRelaIndex(self, self._CurLine, self._InsertIndex)
	DisplayCursorByIndex(self, self._CurLine, RelaIndex)
end

local function CalSelTextIndexRange(EditObj)
	return nil, nil 
end

local function GetDeleteRange(EditObj)
	return EditObj._InsertIndex, EditObj._InsertIndex
end

function clsLightMultiEdit:TryReturnProc()
	self:DoReturnProc()
end

function clsLightMultiEdit:ResetEdit()
	self._ElementRec = {}
	self._TextBuffer = BlankElement 
	self._InsertIndex = 0
	self._TextCursor:setPosition(0, 0)
	for _, TextObj in pairs(self._TextObjList) do
		TextObj:setString(BlankElement)
	end

	self._CurLine = 1
	self._LineRec = {}
	self._LineRec[self._CurLine] = {}
	self._LineRec[self._CurLine].StartIdx = 0
	self._LineRec[self._CurLine].EndIdx = 0
	self._SegIndex = 1
	self._LineRec[self._CurLine].Segment = self._SegIndex 
end

function clsLightMultiEdit:ConvertElementToText(ElementList)
	return ElementList
end

local function CalLineByIndex(EditObj, Index)
	if Index <= 0 then
		return 1
	end

	for LineIndex, LineInfo in ipairs(EditObj._LineRec) do
		if Index >= LineInfo.StartIdx and Index <= LineInfo.EndIdx then
			return LineIndex
		end
	end

	return nil
end

local function GetOverrideRange(Begin1, End1, Begin2, End2)
	local RetBegin = 0
	local RetEnd = 0

	if Begin1 > Begin2 then
		RetBegin = Begin1
	else
		RetBegin = Begin2
	end

	if End1 > End2 then
		RetEnd = End2
	else
		RetEnd = End1
	end

	return RetBegin, RetEnd
end

local IgnoreChar = "\r"
local NewLineChar = "\n"

function clsLightMultiEdit:GetTextByRange(BeginIndex, EndIndex)
	local BeginLine = CalLineByIndex(self, BeginIndex)
	local EndLine = CalLineByIndex(self, EndIndex)
	local RetText = ""
	local PreSegment = nil 
	for i = BeginLine, EndLine do
		if self._LineRec[i].StartIdx > 0 then
			if PreSegment and PreSegment ~= self._LineRec[i].Segment then
				RetText = RetText .. IgnoreChar .. NewLineChar
			end
			PreSegment = self._LineRec[i].Segment
			local TempBeginIndex, TempEndIndex = GetOverrideRange(self._LineRec[i].StartIdx, self._LineRec[i].EndIdx, BeginIndex, EndIndex)
			local SubElement = self:GetSubElement(TempBeginIndex, TempEndIndex)
			local TempText = self:ConvertElementToText(SubElement)
			RetText = RetText .. TempText
		else
			RetText = RetText .. IgnoreChar .. NewLineChar
		end
	end

	return RetText
end

function clsLightMultiEdit:getText()
	local EleCount = #(self._ElementRec)
	if EleCount <= 0 then
		return ""
	end

	return self:GetTextByRange(1, EleCount)
end

function clsLightMultiEdit:NewLine(LineIndex, StartIdx, EndIdx, Segment)
	table.insert(self._LineRec, LineIndex, {})
	self._LineRec[LineIndex].StartIdx = StartIdx 
	self._LineRec[LineIndex].EndIdx = EndIdx 
	self._LineRec[LineIndex].Segment = Segment

	local TempTextObj = self._TextObjList[LineIndex]
	if TempTextObj then
		TempTextObj = LIGHT_UI.clsLabel:New(self._RootNode, 0, self:CalTextObjY(LineIndex), BlankElement, self._Font, self._FontSize)
		table.insert(self._TextObjList, LineIndex, TempTextObj)
	end
	TempTextObj:setAnchorPoint(0, 1)
	TempTextObj:setTextColor(self._Color[1], self._Color[2], self._Color[3])
end

function clsLightMultiEdit:DeleteLine(LineIndex)
	table.remove(self._LineRec, LineIndex)
	self._TextObjList[LineIndex]:removeFromParentAndCleanup(true)
	table.remove(self._TextObjList, LineIndex)
end

local function GetTailOutEleCount(EditObj, LineIndex)
	local BeginIndex = EditObj._LineRec[LineIndex].StartIdx
	local TempEndIndex = BeginIndex
	local ElementList = EditObj:GetSubElement(BeginIndex, TempEndIndex)
	local SumWidth = 0
	local EleWidth, _ = EditObj:CalElementSize(ElementList)
	SumWidth = EleWidth
	while ((SumWidth + CursorWidth) <= EditObj._MaxTextWidth) do
		TempEndIndex = TempEndIndex + 1
		if TempEndIndex > EditObj._LineRec[LineIndex].EndIdx then
			return 0
		end

		ElementList = EditObj:GetSubElement(BeginIndex, TempEndIndex)
		EleWidth, _ = EditObj:CalElementSize(ElementList)
		SumWidth = EleWidth
	end

	return EditObj._LineRec[LineIndex].EndIdx - TempEndIndex + 1 
end

local function CalTailFreeSpace(EditObj, LineIndex)
	if EditObj._LineRec[LineIndex].StartIdx == -1 then
		return EditObj._MaxTextWidth
	end

	local ElementList = EditObj:GetSubElement(EditObj._LineRec[LineIndex].StartIdx, EditObj._LineRec[LineIndex].EndIdx)
	local TempWidth, _ = EditObj:CalElementSize(ElementList)

	return (EditObj._MaxTextWidth - TempWidth)
end

local function GetFitHeadEleCount(EditObj, Space, LineIndex)
	local TempEndIndex = EditObj._LineRec[LineIndex].StartIdx
	while (TempEndIndex <= EditObj._LineRec[LineIndex].EndIdx) do
		local ElementList = EditObj:GetSubElement(EditObj._LineRec[LineIndex].StartIdx, TempEndIndex)
		local TempWidth, _ = EditObj:CalElementSize(ElementList) + CursorWidth
		if TempWidth > Space then
			break
		end

		TempEndIndex = TempEndIndex + 1
	end

	return (TempEndIndex - EditObj._LineRec[LineIndex].StartIdx)
end

TryAdjustLayout = function(EditObj, LineIndex, Segment)
	if (not EditObj._LineRec[LineIndex]) then
		return
	end

	if (EditObj._LineRec[LineIndex].Segment ~= Segment) then
		return
	end

	if EditObj._LineRec[LineIndex].StartIdx == -1 and LineIndex > 1 then
		EditObj:DeleteLine(LineIndex)
		TryAdjustLayout(EditObj, LineIndex, Segment)
		return
	end

	local ElementList = EditObj:GetSubElement(EditObj._LineRec[LineIndex].StartIdx, EditObj._LineRec[LineIndex].EndIdx)
	local TempWidth, _ = EditObj:CalElementSize(ElementList)
	if ((TempWidth + CursorWidth) > EditObj._MaxTextWidth) then 
		local BackEleCount = GetTailOutEleCount(EditObj, LineIndex)
		if (not EditObj._LineRec[LineIndex + 1]) or (EditObj._LineRec[LineIndex + 1].Segment ~= Segment) then
			EditObj._LineRec[LineIndex].EndIdx = EditObj._LineRec[LineIndex].EndIdx - BackEleCount
			EditObj:NewLine(LineIndex + 1, EditObj._LineRec[LineIndex].EndIdx + 1, EditObj._LineRec[LineIndex].EndIdx + BackEleCount, EditObj._LineRec[LineIndex].Segment)

			TryAdjustLayout(EditObj, LineIndex + 1, Segment)

			return
		elseif EditObj._LineRec[LineIndex + 1].StartIdx == -1 then
			EditObj:DeleteLine(LineIndex + 1)
			TryAdjustLayout(EditObj, LineIndex, Segment)

			return
		else
			EditObj._LineRec[LineIndex].EndIdx = EditObj._LineRec[LineIndex].EndIdx - BackEleCount
			EditObj._LineRec[LineIndex + 1].StartIdx = EditObj._LineRec[LineIndex + 1].StartIdx - BackEleCount
		end
	else
		if (not EditObj._LineRec[LineIndex + 1]) then

			return
		end

		if (EditObj._LineRec[LineIndex + 1].StartIdx == -1) then
			EditObj:DeleteLine(LineIndex + 1)
			TryAdjustLayout(EditObj, LineIndex, Segment)

			return
		elseif (EditObj._LineRec[LineIndex + 1].Segment ~= Segment) then

			return
		end

		local LeftSpace = CalTailFreeSpace(EditObj, LineIndex)
		local FitEleCount = GetFitHeadEleCount(EditObj, LeftSpace, LineIndex + 1)
		if FitEleCount <= 0 then
			TryAdjustLayout(EditObj, LineIndex + 1, Segment)

			return
		end

		if EditObj._LineRec[LineIndex].StartIdx == 0 then
			EditObj._LineRec[LineIndex].StartIdx = EditObj._LineRec[LineIndex + 1].StartIdx
			EditObj._LineRec[LineIndex].EndIdx = EditObj._LineRec[LineIndex].StartIdx + FitEleCount - 1
		else
			EditObj._LineRec[LineIndex].EndIdx = EditObj._LineRec[LineIndex].EndIdx + FitEleCount
		end

		EditObj._LineRec[LineIndex + 1].StartIdx = EditObj._LineRec[LineIndex + 1].StartIdx + FitEleCount
		if EditObj._LineRec[LineIndex + 1].StartIdx > EditObj._LineRec[LineIndex + 1].EndIdx then
			EditObj:DeleteLine(LineIndex + 1)
			TryAdjustLayout(EditObj, LineIndex, Segment)

			return
		end
	end

	TryAdjustLayout(EditObj, LineIndex + 1, Segment)
end

function clsLightMultiEdit:GetEditHeight()
	local LineCount = #(self._LineRec)
	return LineCount * (self._LineHeight + self._LineInterval)
end

local function SetClearIndex(EditObj, LineIndex)
	if (EditObj._LineRec[LineIndex - 1] and EditObj._LineRec[LineIndex - 1].Segment == EditObj._LineRec[LineIndex].Segment) then
		EditObj._LineRec[LineIndex].StartIdx = -1 
		EditObj._LineRec[LineIndex].EndIdx = -1 
	else
		EditObj._LineRec[LineIndex].StartIdx = 0 
		EditObj._LineRec[LineIndex].EndIdx = 0 
	end
end

function clsLightMultiEdit:DoInsertNewLineChar()
	table.insert(self._NewLineCharIndexList, self._InsertIndex)
	self._TmpCharCnt = self._TmpCharCnt - 1
end

function clsLightMultiEdit:PreProcInputStr(InputStr)
	return InputStr
end

function clsLightMultiEdit:CalStrEleCnt(Str)
	local StringLen = string.len(Str)
	local StartIndex = 1

	local RetCnt = 0
	while StartIndex <= StringLen do
		RetCnt = RetCnt + 1
		local Char = string.sub(Str, StartIndex, StartIndex)
		local CharByteCount = UnParsChar(Char)
		StartIndex = StartIndex + CharByteCount
	end

	return RetCnt
end

local function FindFirstLineBySegment(EditObj, BeginLine, Segment)
	local SumLine = #EditObj._LineRec
	for i = BeginLine, SumLine do
		if EditObj._LineRec[i].Segment == Segment then
			return i
		end
	end
end

function clsLightMultiEdit:AddEleAdjustLine(EleCount)
	local BeginEditLine = self._CurLine
	local BeginRefreshLine = BeginEditLine
	local AdjustLineList = {}
	table.insert(AdjustLineList, self._LineRec[BeginRefreshLine].Segment)

	if self._LineRec[BeginEditLine].StartIdx == 0 then
		self._LineRec[BeginEditLine].StartIdx = self._InsertIndex - EleCount + 1
		self._LineRec[BeginEditLine].EndIdx = self._LineRec[BeginEditLine].StartIdx + EleCount - 1
	else
		self._LineRec[BeginEditLine].EndIdx = self._LineRec[BeginEditLine].EndIdx + EleCount 
	end

	local LineCount = #(self._LineRec)
	if BeginEditLine < LineCount then
		for i = BeginEditLine + 1, LineCount do
			if self._LineRec[i].StartIdx > 0 then
				self._LineRec[i].StartIdx = self._LineRec[i].StartIdx + EleCount 
				self._LineRec[i].EndIdx = self._LineRec[i].EndIdx + EleCount 
			end
		end
	end

	local TempBeginLine = BeginEditLine
	for _, TempIndex in ipairs(self._NewLineCharIndexList) do
		local TempBeginIdx = TempIndex + 1
		local TempEndIdx = self._LineRec[TempBeginLine].EndIdx
		if TempEndIdx < TempBeginIdx then
			TempBeginIdx = 0
			TempEndIdx = 0
		end
		self._SegIndex = self._SegIndex + 1
		self:NewLine(TempBeginLine + 1, TempBeginIdx, TempEndIdx, self._SegIndex)
		table.insert(AdjustLineList, self._SegIndex)

		if self._LineRec[TempBeginLine].StartIdx > 0 then
			self._LineRec[TempBeginLine].EndIdx = TempIndex
			if self._LineRec[TempBeginLine].EndIdx < self._LineRec[TempBeginLine].StartIdx then
				self._LineRec[TempBeginLine].StartIdx = 0
				self._LineRec[TempBeginLine].EndIdx = 0
			end
		end

		TempBeginLine = TempBeginLine + 1
	end
	self._NewLineCharIndexList = {}

	for _, AdjustSegment in ipairs(AdjustLineList) do
		local LineIndex = FindFirstLineBySegment(self, BeginEditLine, AdjustSegment)
		TryAdjustLayout(self, LineIndex, AdjustSegment)
	end

	RefreshDisplay(self, BeginRefreshLine, #(self._LineRec))
end

function clsLightMultiEdit:SubEleAdjustLine(EleCount)
        local BeginEditLine = self._CurLine
        local BeginRefreshLine = BeginEditLine

	local TempStartLine = BeginEditLine
	local LeftEleCount = self._LineRec[TempStartLine].EndIdx - self._LineRec[TempStartLine].StartIdx + 1  
	local TempEleCount = -EleCount
	while (LeftEleCount < TempEleCount) do
		SetClearIndex(self, TempStartLine)
		TempEleCount = TempEleCount - LeftEleCount
		TempStartLine = TempStartLine - 1
		LeftEleCount = self._LineRec[TempStartLine].EndIdx - self._LineRec[TempStartLine].StartIdx + 1  
	end  

	self._LineRec[TempStartLine].EndIdx = self._LineRec[TempStartLine].EndIdx - TempEleCount 
	BeginRefreshLine = TempStartLine
	if self._LineRec[TempStartLine].EndIdx < self._LineRec[TempStartLine].StartIdx then 
		SetClearIndex(self, TempStartLine)
		BeginRefreshLine = TempStartLine - 1
		if BeginRefreshLine <= 0 then
			BeginRefreshLine = 1
		end
	end  

	local LineCount = #(self._LineRec)
	if BeginEditLine < LineCount then 
		for i = BeginEditLine + 1, LineCount do
			if (self._LineRec[i].StartIdx > 0) then 
				self._LineRec[i].StartIdx = self._LineRec[i].StartIdx - (-EleCount)
				self._LineRec[i].EndIdx = self._LineRec[i].EndIdx - (-EleCount)
			end  
		end  
	end

	TryAdjustLayout(self, BeginRefreshLine, self._LineRec[BeginRefreshLine].Segment)
	RefreshDisplay(self, BeginRefreshLine, #(self._LineRec))
end

function clsLightMultiEdit:DoArrangeLine(EleCount)
	if EleCount > 0 then
		self:AddEleAdjustLine(EleCount)
	end
	if EleCount < 0 then
		self:SubEleAdjustLine(EleCount)
	end
	if EleCount == 0 then
		local BeginRefreshLine = self._CurLine - 1
		if BeginRefreshLine <= 0 then
			BeginRefreshLine = 1
		end
		TryAdjustLayout(self, BeginRefreshLine, self._LineRec[BeginRefreshLine].Segment)
		RefreshDisplay(self, BeginRefreshLine, #(self._LineRec))
	end

	self._CurLine = CalLineByIndex(self, self._InsertIndex)
	local RelaIndex = CalRelaIndex(self, self._CurLine, self._InsertIndex)
	DisplayCursorByIndex(self, self._CurLine, RelaIndex)
end

local function DoInsertString(EditObj, Str)
	Str = EditObj:PreProcInputStr(Str)
	local StringLen = string.len(Str)
	local StartIndex = 1
	local PreInsertIndex = EditObj._InsertIndex
	EditObj._NewLineCharIndexList = {}
	EditObj._TmpCharCnt = EditObj._MaxCharCount
	while StartIndex <= StringLen do
		local sub_str = string.sub(Str, StartIndex)
		local CharByteCount = LIGHT_UI.getByteCount(sub_str, 1)
		local Char = string.sub(sub_str, 1, CharByteCount)

		if Char ~= IgnoreChar then
			if Char == NewLineChar then
				EditObj:DoInsertNewLineChar()
			else
				EditObj:InsertChar(Char, CharByteCount)
			end

			if #(EditObj._ElementRec) >= EditObj._TmpCharCnt then
				break
			end
		end
		StartIndex = StartIndex + CharByteCount
	end

	local AfterInsertIndex = EditObj._InsertIndex
	local EleCount = AfterInsertIndex - PreInsertIndex

	EditObj:DoArrangeLine(EleCount)
end

function clsLightMultiEdit:setString(Text)
	self:ResetEdit()
	DoInsertString(self, Text)
end

local function DoDeleteSubElement(EditObj, BeginIndex, EndIndex, BeginLine, EndLine)
	EditObj:DeleteSubElement(BeginIndex, EndIndex)
	local EleCount = EndIndex - BeginIndex + 1

	if BeginLine == EndLine then
		EditObj._LineRec[BeginLine].EndIdx = EditObj._LineRec[BeginLine].EndIdx - EleCount
		if EditObj._LineRec[BeginLine].EndIdx < EditObj._LineRec[BeginLine].StartIdx then
			SetClearIndex(EditObj, BeginLine)
		end
	else
		local TempBeginLine = BeginLine
		local NewSegment = EditObj._LineRec[TempBeginLine].Segment
		for i = BeginLine, EndLine do
			EditObj._LineRec[i].Segment = NewSegment
		end

		while (EditObj._LineRec[TempBeginLine].StartIdx == 0) do
			SetClearIndex(EditObj, TempBeginLine)
			TempBeginLine = TempBeginLine + 1
		end

		EditObj._LineRec[TempBeginLine].EndIdx = BeginIndex - 1
		if EditObj._LineRec[TempBeginLine].EndIdx < EditObj._LineRec[TempBeginLine].StartIdx then
			SetClearIndex(EditObj, TempBeginLine)
		end

		local TempEndLine = EndLine
		while (EditObj._LineRec[TempEndLine].StartIdx == 0) do
			SetClearIndex(EditObj, TempEndLine)
			TempEndLine = TempEndLine - 1
		end

		if TempEndLine > TempBeginLine then
			if EndIndex == EditObj._LineRec[TempEndLine].EndIdx then
				EditObj._LineRec[TempEndLine].StartIdx = -1 
				EditObj._LineRec[TempEndLine].EndIdx = -1 
			else
				EditObj._LineRec[TempEndLine].StartIdx = BeginIndex
				EditObj._LineRec[TempEndLine].EndIdx = EditObj._LineRec[TempEndLine].EndIdx - EleCount
			end
		end

		local ClearBeginLine = TempBeginLine + 1
		local ClearEndLine = TempEndLine - 1
		if ClearEndLine >= ClearBeginLine then
			for i = ClearBeginLine, ClearEndLine do
				EditObj._LineRec[i].StartIdx = -1 
				EditObj._LineRec[i].EndIdx = -1 
			end
		end
	end

	local LineCount = #(EditObj._LineRec)
	if EndLine < LineCount then
		for i = EndLine + 1, LineCount do
			if EditObj._LineRec[i].StartIdx > 0 then
				EditObj._LineRec[i].StartIdx = EditObj._LineRec[i].StartIdx - EleCount 
				EditObj._LineRec[i].EndIdx = EditObj._LineRec[i].EndIdx - EleCount 
			end
		end
	end

	TryAdjustLayout(EditObj, BeginLine, EditObj._LineRec[BeginLine].Segment)
end

local function HasNextNonBlankLine(EditObj, LineIndex)
	return EditObj._LineRec[LineIndex + 1]
end

function clsLightMultiEdit:DoDelete(BeginIndex, EndIndex)
	if BeginIndex <= 0 then
		return
	end

	local BeginLine = 0
	local EndLine = 0
	BeginLine = CalLineByIndex(self, BeginIndex)
	EndLine = CalLineByIndex(self, EndIndex)

	local RefreshBeginLine = BeginLine
	DoDeleteSubElement(self, BeginIndex, EndIndex, BeginLine, EndLine)

	RefreshDisplay(self, RefreshBeginLine, #(self._LineRec))

	self._InsertIndex = BeginIndex - 1
	self._CurLine = CalLineByIndex(self, self._InsertIndex)
	if self._InsertIndex == self._LineRec[self._CurLine].EndIdx and self._LineRec[self._CurLine + 1] then
		self._CurLine = BeginLine 
	end

	local RelaIndex = CalRelaIndex(self, self._CurLine, self._InsertIndex)
	DisplayCursorByIndex(self, self._CurLine, RelaIndex)

	if #(self._ElementRec) <= 0 then
		self:ResetEdit()
	end
end

function clsLightMultiEdit:DoBackProc()
	local CurLine = self._CurLine
	local RelaIndex = CalRelaIndex(self, CurLine, self._InsertIndex)
	local BeginSel, _ = CalSelTextIndexRange(self)
	if not BeginSel and RelaIndex <= 0 and self._LineRec[CurLine - 1] and self._LineRec[CurLine].Segment ~= self._LineRec[CurLine - 1].Segment then
		if self._LineRec[CurLine].StartIdx == 0 then
			self:DeleteLine(CurLine)
			RefreshDisplay(self, CurLine, #(self._LineRec))
		else
			local OldSegment = self._LineRec[CurLine].Segment
			local NewSegment = self._LineRec[CurLine - 1].Segment
			MergeSegment(self, CurLine, OldSegment, NewSegment)
			TryAdjustLayout(self, CurLine - 1, NewSegment)
			RefreshDisplay(self, CurLine - 1, #(self._LineRec))
		end

		self._CurLine = self._CurLine - 1
		local RelaIndex = CalRelaIndex(self, self._CurLine, self._InsertIndex)
		DisplayCursorByIndex(self, self._CurLine, RelaIndex)
	else
		local BeginIndex, EndIndex = GetDeleteRange(self)
		self:DoDelete(BeginIndex, EndIndex)
	end
end

local function EditInsertStr(EditObj, Str)
	DoInsertString(EditObj, Str)
end

function clsLightMultiEdit:GetLineHeight(LineIndex)
	return self._LineHeight
end

DoNormalKeyProc = function(EditObj, Char)
	if (#(EditObj._ElementRec) >= (EditObj._MaxCharCount)) then
		return
	end

	EditInsertStr(EditObj, Char)
end

function clsLightMultiEdit:CalLineByMouseY(MouseY)
	local TempLine = math.ceil((self._MinMsgHeight - MouseY) / (self._LineHeight + self._LineInterval))
	if TempLine <= 0 then
		TempLine = 1
	end

	while (not self._LineRec[TempLine]) do
		TempLine = TempLine - 1
	end

	return TempLine 
end

local function CalAbsIndex(EditObj, LineIndex, RelaIndex)
	return EditObj._LineRec[LineIndex].StartIdx + RelaIndex - 1
end

local function CalIndexByMousePos(EditObj, MouseX, MouseY)
	local LineIndex = EditObj:CalLineByMouseY(MouseY)
	if EditObj._LineRec[LineIndex].StartIdx == 0 then
		local AbsIndex = 0
		local TempLineIndex = LineIndex - 1
		while (TempLineIndex >= 1) do
			if EditObj._LineRec[TempLineIndex].EndIdx > 0 then
				AbsIndex = EditObj._LineRec[TempLineIndex].EndIdx
				break
			end

			TempLineIndex = TempLineIndex - 1
		end

		return LineIndex, 0, AbsIndex 
	end

	local RelaIndex = CalRelaIndexByMouseX(EditObj, LineIndex, MouseX)

	return LineIndex, RelaIndex, CalAbsIndex(EditObj, LineIndex, RelaIndex)
end

local function LocateCursorPos(EditObj, MouseX, MouseY)
	local LineIndex, RelaIndex, AbsIndex = CalIndexByMousePos(EditObj, MouseX, MouseY)

	if not EditObj._DisableInput then
		EditObj._InsertIndex = AbsIndex
		EditObj._CurLine = LineIndex

		DisplayCursorByIndex(EditObj, LineIndex, RelaIndex)
	end
end

function clsLightMultiEdit:Deactive()
	HideCursor(self)
	self:ResetEdit()
	self._MsgEdit:detachWithIME()
end

function clsLightMultiEdit:active()
	ShowCursor(self)
	LocateCursorPos(self, 0, 0)
	self._MsgEdit:attachWithIME()
end

function clsLightMultiEdit:OnTextLeftDown(MouseX, MouseY)
	ShowCursor(self)
	LocateCursorPos(self, MouseX, MouseY)
	self._MsgEdit:attachWithIME()
end


