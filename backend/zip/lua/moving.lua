-- news page

NEWS_LAYOUT = Import("layout/moving.lua")
local NewsConfig = NEWS_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local newsPage = nil

local all_list = {}
--[[
type = 7
user
id
data = {
	score
	user
	id
	url
	name
}
logo
item
vip_id
sex
name
is_real
time

type = 9
data = ""
--]]
local cur_all_page = 0
local max_all_page = 5

local friend_list = {}
local cur_friend_page = 0
local max_friend_page = 5

local freshing = true

local PLAYER_PAGE = Import("lua/player_info.lua")

local function back_func()
	PLAYER_PAGE.closePlayerInfoPagePanel()
	if PANEL_CONTAINER.closeChildPanel( nil, 3 ) then
		PANEL_CONTAINER.addChild( showNewsPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
	end	
	--showNewsPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

local function createSmallItemByNewsInfo(info)
	local tr = LIGHT_UI.clsTr:New( NewsConfig )
	local nodeObj = tr._RootNode
	
	--[[local function onBack()
		USER_INFO_PANEL.closeUserInfoPagePanel()
		if PANEL_CONTAINER.closeChildPanel( nil, 3 ) then
			PANEL_CONTAINER.addChild( NEWS_PAGE.showNewsPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
		end
	end--]]

	local function on_small_click(obj, x, y)
		
		local user_info = LOGIN_PANEL.getUserData()
		if user_info.id ~= info.user then
			closeNewsPagePanel()
			--USER_INFO_PANEL.showUserInfoPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, onBack, true )
		--else
			PLAYER_PAGE.showByUserId(info.user)
			PLAYER_INFO_PANEL.init( { back_func = back_func })
		else
			showMessage( newsPage, "查看自己的动态可到导航栏点击自己的头像", {ms="3000"} )
		end			
	end

	tr:setMsgDelegate( newsPage._move_small_page._move_small_grp, on_small_click )
	
	tr:addSprite( 'logo', info.logo )
	tr:addLabel( 'name', info.name )
	tr:addLabel( 'time', info.time )

	local sex_res = info.sex == '0' and 'man.png' or 'woman.png'	
	tr:addSprite( 'sex', sex_res )
	
	if info.vip_id ~= '0' then
		local vip_logo = string.format( 'vip%i.png', info.vip_id )
		
		tr:addSprite( 'vip', vip_logo )
	end

	if tonumber(info.type) == 9 then
		local str = clearHTML( info.data )	
		str = getCharString( str, 16 )
		str = str .. "..."
		tr:addMultiLabel( 'content', str )
	elseif tonumber(info.type) == 7 then
		local str = "给" .. info.data.name .. "的照片打了" .. info.data.score .. "分"
		str = getCharString( str, 16 )		
		tr:addMultiLabel( 'content', str )
	elseif tonumber(info.type) == 2 then		
		local str = getCharString( info.data.content, 16 )
		str = str .. "..."
		tr:addMultiLabel( 'content', str )
	elseif tonumber(info.type) == 1 then
		--tr:addSprite( 'content', info.data.url )
		tr:addMultiLabel( 'content', "快来看看，上传了新图了哦" )
	end		

	tr:setHeight( 'content', 31)

	return nodeObj
end

local function clearItemById( page, id, cache )
	local itemList = page:getItemList()
	for k,v in ipairs( itemList ) do
		if v.id == id then
			if not cache then
				page:clearItem( k, item )
			end				
		end
	end 
end

--[[local function judgeItemExist( page, item, cache )
	local itemList = page:getItemList()
	for k,v in ipairs( itemList ) do
		if v.id == item.id then
			if not cache then
				page:replaceItem( k, item )
			end
			return true
		end
	end 
	return false
end--]]

local itemCount = 1
local cacheArr = {}
local function on_get_all_list(data, cache)
	if not newsPage then
		return
	end	
	if freshing and freshing == true then	
		newsPage._move_small_page._all_page:clearAllItem()
		resetAListPosition()			
	end
	
	for k, v in pairs(cacheArr) do
		clearItemById( newsPage._move_small_page._all_page, v, cache )
	end
		
	itemCount = 1
	cacheArr = {}
	for _, news_info in pairs(data.data) do
		local ret_panel = createSmallItemByNewsInfo(news_info)
		if freshing and freshing == true then
			if not cache then
				freshing = false
			end
			if itemCount == 1 then
				ret_panel.tip = createLabel( ret_panel, {x = 100, y = 30, ax = 0, ay = 0.5, text = '等侯刷新', css='c1'} )
				xCenter( ret_panel.tip )
				local locX, locY = ret_panel.tip:getPosition()
				ret_panel.tip:setPosition( locX + 10, locY )
				locX, locY = ret_panel.tip:getPosition()
				ret_panel.arrow = createSprite( ret_panel, {x = locX - 20, y = 30, res = 'arrow_down.png'} )
				ret_panel.loading = createSprite( ret_panel, {x = locX - 20, y = 30, res = 'white_dot.png'})
				ret_panel.loading:setVisible( false )
			end
		end
		if cache then
			table.insert( cacheArr, news_info.id )
		end
		ret_panel.id = news_info.id
		--if judgeItemExist( newsPage._move_small_page._all_page, ret_panel, cache ) == false then				
		newsPage._move_small_page._all_page:append_item(ret_panel)		
		itemCount = itemCount + 1
		--end	
		--table.insert(all_list, news_info)
		
	end

	newsPage._move_small_page._all_page:refresh_view()
	
end

local function on_get_friend_list(data)
	if freshing and freshing == true then
		newsPage._move_small_page._friend_page:clearAllItem()
		resetFListPosition()
	end
	if not data.code then
		itemCount = 1
		for _, news_info in pairs(data.data) do
			local ret_panel = createSmallItemByNewsInfo(news_info)
			if freshing and freshing == true then
				freshing = false
				if itemCount == 1 then
					ret_panel.tip = createLabel( ret_panel, {x = 100, y = 30, ax = 0, ay = 0.5, text = '等侯刷新', css='c1'} )
					xCenter( ret_panel.tip )
					local locX, locY = ret_panel.tip:getPosition()
					ret_panel.tip:setPosition( locX + 10, locY )
					locX, locY = ret_panel.tip:getPosition()
					ret_panel.arrow = createSprite( ret_panel, {x = locX - 20, y = 30, res = 'arrow_down.png'} )				
					ret_panel.loading = createSprite(  ret_panel, {x = locX - 20, y = 30, res = 'white_dot.png'})
					ret_panel.loading:setVisible( false )
				end
			end
			--table.insert(friend_list, news_info)
			if newsPage then
				newsPage._move_small_page._friend_page:append_item(ret_panel)
			end
			itemCount = itemCount + 1
		end
	else
		print(data.data)
	end

	newsPage._move_small_page._friend_page:refresh_view()
	
end

local function request_all_list(page)
	doCommand( 'user', 'moving', { kind = 2, page = page, version=1 }, on_get_all_list,1,{loading=0} )
end

local function request_friend_list(page)
	--doCommand( 'user', 'moving', { kind = 1, page = page }, on_get_friend_list,nil,{loading=0} )
	doCommand( 'user', 'moving', { kind = 1, page = page, version=1 }, on_get_friend_list )
end

local small_desc_info = {
	item_width = 480,
	item_height = 135,
	column_cnt = 1,
	x_space = 0,
	y_space = 0,
}

function onAMoveTouchEnd()
	all_list = {}
	freshing = true
	cur_all_page = 1
	request_all_list(cur_all_page)	
end

function resetAListPosition()
	local config = NewsConfig["move_page_config"]
	newsPage._move_small_page._all_page:setPosition(0, config["inner_y"])
end

function onFMoveTouchEnd()
	friend_list = {}	
	freshing = true
	cur_friend_page = 1
	request_friend_list(cur_friend_page)	
end

function resetFListPosition()
	local config = NewsConfig["move_page_config"]
	newsPage._move_small_page._friend_page:setPosition(0, config["inner_y"])
end

local function createMoveSmallPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = NewsConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_small_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config["view_height"])
	ret_panel._move_small_grp:setVMovable(true)
	
	ret_panel._move_small_grp.touchEnd = onAMoveTouchEnd
	ret_panel._move_small_grp.resetListPos = resetAListPosition
	ret_panel._move_small_grp.tipHeight = 700

	local all_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	all_page:refresh_view()

	local function onAOverTop(layout)
		
	end

	local function onAOverBottom(layout)
		if cur_all_page < max_all_page then
			cur_all_page = cur_all_page + 1
			request_all_list(cur_all_page)
		end
	end

	all_page.onHMoveOverTop = onAOverTop
	all_page.onHMoveOverBottom = onAOverBottom

	local friend_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	friend_page:refresh_view()

	local function onFOverTop(layout)
		
	end

	local function onFOverBottom(layout)
		if cur_friend_page < max_friend_page then
			cur_friend_page = cur_friend_page + 1
			request_friend_list(cur_friend_page)
		end
	end

	friend_page.onHMoveOverTop = onFOverTop
	friend_page.onHMoveOverBottom = onFOverBottom

	all_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(all_page)
	friend_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(friend_page)

	ret_panel._move_small_grp:selectPage(1)

	ret_panel._all_page = all_page
	ret_panel._friend_page = friend_page 

	local function get_y_limit(obj)
		return config["inner_y"] - 70
	end

	all_page.getYLimit = get_y_limit
	friend_page.getYLimit = get_y_limit
	
	return ret_panel
end

local function createNewsPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createContainerChildPanel( parent, x, y )

	local c, c2 = RegConfig, nil
	local o = nil

	local function on_back(btn)
		closeNewsPagePanel()
		MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		--GAMEHALL_PANEL.showGameHallPanel(HTTP_CLIENT.getRootNode(), 0, 0 )
	end

	--ret_panel._onBack = on_back
	--ret_panel._head_text:setString( '动态' )

	config = NewsConfig["move_page_config"]
	ret_panel._move_small_page = createMoveSmallPage(ret_panel, config["x"], config["y"]) -- 小图标列表 

	local function on_sel_small_page(move_obj)
		freshing = true
		ret_panel._move_hint:selItem(move_obj:getCurPage())		
		if move_obj:getCurPage() == 1 and table.maxn(all_list) <= 0 then
			newsPage._move_small_page._move_small_grp.touchEnd = onAMoveTouchEnd
			newsPage._move_small_page._move_small_grp.resetListPos = resetAListPosition
			cur_all_page = 1
			request_all_list(cur_all_page)			
		end			
		if move_obj:getCurPage() == 2 and table.maxn(friend_list) <= 0 then	
			newsPage._move_small_page._move_small_grp.touchEnd = onFMoveTouchEnd
			newsPage._move_small_page._move_small_grp.resetListPos = resetFListPosition	
			cur_friend_page = 1
			request_friend_list(cur_friend_page)			
		end
	end

	ret_panel._move_small_page._move_small_grp.onSelPage = on_sel_small_page

	local item_list = {
		[1] = {
			["txt"] = "所有",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
		[2] = {
			["txt"] = "关注",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
	}

	config = NewsConfig["move_hint_config"]
	local total_width = 480
	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, config["x"], config["y"], "tab_bg_image.png", item_list, total_width, config["res"], "tiny_black_pixel.png")
	local bg = ret_panel._move_hint:getBGObj()
	bg:setScaleX(total_width)
	ret_panel._move_hint:selItem(1)

	--[[if table.maxn(all_list) <= 0 then
		cur_all_page = 1
		request_all_list(cur_all_page)
	end--]]

	local function on_click( obj, idx )
		newsPage._move_hint:selItem( idx )		
		newsPage._move_small_page._move_small_grp:selectPage( idx )
	end

	ret_panel._move_hint.onClick = on_click
	
	ret_panel.closePanel = closeNewsPagePanel

	return ret_panel
end

function showNewsPagePanel(parent, x, y)
	dailyCount('Dymanic')--动态
	freshing = true
	all_list = {}
	friend_list = {}
	if not newsPage then
		newsPage = createNewsPagePanel(parent, x, y)
	else
		newsPage:setVisible(true)
	end
	if table.maxn(all_list) <= 0 then
		cur_all_page = 1
		request_all_list(cur_all_page)
	end
	return newsPage
end

function closeNewsPagePanel()
	--[[if newsPage then
		newsPage:setVisible(false)
	end--]]		
	clear({newsPage})
	newsPage = nil
end

