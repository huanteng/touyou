-- user album page

ALBUM_LAYOUT = Import("layout/user_album.lua")
local AlbumConfig = ALBUM_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local albumPage = nil

local spic_item_width = 105
local spic_item_height = 105
local photo_list_desc = {
	item_width = 105,
	item_height = 105,
	column_cnt = 4,
	x_space = 10,
	y_space = 10,
}

function getAlbumPage()
	return albumPage
end

local function createAlbumPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = UserConfig, nil
	local o = nil

	local function on_back(btn)
		closeAlbumPagePanel()
		--USER_INFO_PANEL.showUserInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		if PANEL_CONTAINER.closeChildPanel( t, 16 ) then	--随意传个非空的table，该参数如果为空，就表明需要清除子面板，否则，就会考虑第2个参数							
			PANEL_CONTAINER.addChild( USER_INFO_PANEL.showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
		end
	end
	
	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '相册' )

	ret_panel._photo_list_txt = LIGHT_UI.clsLabel:New(ret_panel, 20, 718, "", GLOBAL_FONT, 21)
	ret_panel._photo_list_txt:setAnchorPoint(0, 1)
	ret_panel._photo_list_txt:setTextColor(102, 102, 102)

	ret_panel._photo_oper_txt = LIGHT_UI.clsLabel:New(ret_panel, 126, 715, "长按单张照片设置头像或删除", GLOBAL_FONT, 20)
	ret_panel._photo_oper_txt:setAnchorPoint(0, 1)
	ret_panel._photo_oper_txt:setTextColor(153, 153, 153)

	ret_panel._photo_list = LIGHT_UI.clsRowLayout:New(ret_panel, 15, 680, photo_list_desc)
	
	return ret_panel
end

function setMsgProc(tag)
	local function hideMsg(item_grid)
		if item_grid._msg_layer then
			item_grid._msg_layer:setVisible(tag)
		end
	end
	if albumPage then
		albumPage._photo_list:doFunc(hideMsg)
	end
end

local USER_OPEP_PAGE = Import("lua/user_photo_oper.lua")
local USER_BIG_PHOTO_PAGE = Import("lua/user_big_photo.lua")
local UPLOAD_PAGE = Import("lua/upload.lua")

local function configByAlbumData()

	local function set_logo_cb()
	end
	local function delete_cb()
	end

	local function on_pic_touch_began(msg_obj, x, y)
		local function do_show_oper()
			USER_OPEP_PAGE.showOperPagePanel(msg_obj._photo_info)
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(msg_obj._show_oper_cb)
			msg_obj._show_oper_cb = nil 
		end

		if msg_obj._show_oper_cb then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(msg_obj._show_oper_cb)
			msg_obj._show_oper_cb = nil 
		end
		msg_obj._show_oper_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(do_show_oper, 1, false)
	end

	local function on_pic_touch_end(msg_obj, x, y)
		if msg_obj._show_oper_cb then
			closeAlbumPagePanel()
			USER_BIG_PHOTO_PAGE.showBigPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, GLOBAL.uid, msg_obj._photo_idx)
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(msg_obj._show_oper_cb)
			msg_obj._show_oper_cb = nil 
		end
	end
	
	local function on_add_click(obj, x, y)
		closeAlbumPagePanel()
		UPLOAD_PAGE.showUploadPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	local pic_c = {
			sx = 105,
			sy = 105,
			ax = 0,
			ay = 1,
			x = 0,
			y = 0,
			res = '',
		}

	
	local albumData = USER_INFO_PANEL.getAlbumData()
	--CCMessageBox(tostring(#albumData),'')
	albumPage._photo_list_txt:setString(string.format("相片(%d)", table.maxn(albumData)))
	albumPage._photo_list:clearAllItem()
	local photo_idx = 1
	for _, photo_info in pairs(albumData) do
		pic_c.res = photo_info["url"]
		local pic_item = createSprite( nil, pic_c )
		albumPage._photo_list:append_item(pic_item)
		pic_item._msg_layer = LIGHT_UI.clsClipLayer:New(pic_item, 0, -spic_item_width)
		pic_item._msg_layer:set_msg_rect(0, 0, spic_item_width, spic_item_height)
		pic_item._msg_layer.onTouchBegan = on_pic_touch_began
		pic_item._msg_layer.onTouchEnd = on_pic_touch_end
		pic_item._msg_layer._photo_info = photo_info
		pic_item._msg_layer._photo_idx = photo_idx
		photo_idx = photo_idx + 1
	end

	local add_item = LIGHT_UI.clsSimpleButton:New(nil, 0, 0, "add_photo_bg.png", "add_photo_bg.png")
	add_item:setButtonScaleX( 0.875 )
	add_item:setButtonScaleY( 0.875 )
	add_item.onTouchEnd = on_add_click
	albumPage._photo_list:append_item(add_item)

	albumPage._photo_list:refresh_view()
end

function onDeletePhoto()
	configByAlbumData()
	USER_INFO_PANEL.refreshData()
end

function showAlbumPagePanel(parent, x, y)
	if not albumPage then	
		albumPage = createAlbumPagePanel(parent, x, y)
	else
		albumPage:setVisible(true)
	end

	configByAlbumData()
end

function closeAlbumPagePanel()
	--albumPage:setVisible(false)
	clear({albumPage})
	albumPage = nil
end

