-- for CCLuaEngine traceback
WRITE_PATH = CCFileUtils:sharedFileUtils():getWriteablePath()..'touyou/'

TIME_VERSION = "1382676103"
--TIME_VERSION = "2377230246"
ENGINE_VERSION = "201309121204"
VERSION_NAME = "1.2"

--渠道编号
--CHANNEL_NUMBER = "touyou"

function safe_dofile(fname)
    --fname = CCFileUtils:sharedFileUtils():fullPathFromRelativePath(fname)
	fname = WRITE_PATH .. fname
    if CCApplication:sharedApplication():getTargetPlatform() ~= kTargetAndroid then
        return dofile(fname)
    else
        local file_str = xymodule.get_lua_str(fname)
        local func, msg = loadstring(file_str)
        return setfenv(func, _G)()
    end
end

safe_dofile("lua/import.lua")
safe_dofile("lua/class.lua")
safe_dofile("lua/base.lua")
safe_dofile("lua/action.lua")
safe_dofile("lua/config.lua")

-------------------------------------------------------------------------
HTTP_CLIENT = Import("lua/http_client.lua")
HTTP_REQUEST = Import("lua/http_request.lua")
LIGHT_UI = Import("lua/light_ui.lua")
GAMEHALL_PANEL = Import("lua/game_hall.lua")
MAINPAGE_PANEL = Import("lua/main.lua")
CHALLENGE_PANEL = Import("lua/challenge.lua")
CHAT_PANEL = Import("lua/chat.lua")
LUCKY_ROT_PANEL = Import("lua/lucky_rot.lua")
PLAYER_INFO_PANEL = Import("lua/player_info.lua")
USER_INFO_PANEL = Import("lua/user_info.lua")
CONSUME_RECORD_PANEL = Import("lua/consume_record.lua")
USER_PACKAGE_PANEL = Import("lua/user_package.lua")
LOGIN_PANEL = Import("lua/login.lua")
BASE_LAYOUT = Import("layout/base.lua")
PANEL_CONTAINER = Import( "lua/panel_container.lua" )
MAIN_PANEL = Import( "lua/main_panel.lua" )
NEW_GAME_HALL = Import( "lua/new_game_hall.lua" )
	--local cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(test, 1, false)



local function main()
	--local d = base64Decode('5L2gYS4uLuWlvW\/kuIDotKXmtoLlnLBvMyEzMzM=')	
	HTTP_CLIENT.http_main()
end

xpcall(main, __G__TRACKBACK__)

