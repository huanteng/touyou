-- room

ROOM_LAYOUT = Import("layout/room.lua")
local RoomConfig = ROOM_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local RoomPage = nil

local PageData = {}
local cur_page = 1
local pre_page = 0
local room_page = 50


local level_name = {["1"] = "屌丝",["2"] = "土豪",["3"] = "富豪",}

local mall = Import("lua/mall.lua")

--级别
local level = 1

local click_delta = 10
--自己的底金
local MYGold = 0
------------------------------------------------------------------------
--显示空桌
local showNullChair = nil
--返回到商城
local backtoMall = nil
--返回
local back_func = nil
------------------------------------------------------------------------

local currentDrunkenness = 3
local totalDrunkenness = 5
local popWindow = false
local winerName = nil
local exitWindow = nil
local quickChangetable = nil
local letMoney = nil
local isQuickChange = false
local RoomNunber = nil


local firstEnter = true


function setPopWindow( value, name )
	popWindow = value
	winerName = name	
end

--金币不足被弹出的的标志
function setExitWindow(value)
	exitWindow = value
	end	


function back_func(toMall)
	if toMall then
		mall.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0,backtoMall,nil,true)
	else
	
		if basePanel then	
			showPanel( HTTP_CLIENT.getRootNode(), 0, 0, true )
		else
			if PANEL_CONTAINER.closeChildPanel( nil, 10 ) then							
				PANEL_CONTAINER.addChild( showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
			end	
		end	
	end	
end	
	
function backtoMall()	
	back_func()
	mall.closeMallPagePanel()			
end

function showPage()
	local c = RoomConfig.page
	if RoomPage then
		if not RoomPage.page then
			RoomPage.page = createLabel(RoomPage,c)
		end
		local str = string.format("-[ %d/%d ]-",cur_page,room_page)
		RoomPage.page:setString(str)
	end			
end

local function getBaseGold(level)
	local ret = 100
	if(level == 1) then
		ret = 100	
	elseif(level == 2) then
		ret = 500
	else
		ret = 1000
	end
	return ret
end


local function showAlert()
	local function onOk()			
		back_func(true)
		closePanel()
		setExsitAlert(false)
	end
	
	local config = RoomConfig.alert2
	config.title.text = "温馨提示"
	
	config.content3.text = "您的金币不满足入场要求,最低需"..getBaseGold(level)*4.5 .."金币哟"	
	createAlert(RoomPage,config,onOk)
end	

local function showExitAlert()	
	local room_match = Import("lua/room_game.lua")
	local exitLevel = room_match.getLevel()
	local function onOk()			
		back_func(true)
		closePanel()
		setExsitAlert(false)
	end
	
	local config = RoomConfig.alert2
	config.title.text = "温馨提示"
	config.content3.text = "您的金币不满足入场要求,最低需" .. getBaseGold(exitLevel)*4.5 .. "金币哟"			
	createAlert(RoomPage,config,onOk)
end	


--启动游戏进入比赛房间
function enterMatch2( room_id,ip,port,no )
	--no = 10	
		
	
	local function startGame()
		
			if MYGold < getBaseGold(level)*4 + getBaseGold(level)*0.5 then
			showAlert()
			--CCMessageBox("你的金币不足入场要求，请充值",'')					
			--back_func(true)
			return
			end
		
					
		
		closePanel()	
		local room_match = Import("lua/room_game.lua")	
		--room_match.init( { ip = '192.168.1.254',port = 1111,room_id = room_id, chair_id = 1,back_func = back_func } )
		room_match.init( { level = level,ip = ip,port = port,
		room_id = room_id, chair_id = 1,
		room_no = no,room_page = cur_page,
		back_func = back_func } )
		room_match.showPanel(HTTP_CLIENT.getRootNode(), 0, 0 )	
	end
	if currentDrunkenness >= 3 then
		local function okHandler()
			setExsitAlert( false )
			local function onPropsUses( data )
				if data.code == 1 then
					showMessage( RoomPage, data.memo )	
					startGame()
				else
					showMessage( RoomPage, data.memo )
				end			
			end
			doCommand( 'props', 'uses', { props = 50, quantity = 1 }, onPropsUses )
		end
		createAlert( RoomPage, RoomConfig.enter_game_alert, okHandler )
	else
		startGame()
	end		
			
end



local basePanel = nil

--启动游戏进入比赛房间
--[[local function enterMatch( room_id )
	closePanel()
	
	local room_match = Import("lua/room_match.lua")
	
	local function back_func()	
		--showPanel( HTTP_CLIENT.getRootNode(), 0, 0)	
		if basePanel then
			showPanel( HTTP_CLIENT.getRootNode(), 0, 0, true )
		else
			if PANEL_CONTAINER.closeChildPanel( nil, 10 ) then							
				PANEL_CONTAINER.addChild( showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
			end	
		end	
	end
	
	room_match.init( { room_id = room_id, back_func = back_func } )
	room_match.showPanel(HTTP_CLIENT.getRootNode(), 0, 0 )			
end--]]

--1,进入房间
local function doRoomEnter( id,pwd )
    local function onRoomEnter(data)
        if data.code < 0 then
            showMessage( RoomPage, data.memo )
        else
            enterMatch( id )
        end
    end
    doCommand( 'room', 'enter', { ['id'] = id, ['password'] = pwd },onRoomEnter )
end

--坐下
local function doSit( id )
	local function onSit( data )
		if data.code < 0 then
			showMessage( RoomPage, data.memo )
		else
			enterMatch( id )
		end		
	end
	local roomID = (level - 1)* 200 + id
	doCommand( 'room', 'sit', { ['id'] = roomID, ['password'] = '' }, onSit )
end

--快速坐下
local bQuickSit = false
local function doQuickSit()
	--CCMessageBox('还没实现','')		
	local function onQuickSit( data )
		bQuickSit = false		
		if data.code < 0 then
			showMessage( RoomPage, data.memo )
		else			
			--if data.data.id then
			--d(data)			
			enterMatch2( data.data.id,data.data.ip,data.data.port,data.data.no )
			--enterMatch2( 1,'192.168.1.22',1115,201 )
			--end
		end		
	end
	doCommand( 'room', 'quick_start', { ['level'] = level,['id']=0 }, onQuickSit,nil,{priority = true} )
	--[[ROOM_MATCH = Import("lua/room_game.lua")
	RoomNunber = ROOM_MATCH.getRoomNo()	
	if bQuickSit == false then
		bQuickSit = true
		if RoomNunber == nil  then
			doCommand( 'room', 'quick_start', { ['level'] = level,['id']=0 }, onQuickSit,nil,{priority = true} )
		else
			doCommand( 'room', 'quick_start', { ['level'] = level,['id']=tonumber(RoomNunber) }, onQuickSit,nil,{priority = true} )--优先处理的请求	
		end
		
	end	--]]
end
--[[
function back_func()
	PLAYER_INFO_PANEL.closePlayerInfoPagePanel()
	showPanel(HTTP_CLIENT.getRootNode(), 0, 0)
end
]]

local function createRoom(info)
	local c, c2 = RoomConfig, nil
	local config = RoomConfig["big_item_config"]
	local nodeObj = createNode( nil, {} )
	local ip = info.ip
	local port = info.port
	assert(string.len(tostring(ip)) > 0)
	--local ip = '192.168.1.254'
	--local port = 1111
	
	local o = nil
	
	local function onRoomClick()
				
		if string.len(tostring(ip)) > 0 and tonumber(port) > 0 then		
			enterMatch2( info.id,ip,port,info.no )
		end				
		
		--doSit( info.id )
		--doRoomEnter(info.id,'')--参数2为 ‘密码' 暂时留
		
	end

	--o = createSprite( nodeObj, c.desk )	
	c2 = c.desk	
	o = createButton( nodeObj, c2, onRoomClick)
	o:setAnchorPoint(0.5, 0.5)
	--setMsgDelegate(o, RoomPage._move_big_page._move_big_grp, onRoomClick)
		
	local center_x, center_y = c.desk.x, c.desk.y
	local desk_x, desk_y = c.desk_logo.x, c.desk_logo.y
	
	for i = 1, 4 do
		c2 = {}
	
		--[[
		位置布局为： 		
		4   3
		1   2
		]]
		if i == 4 then
		--if i == 1 then
			c2.x = center_x - desk_x
			c2.y = center_y + desk_y
		--elseif i == 2 then
		elseif i == 3 then
			c2.x = center_x + desk_x
			c2.y = center_y + desk_y
		--elseif i == 3 then
		elseif i == 1 then
			c2.x = center_x - desk_x
			c2.y = center_y - desk_y
		else
			c2.x = center_x + desk_x
			c2.y = center_y - desk_y
		end
		
		local name = nil
		local c3 = RoomConfig.logo
		local o = nil
		if info[ 'desk' .. i ] then
			c2.x, c2.y = c2.x - 0, c2.y + 0
			c2.sx = c3.sx
			c2.sy = c3.sy
			c2.res = info[ 'desk' .. i ].logo
			o = createSprite( nodeObj, c2 )
			
			name = info[ 'desk' .. i ].name
		else
			--c2.res = RoomConfig.desk_logo.res
			--o = createSprite(nodeObj, c2 )
			--o = LIGHT_UI.clsSimpleButton:New( nodeObj, c2.x, c2.y, c2.res, c2.res ) 
		
			--o:setScale( 0.4)
			
			--name = ''
		end
		
		--o:setAnchorPoint(0.5, 0.5)
		--setMsgDelegate(o, RoomPage._move_big_page._move_big_grp, onRoomClick)
				
		if i == 1 or i == 2 then
			c2.y = c2.y - 45
			c2.x = c2.x + 0
		else
			c2.y = c2.y + 45
			c2.x = c2.x + 0
		end
		local o = createLabel( nodeObj, text( c2.x, c2.y, name, 'b4') )
		
		--local o = createLabel( nodeObj, text( c2.x, c2.y, getCharString(tostring(name),5), 'b4') )
		if name then
			--print('name：'..name)
			AdjustString(o,tostring(name),80)
			--AdjustString(o,tostring(info.uid),80)
		end
	end
	
	c2 = c.desk_no	
	local no = string.format( '-%s%03d-', level_name[tostring(level)],info.no )
	createLabel( nodeObj, text( center_x + c2.x, center_y + c2.y, no, c2.css ) )
	
	--底金
	local gold = string.format( '金币: %d', info.gold )
	createLabel( nodeObj, text( center_x, center_y , gold, c2.css ) )
	
	local function getContentSize(node)
		return {["width"] = 140, ["height"] = 140,}
	end
	nodeObj.getContentSize = getContentSize	
	
	return nodeObj
end

--列表回调
local function onRoomLists(data)
	local sign = getExsitAlert()
	if sign == true then		--如果警告面板存在则返回	
		cur_page = pre_page
		showPage()
		return
	end
	data = data.data
	
	if not RoomPage then
		return 
	end
	--local page = cur_page	
	local page = pre_page
	local PAGE = RoomPage._move_big_page._page[ page ]
	
	PageData[page] = {}
	PAGE:clearAllItem()
	--RoomPage._move_hint:selItem( page )
	
	local nMaxDesk = 1

	for _, info in ipairs(data) do
		if nMaxDesk < tonumber(info.no) then
			nMaxDesk = tonumber(info.no)
		end
	end
	
	if nMaxDesk == cur_page * 4 then	
		for _, info in ipairs(data) do
			table.insert(PageData[page], info)
			PAGE:append_item(createRoom(info))
		end

		PAGE:refresh_view()
	end
end	

--请求列表
local function doRoomLists(page)
	
	pre_page = cur_page
	cur_page = page
	showPage()
	doCommand( 'room', 'lists', { level = level, page = page }, onRoomLists,1)
end	

--请求列表定时器（3秒请求一次）
local function doRefresh()
	local function onListTimer()
		
			doRoomLists(cur_page)
		
	end
	TIMER[ "lists" ] = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( onListTimer, 3, false)
end

local function createMoveBigPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local c, c2 = RoomConfig, RoomConfig.move_page
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_big_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width + 100, c2.view_height, 0 )
	--ret_panel._move_big_grp:setVMovable(true)
	
	local function get_y_limit(obj)
		return c2.inner_y 
	end		

	ret_panel._page = {}
	local desk_info = c.desk_info
	for i = 1, room_page do
		local girl_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, desk_info)
		girl_page:refresh_view()
		
		girl_page:setPosition(0, c2.inner_y)
		girl_page.getYLimit = get_y_limit
		ret_panel._move_big_grp:appendItem(girl_page)
	
		PageData[i] = {}
		ret_panel._page[i] = girl_page
	end
	
	return ret_panel
end	



local function createPanel(parent, x, y, isBasePanel ) 
	local ret_panel = createNode(parent, point( x, y ) )

	local c,c2 = RoomConfig, nil
	--MYGold = 0
	local o = nil
	
	c2 = c.top_pic
	ret_panel.top_pic = createSprite( ret_panel, c2 )
	ret_panel.top_pic:setAnchorPoint(0, 0)
	ret_panel.top_pic:setScaleX(c2.sx)
	ret_panel.top_pic:setScaleY(c2.sy)

	local function on_back(btn)
		closePanel()
		if not isBasePanel then		
			if PANEL_CONTAINER.closeChildPanel( nil, 10 ) then					
				PANEL_CONTAINER.addChild( MAIN_PANEL.createMainPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
				--PANEL_CONTAINER.addChild( showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
			end	
		end
		--MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
	
	if isBasePanel then
		o = createSprite( ret_panel, c.bg_pic )
	else		
		o = createSprite( ret_panel, c.bg_pic2 )
	end
	o:setAnchorPoint(0, 0)
	
	c2 = c.name
	c2.text = getCharString(tostring(GLOBAL.name),10)
	--print(c2.text)
	o = createLabel( ret_panel, c2 )
	--AdjustString(o,tostring(GLOBAL.name),80)
	ret_panel._name = o
	
	createSprite( ret_panel, c.gold_pic )
	
	o = createLabel( ret_panel, c.gold_text )
	ret_panel._gold = o
		
	local function okHandler()
		setExsitAlert( false )
	end
	
	local function clickWineCupBtn()
		if ret_panel.arrow_bg_pic then
			clear( {ret_panel.arrow_bg_pic} )
			ret_panel.arrow_bg_pic = nil
		end
		createAlert( RoomPage, c.alert, okHandler )
	end
    	
	--回调用户信息
	local function onUserMoney( data )
		if RoomPage then
			ret_panel._gold:setString( data.data.gold )
			MYGold = tonumber(data.data.gold)
			
			currentDrunkenness = data.data.drunk
			if currentDrunkenness < 3 then
				c.wine_cup_pic.res = 'jzd-h.png'
			else
				c.wine_cup_pic.res = 'jzd-r.png'
			end	
			if not ret_panel.drunk_btn_responce_rect then
				ret_panel.drunk_btn_responce_rect = createButton( ret_panel, c.drunk_btn_responce_rect, clickWineCupBtn )
			end					
			
			if not ret_panel.wine_cup_pic then
				ret_panel.wine_cup_pic = createSprite( ret_panel, c.wine_cup_pic )
			else
				ret_panel.wine_cup_pic:setImage( c.wine_cup_pic.res )
			end
			
			local roomData = getData( 'room' )
			if roomData then
				if roomData.firstClick and roomData.firstClick == true then
					firstEnter = false
					setData( 'room', 'firstClick', firstEnter )
					ret_panel.arrow_bg_pic = createSprite( ret_panel, c.arrow_bg_pic )
					a_play( ret_panel.arrow_bg_pic:getSprite(),c.arrow_animation,true)
				end
			else
				firstEnter = false
				setData( 'room', 'firstClick', firstEnter )
				ret_panel.arrow_bg_pic = createSprite( ret_panel, c.arrow_bg_pic )
				a_play( ret_panel.arrow_bg_pic:getSprite(),c.arrow_animation,true)
			end				
			
			c.wine_degree_txt.text = currentDrunkenness .. "/" .. totalDrunkenness
			if not ret_panel.wine_degree_txt then
				ret_panel.wine_degree_txt = createLabel( ret_panel, c.wine_degree_txt )
			end
			ret_panel.wine_degree_txt:setString( data.data.drunk .. "/" .. data.data.max_drunk )			
			
			local function updateDrunkTxt()
				doCommand( 'user', 'money', {}, onUserMoney )
			end
			
			if not drunkTimer then
				drunkTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( updateDrunkTxt, 20, false )
			end
		end
		--[[
		local room_match = Import("lua/room_game.lua")
		if room_match.getqChangetable() == true then
			room_match.setqChangetable(false)
			doQuickSit()
		end
		]]	
	end
	--请求用户资料
	doCommand( 'user', 'money', {}, onUserMoney )
	
	if isBasePanel then
		if isBasePanel == true then		
			o = createButton( ret_panel, c.head_back )
			o.onTouchEnd = on_back
		end
	end
	
	local function clickTabBar( index )	
		level = index + 1
		PANEL_CONTAINER.curLevel = level
		doRoomLists( cur_page )
	end	
	
	local isLoad = false
	local function waitForImageCompleteHandler()
		if isFileExists( getResFilename( "diaosi.png") ) == true and 
		   isFileExists( getResFilename( "tuhao.png") ) == true and 
		   isFileExists( getResFilename( "fuhao.png") )  == true and 
		   isFileExists( getResFilename( "diaosi_on.png") ) == true and 
		   isFileExists( getResFilename( "tuhao_on.png") ) == true and 
		   isFileExists( getResFilename( "fuhao_on.png") )  == true then
			LIGHT_UI.clsSelectStateSlideTabBar:New(ret_panel, 86, 700, {"diaosi.png","tuhao.png","fuhao.png"},
			{"diaosi.png","tuhao.png","fuhao.png"}, {"diaosi_on.png","tuhao_on.png","fuhao_on.png"}, 150, 3, 4, 80, PANEL_CONTAINER.curLevel, clickTabBar )
			clickTabBar( PANEL_CONTAINER.curLevel - 1 )
			if waitForImageCompleteTimer then
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(waitForImageCompleteTimer)
				waitForImageCompleteTimer = nil
			end	
		else
			if isLoad == false then
				isLoad = true
				local o = createSprite( ret_panel, {x=0,y=0,res="diaosi.png"} )
				o:setVisible( false )
				o = createSprite( ret_panel, {x=0,y=0,res="tuhao.png"} )
				o:setVisible( false )
				o = createSprite( ret_panel, {x=0,y=0,res="fuhao.png"} )
				o:setVisible( false )
				o = createSprite( ret_panel, {x=0,y=0,res="diaosi_on.png"} )
				o:setVisible( false )
				o = createSprite( ret_panel, {x=0,y=0,res="tuhao_on.png"} )
				o:setVisible( false )
				o = createSprite( ret_panel, {x=0,y=0,res="fuhao_on.png"} )
				o:setVisible( false )
			end	
		end	
	end
	
	if isFileExists( getResFilename( "diaosi.png") ) == true and 
	   isFileExists( getResFilename( "tuhao.png") ) == true and 
	   isFileExists( getResFilename( "fuhao.png") )  == true and 
	   isFileExists( getResFilename( "diaosi_on.png") ) == true and 
	   isFileExists( getResFilename( "tuhao_on.png") ) == true and 
	   isFileExists( getResFilename( "fuhao_on.png") )  == true then
		LIGHT_UI.clsSelectStateSlideTabBar:New(ret_panel, 86, 700, {"diaosi.png","tuhao.png","fuhao.png"},
		{"diaosi.png","tuhao.png","fuhao.png"}, {"diaosi_on.png","tuhao_on.png","fuhao_on.png"}, 150, 3, 4, 80, PANEL_CONTAINER.curLevel, clickTabBar )
		clickTabBar( PANEL_CONTAINER.curLevel - 1 )
	else
		if not waitForImageCompleteTimer then
			waitForImageCompleteTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( waitForImageCompleteHandler, 0, false )
		end
	end
			
	
	
	
	--屏蔽等级选择按钮
	
	--[[local level_btn = {}
	local function onLevelBtn( btn )
		for i = 1, 3 do
			level_btn[i]:setStatus(LIGHT_UI.NORMAL_STATUS)
		end
		
		level = btn.level
		level_btn[ level ]:setStatus(LIGHT_UI.DOWN_STATUS)
		
		doRoomLists( cur_page )
	end
	
	for i = 1, 3 do	
		if isBasePanel then
			if isBasePanel == true then	
				if i == 1 then
					o = createButton( ret_panel, c[ string.format( 'levelbase%d_btn', i ) ], onLevelBtn )
					o.onDownChangeSprite = empty_function
					o.onUpChangeSprite = empty_function
					o.level = i
					level_btn[i] = o
				else
					o = createButton( ret_panel, c[ string.format( 'level%d_btn', i ) ], onLevelBtn )
					o.onDownChangeSprite = empty_function
					o.onUpChangeSprite = empty_function
					o.level = i
					level_btn[i] = o
				end					
			end
		else
			o = createButton( ret_panel, c[ string.format( 'level%d_btn', i ) ], onLevelBtn )
			o.onDownChangeSprite = empty_function
			o.onUpChangeSprite = empty_function
			o.level = i
			level_btn[i] = o
		end
	end
	
	--默认是第一页被点选
	level_btn[1]:setStatus(LIGHT_UI.DOWN_STATUS)--]]
	
	
	c2 = c.move_page
	ret_panel._move_big_page = createMoveBigPage(ret_panel, c2.x, c2.y ) 
	
	local function on_sel_big_page(move_obj)
		--ret_panel._move_hint:selItem(move_obj:getCurPage())				
			cur_page = move_obj:getCurPage()		
		--print('page', cur_page )
		if cur_page > room_page then
			return
		end
		if cur_page > pre_page then
			showNullChair()	
		end
		doRoomLists( cur_page )
		
	end
	
	ret_panel._move_big_page._move_big_grp.onSelPage = on_sel_big_page
	--ret_panel._move_big_page._move_big_grp:selectPage(1)
	
	c2 = RoomConfig.scroll
	--设置页数
	--[[c2.page = room_page
	local item_list = {}
	for i = 1, c2.page do
		item_list[ i ] = {
			["normal_pic"] = c2.res,
			["sel_pic"] = c2.on,
		}
	end

	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, c2.x, c2.y, nil, item_list, 15 * c2.page, nil)
	local winSize = CCDirector:sharedDirector():getWinSize()
	local x,y = ret_panel._move_hint:getPosition()
	ret_panel._move_hint:setPosition((winSize.width - 15 * c2.page)/2,y) 	--]]
	--ret_panel._move_hint:selItem(1)
	--LIGHT_UI.alignXCenter(ret_panel._move_hint)
	
	o = createButton( ret_panel, c.quick_start,doQuickSit )
	--o.onTouchEnd = doQuickSit

	if table.maxn(PageData) <= 0 then
		cur_page = 1
		doRoomLists(cur_page)
	end
	
	ret_panel.closePanel = closePanel

	return ret_panel
end

function showPanel(parent, x, y, isBasePanel )
	basePanel = isBasePanel
	if not RoomPage then
		RoomPage = createPanel(parent, x, y, isBasePanel)
		RoomPage._move_big_page._move_big_grp:selectPage(1)		
	else
		RoomPage:setVisible(true)
	end
	
	local function okHandler()
		setExsitAlert( false )
		local function onPropsUses( data )	
			if data.code == 1 then
				currentDrunkenness = 0
				local function onUserMoney( data )
					currentDrunkenness = data.data.drunk
					if currentDrunkenness < 3 then
						RoomConfig.wine_cup_pic.res = 'jzd-h.png'
					else
						RoomConfig.wine_cup_pic.res = 'jzd-r.png'
					end	
					RoomPage.wine_cup_pic:setImage( RoomConfig.wine_cup_pic.res )
					RoomPage.wine_degree_txt:setString( data.data.drunk .. "/" .. data.data.max_drunk )
				end
				--请求用户资料
				doCommand( 'user', 'money', {}, onUserMoney )
			end
			showMessage( RoomPage, data.memo )
		end
		doCommand( 'props', 'uses', { props = 50, quantity = 1 }, onPropsUses )
	end		
	--createAlert( RoomPage, RoomConfig.drunk_alert, okHandler )		
	
	if popWindow == true then
		popWindow = false
		RoomConfig.drunk_alert.content3.text = "oh，北鼻，你被" .. winerName .. "灌醉了，赶紧喝解酒药解酒吧，不然要被人抱走咯，菊花残..满腚伤..."
		createAlert( RoomPage, RoomConfig.drunk_alert, okHandler )
	end
	
	if exitWindow == true then
		exitWindow  = false
		showExitAlert()
	end		
	
	--定时刷新列表
	doRefresh()
	
	return RoomPage
end

function closePanel()
	if drunkTimer then	
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(drunkTimer)
		drunkTimer = nil
	end
	if RoomPage then
		setExsitAlert( false )
		clearTimer( "lists" )
		clear({RoomPage})
		RoomPage = nil
	end
	--RoomPage:setVisible(false)
end

--快速游戏
function doQuickGame(mygold,ip,port,id,no)	
	if mygold < getBaseGold(1)*4 + getBaseGold(1)*0.5 then
		CCMessageBox("你的金币不足入场要求，请充值",'')		
		back_func(true)
		--showAlert()
		return
	end
	local room_match = Import("lua/room_game.lua")		
	room_match.init( { level = 1,ip = ip,port = port,
	room_id = id, chair_id = 1,
	room_no = no,back_func = back_func} )
	room_match.showPanel(HTTP_CLIENT.getRootNode(), 0, 0 )									
end

function showNullChair()
	local page = cur_page
	local PAGE = RoomPage._move_big_page._page[ page ]	
	
	if not PAGE then
		return
	end
	PageData[page] = {}
	PAGE:clearAllItem()
	
	local function crateNullChair(idx)
		local c, c2 = RoomConfig, nil
		local config = RoomConfig["big_item_config"]
		local nodeObj = createNode( nil, {} )
		c2 = c.desk
		o = LIGHT_UI.clsSimpleButton:New( nodeObj, c2.x, c2.y, c2.res, c2.res )
		--[[
		o:setAnchorPoint(0.5, 0.5)
		local center_x, center_y = c.desk.x, c.desk.y
		local desk_x, desk_y = c.desk_logo.x, c.desk_logo.y
	
		for i = 1, 4 do
			c2 = {}			
			if i == 4 then			
				c2.x = center_x - desk_x
				c2.y = center_y + desk_y				
			elseif i == 3 then
				c2.x = center_x + desk_x
				c2.y = center_y + desk_y				
			elseif i == 1 then
				c2.x = center_x - desk_x
				c2.y = center_y - desk_y
			else
				c2.x = center_x + desk_x
				c2.y = center_y - desk_y
			end
						
			local c3 = RoomConfig.logo
			local o = nil			
			c2.res = RoomConfig.desk_logo.res			
			o = LIGHT_UI.clsSimpleButton:New( nodeObj, c2.x, c2.y, c2.res, c2.res ) 			
			
			o:setAnchorPoint(0.5, 0.5)			
					
			if i == 1 or i == 2 then
				c2.y = c2.y - 45
				c2.x = c2.x + 0
			else
				c2.y = c2.y + 45
				c2.x = c2.x + 0
			end										
		end
		
		c2 = c.desk_no	
		local no = string.format( '-%s%03d-', level_name[tostring(level)],(cur_page - 1)*4 + idx )
		createLabel( nodeObj, text( center_x + c2.x, center_y + c2.y, no, c2.css ) )
		]]
		--底金
		--local gold = string.format( '金币: %d', getBaseGold(level) )
		--createLabel( nodeObj, text( center_x, center_y , gold, c2.css ) )
		
		local function getContentSize(node)
			return {["width"] = 140, ["height"] = 140,}
		end
		nodeObj.getContentSize = getContentSize	
		
		return nodeObj
	end
	
	for i=1,4 do	
		PAGE:append_item(crateNullChair(i))
	end

	PAGE:refresh_view()
	
end

---------------------------------------------------
-- 假如是创建5个clsBaseButton btn1 btn2 btn3 btn4 btn5 外面要引用住这5个btnobj
-- 重写每个按钮的onTouchUp接口 里面这样写
-- btn1:setStatus(LIGHT_UI.NORMAL_STATUS)
-- btn2:setStatus(LIGHT_UI.NORMAL_STATUS)
-- btn3:setStatus(LIGHT_UI.NORMAL_STATUS)
-- btn4:setStatus(LIGHT_UI.NORMAL_STATUS)
-- btn5:setStatus(LIGHT_UI.NORMAL_STATUS)
-- 先全部设为正常状态 然后
-- btn:setStatus(LIGHT_UI.DOWN_STATUS) 设置当前触发点击的btn为按下状态即可

-- CCLuaEngine.cpp 这个文件放在了lua_code 你需要copy这个文件到引擎目录的scripting/lua/cocos2dx_support/ 覆盖原文件就可以了
