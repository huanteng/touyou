ROOM_MATCH_LAYOUT = Import("layout/room_match.lua")
safe_dofile("lua/room_control.lua")
safe_dofile("lua/room_chat.lua")
safe_dofile("lua/room_face.lua")
safe_dofile("lua/room_luck.lua")
safe_dofile("lua/room_userinfo.lua")


local RoomMatchConfig = ROOM_MATCH_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
----------------------------------------
local UserTimerZOrder = 50  --倒计时
local UserShoutPannelZOrder = 40 --选号面板
local UserDeskZOrder = 20 --游戏桌子
local UserWinLoseZOrder = 30 --输赢动画
local UserWayZOrder = 30 --自己的骰
local level_name = {["1"] = "屌丝",["2"] = "土豪",["3"] = "富豪",}
----------------------------------------
local RoomMatch = nil --游戏房间
local RoomID = nil
local RoomNO = nil --房间编号
local PrePageNo = nil --房间列表页
local ChairID = nil
local back_func = nil
local MyChairID = nil
local MyGlod = nil
local MAX_CHAIR = 4
local matchData = nil -- 比赛数据
local heartbeat = 10  --心跳延时
--local isAct = false --托管
local lastShout = {	-- 上一次喊的数据，以便于作有效性对比
	count = 0,	-- 数量
	number = 0,	-- 数字
	zhai = 0,	--斋
	index = nil,	--谁喊的？1，2，3，4
						-- nil 表示未有人喊过（重要）
}
local nowShout = {}	-- 当前喊号信息
local isWatch = true --默认为旁观状态
local base_gold = 100 --基础底金
local nSameWay = 0 --相同的骰，用于结算动画
local tBeginTimer = 5 --游戏开始前等待时间
local timeout_start = false --倒计时时钟启动标识
local nPlayer = 0 --房间人数
local IP = ""
local PORT = 1111
local sound_list = {} --声效控制，退出时需要停止声效
local isPlaying = false --
local isLeave = false
local ready = nil
local isAlive = true --连接保持
local nLevel = 0 --等级
local isEnterbackground = false
local isMyTimeBegin = false -- 是否轮到自己的标志符
---------------------------------
--游戏消息回调
local Game_CB = nil
--初始化成员变量
local Initialize = nil
--创建位置
local CreateDesk = nil
--创建坐下按钮
local CreateSit = nil
--位置换算
local GetChairIdx = nil
--登录成功
local OnLoginOk = nil
--登录失败
local OnLoginFail = nil
--坐下成功
local OnSitOk = nil
--坐下失败
local OnSitFail = nil
--坐下完成
local OnUserCome = nil
--玩家离开
local OnLeave = nil
--收到消息
local OnUserMessage = nil
--奖励
local OnUserAward = nil
--收到普通消息
local OnChat = nil
--收到表情
local OnFace = nil
--获取个人信息
local GetUserInfo = nil
--离开
local Leave = nil
--关闭
local closePanel = nil
--坐下
local doSit = nil
--清空椅子
local doResetChair = nil
--游戏开始
local OnMatchStart = nil
--创建自己的骰子
local createSelfWay = nil
--显示选号框
local showShoutPanel = nil
--显示喊按钮
local viewShoutBtn = nil
--显示开按钮
local viewOpenBtn = nil
--隐藏开按钮
local hideOpenBtn = nil
--隐藏选号框
local hideShoutPanel = nil
--创建选号框
local createShoutPanel = nil
--喊
local doShout = nil
--开
local doOpen = nil
--本人喊或开后、或倒计时结束统一处理
local afterShoutOrOpen = nil
--显示玩家读秒
local doUserTimer = nil
--销毁玩家读秒
local doKillUserTimer = nil
--玩家喊号
local OnUserShout = nil
--玩家开
local OnUserOpen = nil
--显示开动画
local doUserOpen = nil
--显示几个几
local doShowWayCount = nil
--游戏结束开几个几
local doShowWay = nil
--显示底金倍数
local showDouble = nil
--显示赢/输的积分动画
local doShowScore = nil
--有人开(游戏结束)
local OnUserOpen = nil
--显示胜负标志
local doWinLoseMark = nil
--显示赢取金币
local doWinAction = nil --金币飞动相对调整
--获取财富信息
local doRefreshWealth = nil
--重置比赛
local doResetMatch = nil
--开骰情况
local createWay = nil
--播放火星
local doShowStar = nil
--显示游戏开始倒计时
local doCountDown = nil --从第几秒开始倒计时
--游戏结束，准备下一轮游戏
local doGameReady = nil
--预加载声音
local preLoadSound = nil
--声效控制
local doPlaySound = nil
--判断旁观
local IsWatch = nil
--获取房间ID
local GetRoomID = nil
--退出房间
local ExitRoom = nil
--显示等待动画
local ShowReady = nil
--隐藏等待动画
local HideReady = nil
--启动心跳
local HeartBeat = nil
--托管叫号
local Agent = nil
--进入后台
local EnterBackground = nil
--进入前台
local EnterForgeground = nil
--旁观提示
local ShowLookOnTips = nil
--隐藏旁观提示
local HideLookOnTips = nil
--有玩家使用道具
local onUserUseProps = nil


-------------------------------------
--当前醉酒
local currentDrunkenness = {}
local totalDrunkenness = 5
local drunkDeskId = {}
--标志游戏房间存在
local roomGamePanelExsit = false
--修正旁观状态的辅助标志
local readyGame = false
--坐下状态
local sitState = false

function getLevel()
	return nLevel
end

function getRoomGamePanelExsit()
	return roomGamePanelExsit
end

function Game_CB(data)
	--d(data)
	isAlive = true --连接未中断
	local ret = json.decode(data)	
	if ret.type == "login" then
		if ret.result == 0 then		
			OnLoginOk()
		else
			OnLoginFail()
		end
	elseif ret.type == "sitdown" then
		if ret.result == 0 then
			OnUserCome(ret.data)
		else
			OnSitFail()
		end
	elseif ret.type == "leave" then
		OnLeave(ret.data)
	elseif ret.type == "gamestart" then			
		OnMatchStart(ret.data)
	elseif ret.type == "shout" then
		OnUserShout(ret.data)
	elseif ret.type == "open" then
		if isEnterbackground == true then
			ExitRoom()
		else
			OnUserOpen(ret.data)
		end
	elseif ret.type == "message" then
		OnUserMessage(ret.data)
	elseif ret.type == "award" then
		OnUserAward(ret.data)
	elseif ret.type == "sitok" then
		OnSitOk(ret.data)
		--tcp_useprops(10948,50,1)
	elseif ret.type == "enterbackground" then
		EnterBackground(ret.data)
		isEnterbackground = true
	elseif ret.type == "enterforeground" then
		EnterForgeground(ret.data)
	elseif ret.type == "gameplaying" then
		ShowLookOnTips()
	elseif ret.type == "useprops" then
		onUserUseProps(ret.data)
	elseif ret.type == "kick" then
		tcp_leave()	
	end	
end

local function createTopScrollPanel(parent, x, y, content)
	if parent.scrollTxt then
		parent.scrollTxt._scroll_text:Destroy()
		clear( { parent.scrollTxt } )
		parent.scrollTxt = nil
	end
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	--ret_panel.bg_pic = createSprite( ret_panel, RoomMatchConfig.scroll_bg )		--公告栏底图图片

	ret_panel._scroll_text = LIGHT_UI.clsRecycleScroll:New(ret_panel, RoomMatchConfig._scroll_text.x, 
	RoomMatchConfig._scroll_text.y, RoomMatchConfig._scroll_text.width, RoomMatchConfig._scroll_text.height, true ) --公告文字位置修改
	RoomMatchConfig.scroll_text.text = content
	local txt1 = createLabel( nil, RoomMatchConfig.scroll_text )
	txt1:setTextColor( 255, 255, 255 )
	ret_panel._scroll_text:addInnerObj(txt1, 260 )
	ret_panel._scroll_text:startScroll( 1, 1 )

	return ret_panel
end


function doResetChair(idx)
	--d("doResetChair:" .. tostring(idx ) )
	if idx then
		if RoomMatch[ "_desk" ][idx] then
			clear({RoomMatch[ "_desk" ][idx]})
			RoomMatch[ "_desk" ][idx] = nil
		end
		
		if isWatch == true then 
			CreateSit(idx)
		end
	else	
	    for i = 1,MAX_CHAIR do
			if RoomMatch[ "_desk" ][i] then
				clear({RoomMatch[ "_desk" ][i]})
				RoomMatch[ "_desk" ][i] = nil
			end
		end
	end
end

function doSit(idx)
	tcp_sitdown(RoomID,idx - 1)
end

--本人喊或开后、或倒计时结束统一处理
function afterShoutOrOpen( index )
	--stopTimer( index )	
	hideOpenBtn()
	hideShoutBtn()
	
	if RoomMatch._shoutselpanel then	
		clear( {RoomMatch._shoutselpanel} )
		RoomMatch._shoutselpanel = nil
	end
end

--喊
function doShout()
	local c = RoomMatchConfig.shout_item
	
	if not nowShout.count then
		showMessage( RoomMatch, c.count_str )
		return
	end
		
	if not nowShout.number then
		showMessage( RoomMatch, c.number_str )
		return
	end
	
	if nowShout.number == 1 then
		nowShout.zhai = 1
	end
	
	--计算总和，便于大小比较：百、十、个位分别为：数量、斋、数字
	local function calSum( data )
		local sum = 0
		sum = data.count * 100 + data.zhai * 10
		
		if data.number == 1 then
			sum = sum + 7
		else
			sum = sum + data.number
		end
		
		return sum
	end
	
	if lastShout.index then
		if calSum( nowShout ) <= calSum( lastShout ) then
			showMessage( RoomMatch, c.way_str )
			return
		end
				
		if lastShout.zhai == 1 then
			if nowShout.zhai ~= 1 and lastShout.count * 2 > nowShout.count then
				showMessage( RoomMatch, c.cut_zhai_str )
				return
			end
		end
	end
	
	nowShout.zhai = nowShout.zhai or 0	
	
	tcp_shout(nowShout.count,nowShout.number,nowShout.zhai)	
	if isMyTimeBegin == true then
		dailyCount('UserOperateEnd',2)--玩家操作结束
		isMyTimeBegin = false
	end
	afterShoutOrOpen( 1 )
end

--开
function doOpen()
	if isMyTimeBegin == true then
		dailyCount('UserOperateEnd',2)--玩家操作结束
		isMyTimeBegin = false
	end
	tcp_open()
	afterShoutOrOpen( 1 )
	
end

function OnLeave(data)
	local idx = GetChairIdx(data.chair)
	d("OnLeave" .. tostring(idx))
	doResetChair(idx)
	nPlayer = nPlayer - 1
	if (nPlayer <= 1) then
		isPlaying = false
	end
	--如果当前离线用户是读秒用户，则删除计时器
	if RoomMatch.CurTimerDeskID and RoomMatch.CurTimerDeskID == idx then
		doKillUserTimer()
	end
end

function Leave()
	if isWatch == true or isPlaying == false then
		ExitRoom()
	elseif isPlaying then
		isLeave = true
		if currentDrunkenness[ 1 ] < totalDrunkenness then
			local op = {ms = 3000}
			showMessage( RoomMatch, '游戏进行中，请稍等，当前局结束为您退出',op )
		end
	end
end


function GetUserInfo(uid,cb)
	doCommand( 'room_server', 'my_info', {uid = uid}, cb )
end

function OnLoginOk()
	tcp_sitdown(RoomID,ChairID)		
end

function OnLoginFail()

end

local function saveMsg( data, chairIdx )
	currentDrunkenness[ chairIdx ] = data.drunk
	drunkDeskId[ data.uid ] = chairIdx
	totalDrunkenness = data.maxdrunk
end

function OnUserCome(data)
	--[[if data.vip ~= 0 then
		RoomMatch.scrollTxt = createTopScrollPanel( RoomMatch, 60, 270, "天边忽现佛光，" .. data.name .. "闪亮登场！！" )
	end--]]
	local chairIdx = 0
	if data.uid == tonumber(GLOBAL.uid) then
		MyChairID = data.chair	
	end
	chairIdx = GetChairIdx(data.chair)		
	if data.uid == tonumber(GLOBAL.uid) then	
		if data.vip ~= 0 then
			RoomMatch.scrollTxt = createTopScrollPanel( RoomMatch, 60, 270, "天边乍现佛光，VIP-" .. base64Decode(data.name) .. "闪亮登场！！" )
			RoomMatch.vip_coming_pic = createSprite( RoomMatch, RoomMatchConfig.vip_coming_pic, nil, 200 )
			RoomMatch.vip_coming_pic:setVisible( false )
			if chairIdx == 1 then
				RoomMatch.vip_coming_pic:setPosition( RoomMatchConfig["open"..chairIdx].x - 51, RoomMatchConfig["open"..chairIdx].y - 47 )					
			elseif 	chairIdx == 2 then	
				RoomMatch.vip_coming_pic:setPosition( RoomMatchConfig["open"..chairIdx].x + 34, RoomMatchConfig["open"..chairIdx].y - 43 )	
			elseif 	chairIdx == 3 then
				RoomMatch.vip_coming_pic:setPosition( RoomMatchConfig["open"..chairIdx].x - 43, RoomMatchConfig["open"..chairIdx].y - 45 )
			else
				RoomMatch.vip_coming_pic:setPosition( RoomMatchConfig["open"..chairIdx].x - 54, RoomMatchConfig["open"..chairIdx].y - 45 )
			end
			
			
			local isLoad = false
			local function waitForImageCompleteHandler()
				if isFileExists( getResFilename( RoomMatchConfig.vip_coming_animation.res) ) == true then 	
					RoomMatch.vip_coming_pic:setVisible( true )			
					a_play( RoomMatch.vip_coming_pic:getSprite(), RoomMatchConfig.vip_coming_animation,false)
					if waitForImageCompleteTimer then
						CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(waitForImageCompleteTimer)
						waitForImageCompleteTimer = nil
					end	
				else
					if isLoad == false then
						isLoad = true
						local o = createSprite( ret_panel, {x=0,y=0,res=RoomMatchConfig.vip_coming_animation.res} )
						o:setVisible( false )
					end	
				end	
			end
			
			if isFileExists( getResFilename( RoomMatchConfig.vip_coming_animation.res) ) == true then
				RoomMatch.vip_coming_pic:setVisible( true )
				a_play( RoomMatch.vip_coming_pic:getSprite(), RoomMatchConfig.vip_coming_animation,false)
			else
				if not waitForImageCompleteTimer then
					waitForImageCompleteTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( waitForImageCompleteHandler, 0, false )
				end
			end
			
		end
		MyChairID = data.chair	
		readyGame = true
		
		doResetMatch()
		doResetChair()			
		doRefreshWealth()
		--坐下成功,启动心跳
		HeartBeat()	
	else	
		if sitState == true then
			if data.vip ~= 0 then
				RoomMatch.scrollTxt = createTopScrollPanel( RoomMatch, 60, 270, "天边乍现佛光，VIP-" .. base64Decode(data.name) .. "闪亮登场！！" )
				RoomMatch.vip_coming_pic = createSprite( RoomMatch, RoomMatchConfig.vip_coming_pic, nil, 200 )
				RoomMatch.vip_coming_pic:setVisible( false )
				if chairIdx == 1 then
					RoomMatch.vip_coming_pic:setPosition( RoomMatchConfig["open"..chairIdx].x - 51, RoomMatchConfig["open"..chairIdx].y - 47 )					
				elseif 	chairIdx == 2 then	
					RoomMatch.vip_coming_pic:setPosition( RoomMatchConfig["open"..chairIdx].x + 34, RoomMatchConfig["open"..chairIdx].y - 43 )	
				elseif 	chairIdx == 3 then
					RoomMatch.vip_coming_pic:setPosition( RoomMatchConfig["open"..chairIdx].x - 43, RoomMatchConfig["open"..chairIdx].y - 45 )
				else
					RoomMatch.vip_coming_pic:setPosition( RoomMatchConfig["open"..chairIdx].x - 54, RoomMatchConfig["open"..chairIdx].y - 45 )
				end
							
				local isLoad = false
				local function waitForImageCompleteHandler()
					if isFileExists( getResFilename( RoomMatchConfig.vip_coming_animation.res) ) == true then 	
						RoomMatch.vip_coming_pic:setVisible( true )			
						a_play( RoomMatch.vip_coming_pic:getSprite(), RoomMatchConfig.vip_coming_animation,false)
						if waitForImageCompleteTimer then
							CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(waitForImageCompleteTimer)
							waitForImageCompleteTimer = nil
						end	
					else
						if isLoad == false then
							isLoad = true
							local o = createSprite( ret_panel, {x=0,y=0,res=RoomMatchConfig.vip_coming_animation.res} )
							o:setVisible( false )
						end	
					end	
				end
				
				if isFileExists( getResFilename( RoomMatchConfig.vip_coming_animation.res) ) == true then
					RoomMatch.vip_coming_pic:setVisible( true )
					a_play( RoomMatch.vip_coming_pic:getSprite(), RoomMatchConfig.vip_coming_animation,false)
				else
					if not waitForImageCompleteTimer then
						waitForImageCompleteTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( waitForImageCompleteHandler, 0, false )
					end
				end
			end
		end
	end
	chairIdx = GetChairIdx(data.chair)	
	saveMsg( data, chairIdx )
	local function onInfo(data)	
		if RoomMatch[ "_desk" ][chairIdx] then
			clear({RoomMatch[ "_desk" ][chairIdx]})
			RoomMatch[ "_desk" ][chairIdx] = nil
		end
		RoomMatch[ "_desk" ][chairIdx] = CreateDesk(chairIdx,data)
	end
	--GetUserInfo(data.uid,onInfo)
	d("OnUserCome" .. tostring(chairIdx))
	onInfo(data)
	nPlayer = nPlayer + 1
end

function OnSitOk(data)
	sitState = true
	local strGold = string.format("消耗房间服务费%d金币",base_gold*0.5)
	local op = {ms = 3000}
	if tonumber(data.lookon) == 1 then	
		--showMessage( RoomMatch, "你正在旁观，请等待",op )
		ShowLookOnTips('旁观状态，待他人离开后可加入游戏')
	else		
		showMessage( RoomMatch, strGold,op )		
	end				
end

function OnSitFail()
	CCMessageBox('坐下失败','sitdown fail')
end



local function createPanel(parent, x, y)
	local ret_panel = createNode( parent, point( x, y ) )
	
	local o = nil
	local c, c2 = RoomMatchConfig, nil	
	
	
	--o = createSprite( ret_panel, c.head_bg )
	--o:setAnchorPoint(0, 0)
	local function onClick()	
	end
	o = createButton( ret_panel, c.head_bg )
	o.onTouchEnd = onClick
	
	
	local function on_back(btn)		
		Leave()		
	end
	
	o = createButton( ret_panel, c.head_back )
	o.onTouchEnd = on_back
	
	o = createLabel( ret_panel, c.head_text )
	o:setAnchorPoint(0, 0)
	ret_panel._name = o
	
	o = createLabel(ret_panel, c.head_gold)
	o:setAnchorPoint(0, 0)
	ret_panel._match_gold = o
	
	ret_panel._desk = {}
	
	local no = string.format( '%s%03d', level_name[tostring(nLevel)],tonumber(RoomNO) )
	o = createLabel( ret_panel, c.room_label)
	o:setString(no)	
		
	return ret_panel
end

function showPanel(parent, x, y)
	dailyCount('game')--游戏
	if not RoomMatch then
		RoomMatch = createPanel(parent, x, y)
	else
		RoomMatch:setVisible(true)
	end
	RoomMatch.scrollTxt = createTopScrollPanel( RoomMatch, 60, 270, "醉酒度达到上限将被踢出房间，请及时解酒" )
	roomGamePanelExsit = true
	Initialize()
	
	RoomMatch.IsWatch = IsWatch
	RoomMatch.GetRoomID = GetRoomID
	RoomMatch.doRefreshWealth = doRefreshWealth		
		
	showActBtn(RoomMatch,true) --初始化托管按钮
	
	showChatBtn(RoomMatch) --初始化聊天按钮
	
	showFaceBtn(RoomMatch)  --初始化表情按钮
	
	ShowReady()
	--发送登录
	tcp_login(IP,PORT,GLOBAL.uid,'1234',Game_CB)		
	
	local function reconectHandler()
		if sitState == false then
			tcp_login(IP,PORT,GLOBAL.uid,'1234',Game_CB)
		else
			if reconnectTimer then
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(reconnectTimer)
				reconnectTimer = nil
			end
		end
	end
	
	if not reconnectTimer then
		reconnectTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( reconectHandler, 3, false )
	end
end

local function stopSound(effectID)
	SimpleAudioEngine:sharedEngine():stopEffect(effectID)
end

function closePanel()
	RoomMatch.scrollTxt._scroll_text.tryStopScroll()
	HideReady()
	if not RoomMatch then
		return
	end
	RoomMatch:setVisible(false)
	RoomMatch:removeFromParentAndCleanup( true )
	RoomMatch = nil
	roomGamePanelExsit = false
	
	for i=1, table.maxn(sound_list) do  	 
		stopSound(sound_list[i])				
	end		
end

function getBaseGold(level)
	local ret = 100
	if(level == 1) then
		ret = 100	
	elseif(level == 2) then
		ret = 500
	else
		ret = 1000
	end
	return ret
end

function init( data )
	nLevel = data.level
	base_gold = getBaseGold(data.level)
	RoomID = data.room_id - 1	
	ChairID = data.chair_id - 1
	IP = data.ip
	PORT = data.port
	RoomNO = data.room_no
	PrePageNo = data.room_page

	local function back_fun()
		local ROOM = Import("lua/room.lua")
		ROOM.showPanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
	
	if data.back_func then
		back_func = data.back_func
	else
		back_func = back_fun
	end
	
	sound_list = {}
	--预加载声音
	preLoadSound()
end	



--[[
创建有用户的桌
参数：
	桌索引：1为自己
	用户信息：nil表示空位
]]
function CreateDesk( index, info )
	local p = RoomMatch	
	--位置配置文件
	local c = RoomMatchConfig
	local ret_panel = nil	
	local o = nil	
	ret_panel = createNode(p, c["desk"..index],nil,UserDeskZOrder)	
	--盅
	o = createSprite( ret_panel, c["zhong"..index] )
	ret_panel._zhong = o	
	local logo_panel = nil
	logo_panel = createNode( ret_panel, c["logo"..index] )
	ret_panel.logoPanel = logo_panel
	
	--显示开
	local function show( type )
		local c = RoomMatchConfig[ 'open' .. inde5x]
		c.res = RoomMatchConfig[type].res
		
		createSprite(logo_panel, c, RoomMatchConfig.delay[type] )
	end
	ret_panel.show = show
    --椅子背景
    --local o_bg = createSprite(logo_panel,c["desk_bg"])
	local function onClick()
		if uid ~= p._desk[index]._uid then
			showRoomUserInfo(RoomMatch,p._desk[index]._uid)
		end
	end
	local o_bg = createButton(logo_panel,c["desk_bg"],onClick)
    ret_panel._bg = o_bg
    
	o = createLabel(logo_panel, c.name)	

	ret_panel._name = o
	
	if info then		
		local c = { x = 0,y = 0,sx = 74,sy = 74, res = info.logo }
		o = createSprite(logo_panel, c )		
		o:setImage( info.logo )		
		--AdjustString(ret_panel._name,info.uid,70)			
		AdjustString(ret_panel._name,base64Decode(info.name),70)		
		
		ret_panel._uid = info.uid
	end
	
	--醉酒度
	o = createSprite(logo_panel, { x = 0,y = -30,sx = 75,sy = 17, res = 'zjd-bg.png' } )
	local t = { x = -18,y = -30,sx = 16,sy = 16, res = 'jzd-h.png' }
	o = createSprite(logo_panel, t )
	
	if info then
		t = { x = 5, y = -29, text = info.drunk .. "/" .. info.maxdrunk, css = 'a4' }
		logo_panel.drunk = createLabel(logo_panel, t)
	else
		t = { x = 5, y = -29, text = '加载中', css = 'a4' }
		logo_panel.drunk = createLabel(logo_panel, t)	
	end
	
	--喊号情况
	local hhk = createNode( logo_panel, c[ 'hhk' .. (index) ] )
	
	createSprite( hhk,  res( 0, 0, "hhk.png") )     --喊号内部文字调整
	o = createLabel( hhk, text( -30, 0, "5 ×" ) )
	hhk._count = o
	
	o = createSprite(hhk, res( 5, 0, "szs1.png") )
	hhk._number = o
	
	o = createLabel(hhk, RoomMatchConfig.hhxs )
	hhk._zhai = o

	hide({hhk})
	ret_panel._hhk = hhk
	
	local function showHHK( data )
		local hhk = ret_panel._hhk
	
		hhk._count:setString( data.count .. " ×" )
		hhk._number:setImage( "szs" .. data.number .. ".png" )
		
		if data.zhai == 1 then
			hhk._zhai:setString( "斋" )
		else
			hhk._zhai:setString( "" )
		end
		
		view(hhk)
	end
	ret_panel.showHHK = showHHK
	
	return ret_panel
end

function GetChairIdx(chair_id)
	if MyChairID then	
        if(chair_id < MyChairID) then
            return (MAX_CHAIR  - math.abs(MyChairID - chair_id)) % MAX_CHAIR + 1;
        else
            return (chair_id - MyChairID) % MAX_CHAIR + 1;	
		end	
	else	
		chair_id = chair_id + 1		
	end
	return chair_id
end

function CreateSit(chairIdx)
	--d( "c:" .. c["desk"..chairIdx] )
	if RoomMatch and RoomMatch[ "_desk" ][chairIdx] then
		clear({RoomMatch[ "_desk" ][chairIdx]})
	end
	local p = RoomMatch	
	--位置配置文件
	local c = RoomMatchConfig
	local ret_panel = nil	
	d( "chairIdx:" .. chairIdx )	
	--d( "c:" .. c["desk"..chairIdx] )
	
	--创建坐下按钮
	ret_panel = createNode(p, c["desk"..chairIdx])
	local function onClick()
		--print("sit down")
		doSit(chairIdx) --选择位置坐下
	end					
	local o = createButton(ret_panel,c["sitdown"..chairIdx],onClick)
	ret_panel._sitdown = o	
	--坐下动画
	local a = createSprite(ret_panel,c["a_sitdown"..chairIdx])
	a_play(a:getSprite(),c["action_sitdown"],true)												
	
	RoomMatch[ "_desk" ][chairIdx] = ret_panel
end

--创建自己的骰
function createSelfWay()
	--print("createSelfWay")
	local c = RoomMatchConfig.selfway
	if RoomMatch._selfway then
		clear({RoomMatch._selfway})			
	end
	local ret_panel = createNode(RoomMatch,c,nil,UserWayZOrder)
			
	local o = nil
			
	local start_x = -240
	for i = 1, 5 do
		o = createSprite( ret_panel, res( start_x, 0, "szs1.png" ) )
		o:setAnchorPoint(0, 0)
		start_x = start_x + 48
		ret_panel[i] = o
	end
	RoomMatch._selfway = ret_panel		
	return ret_panel									
end

--摇动骰子
local function doShakeCup(fn,arg)
	local rock = RoomMatchConfig.rock
	doPlaySound( rock.sound, rock.ms )
	local isShake = getUserString( "shake" )
	if tonumber( isShake ) == 1 then
		if xymodule.shake then
			xymodule.shake(0.5,1)
		end
	end
	
	if RoomMatch._desk[ 1 ] and RoomMatch._desk[ 1 ]._zhong then
		a_shake(RoomMatch._desk[ 1 ]._zhong:getCOObj(),0.1,5,15,0,fn,arg)
	else
		fn(arg)
	end
	for i = 2,4 do
		if RoomMatch._desk[ i ] and RoomMatch._desk[ i ]._zhong then
			a_shake(RoomMatch._desk[ i ]._zhong:getCOObj(),0.1,5,15,0)
		end
	end
end

--移动骰子
local function doMoveMyWay(match_data)
	local dice_move_speed = 1000
	--print("doMoveMyWay")
	if RoomMatch._selfway then
		local way = match_data.way
		assert(tonumber(way) > 0)	
		for i = 1, 5 do
			local key = string.sub(way, i, i)	
					
			RoomMatch._selfway[i]:setImage("szm"..key..".png")		
			RoomMatch._selfway[i]:setAnchorPoint(0, 0)			
		end
		--print("LIGHT_UI.moveObj")
		LIGHT_UI.moveObj(RoomMatch._selfway, dice_move_speed, 315+240 )
	end
end	

--显示开
function viewOpen( index )
	if RoomMatch._desk[ index ] then
		RoomMatch._desk[ index ].show( 'open' )
	end
end

--显示喊按钮
function viewShoutBtn()
    if RoomMatch._shout_btn then
	    view( RoomMatch._shout_btn )
	end
end

--隐藏喊按钮
function hideShoutBtn()
    if RoomMatch._shout_btn then
	    hide( {RoomMatch._shout_btn} )
	end
end

--显示开按钮
function viewOpenBtn()
    if RoomMatch._open_btn then
	    view( RoomMatch._open_btn )
	end
end

--隐藏开按钮
function hideOpenBtn()
    if RoomMatch._open_btn then
	    hide( {RoomMatch._open_btn} )
	end
end

--隐藏选号框
function hideShoutPanel()
    if RoomMatch._shoutselpanel then
	    clear( {RoomMatch._shoutselpanel} )
	    RoomMatch._shoutselpanel = nil
	end
end

--隐藏喊号框
local function hideLastHHK()
	for i = 1,4 do
		if RoomMatch._desk[ i ] and RoomMatch._desk[ i ]._hhk then
			hide( {RoomMatch._desk[ i ]._hhk} )
		end
	end		
end

--显示选号框
function showShoutPanel(delay)
    if delay then
        f_delay_do(RoomMatch,createShoutPanel,nil,tonumber(delay))
    else
        f_delay_do(RoomMatch,createShoutPanel,nil,0)
    end
end

--创建选号框
local function createShout(parent, x, y)
	--骰子数
	local person = matchData.player
	
	local max_cnt = person * 4 + 1
	local cnt_width = 60
	local all_cnt_width = max_cnt * cnt_width 

	local ret_panel = createNode( parent, point( x, y),nil,UserShoutPannelZOrder )
	ret_panel._cnt_list = {}
	local c = RoomMatchConfig.shout_item
	local c2 = nil
	
	local o = nil
	local function onClick()
	end	
	--o = createSprite( ret_panel, res(0, 0, "cz-bg.png") )
	o = createButton(ret_panel, res(0, 0, "cz-bg.png") ,onClick )
	o:setAnchorPoint(0, 1)

	o = createButton(ret_panel, RoomMatchConfig.shout_close )
	o.onTouchEnd = hideShoutPanel

	local cnt_move_delta = c.cnt_move_delta

	local function on_left(btn, x, y)
		max_cnt = #ret_panel._cnt_list
		local head_x, _ = ret_panel._cnt_list[1]:getPosition()		
		if head_x >= 0 then
			return
		end

		for i = 1, max_cnt do
			local x, y = ret_panel._cnt_list[i]:getPosition()
			x = x + cnt_move_delta
			ret_panel._cnt_list[i]:setPosition(x, y)
		end
	end

	local function on_right(btn, x, y)
		max_cnt = #ret_panel._cnt_list
		local tail_x, _ = ret_panel._cnt_list[max_cnt]:getPosition()
		if tail_x <= 0 then
			return
		end

		for i = 1, max_cnt do
			local x, y = ret_panel._cnt_list[i]:getPosition()
			x = x - cnt_move_delta
			ret_panel._cnt_list[i]:setPosition(x, y)
		end
	end

	o = createButton(ret_panel, res( c.left_btn_x, c.left_btn_y, c.left_btn_res) )
	o.onTouchEnd = on_left
	
	o = createButton(ret_panel, res( c.right_btn_x, c.right_btn_y, c.right_btn_res, c.right_btn_res) )
	o.onTouchEnd = on_right
	
	o = LIGHT_UI.clsClipLayer:New(ret_panel, c.shout_view_x, c.shout_view_y)
	--o = LIGHT_UI.clsClipLayer:New(ret_panel, 70, c.shout_view_y)
	o:set_msg_rect( c.x, c.y, c.sx, c.sy )
	local shout_cnt_panel = o

	local startx = 27   --喊号框数目设置
	local starty = 26
	local x_step = 54
	local count = {}
	local function onSelectCount(obj, x, y)
		if nowShout.count then
			if nowShout.count == obj.count then
				return
			end
			
			local o = nil
			o = count[nowShout.count]
			o:setTextColor(51, 51, 51)
			o:setStatus(LIGHT_UI.NORMAL_STATUS)
		end
		
		nowShout.count = obj.count
		obj:setStatus(LIGHT_UI.DOWN_STATUS)
		obj:setTextColor(255, 255, 255)
	end
	
	local min_count = person
	if lastShout.index then
		if lastShout.number == 1 then
			min_count = lastShout.count + 1
		else
			min_count = lastShout.count
		end
	end
	
	local btn_res = {
		[LIGHT_UI.NORMAL_STATUS] = 'shuzi.png',
		[LIGHT_UI.DOWN_STATUS] = 'shuzi_on.png',
	}
	
	for i = min_count, person * 5 do
		o = LIGHT_UI.clsBaseButton:New(shout_cnt_panel, startx, starty, btn_res)
		
		o.count = i
		o:setString(tostring(i))
			o:setTextColor(102, 102, 102)
		o:setStatus(LIGHT_UI.NORMAL_STATUS)
		o.onDownChangeSprite = empty_function
		o.onUpChangeSprite = empty_function
		o.onTouchUp = onSelectCount
		count[i] = o
		table.insert(ret_panel._cnt_list, o)
		
		startx = startx + x_step
	end

	local dice = {}
	local function onSelectNumber(obj, x, y)
		if nowShout.number then
			if nowShout.number == obj.number then
				return
			end
			
			dice[nowShout.number]:setStatus( LIGHT_UI.NORMAL_STATUS )
		end
		
		nowShout.number = obj.number
		obj:setStatus( LIGHT_UI.DOWN_STATUS )
	end
	startx = c.sel_dice_startx 
	starty = c.sel_dice_starty
	x_step = 60
	
	for i = 1, 6 do
		local normal_file = string.format("sz%d.png", i)
		local click_file = string.format("sz%d_on.png", i)
		o = createButton( ret_panel, res( startx, starty, normal_file, click_file) )
		o.number = i
		o.onTouchEnd = onSelectNumber
		dice[i] = o
		startx = startx + x_step
	end
	
	local function doJiaYi()
		if not lastShout.index then
			return
		end
		
		if lastShout.count == matchData.player * 5 then
			showMessage( RoomMatch, '数量太大了，不能再加1了' )
			return
		end
		
		nowShout.count, nowShout.number, nowShout.zhai = lastShout.count + 1, lastShout.number, lastShout.zhai
		
		doShout()
	end

	o = createButton( ret_panel, res( c.plus_btn_x, c.plus_btn_y, "jiayi.png", "jiayi_on.png") )
	o.onTouchEnd = doJiaYi

	local function onSelectZhai(obj, x, y)
		if nowShout.zhai == 1 then
			nowShout.zhai = 0
			obj:setStatus(LIGHT_UI.NORMAL_STATUS)			
		else
			nowShout.zhai = 1
			obj:setStatus(LIGHT_UI.DOWN_STATUS)			
		end
	end
	o = createButton( ret_panel, res( c.zhai_btn_x, c.zhai_btn_y, "zai.png", "zai_on.png" ) )
	o.onTouchEnd = onSelectZhai
	
	if lastShout.index then
		if lastShout.zhai == 1 then
			nowShout.zhai = 1
			o:setStatus(LIGHT_UI.DOWN_STATUS)
		else
			nowShout.zhai = 0
		end
	else
		nowShout.zhai = 0
	end 

	o = createButton( ret_panel, res( c.ok_btn_x, c.ok_btn_y, "ok.png", "ok_on.png" ) )
	o:setTextColor(0,0,0)
	o.onTouchEnd = doShout

	return ret_panel
end

--创建选号面板
function createShoutPanel()
	if RoomMatch._shoutselpanel then
		clear( {RoomMatch._shoutselpanel} )
		RoomMatch._shoutselpanel = nil
	else	
		nowShout = {
			count = nil,
			number = nil,
			zhai = nil,
		}	
		o = createShout( RoomMatch, 20, 500)
		RoomMatch._shoutselpanel = o
	end
end

--自己倒计时超时，关闭喊面板及按钮
function onMeTimeOut()
	if not isWatch then
		hideShoutPanel()
		hideShoutBtn()
		hideOpenBtn()
	end
end

--销毁玩家读秒
function doKillUserTimer()
    if RoomMatch._UserTimer then
	    clear({RoomMatch._UserTimer})
	    RoomMatch._UserTimer = nil
		RoomMatch.CurTimerDeskID = nil
	end
end

--显示玩家读秒
function doUserTimer(desk_id) --椅子ID
	assert(desk_id > 0 and desk_id <= 4)
	doKillUserTimer()
	local c = { x = 0,y = 0}
	--local order = RoomMatch:getCOObj():getChildrenCount() + UserTimerZOrder
	local order = UserTimerZOrder
	local n = createNode(RoomMatch,c,nil,order)	
	local c = RoomMatchConfig	
	local o = createSprite(n,c["sitdown"..desk_id])
	RoomMatch._UserTimer = n	
	if desk_id == 1 and not isWatch then --自己超时，则隐藏选择面板	
	    a_play(o:getSprite(),c["action_timeout"],false,nil,nil,onMeTimeOut)
	else
	    a_play(o:getSprite(),c["action_timeout"],false)
	end		
	o:setPosition(c["timeout"..desk_id].x,c["timeout"..desk_id].y)
	RoomMatch.UserTimer = a
	RoomMatch.CurTimerDeskID = desk_id	
end


--创建自己的骰
function OnMatchStart(data)
	
	if readyGame == true then
		readyGame = false
		isWatch = false
	end
	
	HideReady()
	HideLookOnTips()
	
	if tonumber(data.way) > 0 then
		isPlaying = true
	end
	doResetMatch()
	
	matchData = data
	base_gold = data.basegold
	
	lastShout = {
		count = 0,
		number = 0,
		zhai = 0,
		index = nil,
	}
	
	local function doShake()
		doShakeCup(doMoveMyWay,data)		
	end
	if isWatch == false then
		createSelfWay()
	end
	if not RoomMatch._shout_btn then
		o = createButton(RoomMatch, RoomMatchConfig.shout_btn )
		o.onTouchEnd = showShoutPanel
		hide( {o} )
		RoomMatch._shout_btn = o
	end
	
	if not RoomMatch._open_btn then
		o = createButton(RoomMatch, RoomMatchConfig.open_btn )
		o.onTouchEnd = doOpen
		hide({o})
		RoomMatch._open_btn = o
	end
	
	local function onShowShout()
		if not RoomMatch._shoutselpanel and 
		GetChairIdx(matchData.next_chairid) == 1 and 
		isWatch == false then		
			showShoutPanel()			
			viewShoutBtn()			
		end
	end
	
	doShake()
	--延时2秒显示选号框
	if IsAgent() == false then
		if isWatch == false then
			f_delay_do(RoomMatch,onShowShout,nil,2)	
			if GetChairIdx(matchData.next_chairid) == 1 then
				dailyCount('UserOperateBegin',1)--玩家操作开始
				isMyTimeBegin = true
			end					
		end
		
		f_delay_do(RoomMatch,doUserTimer,GetChairIdx(matchData.next_chairid),2)

	else
		f_delay_do(RoomMatch,doUserTimer,GetChairIdx(matchData.next_chairid),2)
		if GetChairIdx(matchData.next_chairid) == 1 and isWatch == false then
			
			
			math.randomseed(os.time())
			nowShout.count = math.random(matchData.player,matchData.player*3)
			nowShout.number = math.random(2,6)
			nowShout.zhai = math.random(0,1)
			local time = math.random(2,15)
			f_delay_do(RoomMatch,Agent,nil,time)			
		end
	end		
end

--玩家喊号
function OnUserShout(data)
	hideShoutPanel()
	hideShoutBtn()	
	hideOpenBtn()	
	if lastShout.index then
		hideLastHHK()
	end

	HideReady()
	
	lastShout = {
		count = tonumber(data.count),
		number = tonumber(data.number),
		zhai = tonumber(data.fast),
		index = GetChairIdx(data.chairid),
	}
			
	showDouble(data.basegold)	
	
	--显示倒计时
	doUserTimer(GetChairIdx(data.next_chairid) )
	
	if RoomMatch._desk[ lastShout.index ] then
		doPlaySound(RoomMatchConfig.shout.sound,0)
		RoomMatch._desk[ lastShout.index ].showHHK( lastShout )
	end					
	
	--print( "lastShout.index:" .. lastShout.index )
	--print( "isPlaying:" .. tostring(isPlaying) )
	if lastShout.index ~= 1 and isPlaying == true then
		viewOpenBtn()		
	end
	
	if GetChairIdx(data.next_chairid) == 1 and isPlaying == true then	
		dailyCount('UserOperateBegin',1)--玩家操作开始
		isMyTimeBegin = true 			
		if IsAgent() == false then
			viewShoutBtn()			
		else
			--doOpen()
			local time = math.random(2,15)
			f_delay_do(RoomMatch,Agent,true,time)			
		end
	end				
end

--翻倍
local function hideDouble()
	if RoomMatch and RoomMatch.double then
		clear( { RoomMatch.double })		
		RoomMatch.double = nil
		if RoomMatch.double_text then
			clear( {RoomMatch.double_text })
			RoomMatch.double_text = nil		
		end
	end
end

function showDouble(gold)
	hideDouble()
	local c = RoomMatchConfig["double"]	
	if gold > base_gold then
		RoomMatch.double = createSprite(RoomMatch,c)
		c = RoomMatchConfig["double_text"]	
		RoomMatch.double_text = createLabel( RoomMatch, c )	
		RoomMatch.double_text:setString(string.format( '× %d', gold/base_gold ))
	end		
end

--4：有人开(游戏结束)
function OnUserOpen(data)
	if not lastShout.index then
		return 
	end
	HideReady()
	--销毁读秒
	doKillUserTimer()
	hideShoutPanel()
	hideLastHHK()
	hideOpenBtn()
	hideShoutBtn()	
	--显示底金
	showDouble(data.gold)
		
	--几个几
	local function showCount()	
		if RoomMatch._selfway then
			clear( {RoomMatch._selfway} )
			RoomMatch._selfway = nil
		end			
		local delay = 0
		for i = 1,4 do		
			if data[ tostring(i - 1) ] then
				local index = GetChairIdx(i - 1)
				local desk = RoomMatch._desk[ index ]				
				createWay( desk, index, data[ tostring(i - 1) ],
				lastShout.zhai,lastShout.number,delay )
				delay = delay + RoomMatchConfig.delay.count --每个玩家有2秒显示统计骰子动画
				--view( o)
			end
		end
	end
	
	local result = nil
	local function showWinLose()	
		doWinLoseMark(GetChairIdx(data.winner),'win')
		doWinLoseMark(GetChairIdx(data.loser),'lose')		
		local c = RoomMatchConfig		
		if isWatch == false then
			if GetChairIdx(data.winner) == 1 then
				result = 'win'				
			elseif GetChairIdx(data.loser) == 1 then
				result = 'lose'				
			end
		elseif isWatch == true then
		    if data.open == data.winner then
			    result = 'win'
			elseif data.open == data.loser then
			    result = 'lose'				
			end
		end
		
		local function createTopScrollPanel(parent, x, y, content)
			if parent.scrollTxt then
				parent.scrollTxt._scroll_text:Destroy()
				clear( { parent.scrollTxt } )
				parent.scrollTxt = nil
			end
			local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)
			
			--ret_panel.bg_pic = createSprite( ret_panel, RoomMatchConfig.scroll_bg )		--公告栏底图图片

			ret_panel._scroll_text = LIGHT_UI.clsRecycleScroll:New(ret_panel, RoomMatchConfig._scroll_text.x, 
			RoomMatchConfig._scroll_text.y, RoomMatchConfig._scroll_text.width, RoomMatchConfig._scroll_text.height, true ) --公告文字位置修改
			RoomMatchConfig.scroll_text.text = content
			local txt1 = createLabel( nil, RoomMatchConfig.scroll_text )
			txt1:setTextColor( 255, 255, 255 )
			ret_panel._scroll_text:addInnerObj(txt1, 260 )
			ret_panel._scroll_text:startScroll( 1, 1 )
			
			return ret_panel
		end
		if currentDrunkenness[ GetChairIdx(data.loser) ] < totalDrunkenness then
			currentDrunkenness[ GetChairIdx(data.loser) ] = currentDrunkenness[ GetChairIdx(data.loser) ] + 1
		end
		
		if GetChairIdx(data.loser) == 1 then
			if currentDrunkenness[ GetChairIdx(data.loser) ] >= 3 then
				createDrunkState( currentDrunkenness[ GetChairIdx(data.loser) ], RoomMatch, RoomMatchConfig )
				RoomMatch.scrollTxt = createTopScrollPanel( RoomMatch, 60, 270, "醉酒度达上限将被踢出房间，请及时解酒" )
			end
		end
		
		if currentDrunkenness[ GetChairIdx(data.loser) ] >= totalDrunkenness then
			if RoomMatch then
				RoomMatch.scrollTxt = createTopScrollPanel( RoomMatch, 60, 270, 	RoomMatch[ "_desk" ][GetChairIdx(data.loser)]._name:getString() .. "真逊，被" .. RoomMatch[ "_desk" ][GetChairIdx(data.winner)]._name:getString() .. "灌醉了" )
			end
		end
		
		if result then
			local c = c[result]
			doPlaySound( c.sound, c.ms )
			
		end
	end		
	
	local function showGold()
		local delay = RoomMatchConfig.delay.gold
		--金币动画
		local to = GetChairIdx(data.winner)
		local from = GetChairIdx(data.loser)
		--print(result)
		doWinAction(from,to,10,result)
		
		doShowScore(to,true,data.gold,delay)
		doShowScore(from,false,data.gold,delay)			
	end
	
	local function showWine()	
		local desk_id = GetChairIdx(data.loser)	
		if RoomMatch._Way and desk_id then		
			local function upHandler()
				if upTimer then
					CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(upTimer)
					upTimer = nil
				end
				if not RoomMatch then
					return
				end
				if currentDrunkenness[ desk_id ] >= 5 then
				RoomMatch.drunkenness_bg_pic = createSprite( RoomMatch, RoomMatchConfig.drunkenness_bg_pic, nil, 200 )
				if desk_id == 1 then
					RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open"..desk_id].x - 51, RoomMatchConfig["open"..desk_id].y - 47 )					
				elseif 	desk_id == 2 then	
					RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open"..desk_id].x + 34, RoomMatchConfig["open"..desk_id].y - 43 )	
				elseif 	desk_id == 3 then
					RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open"..desk_id].x - 43, RoomMatchConfig["open"..desk_id].y - 45 )
				else
					RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open"..desk_id].x - 54, RoomMatchConfig["open"..desk_id].y - 45 )
				end
				RoomMatchConfig.drunkenness_animation.res = 'zj100.png'
				a_play( RoomMatch.drunkenness_bg_pic:getSprite(), RoomMatchConfig.drunkenness_animation,false)
				elseif currentDrunkenness[ desk_id ] >= 3 then
				RoomMatch.drunkenness_bg_pic = createSprite( RoomMatch, RoomMatchConfig.drunkenness_bg_pic, nil, 200 )
				if desk_id == 1 then
					RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open"..desk_id].x - 51, RoomMatchConfig["open"..desk_id].y - 47 )					
				elseif 	desk_id == 2 then	
					RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open"..desk_id].x + 34, RoomMatchConfig["open"..desk_id].y - 43 )	
				elseif 	desk_id == 3 then
					RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open"..desk_id].x - 43, RoomMatchConfig["open"..desk_id].y - 45 )
				else
					RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open"..desk_id].x - 54, RoomMatchConfig["open"..desk_id].y - 45 )
				end
				--RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open3"].x - 43, RoomMatchConfig["open3"].y - 45 )
				--RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open2"].x + 34, RoomMatchConfig["open2"].y - 43 )
				--RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open4"].x - 54, RoomMatchConfig["open4"].y - 43 )
				--RoomMatch.drunkenness_bg_pic:setPosition( RoomMatchConfig["open1"].x - 51, RoomMatchConfig["open1"].y - 47 )
				RoomMatchConfig.drunkenness_animation.res = 'zj60.png'
				a_play( RoomMatch.drunkenness_bg_pic:getSprite(), RoomMatchConfig.drunkenness_animation,false)
				end			
				local c = RoomMatchConfig
				local c2 = c["gold"..(desk_id)]	
				local o = nil 	
				if not RoomMatch then
					return
				end
				o = createLabelAtlas(RoomMatch._Way,c["action_win_score"])
				o:setString("+"..tostring(1))
				if desk_id == 1 then                                  --加1文字位置
					o:setPosition(c2.x + 21,c2.y - 10)					
				elseif 	desk_id == 2 then	
					o:setPosition(c2.x + 28,c2.y + 50)	
				elseif 	desk_id == 3 then
					o:setPosition(c2.x + 18,c2.y + 40)
				else
					o:setPosition(c2.x + 15,c2.y + 50)
				end
				
				local function callBack()
					if RoomMatch and RoomMatch.wineCup then
						--clear( {RoomMatch.wineCup} )
						--RoomMatch.wineCup = nil
						a_fadeout( RoomMatch.wineCup:getSprite(), 0, 0.25 )
					end
				end
				if desk_id == 1 then                 --加1文字偏移位置
					a_fadein_move_fadeout(o:getCOObj(),0.25,1,ccp(c2.x + 21,c2.y + 10),0.5,0.25,0)				
				elseif 	desk_id == 2 then	
					a_fadein_move_fadeout(o:getCOObj(),0.25,1,ccp(c2.x + 28,c2.y + 70),0.5,0.25,0)
				elseif 	desk_id == 3 then
					a_fadein_move_fadeout(o:getCOObj(),0.25,1,ccp(c2.x + 18,c2.y + 60),0.5,0.25,0)
				else
					a_fadein_move_fadeout(o:getCOObj(),0.25,1,ccp(c2.x + 15,c2.y + 70),0.5,0.25,0)
				end
				
				local t = { x = -25,y = -30,sx = 28,sy = 28, res = 'jzd-h.png' }				
				RoomMatch.wineCup = createSprite(RoomMatch._Way, t )
				if desk_id == 1 then                                       --酒杯位置
					RoomMatch.wineCup:setPosition(c2.x - 30,c2.y - 10)				
				elseif 	desk_id == 2 then	
					RoomMatch.wineCup:setPosition(c2.x - 22,c2.y + 50)	
				elseif 	desk_id == 3 then
					RoomMatch.wineCup:setPosition(c2.x - 32,c2.y + 40)
				else
					RoomMatch.wineCup:setPosition(c2.x - 35,c2.y + 50)
				end
				
				if desk_id == 1 then                                        --酒杯偏移位置
					a_fadein_move_fadeout(RoomMatch.wineCup:getCOObj(),0.25,1,ccp(c2.x - 30,c2.y + 10),0.5,0,0,callBack)				
				elseif 	desk_id == 2 then	
					a_fadein_move_fadeout(RoomMatch.wineCup:getCOObj(),0.25,1,ccp(c2.x - 22,c2.y + 70),0.5,0,0,callBack)
				elseif 	desk_id == 3 then
					a_fadein_move_fadeout(RoomMatch.wineCup:getCOObj(),0.25,1,ccp(c2.x - 32,c2.y + 60),0.5,0,0,callBack)
				else
					a_fadein_move_fadeout(RoomMatch.wineCup:getCOObj(),0.25,1,ccp(c2.x - 35,c2.y + 70),0.5,0,0,callBack)
				end
				a_fadein( RoomMatch.wineCup:getSprite(), 0, 0.25 )	
			end
			
			playWav( "sound/drink1.wav" )
			RoomMatch.drunkenness_up_pic = createSprite( RoomMatch, RoomMatchConfig.drunkenness_up_pic, nil, 200 )
			if desk_id == 1 then
				RoomMatch.drunkenness_up_pic:setPosition( RoomMatchConfig["open"..desk_id].x - 48, RoomMatchConfig["open"..desk_id].y - 55 )					
			elseif 	desk_id == 2 then	
				RoomMatch.drunkenness_up_pic:setPosition( RoomMatchConfig["open"..desk_id].x + 37, RoomMatchConfig["open"..desk_id].y - 50 )	
			elseif 	desk_id == 3 then
				RoomMatch.drunkenness_up_pic:setPosition( RoomMatchConfig["open"..desk_id].x - 40, RoomMatchConfig["open"..desk_id].y - 51 )
			else
				RoomMatch.drunkenness_up_pic:setPosition( RoomMatchConfig["open"..desk_id].x - 52, RoomMatchConfig["open"..desk_id].y - 50 )
			end
			--RoomMatch.drunkenness_up_pic:setPosition( RoomMatchConfig["open1"].x - 48, RoomMatchConfig["open1"].y - 55 )
			--RoomMatch.drunkenness_up_pic:setPosition( RoomMatchConfig["open2"].x + 37, RoomMatchConfig["open2"].y - 50 )
			--RoomMatch.drunkenness_up_pic:setPosition( RoomMatchConfig["open3"].x - 40, RoomMatchConfig["open3"].y - 51 )
			--RoomMatch.drunkenness_up_pic:setPosition( RoomMatchConfig["open4"].x - 52, RoomMatchConfig["open4"].y - 50 )
			a_play( RoomMatch.drunkenness_up_pic:getSprite(), RoomMatchConfig.drunkenness_up_animation,false)
			upTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( upHandler, 1, false )
					
		end				
	end
	
	local function drunkHandler()
		--print( "GetChairIdx( data.loser ):" .. GetChairIdx( data.loser ) )	
		if RoomMatch[ "_desk" ][GetChairIdx( data.loser )] and RoomMatch[ "_desk" ][GetChairIdx( data.loser )].logoPanel then
			RoomMatch[ "_desk" ][GetChairIdx( data.loser )].logoPanel.drunk:setString( currentDrunkenness[ GetChairIdx(data.loser) ] .. "/" .. totalDrunkenness )
		end
		
		if isWatch == false and currentDrunkenness[ 1 ] >= totalDrunkenness then		--自己的醉酒度大于醉酒度上限
		--if isWatch == false then
			--print( RoomMatch[ "_desk" ][GetChairIdx(data.winner)]._name:getString() )
			ROOMPANEL = Import( "lua/room.lua" )
			ROOMPANEL.setPopWindow( true, RoomMatch[ "_desk" ][GetChairIdx(data.winner)]._name:getString() )
			ExitRoom()
			--Leave()			
		end			
	end
	
	local function updateWealth()
		if not isWatch then
	        --刷新积分
	        doRefreshWealth()			
	    end			
				
	end
			
	
	doUserOpen(GetChairIdx(data.open))

	doShowWay(lastShout.number) --开几个几
	
	local delay = RoomMatchConfig.delay.open	
	f_delay_do(RoomMatch,showCount, 'count', delay) -- 
		
	local player = MAX_CHAIR
	if matchData and matchData.player then 
		player = tonumber(matchData.player)
	end
	delay = delay + player*RoomMatchConfig.delay.count	
	f_delay_do(RoomMatch,showWinLose, 'showWinLose', delay)
	
	delay = delay + RoomMatchConfig.delay.win	
	f_delay_do(RoomMatch,showGold, 'showGold', delay)
	
	delay = delay + RoomMatchConfig.delay.gold	
	f_delay_do(RoomMatch,showWine, 'showWine', delay)
	
	f_delay_do(RoomMatch,updateWealth, 'updateWealth', delay)
	delay = delay + RoomMatchConfig.delay.drunkenness
	f_delay_do(RoomMatch,drunkHandler, 'drunkHandler', delay)
	delay = delay + RoomMatchConfig.delay.drunkHandler	
	f_delay_do(RoomMatch,start, 'start', delay)
	--有人开后，销毁时间读条
	if RoomMatch._UserTimer then
	    clear({RoomMatch._UserTimer})
	    RoomMatch._UserTimer = nil
	end
	--d(delay)
	--d(data.delay)
	--延时开始
	if 	data.delay then
		doGameReady(data.delay)
	end
end

--显示开动画
function doUserOpen(desk_id)
    if not RoomMatch._Way then
	    RoomMatch._Way = createNode(RoomMatch, point(0,0),nil,UserWinLoseZOrder)
	end
	
    if RoomMatch._desk[ desk_id ] then
        local c = RoomMatchConfig["open"..desk_id]
		local c2 = RoomMatchConfig["open"]
        c.res = c2.res
        --local c2 = c["desk"..getDeskID(desk_id)]
        local o = createSprite(RoomMatch._Way, c)
        --o:setPosition(c2.x + 80,c2.y + 100)
        a_fadeout_move(o:getSprite(),1,1,ccp(0, 20))
		doPlaySound( c2.sound, c2.ms )
	end		
    for i = 2,4 do
        if RoomMatch._desk[i] and RoomMatch._desk[i]._zhong then
            RoomMatch._desk[i]._zhong:setVisible(false)
        end
    end		
end

--显示几个几
function doShowWayCount(count)
    local c = RoomMatchConfig
    if RoomMatch._Way then
		if count > 0 then
			c.result_count.text = '×' .. count
		else
			c.result_count.text = '× 0'
		end
        if RoomMatch._Way.count then
            clear({RoomMatch._Way.count})
            RoomMatch._Way.count = nil
        end
		--创建一个中心的点，为了生成中心点的相对位置
		clear({createNode(RoomMatch,c.center)})
        RoomMatch._Way.count = createLabel( RoomMatch._Way, c.result_count )
	end
end

--游戏结束开几个几
function doShowWay(way_number)
    if not RoomMatch._Way then
	    return
	end
	local c = deepcopy(RoomMatchConfig["result_number"])	
	c.res = string.format(c.res,way_number)	
	local o = createSprite( RoomMatch._Way, c)	
	doShowWayCount(0)
end

--显示赢/输的积分动画
function doShowScore(desk_id,bWin,score,delay)
	local function fn()
		local c = RoomMatchConfig
		local c2 = c["gold"..(desk_id)]
		local o = nil 
		if bWin == true then 
			o = createLabelAtlas(RoomMatch._Way,c["action_win_score"])
			o:setString("+"..tostring(score))
		else
			o = createLabelAtlas(RoomMatch._Way,c["action_loss_score"])
			o:setString("-"..tostring(score))			
		end
		
		o:setPosition(c2.x,c2.y)
		a_fadein_move_fadeout(o:getCOObj(),1,1,ccp(c2.x,c2.y + 20),0.5,0.25,0)
	end
		
	f_delay_do(RoomMatch,fn,nil,delay/2)
end

--显示胜负标志
function doWinLoseMark(desk_id,type)
	
    if RoomMatch._Way and desk_id then 	
		--[[if type == 'lose' then
			
			showWine()
		end--]]
        local c = RoomMatchConfig["open"..desk_id]	
		c.res = RoomMatchConfig[type].res
		
		local o = createSprite(RoomMatch._Way, c)
		--local o = createSprite(RoomMatch, c)
		a_fadeout(o:getSprite(),nil,2)--2秒后消失		
    end
end

--显示赢取金币
function doWinAction(desk_id_from,desk_id_to,count,result)  --金币飞动相对调整
	
    if RoomMatch._Way and desk_id_from and desk_id_to then 
        local c = RoomMatchConfig
        local c2 = c["timeout"..desk_id_from]
        local c3 = c["timeout"..desk_id_to]
        for i = 1,count do 
	        local o = createSprite( RoomMatch._Way,c.add_coin )
	        o:setPosition(c2.x,c2.y)
	        a_emove_fadeIn(o:getCOObj(),1,c3,(count - i)*0.1)
	    end
		local function playwinsound()
			doPlaySound(c.add_coin.sound,0)
		end
		if 'win' == result then
			f_delay_do(RoomMatch,playwinsound,nil,1)	
		end		
	end
end

--获取财富信息
function doRefreshWealth()
    local function onUserMoney( data )
		if not RoomMatch then
			return
		end
        if not RoomMatch._gold then
            local c = RoomMatchConfig
            createSprite( RoomMatch, c.gold_pic )
            RoomMatch._gold = createLabel( RoomMatch, c.gold_text )
        end
		if data.data.gold then
			RoomMatch._gold:setString( data.data.gold )
			if tonumber(data.data.gold) < base_gold*4 then
				--CCMessageBox("你的金币不足入场要求，请充值",'')
				ROOM_PANEL = Import( "lua/room.lua" )
				ROOM_PANEL.setExitWindow(true)
				ExitRoom(true)				
			end
		end
	end
	if RoomMatch then
		--请求用户资料
		doCommand( 'user', 'money', {}, onUserMoney )
	end
end

--重置比赛
function doResetMatch()
    if RoomMatch then 
		if RoomMatch._Star then
            clear({RoomMatch._Star})
            RoomMatch._Star = nil
        end
		
        if RoomMatch._Way then
            clear( {RoomMatch._Way} )
            RoomMatch._Way = nil
        end
        		
		for i = 2,4 do
			if RoomMatch._desk[i] and RoomMatch._desk[i]._zhong then
				RoomMatch._desk[i]._zhong:setVisible(true)
			end	
		end			
    end			
    nSameWay = 0	
	hideDouble()		
end

--开骰情况
function createWay( parent, index, way ,zhai,op_number,delay)
	local step = 32
	local c = RoomMatchConfig
	
	--创建骰子
	-- x,y表示偏离值
	local function createNumber( x, y, num )
		return createSprite( RoomMatch._Way, res( x, y, "szs" .. num .. ".png" ) )
	end
	
	--print( "way is:" .. way )
	local num = {}
	for i = 1, 5 do
		num[i] = string.sub( way, i, i )
	end		
		
	--移动
	local ptMid = point(c.center.x,c.center.y)
	local t_delay = delay
	
	local function movetoCenter(o,value)
		local function onMovetoCenter(arg)
	        doShowStar(arg)
	        o:setVisible(false)
	    end
		local per_count_time = c.delay.count/5
		if tonumber(value) == op_number or (tonumber(value) == 1 and tonumber(zhai) <= 0) then
		    nSameWay = nSameWay + 1
			a_move(o:getCOObj(),t_delay,per_count_time,ccp(ptMid.x,ptMid.y),onMovetoCenter,nSameWay)
			t_delay = t_delay + per_count_time --每个骰子有0.3秒的移动时间
		end
	end
		
	local function getPos(i)
	    return RoomMatchConfig["way"..(index)]
	end
	
	local o1 = createNumber( getPos(index).x + 0, getPos(index).y +0, num[1] )
	movetoCenter(o1,num[1])
	local o2 = createNumber( getPos(index).x +step, getPos(index).y +0, num[2] )
	movetoCenter(o2,num[2])
	local o3 = createNumber( getPos(index).x +0, getPos(index).y-step, num[3] )
	movetoCenter(o3,num[3])
	local o4 = createNumber( getPos(index).x +step, getPos(index).y-step, num[4] )
	movetoCenter(o4,num[4])
	local o5 = createNumber( getPos(index).x +step * 2, getPos(index).y-step, num[5] )
	movetoCenter(o5,num[5])	
end

--显示火星动画
function doShowStar(count)
    if not RoomMatch._Star then 
        RoomMatch._Star = createNode(RoomMatch._Way, point(0,0))
    end
    local c = RoomMatchConfig
    local o = createSprite( RoomMatch._Star, res(0, 0, c["action_star"].res) )
	o:setPosition(c.center.x,c.center.y)
	local c = RoomMatchConfig
    a_play(o:getSprite(),c["action_star"],false)
	doPlaySound(c["action_star"].sound,0)
    doShowWayCount(count)
	--print("doShowStar")
end	

--显示游戏开始倒计时
function doCountDown( begin_sec,delay ) --从第几秒开始倒计时
	isPlaying = false
	--坐下动画
	local c = RoomMatchConfig
	local function onTimeOut()	
		clear({RoomMatch._countdown1})
		RoomMatch._countdown1 = nil
		clear({RoomMatch._countdown2})
		RoomMatch._countdown2 = nil
	end
	if RoomMatch._countdown1 then
	    return
	end
	play_countdown(RoomMatch:getCOObj(),c["action_countdown"],begin_sec,delay,onTimeOut)
    local function showText()
		matching = true --设置为游戏中，防止玩家退出
        RoomMatch._countdown1:setVisible(true)
		RoomMatch._countdown2:setVisible(true)
		doPlaySound( c["action_countdown"].sound, c["action_countdown"].ms )
		
    end
	RoomMatch._countdown1 = createLabel( RoomMatch, c.timeout_text1 )
	RoomMatch._countdown2 = createLabel( RoomMatch, c.timeout_text2 )
	--RoomMatch._countdown:setString("秒后游戏开始")
	RoomMatch._countdown1:setVisible(false)
	RoomMatch._countdown2:setVisible(false)
	f_delay_do(RoomMatch,showText,nil,delay)
	
end

--游戏结束，准备下一轮游戏
function doGameReady(nextBiginTime)
	local delay = nextBiginTime - tBeginTimer		
	local function startCountdown()	
		timeout_start = false
		if isLeave == true then
			ExitRoom()
		else
			doResetMatch()
			--主动退出时直接退出		
			if nPlayer > 1 then		
				--hideWaittips()				
				doCountDown(tBeginTimer,0)		
			else
				ShowReady()
			end
		end
	end
	if timeout_start == false then
		timeout_start = true
		f_delay_do(RoomMatch,startCountdown,nil,delay)
	end
end

function preLoadSound()
	local c = RoomMatchConfig
	local sound = c.win.sound
	preLoadWav( sound )	
	sound = c.lose.sound
	preLoadWav( sound )
	sound = c.open.sound
	preLoadWav( sound )
	sound = c.rock.sound
	preLoadWav( sound )
	sound = c.action_countdown.sound
	preLoadWav( sound )	
	sound = c.shout.sound
	preLoadWav( sound )
	sound = c.action_star.sound
	preLoadWav( sound )	
	sound = c.add_coin.sound
	preLoadWav( sound )
	preLoadWav( "sound/drink1.wav" )
	preLoadWav( "sound/udrink.mp3" )
end

function doPlaySound(file,ms)
	local effectID = playWav(file,ms)	
	local bFind = false
	for i=1, table.maxn(sound_list) do  	 
		if sound_list[k] == effectID then
			bFind = true
			break
		end
	end
	if bFind == false then
		table.insert(sound_list,effectID)
	end
end

--初始化成员变量
function Initialize()
	sitState = false
	readyGame = false
    isWatch = true	
    isLeave = false
	isPlaying = false
    nSameWay = 0
    nPlayer = 0	
	sitdown = false
	ready = nil
	timeout_start = false
	isAlive = true
	isEnterbackground = false	
	isMyTimeBegin = false
	--	isAct = false --托管
end



--判断旁观
function IsWatch()
    return isWatch
end

--获取房间ID
function GetRoomID()
	return RoomID
end	

--收到普通消息
function OnChat(data)
	showMsg(RoomMatch,GetChairIdx(data.chairid),data.message)
end

--收到表情消息
function OnFace(data)
	showFace(RoomMatch,GetChairIdx(data.chairid),tonumber(data.message))
end

function OnUserMessage(data)
	if data.type == 0 then --0为聊天消息
		OnChat(data)
	elseif data.type == 1 then --1为表情
		OnFace(data)
	end		
end

--奖励
function OnUserAward(data)
	showLuckAction(RoomMatch,data.gold)
end

--退出房间
function ExitRoom(toMail)
	HideLookOnTips()
	tcp_leave()
	closePanel()	
	back_func(toMial)	
	--[[
	if PANEL_CONTAINER.closeChildPanel( nil, 10 ) then
		local ROOM_PANEL = Import("lua/room.lua")			
		PANEL_CONTAINER.addChild( ROOM_PANEL.showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
	end	
	]]
end

--显示等待动画
function ShowReady()
	HideReady()
	local c = RoomMatchConfig
	ready = createNode(RoomMatch,c.ready_action)
	local o = createSprite( ready, res(0, 0, c["ready_action"].res) )
	ready:setPosition(c.center.x + 15,c.center.y)	
    a_play(o:getSprite(),c["ready_action"],true)	
	
end

function HideReady()
	if ready then
		clear({ready})
		ready = nil
	end
end

function HeartBeat()
	if isWatch == true or isPlaying == false then
		f_delay_do(RoomMatch,HeartBeat,nil,heartbeat)
		--d('-----------1,send heartbeat---------------')
		return
	else 
		if isAlive == true then
			isAlive = false
			tcp_heartbeat()
			f_delay_do(RoomMatch,HeartBeat,nil,heartbeat)
			--d('-----------2,send heartbeat---------------')			
		else
			CCMessageBox('网络异常，请重新进入','')
			ExitRoom()
		end	
	end	
end

--托管叫号
function Agent(isOpen)
	if IsAgent() == true then	
		if isOpen then
			doOpen()
		else
			doShout()
		end
	end
end

function EnterBackground()
	Leave()
end

function EnterForgeground()
	--isLeave = false
end

function ShowLookOnTips(msg)
	HideLookOnTips()
	local c = RoomMatchConfig
	if RoomMatch then
		RoomMatch.LookOnTips = createLabel(RoomMatch,c.lookontips)
		if msg then
			RoomMatch.LookOnTips:setString(tostring(msg))
		end
	end
end

function HideLookOnTips()
	if RoomMatch and RoomMatch.LookOnTips then
		clear({RoomMatch.LookOnTips})
		RoomMatch.LookOnTips = nil
	end
end

function onUserUseProps(data)
	local user_info = LOGIN_PANEL.getUserData()
	if data.result == 1 then		--	成功
		local deskId = drunkDeskId[ data.uid ]
		currentDrunkenness[ deskId ] = 0
		RoomMatch[ "_desk" ][deskId].logoPanel.drunk:setString( currentDrunkenness[ deskId ] .. "/" .. totalDrunkenness )	
		if tonumber(user_info.id) == data.uid then
			showMessage( RoomMatch, "解酒成功", {ms = 3000} )			
			local ROOMCTRL = Import( "lua/room_control.lua" )
			ROOMCTRL.updateDrunkGoodNum( RoomMatch )
			--print( currentDrunkenness[ deskId ] / totalDrunkenness )
			if currentDrunkenness[ deskId ] < 3 then
				if RoomMatch.drunkenness_btn_state_pic then
					clear( {RoomMatch.drunkenness_btn_state_pic} )
					RoomMatch.drunkenness_btn_state_pic = nil
				end
			end
		end
		playWav( "sound/udrink.mp3",3000 )
		RoomMatch.drunkenness_flash_pic = createSprite( RoomMatch, RoomMatchConfig.drunkenness_flash_pic, nil, 200 )
		if deskId == 1 then
			RoomMatch.drunkenness_flash_pic:setPosition( RoomMatchConfig["open"..deskId].x - 48, RoomMatchConfig["open"..deskId].y - 55 )					
		elseif 	deskId == 2 then	
			RoomMatch.drunkenness_flash_pic:setPosition( RoomMatchConfig["open"..deskId].x + 37, RoomMatchConfig["open"..deskId].y - 50 )	
		elseif 	deskId == 3 then
			RoomMatch.drunkenness_flash_pic:setPosition( RoomMatchConfig["open"..deskId].x - 40, RoomMatchConfig["open"..deskId].y - 51 )
		else
			RoomMatch.drunkenness_flash_pic:setPosition( RoomMatchConfig["open"..deskId].x - 52, RoomMatchConfig["open"..deskId].y - 50 )
		end
		
		a_play( RoomMatch.drunkenness_flash_pic:getSprite(), RoomMatchConfig.drunkenness_flash_animation,false)
		RoomMatch.wake_up_drunk_good_pic = createSprite( RoomMatch, RoomMatchConfig.wake_up_drunk_good_pic, nil, 200 )
		if deskId == 1 then
			RoomMatch.wake_up_drunk_good_pic:setPosition( RoomMatchConfig["open"..deskId].x - 48, RoomMatchConfig["open"..deskId].y - 55 )					
		elseif 	deskId == 2 then	
			RoomMatch.wake_up_drunk_good_pic:setPosition( RoomMatchConfig["open"..deskId].x + 37, RoomMatchConfig["open"..deskId].y - 50 )	
		elseif 	deskId == 3 then
			RoomMatch.wake_up_drunk_good_pic:setPosition( RoomMatchConfig["open"..deskId].x - 40, RoomMatchConfig["open"..deskId].y - 51 )
		else
			RoomMatch.wake_up_drunk_good_pic:setPosition( RoomMatchConfig["open"..deskId].x - 52, RoomMatchConfig["open"..deskId].y - 50 )
		end
		local function wakeUpHandler()
			if wakeUpTimer then
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(wakeUpTimer)
				wakeUpTimer = nil
			end
			if not RoomMatch then
				return
			end
			a_fadeout( RoomMatch.wake_up_drunk_good_pic:getSprite(), 0, 1)
		end
		wakeUpTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( wakeUpHandler, 2.5, false )	
				
		RoomMatch.scrollTxt = createTopScrollPanel( RoomMatch, 60, 270, 	RoomMatch[ "_desk" ][deskId]._name:getString() .. "使用了小瓶解酒药" )
	else
		if tonumber(user_info.id) == data.uid then
			showMessage( RoomMatch, base64Decode(data.memo), {ms = 3000} )
		end	
	end
	--[[if _root.AgentBtn then
		_root.AgentBtn:setTouchEnabled( false )
	end--]]
end