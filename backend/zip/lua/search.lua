-- search player page

SERACH_LAYOUT = Import("layout/search.lua")
local SearchConfig = SERACH_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local searchPage = nil

local FRIEND_PAGE = Import("lua/friend.lua")

--[[
gain
id
age
is_real
online
name
sex
vip_id
logo
mood
city
--]]

local click_delta = 10
local CHALLENGE_PAGE = Import("lua/challenge.lua")
local PLAYER_INFO_PANEL = Import("lua/player_info.lua")

local function back_func()
	PLAYER_INFO_PANEL.closePlayerInfoPagePanel()
	showSearchPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

function createSmallItemByUserInfo(info)
	local c = SearchConfig
	local tr = LIGHT_UI.clsTr:New( c )
	local nodeObj = tr._RootNode
	nodeObj._user_id = info.id

	local function on_click(msg_obj, x, y)	
		closeSearchPagePanel()
		PLAYER_INFO_PANEL.showByUserId(nodeObj._user_id)
		PLAYER_INFO_PANEL.init( { back_func = back_func })
	end
	
	tr:setMsgDelegate( searchPage._move_small_page._move_small_grp, on_click )
	
	tr:addSprite( 'logo', info.logo )
	tr:addLabel( 'name', info.name )
	
	if info.online == 1 then
		tr:addSprite( 'online' )
	end
	
	local sex_res = info.sex == '0' and 'man.png' or 'woman.png'	
	tr:addSprite( 'sex', sex_res )
	
	tr:addLabel( 'address', info.city )

	if info.is_real == '1' then		
		tr:addSprite( 'real' )
	end

	if info.vip_id ~= '0' then
		local vip_logo = string.format( 'vip%i.png', info.vip_id )
		
		tr:addSprite( 'vip', vip_logo )
	end

	if string.len( info.mood ) < 1 then info.mood = ' ' end
	tr:addMultiLabel( 'mood', info.mood )
	
	tr:setHeight( 'mood', c.tr.split )
	
	return nodeObj
end

local function on_get_search_data(data)
	local real_data = data.data
	searchPage._move_small_page._player_page:clearAllItem()
	for _, player_info in pairs(real_data) do
		searchPage._move_small_page._player_page:append_item( createSmallItemByUserInfo(player_info))
	end

	searchPage._move_small_page._player_page:refresh_view()
end

local small_desc_info = {
	item_width = 480,
	item_height = 1,
	column_cnt = 1,
	x_space = 0,
	y_space = 0,
}

local function createMoveSmallPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = SearchConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_small_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config["view_height"])
	ret_panel._move_small_grp:setVMovable(true)

	local player_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	player_page._use_dynamic_size = true
	player_page:refresh_view()

	player_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(player_page)
	ret_panel._move_small_grp:selectPage(1)

	ret_panel._player_page = player_page

	local function get_y_limit(obj)
		return config["inner_y"] 
	end

	player_page.getYLimit = get_y_limit
	
	return ret_panel
end

local back_func = nil

function init( data )
	if data.back_func then
		back_func = data.back_func
	end
end	

local function createSearchPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = SearchConfig, nil
	local o = nil

	local function on_back(btn)
		closeSearchPagePanel()
		if back_func then
			back_func()
		else
		    --把原来跳回偶遇主页给弄到跑回主面板了
			--CHALLENGE_PANEL.showChallengePagePanel(HTTP_CLIENT.getRootNode(), 0, 0) 		
			--MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
			if PANEL_CONTAINER.closeChildPanel( nil, 15 ) then
				local SETTING = Import("lua/setting.lua")							
				PANEL_CONTAINER.addChild( SETTING.showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
			end	
		end
		end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '查找好友' )

	create9Sprite( ret_panel, c.search_rect )
	
	ret_panel._playername_input = LIGHT_UI.clsEdit:New(ret_panel, 24, 714, GLOBAL_FONT, 22, 200, 28)    --输入用户名调整
	ret_panel._playername_input:setTextColor(102, 102, 102)
	ret_panel._playername_input:setAnchorPoint(0, 0.5)
	ret_panel._playername_input:active(true)

	local function on_search(btn, x, y)
		local search_name = ret_panel._playername_input:getText()
		doCommand( 'user', 'search', { q = search_name }, on_get_search_data,nil,{loading = 0} )
	end
	
	ret_panel._reply_btn = create9Button( ret_panel, c.search_btn, on_search )

	config = SearchConfig["move_page_config"]
	ret_panel._move_small_page = createMoveSmallPage(ret_panel, config["x"], config["y"]) -- 小图标列表 
	
	return ret_panel
end

function showSearchPagePanel(parent, x, y)
	if not searchPage then
		searchPage = createSearchPagePanel(parent, x, y)
	else
		searchPage:setVisible(true)
	end
	searchPage._playername_input:active(true)
end

function closeSearchPagePanel()
	--searchPage:setVisible(false)
	searchPage._playername_input:active(false)
	clear({searchPage})
	searchPage = nil
end

