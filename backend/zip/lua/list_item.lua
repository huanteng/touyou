g_ret_panel = {}
local FRIEND_PAGE = Import("lua/friend.lua")
local CHAT_PAGE = Import("lua/chat.lua")
local NEWS_PAGE = Import("lua/moving.lua")
local NEWEST_UPLOAD = Import("lua/photo.lua")
local MEET_BYCHANCE = Import("lua/challenge.lua")
local MALL_PANEL = Import("lua/mall.lua")
local TASK_PAGE = Import("lua/task.lua")
local MSG_PAGE = Import("lua/message.lua")
local checkRunning = nil

LISTITEM_LAYOUT = Import("layout/list_item.lua")
local listItemConfig = LISTITEM_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local selectTarget = nil


local function getClickTarget( t, btnArr )
	for i = 1, #btnArr do
		if t._btn_res_list[ 1 ] ==  btnArr[ i ]._btn_res_list[ 1 ] then
		--if not btnArr[ i ]._btn_res_list[ 1 ] then
			return btnArr[ i ]
		end
	end
	return nil
end

local function slideHandler( id )
	local function onCheckRunning()
		if PANEL_CONTAINER.getRunning() == false then
			if checkRunning then
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(checkRunning)
				checkRunning = nil
			end	
			if id == 1 then
				dailyCount('Friends')--圈子
				PANEL_CONTAINER.addChild( FRIEND_PAGE.showFriendPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			elseif id == 2 then
				dailyCount('SayWords')--说两句
				PANEL_CONTAINER.addChild( CHAT_PAGE.showChatPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			elseif id == 3 then
				PANEL_CONTAINER.addChild( NEWS_PAGE.showNewsPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			elseif id == 4 then
				PANEL_CONTAINER.addChild( NEWEST_UPLOAD.showPhotoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			elseif id == 5 then
			    dailyCount('ComeAcross') -- 偶遇
				PANEL_CONTAINER.addChild( MEET_BYCHANCE.showChallengePagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			elseif id == 6 then
				dailyCount('PropStore')--道具商城
				PANEL_CONTAINER.addChild( MALL_PANEL.showMallPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			elseif id == 7 then
				dailyCount('GetGold')--获得金币
				PANEL_CONTAINER.addChild( TASK_PAGE.showTaskPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			elseif id == 8 then
				PANEL_CONTAINER.addChild( LUCKY_ROT_PANEL.showLuckyPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0)  )
			elseif id == 9 then
				PANEL_CONTAINER.addChild( MAIN_PANEL.createMainPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			elseif id == 13 then
				PANEL_CONTAINER.addChild( MSG_PAGE.showMsgPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			end								
		end
	end
	PANEL_CONTAINER.clickButton()
	checkRunning = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( onCheckRunning, 0.1, false )
	
end

local function addChildPanel( t )
	if t._btn_res_list[ 1 ] ==  'goodfriend_clickrect.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 1) then
			slideHandler( 1 )
		else	
			PANEL_CONTAINER.clickButton()
		end			
	elseif t._btn_res_list[ 1 ] ==  'chat_clickrect.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 2 ) then
			slideHandler( 2 )
		else	
			PANEL_CONTAINER.clickButton()
		end
	elseif t._btn_res_list[ 1 ] ==  'dynamic_clickrect.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 3 ) then
			slideHandler( 3 )
		else	
			PANEL_CONTAINER.clickButton()
		end
	elseif t._btn_res_list[ 1 ] ==  'discuss_clickrect.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 4 ) then
			slideHandler( 4 )	
		else	
			PANEL_CONTAINER.clickButton()
		end						
	elseif t._btn_res_list[ 1 ] ==  'meetbychance_clickrect.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 5 ) then
			slideHandler( 5 )	
		else	
			PANEL_CONTAINER.clickButton()
		end
	elseif t._btn_res_list[ 1 ] ==  'goodmall_clickrect.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 6 ) then
			slideHandler( 6 )	
		else	
			PANEL_CONTAINER.clickButton()
		end
	elseif t._btn_res_list[ 1 ] ==  'getgold_clickrect.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 7 ) then
			slideHandler( 7 )	
		else	
			PANEL_CONTAINER.clickButton()
		end
	elseif t._btn_res_list[ 1 ] ==  'luckyplate_clickrect.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 8 ) then
			slideHandler( 8 )	
		else	
			PANEL_CONTAINER.clickButton()
		end		
	elseif t._btn_res_list[ 1 ] ==  'gamehall_clickrect.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 9 ) then
			slideHandler( 9 )	
		else	
			PANEL_CONTAINER.clickButton()
		end	
	elseif t._btn_res_list[ 1 ] ==  'message_clickrect.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 13 ) then
			slideHandler( 13 )	
		else	
			PANEL_CONTAINER.clickButton()
		end	
	end		
end		

local function createListItem( parent, x, y, titleName, btnArr, rectArr, index )
	local ret_panel = createNode( parent, point( x, y ) )
	ret_panel.bg = {}
	ret_panel.btn = {}	
	
	local function onClick( t, lx, ly )	
		local sign = PANEL_CONTAINER.getCanSwitch()
		if sign == true then
			PANEL_CONTAINER.setCanSwitch( false )
		else
			return
		end
		local clickTarget = getClickTarget( t, g_ret_panel[ index ].btn )
		if clickTarget then
			if selectTarget then
				selectTarget:setNormalStatus()
			end
			clickTarget:setDownStatus()
			selectTarget = clickTarget
			--NEW_GAME_HALL.ret_panel.select:setPosition(  clickTarget.x + listItemConfig.click_target_offset_x.x, clickTarget.y + y )			
			addChildPanel( t )					
		end
	end
	
	
	
	--ret_panel.title_txt:setTextColor( 255, 255, 255 )
	
	for i = 1, #rectArr do 	
		btnArr[ i ][ "y" ] = listItemConfig.btn_bg.offset_y - ( i - 1 ) * listItemConfig.btn_bg.space_y
		btnArr[ i ][ "x" ] = listItemConfig.btn_bg.offset_x
		rectArr[ i ][ "y" ] = listItemConfig.btn_rect.offset_y - ( i - 1 ) * listItemConfig.btn_rect.space_y
		rectArr[ i ][ "x" ] = listItemConfig.btn_rect.offset_x		
		rectArr[ i ][ "sy" ] = listItemConfig.btn_rect.scale_y
		rectArr[ i ][ "sx" ] = listItemConfig.btn_rect.scale_x				
		ret_panel.btn[i] = createCustomStatusButton( ret_panel, rectArr[ i ], onClick )
		if titleName == "" then
			if i == 1 then
				ret_panel.btn[i]:setDownStatus()
				selectTarget = ret_panel.btn[i]
			end
		end
		ret_panel.bg[ i ] = createSprite( ret_panel, btnArr[ i ] )
	end
	--[[ret_panel.btn2 = LIGHT_UI.clsStatusButton:New( ret_panel, -10, 730, "send_say.png", "send_say.png" )
	ret_panel.btn2.onTouchBegan = onClick--]]
	--createButton( ret_panel, btnArr[ 1 ], onClick )
	
	if titleName ~= "" then
		listItemConfig.list_title_text[ "text" ] = titleName
		listItemConfig.list_title_text[ "css" ] = 'd5'		
		ret_panel.title_txt = createLabel( ret_panel, listItemConfig.list_title_text )
		--ret_panel._gold:setTextColor( 197, 118, 28 )
		ret_panel.h_line_up = createSprite( ret_panel, listItemConfig.h_line_up )
		ret_panel.h_line_down = createSprite( ret_panel, listItemConfig.h_line_down )
	end
	return ret_panel
end

function showListItem( parent, x, y, titleName, btnArr, rectArr, index )
	g_ret_panel[ index ]= createListItem( parent, x, y, titleName, btnArr, rectArr, index )
end	