-- reward list page

local rewardListPage = nil

REWARD_LAYOUT = Import("layout/reward_list.lua")
local RewardConfig = REWARD_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local rewardData = {} 
local cur_reward_page = 0
local max_reward_page = 2
--[[
uid
description
id
name
time
type
--]]

local reward_desc_info = {
	item_width = 480,
	item_height = 32,
	column_cnt = 1,
	x_space = 150,
	y_space = 5,
}

local function createItemByData( info )
	local tr = LIGHT_UI.clsTr:New( RewardConfig )
	local nodeObj = tr._RootNode

	tr:addLabel( 'time', info.time )
	tr:addLabel( 'name', info.name )
	tr:addLabel( 'desc', info.description )
	
--	tr:setHeight( 'time', 23 )

	return nodeObj
end

local function on_get_reward_list(data)
	for _, reward_info in pairs(data.data) do
		table.insert(rewardData, reward_info)
		local item = createItemByData(reward_info)
		rewardListPage._move_big_page._reward_list:append_item(item)
	end
	rewardListPage._move_big_page._reward_list:refresh_view()
end

local function request_reward_list(page)
	doCommand( 'lucky', 'lists', { size = 30, page = page }, on_get_reward_list, 5, { loading = 0 } )
end

local function createRewardList(parent, x, y)
	local config = RewardConfig.move_page_config
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config.view_height)
	ret_panel._move_grp:setVMovable(true)

	local reward_list = LIGHT_UI.clsRowLayout:New(nil, 0, 0, reward_desc_info)
	reward_list:refresh_view()

	local function onRewardOverTop(layout)
		rewardData = {}
		reward_list:clearAllItem()
		cur_reward_page = 1
		request_reward_list(cur_reward_page)
	end
	local function onRewardOverBottom(layout)
		if cur_reward_page < max_reward_page then
			cur_reward_page = cur_reward_page + 1
			request_reward_list(cur_reward_page)
		end
	end

	reward_list.onHMoveOverTop = onRewardOverTop
	reward_list.onHMoveOverBottom = onRewardOverBottom

	reward_list:setPosition(0, config.inner_y)
	ret_panel._move_grp:appendItem(reward_list)
	ret_panel._move_grp:selectPage(1)

	local function get_y_limit(obj)
		return config.inner_y 
	end

	reward_list.getYLimit = get_y_limit

	ret_panel._reward_list = reward_list
	
	return ret_panel
end

local function createBlankPage(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = RewardConfig, nil
	local o = nil

	local function on_back(btn)
		closeRewardListPagePanel()
		--LUCKY_ROT_PANEL.showLuckyPagePanel(parent,x,y)
		if PANEL_CONTAINER.closeChildPanel( nil, 8 ) then
			PANEL_CONTAINER.addChild( LUCKY_ROT_PANEL.showLuckyPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
		end	
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '中奖名单' )

	config = RewardConfig.move_page_config
	ret_panel._move_big_page = createRewardList(ret_panel, config.x, config.y)

	c2 = c.title_config
	local align_tbl = {}
	local time_title = createLabel( ret_panel, { x = 0, y = c2.y + 20, text = '时间' , css = 'c2'} )
	local name_title = createLabel( ret_panel, { x = 1, y = c2.y + 20, text = '名字' , css = 'c2'} )
	local reward_title = createLabel( ret_panel, { x = 10, y = c2.y + 20, text = '奖品' , css = 'c2'} )
	
	table.insert(align_tbl, time_title)
	table.insert(align_tbl, name_title)
	table.insert(align_tbl, reward_title)

	LIGHT_UI.alignXCenterForObjList(align_tbl, c2.gap )
	
	return ret_panel
end

local function blankPage()
	local config = RewardConfig["move_page_config"]
	rewardListPage._move_big_page._reward_list:clearAllItem()
	rewardListPage._move_big_page._reward_list:setPosition(0, config.inner_y)
end

function showRewardListPagePanel(parent, x, y)
	if not rewardListPage then
		rewardListPage = createBlankPage(parent, x, y)
	else
		rewardListPage:setVisible(true)
	end

	blankPage()

	cur_reward_page = 1
	request_reward_list(cur_reward_page)
end

function closeRewardListPagePanel()
	--rewardListPage:setVisible(false)
	clear({rewardListPage})
	rewardListPage = nil
end

