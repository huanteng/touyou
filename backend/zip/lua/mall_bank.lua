BANK_LAYOUT = Import("layout/mall_bank.lua")
local BankConfig = BANK_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local bankPage = nil
--local ITEM_DETAIL = Import("lua/item_detail.lua")
local MALL_PAGE = Import("lua/mall.lua")
local g_order = nil
local target_uid = 0
local _canPayByGold = nil


--支付结果
local function onPayFinish(data)
	if data == "ok" then
		closeBankPanel()
		if target_uid == GLOBAL.uid then
			USER_PACKAGE_PANEL.showPackageInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		else
			local function back_func()
				closeBankPanel()
				local FRIEND_PAGE = Import("lua/friend.lua")
				FRIEND_PAGE.showFriendPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
			end
			PLAYER_INFO_PANEL.showByUserId( target_uid )
			PLAYER_INFO_PANEL.init( { back_func = back_func })
		end
	else
		showMessage( bankPage, data )
	end
end

local function onDelayPayFinish(data)
	f_delay_do(bankPage,onPayFinish,data,0.1)
end

--[[
sid
用户id
道具id
数量
道具名称
金额
]]

--支付宝
local function doAlipay()
	dailyCount('ApplayForZhifubao')--支付宝
	local function cb(data)	
		--if g_order then
			local order_number = data.no
			local order_name = g_order.name
			local order_price = g_order.price	
			--onPayFinish( "ok" )			
			xymodule.aliPay("buy", order_name,order_price,order_number, onDelayPayFinish)
		--end
	end		
	doCommand( 'props', 'payment_no', 
	{ props = g_order.props, target_uid = g_order.target_uid, 
	count = g_order.count, types = 0 }, cb )
end

--盛付通
local function doShengpay()
	dailyCount('ApplayForChengfu')--盛付通
	local function cb(data)
		--if g_orde then 
			local order_number = data.no
			local order_name = g_order.name
			local order_price = g_order.price		
			xymodule.shengPay("buy",order_name,order_price,order_number, onDelayPayFinish)
		--end
	end
	doCommand( 'props', 'payment_no', 
	{ props = g_order.props, target_uid = g_order.target_uid, 
	count = g_order.count, types = 2 }, cb )
end	

--支付
local doPay = {
	[1] = doAlipay,
	[2] = doShengpay,	
}	

--创建支付按钮
local function createPayBtn(parent)
	local c = BankConfig
	local function createBtn(tPay)	
		local function onClick()
			if doPay[tPay] then
				doPay[tPay]()
			end
		end
		if _canPayByGold then
			local t = { id = c["pay"..tPay].id, x = c["pay"..tPay].x, y = c["pay"..tPay].y - c.contentMoveDown.distance, ax = c["pay"..tPay].ax, ay = c["pay"..tPay].ay, res = c["pay"..tPay].res }
			local btn = createButton(parent,t,onClick)
		else
			local btn = createButton(parent,c["pay"..tPay],onClick)
		end
	end
	
	if c.pay_info and c.pay_info.count then
		for i = 1,tonumber(c.pay_info.count) do
			createBtn(i)
		end
	end 
	
end

--创建页面
function createBankPanel(parent, x, y, order, canPayByGold)
	_canPayByGold = canPayByGold
	if not bankPage then 	
		bankPage = createBasePanel( parent, x, y )

		local c ,c2 = BankConfig,nil
		local o = nil

		--保存订单信息
		g_order = order 
		target_uid = order.target_uid
		
		local function on_back(btn)
			closeBankPanel()			
			--MALL_PAGE.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)			
			if PANEL_CONTAINER.closeChildPanel( nil, 6 ) then
				PANEL_CONTAINER.addChild( MALL_PAGE.showMallPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			end	
		end

		bankPage._onBack = on_back
		bankPage._head_text:setString('支付页面')
			
		--bankPage._move_big_page = createMoveBigPage(bankPage, config["x"], config["y"])

		createPayBtn(bankPage)
		c2 = c["logo"]
		c2.res = order.logo
		createSprite(bankPage, c2, nil)
		
		o = createLabel(bankPage,c["name"])
		o:setString("产品名称: "..order.name)
		
		
		o = createLabel(bankPage,c["count"])
		o:setString("产品数量: "..order.count)
		
		bankPage.needPayGold = createLabel(bankPage,c["money"])
		--o:setString("需要支付: "..order.price.."RMB")
		
		
				
		if canPayByGold then
			local t = { x = c["pay_text"].x, y = c["pay_text"].y - c.contentMoveDown.distance, ax = c["pay_text"].ax, ay = c["pay_text"].ay, css = c["pay_text"].css, text = c["pay_text"].text2 }
			o = createLabel(bankPage,t)			
			t = { to = c["pay_tips"].to, x = c["pay_tips"].x, y = c["pay_tips"].y - c.contentMoveDown.distance + 110, ax = c["pay_tips"].ax, ay = c["pay_tips"].ay, css = c["pay_tips"].css, text = c["pay_tips"].text }
			o = createLabel(bankPage,t)
			createSprite(bankPage, c[ "gold_dicretion_bg" ])
			bankPage.canPayGold = createLabel(bankPage,c["can_pay_gold_text"])	
			local function onMsg( data )
				local function on_other_pay_cb(data)
					showMessage( bankPage, data.memo )
				end
				local function onClick()
					doCommand( 'props', 'buy', 
					{ target_uid = target_uid, count = order.count, props_id = order.props }, on_other_pay_cb )
				end
				
				bankPage.canPayGold:setString( bankPage.canPayGold:getString() .. data.data.bullion )
				local discount
				local user_info = LOGIN_PANEL.getUserData()
				if user_info["vip_id"] == "1" then
					discount = 0.9
				elseif user_info["vip_id"] == "2" then
					discount = 0.85
				elseif user_info["vip_id"] == "3" then
					discount = 0.8
				else
					discount = 1
				end
				local needGold = order.price * 100 * discount
				bankPage.needPayGold:setString( "需要支付: " .. needGold .. "元宝(" ..order.price .."RMB)" )
				if tonumber( data.data.bullion ) >= needGold then
					create9Button( bankPage, c["confirm_pay"], onClick )
				else
					createLabel(bankPage,c["gold_pay_dicretion_text"])
				end
			end
			doCommand( 'user', 'money', {}, onMsg )
		else			
			bankPage.needPayGold:setString( "需要支付: " .. order.price .."RMB" )
			o = createLabel(bankPage,c["pay_text"])			
		
			o = createLabel(bankPage,c["pay_tips"])
		end
		
		return bankPage
	end
end

--返回
function closeBankPanel()
	if bankPage then 
		clear( { bankPage } )
		bankPage = nil
	end
end