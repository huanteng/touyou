-- friend

FRIEND_LAYOUT = Import("layout/friend.lua")
local FriendPageConfig = FRIEND_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local friendPage = nil

local focus_list = {}
--[[
gain
id
age
is_real
online
name
sex
vip_id
logo
mood
city
--]]
local cur_focus_page = 0
local max_focus_page = 5

local fans_list = {}
local cur_fans_page = 0
local max_fans_page = 5
local total_width = 480
local click_delta = 10

local freshing = true

--local CHALLENGE_PAGE = Import("lua/challenge.lua")

local function back_func()
	PLAYER_INFO_PANEL.closePlayerInfoPagePanel()
	if PANEL_CONTAINER.closeChildPanel( nil, 1 ) then
		PANEL_CONTAINER.addChild( showFriendPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
	end	
	--showFriendPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

function createSmallItemByUserInfo(info)
	local tr = LIGHT_UI.clsTr:New( FriendPageConfig )
	local nodeObj = tr._RootNode
	nodeObj._user_id = info.id

	local function on_click(msg_obj, x, y)
		GAMEHALL_PANEL.closeGameHallPanel()
		closeFriendPagePanel()
		PLAYER_INFO_PANEL.showByUserId(nodeObj._user_id)
		PLAYER_INFO_PANEL.init( { back_func = back_func })
	end
	
	tr:setMsgDelegate( friendPage._move_small_page._move_small_grp, on_click )
	
	tr:addSprite( 'logo', info.logo )
	tr:addLabel( 'name', info.name )
	
	if info.online == 1 then
		tr:addSprite( 'online' )
	end
	
	local sex_res = info.sex == '0' and 'man.png' or 'woman.png'	
	tr:addSprite( 'sex', sex_res )
	
	tr:addLabel( 'address', info.city )

	if info.is_real == '1' then		
		tr:addSprite( 'real' )
	end

	if info.vip_id ~= '0' then
		local vip_logo = string.format( 'vip%i.png', info.vip_id )
		
		tr:addSprite( 'vip', vip_logo )
	end

	if string.len( info.mood ) < 1 then info.mood = ' ' end
	local modStr = getCharString( info.mood, 16 )	
	modStr = modStr .. "..."
	tr:addMultiLabel( 'mood', modStr )
	
	tr:setHeight( 'mood', FriendPageConfig.tr.split )
	
	return nodeObj
end

local function go_find_friend()
	closeFriendPagePanel()
	CHALLENGE_PANEL.showChallengePagePanel( HTTP_CLIENT.getRootNode(), 0, 0, true)
end

local function create_empty_tips()
	local ret_panel = LIGHT_UI.clsNode:New( nil, 0, 0 )
	createMultiLabel( ret_panel, FriendPageConfig.empty_tips )
	create9Button( ret_panel, FriendPageConfig.friend_btn, go_find_friend )
	return ret_panel
end

local function clearItemById( page, id, cache )
	local itemList = page:getItemList()
	for k,v in ipairs( itemList ) do
		if v.id == id then
			if not cache then
				page:clearItem( k, item )
			end				
		end
	end 
end

local itemCount = 1
local cacheArr = {}
local function on_get_focus_list(data, cache)
	focus_list = {}
	if not friendPage then
		return
	end
	if freshing and freshing == true then
		friendPage._move_small_page._focus_page:clearAllItem()
		resetFListPosition()
	end
	
	for k, v in pairs(cacheArr) do
		clearItemById( friendPage._move_small_page._focus_page, v, cache )
	end
	
	local real_data = data.data.data
	if not real_data then
		return
	end
	itemCount = 1
	cacheArr = {}
	for _, user_info in ipairs(real_data) do
		local ret_panel = createSmallItemByUserInfo(user_info)
		if freshing and freshing == true then
			if not cache then
				freshing = false
			end
			if itemCount == 1 then
				ret_panel.tip = createLabel( ret_panel, {x = 100, y = 30, ax = 0, ay = 0.5, text = '等侯刷新', css='c1'} )
				xCenter( ret_panel.tip )
				local locX, locY = ret_panel.tip:getPosition()
				ret_panel.tip:setPosition( locX + 10, locY )
				locX, locY = ret_panel.tip:getPosition()
				ret_panel.arrow = createSprite( ret_panel, {x = locX - 20, y = 30, res = 'arrow_down.png'} )			
				ret_panel.loading = createSprite(  ret_panel, {x = locX - 20, y = 30, res = 'white_dot.png'})
				ret_panel.loading:setVisible( false )
			end
		end
		if cache then
			table.insert( cacheArr, user_info.id )
		end
		ret_panel.id = user_info.id
		--table.insert(focus_list, user_info)
		friendPage._move_small_page._focus_page:append_item(ret_panel)
		itemCount = itemCount + 1
	end

	if freshing == true then
		if table.getn(real_data) == 0 then
			friendPage._move_small_page._focus_page:append_item( create_empty_tips() )
		end
	end

	friendPage._move_small_page._focus_page:refresh_view()
	
end

local function on_get_fans_list(data, cache)
	fans_list = {}
	if freshing and freshing == true then
		friendPage._move_small_page._fans_page:clearAllItem()
		resetF2ListPosition()
	end
	
	for k, v in pairs(cacheArr) do
		clearItemById( friendPage._move_small_page._fans_page, v, cache )
	end
	local real_data = data.data.data
	itemCount = 1
	cacheArr = {}
	for _, user_info in ipairs(real_data) do
		local ret_panel = createSmallItemByUserInfo(user_info)
		if freshing and freshing == true then
			if not cache then
				freshing = false
			end
			if itemCount == 1 then
				ret_panel.tip = createLabel( ret_panel, {x = 100, y = 30, ax = 0, ay = 0.5, text = '等侯刷新', css='c1'} )
				xCenter( ret_panel.tip )
				local locX, locY = ret_panel.tip:getPosition()
				ret_panel.tip:setPosition( locX + 10, locY )
				locX, locY = ret_panel.tip:getPosition()
				ret_panel.arrow = createSprite( ret_panel, {x = locX - 20, y = 30, res = 'arrow_down.png'} )
				--ret_panel.arrow:setVisible( false )
				ret_panel.loading = createSprite(  ret_panel, {x = locX - 20, y = 30, res = 'white_dot.png'})
				ret_panel.loading:setVisible( false )
			end
		end
		if cache then
			table.insert( cacheArr, user_info.id )
		end
		ret_panel.id = user_info.id
		--table.insert(fans_list, user_info)
		friendPage._move_small_page._fans_page:append_item(ret_panel)
		itemCount = itemCount + 1
	end
	friendPage._move_small_page._fans_page:refresh_view()
	
end

local function request_focus_list(curPage)
	--doCommand( 'user', 'my_focus',{page = curPage}, on_get_focus_list )
	doCommand( 'user', 'my_focus',{page = curPage}, on_get_focus_list,300,{['loading'] = 0} )
end

local function request_fans_list(curPage)
	--doCommand( 'user', 'my_fans', {page = curPage},on_get_fans_list )
	doCommand( 'user', 'my_fans', {page = curPage},on_get_fans_list,300,{['loading'] = 0} )
end

--[[
[1] = {
	frendship = 0,
	id = *,
	profit = 188,
	is_real = 0,
	vip_id = 0,
	city = "",
	sex = 1,
	logo = "http ***",
	name = "**",
	mood = "**",
	province = "**",
}
--]]

local small_desc_info = {
	item_width = total_width,
	item_height = 115,    --两线间距离
	column_cnt = 1,
	x_space = 0,
	y_space = 0,
}

function onFFriendTouchEnd()
	focus_list = {}	
	freshing = true
	cur_focus_page = 1
	request_focus_list(cur_focus_page)	
end

function resetFListPosition()
	local config = FriendPageConfig["move_page_config"]
	friendPage._move_small_page._focus_page:setPosition(0, config["inner_y"])
end

function onF2FriendTouchEnd()
	fans_list = {}
	freshing = true
	--fans_page:clearAllItem()
	cur_fans_page = 1
	request_fans_list(cur_fans_page)	
end

function resetF2ListPosition()
	local config = FriendPageConfig["move_page_config"]
	friendPage._move_small_page._fans_page:setPosition(0, config["inner_y"])
end

local SEARCH_PAGE = Import("lua/search.lua")

local function createMoveSmallPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = FriendPageConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_small_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, 695) --黄区位置
	ret_panel._move_small_grp:setVMovable(true)
	
	ret_panel._move_small_grp.touchEnd = onFFriendTouchEnd
	ret_panel._move_small_grp.resetListPos = resetFListPosition
	ret_panel._move_small_grp.tipHeight = 700

	local focus_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	--focus_page._use_dynamic_size = true  把动态坐标设为固定的，然后新建下次再点击就不会出现动态坐标显示不正常了
	focus_page._use_dynamic_size = false
	focus_page:refresh_view()

	local function onFOverTop(layout)
		
	end

	local function onFOverBottom(layout)
		if cur_focus_page < max_focus_page then
			cur_focus_page = cur_focus_page + 1
			freshing = false
			request_focus_list(cur_focus_page)
		end
	end

	focus_page.onHMoveOverTop = onFOverTop
	focus_page.onHMoveOverBottom = onFOverBottom
	
	local fans_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	fans_page._use_dynamic_size = true
	fans_page:refresh_view()

	local function onF2OverTop(layout)
		
	end

	local function onF2OverBottom(layout)
		if cur_fans_page < max_fans_page then
			cur_fans_page = cur_fans_page + 1
			request_fans_list(cur_fans_page)
		end
	end

	fans_page.onHMoveOverTop = onF2OverTop
	fans_page.onHMoveOverBottom = onF2OverBottom

	focus_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(focus_page)
	fans_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(fans_page)
	ret_panel._move_small_grp:selectPage(1)

	ret_panel._focus_page = focus_page
	ret_panel._fans_page = fans_page 

	local function get_y_limit(obj)
		return config["inner_y"] - 60
	end
	
	--查找好友功能	
	local function onClickAddFriend(obj, x, y)
		closeFriendPagePanel()
		SEARCH_PAGE.showSearchPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		SEARCH_PAGE.init( { back_func = back_func })
		
	end		
	config = FriendPageConfig["find_btn_config"]
	ret_panel._say_btn = LIGHT_UI.clsCheckButton:New(ret_panel, config.x, config.y,        config.normal_res, config.click_res)
	ret_panel._say_btn.onTouchEnd = onClickAddFriend
	
	

	focus_page.getYLimit = get_y_limit
	fans_page.getYLimit = get_y_limit
	
	return ret_panel
end

--local SEARCH_PAGE = Import("lua/search.lua")

local function createFriendPagePanel(parent, x, y)
	local ret_panel = createContainerChildPanel( parent, x, y )

	local c, c2 = FriendPageConfig, nil
	local o = nil

	--[[local function on_back(btn)
		closeFriendPagePanel()
		MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		GAMEHALL_PANEL.showGameHallPanel(HTTP_CLIENT.getRootNode(), 0, 0 )
	end--]]

	--ret_panel._onBack = on_back
	--ret_panel._head_text:setString( '好友' )
	
--	local function onClickAddFriend(obj, x, y)
	--	closeFriendPagePanel()
	--	SEARCH_PAGE.showSearchPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	--end
	
	--ret_panel._add_friend_btn = createButton( ret_panel, c.add_btn, onClickAddFriend )

	config = FriendPageConfig["move_page_config"]
	ret_panel._move_small_page = createMoveSmallPage(ret_panel, config["x"], config["y"])

	local function on_sel_small_page(move_obj)
		freshing = true
		ret_panel._move_hint:selItem(move_obj:getCurPage())
		if move_obj:getCurPage() == 1 and table.maxn(focus_list) <= 0 then
			ret_panel._move_small_page._move_small_grp.touchEnd = onFFriendTouchEnd
			ret_panel._move_small_page._move_small_grp.resetListPos = resetFListPosition
			cur_focus_page = 1
			request_focus_list(cur_focus_page)
		end
		if move_obj:getCurPage() == 2 and table.maxn(fans_list) <= 0 then
			ret_panel._move_small_page._move_small_grp.touchEnd = onF2FriendTouchEnd
			ret_panel._move_small_page._move_small_grp.resetListPos = resetF2ListPosition
			cur_fans_page = 1
			request_fans_list(cur_fans_page)
		end
	end

	ret_panel._move_small_page._move_small_grp.onSelPage = on_sel_small_page

	local item_list = {
		[1] = {
			["txt"] = "关注",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
		[2] = {
			["txt"] = "粉丝",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
	}

	config = FriendPageConfig["move_hint_config"]
	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, config["x"], config["y"], "tab_bg_image.png", item_list, total_width, config["res"], "tiny_black_pixel.png")
	local bg = ret_panel._move_hint:getBGObj()
	bg:setScaleX(total_width)
	ret_panel._move_hint:selItem(1)

	cur_focus_page = 1
	--request_focus_list(cur_focus_page)

	local function on_click( obj, idx )
		friendPage._move_hint:selItem( idx )		
		friendPage._move_small_page._move_small_grp:selectPage( idx )
	end

	ret_panel._move_hint.onClick = on_click
	
	ret_panel.closePanel = closeFriendPagePanel

	return ret_panel
end

function showFriendPagePanel(parent, x, y)
	freshing = true
	
	friendPage = createFriendPagePanel(parent, x, y)
	request_focus_list(cur_focus_page)
	
	return friendPage
end

function closeFriendPagePanel()
	if friendPage and friendPage._move_small_page._move_small_grp._move_cb then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(friendPage._move_small_page._move_small_grp._move_cb)	
		friendPage._move_small_page._move_small_grp._move_cb = nil
	end
	--[[if friendPage then
		friendPage:setVisible(false)
	end--]]
	clear({friendPage})
	friendPage = nil
end

