--room_match
ROOM_MATCH_LAYOUT = Import("layout/room_match.lua")

--room_control
--ROOM_CONTROL = Import("lua/room_control.lua")
safe_dofile("lua/room_control.lua")
safe_dofile("lua/room_chat.lua")
safe_dofile("lua/room_face.lua")
safe_dofile("lua/room_luck.lua")
safe_dofile("lua/room_userinfo.lua")


local RoomMatchConfig = ROOM_MATCH_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local isWatch = true --默认为旁观状态
local curMsgID = 0 --当前消息id
local isLeave = false --游戏中主动离开
local isAct = false --托管
local tBeginTimer = 5 --游戏开始前等待时间
local tGameEndActionTimer = 10 --游戏结束播放动画总时间
local tShowShoutDelay = 3 --显示选号框延时
local nSameWay = 0 --相同的骰，用于结算动画
local RoomMatch = nil --游戏房间
local nPlayer = 0 --玩家数量
local roomData = nil	-- 房间信息
local matchData = nil -- 比赛数据
local nowShout = nil	-- 当前喊号信息
local waittips = nil --等待提示
local gold_multiple = 1 --游戏底金倍数
local matching = false  --比赛中
local sitdown = false --坐下标志

local timeout_start = false --倒计时时钟启动标识
local lastShout = {	-- 上一次喊的数据，以便于作有效性对比
	count = 0,	-- 数量
	number = 0,	-- 数字
	zhai = 0,	--斋
	index = nil,	--谁喊的？1，2，3，4
						-- nil 表示未有人喊过（重要）
}

local next_index = {	-- 记录下一个索引，为循环的环形结构，主要用于实现“下一个”的倒计时
	-- 注意：key为字符串形式
}

-------------------------------------
--前置函数声明

--开始
local start
--结束
local stop
--同步游戏场景
local syncGameScene
--通过uid获取椅子号
local getChairID
--初始化设置位置
local initSeat
--创建选号框
local createShoutPanel
--获取椅子号,区分坐下前后
local getDeskID
--自己倒计时超时，关闭喊面板及按钮
local onMeTimeOut
--显示开
local viewOpen
--显示喊按钮
local viewShoutBtn
--隐藏喊按钮
local hideShoutBtn
--显示开按钮
local viewOpenBtn
--隐藏开按钮
local hideOpenBtn
--隐藏选号框
local hideShoutPanel
--显示选号框
local showShoutPanel
--取消托管
local cancelAct
--刷新积分
local doRefreshWealth
--创建自己的骰
local createSelfWay
--同步自己的骰
local syncMyWay
-------------------------------------

--返回处理
local back_func = nil

local asking = false

--注意：字符串形式
local uid = nil
local room_id = nil
local desk_id = nil

--将uid转为desk序号
local uid2desk = nil

--显示等待玩家进入游戏提示
local WaitType = {
	[1] = '等待其他玩家进入游戏',
	[2] = '游戏进行中，请等待下一局开始',
}

--初始化成员变量
local function Initialize()
    isWatch = true
    curMsgID = 0
    isLeave = false
    nSameWay = 0
    nPlayer = 0
    gold_multiple = 1
	sitdown = false
	isAct = false --托管
end

local function IsWatch()
    return isWatch
end

local function hideWaittips()
	if waittips then 
		clear({waittips})
		waittips = nil
	end
end
local function showWaittips(nType)
	hideWaittips()
	local c = RoomMatchConfig.message_tips
	local option = {x= c.x,y = c.y}
	if WaitType[nType] then
		waittips = showMessageX(RoomMatch,WaitType[nType],option)
	end
end

--翻倍
local function hideDouble()
	if RoomMatch and RoomMatch.double then
		clear( { RoomMatch.double })		
		RoomMatch.double = nil
		if RoomMatch.double_text then
			clear( {RoomMatch.double_text })
			RoomMatch.double_text = nil		
		end
	end
end

local function showDouble(count)
	hideDouble()
	local c = RoomMatchConfig["double"]
	RoomMatch.double = createSprite(RoomMatch,c)	
	if count > 1 then
		c = RoomMatchConfig["double_text"]	
		RoomMatch.double_text = createLabel( RoomMatch, c )	
		RoomMatch.double_text:setString(string.format( '× %d', count ))
	end
	gold_multiple = count
end

--获取财富信息
function doRefreshWealth()
    local function onUserMoney( data )
		if not RoomMatch then
			return
		end
        if not RoomMatch._gold then
            local c = RoomMatchConfig
            createSprite( RoomMatch, c.gold_pic )
            RoomMatch._gold = createLabel( RoomMatch, c.gold_text )
        end
		RoomMatch._gold:setString( data.data.gold )
	end
	--请求用户资料
	doCommand( 'user', 'money', {}, onUserMoney )
end

--重置比赛
local function doResetMatch()
    if RoomMatch then 
        if RoomMatch._Way then
            clear( {RoomMatch._Way} )
            RoomMatch._Way = nil
        end
        if RoomMatch._Star then
            clear({RoomMatch._Star})
            RoomMatch._Star = nil
        end
		
		for i = 2,4 do
			if RoomMatch._desk[i] and RoomMatch._desk[i]._zhong then
				RoomMatch._desk[i]._zhong:setVisible(true)
			end	
		end			
    end
	--还原底金倍数
	gold_multiple = 1
    --RoomMatch._match_gold:setString( string.format( '金币：%s', g_match_gold ) )
    nSameWay = 0
	--matching = false --设置为非比赛状态	
	hideDouble()	
	--isLeave = false
end
--获取房间ID
local function GetRoomID()
	--assert(matchData)
	--assert(matchData.id)	
	return room_id
end

--获取椅子号,区分坐下前后
function getDeskID(index)
    return index
end

--显示游戏开始倒计时
local function doCountDown( begin_sec,delay ) --从第几秒开始倒计时
	--坐下动画
	local c = RoomMatchConfig
	local function onTimeOut()	
		clear({RoomMatch._countdown1})
		RoomMatch._countdown1 = nil
		clear({RoomMatch._countdown2})
		RoomMatch._countdown2 = nil
	end
	if RoomMatch._countdown1 then
	    return
	end
	play_countdown(RoomMatch:getCOObj(),c["action_countdown"],begin_sec,delay,onTimeOut)
    local function showText()
		matching = true --设置为游戏中，防止玩家退出
        RoomMatch._countdown1:setVisible(true)
		RoomMatch._countdown2:setVisible(true)
		playWav( c["action_countdown"].sound, c["action_countdown"].ms )
    end
	RoomMatch._countdown1 = createLabel( RoomMatch, c.timeout_text1 )
	RoomMatch._countdown2 = createLabel( RoomMatch, c.timeout_text2 )
	--RoomMatch._countdown:setString("秒后游戏开始")
	RoomMatch._countdown1:setVisible(false)
	RoomMatch._countdown2:setVisible(false)
	f_delay_do(RoomMatch,showText,nil,delay)
	
end

--显示赢取金币
local function doWinAction(desk_id_from,desk_id_to,count)  --金币飞动相对调整
    if RoomMatch._Way and desk_id_from and desk_id_to then 
        local c = RoomMatchConfig
        local c2 = c["timeout"..desk_id_from]
        local c3 = c["timeout"..desk_id_to]
        for i = 1,count do 
	        local o = createSprite( RoomMatch._Way, res(0, 0, "res/coin.png") )
	        o:setPosition(c2.x,c2.y)
	        a_emove_fadeIn(o:getCOObj(),2,c3,(count - i)*0.2)
	    end
	    if not isWatch then
	        --刷新积分
	        doRefreshWealth()
	    end
	end
end

--显示胜负标志
local function doWinLoseMark(desk_id,type)
    if RoomMatch._Way and desk_id then 	
        local c = RoomMatchConfig["open"..desk_id]	
		c.res = RoomMatchConfig[type].res
		
		local o = createSprite(RoomMatch._Way, c)
		--local o = createSprite(RoomMatch, c)
		a_fadeout(o:getSprite(),nil,2)--2秒后消失		
    end
end

--显示几个几
local function doShowWayCount(count)
    local c = RoomMatchConfig
    if RoomMatch._Way then
		if count > 0 then
			c.result_count.text = '×' .. count
		else
			c.result_count.text = '× ?'
		end
        if RoomMatch._Way.count then
            clear({RoomMatch._Way.count})
            RoomMatch._Way.count = nil
        end
		--创建一个中心的点，为了生成中心点的相对位置
		clear({createNode(RoomMatch,c.center)})
        RoomMatch._Way.count = createLabel( RoomMatch._Way, c.result_count )
	end
end

--显示火星动画
local function doShowStar(count)
    if not RoomMatch._Star then 
        RoomMatch._Star = createNode(RoomMatch, point(0,0))
    end
    local c = RoomMatchConfig
    local o = createSprite( RoomMatch._Star, res(0, 0, "res/stars.png") )
	o:setPosition(c.center.x,c.center.y)
	local c = RoomMatchConfig
    a_play(o:getSprite(),c["action_star"],false)
    doShowWayCount(count)
end	

--摇动骰子
local function doShakeCup(desk_id,fn,arg)
	if RoomMatch._desk[ desk_id ] and RoomMatch._desk[ desk_id ]._zhong then
		a_shake(RoomMatch._desk[ desk_id ]._zhong:getCOObj(),0.1,5,15,0,fn,arg)
	end
	for i = 2,4 do
		if RoomMatch._desk[ i ] and RoomMatch._desk[ i ]._zhong then
			a_shake(RoomMatch._desk[ i ]._zhong:getCOObj(),0.1,5,15,0)
		end
	end
end

--移动骰子
local function doMoveMyWay(match_data)
	local dice_move_speed = 1000

	match_id = match_data.id
	local way = match_data.way
	print(way)
	for i = 1, 5 do
		local key = string.sub(way, i, i)		
		print(key)
		RoomMatch._selfway[i]:setImage("szm"..key..".png")
		print(i)
		RoomMatch._selfway[i]:setAnchorPoint(0, 0)
	end

	LIGHT_UI.moveObj(RoomMatch._selfway, dice_move_speed, 315+240 )
end		

--展示自己的摇骰
local function doShowMyWay(way)
	for i = 1, 5 do
		local key = string.sub(way, i, i)
		print(key)
		RoomMatch._selfway[i]:setImage("szm"..key..".png")
	end
end

--显示赢/输的积分动画
local function doShowScore(desk_id,bWin,score,delay)
	local function fn()
		local c = RoomMatchConfig
		local c2 = c["gold"..getDeskID(desk_id)]
		local o = nil 
		if bWin == true then 
			o = createLabelAtlas(RoomMatch,c["action_win_score"])
			o:setString("+"..tostring(score))
		else
			o = createLabelAtlas(RoomMatch,c["action_loss_score"])
			o:setString("-"..tostring(score))
		end
		
		o:setPosition(c2.x,c2.y)
		a_fadein_move_fadeout(o:getCOObj(),1,1,ccp(c2.x,c2.y + 20),1,0.5,0)
	end
		
	f_delay_do(RoomMatch,fn,nil,delay)
end

--销毁玩家读秒
local function doKillUserTimer()
    if RoomMatch._UserTimer then
	    clear({RoomMatch._UserTimer})
	    RoomMatch._UserTimer = nil
	end
end
--显示玩家读秒
local function doUserTimer(desk_id) --椅子ID
	doKillUserTimer()
	local c = RoomMatchConfig	
	local o = createSprite(RoomMatch,c["sitdown"..getDeskID(desk_id)])
	RoomMatch._UserTimer = o	
	if desk_id == 1 and not isWatch then --自己超时，则隐藏选择面板
	    a_play(o:getSprite(),c["action_timeout"],false,nil,nil,onMeTimeOut)
	else
	    a_play(o:getSprite(),c["action_timeout"],false)
	end
	--local desk = RoomMatch._desk[desk_id]
	--local x,y = desk:getPosition()
	o:setPosition(c["timeout"..desk_id].x,c["timeout"..desk_id].y)
	RoomMatch.UserTimer = a	
end


--显示开动画
local function doUserOpen(desk_id)
    if not RoomMatch._Way then
	    RoomMatch._Way = createNode(RoomMatch, point(0,0))
	end
	
    if RoomMatch._desk[ desk_id ] then
        local c = RoomMatchConfig["open"..desk_id]
        c.res = RoomMatchConfig["open"].res
        --local c2 = c["desk"..getDeskID(desk_id)]
        local o = createSprite(RoomMatch._Way, c)
        --o:setPosition(c2.x + 80,c2.y + 100)
        a_fadeout_move(o:getSprite(),1,1,ccp(0, 20))
	end		
    for i = 2,4 do
        if RoomMatch._desk[i] and RoomMatch._desk[i]._zhong then
            RoomMatch._desk[i]._zhong:setVisible(false)
        end
    end		
end

--获取当前玩家数
local function getPlayerCount()
	local nPlayer = 0
	for i = 1,4 do
		if RoomMatch and RoomMatch._desk[i] then
			nPlayer = nPlayer + 1
		end
	end
	return nPlayer
end

--自己倒计时超时，关闭喊面板及按钮
function onMeTimeOut()
	if not isWatch then
		hideShoutPanel()
		hideShoutBtn()
		hideOpenBtn()
	end
end

--取消托管
function cancelAct()
	if RoomMatch and IsAgent() == true then
		showNoActBtn(RoomMatch,true)
		doCancelAct(RoomMatch)
	end
end

--创建自己的骰
function createSelfWay()
	local c = RoomMatchConfig.selfway
	if RoomMatch._selfway then
		clear({RoomMatch._selfway})			
	end
	local ret_panel = createNode(RoomMatch,c)
			
	local o = nil
			
	local start_x = -240
	for i = 1, 5 do
		o = createSprite( ret_panel, res( start_x, 0, "szs1.png" ) )
		o:setAnchorPoint(0, 0)
		start_x = start_x + 48
		ret_panel[i] = o
	end
	RoomMatch._selfway = ret_panel		
	return ret_panel									
end

--同步自己的骰
function syncMyWay(way)
	createSelfWay()
	for i = 1, 5 do
		local key = string.sub(way, i, i)		
		RoomMatch._selfway[i]:setImage("szm"..key..".png")		
		RoomMatch._selfway[i]:setAnchorPoint(0, 0)
	end
	local x,y = RoomMatch._selfway:getPosition()
	RoomMatch._selfway:setPosition(x + 555,y)	
end


-----------------------------------------------------------------

--显示开
function viewOpen( index )
	if RoomMatch._desk[ index ] then
		RoomMatch._desk[ index ].show( 'open' )
	end
end

--显示喊按钮
function viewShoutBtn()
    if RoomMatch._shout_btn then
	    view( RoomMatch._shout_btn )
	end
end

--隐藏喊按钮
function hideShoutBtn()
    if RoomMatch._shout_btn then
	    hide( {RoomMatch._shout_btn} )
	end
end

--显示开按钮
function viewOpenBtn()
    if RoomMatch._open_btn then
	    view( RoomMatch._open_btn )
	end
end

--隐藏开按钮
function hideOpenBtn()
    if RoomMatch._open_btn then
	    hide( {RoomMatch._open_btn} )
	end
end

--隐藏选号框
function hideShoutPanel()
    if RoomMatch._shoutselpanel then
	    clear( {RoomMatch._shoutselpanel} )
	    RoomMatch._shoutselpanel = nil
	end
end

--显示选号框
function showShoutPanel(delay)
	cancelAct()
    if delay then
        f_delay_do(RoomMatch,createShoutPanel,nil,tonumber(delay))
    else
        f_delay_do(RoomMatch,createShoutPanel,nil,0)
    end
end

--坐下
local function doSit( desk )
	local function onSit( data )
		if data.code < 0 then
		showMessage( RoomMatch, data.memo )
			--showMessage( RoomPage, data.memo )
		else
			--刷新“自己的”积分
			doRefreshWealth()
			--isWatch = false
			--坐下标识
			sitdown = true
			start() --进入游戏，再次刷新
			--[[
			if data.code > 0 then
			    local delay = data.code - tBeginTimer
			    if delay < 0 then
			        delay = 0
			    end
			    doCountDown(tBeginTimer,delay)
			end
			]]
		end		
	end
	--local msg = string.format("坐下的全局位置：%d",desk)
	--local op = {ms=5000}
	--showMessage(RoomMatch,msg,op)
	doCommand( 'room', 'sit', { ['id'] = room_id,['index']= desk, ['password'] = '' }, onSit )
end

--本人喊或开后、或倒计时结束统一处理
local function afterShoutOrOpen( index )
	--stopTimer( index )
	hideOpenBtn()
	hideShoutBtn()
	
	if RoomMatch._shoutselpanel then	
		clear( {RoomMatch._shoutselpanel} )
		RoomMatch._shoutselpanel = nil
	end
end
--喊
local function doShout()
	local c = RoomMatchConfig.shout_item
	
	if not nowShout.count then
		showMessage( RoomMatch, c.count_str )
		return
	end
		
	if not nowShout.number then
		showMessage( RoomMatch, c.number_str )
		return
	end
	
	if nowShout.number == 1 then
		nowShout.zhai = 1
	end
	
	--计算总和，便于大小比较：百、十、个位分别为：数量、斋、数字
	local function calSum( data )
		local sum = 0
		sum = data.count * 100 + data.zhai * 10
		
		if data.number == 1 then
			sum = sum + 7
		else
			sum = sum + data.number
		end
		
		return sum
	end
	
	if lastShout.index then
		if calSum( nowShout ) <= calSum( lastShout ) then
			showMessage( RoomMatch, c.way_str )
			return
		end
				
		if lastShout.zhai == 1 then
			if nowShout.zhai ~= 1 and lastShout.count * 2 > nowShout.count then
				showMessage( RoomMatch, c.cut_zhai_str )
				return
			end
		end
	end
	
	nowShout.zhai = nowShout.zhai or 0
	
	local function onShoutCB(data)
		if data.code < 0 then
			print "error when shout: "
			d(data)
		end
	end
	
	print( 'doShout' )
	d( nowShout )

	doCommand( "room", "shout", { id = matchData.id, count = nowShout.count, 
		number = nowShout.number, zhai = nowShout.zhai }, onShoutCB)
	
	afterShoutOrOpen( 1 )
end

--创建选号框
local function createShout(parent, x, y)
	--matchData = {}
	--matchData.person = 2
	--nowShout = {}	
	
	--骰子数
	local person = matchData.person
	
	local max_cnt = person * 4 + 1
	local cnt_width = 60
	local all_cnt_width = max_cnt * cnt_width 

	local ret_panel = createNode( parent, point( x, y) )
	ret_panel._cnt_list = {}
	local c = RoomMatchConfig.shout_item
	local c2 = nil
	
	local o = nil
	
	o = createSprite( ret_panel, res(0, 0, "cz-bg.png") )
	o:setAnchorPoint(0, 1)

	o = createButton(ret_panel, RoomMatchConfig.shout_close )
	o.onTouchEnd = hideShoutPanel

	local cnt_move_delta = c.cnt_move_delta

	local function on_left(btn, x, y)
		max_cnt = #ret_panel._cnt_list
		local head_x, _ = ret_panel._cnt_list[1]:getPosition()		
		if head_x >= 0 then
			return
		end

		for i = 1, max_cnt do
			local x, y = ret_panel._cnt_list[i]:getPosition()
			x = x + cnt_move_delta
			ret_panel._cnt_list[i]:setPosition(x, y)
		end
	end

	local function on_right(btn, x, y)
		max_cnt = #ret_panel._cnt_list
		local tail_x, _ = ret_panel._cnt_list[max_cnt]:getPosition()
		if tail_x <= 0 then
			return
		end

		for i = 1, max_cnt do
			local x, y = ret_panel._cnt_list[i]:getPosition()
			x = x - cnt_move_delta
			ret_panel._cnt_list[i]:setPosition(x, y)
		end
	end

	o = createButton(ret_panel, res( c.left_btn_x, c.left_btn_y, c.left_btn_res) )
	o.onTouchEnd = on_left
	
	o = createButton(ret_panel, res( c.right_btn_x, c.right_btn_y, c.right_btn_res, c.right_btn_res) )
	o.onTouchEnd = on_right
	
	o = LIGHT_UI.clsClipLayer:New(ret_panel, c.shout_view_x, c.shout_view_y)
	--o = LIGHT_UI.clsClipLayer:New(ret_panel, 70, c.shout_view_y)
	o:set_msg_rect( c.x, c.y, c.sx, c.sy )
	local shout_cnt_panel = o

	local startx = 27   --喊号框数目设置
	local starty = 26
	local x_step = 54
	local count = {}
	local function onSelectCount(obj, x, y)
		if nowShout.count then
			if nowShout.count == obj.count then
				return
			end
			
			local o = nil
			o = count[nowShout.count]
			o:setTextColor(51, 51, 51)
			o:setStatus(LIGHT_UI.NORMAL_STATUS)
		end
		
		nowShout.count = obj.count
		obj:setStatus(LIGHT_UI.DOWN_STATUS)
		obj:setTextColor(255, 255, 255)
	end
	
	local min_count = person
	if lastShout.index then
		if lastShout.number == 1 then
			min_count = lastShout.count + 1
		else
			min_count = lastShout.count
		end
	end
	
	local btn_res = {
		[LIGHT_UI.NORMAL_STATUS] = 'shuzi.png',
		[LIGHT_UI.DOWN_STATUS] = 'shuzi_on.png',
	}
	
	for i = min_count, person * 5 do
		o = LIGHT_UI.clsBaseButton:New(shout_cnt_panel, startx, starty, btn_res)
		
		o.count = i
		o:setString(tostring(i))
			o:setTextColor(102, 102, 102)
		o:setStatus(LIGHT_UI.NORMAL_STATUS)
		o.onDownChangeSprite = empty_function
		o.onUpChangeSprite = empty_function
		o.onTouchUp = onSelectCount
		count[i] = o
		table.insert(ret_panel._cnt_list, o)
		
		startx = startx + x_step
	end

	local dice = {}
	local function onSelectNumber(obj, x, y)
		if nowShout.number then
			if nowShout.number == obj.number then
				return
			end
			
			dice[nowShout.number]:setStatus( LIGHT_UI.NORMAL_STATUS )
		end
		
		nowShout.number = obj.number
		obj:setStatus( LIGHT_UI.DOWN_STATUS )
	end
	startx = c.sel_dice_startx 
	starty = c.sel_dice_starty
	x_step = 60
	
	for i = 1, 6 do
		local normal_file = string.format("sz%d.png", i)
		local click_file = string.format("sz%d_on.png", i)
		o = createButton( ret_panel, res( startx, starty, normal_file, click_file) )
		o.number = i
		o.onTouchEnd = onSelectNumber
		dice[i] = o
		startx = startx + x_step
	end
	
	local function doJiaYi()
		if not lastShout.index then
			return
		end
		
		if lastShout.count == matchData.person * 5 then
			showMessage( RoomMatch, '数量太大了，不能再加1了' )
			return
		end
		
		nowShout.count, nowShout.number, nowShout.zhai = lastShout.count + 1, lastShout.number, lastShout.zhai
		
		doShout()
	end

	o = createButton( ret_panel, res( c.plus_btn_x, c.plus_btn_y, "jiayi.png", "jiayi_on.png") )
	o.onTouchEnd = doJiaYi

	local function onSelectZhai(obj, x, y)
		if nowShout.zhai == 1 then
			nowShout.zhai = 0
			obj:setStatus(LIGHT_UI.NORMAL_STATUS)			
		else
			nowShout.zhai = 1
			obj:setStatus(LIGHT_UI.DOWN_STATUS)			
		end
	end
	o = createButton( ret_panel, res( c.zhai_btn_x, c.zhai_btn_y, "zai.png", "zai_on.png" ) )
	o.onTouchEnd = onSelectZhai
	
	if lastShout.index then
		if lastShout.zhai == 1 then
			nowShout.zhai = 1
			o:setStatus(LIGHT_UI.DOWN_STATUS)
		else
			nowShout.zhai = 0
		end
	else
		nowShout.zhai = 0
	end 

	o = createButton( ret_panel, res( c.ok_btn_x, c.ok_btn_y, "ok.png", "ok_on.png" ) )
	o:setTextColor(0,0,0)
	o.onTouchEnd = doShout

	return ret_panel
end
--创建选号框
function createShoutPanel()
	if RoomMatch._shoutselpanel then
		clear( {RoomMatch._shoutselpanel} )
		RoomMatch._shoutselpanel = nil
	else
		nowShout = {
			count = nil,
			number = nil,
			zhai = nil,
		}
		
		o = createShout( RoomMatch, 20, 500)
		RoomMatch._shoutselpanel = o
	end
end

--隐藏喊号框
local function hideLastHHK()
	for i = 1,4 do
		if RoomMatch._desk[ i ] and RoomMatch._desk[ i ]._hhk then
			hide( {RoomMatch._desk[ i ]._hhk} )
		end
	end		
end

--[[
创建有用户的桌
参数：
	桌索引：1为自己
	用户信息：nil表示空位
]]
local function createDesk( index, info )
	local p = RoomMatch
	
	if p._desk[index] then
		local new_uid = 0
		if info ~= nil and info['uid'] then
			new_uid = info.uid
		end
		if p._desk[index]._uid == new_uid then
			do return p._desk[index] end
		else
			clear( {p._desk[index]} )
			p._desk[index] = nil
		end
	end		
	--位置配置文件
	local c = RoomMatchConfig
	local ret_panel = nil
	if not info then
		if  false == sitdown then
			--创建坐下按钮
			ret_panel = createNode(p, c["desk"..index])
			local function onClick()
				print("sit down")
				doSit(index) --选择位置坐下
			end					
			local o = createButton(ret_panel,c["sitdown"..index],onClick)
			ret_panel._sitdown = o	
			--坐下动画
			local a = createSprite(ret_panel,c["a_sitdown"..index])
			a_play(a:getSprite(),c["action_sitdown"],true)											
		end					
		do return ret_panel end								
	end
		
	local o = nil	
	ret_panel = createNode(p, c["desk"..getDeskID(index)])	
	--盅
	o = createSprite( ret_panel, c["zhong"..getDeskID(index)] )
	ret_panel._zhong = o	
	local logo_panel = nil
	logo_panel = createNode( ret_panel, c["logo"..getDeskID(index)] )
	
	--显示开
	local function show( type )
		local c = RoomMatchConfig[ 'open' .. getDeskID(index)]
		c.res = RoomMatchConfig[type].res
		
		createSprite(logo_panel, c, RoomMatchConfig.delay[type] )
	end
	ret_panel.show = show
    --椅子背景
    --local o_bg = createSprite(logo_panel,c["desk_bg"])
	local function onClick()
		if uid ~= p._desk[index]._uid then
			showRoomUserInfo(RoomMatch,p._desk[index]._uid)
		end
	end
	local o_bg = createButton(logo_panel,c["desk_bg"],onClick)
    ret_panel._bg = o_bg
    
	o = createLabel(logo_panel, c.name)

	ret_panel._name = o
	
	if info then	
		
		--o:setImage( info.logo )
		--a_fadein(o:getSprite(),1) --1秒谈入
		
		--[[local scale = nil
		if string.find( info.logo, '/logo/sex' ) then
			scale = 0.48	-- 默认头像收缩
		else
			scale = 0.6	-- 一般头像收缩
		end--]]
		local c = { x = 0,y = 0,sx = 74,sy = 74, res = info.log }
		o = createSprite(logo_panel, c )
		o:setImage( info.logo )			
		AdjustString(ret_panel._name,info.name,70)
		--ret_panel._name:setString( info.name )
		
		ret_panel._uid = info.uid
	end
	--喊号情况
	local hhk = createNode( logo_panel, c[ 'hhk' .. getDeskID(index) ] )
	
	createSprite( hhk,  res( 0, 0, "hhk.png") )     --喊号内部文字调整
	o = createLabel( hhk, text( -30, 0, "5 ×" ) )
	hhk._count = o
	
	o = createSprite(hhk, res( 5, 0, "szs1.png") )
	hhk._number = o
	
	o = createLabel(hhk, RoomMatchConfig.hhxs )
	hhk._zhai = o

	hide({hhk})
	ret_panel._hhk = hhk
	
	local function showHHK( data )
		local hhk = ret_panel._hhk
	
		hhk._count:setString( data.count .. " ×" )
		hhk._number:setImage( "szs" .. data.number .. ".png" )
		
		if data.zhai == 1 then
			hhk._zhai:setString( "斋" )
		else
			hhk._zhai:setString( "" )
		end
		
		view(hhk)
	end
	ret_panel.showHHK = showHHK
	
	return ret_panel
end


local function onRoomDetail(data)	
	data = data.data
	roomData = data
	
	local level = {
		["1"] = "初级",
		["2"] = "中级",
		["3"] = "高级",
	}
	assert(data.room)
	--1,先记录服务器传过来的pool_id
	--if tonumber(roomData.pool_id) > curMsgID then
	if curMsgID == 0 then
		curMsgID = tonumber(roomData.pool_id)
	end
	--2,解析roomData.room数据
	if not RoomMatch then
		return
	end
		
	if RoomMatch._name then
		RoomMatch._name:setString( string.format( '%s房间：%03d', level[ data.room.level ], data.room.no ) )
	end
	if RoomMatch._match_gold then
		RoomMatch._match_gold:setString( string.format( '金币：%s', data.room.gold ) )
	end	
	--g_match_gold = data.room.gold
	
	if _G.next( data.room.desk ) == nil then
	    doSit(1)
		--第一个玩家，提示等待
		showWaittips(1)		
	else 
		--判断短线重进	
		for i = 1, 4 do
			if data.room.desk[tostring(i)] then
				if data.room.desk[tostring(i)].uid == uid and sitdown == false then
					isWatch = false
					sitdown = true
					if data.way then	
						syncMyWay(data.way)	
					end		
					break
				end
			end
		end
	
	    initSeat(data.room.desk)
	    --同步游戏场景
	    syncGameScene(data.room)
	end
	--比赛进行中
	--1等待，2比赛中，3比赛结束
	if tonumber(data.room.status) == 2 and isWatch == true and sitdown == true then
		showWaittips(tonumber(data.room.status))		
	end
end

local function createTopScrollPanel(parent, x, y)
	local ret_panel = createNode( parent, point( x, y ) )

	local o = createSprite( ret_panel, res( 0, 0, RoomMatchConfig.head_scroll.res ) )
	o:setAnchorPoint(0, 0)
	--o:setPosition(0, 0)

	local _scroll_text = LIGHT_UI.clsRecycleScroll:New(ret_panel, 10, 8, 460, 30)
	
	local txt = {
		[1] = '骰友比赛，娱乐无穷！',
		[2] = '惊险刺激，回味无穷。',
		[3] = '',
	}
	
	local obj = createLabel( nil, text( 0, 0, txt[i], "c3" ) )
	_scroll_text:addInnerObj( obj )
	
	local function setString( content )
		for i = 3, 2, -1 do
			txt[i] = txt[i-1]
		end
		txt[1] = content
		
		obj:setString( string.format( '              %s             %s             %s', txt[1], txt[2], txt[3] ) )
		_scroll_text:tryStopScroll()
		
		view( ret_panel )
		_scroll_text:startScroll()
		
		f_delay_do(RoomMatch,hide, {ret_panel}, 2000)
	end
	ret_panel.setString = setString
	
	local function stop()
		_scroll_text:tryStopScroll()
	end
	ret_panel.stop = stop
	
	return ret_panel
end

--游戏结束开几个几
local function doShowWay(way_number)
    if not RoomMatch._Way then
	    return
	end
	local c = deepcopy(RoomMatchConfig["result_number"])	
	c.res = string.format(c.res,way_number)	
	local o = createSprite( RoomMatch._Way, c)	
	doShowWayCount(0)
end

--开骰情况
local function createWay( parent, index, way ,zhai,op_number,delay)
	local step = 32
	local c = RoomMatchConfig
	
	--创建骰子
	-- x,y表示偏离值
	local function createNumber( x, y, num )
		return createSprite( RoomMatch._Way, res( x, y, "szs" .. num .. ".png" ) )
	end
	
	print( "way is:" .. way )
	local num = {}
	for i = 1, 5 do
		num[i] = string.sub( way, i, i )
	end		
		
	--移动
	local ptMid = point(c.center.x,c.center.y)
	local t_delay = delay
	
	local function movetoCenter(o,value)
		local function onMovetoCenter(arg)
	        doShowStar(arg)
	        o:setVisible(false)
	    end
		if tonumber(value) == op_number or (tonumber(value) == 1 and tonumber(zhai) <= 0) then
		    nSameWay = nSameWay + 1
			a_move(o:getCOObj(),t_delay,0.5,ccp(ptMid.x,ptMid.y),onMovetoCenter,nSameWay)
			t_delay = t_delay + 0.5 --每个骰子有0.5秒的移动时间
		end
	end
		
	local function getPos(i)
	    return RoomMatchConfig["way"..getDeskID(index)]
	end
	
	local o1 = createNumber( getPos(index).x + 0, getPos(index).y +0, num[1] )
	movetoCenter(o1,num[1])
	local o2 = createNumber( getPos(index).x +step, getPos(index).y +0, num[2] )
	movetoCenter(o2,num[2])
	local o3 = createNumber( getPos(index).x +0, getPos(index).y-step, num[3] )
	movetoCenter(o3,num[3])
	local o4 = createNumber( getPos(index).x +step, getPos(index).y-step, num[4] )
	movetoCenter(o4,num[4])
	local o5 = createNumber( getPos(index).x +step * 2, getPos(index).y-step, num[5] )
	movetoCenter(o5,num[5])
	
	--return ret_panel
end

local function createPanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createNode( parent, point( x, y ) )
	
	local o = nil
	local c, c2 = RoomMatchConfig, nil	-- 为了代码简化，另据资料说局部变量也利于效率
	
	o = createSprite( ret_panel, c.head_bg )
	o:setAnchorPoint(0, 0)
	
	local function on_back(btn)	
		--[[
		if matching == true and not isWatch then
			showMessage( RoomMatch, '比赛中不允许退出' )
			--设置比赛中主动退出标识,等待比赛完毕后自动退出
			isLeave = true
			do return end
		end			
		]]
		--结束游戏
		stop()		
	end
	
	o = createButton( ret_panel, c.head_back )
	o.onTouchEnd = on_back
	
	o = createLabel( ret_panel, c.head_text )
	o:setAnchorPoint(0, 0)
	ret_panel._name = o
	
	o = createLabel(ret_panel, c.head_gold)
	o:setAnchorPoint(0, 0)
	ret_panel._match_gold = o
	
	--凡说暂时隐藏
	--[[
	c2 = RoomMatchConfig.head_scroll
	o = createTopScrollPanel(ret_panel, c2.x, c2.y)
	ret_panel.setScroll = o.setString
	ret_panel.stopScroll = o.stop
	]]
	ret_panel._desk = {}
	
	return ret_panel
end

function start()
	
	--重置游戏状态
    doResetMatch()
	
	doCommand( "room", "detail", { id = room_id}, onRoomDetail)
end

--退出游戏
local function exit()
	   closePanel()
	   if back_func then
		  --还原旁观状态
		  isWatch = true		
		  back_func()
	   else
		  local ROOM_PANEL = Import("lua/room.lua")
		  ROOM_PANEL.showPanel( HTTP_CLIENT.getRootNode(), 0, 0)		
	   end
end

--结束游戏
function stop()
	local function onRoomLeave(data)
		if data.code < 0 then
			print "error when leave"
			d( data )
			local op = {ms = 3000}
			showMessage( RoomMatch, '游戏进行中，请稍等，当前局结束为您退出',op )
		else
		   exit()
		end
	end
		
	isLeave = true
	if sitdown == true then
		doCommand( "room", "leave", { ["id"] = room_id }, onRoomLeave)
	else
		exit()
	end
end
--比赛开始
--[[
1：比赛开始
消息结构为：
id：比赛id
uid1= {uid, name, logo}，uid=0时表示此座位没人。
uid2
uid3
uid4
way = xxxxx自己骰子
next_uid = xxx，当前该谁喊
]]
local function onMsgMatchStart(data)
    --重置游戏状态
    doResetMatch()
    
	matchData = data
	d(data)
	
	--判断旁观
	if isWatch == true and sitdown == true then
		isWatch = false
	end
	
	-- 比赛状态时忽略专门给旁观者发的开赛信息
	if not isWatch and not data.way then
		return
	end
	
	lastShout = {
		count = 0,
		number = 0,
		zhai = 0,
		index = nil,
	}
	print("lastShout clear")
	
	local function init( parent )
		--自己的骰，横向
		createSelfWay()		
		--o = createSelfWay()
		--RoomMatch._selfway = o
		if not RoomMatch._shout_btn then
			o = createButton(RoomMatch, RoomMatchConfig.shout_btn )
			o.onTouchEnd = showShoutPanel
			hide( {o} )
			RoomMatch._shout_btn = o
		end
		
		local function onRoomOpen( data )
			d(data)
			if data.code < 0 then
				print "error when open"
			end
		end
		local function doRoomOpen()
			cancelAct()
			doCommand( "room", "open", { ["id"] = matchData.id }, onRoomOpen)
			
			afterShoutOrOpen( 1 )
		end
		
		if not RoomMatch._open_btn then
			o = createButton(RoomMatch, RoomMatchConfig.open_btn )
			o.onTouchEnd = doRoomOpen
			hide({o})
			RoomMatch._open_btn = o
		end
	end

	local function doShake()
		doShakeCup(1,doMoveMyWay,matchData)		
	end
	
	local function calNextIndex()
		for i = 1, 4 do
			if data[ "uid" .. i ] then
				for j = i + 1, i + 3 do
					local k = j
					if k > 4 then
						k = k - 4
					end
					if data[ "uid" .. k ] then
						next_index[ tostring( uid2desk[ data[ "uid" .. i ].uid ] ) ] = uid2desk[ data[ "uid" .. k ].uid ]						
						break
					end
				end
			end
		end
	end
	
	local rock = RoomMatchConfig.rock
	playWav( rock.sound, rock.ms )
	
	next_index = {}
	calNextIndex()
	
	print( 'next_index')
	d(next_index)
	
	if data.way then	
		print( 'init begin' )
		init( RoomMatch )
		
		doShake()
		print('end dobeginmatch')
		--自己是非旁观状态时显示“喊”
		if matchData.next_uid == uid and not isWatch then
			local function onShowShout()
				if not RoomMatch._shoutselpanel then
					if IsAgent() == false then
						showShoutPanel()
					end
					viewShoutBtn()
				end
			end
			--延时2秒显示选号框
			f_delay_do(RoomMatch,onShowShout,nil,2)
		end
	else
		--旁观
		doShakeCup(1)
	end
	--延时2秒显示倒计时
	if uid2desk[matchData.next_uid] then	
	    f_delay_do(RoomMatch,doUserTimer,uid2desk[matchData.next_uid],2)
	end
end

--[[
2：有人离开
id：比赛id
uid：逃跑者uid
]]
local function onMsgUserLeave(data)
	if nPlayer >= 1 then
		nPlayer = nPlayer - 1		
	end
	doCommand( "room", "detail", {["id"]=room_id}, onRoomDetail)
end

--[[
3：有人喊骰
uid：喊者uid
next_uid：下一个该喊的uid
count：数量
number：数字
zhai：0为斋，1为非斋
]]
local function onMsgUserShout(data)
	print('some one shout')
	d(data)
	
	if uid2desk and not uid2desk[ data.uid ] then
		do return end
	end
		
	--如果是电脑帮自己喊，则隐藏喊的按钮
	--if data.uid == uid then
	   -- hideShoutPanel()
	--end
	hideShoutPanel()
	hideShoutBtn()	
	hideOpenBtn()	
	if lastShout.index then
		hideLastHHK()
	end

	lastShout = {
		count = tonumber(data.count),
		number = tonumber(data.number),
		zhai = tonumber(data.zhai),
		index = uid2desk[ data.uid ],
	}
	
	d(lastShout)
	
	if data.double then      --双倍设置
		showDouble(gold_multiple*2)
	end
	
	if RoomMatch._desk[ lastShout.index ] then
		RoomMatch._desk[ lastShout.index ].showHHK( lastShout )
	end
	if RoomMatch._desk[ uid2desk[ data.next_uid ] ] then	
		--RoomMatch._desk[ uid2desk[ data.next_uid ] ].startTimer()
	else
		print(data.next_uid)
		print(uid2desk[ data.next_uid ])
		--assert(RoomMatch._desk[ uid2desk[ data.next_uid ] ])
	end
	
	if data.next_uid == uid and not isWatch then
		viewShoutBtn()
	end
	
	if lastShout.index ~= 1 and not isWatch then
		viewOpenBtn()
	end
		
	--显示倒计时
	if uid2desk and uid2desk[ data.next_uid ] then
		doUserTimer(uid2desk[ data.next_uid ])
	end
	
end

--4：有人开(游戏结束)
local function onMsgUserOpen(data)
	d(data)
	
	--[[
	if uid2desk and not uid2desk[ data.uid ] then
		showMessage(RoomMatch,"有人开，返回")
		return
	end
	]]
		
	if not lastShout.index then
		return 
	end
	--销毁读秒
	doKillUserTimer()
	hideShoutPanel()
	hideLastHHK()
	hideOpenBtn()
	hideShoutBtn()	
	
	print(lastShout.index)
	d(next_index)
	d(uid2desk)
	print( 'stop', next_index[ tostring( lastShout.index ) ] )
	--stopTimer( next_index[ tostring( lastShout.index ) ] )
	lastShout.index = uid2desk[ data.uid ]
	
	--显示开
		--[[
	local function showOpen()
		viewOpen( uid2desk[ data.uid ] )
	end
		]]
	--几个几
	local function showCount()
		local c = RoomMatchConfig
		local result = nil
		if data.winner_uid == uid then
			result = 'win'
		elseif data.loser_uid == uid then
			result = 'lose'
		elseif isWatch == true then
		    if data.uid == data.winner_uid then
			    result = 'win'
			elseif data.uid == data.loser_uid then
			    result = 'lose'
			end
		end
		if result then
			local c = c[result]
			playWav( c.sound, c.ms )
			
		end
		
		if RoomMatch._selfway then
			clear( {RoomMatch._selfway} )
			RoomMatch._selfway = nil
		end
		
		--local delay = c.delay.count
		
		local delay = 0
		for uid, index in pairs( uid2desk ) do
			if data[ "uid_" .. uid ] then
				local desk = RoomMatch._desk[ index ]				
				createWay( desk, index, data[ "uid_" .. uid ].way,
				data.shout.zhai,lastShout.number,delay )
				delay = delay + RoomMatchConfig.delay.count --每个玩家有1.5秒显示统计骰子动画
				--view( o)
			end
		end
	end
	
	local function showWinLose()
		local pair = { winner_uid = 'win', loser_uid = 'lose' }
		for uid, type in pairs( pair ) do
			--RoomMatch._desk[ uid2desk[ data[ uid ] ] ].show( type )
		    doWinLoseMark(uid2desk[ data[ uid ]],type)
		end
	end
	
	local function showGold()
		local delay = RoomMatchConfig.delay.gold
		--金币动画
		local to = uid2desk[data["winner_uid"]]
		local from = uid2desk[data["loser_uid"]]
		doWinAction(from,to,10)
		
		for uid, index in pairs( uid2desk ) do
			if data[ "uid_" .. uid ] then
				local desk = RoomMatch._desk[ index ]
				
				if desk then
					local gold = data[ "uid_" .. uid ].gold
					local win = true
					if gold < 0 then
					    win = false
					    gold = 0 - gold
					end
					
					doShowScore(index,win,gold,2)
				end
			end
		end
	end
	
	--showOpen()	
	
	doUserOpen(lastShout.index)
		
	doShowWay(lastShout.number) --开几个几
	
	local delay = RoomMatchConfig.delay.open
	--delayCall( showCount, 'count', delay )
	f_delay_do(RoomMatch,showCount, 'count', delay)
		
	delay = delay + getPlayerCount()*RoomMatchConfig.delay.count
	--delayCall( showWinLose, 'showWinLose', delay )
	f_delay_do(RoomMatch,showWinLose, 'showWinLose', delay)
	
	delay = delay + RoomMatchConfig.delay.win
	--delayCall( showGold, 'showGold', delay )
	f_delay_do(RoomMatch,showGold, 'showGold', delay)
	
	delay = delay + RoomMatchConfig.delay.gold
	--delayCall( start, 'start', delay )
	f_delay_do(RoomMatch,start, 'start', delay)
	--有人开后，销毁时间读条
	if RoomMatch._UserTimer then
	    clear({RoomMatch._UserTimer})
	    RoomMatch._UserTimer = nil
	end
end
--[[
7：有人坐下
uid
]]
local function onMsgUserSit(data)
	nPlayer = nPlayer + 1
	if nPlayer == 2 and sitdown == true then
		hideWaittips()
	end
	doCommand( 'room', 'detail', { id = room_id }, onRoomDetail)
end

--8: 被踢出
local function onMsgUserWeed(data)
	local function doDelayExit()
		exit()
	end
	local op = {ms = 3000}
	showMessage( RoomMatch, data.content,op )
	f_delay_do(RoomMatch,doDelayExit,nil,3)
end

--9: 几秒后开始
local function onMsgGameReady(data)
	local delay = data.second - tBeginTimer		
	local function startCountdown()
		timeout_start = false
		--主动退出时直接退出
	    if isLeave then 
		   exit()
		   return 
		end
		if nPlayer > 1 then		
			hideWaittips()
			doCountDown(tBeginTimer,0)
		else
			showWaittips(1)
		end
	end
	if timeout_start == false then
		timeout_start = true
		f_delay_do(RoomMatch,startCountdown,nil,delay)
	end
		--doCountDown(tBeginTimer,delay)		
end

--10: 聊天
local function onMsgUserChat(data)	
	showMsg(RoomMatch,getChairID(data.uid),data.content)	
end

--11: 表情
local function onMsgUserFace(data)
	showFace(RoomMatch,getChairID(data.uid),tonumber(data.id))
end

--12: 彩蛋
local function onGameLuck(data)
	showLuckAction(RoomMatch,data.id,data.second)
	--showLuckAction(RoomMatch,1,13)
end

local answer = {
	[1] = onMsgMatchStart,
	[2] = onMsgUserLeave,
	[3] = onMsgUserShout,
	[4] = onMsgUserOpen,
--	[5] = onMsgMatchPreStart,
--	[6] = onMsgUserReady,
	[7] = onMsgUserSit,
	[8] = onMsgUserWeed,
	[9] = onMsgGameReady,
	[10] = onMsgUserChat,
	[11] = onMsgUserFace,
	[12] = onGameLuck,
}

local function onAnswer(data)
	Log(LIGHT_UI.db_fmt(data))
	asking = false
	if data.code < 0 then
		--print( data.code, data.data )
		Log("---------------[error  data.code = -1]------------------ ")
		return
	end
		
	if table.maxn(data.data) <= 0 then
		return  
	end
	
	local msg_data = data.data
	for _, data_info in ipairs(msg_data) do
		print( "receive msg:" .. data_info["type"] )
		d(data_info)		
		--凡说先屏蔽
		--[[
		if data_info.content ~= ''  and RoomMatch.setScroll then
			RoomMatch.setScroll( data_info.content )
		end
		]]
		data_info.type = tonumber(data_info.type)
		print(data_info.type)
		if answer[ data_info.type ] then
			answer[ data_info.type ](data_info.data)
		else
			print( '房间消息未实现，type: ' .. data_info.type )
		end
		--记录ID
		if tonumber(data_info.id) > curMsgID then
			curMsgID = tonumber(data_info.id)			
		end
	end
end	

--离开

--2,默认msg_id = 0
local function doAsk()
	if not asking then	
		--print(curMsgID)
		local tab=os.date("*t",time);
		local str = tab.hour..":"..tab.min..":"..tab.sec
		local function onAnswers(data)		
			data.MsgID = curMsgID
			data.curTime = str
			onAnswer(data)
		end					
		--doCommand( 'room', 'popup', { room_id = room_id, pool_id = curMsgID }, onAnswer)
		Log(curMsgID)
		doCommand( 'room', 'popup', { room_id = room_id, pool_id = curMsgID,time=str }, onAnswers)		
		asking = true
	end		
end
--初始化房间数据及回调
function init( data )
	room_id = data.room_id
	uid = GLOBAL.uid
	
	if data.back_func then
		back_func = data.back_func
	end
end	
--显示比赛房间
function showPanel(parent, x, y)
	if not RoomMatch then
		RoomMatch = createPanel(parent, x, y)
	else
		RoomMatch:setVisible(true)
	end
	--初始化成员变量
	Initialize()
		
	RoomMatch.IsWatch = IsWatch
	RoomMatch.GetRoomID = GetRoomID
	RoomMatch.doRefreshWealth = doRefreshWealth
		
	start()
		
	showActBtn(RoomMatch,true) --初始化托管按钮
	
	showChatBtn(RoomMatch) --初始化聊天按钮
	
	showFaceBtn(RoomMatch)  --初始化表情按钮	
	
	--凡说暂时隐藏
	--RoomMatch.setScroll( GLOBAL.name .. ' 进入了房间' )
		
	TIMER[ "doAsk" ] = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(doAsk, 1, false)
end

function closePanel()
	
	hideWaittips()
	
	quit = true
	--凡说暂时隐藏
	--RoomMatch.stopScroll()

	if not RoomMatch then
		return
	end
	RoomMatch:setVisible(false)
	
	if lastShout.index then
		--stopTimer( lastShout.index )
	end
	
	for id, _ in pairs( TIMER ) do
		clearTimer( id )
	end
	
	RoomMatch:removeFromParentAndCleanup( true )
	RoomMatch = nil
end

--等待开始
local function doGameReady(room)
	--showMessage(RoomMatch,"等待开始")
	--hideWaittips()
end

--游戏中
local function doGamePlaying(room)
	--showMessage(RoomMatch,"游戏中")
	--showWaittips(2)
end

--游戏结束
local function doGameOver(room)
	--showMessage(RoomMatch,"游戏结束")
	hideWaittips()
end

--初始化设置位置
function initSeat(desk)
    uid2desk = nil  
	local me_index = 1
	for i = 1, 4 do
		if desk[tostring(i)] then
			if desk[tostring(i)].uid == uid then
				me_index = i
				--isWatch = false
				break
			end
		end
	end
	
	if sitdown == false then
	    uid2desk = {}
	    for i = 1, 4 do
		    if desk[tostring(i)] then
			    if desk[tostring(i)].uid then
				    uid2desk[ desk[tostring(i)].uid ] = i
			    end
		    end
	    end
	else
	    uid2desk = { [uid] = 1 }
	    for i = 2, 4 do
		    j = me_index + i - 1
		    if j > 4 then
			    j = j - 4
		    end
		    if desk[tostring(j)] then
			    uid2desk[ desk[tostring(j)].uid ] = i
		    end
	    end
	end
	nPlayer = 0
	
	for i = 1, 4 do		
		local j = me_index + i - 1
		if j > 4 then
			j = j - 4
		end			
		RoomMatch[ "_desk" ][i] = createDesk( i, desk[tostring(j)] or nil )
		if desk[tostring(j)] then
			nPlayer = nPlayer + 1
		end
	end
		
	--doUserTimer(1,0)
end

--通过uid获取椅子号
function  getChairID(uid)
	return uid2desk[uid]
end

--同步游戏场景
function syncGameScene(room)
	if tonumber(room.status) == 1 then
		doGameReady(room)
	elseif tonumber(room.status) == 2 then
		doGamePlaying(room)
	elseif tonumber(room.status) == 3 then 
		doGameOver(room)
	end
	--[[
	if not RoomMatch._Way then
	    RoomMatch._Way = createNode(RoomMatch, point(0,0))
	end
	local index = 1
	local desk1 = RoomMatch._desk[ index ]
	createWay( RoomMatch, index, 22226,0,2,0 )
	]]
	--createWay( parent, index, way ,zhai,op_number,delay)
	--local desk3 = RoomMatch._desk[ 3 ]
    --createWay( desk, 3, 22226,2,0 )
	--[[
	doShowScore(false,1923045678)
    doWinAction(point(0,0),point(100,700),10)
	doShake(1)
    doCountDown(5)
	doShowStar(5)
	]]
	--doShowWayCount(5)
	--doCountDown(5)
	--doShowScore(1,true,100)
	--[[
	doWinLoseMark(1,'win')
	doWinLoseMark(2,'lose')
	doWinLoseMark(3,'win')
	doWinLoseMark(4,'win')
		]]
	--doUserOpen(1)
	--doUserTimer(1)
	--showWaittips(2)		
	--showMsg(RoomMatch,2,'测试一下')	
	--showLuckAction(RoomMatch,1)
	--[[local function fn_cb()
		showLuckAction(RoomMatch,1)
	end
	f_delay_do(RoomMatch,fn_cb,nil,3)--]]
	--f_delay_do(RoomMatch,onGameLuck,nil,2)
	--createShout( RoomMatch, 20, 500)	
	
end

