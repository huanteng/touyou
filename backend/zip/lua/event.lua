-- event

local EVENT_LAYOUT = Import("layout/event.lua")
local EventConfig = EVENT_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local EventPanel = nil

local function back_func()
	USER_INFO_PANEL.closeUserInfoPagePanel()
	showEventPanel( getRootNode(), 0, 0)
end

local function onEventAd( data )
	data = data.data
	local c, c2 = EventConfig, nil
	local o = nil
	
	c2 = c.ad1
	c2.res = data.url1
	o = createSprite( EventPanel, c2 )
	
	c2 = c.ad2
	c2.res = data.url2
	o = createSprite( EventPanel, c2 )	
	
	c2 = c.ad3
	c2.res = data.url3
	o = createSprite( EventPanel, c2 )	
end

local function createEventPanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent,  x, y )
	
	local function on_back(btn)
		closeEventPanel()
		MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '活动优惠' )
	
	return ret_panel
end

function showEventPanel(parent, x, y)
	if not EventPanel then
		EventPanel = createEventPanel(parent, x, y)
	else
		EventPanel:setVisible(true)
	end
	
	doCommand( 'event', 'ad', {}, onEventAd )
end

function closeEventPanel()
	--EventPanel:setVisible(false)
	clear({EventPanel})
	EventPanel = nil
end

