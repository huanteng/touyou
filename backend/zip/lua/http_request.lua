-------------------------------------------------------------- http
local http_queue = {
}

local http_global_idx = 0

local function http_response_cb(data, request_idx)
	local request_info = http_queue[request_idx]
	if request_info then
		request_info["response_cb"](data)
		http_queue[request_idx] = nil
	end
end

function proc_single_request()
    --先获取优先级高的请求
    for request_idx, request_info in pairs(http_queue) do
		if request_info["busy"] == false then
			if request_info["http"] and request_info["priority"] then		
					libcurl.lcurl_http_request(request_info["url"], request_idx, http_response_cb)
					request_info["busy"] = true
					return
			end
		end
	end
	--再遍历列表
	for request_idx, request_info in pairs(http_queue) do
		if request_info["busy"] == false then
			if request_info["http"] then		
					libcurl.lcurl_http_request(request_info["url"], request_idx, http_response_cb)
					request_info["busy"] = true
					break
			elseif request_info["download"] then				
					libcurl.lcurl_http_download(request_info["url"], request_info["file"], request_idx, http_response_cb)
					request_info["busy"] = true
					break						
			elseif request_info["upload"] then				
					libcurl.lcurl_http_upload(request_info["url"], request_info["param"], request_idx, http_response_cb)
					request_info["busy"] = true
					break											
			end
		end
	end
end

function http_request(url, response_cb,priority)
	http_global_idx = http_global_idx + 1

	local url_info = {}
	url_info["url"] = url
	url_info["response_cb"] = response_cb
	url_info["busy"] = false
	url_info["http"] = true
	url_info["priority"] = priority

	http_queue[http_global_idx] = url_info
end

function http_download(url,file,response_cb)
	http_global_idx = http_global_idx + 1

	local url_info = {}
	url_info["url"] = url
	url_info["response_cb"] = response_cb
	url_info["busy"] = false
	url_info["download"] = true
	url_info["file"] = file

	http_queue[http_global_idx] = url_info
end

function http_upload(url, param_info, response_cb)
	http_global_idx = http_global_idx + 1

	local url_info = {}
	url_info["url"] = url
	url_info["response_cb"] = response_cb
	url_info["busy"] = false
	url_info["upload"] = true
	url_info["param"] = param_info

	http_queue[http_global_idx] = url_info
end


