ROOM_LUCK_LAYOUT = Import("layout/room_luck.lua")
local RoomLuckConfig = ROOM_LUCK_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local isLucky = false
local egg_gold = nil
local showdelay = 20

local function getLuck(_root)
	if isLucky == true then		
		local function onTimeOut()
			clear({_root.Luck})
			_root.Luck = nil
		end
		
		local function onSend()			
			--if data.code > 0 and _root.Luck then
				playWav(RoomLuckConfig["luck_sound"].sound,RoomLuckConfig["luck_sound"].ms)
				--刷新积分
				_root:doRefreshWealth()
				
				_root.Luck.btn:setVisible(false)
				_root.Luck.btnbg:setVisible(false)
				
				local a = createSprite(_root.Luck,RoomLuckConfig["luck_btn"])
				a_play(a:getSprite(),RoomLuckConfig["action_luck"],true)
				local l = createLabel(_root.Luck,RoomLuckConfig.luck_text)
				--l:setString(data.data)
				l:setString(tostring(egg_gold)..'金币')
				local x,y = l:getPosition();
				a_move(l:getCOObj(),0,RoomLuckConfig.luck_text_move.speed,ccp(x,y +RoomLuckConfig.luck_text_move.height ))
				f_delay_do(_root,onTimeOut,nil,RoomLuckConfig.luck_showtime.time)				
			--end
		end
		isLucky = false
		tcp_award()
		f_delay_do(_root,onSend,nil,1)
		--doCommand('room', 'egg', { id = tonumber(egg_id) }, onSend)
		--onSend({code = 1,data = '奖励300金币'})
	end
end

local function showLuckBtn(_root,showdelay)
	if _root then	
		local function onClick()			
			getLuck(_root)
		end
		if 	not _root.Luck.btn then	
			_root.Luck.btnbg = createSprite(_root.Luck,RoomLuckConfig.luck_btnbg)		
			_root.Luck.btn = createButton(_root.Luck,RoomLuckConfig.luck_btn,onClick)	
			
		end	
		_root.Luck.btn:setVisible(true)
		_root.Luck.btnbg:setVisible(true)
		playWav( RoomLuckConfig.luck_show.sound, RoomLuckConfig.luck_show.ms )
		
		local function hide()
			_root.Luck.btn:setVisible(false)
			_root.Luck.btnbg:setVisible(false)
		end
		local delay = tonumber(RoomLuckConfig.luck_delay.time)
		if tonumber(showdelay) > delay then		
			a_fadeout(_root.Luck.btnbg:getSprite(),
			showdelay - delay,delay,hide)						
		end
	end			
end

function showLuckAction(_root,gold)
	if _root then
		preLoadWav(RoomLuckConfig["luck_sound"].sound)
		preLoadWav(RoomLuckConfig.luck_show.sound)
		isLucky = true
		egg_gold = gold
		if not _root.Luck then
			local c = {x = 0,y = 0}
			local order = _root:getCOObj():getChildrenCount()
			_root.Luck = createNode(_root,c,nil,order)		
		end	
		c = RoomLuckConfig
		showLuckBtn(_root,showdelay)		
	end
end



