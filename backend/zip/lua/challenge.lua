-- challenge page

CHA_LAYOUT = Import("layout/challenge.lua")
local ChaPageConfig = CHA_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local challengePage = nil

local girl_list = {}
local cur_girl_page = 0
local max_girl_page = 5
local boy_list = {}
local cur_boy_page = 0
local max_boy_page = 5

local click_delta = 10
local freshing = true

function setMsgDelegate(msg_obj, move_obj, click_func)
	local startx, starty
	local endx, endy
	local is_stop = false
	local function onTouchBegan(btn, x, y)
		if move_obj:isAutoMove() then
			is_stop = true
		end
		startx, starty = x, y
		move_obj:onTouchBegan(btn:convertToWorldSpace(x, y))
	end
	local function onTouchMove(btn, x, y)
		move_obj:onTouchMove(btn:convertToWorldSpace(x, y))
	end
	local function onTouchEnd(btn, x, y)
		endx, endy = x, y
		move_obj:onTouchEnd(btn:convertToWorldSpace(x, y))

		if ((math.abs(startx - endx) + math.abs(starty - endy)) <= click_delta) and (not is_stop) then
			click_func(btn, x, y)
		end
		is_stop = false
	end
	msg_obj.onTouchBegan = onTouchBegan
	msg_obj.onTouchMove = onTouchMove
	msg_obj.onTouchEnd = onTouchEnd
end

local function back_func()
	PLAYER_INFO_PANEL.closePlayerInfoPagePanel()
	--showChallengePagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	if PANEL_CONTAINER.closeChildPanel( nil, 5 ) then
		PANEL_CONTAINER.addChild( showChallengePagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
	end
end

local function createBigItemByUserInfo( info )	
	local c, c2 = ChaPageConfig, nil
	
	local config = ChaPageConfig["big_item_config"]
	local nodeObj = LIGHT_UI.clsNode:New(nil, 0, 0)
	nodeObj._user_id = info["id"]
	
	local function newSection( section, key, value )
		return replaceConfigValue( c, section, key, value )
	end

	local msg_obj = LIGHT_UI.clsSimpleButton:New(nodeObj, 6, -8, "portrait.png", "portrait.png")  --����Ĭ�ϵ�ͼλ�õ���
	msg_obj:setAnchorPoint(0, 1)
	
	local function on_click()
		GAMEHALL_PANEL.closeGameHallPanel()
		closeChallengePagePanel()
		PLAYER_INFO_PANEL.showByUserId(nodeObj._user_id)
		PLAYER_INFO_PANEL.init( { back_func = back_func } )
	end

	setMsgDelegate(msg_obj, challengePage._move_big_page._move_big_grp, on_click)

	createSprite( nodeObj, newSection( 'logo1', 'res', info.logo ) )

	if tonumber(info["is_real"]) == 1 then
		local video_pic = LIGHT_UI.clsSprite:New(nodeObj, config["real_x"], config["real_y"], "vedioauth_pic.png")
		video_pic:setAnchorPoint(0, 1)
	end

	if info["vip_id"] ~= "0" then
		local vip_logo = string.format( 'vip%i.png', info["vip_id"] )
		local vip_pic = LIGHT_UI.clsSprite:New(nodeObj, config["vip_x"], config["vip_y"], vip_logo )
		vip_pic:setAnchorPoint(0, 1)
	end

	local function getContentSize(node)
		return {["width"] = 140, ["height"] = 140,}
	end
	nodeObj.getContentSize = getContentSize
	return nodeObj
end

local function createSmallItemByUserInfo(info)
	local c, c2 = ChaPageConfig, nil
	
	local function newSection( section, key, value )
		return replaceConfigValue( c, section, key, value )
	end
	
	local config = ChaPageConfig["small_item_config"]
	local nodeObj = LIGHT_UI.clsNode:New(nil, 0, 0)
	nodeObj._user_id = info["id"]

	local msg_obj = LIGHT_UI.clsFrameButton:New( nodeObj, 0, 0, 0, 0, "tiny_bg_pixel.png", "tiny_yellow_pixel.png")
	msg_obj:hideLine()

	local function on_click()
		GAMEHALL_PANEL.closeGameHallPanel()
		closeChallengePagePanel()
		PLAYER_INFO_PANEL.showByUserId(nodeObj._user_id)
		PLAYER_INFO_PANEL.init( { back_func = back_func } )
	end

	setMsgDelegate(msg_obj, challengePage._move_small_page._move_small_grp, on_click)

	createSprite( nodeObj, newSection( 'logo2', 'res', info.logo ) )
	
	local name_obj = LIGHT_UI.clsLabel:New(nodeObj, config["name_x"], config["name_y"], info["name"], GLOBAL_FONT, 22)
	name_obj:setAnchorPoint(0, 1)
	name_obj:setTextColor(102, 102, 102)
	local sex_pic_res = "man.png"
	if tonumber(info["sex"]) == 1 then sex_pic_res = "woman.png" end
	local sex_pic = LIGHT_UI.clsSprite:New(nodeObj, config["sex_pic_x"], config["sex_pic_y"], sex_pic_res)
	sex_pic:setAnchorPoint(0, 1)
	local address = LIGHT_UI.clsLabel:New(nodeObj, config["address_x"], config["address_y"], info["city"], GLOBAL_FONT, 20)
	address:setAnchorPoint(0, 1)
	address:setTextColor(102, 102, 102)
	if tonumber(info["is_real"]) == 1 then
		local real_sprite = "vedioauth_pic.png"
		local video_pic = LIGHT_UI.clsSprite:New(nodeObj, config["real_x"], config["real_y"], real_sprite)
		video_pic:setAnchorPoint(0, 1)
	end

	if info["vip_id"] ~= "0" then
		local temp_size = name_obj:getContentSize()
		
		local vip_logo = string.format( 'vip%i.png', info["vip_id"] )
		local vip_pic = LIGHT_UI.clsSprite:New(nodeObj, config["name_x"] + temp_size.width + config["vip_x"], config["name_y"], vip_logo)
		vip_pic:setAnchorPoint(0, 1)
	end

	local mood_txt = LIGHT_UI.clsMultiLabel:New(nodeObj, config["mood_x"], config["mood_y"], info["mood"], GLOBAL_FONT, 22, 364, 1)
	mood_txt:setTextColor(102, 102, 102)
	local size = { width = 0, height = 0, }
	if info["mood"] and string.len(info["mood"]) > 0 then
		size = mood_txt:getContentSize()
	end

	if size.height == 0 then size.height = config["min_height_split"] end

	local divider_pic = LIGHT_UI.clsSprite:New(nodeObj, config["divider_x"], config["divider_y"], "divider.png")
	divider_pic:setAnchorPoint(0, 0)

	local function getContentSize(node)
		return {["width"] = 480, ["height"] = size.height + config["min_height_split2"],}
	end

	nodeObj.getContentSize = getContentSize
	msg_obj:setPixelScale( 480, ( size.height + config["min_height_split2"] ) * 10 )

	return nodeObj
end

local itemCount = 1
local function on_get_girl_list(data)
	if not challengePage then
		return
	end
	local real_data = data.data
	if freshing and freshing == true then
	--if cur_girl_page == 1 then	
		girl_list = {}
		challengePage._move_big_page._girl_page:clearAllItem()
		challengePage._move_small_page._girl_page:clearAllItem()
		resetBigGirlListPosition()
		resetSmallGirlListPosition()
	end
	itemCount = 1
	for _, user_info in ipairs(real_data) do
		local ret_panel = createBigItemByUserInfo(user_info)
		local ret_panel2 = createSmallItemByUserInfo(user_info)
		if freshing and freshing == true then
			freshing = false
			if itemCount == 1 then
				ret_panel.tip = createLabel( ret_panel, {x = 100, y = 30, ax = 0, ay = 0.5, text = '等侯刷新', css='c1'} )
				xCenter( ret_panel.tip )
				local locX, locY = ret_panel.tip:getPosition()
				ret_panel.tip:setPosition( locX + 10, locY )
				locX, locY = ret_panel.tip:getPosition()
				ret_panel.arrow = createSprite( ret_panel, {x = locX - 20, y = 30, res = 'arrow_down.png'} )
				ret_panel.loading = createSprite( ret_panel, {x = locX - 20, y = 30, res = 'white_dot.png'})
				ret_panel.loading:setVisible( false )
				ret_panel2.tip = createLabel( ret_panel2, {x = 100, y = 30, ax = 0, ay = 0.5, text = '等侯刷新', css='c1'} )
				xCenter( ret_panel2.tip )
				local locX, locY = ret_panel2.tip:getPosition()
				ret_panel2.tip:setPosition( locX + 10, locY )
				locX, locY = ret_panel2.tip:getPosition()
				ret_panel2.arrow = createSprite( ret_panel2, {x = locX - 20, y = 30, res = 'arrow_down.png'} )
				ret_panel2.loading = createSprite( ret_panel2, {x = locX - 20, y = 30, res = 'white_dot.png'})
				ret_panel2.loading:setVisible( false )
			end
		end
		--table.insert(girl_list, user_info)
		challengePage._move_big_page._girl_page:append_item(ret_panel)
		challengePage._move_small_page._girl_page:append_item(ret_panel2)
		itemCount = itemCount + 1
	end

	challengePage._move_big_page._girl_page:refresh_view()
	challengePage._move_small_page._girl_page:refresh_view()
	
	
end	

local function on_get_boy_list(data)
	if freshing and freshing == true then
	--if cur_boy_page == 1 and boy_page then
		boy_list = {}
		challengePage._move_big_page._boy_page:clearAllItem()
		challengePage._move_small_page._boy_page:clearAllItem()			
	end
	local real_data = data.data
	itemCount = 1
	for _, user_info in ipairs(real_data) do
		local ret_panel = createBigItemByUserInfo(user_info)
		local ret_panel2 = createSmallItemByUserInfo(user_info)
		if freshing and freshing == true then
			freshing = false
			if itemCount == 1 then
				ret_panel.tip = createLabel( ret_panel, {x = 100, y = 30, ax = 0, ay = 0.5, text = '等侯刷新', css='c1'} )
				xCenter( ret_panel.tip )
				local locX, locY = ret_panel.tip:getPosition()
				ret_panel.tip:setPosition( locX + 10, locY )
				locX, locY = ret_panel.tip:getPosition()
				ret_panel.arrow = createSprite( ret_panel, {x = locX - 20, y = 30, res = 'arrow_down.png'} )
				ret_panel.loading = createSprite( ret_panel, {x = locX - 20, y = 30, res = 'white_dot.png'})
				ret_panel.loading:setVisible( false )
				ret_panel2.tip = createLabel( ret_panel2, {x = 100, y = 30, ax = 0, ay = 0.5, text = '等侯刷新', css='c1'} )
				xCenter( ret_panel2.tip )
				local locX, locY = ret_panel2.tip:getPosition()
				ret_panel2.tip:setPosition( locX + 10, locY )
				locX, locY = ret_panel2.tip:getPosition()
				ret_panel2.arrow = createSprite( ret_panel2, {x = locX - 20, y = 30, res = 'arrow_down.png'} )
				ret_panel2.loading = createSprite( ret_panel2, {x = locX - 20, y = 30, res = 'white_dot.png'})
				ret_panel2.loading:setVisible( false )
			end
		end
		--table.insert(boy_list, user_info)
		challengePage._move_big_page._boy_page:append_item(ret_panel)
		challengePage._move_small_page._boy_page:append_item(ret_panel2)
		itemCount = itemCount + 1
	end

	challengePage._move_big_page._boy_page:refresh_view()
	challengePage._move_small_page._boy_page:refresh_view()	
	resetBigBoyListPosition()
	resetSmallBoyListPosition()
end

local function request_girl_list(page)
	if tonumber(page) == 1 then
		doCommand( 'challenge', 'list_user', { page = page, sex = 1 }, on_get_girl_list )
		--doCommand( 'challenge', 'list_user', { page = page, sex = 1 }, on_get_girl_list,300,{['loading'] = 0} )
	else
		doCommand( 'challenge', 'list_user', { page = page, sex = 1 }, on_get_girl_list,600,{['loading'] = 0} )
	end
end

local function request_boy_list(page)
	if tonumber(page) == 1 then
		doCommand( 'challenge', 'list_user', { page = page, sex = 0 }, on_get_boy_list,300,{['loading'] = 0} )
	else
		doCommand( 'challenge', 'list_user', { page = page, sex = 0 }, on_get_boy_list,600,{['loading'] = 0} )
	end
end

--[[
[1] = {
	frendship = 0,
	id = *,
	profit = 188,
	is_real = 0,
	vip_id = 0,
	city = "",
	sex = 1,
	logo = "http ***",
	name = "**",
	mood = "**",
	province = "**",
}
--]]

local big_desc_info = {
	item_width = 150,
	item_height = 150,
	column_cnt = 3,
	x_space = 7,
	y_space = 7,
}

local small_desc_info = {
	item_width = 470,
	item_height = 160,
	column_cnt = 1,
	x_space = 10,
	y_space = 0,
}

function onBigGirlTouchEnd()
	cur_girl_page = 1
	freshing = true
	request_girl_list(cur_girl_page)
	showLoading(challengePage._move_big_page)	
end

function resetBigGirlListPosition()
	local config = ChaPageConfig["move_page_config"]
	challengePage._move_big_page._girl_page:setPosition(0, config["inner_y"])
end

function onBigBoyTouchEnd()
	cur_boy_page = 1
	freshing = true
	request_boy_list(cur_boy_page)	
end

function resetBigBoyListPosition()
	local config = ChaPageConfig["move_page_config"]
	challengePage._move_big_page._boy_page:setPosition(0, config["inner_y"])
end

local function createMoveBigPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = ChaPageConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_big_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config["view_height"])
	ret_panel._move_big_grp:setVMovable(true)
	
	ret_panel._move_big_grp.touchEnd = onBigGirlTouchEnd
	ret_panel._move_big_grp.resetListPos = resetBigGirlListPosition
	ret_panel._move_big_grp.tipHeight = 700

	local girl_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, big_desc_info)
	girl_page:refresh_view()

	local function onGirlOverTop(layout)
		
	end
	local function onGirlOverBottom(layout)
		if cur_girl_page < max_girl_page then
			cur_girl_page = cur_girl_page + 1
			request_girl_list(cur_girl_page)
			showLoading(ret_panel)
		end
	end

	girl_page.onHMoveOverTop = onGirlOverTop
	girl_page.onHMoveOverBottom = onGirlOverBottom

	local boy_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, big_desc_info)
	boy_page:refresh_view()

	local function onBoyOverTop(layout)		
		
	end
	local function onBoyOverBottom(layout)
		if cur_boy_page < max_boy_page then
			cur_boy_page = cur_boy_page + 1
			request_boy_list(cur_boy_page)
		end
	end

	boy_page.onHMoveOverTop = onBoyOverTop
	boy_page.onHMoveOverBottom = onBoyOverBottom

	girl_page:setPosition(0, config["inner_y"])
	ret_panel._move_big_grp:appendItem(girl_page)
	boy_page:setPosition(0, config["inner_y"])
	ret_panel._move_big_grp:appendItem(boy_page)
	ret_panel._move_big_grp:selectPage(1)

	ret_panel._girl_page = girl_page
	ret_panel._boy_page = boy_page 

	local function get_y_limit(obj)
		return config["inner_y"] - 60
	end

	girl_page.getYLimit = get_y_limit
	boy_page.getYLimit = get_y_limit
	
	return ret_panel
end

function onSmallGirlTouchEnd()
	cur_girl_page = 1
	freshing = true
	request_girl_list(cur_girl_page)		
end

function resetSmallGirlListPosition()
	local config = ChaPageConfig["move_page_config"]
	challengePage._move_small_page._girl_page:setPosition(0, config["inner_y"])
end

function onSmallBoyTouchEnd()
	cur_boy_page = 1
	freshing = true
	request_boy_list(cur_boy_page)
	showLoading(challengePage._move_small_page)	
end

function resetSmallBoyListPosition()
	local config = ChaPageConfig["move_page_config"]
	challengePage._move_small_page._boy_page:setPosition(0, config["inner_y"])
end

local function createMoveSmallPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = ChaPageConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_small_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config["view_height"])
	ret_panel._move_small_grp:setVMovable(true)
	
	ret_panel._move_small_grp.touchEnd = onSmallGirlTouchEnd
	ret_panel._move_small_grp.resetListPos = resetSmallGirlListPosition
	ret_panel._move_small_grp.tipHeight = 700

	local girl_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	girl_page._use_dynamic_size = true
	girl_page:refresh_view()

	local function onGirlOverTop(layout)
			
	end
	local function onGirlOverBottom(layout)
		if cur_girl_page < max_girl_page then
			cur_girl_page = cur_girl_page + 1
			request_girl_list(cur_girl_page)			
		end
	end

	girl_page.onHMoveOverTop = onGirlOverTop
	girl_page.onHMoveOverBottom = onGirlOverBottom

	local boy_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	boy_page._use_dynamic_size = true
	boy_page:refresh_view()

	local function onBoyOverTop(layout)
		
	end
	local function onBoyOverBottom(layout)
		if cur_boy_page < max_boy_page then
			cur_boy_page = cur_boy_page + 1
			request_boy_list(cur_boy_page)
			showLoading(ret_panel)
		end
	end

	boy_page.onHMoveOverTop = onBoyOverTop
	boy_page.onHMoveOverBottom = onBoyOverBottom

	girl_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(girl_page)
	boy_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(boy_page)
	ret_panel._move_small_grp:selectPage(1)

	ret_panel._girl_page = girl_page
	ret_panel._boy_page = boy_page 

	local function get_y_limit(obj)
		return config["inner_y"] - 60
	end

	girl_page.getYLimit = get_y_limit
	boy_page.getYLimit = get_y_limit
	
	return ret_panel
end

local function createChallengePagePanel(parent, x, y, isBase) 
	local ret_panel
	if isBase and isBase == true then
		ret_panel = createBasePanel( parent, x, y )
	else
		ret_panel = createContainerChildPanel( parent, x, y )
	end

	local c, c2 = ChaPageConfig, nil
	local o = nil

	local function on_back(btn)
		closeChallengePagePanel()
		--MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		local FRIEND_PAGE = Import("lua/friend.lua")
		if PANEL_CONTAINER.closeChildPanel( nil, 1) then
			PANEL_CONTAINER.addChild( FRIEND_PAGE.showFriendPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
		end	
	end

	if isBase and isBase == true then
		ret_panel._onBack = on_back
	    ret_panel._head_text:setString( "偶遇" )
	end
	--ret_panel._onBack = on_back
	--ret_panel._head_text:setString( "偶遇" )
	
	local function onClickChangeStyle(obj, x, y)
		local big_is_visible = not ret_panel._move_big_page:isVisible()
		local small_is_visible = not ret_panel._move_small_page:isVisible()
		ret_panel._move_big_page:setVisible(big_is_visible)
		ret_panel._move_small_page:setVisible(small_is_visible)
		ret_panel._move_big_page._move_big_grp:selectPage(1)
		ret_panel._move_small_page._move_small_grp:selectPage(1)
	end

	config = ChaPageConfig["style_btn_config"]
	ret_panel._change_style_btn = LIGHT_UI.clsCheckButton:New(ret_panel, config["x"], 
	config["y"], config["grid_res"], config["list_res"])
	ret_panel._change_style_btn.onTouchEnd = onClickChangeStyle
	--createCheckButton(ret_panel,config,onClickChangeStyle)

	config = ChaPageConfig["move_page_config"]
	ret_panel._move_big_page = createMoveBigPage(ret_panel, config["x"], config["y"]) 
	ret_panel._move_small_page = createMoveSmallPage(ret_panel, config["x"], config["y"]) 
	ret_panel._move_small_page:setVisible(false)

	local function on_sel_big_page(move_obj)
		freshing = true
		ret_panel._move_hint:selItem(move_obj:getCurPage())
		if move_obj:getCurPage() == 1 and table.maxn(girl_list) <= 0 then
			challengePage._move_big_page._move_big_grp.touchEnd = onBigGirlTouchEnd
			challengePage._move_big_page._move_big_grp.resetListPos = resetBigGirlListPosition
			cur_girl_page = 1
			request_girl_list(cur_girl_page)
		end
		if move_obj:getCurPage() == 2 and table.maxn(boy_list) <= 0 then
			challengePage._move_big_page._move_big_grp.touchEnd = onBigBoyTouchEnd
			challengePage._move_big_page._move_big_grp.resetListPos = resetBigBoyListPosition
			cur_boy_page = 1
			request_boy_list(cur_boy_page)
		end
	end

	local function on_sel_small_page(move_obj)
		freshing = true
		ret_panel._move_hint:selItem(move_obj:getCurPage())
		if move_obj:getCurPage() == 1 and table.maxn(girl_list) <= 0 then
			challengePage._move_small_page._move_small_grp.touchEnd = onSmallGirlTouchEnd
			challengePage._move_small_page._move_small_grp.resetListPos = resetSmallGirlListPosition
			cur_girl_page = 1
			request_girl_list(cur_girl_page)
		end
		if move_obj:getCurPage() == 2 and table.maxn(boy_list) <= 0 then
			challengePage._move_small_page._move_small_grp.touchEnd = onSmallBoyTouchEnd
			challengePage._move_small_page._move_small_grp.resetListPos = resetSmallBoyListPosition
			cur_boy_page = 1
			request_boy_list(cur_boy_page)
		end
	end

	ret_panel._move_big_page._move_big_grp.onSelPage = on_sel_big_page
	ret_panel._move_small_page._move_small_grp.onSelPage = on_sel_small_page

	local item_list = {
		[1] = {
			["txt"] = "美女",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
		[2] = {
			["txt"] = "帅哥",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
	}

	config = ChaPageConfig["move_hint_config"]
	local total_width = 480
	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, config["x"], config["y"], 
	"tab_bg_image.png", item_list, total_width, config["res"], "tiny_black_pixel.png")
	local bg = ret_panel._move_hint:getBGObj()
	bg:setScaleX(total_width)
	ret_panel._move_hint:selItem(1)

	cur_girl_page = 1
	--request_girl_list(cur_girl_page)

	local function on_click( obj, idx )
		challengePage._move_hint:selItem( idx )		

		if challengePage._move_big_page:isVisible() then
			challengePage._move_big_page._move_big_grp:selectPage( idx )
		else
			challengePage._move_small_page._move_small_grp:selectPage( idx )
		end
	end

	ret_panel._move_hint.onClick = on_click
	
	ret_panel.closePanel = closeChallengePagePanel

	return ret_panel
end

function showChallengePagePanel(parent, x, y, isBase )
	freshing = true
	if not challengePage then
		challengePage = createChallengePagePanel(parent, x, y, isBase)
		request_girl_list(cur_girl_page)
	else
		challengePage:setVisible(true)
	end
	return challengePage
end

function closeChallengePagePanel()
	--challengePage:setVisible(false)
	clear({challengePage})
	challengePage = nil
end

