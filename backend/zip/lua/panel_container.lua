PANELCONTAINER_LAYOUT = Import("layout/panel_container.lua")
local panelContainerConfig = PANELCONTAINER_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
ret_panel = nil
local moveTimer = nil
local layer_pos_x = panelContainerConfig.panel_container_property.layer_pos_x
local layer_pos_y = 0
local _last_x = nil
local _direction = 1
local speed = panelContainerConfig.panel_container_property.speed
local isMoving = false
local deppendState = true
local running = false
local first_x = nil

local drag_confirm_distance = 50		--拖动确认距离，超过这距离被认为是拖动了
local winSize = CCDirector:sharedDirector():getWinSize()
local distance = winSize.width / 2
--容器向右的最大距离
local right_limit_distance = winSize.width - panelContainerConfig.panel_container_property.right_difference

--能否切换容器页面
local canSwitch = true		--true 能切换 false 不能切换

curLevel = 1

function setCanSwitch( value )
	canSwitch = value
end

function getCanSwitch()
	return canSwitch
end

function getRunning()
	return running
end

local function changeDirectionByClick()
	if deppendState == true then
		_direction = 1						
	else
		_direction = -1				
	end
end

local function movePanelContainer()
	if isMoving == false then
		layer_pos_x = layer_pos_x + speed * _direction
		if layer_pos_x < 0 then
			layer_pos_x = 0
			ret_panel.bg:setPosition( 0, layer_pos_y )			
			if moveTimer then
				--CCMessageBox("here5",'')	
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(moveTimer)
				moveTimer = nil
				deppendState = true
				if ret_panel.mask then
					ret_panel.mask:setTouchEnabled( false )
				end
				running = false				
			end
			return
		elseif layer_pos_x  > right_limit_distance then			
			NEW_GAME_HALL.updateGoldText()
			NEW_GAME_HALL.updateMessageNum( getMessageNum() )
			NEW_GAME_HALL.setLogo()
			layer_pos_x = right_limit_distance
			ret_panel.bg:setPosition( right_limit_distance, layer_pos_y )
			canSwitch = true
			if moveTimer then
				--CCMessageBox("here6",'')
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(moveTimer)
				moveTimer = nil
				deppendState = false
				if ret_panel.mask then
					ret_panel.mask:setTouchEnabled( true )
				end
				--ret_panel.bg:set_msg_rect(0, 0, 480, 800)
				running = false
			end
			return
		end
		ret_panel.bg:setPosition( layer_pos_x, layer_pos_y )
	end
end

local function layerClickButtonBeganHandler( t, x, y )
	--ret_panel.btn:setClickStatus()
	if running == false then
		running = true
		if not first_x then
			first_x = x
		end
		changeDirectionByClick()
		
	end	
end


local function clickButtonBeganHandler( t, x, y )
	ret_panel.btn:setClickStatus()
	if running == false then
		running = true		
		if not first_x then
			first_x = x
		end
		changeDirectionByClick()
		
	end	
end

local function createMoveTimer( t, x, y )
	if deppendState == false then
		if x < right_limit_distance then
			return
		end	
	end
	if running == false then
		if deppendState == false then
			if x >= right_limit_distance then
				running = true					
			end				
		end
		if not first_x then
			first_x = x
		end
		changeDirectionByClick()
		
	end			
end

local function changeDirection( lastX, x )
	--print( distance )
	--if lastX < x then	
	if x >= distance then
		_direction = 1
	else
		_direction = -1
	end
	_last_x = x
end	

local function moveHandler( t, x, y )
	--CCMessageBox("here4",'')	
	if first_x then
		if layer_pos_x + x - first_x < 0 then
			return
		elseif x  > right_limit_distance then
			return
		end
	end		
	if deppendState == true then
		running = true
	else
		if x >= right_limit_distance then
			running = true
		end
	end
	isMoving = true
	if not _last_x then
		_last_x = x
		return
	else		
		--changeDirection( _last_x, x )
	end
	
	if first_x then
		ret_panel.bg:setPosition( layer_pos_x + x - first_x, layer_pos_y )
	end		
end

local function stopHandler( t, x, y )
	if first_x then
		layer_pos_x = layer_pos_x + x - first_x
		--[[if x - first_x < drag_confirm_distance then		
			running = false
			isMoving = false	
			first_x = nil
			return
		end--]]
	end
	--CCMessageBox("here3",'')
	changeDirection( 0, layer_pos_x )		--此时第一个参数已经没用了，暂时传个0来顶替
	isMoving = false	
	first_x = nil	
	
	if running == true then	
		if not moveTimer then
			if ret_panel.mask then
			ret_panel.mask:setTouchEnabled( true )
		end
			moveTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( movePanelContainer, 0.1, false )						
		end			
	end
			
end

local function clickButtonStopHandler( t, x, y )
	ret_panel.btn:setNormalStatus()
	if first_x then
		layer_pos_x = layer_pos_x + x - first_x
	end
	isMoving = false	
	first_x = nil
	changeDirectionByClick()
	if not moveTimer then
		--CCMessageBox("here",'')	
		if ret_panel.mask then
			ret_panel.mask:setTouchEnabled( true )
		end
		moveTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( movePanelContainer, 0.1, false )
	else
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(moveTimer)
		moveTimer = nil
		isMoving = false
		changeDirectionByClick()
		if ret_panel.mask then
			ret_panel.mask:setTouchEnabled( true )
		end
		moveTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( movePanelContainer, 0.1, false )
	end
end

local function layerClickButtonStopHandler( t, x, y )
	ret_panel.btn:setNormalStatus()
	if first_x then
		layer_pos_x = layer_pos_x + x - first_x
	end
	isMoving = false	
	first_x = nil
	if layer_pos_x == right_limit_distance then
		changeDirectionByClick()
	else
		changeDirection( 0, x )
	end
	if not moveTimer then
		--CCMessageBox("here",'')	
		moveTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( movePanelContainer, 0.1, false )
	end
end

function clickButton()
	clickButtonBeganHandler()
	clickButtonStopHandler()
end

local pageId = nil
function closeChildPanel( childPanel, id )
	if ret_panel.mask then
		ret_panel.mask:setTouchEnabled( false )
		ret_panel.mask = nil
	end
	if not childPanel then
		if ret_panel.child then	
			ret_panel.child:closePanel()					
		end	
		pageId = id	
		return true
	elseif pageId ~= id then
		if ret_panel.child then	
			ret_panel.child:closePanel()
			pageId = id	
			return true			
		end		
	end				
	pageId = id	
	return false
end

local function setTitleString()
	if pageId == 1 then
		ret_panel.title_txt:setString( "圈子" )
	elseif pageId == 2 then
		ret_panel.title_txt:setString( "说两句" )
	elseif pageId == 3 then
		ret_panel.title_txt:setString( "动态" )
	elseif pageId == 4 then
		ret_panel.title_txt:setString( "看相打分" )
	elseif pageId == 5 then		
		ret_panel.title_txt:setString( "偶遇" )	
	elseif pageId == 6 then		
		ret_panel.title_txt:setString( "商城" )
	elseif pageId == 7 then		
		ret_panel.title_txt:setString( "获得金币" )
	elseif pageId == 8 then
		ret_panel.title_txt:setString( "幸运转盘" )
	elseif pageId == 9 then
		ret_panel.title_txt:setString( "骰友" )
	elseif pageId == 10 then
		ret_panel.title_txt:setString( "" )
	elseif pageId == 11 then
		ret_panel.title_txt:setString( "排行榜" )
	elseif pageId == 12 then
		ret_panel.title_txt:setString( "摇骰子" )
	elseif pageId == 13 then
		ret_panel.title_txt:setString( "消息" )
	elseif pageId == 14 then
		ret_panel.title_txt:setString( "帮助" )
	elseif pageId == 15 then
		ret_panel.title_txt:setString( "设置" )
	elseif pageId == 16 then
		ret_panel.title_txt:setString( "我的资料" )
	else
		ret_panel.title_txt:setString( "骰友" )
	end
end

function expandContainer()
	ret_panel.bg:setPosition( 0, layer_pos_y )
end

function addChild( childPanel )
	ret_panel.child = childPanel
	ret_panel.mask = LIGHT_UI.clsClipLayer:New( ret_panel.bg, 0, 0 )
	ret_panel.mask:set_msg_rect(0, 0, panelContainerConfig.panel_container_property.screen_width, panelContainerConfig.panel_container_property.screen_height)	
	ret_panel.mask.onTouchBegan = layerClickButtonBeganHandler
	ret_panel.mask.onTouchMove = moveHandler
	ret_panel.mask.onTouchEnd = layerClickButtonStopHandler
	if not moveTimer then
		if deppendState == true then
			ret_panel.mask:setTouchEnabled( false )
		else
			ret_panel.mask:setTouchEnabled( true )
		end
	else
		ret_panel.mask:setTouchEnabled( true )
	end		
	setTitleString()
	xCenter( ret_panel.title_txt )
	--[[if ret_panel.btn then
		ret_panel.btn = nil
	end--]]
	
end

local msg_style = {}

----消除添加消息后删除的显示数字和红点
function clearMsgPicNum( i )
	if msg_style then
		if msg_style.pic then 
			if not i then
				clear({msg_style.pic})	
				msg_style.pic = nil		
			else
				if tonumber( msg_style.num:getString() ) - i <= 0 then
					clear({msg_style.pic})	
					msg_style.pic = nil		
				end
			end
		end 
		if msg_style.num then 
			if not i then				
				clear({msg_style.num})
				msg_style.num = nil
			else
				if tonumber( msg_style.num:getString() ) - i <= 0 then
					clear({msg_style.num})
					msg_style.num = nil
				else
					msg_style.num:setString( tonumber( msg_style.num:getString() ) - i )
				end					
			end
		end 
	end
end 		

function getMessageNum()
	if msg_style.num then
		return tonumber( msg_style.num:getString() )
	end
	return 0
end

function update_message_num()	
	
	CCUserDefault:sharedUserDefault():flush()

	local x = 145
	local y = 730
	local interval = 120
   
	local msg_size = 0
	for i = 2, 4 do	
		local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_" .. i, "{}" )
		local db_bin = json.decode( db_txt );
		for key, data in ipairs( db_bin ) do
			if not db_bin[key].readed then msg_size = msg_size + 1 end
		end			
	end	
	if msg_size > 0 then
		msg_style.pic = LIGHT_UI.clsSprite:New( ret_panel.bg, panelContainerConfig.msg_style_pic.x, panelContainerConfig.msg_style_pic.y, "tab_unread_bg.png" )
		msg_style.num = LIGHT_UI.clsLabel:New( ret_panel.bg, panelContainerConfig.msg_style_num.x, panelContainerConfig.msg_style_num.y, msg_size, GLOBAL_FONT, 18 )						
		msg_style.num:setTextColor(255, 255, 255)							
	else	
		clearMsgPicNum()	  				
	end
end

local function receiveMessageHandler()
	playWav(panelContainerConfig["music_sound"].sound,panelContainerConfig["music_sound"].ms)
	showMessageNum()
	local MSG = Import("lua/message.lua") 
	local panel = MSG.getMsgPanel()
	if panel then
		MSG.update_message_num()
	end
end

function showMessageNum()
	clearMsgPicNum()
	update_message_num()
end

local function onAnswer(data)
	--print( data.code )
	if data.code == -100 then
		SETTING = Import("lua/setting.lua")
		SETTING.backToLogin()
		return
	end
	local a = data.data
	if data.code == 1 and data.data and table.getn( data.data ) > 0 then
		for _, info in ipairs( data.data ) do
			if info.type ~= "6" then
				print( "type" .. info.type )
				print( "kind" .. info.kind )
				info._id = os.time() + math.random();
				local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_" .. info.kind, "{}" )
				local db_bin = json.decode( db_txt );
				table.insert( db_bin, info )
				db_txt = table2json( db_bin );
				setUserString( "msg_db_" .. GLOBAL.uid .. "_" .. info.kind, db_txt )
				if tonumber(info.kind) == 2 then 
				--如果收到是聊天消息就播放出声音，然后把game_hall.lua中消息更新数量
					--GAMEHALL_PANEL = Import("lua/game_hall.lua")
					receiveMessageHandler()					
				elseif tonumber(info.kind) == 3 then
					receiveMessageHandler()				
					--GAMEHALL_PANEL.update_message_num()
				elseif tonumber(info.kind) == 4 then
					receiveMessageHandler()				
				end																				
			end
		end	
	end

	asking = false
end	

local function doAsk()
	if not asking then
		doCommand( 'pool', 'popup', {}, onAnswer )
		asking = true
	end	
end

function beginAskData()
	if not TIMER['askDataCB'] then 
		TIMER['askDataCB'] = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(doAsk, 5, false)
	end
end

local function createPanelContainer( parent, x, y )
	--初始化数据
	Init()
	beginAskData()
	
	local ret_panel = createNode( parent, point( x, y ) )
	--ret_panel.bg = LIGHT_UI.clsLayer:New( parent, 0, 0 )	
	ret_panel.bg = LIGHT_UI.clsClipLayer:New( parent, 0, 0 )
	ret_panel.bg:set_msg_rect(0, 0, panelContainerConfig.panel_container_property.screen_width, panelContainerConfig.panel_container_property.screen_height)
	ret_panel.title_bg = createSprite( ret_panel.bg, panelContainerConfig.bg_config, nil )	
	--print( panelContainerConfig.bg_config.sy )
	ret_panel.title_txt = createLabel( ret_panel.bg, panelContainerConfig.title_text )
	xCenter( ret_panel.title_txt )
	
	ret_panel.bg_pic = createSprite( ret_panel.bg, panelContainerConfig.bg_pic, nil )
	
	ret_panel.v_line = createSprite( ret_panel.bg, panelContainerConfig.v_line )
	--ret_panel.spr = LIGHT_UI.clsSprite:New( ret_panel.bg, x + 50, y, "radiobutton_default_bg.png" )
	--ret_panel.spr2 = LIGHT_UI.clsSprite:New( ret_panel.bg, x + 200, y, "radiobutton_default_bg.png" )
	ret_panel.btn = LIGHT_UI.clsStatusButton:New( ret_panel.bg, panelContainerConfig.scale_container_btn.x, panelContainerConfig.scale_container_btn.y, panelContainerConfig.scale_container_btn.res, panelContainerConfig.scale_container_btn.on )
	ret_panel.btn.onTouchBegan = clickButtonBeganHandler
	ret_panel.btn.onTouchMove = moveHandler
	ret_panel.btn.onTouchEnd = clickButtonStopHandler
	ret_panel.bg.onTouchBegan = createMoveTimer
	ret_panel.bg.onTouchMove = moveHandler
	ret_panel.bg.onTouchEnd = stopHandler	
	
	
	return ret_panel
end

function showPanelContainer( parent, x, y )
	ret_panel = createPanelContainer( parent, 0, 0 )	
	update_message_num()
end	

function Init()
	msg_style = {}
	preLoadWav( panelContainerConfig["music_sound"].sound )	
end