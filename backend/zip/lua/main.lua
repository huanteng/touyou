-- main

MAINPAGE_LAYOUT = Import("layout/main.lua")
local MainPageConfig = MAINPAGE_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local mainPagePanel = nil

local FRIEND_PAGE = Import("lua/friend.lua")
local MESSAGE_PAGE = Import("lua/message.lua")
local NEWS_PAGE = Import("lua/moving.lua")

local closeMainPagePanel = nil

function closeAllPage()
	closeGridPage()
	FRIEND_PAGE.closeFriendPagePanel()
	MESSAGE_PAGE.closeMsgPagePanel()
	NEWS_PAGE.closeNewsPagePanel()
	closeMainPagePanel()
end

local function createTopScrollPanel(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	ret_panel._bg_pic = LIGHT_UI.clsSprite:New(ret_panel, 0, 0, "top_scroll_bg.png") --公告栏底图图片
	ret_panel._bg_pic:setAnchorPoint(0, 0)
	ret_panel._bg_pic:setPosition(0, -10)

	ret_panel._scroll_text = LIGHT_UI.clsRecycleScroll:New(ret_panel, 10, -2, 460, 30) --公告文字位置修改
	local txt1 = createLabel( nil, MainPageConfig.scroll_text )
	ret_panel._scroll_text:addInnerObj(txt1)
	ret_panel._scroll_text:startScroll()

	return ret_panel
end

local function getRootNode()
	return HTTP_CLIENT.getRootNode()
end

local function default_click_func(obj, x, y)
end

local function challenge_click(obj, x, y)
	closeMainPagePanel()
	CHALLENGE_PANEL.showChallengePagePanel( getRootNode(), 0, 0)
end

local function room_click(obj, x, y)
	closeMainPagePanel()
	local ROOM_PANEL = Import("lua/room.lua")
	ROOM_PANEL.showPanel(getRootNode(), 0, 0)
end

local function lucky_roll_click(obj, x, y)
	closeMainPagePanel()
	LUCKY_ROT_PANEL.showLuckyPagePanel( getRootNode(), 0, 0)
end

local function back_func()
	USER_INFO_PANEL.closeUserInfoPagePanel()
	showMainPagePanel( getRootNode(), 0, 0)
end

local function user_info_click(obj, x, y)
	closeMainPagePanel()
	USER_INFO_PANEL.showUserInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	USER_INFO_PANEL.set_back_func(back_func)
end

local function mall_click(obj, x, y)
	closeMainPagePanel()
	local MALL_PANEL = Import("lua/mall.lua")
	MALL_PANEL.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

local function say_click(obj, x, y)
	closeMainPagePanel()
	CHAT_PANEL.showChatPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

local function rank_click(obj, x, y)
	closeMainPagePanel()
	local RANK_PANEL = Import("lua/rank.lua")
	RANK_PANEL.showRankPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

local function event_click(obj, x, y)
	closeMainPagePanel()
	local o = Import("lua/event.lua")
	o.showEventPanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

local function photo_click(obj, x, y)
	closeMainPagePanel()
	local o = Import("lua/photo.lua")
	o.showPhotoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

local function setting_click(obj, x, y)
	closeMainPagePanel()
	SETTING_PANEL = Import("lua/setting.lua")
	SETTING_PANEL.showPanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

local function task_click(obj, x, y)
	closeMainPagePanel()
	local TASK_PANEL = Import("lua/task.lua")
	TASK_PANEL.showTaskPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

local btn_list_info1 = {
	[1] = {
		["normal_res"] = "tc_on.png",
		["selected_res"] = "tc_off.png",
		["dis_txt"] = "排行榜",
		["on_click_func"] = rank_click,
	},
	[2] = {
		["normal_res"] = "yxdt_on.png",
		["selected_res"] = "yxdt_off.png",
		["dis_txt"] = "多人游戏",
		["on_click_func"] = room_click,
	},
	[3] = {
		["normal_res"] = "rw_on.png",
		["selected_res"] = "rw_off.png",
		["dis_txt"] = "获得金币",
		["on_click_func"] = task_click,
	},
	[4] = {
		["normal_res"] = "hall.png",
		["selected_res"] = "hall_on.png",
		["dis_txt"] = "偶遇",
		["on_click_func"] = challenge_click,
	},
	[5] = {
		["normal_res"] = "kxp_on.png",
		["selected_res"] = "kxp_off.png",
		["dis_txt"] = "最新上传",
		["on_click_func"] = photo_click,
	},
	[6] = {
		["normal_res"] = "sl_on.png",
		["selected_res"] = "sl_off.png",
		["dis_txt"] = "说两句",
		["on_click_func"] = say_click,
	},
	[7] = {
		["normal_res"] = "zp.png",
		["selected_res"] = "zp_on.png",
		["dis_txt"] = "幸运转盘",
		["on_click_func"] = lucky_roll_click,
	},
	[8] = {
		["normal_res"] = "dj_on.png",
		["selected_res"] = "dj_off.png",
		["dis_txt"] = "道具商城",
		["on_click_func"] = mall_click,
	},
	[9] = {
		["normal_res"] = "activity_default.png",
		["selected_res"] = "activity_selected.png",
		["dis_txt"] = "活动优惠",
		["on_click_func"] = event_click,
	},
}

local btn_list_info2 = {
	[1] = {
		["normal_res"] = "myhome_default.png",
		["selected_res"] = "myhome_selected.png",
		["dis_txt"] = "个人主页",
		["on_click_func"] = user_info_click,
	},
	[2] = {
		["normal_res"] = "sz_on.png",
		["selected_res"] = "sz_off.png",
		["dis_txt"] = "设置",
		["on_click_func"] = setting_click,
	},
	--[3] = {
		--["normal_res"] = "yj_on.png",
		--["selected_res"] = "yj_off.png",
		--["dis_txt"] = "比赛中心",
		--["on_click_func"] = room_click,
	--},	
	}

local CHALLENGE_PAGE = Import("lua/challenge.lua")

local function doCreateBtnList(row_layout, btn_list_info, move_grp)
	local function on_click(btn, x, y)
		if not move_grp:isMoving() then
			btn._btn_info["on_click_func"](btn, x, y)
		end
	end

	for tag, btn_info in ipairs(btn_list_info) do
		local nodeObj = LIGHT_UI.clsNode:New(nil, 0, 0)
		local BtnObj = LIGHT_UI.clsSimpleButton:New(nil, 0, 0, btn_info["normal_res"], btn_info["selected_res"])
		BtnObj._btn_info = btn_info
		BtnObj:setPosition(0, 0)
		BtnObj:setAnchorPoint(0, 1)
		nodeObj:addChild(BtnObj)

		CHALLENGE_PANEL.setMsgDelegate(BtnObj, move_grp, on_click)

		local txt = LIGHT_UI.clsLabel:New(nil, 0, 0, btn_info["dis_txt"], GLOBAL_FONT, 22) --各模块文字控制
		txt:setPosition(48, -128)
		txt:setTextColor(51, 51, 51)
		nodeObj:addChild(txt)

		local function getContentSize(node)
			return BtnObj:getContentSize()
		end
		nodeObj.getContentSize = getContentSize

		row_layout:append_item(nodeObj)
	end
end

local function createMovePage(parent, x, y)
	local config = MainPageConfig["move_config"]
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_grp = LIGHT_UI.clsRecycleHMoveGroup:New(ret_panel, 0, 0, winSize.width, config["view_height"])
	ret_panel._move_grp._v_movable = false 

	local desc_info = MainPageConfig.desc_info
	local row_layout1 = LIGHT_UI.clsRowLayout:New(nil, 0, 0, desc_info)
	doCreateBtnList(row_layout1, btn_list_info1, ret_panel._move_grp)
	row_layout1:refresh_view()

	local row_layout2 = LIGHT_UI.clsRowLayout:New(nil, 0, 0, desc_info)
	doCreateBtnList(row_layout2, btn_list_info2, ret_panel._move_grp)
	row_layout2:refresh_view()

	row_layout1:setPosition(0, config["inner_y"])
	ret_panel._move_grp:appendItem(row_layout1)
	row_layout2:setPosition(0, config["inner_y"])
	ret_panel._move_grp:appendItem(row_layout2)
	ret_panel._move_grp:selectPage(1)
	
	return ret_panel
end

local function createMainPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y, 400 )

	hide( {ret_panel._head_back} )
	ret_panel._head_text:setString( '骰友' )

	config = MainPageConfig["top_scroll_config"]
	ret_panel._top_scroll_panel = createTopScrollPanel(ret_panel, config["x"], config["y"])

	config = MainPageConfig["move_config"]
	ret_panel._move_page = createMovePage(ret_panel, config["x"], config["y"])

	local function on_sel_page(move_obj)
		ret_panel._move_hint:selItem(move_obj:getCurPage())
	end

	ret_panel._move_page._move_grp.onSelPage = on_sel_page

	config = MainPageConfig["move_hint_config"]
	local item_list = {
		[1] = {
			["normal_pic"] = config["normal_res"],
			["sel_pic"] = config["select_res"],
		},
		[2] = {
			["normal_pic"] = config["normal_res"],
			["sel_pic"] = config["select_res"],
		},
	}

	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New( ret_panel, 150, 120, nil, item_list, 180 )--底部翻页标记间距
	ret_panel._move_hint:selItem(1)

	return ret_panel
end

--local askDataCB = nil
local asking = false

local MESSAGE_PAGE = Import("lua/message.lua")

local function onAnswer(data)

	if data.code == 1 and data.data and table.getn( data.data ) > 0 then
		for _, info in ipairs( data.data ) do
			if info.type ~= "6" then
				info._id = os.time() + math.random();
				local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_" .. info.kind, "{}" )
				local db_bin = json.decode( db_txt );				
				table.insert( db_bin, info )
				db_txt = table2json( db_bin );
				setUserString( "msg_db_" .. GLOBAL.uid .. "_" .. info.kind, db_txt )
				if (tonumber(info.kind)==2) then 				
				--如果收到是聊天消息就播放出声音，然后把game_hall.lua中消息更新数量
					GAMEHALL_PANEL = Import("lua/game_hall.lua")
					playWav(MainPageConfig["music_sound"].sound,MainPageConfig["music_sound"].ms)
					GAMEHALL_PANEL.update_message_num()
				end																
			end
		end
	end

	asking = false
end

local function doAsk()
	if not asking then
		doCommand( 'pool', 'popup', {}, onAnswer )
		asking = true
	end
end

function beginAskData()
	--[[if not askDataCB then
		askDataCB = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(doAsk, 1, false)
	end--]]
	if not TIMER['askDataCB'] then 
		TIMER['askDataCB'] = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(doAsk, 1, false)
	end
end

function showMainPagePanel(parent, x, y)
	GAMEHALL_PANEL.showGameHallPanel(HTTP_CLIENT.getRootNode(), 0, 0)
	if not mainPagePanel then
		mainPagePanel = createMainPagePanel(parent, x, y)
	else
		mainPagePanel:setVisible(true)
	end
	beginAskData()
end

function closeGridPage()
	if mainPagePanel then
		mainPagePanel:setVisible(false)
	end
end

function closeMainPagePanel()
	GAMEHALL_PANEL.closeGameHallPanel()
	--mainPagePanel:setVisible(false)
	mainPagePanel._top_scroll_panel._scroll_text:tryStopScroll()
	clear({mainPagePanel})
	mainPagePanel = nil
end

