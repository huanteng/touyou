-- oper page panel

local operPage = nil

local function on_set_logo_cb(data)
	print(LIGHT_UI.db_fmt(data))
end

local USER_ALBUM_PAGE = Import("lua/user_album.lua")
local function on_remove_photo_cb(data)
	print(LIGHT_UI.db_fmt(data))
	if data.code > 0 then
		USER_INFO_PANEL.removePhoto(operPage._photo_info["id"])
		USER_ALBUM_PAGE.onDeletePhoto()		
	else
		ALBUMPAGE = Import("lua/user_album.lua")
		showMessage( ALBUMPAGE.getAlbumPage(), "删除失败，视频认证用户必须保留3张以上照片", {ms="3000",y="500"} )
	end
	closeOperPagePanel()
end



local function createOperPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = LIGHT_UI.clsLayer:New(parent, x, y)

	ret_panel._bg_pic = LIGHT_UI.clsSprite:New(ret_panel, 65, 229, "tiny_white_pixel.png")
	ret_panel._bg_pic:setAnchorPoint(0, 1)
	ret_panel._bg_pic:setScaleX(300)
	ret_panel._bg_pic:setScaleY(130)

	ret_panel._top_pic = LIGHT_UI.clsSprite:New(ret_panel, 65, 229, "tiny_black_pixel.png")
	ret_panel._top_pic:setAnchorPoint(0, 1)
	ret_panel._top_pic:setScaleX(300)
	ret_panel._top_pic:setScaleY(50)

	ret_panel._top_title = LIGHT_UI.clsLabel:New(ret_panel, 85, 219, "照片操作", GLOBAL_FONT, 22)
	ret_panel._top_title:setTextColor(255, 255, 255)
	ret_panel._top_title:setAnchorPoint(0, 1)

	local function on_set_logo(btn, x, y)
		doCommand( 'album', 'set_logo', { id = operPage._photo_info["id"] }, on_set_logo_cb )
		--USER_INFO_PANEL.setUserPic(operPage._photo_info["url"])
		LOGIN_PANEL.setLogo( operPage._photo_info["url"] )
		closeOperPagePanel()
	end

	local function on_delete_pic(btn, x, y)
		doCommand( 'album', 'remove',{ id = operPage._photo_info["id"] }, on_remove_photo_cb )		
	end
	ret_panel._set_logo_btn = LIGHT_UI.clsSimpleButton:New(ret_panel, 150, 138, "sztx.png", "sztx_on.png")
	ret_panel._set_logo_btn:setString("设为头像")
	ret_panel._set_logo_btn:setTextColor(255, 255, 255)
	ret_panel._set_logo_btn.onTouchEnd = on_set_logo
	ret_panel._delete_btn = LIGHT_UI.clsSimpleButton:New(ret_panel, 280, 138, "sztx.png", "sztx_on.png")
	ret_panel._delete_btn:setString("删除")
	ret_panel._delete_btn:setTextColor(255, 255, 255)
	ret_panel._delete_btn.onTouchEnd = on_delete_pic

	local function on_click_layer(layer, x, y)
		closeOperPagePanel()
	end

	ret_panel.onTouchEnd = on_click_layer

	return ret_panel
end

function showOperPagePanel(photo_info)
	HTTP_CLIENT.showMask()
	if not operPage then
		operPage = createOperPagePanel(HTTP_CLIENT.getMaskNode(), 20, 300)
	else
		operPage:setVisible(true)
	end
	operPage._photo_info = photo_info
	USER_ALBUM_PAGE.setMsgProc(false)
end

function closeOperPagePanel()
	--operPage:setVisible(false)
	HTTP_CLIENT.hideMask()
	USER_ALBUM_PAGE.setMsgProc(true)
	--clear({operPage})
	operPage = nil
end

