--定时执行
function f_delay_do(obj,fn,arg,delay)--对象,方法，参数，延时(秒)
    if not obj then
        return
    end
    local function fn_cb()
		if fn then
			return fn(arg)
		end
    end
    local t_delay = 0
    if delay then 
        t_delay = tonumber(delay)
    end
    local action = CCSequence:createWithTwoActions(
    CCDelayTime:create(t_delay),
    CCCallFuncN:create(fn_cb))
    obj._RootNode:runAction(action)
end
---------------------------------------------------------------------------------------------
--淡入 动作
function a_fadein(obj,t_fadein,delay,cb)--对象，延时，时效，回调函数,是否复原
    assert(obj)
    local array = CCArray:create()
	if delay then
		array:addObject(CCDelayTime:create(delay))
	end
    local action = CCFadeIn:create(t_fadein)
	array:addObject(action)	
	if cb then
	    array:addObject(CCCallFuncN:create(cb))
	end
	obj:runAction(CCSequence:create(array))
end

--淡出 动作
function a_fadeout(obj,delay,t_fadeout,fn,arg)--对象，延时，时效，回调函数,是否复原
    --obj:runAction( CCFadeIn:create(t_fadeout))
    local function fn_cb()
        if fn then
            return fn(arg)
        end
    end
    local array = CCArray:create()
    if delay then
        array:addObject(CCDelayTime:create(delay))
    end
	array:addObject(CCFadeOut:create(t_fadeout))
	array:addObject(CCCallFuncN:create(fn_cb))
	obj:runAction(CCSequence:create(array))
end

--谈出并移动
function a_fadeout_move(obj,t_fadeout,t_move,ptTo,delay,fn,arg)
    local function fn_cb()
        if fn then
            return fn(arg)
        end
    end
    local array = CCArray:create()
    if delay then
        array:addObject(CCDelayTime:create(delay))
    end
    --array:addObject(CCFadeOut:create(t_fadeout))
	array:addObject(CCSpawn:createWithTwoActions(CCFadeOut:create(t_fadeout),CCMoveBy:create(t_move,ptTo)))
	array:addObject(CCCallFuncN:create(fn_cb))
	obj:runAction(CCSequence:create(array))
end


--淡入&移动-停留-淡出 动作
function a_fadein_move_fadeout(obj,t_fadein,t_move,ptTo,t_stay,t_fadeout,delay,fn,arg) -- ptTo need ccp(x,y)
    local function fn_cb()
        return fn(arg)
    end
    local array = CCArray:create()
    local m_fi = CCSpawn:createWithTwoActions(CCFadeIn:create(t_fadein),CCMoveTo:create(t_move,ptTo))
    if delay then
        array:addObject(CCDelayTime:create(delay))
    end
	array:addObject(m_fi)
	array:addObject(CCDelayTime:create(t_stay))
	array:addObject(CCFadeOut:create(t_fadeout))
	if fn then
		array:addObject(CCCallFuncN:create(fn_cb))
	end
	obj:runAction(CCSequence:create(array))
end

--移动 动作
function a_move(obj,delay,t_move,ptTo,fn,arg) -- ptTo need ccp(x,y)
    local function fn_cb()	
        return fn(arg)
    end
    local array = CCArray:create()
	if delay then
		array:addObject(CCDelayTime:create(delay))
	end
	array:addObject(CCMoveTo:create(t_move,ptTo))
	if fn then
		array:addObject(CCCallFuncN:create(fn_cb))
	end
	obj:runAction(CCSequence:create(array))
end

--加速度移动&淡入 动作
function a_emove_fadeIn(obj,t_move,ptTo,delay,fn,arg)
    local function fn_cb()
		if fn then
			return fn(arg)
		end
    end
	local function fn_hide()
		obj:setVisible(false)
	end
    local a_move = CCMoveTo:create(t_move, ccp(ptTo.x,ptTo.y))
    local a_ease = CCEaseExponentialIn:create(a_move)
    local m_fo = CCSpawn:createWithTwoActions(CCFadeIn:create(t_move),a_ease)
    local array = CCArray:create()
	if delay then
		array:addObject(CCDelayTime:create(delay))
	end
    array:addObject(m_fo)
	array:addObject(CCCallFuncN:create(fn_hide))
    array:addObject(CCCallFuncN:create(fn_cb))
    obj:runAction(CCSequence:create(array))
end

--摇动
function a_shake(obj,t_shake,count,angle,delay,fn,arg)--t_shake摇动一次时长,摇动次数,角度
	assert(obj)
    local function fn_cb()
		if fn then
			return fn(arg)
		end
    end		
	local array = CCArray:create()
    local a = CCRotateTo:create(t_shake,angle)
	local b = CCRotateTo:create(t_shake,-angle)
	local c = CCRotateTo:create(t_shake/2,0)	
	for i = 1,count do
		array:addObject(a)		
		array:addObject(b)
	end
	array:addObject(c)	
	array:addObject(CCCallFuncN:create(fn_cb))		
    obj:runAction(CCSequence:create(array))
end

--转动
function a_rotate(obj,t_shake,angle,delay,fn,arg)--a_rotate转动一次时长,摇动次数,角度
	assert(obj)
    local function fn_cb()
		if fn then
			return fn(arg)
		end
    end		
	local array = CCArray:create()
    local a = CCRotateTo:create(t_shake,angle)					
	array:addObject(a)	
	array:addObject(CCCallFuncN:create(fn_cb))		
    obj:runAction(CCSequence:create(array))
end

--循环动作
function a_play(obj,config,bLoop,begin_sec,delay,fn,arg)
	local function fn_cb()
		if fn then
			fn(arg)
		end
	end
    local file = getResFilename( config.res )	
    local frame_time = config.time
    local frame_width = config.width
    local frame_height = config.height
	local frame_across = config.across
	local frame_row = config.row
	local frame_count = frame_across * frame_row	
    local texture = CCTextureCache:sharedTextureCache():addImage(file)	
    local frame_index = 0	
	if begin_sec then
		assert(begin_sec <= frame_time)
		frame_index = math.floor(frame_count / frame_time )*frame_width
	end
    local animFrames = CCArray:create()
     --local sprite = nil
    for i = 0,frame_across - 1 do
		for j = 0,frame_row - 1 do
			if frame_index <= i*j then
				local rect = CCRectMake(j*frame_width,frame_height*i,frame_width,frame_height)
				local frame = CCSpriteFrame:createWithTexture(texture, rect)				
				animFrames:addObject(frame)
			end
		end
    end		
	local animation = nil
	local animation = CCAnimation:createWithSpriteFrames(animFrames, frame_time)
    local animate = CCAnimate:create(animation);
    local repeatloop = CCRepeatForever:create(animate)
    local array = CCArray:create()
    if bLoop then
        --array:addObject(repeatloop)
		obj:runAction(repeatloop)
		return
    else
        local function destory()
            obj:removeFromParentAndCleanup(true)
        end
        array:addObject(animate)
        array:addObject(CCCallFuncN:create(destory))
    end
	array:addObject(CCCallFuncN:create(fn_cb))
    local sequence = CCSequence:create(array)
    obj:runAction(sequence)
end

--倒计时动作
function a_countdown(obj,delay,scale,fn,arg)
    local function fn_show()
        obj:setVisible(true)
    end
	local function fn_cb()
		if fn then
			fn(arg)
		end
	end
	local fScale = 1
	if scale then
	    fScale = tonumber(scale)
	end
	local aScale =  CCScaleTo:create(0.5*fScale, 1.5*fScale)
	local aFaceOut = CCFadeOut:create(0.5)
	local bScale =  CCScaleTo:create(0.5*fScale, 1*fScale)
	local array = CCArray:create()
	array:addObject(CCDelayTime:create(delay))
	array:addObject(CCCallFuncN:create(fn_show))
	array:addObject(aScale)
	array:addObject(CCSpawn:createWithTwoActions(bScale,aFaceOut))
	if fn then
	    array:addObject(CCCallFuncN:create(fn_cb))
	end
	local as = CCSequence:create(array)
	obj:runAction(as)	 
end
function play_countdown(parent,config,begin_sec,delay,fn)
	local file_path = getResFilename( config.path )
	local frame_count = config.count
	local frame_x = config.x
	local frame_y = config.y
	local frame_scale = config.scale
	local animFrames = CCArray:create()
	local function fn_cb(arg)
	    if fn and tonumber(arg) >= frame_count then
	        return fn()
	    end
    end 
	if begin_sec > 1 and begin_sec <= frame_count then
		local t_delay = 0
		if delay then
		    t_delay = t_delay + delay
		end
		for i = 1+(frame_count - begin_sec),frame_count do		
		    --local file = string.format("res/countdown/%d.png", i)
		    local file = string.format("%s/%d.png", file_path,i)
			local sprite = CCSprite:create( file)
			parent:addChild(sprite)
			sprite:setPosition(frame_x,frame_y)
			sprite:setVisible(false)
			a_countdown(sprite,t_delay,frame_scale,fn_cb,i)
			t_delay = t_delay + 1
		end		
	end 
end




