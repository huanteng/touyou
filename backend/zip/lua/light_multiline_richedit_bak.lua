local UI_MULTI_EDIT = import("base/light_ui/light_multiline_edit.lua")
local UI_TEXT = import("base/light_ui/light_text.lua")
local UI_ANIMATION = import("base/light_ui/light_animation.lua")
local UI_HTTP_TEXT = import("base/light_ui/light_http_text.lua")
local SHAPE_INFO = import("info.lua").SHAPE_INFO
local RICHTAG_FUNC = import("base/light_ui/richedit_func.lua")
local LIGHT_LINK = import("base/light_ui/light_links.lua")

clsLightMultiRichEdit = UI_MULTI_EDIT.clsLightMultiEdit:Inherit()

--[[
_ElementRec = {
[1] = {
	ElementObj = ,
	DescInfo = {
		EleType = ,
	},
},
}
--]]

--[[
TextInfo = {
	Color = ,
	Font =,
	Width = ,
	MaxCharCount = ,
	MaxTextWidth = ,
	LineInterval = ,
	MinMsgHeight = ,
}
--]]

local ElementInterval = 1 

local ELETYPE_TEXT = 1
local ELETYPE_TCP = 2
local ELETYPE_ITEMLINK = 3
local ELETYPE_ITEMDESC = 4
local ELETYPE_HTTPLINK = 5
local ELETYPE_POSLINK = 6
local ELETYPE_NPCLINK = 7
local ELETYPE_FUNCTION = 8

local SpeBeginChar = "#"
local CursorWidth = 3 

local TestTag = false 

local TextZ = 2 

local EFF_BLINK = 1
local EFF_UNDERLINE = 2
local EFF_COLOR = 3
local EFF_BIG = 4

local NON_STATUS = 0
local REC_POUND = 1
local REC_CUSTOM_COLOR_TAG = 2
local REC_EMOTE_NUM = 3
local REC_ITEM_LINK = 4

local EmoteSpeed = 10

local NormalTcpNumList = {
	[1] = {
		Min = 0,
		Max = 63,
	},
	[2] = {
		Min = 74,
		Max = 99,
	},
	[3] = {
		Min = 101,
		Max = 138,
	},
}

local ExtendTcpNumList = {
	[1] = {
		Min = 0,
		Max = 63,
	},
	[2] = {
		Min = 74,
		Max = 99,
	},
	[3] = {
		Min = 101,
		Max = 138,
	},
	[4] = {
		Min = 901,
		Max = 934,
	},
	[5] = {
		Min = 987,
		Max = 993,
	},
	[6] = {
		Min = 996,
		Max = 999,
	},
}

function clsLightMultiRichEdit:__init__(ParentObj, BaseX, BaseY, TextInfo)
	Super(self).InitCommon(self, ParentObj, BaseX, BaseY, TextInfo)

	self._DefaultLineHeight = self._HideTextObj:GetFontHeight()
	self._LineHeightRec = {}
	self._LineHeightRec[self._CurLine] = self._DefaultLineHeight
	--[[self._TextEffectDesc = { 
		[EFF_BLINK] = para,
		[EFF_UNDERLINE] = true,
		[EFF_BIG] = true,
		[EFF_COLOR] = 0x******,
	}]]
	self._TextEffectDesc = {} 
	self._NormalTextEffectDesc = {}
	self._ProcRichStatus = NON_STATUS 
	self._CustomColorStr = "" 
	self._TcpNumNameStr = ""
	self._TcpNumList = NormalTcpNumList 
	self._AcceptItemLink = false
	self._ItemDescCnt = 0

	self._LightLinkObj = LIGHT_LINK.clsLinkObject:NewObj()
    
	self._TextCursor:SetSize(CursorWidth, self._DefaultLineHeight)

	if TestTag then
		self._TestBGObj:SetSize(self._MaxTextWidth, self._DefaultLineHeight)
	end
end

local LineOffset = 10 

function clsLightMultiRichEdit:EnableSysTag()
	self._TcpNumList = ExtendTcpNumList 
end

function clsLightMultiRichEdit:AcceptItemLink()
	return self._AcceptItemLink
end

function clsLightMultiRichEdit:EnableItemLink()
	self._AcceptItemLink = true
end

local function DeleteWordForwardLeft(EditObj, Count)
	EditObj:DeleteSubElement(EditObj._InsertIndex - Count + 1, EditObj._InsertIndex)
	EditObj._InsertIndex = EditObj._InsertIndex - Count
end

function clsLightMultiRichEdit:InsertClickItemLink()
	local DelCnt = self._LightLinkObj:CalcLinkTextCnt()
	DeleteWordForwardLeft(self, DelCnt)
	
    DescInfo = self._LightLinkObj:GetDescInfo()
	local ItemId = DescInfo.Id
	local ItemColor = DescInfo.Color or 0
	local HostIdStr = DescInfo.HostIdStr
	local ItemName = DescInfo.Name
	if not ItemId or not ItemColor then
		return
	end

	local RealColor = COLORS[ItemColor]
	local LightLinkObj = self._LightLinkObj
	local LinkType = LightLinkObj:GetLinkType()

	local ItemLinkObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, ItemName, RealColor, self._Font)
	ItemLinkObj:SetHandleMouseMove(true)
	ItemLinkObj:SetHandleMouseClick(true)
	
	--- 鼠标移进移出变化
	local OnMouseIn = ItemLinkObj.OnMouseIn
	ItemLinkObj.OnMouseIn = function(self, MouseX, MouseY)
	    OnMouseIn(self, MouseX, MouseY)
		g_cursor:set_style("手点")
	end
	
	local OnMouseOut = ItemLinkObj.OnMouseOut
	ItemLinkObj.OnMouseOut = function(self, MouseX, MouseY)
	    OnMouseOut(self, MouseX, MouseY)
		g_cursor:set_style()
	end	
	
	LIGHT_LINK.SetLinksForRichEdit(self, ItemLinkObj, LinkType, ItemId, HostIdStr)

	--- ItemLinkObj:SetShow(false)
	table.insert(self._ElementRec, self._InsertIndex + 1, { ElementObj = ItemLinkObj, DescInfo = { EleType = ELETYPE_ITEMLINK, Name = ItemName, 
		Text = ItemName, Id = ItemId, Color = ItemColor }, })
	self._InsertIndex = self._InsertIndex + 1
end

function clsLightMultiRichEdit:AddLinkDesc(Id, Name, LinkTypeChar, nColor)
    self._ItemDescCnt = self._ItemDescCnt + 1
    DescIndex = self._ItemDescCnt
    DescStr = LIGHT_LINK.GenerateLinkDesc(Id, LinkTypeChar, nColor)
    local TextObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, "[" .. Name .. "]", COLORS[nColor], self._Font)
    TextObj:SetShow(false)

    table.insert(self._ElementRec, self._InsertIndex + 1, { 
            ElementObj = TextObj , 
            DescInfo = { 
                EleType = ELETYPE_ITEMDESC, 
                Text = "[链接" .. DescIndex .. ":" .. Name .. "]", 
                LinkDesc = DescStr,
                LinkIndex = DescIndex,
            }, 
        }
    )

	self._InsertIndex = self._InsertIndex + 1
	self:DoArrangeLine(1)
end

--[[
local function GenItemDesc(EditObj, ItemInfo)
	EditObj._ItemDescCnt = EditObj._ItemDescCnt + 1
	return "i" .. ItemInfo.id .. "c" .. ItemInfo.ncolor, EditObj._ItemDescCnt
end

local function GenSummonDesc(EditObj, SummonInfo, SummonColor)
	EditObj._ItemDescCnt = EditObj._ItemDescCnt + 1
	return "s" .. SummonInfo.id .. "c" .. SummonColor, EditObj._ItemDescCnt
end

local function GenTaskDesc(EditObj, TaskInfo)
    EditObj._ItemDescCnt = EditObj._ItemDescCnt + 1
    return "t" .. TaskInfo.id .. "c" .. TaskInfo.ncolor, EditObj._ItemDescCnt
end


function clsLightMultiRichEdit:AddItemDesc(ItemInfo, Prefix, SummonColor)
	if Prefix == "i" then
		local ItemDesc, DescIndex = GenItemDesc(self, ItemInfo)
		local DisName = "[" .. ItemInfo.name .. "]"
		local ItemObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, DisName, COLORS[ItemInfo.ncolor], self._Font)
		ItemObj:SetShow(false)
		table.insert(self._ElementRec, self._InsertIndex + 1, { ElementObj = ItemObj , DescInfo = { EleType = ELETYPE_ITEMDESC, 
			Text = "[链接" .. DescIndex .. ":" .. ItemInfo.name .. "]", LinkDesc = ItemDesc, LinkIndex = DescIndex, }, })
		self._InsertIndex = self._InsertIndex + 1

		self:DoArrangeLine(1)
	elseif Prefix == "s" then
		local ItemDesc, DescIndex = GenSummonDesc(self, ItemInfo, SummonColor)
		local SummonName = SHAPE_INFO[ ItemInfo["type"] ].name
		local DisName = "[" .. SummonName .. "]"
		local ItemObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, DisName, COLORS[SummonColor], self._Font)
		ItemObj:SetShow(false)
		table.insert(self._ElementRec, self._InsertIndex + 1, { ElementObj = ItemObj , DescInfo = { EleType = ELETYPE_ITEMDESC, 
			Text = "[链接" .. DescIndex .. ":" .. SummonName .. "]", LinkDesc = ItemDesc, LinkIndex = DescIndex, }, })
		self._InsertIndex = self._InsertIndex + 1

		self:DoArrangeLine(1)
	elseif Prefix == "t" then
	    local ItemDesc, DescIndex = GenTaskDesc(self, ItemInfo)
	    local DisName = "[" .. ItemInfo.name .. "]"
	    local ItemObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, DisName, COLORS[ItemInfo.ncolor], self._Font)
	    ItemObj:SetShow(false)
		table.insert(self._ElementRec, self._InsertIndex + 1, { ElementObj = ItemObj , DescInfo = { EleType = ELETYPE_ITEMDESC, 
			Text = "[链接" .. DescIndex .. ":" .. ItemInfo.name .. "]", LinkDesc = ItemDesc, LinkIndex = DescIndex, }, })
		self._InsertIndex = self._InsertIndex + 1

		self:DoArrangeLine(1)	    
	end
end
]]

-- #f{id=***,txt=***}
function clsLightMultiRichEdit:AddFuncBlock(Text, FuncId, ParamList)
	local Color = self._TextEffectDesc[EFF_COLOR] or self._Color
	local ItemObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, Text, Color, self._Font)
	ItemObj:SetShow(false)
	ItemObj:SetHandleMouseClick(true)
	ItemObj:SetHandleMouseMove(true)
	ItemObj:SetUnderLine(true)
	ItemObj:SetExtVar("ParamList", ParamList)
	ItemObj.OnMouseIn = function(obj, x, y)
		g_cursor:set_style("手点")
	end
	ItemObj.OnMouseOut = function(obj, x, y)
		g_cursor:set_style()
	end
	ItemObj.OnLeftUp = function(obj, x, y)
		RICHTAG_FUNC.DoRichTagFunc(FuncId, obj:GetExtVar("ParamList"))
	end
	table.insert(self._ElementRec, self._InsertIndex + 1, { ElementObj = ItemObj , DescInfo = { EleType = ELETYPE_FUNCTION, 
		Text = "#f{id=" .. FuncId .. ",txt=" .. Text .. "}", FuncId = FuncId, }, })
	self._InsertIndex = self._InsertIndex + 1

	self:DoArrangeLine(1)
	return ItemObj
end

function clsLightMultiRichEdit:AppendFuncBlock(Text, FuncId, ParamList)
	local ItemObj = self:AddFuncBlock(Text, FuncId, ParamList)
	self:DoReturnProc()
	self:TryNoticeListener(true)
	return ItemObj
end

local ColorMatchList = {
	["#R"] = 0xff0000,
	["#G"] = 0x00ff00,
	["#B"] = 0x0000ff,
	["#Y"] = 0xffff00,
	["#W"] = 0xffffff,
}

local function TryProcMidEffect(PlainText, ItemObj)
	local NewPlainText = string.gsub(PlainText, "#[RGBYWu]", "")
	ItemObj:SetText(NewPlainText)

	local ColorText = string.match(PlainText, "#[RGBYW]")
	if ColorText then
		ItemObj:SetColor(ColorMatchList[ColorText])
	end

	local UnderLineText = string.match(PlainText, "#u")
	if UnderLineText then
		ItemObj:SetUnderLine(true)
	end
end

function clsLightMultiRichEdit:AddHttpLink(PlainText, LinkAddr)
	local Color = self._TextEffectDesc[EFF_COLOR] or self._Color
	local ItemObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, PlainText, Color, self._Font)
	ItemObj:SetShow(false)
	ItemObj:SetHandleMouseClick(true)
	ItemObj.OnLeftUp = function(obj, x, y)
		win32.open_www(LinkAddr)
	end

	if self._TextEffectDesc[EFF_UNDERLINE] then
		ItemObj:SetUnderLine(true)
	end

	TryProcMidEffect(PlainText, ItemObj)

	table.insert(self._ElementRec, self._InsertIndex + 1, { ElementObj = ItemObj , DescInfo = { EleType = ELETYPE_HTTPLINK, 
		Text = "<a href=" .. LinkAddr .. ">" .. PlainText .. "</a>" }, })
	self._InsertIndex = self._InsertIndex + 1

	self:DoArrangeLine(1)
end

function clsLightMultiRichEdit:AddPosLink(PlainText, SceneId, X, Y, NpcId)
	local Color = self._TextEffectDesc[EFF_COLOR] or self._Color
	local ItemObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, PlainText, Color, self._Font)
	ItemObj:SetShow(false)
	ItemObj:SetHandleMouseClick(true)
	ItemObj.OnLeftUp = function(obj, x, y)
		local FindPathModule = import("logic/module/AutoFindPath/FindPathModule.lua")
		FindPathModule.AutoGotoNpc(0, "", tonumber(SceneId), tonumber(X)*20, tonumber(Y)*20)
	end

	if self._TextEffectDesc[EFF_UNDERLINE] then
		ItemObj:SetUnderLine(true)
	end

	TryProcMidEffect(PlainText, ItemObj)

	table.insert(self._ElementRec, self._InsertIndex + 1, { ElementObj = ItemObj , DescInfo = { EleType = ELETYPE_POSLINK, 
		Text = string.format("<pos scene=%s x=%s y=%s>%s</pos>", SceneId, X, Y, PlainText) }, })
	self._InsertIndex = self._InsertIndex + 1

	self:DoArrangeLine(1)
end

function clsLightMultiRichEdit:AddNpcLink(PlainText, NpcId)
	local Color = self._TextEffectDesc[EFF_COLOR] or self._Color
	local ItemObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, PlainText, Color, self._Font)
	ItemObj:SetShow(false)
	ItemObj:SetHandleMouseClick(true)
	ItemObj.OnLeftUp = function(obj, x, y)
		local FindPathModule = import("logic/module/AutoFindPath/FindPathModule.lua")
		local _info = import("logic/netdata/ginfo.lua").get_npc_info(NpcId)
		if _info and _info.name then
			FindPathModule.AutoGotoNpc(NpcId, _info.name, _info.scene_id, _info.x, _info.y)
		end
	end

	if self._TextEffectDesc[EFF_UNDERLINE] then
		ItemObj:SetUnderLine(true)
	end

	TryProcMidEffect(PlainText, ItemObj)

	table.insert(self._ElementRec, self._InsertIndex + 1, { ElementObj = ItemObj , DescInfo = { EleType = ELETYPE_NPCLINK, 
		Text = string.format("#c(%s)%s#n", NpcId, PlainText) }, })
	self._InsertIndex = self._InsertIndex + 1

	self:DoArrangeLine(1)
end

function clsLightMultiRichEdit:CalLineByMouseY(MouseY)
	MouseY = MouseY + LineOffset
	local SumHeight = 0
	for LineIndex, LineHeight in ipairs(self._LineHeightRec) do
		SumHeight = SumHeight + LineHeight + self._LineInterval

		if SumHeight >= MouseY then
			return LineIndex
		end
	end

	return #(self._LineHeightRec)
end

function clsLightMultiRichEdit:CalElementSize(ElementInfoList)
	local Width, Height = 0, 0
	for _, ElementInfo in pairs(ElementInfoList) do
		local ElementWidth, ElementHeight = ElementInfo.ElementObj:GetSize()
		Width = Width + ElementWidth + ElementInterval
		if ElementHeight > Height then
			Height = ElementHeight
		end
	end

	return Width, Height
end

function clsLightMultiRichEdit:GetSubElement(BeginIndex, EndIndex)
	local ElementInfoList = {}
	if BeginIndex <= 0 then
		return ElementInfoList
	end

	for i = BeginIndex, EndIndex do
		ElementInfoList[i] = self._ElementRec[i]
	end

	return ElementInfoList
end

local function FindMaxHeight(EditObj, ElementInfoList)
	local Height = EditObj._DefaultLineHeight 
	for _, ElementInfo in pairs(ElementInfoList) do
		local _, TempHeight = ElementInfo.ElementObj:GetSize()
		if TempHeight > Height then
			Height = TempHeight
		end
	end

	return Height
end

local function CalSumLineHeight(EditObj, LineIndex)
	local RetHeight = 0
	for i = 1, LineIndex do
		RetHeight = RetHeight + EditObj._LineHeightRec[i] + EditObj._LineInterval
	end

	return RetHeight
end

local function KeyCompare(k1, k2)
	if k1 < k2 then
		return true
	else
		return false
	end
end

function clsLightMultiRichEdit:CalTextObjY(LineIndex)
	if LineIndex == 1 then
		return 0
	else
		return CalSumLineHeight(self, LineIndex - 1)
	end
end

local function pairs_orderly(tbl, comp)
	local keys = {}
	table.foreach(tbl, function (k,v) table.insert(keys, k) end)
	table.sort(keys, comp)
	local keys_count = #keys
	local index = 0

	local next_orderly = function(tbl)
		index = index + 1
		if index > keys_count then
			return
		end
		return keys[index], tbl[keys[index]]
	end

	return next_orderly, tbl
end

local function DisplayElementList(EditObj, ElementInfoList, LineIndex)
	local LineMaxHeight = FindMaxHeight(EditObj, ElementInfoList)
	EditObj._LineHeightRec[LineIndex] = LineMaxHeight

	local StartPosX = 0
	local StartPosY = EditObj:CalTextObjY(LineIndex)

	for Index, ElementInfo in pairs_orderly(ElementInfoList, KeyCompare) do
		local ElementObj = ElementInfo.ElementObj
		local ElementWidth, ElementHeight = ElementObj:GetSize()
		local DisplayPosX = StartPosX 
		local DisplayPosY = StartPosY + LineMaxHeight - ElementHeight 

		ElementObj:SetPos(DisplayPosX, DisplayPosY)
		StartPosX = StartPosX + ElementWidth + ElementInterval

		ElementObj:SetShow(true)
	end
end

function clsLightMultiRichEdit:DisplayLine(LineIndex)
	local EleList = self:GetSubElement(self._LineRec[LineIndex].StartIdx, self._LineRec[LineIndex].EndIdx)
	DisplayElementList(self, EleList, LineIndex)
end

function clsLightMultiRichEdit:UnDisplayLine(LineIndex)
	local EleList = self:GetSubElement(self._LineRec[LineIndex].StartIdx, self._LineRec[LineIndex].EndIdx)
	for Index, ElementInfo in pairs_orderly(EleList, KeyCompare) do
		local ElementObj = ElementInfo.ElementObj
		ElementObj:SetShow(false)
		ElementObj:SetPos(0, 0)
	end
end
------------------------------------------------------------------------------------ ???
-- text effect func
-- 返回值代表特效是否改变字大小
local function BlinkText(TextObj)
	TextObj:SetBlink(true, 1)
	return false
end

local function UnBlinkText(TextObj)
	TextObj:SetBlink(false, 1)
	return false
end

local function UnderLineText(TextObj)
	TextObj:SetUnderLine(true)
	return false
end

local function UnUnderLineText(TextObj)
	TextObj:SetUnderLine(false)
	return false
end

local function ColorText(TextObj, Color)
	TextObj:SetColor(Color)
	return false
end

-- 暂不实现还原颜色接口 ???
local function UnColorText(TextObj)
	return false
end

local function BigText(TextObj)
	TextObj:SetBig(true)
	return true
end

local function UnBigText(TextObj)
	TextObj:SetBig(false)
	return true
end

---

local SpeProcTextEffect = {
	[EFF_BIG] = true,
	[EFF_BLINK] = true,
}

local function ConfigUnderLineEffect(EditObj)
	if EditObj._NormalTextEffectDesc[EFF_UNDERLINE] then
		EditObj._NormalTextEffectDesc[EFF_UNDERLINE] = nil 
	else
		EditObj._NormalTextEffectDesc[EFF_UNDERLINE] = true 
	end

	EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
end

local function ConfigBlinkEffect(EditObj)
	if EditObj._NormalTextEffectDesc[EFF_BLINK] then
		EditObj._NormalTextEffectDesc[EFF_BLINK] = nil 
	else
		EditObj._NormalTextEffectDesc[EFF_BLINK] = true 
	end

	EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
end

local function ConfigNoEffect(EditObj)
	EditObj._NormalTextEffectDesc = {}
	EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
end

local function ConfigColor(EditObj, Color)
	EditObj._NormalTextEffectDesc[EFF_COLOR] = Color
	EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
end

local function ConfigRColorEffect(EditObj)
	ConfigColor(EditObj, 0xff0000)
end

local function ConfigGColorEffect(EditObj)
	ConfigColor(EditObj, 0x00ff00)
end

local function ConfigBColorEffect(EditObj)
	ConfigColor(EditObj, 0x0000ff)
end

local function ConfigYColorEffect(EditObj)
	ConfigColor(EditObj, 0xffff00)
end

local function ConfigWColorEffect(EditObj)
	ConfigColor(EditObj, 0xffffff)
end

local function ConfigPoundEffect(EditObj)
	EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
end

local function ConfigCustomColorEffect(EditObj)
end

local function NoMatchEffectProc(EditObj)
	EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
end

local MatchEffectFuncList = {
	["u"] = ConfigUnderLineEffect,
	["b"] = ConfigBlinkEffect,
	["#"] = ConfigPoundEffect, 
	["n"] = ConfigNoEffect,
	["R"] = ConfigRColorEffect,
	["G"] = ConfigGColorEffect,
	["B"] = ConfigBColorEffect,
	["Y"] = ConfigYColorEffect,
	["W"] = ConfigWColorEffect,
	["c"] = ConfigCustomColorEffect,
}

local CustomColorStrLen = 6
local function IsCustomColorChar(Char)
	return string.match(Char, "[%da-fA-F]")
end

local function IsNumberChar(Char)
	return string.match(Char,"%d")
end

local function ConfigTcpEffect(EditObj)
end

local function TryMatchEffectFunc(Char)
	if IsNumberChar(Char) then
		return ConfigTcpEffect
	else
		return MatchEffectFuncList[Char]
	end
end

local function TryConfigTextEffect(EditObj, Char) -- 只负责配置特效
	if (EditObj._ProcRichStatus == NON_STATUS) and Char == SpeBeginChar then
		EditObj._TextEffectDesc = SpeProcTextEffect
		return
	end

	if (EditObj._ProcRichStatus == REC_POUND) then
		local MatchFunc = TryMatchEffectFunc(Char)
		if MatchFunc then
			MatchFunc(EditObj)
		else
			NoMatchEffectProc(EditObj)
		end
		return
	end

	if (EditObj._ProcRichStatus == REC_CUSTOM_COLOR_TAG) then
		if IsCustomColorChar(Char) then
			EditObj._CustomColorStr = EditObj._CustomColorStr .. Char
			if string.len(EditObj._CustomColorStr) >= CustomColorStrLen then
				local Color = tonumber(EditObj._CustomColorStr, 16)
				EditObj._NormalTextEffectDesc[EFF_COLOR] = Color 
				EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
			end
		else
			NoMatchEffectProc(EditObj)
		end
		return
	end
end

function clsLightMultiRichEdit:ClearEffect()
	self._TextEffectDesc = {}
	self._NormalTextEffectDesc = {}
end

local EffectFuncList = {
	[EFF_BLINK] = BlinkText,
	[EFF_UNDERLINE] = UnderLineText,
	[EFF_COLOR] = ColorText,
	[EFF_BIG] = BigText,
}

local function RenderTextEffect(EditObj, BeginIndex, EndIndex, EffectList)
	for i = BeginIndex, EndIndex do
		if EditObj._ElementRec[i].DescInfo.EleType == ELETYPE_TEXT then
			local TextObj = EditObj._ElementRec[i].ElementObj
			for EffType, Para in pairs(EffectList) do
				EffectFuncList[EffType](TextObj, Para)
			end
		end
	end
end

local function CancelAllEffect(EditObj, BeginIndex, EndIndex)
	for i = BeginIndex, EndIndex do -- 目前只支持文本特效
		if EditObj._ElementRec[i].DescInfo.EleType == ELETYPE_TEXT then
			local ElementObj = EditObj._ElementRec[i].ElementObj
			ElementObj:CancelAllEffect()
		end
	end
end

local function PoundProc(EditObj)
	DeleteWordForwardLeft(EditObj, 1)
	CancelAllEffect(EditObj, EditObj._InsertIndex, EditObj._InsertIndex)
	RenderTextEffect(EditObj, EditObj._InsertIndex, EditObj._InsertIndex, EditObj._TextEffectDesc)

	EditObj._ProcRichStatus = NON_STATUS 
end

local function BackWordsProc(EditObj)
	DeleteWordForwardLeft(EditObj, 2)

	EditObj._ProcRichStatus = NON_STATUS 
end

local function ReturnProc(EditObj)
	BackWordsProc(EditObj)
	EditObj:DoInsertNewLineChar()
end

local function NoMatchSpeProc(EditObj)
	CancelAllEffect(EditObj, EditObj._InsertIndex - 1, EditObj._InsertIndex)
	RenderTextEffect(EditObj, EditObj._InsertIndex - 1, EditObj._InsertIndex, EditObj._TextEffectDesc)
	EditObj._ProcRichStatus = NON_STATUS 
end

local function CustomColorProc(EditObj)
	EditObj._ProcRichStatus = REC_CUSTOM_COLOR_TAG
end

local function LightLinkProc(EditObj, Char)
    EditObj._ProcRichStatus = REC_ITEM_LINK
    EditObj._LightLinkObj:SetLinkTypeByChar(Char)
end

--[[
#r
#u
#b
##
#n
#R
#G
#B
#Y
#W
#数字
#l
#c颜色码
--]]


local MatchSpeFuncList = {
	["u"] = BackWordsProc,
	["b"] = BackWordsProc,
	["#"] = PoundProc, 
	["n"] = BackWordsProc,
	["R"] = BackWordsProc,
	["G"] = BackWordsProc,
	["B"] = BackWordsProc,
	["Y"] = BackWordsProc,
	["W"] = BackWordsProc,
	["c"] = CustomColorProc,
	["r"] = ReturnProc,
}

local function TcpProc(EditObj, Char)
	EditObj._ProcRichStatus = REC_EMOTE_NUM
	EditObj._TcpNumNameStr = EditObj._TcpNumNameStr .. Char
end

local function TryMatchSpeFunc(Char)
	if IsNumberChar(Char) then
		return TcpProc
	else
		return MatchSpeFuncList[Char]
	end
end

local function IsTcpEndChar(Char)
	return Char == " "
end

local EmoteRelaPath = "emote/"
local function InsertTcpElement(EditObj, TcpNameStr, InsertIndex)
	local FileName = string.format("%s%03d.tcp", EmoteRelaPath, tonumber(TcpNameStr))
	local EmoteObj = UI_ANIMATION.clsLightAnimation:NewObj(EditObj._TextMsgHandler, 0, 0, FileName, RESOURCE_WDF_UI_PATH, EmoteSpeed)
	EmoteObj:SetShow(false)
	table.insert(EditObj._ElementRec, InsertIndex + 1, { ElementObj = EmoteObj, DescInfo = { EleType = ELETYPE_TCP, Text = string.format("#%s ", TcpNameStr), }, })
	EditObj._InsertIndex = EditObj._InsertIndex + 1
end

function clsLightMultiRichEdit:PreProcInputStr(InputStr)
	local RetStr = string.gsub(InputStr, "#%d+", "%0 ")
	return RetStr
end

local MaxTcpNumCnt = 3

local function IsAmongTcpRange(EditObj, Num)
	if Num == 0 then
		return true
	end

	for _, RangeInfo in ipairs(EditObj._TcpNumList) do
		if Num >= RangeInfo.Min and Num <= RangeInfo.Max then
			return true
		end
	end

	return false
end

local function CanTcpNumExtend(EditObj, Num)
	if Num == 0 then
		return false
	end

	local NumCnt = string.len(tostring(Num))
	local NumStr = tostring(Num)
	if NumCnt >= MaxTcpNumCnt then
		return false
	end

	for _, RangeInfo in ipairs(EditObj._TcpNumList) do
		for TempNum = RangeInfo.Min, RangeInfo.Max do
			local TempStrNum = tostring(TempNum)
			if (string.len(TempStrNum) > NumCnt) and (string.sub(TempStrNum, 1, NumCnt) == NumStr) then
				return true
			end
		end
	end

	return false
end

local function QuitTcpInput(EditObj)
	local PoundIndex = EditObj._InsertIndex - string.len(EditObj._TcpNumNameStr)
	CancelAllEffect(EditObj, PoundIndex, EditObj._InsertIndex)

	EditObj._TcpNumNameStr = ""
	EditObj._ProcRichStatus = NON_STATUS
	EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
end

local function TryParsSpeText(EditObj, Char, CharSize)
	if (EditObj._ProcRichStatus == NON_STATUS) and Char == SpeBeginChar then
		EditObj._ProcRichStatus = REC_POUND 
		return
	end

	if (EditObj._ProcRichStatus == REC_POUND) then
		local MatchFunc = TryMatchSpeFunc(Char)
		if MatchFunc then
			MatchFunc(EditObj, Char)
		else
		    --- 如果通过当前字符可以找到对应的链接类型则进行处理
		    if LIGHT_LINK.GetLinkTypeByChar(Char) then
		        LightLinkProc(EditObj, Char)
		    else
			    NoMatchSpeProc(EditObj)
			end
		end
		return
	end

	if (EditObj._ProcRichStatus == REC_CUSTOM_COLOR_TAG) then
		if IsCustomColorChar(Char) then
			if string.len(EditObj._CustomColorStr) >= CustomColorStrLen then
				DeleteWordForwardLeft(EditObj, CustomColorStrLen + 2)
				EditObj._ProcRichStatus = NON_STATUS
				EditObj._CustomColorStr = ""
			end
		else
			local PoundIndex = EditObj._InsertIndex - 2 - string.len(EditObj._CustomColorStr)
			CancelAllEffect(EditObj, PoundIndex, EditObj._InsertIndex)
			RenderTextEffect(EditObj, PoundIndex, EditObj._InsertIndex, EditObj._TextEffectDesc)

			EditObj._ProcRichStatus = NON_STATUS 
			EditObj._CustomColorStr = ""
		end
		return
	end

    --- 处理LightLink
	if (EditObj._ProcRichStatus == REC_ITEM_LINK) then
	    ParseEnd = EditObj._LightLinkObj:LinkParse(Char, CharSize, SpeBeginChar)
	    
	    if ParseEnd then
	        EditObj:InsertClickItemLink()
			EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
			EditObj._ProcRichStatus = NON_STATUS
	    end
	end

	if (EditObj._ProcRichStatus == REC_EMOTE_NUM) then
		if IsNumberChar(Char) then
			EditObj._TcpNumNameStr = EditObj._TcpNumNameStr .. Char

			local TcpNum = tonumber(EditObj._TcpNumNameStr)

			local AmongTag = IsAmongTcpRange(EditObj, TcpNum)
			local ExtendTag = CanTcpNumExtend(EditObj, TcpNum)

			if (not AmongTag) and (not ExtendTag) then
				QuitTcpInput(EditObj)
				return
			end

			if (not ExtendTag) then
				DeleteWordForwardLeft(EditObj, string.len(EditObj._TcpNumNameStr) + 1) -- 加上非匹配符 井号的长度
				InsertTcpElement(EditObj, EditObj._TcpNumNameStr, EditObj._InsertIndex)

				EditObj._TcpNumNameStr = ""
				EditObj._ProcRichStatus = NON_STATUS
				EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
				return
			end
		else
			if EditObj._TcpNumNameStr ~= "" then
				local TempChar = Char
				DeleteWordForwardLeft(EditObj, string.len(EditObj._TcpNumNameStr) + 2) -- 加上非匹配符 井号的长度
				InsertTcpElement(EditObj, EditObj._TcpNumNameStr, EditObj._InsertIndex)

				EditObj._TcpNumNameStr = ""
				EditObj._ProcRichStatus = NON_STATUS
				EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc

				EditObj:InsertChar(TempChar, CharSize)
			else
				QuitTcpInput(EditObj)
			end
		end
		return
	end
end
-- ele rec
-- line rec
-- line height rec
---------------------------------------------------------------------------------------------- ???
function clsLightMultiRichEdit:InsertChar(Char, CharSize)
	local InsertIndex = self._InsertIndex
	local TextObj = UI_TEXT.clsLightText:NewObj(self._TextMsgHandler, 0, 0, Char, self._Color, self._Font)
	TextObj:SetZ(TextZ)
	TextObj:SetShow(false)
	table.insert(self._ElementRec, InsertIndex + 1, { ElementObj = TextObj, DescInfo = { EleType = ELETYPE_TEXT, Text = Char, }, })
	self._InsertIndex = self._InsertIndex + 1

	TryConfigTextEffect(self, Char)
	RenderTextEffect(self, self._InsertIndex, self._InsertIndex, self._TextEffectDesc)
	TryParsSpeText(self, Char, CharSize)
end

function clsLightMultiRichEdit:DeleteSubElement(BeginIndex, EndIndex)
	local RemoveCount = EndIndex - BeginIndex + 1
	for i = 1, RemoveCount do
		self._ElementRec[BeginIndex].ElementObj:Release()
		table.remove(self._ElementRec, BeginIndex)
	end
end

local function GenTextEffDesc(TextObj)
	local EffDesc = {}
	local Color = TextObj:GetColor()
	EffDesc[EFF_COLOR] = Color
	if TextObj:HasChangeBig() then
		EffDesc[EFF_BIG] = true
	end
	
	if TextObj:HasBlink() then
		EffDesc[EFF_BLINK] = true
	end

	if TextObj:HasUnderLine() then
		EffDesc[EFF_UNDERLINE] = true
	end

	return EffDesc
end

local function ConvertTextEffToText(EffDesc)
	local TempStrTbl = {} 
	for Eff, Para in pairs(EffDesc) do
		if Eff == EFF_BLINK then
			table.insert(TempStrTbl, "#b")
		elseif Eff == EFF_UNDERLINE then
			table.insert(TempStrTbl, "#u")
		elseif Eff == EFF_BIG then
			table.insert(TempStrTbl, "#b")
		elseif Eff == EFF_COLOR then
			local ColorStr = string.format("%06x", Para)
			table.insert(TempStrTbl, string.format("#c%s", ColorStr))
		end
	end

	return table.concat(TempStrTbl)
end

local function DiffEffDesc(Desc1, Desc2)
	if table.size(Desc1) ~= table.size(Desc2) then
		return true
	end

	for k, v in pairs(Desc1) do
		if (not Desc2[k]) or (Desc2[k] ~= v) then
			return true
		end
	end
	
	return false
end

function clsLightMultiRichEdit:ConvertElementToText(ElementInfoList)
	local TempStrTbl = {} 
	local LastEffDesc = nil 
	for _, ElementInfo in pairs_orderly(ElementInfoList, KeyCompare) do -- 按从小到大的顺序遍历
		if ElementInfo.DescInfo.EleType == ELETYPE_TEXT then
			local Char = ElementInfo.DescInfo.Text
			if Char == SpeBeginChar then
				Char = string.rep(SpeBeginChar, 2)
			end

			if not LastEffDesc then
				LastEffDesc = GenTextEffDesc(ElementInfo.ElementObj)
				table.insert(TempStrTbl, string.format("%s%s", ConvertTextEffToText(LastEffDesc), Char))
			else
				local CurEffDesc = GenTextEffDesc(ElementInfo.ElementObj)
				if DiffEffDesc(LastEffDesc, CurEffDesc) then
					LastEffDesc = CurEffDesc
					table.insert(TempStrTbl, string.format("#n%s%s", ConvertTextEffToText(CurEffDesc), Char))
				else
					table.insert(TempStrTbl, Char)
				end
			end
		else
			table.insert(TempStrTbl, ElementInfo.DescInfo.Text)
		end
	end

	return table.concat(TempStrTbl)
end

function clsLightMultiRichEdit:GetLinkDescByRange(BeginIdx, EndIdx)
	local RetTbl = {}
	local RetList = {}
	for i = BeginIdx, EndIdx do
		if self._ElementRec[i].DescInfo.EleType == ELETYPE_ITEMDESC then
			local DescInfo = self._ElementRec[i].DescInfo
			table.insert(RetTbl, { key = DescInfo.LinkIndex, value = DescInfo.LinkDesc, })
			RetList[DescInfo.LinkIndex] = DescInfo.LinkDesc
		end
	end

	return RetTbl, RetList
end

function clsLightMultiRichEdit:GetText()
	local EleCnt = #(self._ElementRec)
	if EleCnt <= 0 then
		return ""
	end

	return self:GetTextByRange(1, EleCnt), self:GetLinkDescByRange(1, EleCnt)
end

function clsLightMultiRichEdit:NewLine(LineIndex, StartIdx, EndIdx, Segment)
	table.insert(self._LineRec, LineIndex, {})
	self._LineRec[LineIndex].StartIdx = StartIdx 
	self._LineRec[LineIndex].EndIdx = EndIdx 
	self._LineRec[LineIndex].Segment = Segment

	local ElementInfoList = self:GetSubElement(StartIdx, EndIdx)
	local LineMaxHeight = FindMaxHeight(self, ElementInfoList)
	table.insert(self._LineHeightRec, LineIndex, LineMaxHeight)
end

function clsLightMultiRichEdit:DeleteLine(LineIndex)
	table.remove(self._LineRec, LineIndex)
	table.remove(self._LineHeightRec, LineIndex)
end

function clsLightMultiRichEdit:GetEditHeight()
	local RetHeight = 0
	for LineIndex, LineHeight in ipairs(self._LineHeightRec) do
		RetHeight = RetHeight + LineHeight + self._LineInterval
	end

	return RetHeight
end

function clsLightMultiRichEdit:GetLineHeight(LineIndex)
	return self._LineHeightRec[LineIndex]
end

function clsLightMultiRichEdit:ResetEdit()
	for _, EleInfo in pairs(self._ElementRec) do
		EleInfo.ElementObj:Release()
	end
	self._ElementRec = {}
	self._InsertIndex = 0
	self._TextCursor:SetPos(0, 0)
	self._TextCursor:SetSize(CursorWidth, self._DefaultLineHeight)
	self._DesIndex = nil -- 选择文本的目标下标
	self._DesLine = nil
	self._CurLine = 1
	self._LineRec = {}
	self._LineRec[self._CurLine] = {}
	self._LineRec[self._CurLine].StartIdx = 0
	self._LineRec[self._CurLine].EndIdx = 0
	self._SegIndex = 1
	self._LineRec[self._CurLine].Segment = self._SegIndex 

	self._LineHeightRec = {}
	self._LineHeightRec[self._CurLine] = self._DefaultLineHeight
	self._ProcRichStatus = NON_STATUS 
	self._CustomColorStr = "" 

	self:TryNoticeListener(true)
	self._TextEffectDesc = {}
	self._NormalTextEffectDesc = {}

end

local function DoQuitSpeProc(EditObj)
	if EditObj._ProcRichStatus == REC_POUND then
		local DeleteCount = 1
		EditObj:DoDelete(EditObj._InsertIndex - DeleteCount + 1, EditObj._InsertIndex)
		EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
	end

	if EditObj._ProcRichStatus == REC_CUSTOM_COLOR_TAG then
		local TempStrLen = string.len(EditObj._CustomColorStr) + 2
		EditObj:DoDelete(EditObj._InsertIndex - TempStrLen + 1, EditObj._InsertIndex)
		EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
		EditObj._CustomColorStr = ""
	end

	if EditObj._ProcRichStatus == REC_EMOTE_NUM then
		local TempStrLen = string.len(EditObj._TcpNumNameStr) + 1
		EditObj:DoDelete(EditObj._InsertIndex - TempStrLen + 1, EditObj._InsertIndex)
		EditObj._TextEffectDesc = EditObj._NormalTextEffectDesc
		EditObj._TcpNumNameStr = ""
	end

	EditObj._ProcRichStatus = NON_STATUS
end

function clsLightMultiRichEdit:OnTextLeftDown(MouseX, MouseY)
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
		return
	end

	Super(clsLightMultiRichEdit).OnTextLeftDown(self, MouseX, MouseY)
end

function clsLightMultiRichEdit:DoBackProc()
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
		return
	end

	Super(clsLightMultiRichEdit).DoBackProc(self)
end

function clsLightMultiRichEdit:DoLeftProc()
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
		return
	end

	Super(clsLightMultiRichEdit).DoLeftProc(self)
end

function clsLightMultiRichEdit:DoRightProc()
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
		return
	end

	Super(clsLightMultiRichEdit).DoRightProc(self)
end

function clsLightMultiRichEdit:DoTryPasteProc()
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
	end

	Super(clsLightMultiRichEdit).DoTryPasteProc(self)
end

function clsLightMultiRichEdit:EditBackToStart()
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
	end

	Super(clsLightMultiRichEdit).EditBackToStart(self)
end

function clsLightMultiRichEdit:EditDeleteAfter()
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
	end

	Super(clsLightMultiRichEdit).EditDeleteAfter(self)
end

function clsLightMultiRichEdit:TrySelectAll()
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
	end

	Super(clsLightMultiRichEdit).TrySelectAll(self)
end

function clsLightMultiRichEdit:TryReturnProc()
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
	end

	Super(clsLightMultiRichEdit).TryReturnProc(self)
end

function clsLightMultiRichEdit:GetTextWidth()
	if #(self._ElementRec) <= 0 then
		return 0
	end

	if #(self._LineRec) > 1 then
		return self._TextMsgHandler:GetSize()
	end

	return self:CalElementSize(self._ElementRec)
end

function clsLightMultiRichEdit:GetSize()
	local RetWidth, _ = self._TextMsgHandler:GetSize()
	local RetHeight = CalSumLineHeight(self, #(self._LineRec)) 

	return RetWidth, RetHeight
end

local MyText = nil
function TestFunc()
	local TextInfo = {}
	TextInfo.Width = 100 
	TextInfo.MaxCharCount = 100
	TextInfo.MaxTextWidth = 100 
	TextInfo.LineInterval = 3
	TextInfo.MinMsgHeight = 70
	TextInfo.Font = FONT_INFO["中标题"]

	MyText = clsLightMultiRichEdit:NewObj(nil, 100, 100, TextInfo)
end

function Destroy()
	MyText:Release()
end

