CHATDETAIL_LAYOUT = Import("layout/chat_detail.lua")
local DetailConfig = CHATDETAIL_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local main_say_panel = nil
local detailPage = nil
local detail_data = {}
local user_info = nil

local total_width = 480
local main_height_1 = 0
local main_height_2 = 0
local divider_pic2 = nil

local reply_user_id = nil

local user_chat_id = nil		--用于临时保存聊天记录id
local loadedPage = 1
local max_page = 10

local freshing = false

local detail_desc_info = {
	item_width = total_width,
	item_height = 140,
	column_cnt = 1,
	x_space = 0,
	y_space = 0,
}

local function on_reply_cb(data)
	onDetailTouchEnd()
	setCacheExpires( 'comment.lists.2.1' )
	setCacheExpires( 'comment.lists.3.1' )
end

local function onReply(content)
	local function filterChar( msg )
		local i = 1
		local str = " "
		while(i <= string.len(msg)) do
			local b = string.sub(msg,1,i)		
			if b ~= str then
				return true
			end
			str = str .. " "
			i = i + 1
		end
		return false
	end	
	local sayContent = filterChar( content )
	if sayContent == true then		
		if string.len( content ) > 0 then
			local url = GLOBAL.interface .. "comment.php?sid=" .. GLOBAL.sid

			if tonumber( detailPage._chat_id ) > 0 then
				--local post_data = { id = detailPage._chat_id, content = content, kind = 3 }
				local post_data = { id = reply_user_id, content = content, kind = 3 }			
				if pic_path then post_data.file = pic_path end
				if post_data.file then
					post_data.type = 'answer'
					post_data.sid = GLOBAL.sid
					HTTP_REQUEST.http_upload( url, post_data, on_reply_cb )
				else
					doCommand( 'comment', 'answer', post_data, on_reply_cb )
				end
			else
				local post_data = { content = content }
				if pic_path then post_data.file = pic_path end

				if post_data.file then
					post_data.type = 'say'
					post_data.sid = GLOBAL.sid
					HTTP_REQUEST.http_upload( url, post_data, on_reply_cb )
				else
					doCommand( 'comment', 'say', post_data, on_reply_cb )
				end
			end				
		end	
	else
		showMessage( detailPage, "输入不能为空...", {ms = '3000'})
	end	
				
end

local function onDelayreply(data)
	f_delay_do( detailPage, onReply, data,1 )
end
local tryConfigPageByData
function onDetailTouchEnd()	
	if not detailPage then
		return
	end
	detail_data[detailPage._chat_id] = nil
	
	freshing = true
	tryConfigPageByData(detailPage._chat_id)
	loadedPage = 1			
end

local function blankPage()
	detailPage._move_detail_page._detail_page:clearAllItem()
end


local function createMoveDetailPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = DetailConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_detail_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config["view_height"])
	ret_panel._move_detail_grp:setVMovable(true)

	local detail_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, detail_desc_info)
	detail_page._use_dynamic_size = true
	detail_page:refresh_view()

	local function onDetailOverTop(layout)
		--[[detail_data[detailPage._chat_id] = nil
		blankPage()
		tryConfigPageByData(detailPage._chat_id)
		loadedPage = 1--]]
	end

	local function onDetailOverBottom(layout)
		if loadedPage < max_page then		
			loadedPage = loadedPage + 1
			doCommand( 'comment', 'view', { id = detailPage._chat_id, page = loadedPage, size = 10 }, on_get_detail_data )
		end			
	end	

	detail_page.onHMoveOverTop = onDetailOverTop
	detail_page.onHMoveOverBottom = onDetailOverBottom
	ret_panel._move_detail_grp.touchEnd = onDetailTouchEnd
	ret_panel._move_detail_grp.resetListPos = resetListPosition
	ret_panel._move_detail_grp.tipHeight = 750

	detail_page:setPosition(0, config["inner_y"])
	
	ret_panel._move_detail_grp:appendItem(detail_page)
	ret_panel._move_detail_grp:selectPage(1)

	ret_panel._detail_page = detail_page

	local function get_y_limit(obj)
		return config["inner_y"] - 60
	end

	detail_page.getYLimit = get_y_limit
	
	return ret_panel
end

local CHAT_PAGE = Import("lua/chat.lua")
local REPLY_PAGE = Import("lua/reply.lua")

--[[local function on_reply_cb(data)
	--CHAT_PAGE.showChatPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	setCacheExpires( 'comment.lists.2.1' )
	setCacheExpires( 'comment.lists.3.1' )
end--]]

local back_func = nil

function init( data )
	uid = GLOBAL.uid
	
	if data.back_func then
		back_func = data.back_func
	end
end	

local function okHandler( content )
	dailyCount('SayWordsReply')--回复说两句
	local function filterChar( content )
		local i = 1
		local str = " "
		while(i <= string.len(content)) do
			local b = string.sub(content,1,i)		
			if b ~= str then
				return true
			end
			str = str .. " "
			i = i + 1
		end
		return false
	end		
	local sayContent = filterChar( content )	
	if sayContent == true then
		if string.len( content ) > 0 then
			doCommand( 'comment', 'answer', { id = detailPage._chat_id, content = content, kind = 1 }, on_reply_cb )
		end			
	else
		showMessage( detailPage, "输入不能为空...", {ms = '3000'})
	end		
end

local function createDetailPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )
	
	detailPage = ret_panel

	local c, c2 = DetailConfig, nil
	local o = nil

	local function on_back(btn)
		closeDetailPagePanel()

		if back_func then
			back_func()
		else
			--CHAT_PAGE.showChatPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
			--if PANEL_CONTAINER.closeChildPanel( nil, 2 ) then
				--PANEL_CONTAINER.addChild( CHAT_PAGE.showChatPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			--end
		end
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '详情' )
	
	config = DetailConfig["move_page_config"]
	ret_panel._move_detail_page = createMoveDetailPage(ret_panel, config["x"], config["y"])
	
	

	local function on_ok(content)
		f_delay_do( ret_panel, okHandler, content, 0.1 )		
	end

	local function on_cancel()
		showDetailPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, detailPage._chat_id)
	end

	local function on_reply_click(obj, x, y)
		local page_info = {
			title = "回复",
			desc = "",
			id = detailPage._chat_id,
			nophoto = 1,
		}
		--REPLY_PAGE.showReplyPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, on_cancel, page_info, nil)
		--closeDetailPagePanel()
		xymodule.mulitiInput( "", on_ok )	
		--on_ok( " " )
		--doCommand( 'comment', 'answer', { id = detailPage._chat_id, content = "hao", kind = 1 }, on_reply_cb )		
	end
	config = DetailConfig["replybtn_config"]
	ret_panel._reply_btn = LIGHT_UI.clsButton:New(ret_panel, config.x, config.y, config.normal_res, config.click_res)
	ret_panel._reply_btn.onTouchEnd = on_reply_click


	return ret_panel
end

local function after_get_http_pic( parent, obj )
	local size = obj:getContentSize()		
	local size_old = parent.getContentSize()
	local pos_old_x, pos_old_y = obj:getPosition()
	local pic_new_width = 440
	local pic_new_height = 0

	if size.width > 440 then
		pic_new_width = 440
	else
		pic_new_width = size.width
	end

	pic_new_height = pic_new_width * size.height / size.width
	
	obj:setContentSize( pic_new_width, pic_new_height )
	obj:setPosition( ( total_width - pic_new_width ) / 2, pos_old_y )
	
	local height = pic_new_height + 10
	
	local list = { '_reply_num_pic_obj', '_reply_num_obj', '_bottom_split_line', 'bg' }
	
	for _,v in pairs( list ) do
		local x, y = parent[v]:getPosition()
		parent[v]:setPosition( x, y - height )
	end
	local bx, by = parent[ 'bg' ]:getPosition()
	parent[ 'bg' ]:setScaleY( -by + 90 )
	
	detailPage._move_detail_page._detail_page:refresh_view()
end



local function createUserInfoPanel( chat_id )
	local c, c2 = DetailConfig, nil
	
	if not detail_data[chat_id] then
		return
	end
	info = detail_data[chat_id].first
	
	local tr = LIGHT_UI.clsTr:New( DetailConfig )
	local ret_panel = tr._RootNode
	
	ret_panel.bg = createSprite( ret_panel, {x=0,y=0,ax = 0,ay=0, sx = 480,sy = 100,res='baidian.png'} )
	
	ret_panel.tip = createLabel( ret_panel, {x = 100, y = 30, ax = 0, ay = 0.5, text = '等侯刷新', css='c1'} )
	xCenter( ret_panel.tip )
	local locX, locY = ret_panel.tip:getPosition()
	ret_panel.tip:setPosition( locX + 10, locY )
	locX, locY = ret_panel.tip:getPosition()
	ret_panel.arrow = createSprite( ret_panel, {x = locX - 20, y = 30, res = 'arrow_down.png'} )	
	ret_panel.loading = createSprite(  ret_panel, {x = locX - 20, y = 30, res = 'white_dot.png'})
	ret_panel.loading:setVisible( false )
	
	local function click_logo()
		user_chat_id = chat_id
		closeDetailPagePanel()

		local function back_func()
			showDetailPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, user_chat_id )
		end

		PLAYER_INFO_PANEL.showByUserId( info["user"] )
		PLAYER_INFO_PANEL.init( { back_func = back_func })
	end
	
	ret_panel.chat_pic_bg = createButton( ret_panel, DetailConfig.chat_pic_bg, click_logo )
	
	tr:addSprite( 'logo', info.logo )
	
	if info.vip_id ~= '0' then
		local vip_logo = string.format( 'vip%i.png', info.vip_id )
		
		tr:addSprite( 'vip', vip_logo )
	end
	
	local l = tr:addLabel( 'name', info.name )
	AdjustString(l,info.name,100)
	
	tr:addLabel( 'time', info.time )
	
	tr:addSprite( 'split_line' )
	
	tr:addMultiLabel( 'content', info.content )
	
	c.reply_pic.x = PAGE.time.x1 - PAGE.content.x0
	local reply_num_pic_obj = tr:addSprite( 'reply_pic' )
	local reply_num_obj = tr:addLabel( 'replys', info.replys )
	if info.replys % 10 == 0 then
		max_page = info.replys / 10
	else
		max_page = info.replys / 10 + 1
	end
	
	c2 = deepcopy( DetailConfig.split_line )
	c2.id = 'bottom'
	c2.y = PAGE.reply_pic.y0 - DetailConfig['other_config']['margin_4']
	ret_panel.bg:setPosition( 0, c2.y - 20 )
	local bottom_split_line = createSprite( ret_panel, c2 )
	bottom_split_line:setVisible( false )

	ret_panel._reply_num_pic_obj = reply_num_pic_obj
	ret_panel._reply_num_obj = reply_num_obj
	ret_panel._bottom_split_line = bottom_split_line
	
	ret_panel.bg:setScaleY( -c2.y + 110 )
	
	local function getContentSize()
		local _, y = ret_panel._bottom_split_line:getPosition()
		return { width = total_width, height = -y, }
	end

	ret_panel.getContentSize = getContentSize
	
	local function loadedBigPic( obj )	
		return after_get_http_pic( ret_panel, obj )		
	end
	
	if string.len( info.large ) > 0 then
		tr:addSprite( 'say_pic_c', info.large, loadedBigPic )
				
	end	
	
	return ret_panel
end	

local function createReplyItem( info )
	
	local ret_panel = LIGHT_UI.clsNode:New( nil, 0, 0 )

	PAGE[ 'xyz' ] = {
			x = 0,
			y = 0,
			sx = 0,
			sy = 0,
			ax = 0,
			ay = 0,
		}

	PAGE[ 'xyz2' ] = {
			x = total_width,
			y = 0,
			sx = 0,
			sy = 0,
			ax = 0,
			ay = 0,
		}

	local function on_ok( content )		
		doCommand( 'comment', 'answer', { id = reply_info["id"], content = content, kind = 1 }, on_reply_cb )
		showDetailPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, detailPage._chat_id)
	end

	local function on_cancel()
		showDetailPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, detailPage._chat_id )
	end

	local function click_reply( obj, x, y )
		local page_info = {
			title = "回复",
			desc = "",
			id = detailPage._chat_id,
			nophoto = 1,
		}
		REPLY_PAGE.showReplyPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, on_cancel, page_info, reply_info )
		closeDetailPagePanel()
	end
	
	local function reply()
		reply_user_id = info[ "id" ]
		xymodule.mulitiInput( "", onDelayreply )
		--onDelayreply( " " )	
		--onReply( "Hello" )	
	end
	
	local function click_logo()
		user_chat_id = detailPage._chat_id
		closeDetailPagePanel()

		local function back_func()
			showDetailPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, user_chat_id )
		end

		PLAYER_INFO_PANEL.showByUserId( info["user"] )
		PLAYER_INFO_PANEL.init( { back_func = back_func })
	end
	
	
	--local reply_pic_bg = createButton( ret_panel, DetailConfig.logo_pic_c2, click_logo )	

	local reply_whole_bg = createFrameButton( ret_panel, DetailConfig.reply_whole_bg )
	local reply_pic_bg = createFrameButton( ret_panel, DetailConfig.reply_pic_bg )
	setMsgDelegate( reply_whole_bg, detailPage._move_detail_page._move_detail_grp, reply )
	setMsgDelegate( reply_pic_bg, detailPage._move_detail_page._move_detail_grp, click_logo )
	--setMsgDelegate( reply_whole_bg, detailPage._move_detail_page._move_detail_grp, click_reply )

	
	
	DetailConfig.logo_pic_c2.res = info["logo"]
	createSprite( ret_panel, DetailConfig.logo_pic_c2 )	
	
	DetailConfig.username_c2.text = info["name"]
	local username_obj = createLabel( ret_panel, DetailConfig.username_c2 )	
	AdjustString(username_obj,info["name"],100)

	DetailConfig.say_time_c2.text = info["time"]
	createLabel( ret_panel, DetailConfig.say_time_c2 )

	DetailConfig.say_content_c2.text = info["content"]
	local content_obj = createMultiLabel( ret_panel, DetailConfig.say_content_c2 )

	local content_x, content_y = content_obj:getPosition()
	local content_size = content_obj:getContentSize()
	local height = math.abs( content_y ) + content_size.height
	
	local replyHeight = 0

	if info["prev_content"] then
		ret_panel.reply_bg = create9Sprite( ret_panel, DetailConfig.replg_bg_c )

		local text = string.format( "%s说: %s", info["prev_name"], info["prev_content"] )
		--text = string.sub( text, 1, DetailConfig.say_content_c3.len ) .. '...'
		DetailConfig.say_content_c3.text = text
		local content2_obj = createMultiLabel( ret_panel, DetailConfig.say_content_c3 )		
		local content2_x, content2_y = content2_obj:getPosition()
		local content2_size = content2_obj:getContentSize()
		replyHeight = content2_size.height
		height = math.abs( content2_y ) + content2_size.height
		ret_panel.reply_bg:setSpriteContentSize( content2_size.width + 20, content2_size.height )
	end

	if height < DetailConfig.logo_pic_c2.sy then height = DetailConfig.logo_pic_c2.sy end
	height = height + DetailConfig['other_config']['margin_2']
	reply_whole_bg:setContentSize( DetailConfig.reply_whole_bg.sx, height )
	reply_pic_bg:setContentSize( DetailConfig.reply_pic_bg.sx, 93 )
	
	ret_panel.line = createSprite( ret_panel, DetailConfig.line )
	--ret_panel.line:setPosition( 0, height - 20 )
	--[[if content_size.height + replyHeight < 150 then
		ret_panel.line:setPosition( 0, content_size.height + replyHeight - 20 )
	else
		ret_panel.line:setPosition( 0, 160 )			
	end--]]

	local function getContentSize( obj )
		return { width = total_width, height = height, }
	end

	ret_panel.getContentSize = getContentSize
	
	
	return ret_panel
end

local addHead = nil

local function doConfigPageByData(chat_id)
	local row_obj = detailPage._move_detail_page._detail_page
	if freshing and freshing == true then
		freshing = false
		blankPage()
		resetListPosition()
		
	end
	if addHead == true then
		addHead = false
		if createUserInfoPanel(chat_id) then
			row_obj:append_item(createUserInfoPanel(chat_id))
		end
	end

	if not detail_data[chat_id] then
		return
	end
	local reply_data = detail_data[chat_id].lists.data
	for _, reply_info in ipairs(reply_data) do
		row_obj:append_item(createReplyItem(reply_info))
	end
	
	row_obj:refresh_view()
	
end

function resetListPosition()
	local config = DetailConfig["move_page_config"]
	detailPage._move_detail_page._detail_page:setPosition(0, config["inner_y"])	
end

function on_get_detail_data(data)
	if detailPage then
		if data.code == -1 then
			showMessage( detailPage, "帖子不存在", {3000} )
		else
			detail_data[detailPage._chat_id] = data.data
			doConfigPageByData(detailPage._chat_id)
		end	
	end
end

tryConfigPageByData = function(chat_id)
	if not detail_data[chat_id] then
		addHead = true
		doCommand( 'comment', 'view', { id = chat_id, page = 1, size = 10 }, on_get_detail_data )
	else
		doConfigPageByData(chat_id)
	end
end

function showDetailPagePanel(parent, x, y, chat_id)
	back_func = nil
	detail_data = {}
	--if not detailPage then
		detailPage = createDetailPagePanel(parent, x, y)
	--else
		--detailPage:setVisible(true)
	--end

	detailPage._chat_id = chat_id

	blankPage()
	tryConfigPageByData(detailPage._chat_id)
end

function closeDetailPagePanel()
    blankPage()
	detailPage._move_detail_page._detail_page = nil	
	if detailPage._move_detail_page._move_detail_grp._move_cb then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(detailPage._move_detail_page._move_detail_grp._move_cb)	
		detailPage._move_detail_page._move_detail_grp._move_cb = nil
	end
	detailPage._move_detail_page._move_detail_grp = nil
	detailPage._move_detail_page = nil
	clear({detailPage})
	detailPage = nil
	--detailPage:setVisible(false)
end