-- select page panel

local selectPage = nil

local info_desc_info = {
	item_width = 480,
	item_height = 60,
	column_cnt = 1,
	x_space = 2,
	y_space = 4,
}

local function createSelectPagePanel(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	ret_panel._bg_pic = LIGHT_UI.clsSprite:New(ret_panel, 40, 50, "tiny_white_pixel.png")
	ret_panel._bg_pic:setAnchorPoint(0, 0)
	ret_panel._bg_pic:setScaleX(400)
	ret_panel._bg_pic:setScaleY(700)

	ret_panel._top_bg = LIGHT_UI.clsSprite:New(ret_panel, 40, 750, "tiny_black_pixel.png")
	ret_panel._top_bg:setAnchorPoint(0, 1)
	ret_panel._top_bg:setScaleX(400)
	ret_panel._top_bg:setScaleY(100)

	ret_panel._top_title = LIGHT_UI.clsLabel:New(ret_panel, 226, 700, "", GLOBAL_FONT, 21)
	ret_panel._top_title:setAnchorPoint(0.5, 0.5)
	ret_panel._top_title:setTextColor(255, 255, 255)

	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_info_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, 630)
	ret_panel._move_info_grp:setVMovable(true)

	local select_layout = LIGHT_UI.clsRowLayout:New(nil, 0, 0, info_desc_info)
	select_layout:refresh_view()

	local function get_y_limit(obj)
		return 600
	end

	select_layout:setPosition(0, 600)
	ret_panel._move_info_grp:appendItem(select_layout)

	select_layout.getYLimit = get_y_limit
	ret_panel._select_layout = select_layout 

	ret_panel._move_info_grp:selectPage(1)

	return ret_panel
end

local CHALLENGE_PAGE = Import("lua/challenge.lua")

local function createSelItem(txt, select_cb)
	local node_obj = LIGHT_UI.clsNode:New(nil, 0, 0)

	local function on_click(obj, x, y)
		select_cb(txt)
		closeSelectPagePanel()
	end
	local msg_obj = LIGHT_UI.clsFrameButton:New(node_obj, 40, 23, 400, 50, "tiny_bg_pixel.png", "tiny_yellow_pixel.png")
	msg_obj:hideLine()

	CHALLENGE_PAGE.setMsgDelegate(msg_obj, selectPage._move_info_grp, on_click)

	local txt_obj = LIGHT_UI.clsLabel:New(node_obj, 240, 0, txt, GLOBAL_FONT, 16)
	txt_obj:setTextColor(255, 0, 0)

	return node_obj
end

local function setPage(title, txt_list, select_cb)
	selectPage._top_title:setString(title)
	local select_layout = selectPage._select_layout
	select_layout:clearAllItem()
	for _, txt in ipairs(txt_list) do
		select_layout:append_item(createSelItem(txt, select_cb))
	end
	select_layout:refresh_view()
end

function showSelectPagePanel(title, txt_list, select_cb)
	HTTP_CLIENT.showMask()
	if not selectPage then
		selectPage = createSelectPagePanel(HTTP_CLIENT.getMaskNode(), 0, 0)
	else
		selectPage:setVisible(true)
	end
	setPage(title, txt_list, select_cb)
end

function closeSelectPagePanel()
	--selectPage:setVisible(false)
	HTTP_CLIENT.hideMask()
	clear(selectPage)
	selectPage = nil
end

