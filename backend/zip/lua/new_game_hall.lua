NEWGAMEHALL_LAYOUT = Import("layout/new_game_hall.lua")
local newGameConfig = NEWGAMEHALL_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
LIST_ITEM = Import( "lua/list_item.lua" )
ret_panel = nil

local function clickButtonHandler( t, x, y )
	PANEL_CONTAINER.clickButton()
	if t._btn_res_list[ 1 ] == 'sz-off.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 15 ) then	--随意传个非空的table，该参数如果为空，就表明需要清除子面板，否则，就会考虑第2个参数
			dailyCount('Setting')--设置
			local SETTING = Import("lua/setting.lua")							
			PANEL_CONTAINER.addChild( SETTING.showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
		end	
	elseif t._btn_res_list[ 1 ] == 'tx.png' then
		if PANEL_CONTAINER.closeChildPanel( t, 16 ) then	--随意传个非空的table，该参数如果为空，就表明需要清除子面板，否则，就会考虑第2个参数							
			PANEL_CONTAINER.addChild( USER_INFO_PANEL.showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )				
		end	
	end
end

function updateGoldText()
	--回调用户信息
	local function onUserMoney( data )
		ret_panel._gold:setString( data.data.gold )
	end
	--请求用户资料
	doCommand( 'user', 'money', {}, onUserMoney )
end

function updateMessageNum( num )
	if num ~= 0 then
		ret_panel.newPic1:setString( "+" .. num )
		ret_panel.msg_purper_bg:setVisible( true )
	else
		ret_panel.newPic1:setString( "" )
		ret_panel.msg_purper_bg:setVisible( false )
	end
end

function updateOnlineNum()
	local function onMsg( data )
		if data.data.person then
			ret_panel.onlineNum:setString( data.data.person .. "人" )
		end	
	end
	doCommand( 'room', 'info', {}, onMsg )
end

function setLogo()
	local user_info = LOGIN_PANEL.getUserData()
	ret_panel._user_pic:setImage( user_info["logo"] )
end

local function createGameHall( parent, x, y )
	local ret_panel = createNode( parent, point( x, y ) )		
	ret_panel.bg = createSprite( ret_panel, newGameConfig.bg_config )
	--ret_panel.select = createSprite( ret_panel, newGameConfig.select_config )
	
	local user_info = LOGIN_PANEL.getUserData()
	--print( "logo:" .. user_info["logo"] )
	
	newGameConfig._user_pic.res = user_info["logo"]
	ret_panel._user_pic = createSprite( ret_panel, newGameConfig._user_pic )--头像位置调整
	ret_panel._user_btn = createButton( ret_panel, newGameConfig.user_button, clickButtonHandler )
	
	ret_panel.h_line = createSprite( ret_panel, newGameConfig.h_line )
	
	
	ret_panel.user_name = createLabel( ret_panel, newGameConfig.user_name )
	ret_panel.user_name:setString( user_info["name"] )
	ret_panel.user_name:setTextColor( 197, 118, 28 )
	
	ret_panel.gold_pic = createSprite( ret_panel, newGameConfig.gold_pic )
	
	ret_panel._gold = createLabel( ret_panel, newGameConfig.gold_text )
	ret_panel._gold:setTextColor( 197, 118, 28 )
	

	
	ret_panel.setting_btn = createButton( ret_panel, newGameConfig.setting_button, clickButtonHandler )
	
	local makeFriendArr = { newGameConfig.good_friend, newGameConfig.chat, newGameConfig.dynamic, newGameConfig.discuss, newGameConfig.meetByChance }
	local makeFriendRect = { newGameConfig.good_friend_rect, newGameConfig.chat_rect, newGameConfig.dynamic_rect, newGameConfig.discuss_rect, newGameConfig.meetByChance_rect }
	--local makeFriendArr = { newGameConfig.good_friend, newGameConfig.chat, newGameConfig.dynamic, newGameConfig.discuss, }
	--local makeFriendRect = { newGameConfig.good_friend_rect, newGameConfig.chat_rect, newGameConfig.dynamic_rect, newGameConfig.discuss_rect, }	
	--local richesArr = { newGameConfig.goodMall, newGameConfig.getGold, newGameConfig.luckyTurnplate }
	--local richesRect = { newGameConfig.goodMall_rect, newGameConfig.getGold_rect, newGameConfig.luckyTurnplate_rect }
	local richesArr = { newGameConfig.goodMall, newGameConfig.getGold }
	local richesRect = { newGameConfig.goodMall_rect, newGameConfig.getGold_rect }
	--local richesArr = {  }
	--local richesRect = {  }
	local gameArr = { newGameConfig.game, newGameConfig.message }
	local gameRect = { newGameConfig.game_rect, newGameConfig.message_rect }
	LIST_ITEM.showListItem( ret_panel, newGameConfig.game_list_item.x, newGameConfig.game_list_item.y, newGameConfig.game_list_item.text, gameArr, gameRect, 1 )
	LIST_ITEM.showListItem( ret_panel, newGameConfig.makefriend_list_item.x, newGameConfig.makefriend_list_item.y, newGameConfig.makefriend_list_item.text, makeFriendArr, makeFriendRect, 2 )	
	LIST_ITEM.showListItem( ret_panel, newGameConfig.riches_list_item.x, newGameConfig.riches_list_item.y, newGameConfig.riches_list_item.text, richesArr, richesRect, 3 )		
	
	ret_panel.msg_purper_bg = createSprite( ret_panel, newGameConfig.msg_purper_bg )
	ret_panel.chat_purper_bg = createSprite( ret_panel, newGameConfig.chat_purper_bg )
	ret_panel.dynamic_purper_bg = createSprite( ret_panel, newGameConfig.dynamic_purper_bg )
	--ret_panel.discuss_purper_bg = createSprite( ret_panel, newGameConfig.discuss_purper_bg )
	--ret_panel.meet_by_chance_purper_bg = createSprite( ret_panel, newGameConfig.meet_by_chance_purper_bg )	
	
	--ret_panel.onlineNum = createLabel( ret_panel, newGameConfig.online_num )
	ret_panel.hot_bg = createSprite( ret_panel, newGameConfig.hot_bg )
	a_play( ret_panel.hot_bg:getSprite(),newGameConfig.hot_animation,true)
	ret_panel.newPic1 = createLabel( ret_panel, newGameConfig.new_pic1 )
	--ret_panel.newPic2 = createLabel( ret_panel, newGameConfig.new_pic2 )
	--ret_panel.newPic3 = createLabel( ret_panel, newGameConfig.new_pic3 )
	ret_panel.newUser1 = createLabel( ret_panel, newGameConfig.new_user1 )
	ret_panel.newUser2 = createLabel( ret_panel, newGameConfig.new_user2 )
	
	ret_panel.h_line2 = createSprite( ret_panel, newGameConfig.white_line_up )
	ret_panel.h_line3 = createSprite( ret_panel, newGameConfig.white_line_down )
	
	--updateOnlineNum()
	local isShake = getUserString( "shake" )
	if not isShake then
		setUserString( "shake", 1 )
	end
	
	return ret_panel
end

function showGameHall( parent, x, y )
	ret_panel = createGameHall( parent, 0, 0 )	
end	