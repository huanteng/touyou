-- 9
local config = {
	green_btn = {    --绿色按钮
		x = 0,
		y = 0,
		width = 17,
		height = 59,
	},
	green_btn_cut = {
		x = 7,
		y = 7,
		width = 3,
		height = 45,
	},
	blue_btn = {  --蓝色按钮
		same = 'green_btn',
	},		
	yellow_btn = {    --黄色按钮
		x = 0,
		y = 0,
		width = 17,
		height = 59,
	},
	yellow_btn_cut = {
		x = 7,
		y = 7,
		width = 3,
		height = 45,
	},
	gray_btn = {  --灰色按钮
		x = 0,
		y = 0,
		width = 17,
		height = 59,
	},
	gray_btn_cut = {
		x = 7,
		y = 7,
		width = 3,
		height = 45,
	},
	ic_first = {  --上圆角内容底衬
		x = 0,
		y = 0,
		width = 32,
		height = 32,
	},
	ic_first_cut = {
		x = 15,
		y = 15,
		width = 2,
		height = 2,
	},	
	ic_normal = {  --无圆角内容底衬
		same = 'ic_first',
	},
	ic_last = {  --下圆角内容底衬
		same = 'ic_first',
	},
	ic_single = {  --圆角输入区
		x = 0,
		y = 0,
		width = 32,
		height = 32,
	},
	ic_single_cut = {
		x = 15,
		y = 15,
		width = 2,
		height = 2,
	},
	ic_first_on = {  --上圆角内容底衬
		same = 'ic_first',
	},
	ic_normal_on = {  --无圆角内容底衬
		same = 'ic_first',
	},
	ic_last_on = {  --下圆角内容底衬
		same = 'ic_first',
	},
	ic_single_on = {  --圆角输入区
		same = 'ic_first',
	},		
	say_content = {  --说两句内容底衬
		x = 0,
		y = 0,
		width = 23,
		height = 37,
	},
	say_content_cut = {
		x = 19,
		y = 32,
		width = 1,
		height = 1,
	},	
	userdetail_line = {  --动态内容标记线
		x = 0,
		y = 0,
		width = 22,
		height = 46,
	},
	userdetail_line_cut = {
		x = 20,
		y = 45,
		width = 0,
		height = 1,
	},	
	photo_bg = {  --照片底衬
		x = 0,
		y = 0,
		width = 23,
		height = 25,
	},
	photo_bg_cut = {
		x = 12,
		y = 12,
		width = 1,
		height = 1,
	},
	checkbox = {
		x = 0,
		y = 0,
		width = 20,
		height = 19,
	},
	checkbox_cut = {
		x = 2,
		y = 2,
		width = 1,
		height = 1,
	},
}
--------------------------------------------------------------

function getConfigData()
	return config
end
