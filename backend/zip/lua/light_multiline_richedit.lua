-- multi rich edit
local MULTI_EDIT = Import("lua/light_multiline_edit.lua")
clsLightMultiRichEdit = MULTI_EDIT.clsLightMultiEdit:Inherit()

--[[
_ElementRec = {
[1] = {
	ElementObj = ,
	DescInfo = {
		EleType = ,
	},
},
}
--]]

--[[
TextInfo = {
	Color = ,
	Font =,
	Width = ,
	MaxCharCount = ,
	MaxTextWidth = ,
	LineInterval = ,
	MinMsgHeight = ,
}
--]]

local ElementInterval = 1 

local ELETYPE_TEXT = 1
local ELETYPE_TCP = 2

local SpeBeginChar = "#"

local EFF_BLINK = 1
local EFF_UNDERLINE = 2
local EFF_COLOR = 3
local EFF_BIG = 4

local NON_STATUS = 0
local REC_POUND = 1
local REC_CUSTOM_COLOR_TAG = 2
local REC_EMOTE_NUM = 3
local REC_ITEM_LINK = 4

local EmoteSpeed = 10

local NormalTcpNumList = {
	[1] = {
		Min = 1,
		Max = 75,
	},
}

function clsLightMultiRichEdit:__init__(ParentObj, BaseX, BaseY, TextInfo)
	Super(self).InitCommon(self, ParentObj, BaseX, BaseY, TextInfo)

	local width, height = Super(self).CalElementSize(self, " ")
	self._DefaultLineHeight = height
	self._LineHeightRec = {}
	self._LineHeightRec[self._CurLine] = self._DefaultLineHeight
	self._ProcRichStatus = NON_STATUS 
	self._TcpNumNameStr = ""
	self._TcpNumList = NormalTcpNumList 
end

local function DeleteWordForwardLeft(EditObj, Count)
	EditObj:DeleteSubElement(EditObj._InsertIndex - Count + 1, EditObj._InsertIndex)
	EditObj._InsertIndex = EditObj._InsertIndex - Count
end

function clsLightMultiRichEdit:CalLineByMouseY(MouseY)
	MouseY = self._MinMsgHeight - MouseY
	local SumHeight = 0
	for LineIndex, LineHeight in ipairs(self._LineHeightRec) do
		SumHeight = SumHeight + LineHeight + self._LineInterval

		if SumHeight >= MouseY then
			return LineIndex
		end
	end

	return #(self._LineHeightRec)
end

function clsLightMultiRichEdit:CalElementSize(ElementInfoList)
	local Width, Height = 0, 0
	for _, ElementInfo in pairs(ElementInfoList) do
		local size = ElementInfo.ElementObj:getContentSize()
		local ElementWidth, ElementHeight = size.width, size.height
		Width = Width + ElementWidth + ElementInterval
		if ElementHeight > Height then
			Height = ElementHeight
		end
	end

	return Width, Height
end

function clsLightMultiRichEdit:GetSubElement(BeginIndex, EndIndex)
	local ElementInfoList = {}
	if BeginIndex <= 0 then
		return ElementInfoList
	end

	for i = BeginIndex, EndIndex do
		ElementInfoList[i] = self._ElementRec[i]
	end

	return ElementInfoList
end

local function FindMaxHeight(EditObj, ElementInfoList)
	local Height = EditObj._DefaultLineHeight 
	for _, ElementInfo in pairs(ElementInfoList) do
		local size = ElementInfo.ElementObj:getContentSize()
		local TempHeight = size.height
		if TempHeight > Height then
			Height = TempHeight
		end
	end

	return Height
end

local function CalSumLineHeight(EditObj, LineIndex)
	local RetHeight = 0
	for i = 1, LineIndex do
		RetHeight = RetHeight + EditObj._LineHeightRec[i] + EditObj._LineInterval
	end

	return RetHeight
end

local function KeyCompare(k1, k2)
	if k1 < k2 then
		return true
	else
		return false
	end
end

function clsLightMultiRichEdit:CalTextObjY(LineIndex)
	if LineIndex == 1 then
		return self._MinMsgHeight 
	else
		return self._MinMsgHeight - CalSumLineHeight(self, LineIndex - 1)
	end
end

local function pairs_orderly(tbl, comp)
	local keys = {}
	table.foreach(tbl, function (k,v) table.insert(keys, k) end)
	table.sort(keys, comp)
	local keys_count = #keys
	local index = 0

	local next_orderly = function(tbl)
		index = index + 1
		if index > keys_count then
			return
		end
		return keys[index], tbl[keys[index]]
	end

	return next_orderly, tbl
end

local function DisplayElementList(EditObj, ElementInfoList, LineIndex)
	local LineMaxHeight = FindMaxHeight(EditObj, ElementInfoList)
	EditObj._LineHeightRec[LineIndex] = LineMaxHeight

	local StartPosX = 0
	local StartPosY = EditObj:CalTextObjY(LineIndex)

	for Index, ElementInfo in pairs_orderly(ElementInfoList, KeyCompare) do
		local ElementObj = ElementInfo.ElementObj
		local size = ElementObj:getContentSize()
		local ElementWidth, ElementHeight = size.width, size.height
		local DisplayPosX = StartPosX 
		local DisplayPosY = StartPosY - (LineMaxHeight - ElementHeight)

		ElementObj:setPosition(DisplayPosX, DisplayPosY)
		StartPosX = StartPosX + ElementWidth + ElementInterval

		ElementObj:setVisible(true)
	end
end

function clsLightMultiRichEdit:DisplayLine(LineIndex)
	local EleList = self:GetSubElement(self._LineRec[LineIndex].StartIdx, self._LineRec[LineIndex].EndIdx)
	DisplayElementList(self, EleList, LineIndex)
end

function clsLightMultiRichEdit:UnDisplayLine(LineIndex)
	local EleList = self:GetSubElement(self._LineRec[LineIndex].StartIdx, self._LineRec[LineIndex].EndIdx)
	for Index, ElementInfo in pairs_orderly(EleList, KeyCompare) do
		local ElementObj = ElementInfo.ElementObj
		ElementObj:setVisible(false)
		ElementObj:setPosition(0, 0)
	end
end

local function IsNumberChar(Char)
	return string.match(Char,"%d")
end

local function NoMatchSpeProc(EditObj)
	EditObj._ProcRichStatus = NON_STATUS 
end

local function TcpProc(EditObj, Char)
	EditObj._ProcRichStatus = REC_EMOTE_NUM
	EditObj._TcpNumNameStr = EditObj._TcpNumNameStr .. Char
end

local function PoundProc(EditObj)
	DeleteWordForwardLeft(EditObj, 1)

	EditObj._ProcRichStatus = NON_STATUS 
end

local MatchSpeFuncList = {
	["#"] = PoundProc, 
}

local function TryMatchSpeFunc(Char)
	if IsNumberChar(Char) then
		return TcpProc
	else
		return MatchSpeFuncList[Char]
	end
end

local function IsTcpEndChar(Char)
	return Char == " "
end

local function InsertTcpElement(EditObj, TcpNameStr, InsertIndex)
	local FileName = string.format("emote/%d.png", TcpNameStr)
	local EmoteObj = LIGHT_UI.clsSprite:New(EditObj._RootNode, 0, 0, FileName)
	EmoteObj:setVisible(false)
	EmoteObj:setAnchorPoint(0, 1)
	table.insert(EditObj._ElementRec, InsertIndex + 1, { ElementObj = EmoteObj, DescInfo = { EleType = ELETYPE_TCP, Text = string.format("#%s ", TcpNameStr), }, })
	EditObj._InsertIndex = EditObj._InsertIndex + 1
end

function clsLightMultiRichEdit:PreProcInputStr(InputStr)
	local RetStr = string.gsub(InputStr, "#%d+", "%0 ")
	return RetStr
end

local MaxTcpNumCnt = 3

local function IsAmongTcpRange(EditObj, Num)
	for _, RangeInfo in ipairs(EditObj._TcpNumList) do
		if Num >= RangeInfo.Min and Num <= RangeInfo.Max then
			return true
		end
	end

	return false
end

local function CanTcpNumExtend(EditObj, Num)
	local NumStr = tostring(Num)
	local NumCnt = string.len(NumStr)
	if NumCnt >= MaxTcpNumCnt then
		return false
	end

	for _, RangeInfo in ipairs(EditObj._TcpNumList) do
		for TempNum = RangeInfo.Min, RangeInfo.Max do
			local TempStrNum = tostring(TempNum)
			if (string.len(TempStrNum) > NumCnt) and (string.sub(TempStrNum, 1, NumCnt) == NumStr) then
				return true
			end
		end
	end

	return false
end

local function QuitTcpInput(EditObj)
	EditObj._TcpNumNameStr = ""
	EditObj._ProcRichStatus = NON_STATUS
end

local function TryParsSpeText(EditObj, Char, CharSize)
	if (EditObj._ProcRichStatus == NON_STATUS) and Char == SpeBeginChar then
		EditObj._ProcRichStatus = REC_POUND 
		return
	end

	if (EditObj._ProcRichStatus == REC_POUND) then
		local MatchFunc = TryMatchSpeFunc(Char)
		if MatchFunc then
			MatchFunc(EditObj, Char)
		else
			NoMatchSpeProc(EditObj)
		end
		return
	end

	if (EditObj._ProcRichStatus == REC_EMOTE_NUM) then
		if IsNumberChar(Char) then
			EditObj._TcpNumNameStr = EditObj._TcpNumNameStr .. Char

			local TcpNum = tonumber(EditObj._TcpNumNameStr)

			local AmongTag = IsAmongTcpRange(EditObj, TcpNum)
			local ExtendTag = CanTcpNumExtend(EditObj, TcpNum)

			if (not AmongTag) and (not ExtendTag) then
				QuitTcpInput(EditObj)
				return
			end

			if (not ExtendTag) then
				DeleteWordForwardLeft(EditObj, string.len(EditObj._TcpNumNameStr) + 1) -- 加上非匹配符 井号的长度
				InsertTcpElement(EditObj, EditObj._TcpNumNameStr, EditObj._InsertIndex)

				EditObj._TcpNumNameStr = ""
				EditObj._ProcRichStatus = NON_STATUS
				return
			end
		else
			if EditObj._TcpNumNameStr ~= "" then
				local TempChar = Char
				DeleteWordForwardLeft(EditObj, string.len(EditObj._TcpNumNameStr) + 2) -- 加上非匹配符 井号的长度
				InsertTcpElement(EditObj, EditObj._TcpNumNameStr, EditObj._InsertIndex)

				EditObj._TcpNumNameStr = ""
				EditObj._ProcRichStatus = NON_STATUS

				EditObj:InsertChar(TempChar, CharSize)
			else
				QuitTcpInput(EditObj)
			end
		end
		return
	end
end
-- ele rec
-- line rec
-- line height rec
---------------------------------------------------------------------------------------------- ???
function clsLightMultiRichEdit:print_ele()
	for _, ele_info in ipairs(self._ElementRec) do
		print("ele", ele_info.DescInfo.EleType)
	end
end
function clsLightMultiRichEdit:InsertChar(Char, CharSize)
	local InsertIndex = self._InsertIndex
	local TextObj = LIGHT_UI.clsLabel:New(self._RootNode, 0, 0, Char, self._Font, self._FontSize)
	TextObj:setVisible(false)
	TextObj:setAnchorPoint(0, 1)
	table.insert(self._ElementRec, InsertIndex + 1, { ElementObj = TextObj, DescInfo = { EleType = ELETYPE_TEXT, Text = Char, }, })
	self._InsertIndex = self._InsertIndex + 1

	TryParsSpeText(self, Char, CharSize)
end

function clsLightMultiRichEdit:DeleteSubElement(BeginIndex, EndIndex)
	local RemoveCount = EndIndex - BeginIndex + 1
	for i = 1, RemoveCount do
		self._ElementRec[BeginIndex].ElementObj:removeFromParentAndCleanup(true)
		table.remove(self._ElementRec, BeginIndex)
	end
end

function clsLightMultiRichEdit:ConvertElementToText(ElementInfoList)
	local TempStrTbl = {} 
	local LastEffDesc = nil 
	for _, ElementInfo in pairs_orderly(ElementInfoList, KeyCompare) do -- 按从小到大的顺序遍历
		table.insert(TempStrTbl, ElementInfo.DescInfo.Text)
	end

	return table.concat(TempStrTbl)
end

function clsLightMultiRichEdit:getText()
	local EleCnt = #(self._ElementRec)
	if EleCnt <= 0 then
		return ""
	end

	return self:GetTextByRange(1, EleCnt)
end

function clsLightMultiRichEdit:NewLine(LineIndex, StartIdx, EndIdx, Segment)
	table.insert(self._LineRec, LineIndex, {})
	self._LineRec[LineIndex].StartIdx = StartIdx 
	self._LineRec[LineIndex].EndIdx = EndIdx 
	self._LineRec[LineIndex].Segment = Segment

	local ElementInfoList = self:GetSubElement(StartIdx, EndIdx)
	local LineMaxHeight = FindMaxHeight(self, ElementInfoList)
	table.insert(self._LineHeightRec, LineIndex, LineMaxHeight)
end

function clsLightMultiRichEdit:DeleteLine(LineIndex)
	table.remove(self._LineRec, LineIndex)
	table.remove(self._LineHeightRec, LineIndex)
end

function clsLightMultiRichEdit:GetEditHeight()
	local RetHeight = 0
	for LineIndex, LineHeight in ipairs(self._LineHeightRec) do
		RetHeight = RetHeight + LineHeight + self._LineInterval
	end

	return RetHeight
end

function clsLightMultiRichEdit:GetLineHeight(LineIndex)
	return self._LineHeightRec[LineIndex]
end

function clsLightMultiRichEdit:ResetEdit()
	for _, EleInfo in pairs(self._ElementRec) do
		EleInfo.ElementObj:removeFromParentAndCleanup(true)
	end
	self._ElementRec = {}
	self._InsertIndex = 0
	self._TextCursor:setPosition(0, self._MinMsgHeight)
	self._CurLine = 1
	self._LineRec = {}
	self._LineRec[self._CurLine] = {}
	self._LineRec[self._CurLine].StartIdx = 0
	self._LineRec[self._CurLine].EndIdx = 0
	self._SegIndex = 1
	self._LineRec[self._CurLine].Segment = self._SegIndex 

	self._LineHeightRec = {}
	self._LineHeightRec[self._CurLine] = self._DefaultLineHeight
	self._ProcRichStatus = NON_STATUS 
end

local function DoQuitSpeProc(EditObj)
	if EditObj._ProcRichStatus == REC_POUND then
		local DeleteCount = 1
		EditObj:DoDelete(EditObj._InsertIndex - DeleteCount + 1, EditObj._InsertIndex)
	end

	if EditObj._ProcRichStatus == REC_EMOTE_NUM then
		local TempStrLen = string.len(EditObj._TcpNumNameStr) + 1
		EditObj:DoDelete(EditObj._InsertIndex - TempStrLen + 1, EditObj._InsertIndex)
		EditObj._TcpNumNameStr = ""
	end

	EditObj._ProcRichStatus = NON_STATUS
end

function clsLightMultiRichEdit:OnTextLeftDown(MouseX, MouseY)
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
		return
	end

	Super(clsLightMultiRichEdit).OnTextLeftDown(self, MouseX, MouseY)
end

function clsLightMultiRichEdit:DoBackProc()
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
		return
	end

	Super(clsLightMultiRichEdit).DoBackProc(self)
end

function clsLightMultiRichEdit:TryReturnProc()
	if self._ProcRichStatus ~= NON_STATUS then
		DoQuitSpeProc(self)
	end

	Super(clsLightMultiRichEdit).TryReturnProc(self)
end

function clsLightMultiRichEdit:getContentSize()
	return self._MaxTextWidth, self._MinMsgHeight
end

