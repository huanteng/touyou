-- task detail

DETAIL_LAYOUT = Import("layout/task_detail.lua")
local DetailConfig = DETAIL_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
local TASK_PAGE = Import("lua/task.lua")
local detailPage = nil

--当前任务id
local task_id = nil

local rewardState = nil		--true 已领 false 未领
--[[
name
status
text
id
jump
description
jump_data
expires
--]]

local function back_func()
	if PANEL_CONTAINER.closeChildPanel( nil, 7 ) then
		if rewardState == true then
			PANEL_CONTAINER.addChild( TASK_PAGE.showTaskPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0, true) )
		else
			PANEL_CONTAINER.addChild( TASK_PAGE.showTaskPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
		end
	end
end


local function onTaskFinish(data)
	rewardState = true
	detailPage._do_btn:setString( "未完成" )
	
	--closeDetailPagePanel()
	TASK_PAGE.refreshData()
	closeDetailPagePanel()
	local taskList
	if PANEL_CONTAINER.closeChildPanel( nil, 7 ) then
		if rewardState == true then
			taskList = TASK_PAGE.showTaskPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0, true)
			PANEL_CONTAINER.addChild( taskList )
		else
			taskList = TASK_PAGE.showTaskPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0)
			PANEL_CONTAINER.addChild( taskList )
		end
	end	
	showMessage( taskList, data.memo )
end
local function doTaskFinish()
	doCommand( 'task', 'finish', { id = task_id }, onTaskFinish )
end

local function createDetailPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = DetailConfig, nil
	local o = nil

	local function on_back(btn)
		closeDetailPagePanel()
		if PANEL_CONTAINER.closeChildPanel( nil, 7 ) then
			if rewardState == true then
				PANEL_CONTAINER.addChild( TASK_PAGE.showTaskPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0, true) )
			else
				PANEL_CONTAINER.addChild( TASK_PAGE.showTaskPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			end
		end
		--TASK_PAGE.showTaskPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '获得金币' )		
	
	o = createLabel( ret_panel, c.name )
	ret_panel._task_title = o
	
	o = createMultiLabel( ret_panel, c.introduce )
	ret_panel._task_detail = o

	local function on_do_btn(obj, x, y)
		local info = detailPage._task_info
		if tonumber( info.jump ) > 0 then		
			closeDetailPagePanel()			
			jump( tonumber( info.jump ), info.jump_data, back_func )
		end			
	end
	
	ret_panel._do_btn = create9Button( ret_panel, c.btn, on_do_btn )

	return ret_panel
end

local function on_detail(data)
	info = data.data
	if not detailPage then
		return
	end
	if not info then
		return
	end
	detailPage._task_title:setString(info.name)
	
	local description = clearHTML( info.description )
	
	detailPage._task_detail:setString( description )
	detailPage._task_info = info
	local winSize = CCDirector:sharedDirector():getWinSize()
	local size = detailPage._task_detail:getContentSize()
	
	local o = detailPage._do_btn
	o:setPosition(236, winSize.height - size.height - 175)
	o:setString(info.text)
	
	if info.status == '1' then	
		o:setString( '领取' )
		o.onTouchEnd = doTaskFinish
	end
end

function init( data )
	task_id = data.id
end

function showDetailPagePanel(parent, x, y )
	rewardState = false
	if not detailPage then
		detailPage = createDetailPagePanel(parent, x, y)
	else
		detailPage:setVisible(true)
	end

	doCommand( 'task', 'detail', { id = task_id }, on_detail, 5, { loading = 1 } )
end

function closeDetailPagePanel()
	--detailPage:setVisible(false)
	clear({detailPage})
	detailPage = nil
end

