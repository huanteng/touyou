LAYOUT = Import("layout/about_team.lua")
local layoutConfig = LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
local panel = nil

local function createPanel(parent, x, y)
	
	local ret_panel = createBasePanel( parent, x, y  )	
	
	
	local c = layoutConfig.bg_config	
	ret_panel._bg = createMultiLabel( ret_panel, c )
	ret_panel._bg:setAnchorPoint(0, 0)
	
	
	local function on_back()
		closePanel()
		--[[local o = Import("lua/setting.lua")
		o.showPanel(HTTP_CLIENT.getRootNode(), 0, 0)--]]
		if PANEL_CONTAINER.closeChildPanel( nil, 15 ) then
			local SETTING = Import("lua/setting.lua")							
			PANEL_CONTAINER.addChild( SETTING.showPanel(			PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
		end	
	end
	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '联系我们' )
	--[[local c = layoutConfig.head_back
	o = createButton( ret_panel, c )
	o:setAnchorPoint(0, 0)	
	o.onTouchEnd = on_back
	ret_panel._onBack = o
	]]
	return ret_panel
end

function showPanel(parent, x, y)
	dailyCount('SettingeCallUs')--联系我们
	if not panel then
		panel = createPanel(parent, x, y)
	else
		panel:setVisible(true)
	end
end

function closePanel()
	
	panel:setVisible(false)
	clear{panel}
	panel = nil
end