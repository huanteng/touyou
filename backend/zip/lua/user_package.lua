-- game hall panel

local packageInfoPage = nil

PACK_LAYOUT = Import("layout/user_package.lua")
local PackConfig = PACK_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local max_pic = 4

local package_data = nil

--[[
description
price
gift
expires
need_use
logo
status
chinese
discount
is_list
is_hot
quantity
name
id
short_desc
kind
seq
momo
--]]

local backUserFun = nil

local info_desc_info = {
	item_width = 480,
	item_height = 118,   --两线间距
	column_cnt = 1,
	x_space = 0,
	y_space = 0,
}

local function createMoveInfoPage(parent, x, y)
	local config = PackConfig.move_page_config
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_info_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, 737)
	ret_panel._move_info_grp:setVMovable(true)

	local pack_layout = LIGHT_UI.clsRowLayout:New(nil, 0, 0, info_desc_info)
	pack_layout:refresh_view()

	local function get_y_limit(obj)
		return config.inner_y 
	end

	pack_layout:setPosition(0, config.inner_y)
	ret_panel._move_info_grp:appendItem(pack_layout)
	ret_panel._move_info_grp:selectPage(1)

	pack_layout.getYLimit = get_y_limit

	ret_panel._pack_layout = pack_layout 
	
	return ret_panel
end

local USER_INFO_PANEL = Import("lua/user_info.lua")

local function back_func()
	USER_INFO_PANEL.closeUserInfoPagePanel()
	
	showPackageInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

local function createPackageInfoPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = PackConfig, nil
	local o = nil

	local function on_back(btn)
		local MALL_PANEL = Import("lua/mall.lua")
		--USER_INFO_PANEL.showUserInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		--USER_INFO_PANEL.set_back_func(nil)
		--MALL_PANEL.showMallPagePanel(HTTP_CLIENT.getRootNode(),0,0)
		if packageInfoPage.type == 1 then
			if PANEL_CONTAINER.closeChildPanel( nil, 6 ) then
				PANEL_CONTAINER.addChild( MALL_PANEL.showMallPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			end
		elseif packageInfoPage.type == 2 then
			MALL_PANEL.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, packageInfoPage.backFun, packageInfoPage.uid, true )
		elseif packageInfoPage.type == 3 then
			packageInfoPage.backFun()
		elseif packageInfoPage.type == 4 then
			if PANEL_CONTAINER.closeChildPanel( nil, 8 ) then
				PANEL_CONTAINER.addChild( LUCKY_ROT_PANEL.showLuckyPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0)  )
			end
		else
			if PANEL_CONTAINER.closeChildPanel( nil, 6 ) then
				PANEL_CONTAINER.addChild( MALL_PANEL.showMallPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			end
		end
		
		closePackageInfoPagePanel()
	end
	
	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '我的背包' )

	local function onMallClick(btn)
		local function backPackage()
			local MALL_PANEL = Import("lua/mall.lua")
			MALL_PANEL.closeMallPagePanel()
			showPackageInfoPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, backUserFun, nil, 3 )
		end
		local function backPackage2()
			local MALL_PANEL = Import("lua/mall.lua")
			MALL_PANEL.closeMallPagePanel()
			showPackageInfoPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, nil, nil, 4 )
		end
		local MALL_PANEL = Import("lua/mall.lua")
		if packageInfoPage.type == 1 then
			if PANEL_CONTAINER.closeChildPanel( nil, 6 ) then
				PANEL_CONTAINER.addChild( MALL_PANEL.showMallPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			end
		elseif packageInfoPage.type == 2 then
			MALL_PANEL.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, packageInfoPage.backFun, packageInfoPage.uid, true )
		elseif packageInfoPage.type == 3 then
			backUserFun = packageInfoPage.backFun
			MALL_PANEL.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, backPackage, nil, true )			
		elseif packageInfoPage.type == 4 then
			MALL_PANEL.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, backPackage2, nil, true )
		else
			if PANEL_CONTAINER.closeChildPanel( nil, 6 ) then
				PANEL_CONTAINER.addChild( MALL_PANEL.showMallPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			end
		end		
		closePackageInfoPagePanel()	
		--MALL_PANEL.showMallPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
	
	createButton( ret_panel, c.mall, onMallClick )
	
	c2 = c.move_page_config
	ret_panel._move_info_page = createMoveInfoPage(ret_panel, c2.x, c2.y)

	return ret_panel
end

local function createItemByData( info )
	local tr = LIGHT_UI.clsTr:New( PackConfig )
	local nodeObj = tr._RootNode
	
	local function onPropsUses( data )
		showMultiMessage( packageInfoPage, data.memo ,{ms = 3000})
		doCommand( 'props', 'my', {  }, on_get_pack_data )
	end
	
	local function doPropsUses()
		local function onOk()		
			doCommand( 'props', 'uses', { props = info.id, quantity = 1 }, onPropsUses )
		end			
		local config = deepcopy(PackConfig.alert)
		config.content2.text = info.name
		--config.icon.res = info.logo
		createAlert(packageInfoPage,config,onOk )		
	end
	
	tr:setMsgDelegate( packageInfoPage._move_info_page._move_info_grp, doPropsUses )
	
	tr:addSprite( 'logo', info.logo )
	tr:addLabel( 'name', info.name )
	tr:addLabel( 'count', info.quantity )
	--tr:addLabel( 'day', info.day )
	local day = '此日期前使用:'..tostring(info.expires)
	--local day = '剩余时间:'..'30'..'天'
	tr:addLabel( 'day', day )
	tr:addMultiLabel( 'desc', info.short_desc )
		
	tr:setHeight( 'day', 22) --线距离有效期元素的距离
	
	return nodeObj
end

function on_get_pack_data(data)
	package_data = data.data
	if type( package_data ) ~= "table" then
		--d( package_data )
		return
	end
	
	if not packageInfoPage then
		return
	end
	
	local page = packageInfoPage._move_info_page._pack_layout
	page:clearAllItem()
	
	if packageInfoPage._move_info_page.tip then
		clear( {packageInfoPage._move_info_page.tip} )
		packageInfoPage._move_info_page.tip = nil
	end
	
	if #package_data > 0 then
		for _, pack_info in pairs(package_data) do
			local item = createItemByData(pack_info)
			packageInfoPage._move_info_page._pack_layout:append_item(item)
		end
	else
		packageInfoPage._move_info_page.tip = createLabel( packageInfoPage._move_info_page, PackConfig.nothing_tip )
		xCenter( packageInfoPage._move_info_page.tip )
	end

	page:refresh_view()
	setExsitAlert( false )
end

function showPackageInfoPagePanel(parent, x, y, callBack, target_uid2, back_type )
	--if not packageInfoPage then
		packageInfoPage = createPackageInfoPagePanel(parent, x, y)
	--else
		--packageInfoPage:setVisible(true)
	--end
	if callBack then
		packageInfoPage.backFun = callBack
	end
	if target_uid2 then
		packageInfoPage.uid = target_uid2
	end
	if back_type then
		packageInfoPage.type = back_type
	end
	--if not package_data then
		doCommand( 'props', 'my', {  }, on_get_pack_data, 1 )
	--end
end

function closePackageInfoPagePanel()
	--packageInfoPage:setVisible(false)
	clear({packageInfoPage})
	packageInfoPage = nil
end

