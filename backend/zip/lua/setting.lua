-- setting_page
LAYOUT = Import("layout/setting.lua")
local SettingConfig = LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local settingPanel = nil


local function on_btn_1()
	closeSettingPanel()
	local o = Import("lua/system_setting.lua")
	o.showPanel(HTTP_CLIENT.getRootNode(), 0, 0)
end
local function on_btn_2()
	closeSettingPanel()
	local o = Import("lua/safe_setting.lua")
	o.showPanel(HTTP_CLIENT.getRootNode(), 0, 0)
end
local function on_btn_3()
	closeSettingPanel()
	local o = Import("lua/help.lua")
	o.showPanel(HTTP_CLIENT.getRootNode(), 0, 0, 1)
end
local function on_btn_4()
	closeSettingPanel()
	local o = Import("lua/about.lua")
	o.showPanel(HTTP_CLIENT.getRootNode(), 0, 0)
end
local function on_btn_5()
	closeSettingPanel()
	local o = Import("lua/about_team.lua")
	o.showPanel(HTTP_CLIENT.getRootNode(), 0, 0)
end
local function on_btn_6()
	dailyCount('SettinhfFeedback')--意见反馈
	closeSettingPanel()
	PLAYER_INFO_PANEL.showByUserId( 850 )
end
local function on_btn_7()
	dailyCount('SettinhgClear')--清除缓存
	--closeSettingPanel()
	clearCache()
    showMessage( settingPanel, "清理完毕" )
end
local function on_btn_8()
	dailyCount('SettingLogOut')--注销
	local function onUserLogout( data )	
		closeSettingPanel()
		GLOBAL.sid = nil
		setData( 'login' )
		HTTP_CLIENT.resetNode()		
		LOGIN_PANEL.creatLogin( HTTP_CLIENT.getRootNode())
		--showMessage( LOGIN_PANEL.loginPanel, data.memo )		
		--LOGIN_PANEL.loginPanel._username_input:setString( '' )
		--LOGIN_PANEL.loginPanel._password_input:setString( '' )
	end
	CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(TIMER['askDataCB'])
	TIMER['askDataCB'] = nil
	doCommand( 'user', 'logout', {}, onUserLogout )
end

function backToLogin()
	GLOBAL.sid = nil
	setData( 'login' )
	HTTP_CLIENT.resetNode()		

	LOGIN_PANEL.showLoginPanel( HTTP_CLIENT.getRootNode(), 0, 0)
	CCMessageBox( "很抱歉，程序异常，将自动导航回登录界面", '' )	
end

local function createSettingPanel(parent, x, y)
	local ret_panel = createContainerChildPanel( parent, x, y )

	local c = SettingConfig
	
	local function on_back(btn)
		closeSettingPanel()
		MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
	
	for i=1,3 do
		local bg = replaceTable( c.bg, c[ 'bg_'..i] )
		createGrid( ret_panel, bg )
	end
	
	local _btn = {}
	for i=1, 14 do
		local label = replaceTable( c.label, c[ 'label_'..i] )
		_btn[i] = createButton( ret_panel, label )
	end

	_btn[1].onTouchEnd = on_btn_1
	_btn[2].onTouchEnd = on_btn_2
	_btn[3].onTouchEnd = on_btn_3
	_btn[4].onTouchEnd = on_btn_4
	_btn[5].onTouchEnd = on_btn_5
	_btn[6].onTouchEnd = on_btn_6
	_btn[7].onTouchEnd = on_btn_7
	
	_btn[8].onTouchEnd = on_btn_1
	_btn[9].onTouchEnd = on_btn_2
	_btn[10].onTouchEnd = on_btn_3
	_btn[11].onTouchEnd = on_btn_4
	_btn[12].onTouchEnd = on_btn_5
	_btn[13].onTouchEnd = on_btn_6
	_btn[14].onTouchEnd = on_btn_7
	
	create9Button( ret_panel, c.logout_btn, on_btn_8 )
	
	ret_panel.closePanel = closeSettingPanel
	
	return ret_panel
end

function showPanel(parent, x, y)
	if not settingPanel then
		settingPanel = createSettingPanel(parent, x, y)
	else
		settingPanel:setVisible(true)
	end
	return settingPanel
end

function closeSettingPanel()
	--settingPanel:setVisible(false)
	clear({settingPanel})
	settingPanel = nil
end