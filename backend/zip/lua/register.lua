-- register

REG_LAYOUT = Import("layout/register.lua")
LOGIN_PANEL = Import("lua/login.lua")
local RegConfig = REG_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local registerPanel = nil
local regisiterComplete = true

local function onUserRegister( data )
	regisiterComplete = true
	local function doAutoLogin()
		local username = registerPanel._username_input:getText()
		local password = registerPanel._password_input:getText()		
		
		
		--LOGIN_PANEL.doLogin(username, password)
		local path = WRITE_PATH..'cache/'..tostring(GLOBAL.uid)
		xymodule.create_dir(path)
		setData( 'login', 'username', username )			
		setData( 'login', 'password', registerPanel._password_input:getText() )
		LOGIN_PANEL = Import("lua/login.lua")
		LOGIN_PANEL.creatLogin(HTTP_CLIENT.getRootNode())
		closeRegisterPanel()
		
	end
	
	if data.code <= 0 then
		showMessage( registerPanel, data.memo, {y=600} )
	else
		dailyCount('Register')--注册成功
		showMessage( registerPanel, '注册成功,5秒后自动登录', {y=600} )
			
		local action = Import("lua/action.lua")
		f_delay_do( registerPanel,doAutoLogin,{}, 3 )
	end
end

local selectBtn = nil

local function singleChooseHandler( clickBtn )
	if selectBtn then
		selectBtn:setNormalStatus()
	end
	clickBtn:setDownStatus()
	selectBtn = clickBtn	
end

local function createItem( parent, x, y, resState, onState, sexNum, selected )
	local config = {                     --选项宽高调整
		--sx = 120,
		--sy = 50,
		--css = 'checkbox',
		text = '',
		--text_css = 'b2',
		res = 'addfocus_bt_selected.png',
		on = 'addfocus_bt_default.png'
	}
	config.x = x
	config.y = y
	config.res = resState
	config.on = onState
	local btn
	
	btn = createCustomStatusButton( parent, config, singleChooseHandler )	
	btn.sex = sexNum
	--btn:setString( text )
	--btn.text = text	
	
	if selected then
		selectBtn = btn
		singleChooseHandler( btn )
	end		
	
	return btn
end

local function createRegisterPanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = RegConfig, nil
	local o = nil

	local function on_back(btn)
		if regisiterComplete == true then
			closeRegisterPanel()
			local LOGIN_PANEL = Import("lua/login.lua")
			LOGIN_PANEL.creatLogin(HTTP_CLIENT.getRootNode())
		end
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '注册' )
	
	createGrid( ret_panel, c.rect )
		
	o = createEdit2( ret_panel, c.username, 1 )		
	o:setMaxInputChar(10)
	ret_panel._username_input = o
	--o:setString("hello world")
	
	o = createEdit2( ret_panel, c.password, 2 )	
	--o:setString("hello world")
	o:setPasswordChar("*")
	o:setMaxInputChar(9)
	ret_panel._password_input = o
	
	local view = nil
	local function viewPassword()
		if view then
			view = nil
		else
			view = ''
		end
		
		local o = password_input
		if view then
			o:setPasswordChar(nil)
		else
			o:setPasswordChar('*')
		end
		o:setString( o:getText() )
	end
	
	createCheckButton( ret_panel, c.view, viewPassword )
	
	local function on_finish(btn)	
		local username = ret_panel._username_input:getText()
		local password = ret_panel._password_input:getText()
		
		if username == '' then
			showMessage( ret_panel, '请输入用户名', {y=600} )
			return
		end
		
		if password == '' then
			showMessage( ret_panel, '请输入密码', {y=600} )
			return
		end
		
		--print( string.len( username ) )
		if string.len( username ) > 15 then
			showMessage( ret_panel, '用户名长度不能超过10位', {y=600} )
			return
		end	
		
		if string.len( password ) < 4 then
			showMessage( ret_panel, '密码长度需要超过3位', {y=600} )
			return
		end	
		
		if string.len( password ) > 9 then
			showMessage( ret_panel, '密码长度不能超过9位', {y=600} )
			return
		end	
		
		local sexStr
		if selectBtn.sex == 0 then
			sexStr = "0"
		else
			sexStr = "1"
		end
		--sex = '0' 是男，sex = '1' 是女。
		local imei = xymodule.get_imei()	
		local channel = nil
		if xymodule.getchannel == nil then
			channel = "touyou"
		else
			channel = xymodule.getchannel()
		end
		if regisiterComplete == true then
			regisiterComplete = false
			doCommand( 'user', 'register', { name = username, pass = password, version = 102,
			imei = imei, channel = channel,sex = sexStr }, onUserRegister )
		end
				
	end		
	
	create9Button( ret_panel, c.btn, on_finish )
	
	ret_panel.singleBtn = createItem( 	ret_panel, 135, 525, "nan-off.png", "nan-on.png", 0  )
	ret_panel.singleBtn2 = createItem( ret_panel, 335, 525, "nv-off.png", "nv-on.png", 1, true )
	
	ret_panel.tip_txt = createLabel( ret_panel, RegConfig.tip_txt )
	xCenter( ret_panel.tip_txt )
	
	return ret_panel
end

function showRegisterPanel(parent, x, y)
	--if not registerPanel then
		registerPanel = createRegisterPanel(parent, x, y)
	--else
		--registerPanel:setVisible(true)
	--end
end

function closeRegisterPanel()
	--registerPanel:setVisible(false)
	registerPanel._username_input:active(false)
	registerPanel._password_input:active(false)
	registerPanel._username_input:clearCursor()
	registerPanel._password_input:clearCursor()
	LIGHT_UI.clearActEdit()
	registerPanel:removeFromParentAndCleanup(true)
	registerPanel = nil
	regisiterComplete = true
end

