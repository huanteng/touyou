-- rank

local rankPage = nil

RANK_LAYOUT = Import("layout/rank.lua")
local RankConfig = RANK_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local girl_list = {}
--[[
win
id
profit
online
sum
logo
vip_id
sex
name
--]]
local cur_girl_page = 0
local max_girl_page = 5
local boy_list = {}
local cur_boy_page = 0
local max_boy_page = 5
local friend_list = {}
local cur_friend_page = 0
local max_friend_page = 0

local small_desc_info = {
	item_width = 480,
	item_height = 120,
	column_cnt = 1,
	x_space = 0,
	y_space = 0,
}

--local CHALLENGE_PAGE = Import("lua/challenge.lua")
local PLAYER_PAGE = Import("lua/player_info.lua")

local function back_func()
	PLAYER_PAGE.closePlayerInfoPagePanel()
	if PANEL_CONTAINER.closeChildPanel( nil, 11 ) then							
		PANEL_CONTAINER.addChild( showRankPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
	end
	--showRankPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
end

local function createSmallItemByRankInfo(info, rank)
	local tr = LIGHT_UI.clsTr:New( RankConfig )
	local nodeObj = tr._RootNode

	local function on_small_click(obj, x, y)
		closeRankPagePanel()
		PLAYER_PAGE.showByUserId(info.uid)
		PLAYER_PAGE.init( { back_func = back_func })
	end

	tr:setMsgDelegate( rankPage._move_small_page._move_small_grp, on_small_click )
	
	tr:addSprite( 'logo', info.logo )
	
	if info.online == 1 then
		tr:addSprite( 'online' )
	end

	local rank_pic_res = "rank_first_bg.png"
	if rank > 3 then
		rank_pic_res = "rank_four_bg.png"
	end
	tr:addSprite( 'rank_bg', rank_pic_res )

	tr:addLabel( 'rank', rank )
	
	tr:addLabel( 'name', info.name )
	tr:addLabel( 'win_hint', "总:" .. info["total"] .. "  " .. "胜:" .. info["win"] )
	
	tr:addSprite( 'gold_pic' )
	tr:addLabel( 'win_gold', '+' .. info.profit )

	tr:setHeight( 'logo', 22 )
	
	return nodeObj
end

local function doCreateSmallBtnList(row_layout, btn_list_info, move_grp)
	local function onTouchBegan(btn, x, y)
		move_grp:onTouchBegan(btn:convertToWorldSpace(x, y))
	end
	local function onTouchMove(btn, x, y)
		move_grp:onTouchMove(btn:convertToWorldSpace(x, y))
	end

	for rank, btn_info in ipairs(btn_list_info) do
		local function onTouchEnd(btn, x, y)
			move_grp:onTouchEnd(btn:convertToWorldSpace(x, y))
			if not move_grp:isMoving() then
				btn_info["on_click_func"](btn, x, y)
			end
		end

		local nodeObj = createSmallItem(btn_info, rank)

		row_layout:append_item(nodeObj)
	end
end

local row_layout_y = 380 
local function createMoveSmallPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = RankConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_small_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, 700)
	ret_panel._move_small_grp:setVMovable(true)

	local boy_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	boy_page:refresh_view()

	local girl_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	girl_page:refresh_view()

	local friend_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	friend_page:refresh_view()

	girl_page:setPosition(0, config.inner_y)
	ret_panel._move_small_grp:appendItem(girl_page)
	boy_page:setPosition(0, config.inner_y)
	ret_panel._move_small_grp:appendItem(boy_page)
	friend_page:setPosition(0, config.inner_y)
	ret_panel._move_small_grp:appendItem(friend_page)
	ret_panel._move_small_grp:selectPage(1)

	ret_panel._girl_page = girl_page
	ret_panel._boy_page = boy_page 
	ret_panel._friend_page = friend_page 

	local function get_y_limit(obj)
		return config["inner_y"] 
	end

	girl_page.getYLimit = get_y_limit
	boy_page.getYLimit = get_y_limit
	friend_page.getYLimit = get_y_limit
	
	return ret_panel
end

local function on_get_girl_list(data)
	if not rankPage then
		return
	end
	rankPage._move_small_page._girl_page:clearAllItem()
	--[[local real_data = data.data
	if type( real_data ) ~= "table" then
		return
	end--]]
	for rank, rank_info in ipairs(data) do
		table.insert(girl_list, rank_info)
		rankPage._move_small_page._girl_page:append_item(createSmallItemByRankInfo(rank_info, rank))
	end

	rankPage._move_small_page._girl_page:refresh_view()
end

local function on_get_boy_list(data)
	rankPage._move_small_page._boy_page:clearAllItem()
	--local real_data = data.data
	for rank, rank_info in ipairs(data) do
		table.insert(boy_list, rank_info)
		rankPage._move_small_page._boy_page:append_item(createSmallItemByRankInfo(rank_info, rank))
	end

	rankPage._move_small_page._boy_page:refresh_view()
end

local function on_get_friend_list(data)
	rankPage._move_small_page._friend_page:clearAllItem()
	local real_data = data.data
	for rank, rank_info in ipairs(real_data) do
		table.insert(friend_list, rank_info)
		rankPage._move_small_page._friend_page:append_item(createSmallItemByRankInfo(rank_info, rank))
	end

	rankPage._move_small_page._friend_page:refresh_view()
end

local function request_girl_list(page)
	doCommand( 'room', 'rank', { page = page, sex = 1 }, on_get_girl_list,1,{['loading'] = 0} )
end

local function request_boy_list(page)
	doCommand( 'room', 'rank', { page = page, sex = 0 }, on_get_boy_list,1,{['loading'] = 0} )
end

local function request_friend_list(page)
	doCommand( 'challenge', 'friend_rank', { page = page }, on_get_friend_list,1,{['loading'] = 0} )
end

local function createRankPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createContainerChildPanel( parent, x, y )

	local c, c2 = RankConfig, nil
	local o = nil

	local function on_back(btn)
		closeRankPagePanel()
		MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	--ret_panel._onBack = on_back
	--ret_panel._head_text:setString( '每日排行榜' )
		
	config = RankConfig["move_page_config"]
	ret_panel._move_small_page = createMoveSmallPage(ret_panel, config.x, config.y)

	local function on_sel_page(move_obj)
		ret_panel._move_hint:selItem(move_obj:getCurPage())
		--if move_obj:getCurPage() == 1 and table.maxn(girl_list) <= 0 then
		if move_obj:getCurPage() == 1 then
			cur_girl_page = 1
			request_girl_list(cur_girl_page)
		end
		if move_obj:getCurPage() == 2 then
		--if move_obj:getCurPage() == 2 and table.maxn(boy_list) <= 0 then
			cur_boy_page = 1
			request_boy_list(cur_boy_page)
		end
		--if move_obj:getCurPage() == 3 and table.maxn(friend_list) <= 0 then
		if move_obj:getCurPage() == 3 then
			cur_friend_page = 1
			request_friend_list(cur_friend_page)
		end
	end

	ret_panel._move_small_page._move_small_grp.onSelPage = on_sel_page

	local item_list = {
		[1] = {
			["txt"] = "美女",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = RankConfig["move_hint_config"]["text_size"],
		},
		[2] = {
			["txt"] = "帅哥",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = RankConfig["move_hint_config"]["text_size"],
		},
		--[[[3] = {
			["txt"] = "好友",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = RankConfig["move_hint_config"]["text_size"],
		},--]]
	}

	config = RankConfig["move_hint_config"]
	local total_width = 480
	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, config.x, config.y, "tab_bg_image.png", item_list, total_width, config.res, "tiny_black_pixel.png")
	local bg = ret_panel._move_hint:getBGObj()
	bg:setScaleX(total_width)
	--bg:setScaleY(40)
	ret_panel._move_hint:selItem(1)

	cur_girl_page = 1
	-- 当存在缓存时，此处请求会过早而报错
--	request_girl_list(cur_girl_page)

	local function on_click( obj, idx )
		rankPage._move_hint:selItem( idx )		
		rankPage._move_small_page._move_small_grp:selectPage( idx )
	end

	ret_panel._move_hint.onClick = on_click
	
	ret_panel.closePanel = closeRankPagePanel

	return ret_panel
end

function showRankPagePanel(parent, x, y)
	dailyCount('RankingLists')
	if not rankPage then
		rankPage = createRankPagePanel(parent, x, y)
		-- by dda, 20130723，迁移到此
		request_girl_list(cur_girl_page)
	else
		rankPage:setVisible(true)
	end
	return rankPage
end

function closeRankPagePanel()
	--rankPage:setVisible(false)
	clear({rankPage})
	rankPage = nil
end

