local mallPage = nil
MALL_LAYOUT = Import("layout/mall.lua")
local MallConfig = MALL_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
local target_uid = 0
local CHALLENGE_PAGE = Import("lua/challenge.lua")
local ITEM_DETAIL = Import("lua/item_detail.lua")

local function createGridByItemInfo(info, money_tag)
	local tr = LIGHT_UI.clsTr:New( MallConfig )
	local nodeObj = tr._RootNode
	local function on_item_click(msg, x, y)
		
		showLoading()
		--物品id
		local Pid = tonumber(info.id)
		--友盟具体购物点击事件ID
		if Pid == 45 then
			dailyCount('PropStoreaSmallBag')--小袋元宝	
		elseif Pid == 47 then
			dailyCount('PropStorebBigBag')--大袋元宝
		elseif Pid == 48 then
			dailyCount('PropStorecSmallBox')--小箱元宝
		elseif Pid == 49 then
			dailyCount('PropStoredBigBox')--大箱元宝
		elseif Pid == 44 then
			dailyCount('PropStoreeLuckyCard')--幸运卡
		elseif Pid == 46 then
			dailyCount('PropStorefSmallBagGold')--小袋金币
		elseif Pid == 41 then
			dailyCount('PropStoregVipA')--VIP初级
		elseif Pid == 42 then
			dailyCount('PropStoregVipB')--VIP中级
		elseif Pid == 43 then
			dailyCount('PropStoregVipC')--VIP高级是
		end
		
		local function onBack( backFun, uid )
			showMallPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, backFun, uid, true )
		end	
		
		if mallPage.basePanel then
			ITEM_DETAIL.showItemPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, info["id"],target_uid, onBack)
		else
			ITEM_DETAIL.showItemPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, info["id"],target_uid)
		end
		closeMallPagePanel()
	end

	tr:setMsgDelegate( mallPage._move_page._move_grp, on_item_click)

	tr:addSprite( 'logo', info.logo )
	tr:addLabel( 'name', info.name )

	if info.is_hot == 1 then
		tr:addSprite( 'hot' )
	end
	
	if money_tag == 'rmb' then
		tr:addSprite( 'money_type', 'rmb.png' )				
	elseif money_tag == 'yb' then
		--tr:addSprite( 'money_type', 'gold.png' )
		--假如是页面二（元宝道具这个页面的话就就让价格乘以100,就改了之前的BUG
		info.price = (info.price)
		tr:addSprite( 'money_type', 'ingot_pic.png' )
	elseif money_tag == 'gold' then
		tr:addSprite( 'money_type', 'ingot_pic.png' )
	elseif money_tag == 'exchange' then
		tr:addSprite('money_type','bs.png')
	end		
		-- 否则的话就还是还结果
	tr:addLabel( 'price', info.price )
    
    
	tr:addMultiLabel( 'desc', info.short_desc )
	
	tr:setHeight( 'logo', 27 )    --分隔线到logo距离

	return nodeObj
end

local function on_get_yb_money_data(data)
	local real_data = data.data
	if not mallPage then
		return
	end
	mallPage._move_page._yb_page:clearAllItem()
	for _, item_info in ipairs(real_data) do
		mallPage._move_page._yb_page:append_item(createGridByItemInfo(item_info, "rmb"))
	end

	mallPage._move_page._yb_page:refresh_view()
end

local function on_get_yb_item(data)
	local real_data = data.data
	if not mallPage then
		return
	end
	mallPage._move_page._yb_item_page:clearAllItem()
	for _, item_info in ipairs(real_data) do
		mallPage._move_page._yb_item_page:append_item(createGridByItemInfo(item_info, "yb"))
	end

	mallPage._move_page._yb_item_page:refresh_view()
end

local function on_get_gold_item(data)
	local real_data = data.data
	if not mallPage then
		return
	end
	mallPage._move_page._gold_item_page:clearAllItem()
	for _, item_info in ipairs(real_data) do
		mallPage._move_page._gold_item_page:append_item(createGridByItemInfo(item_info, "gold"))
	end

	mallPage._move_page._gold_item_page:refresh_view()
end

local function on_get_exchange_item(data)
	local real_data = data.data
	if not mallPage then
		return
	end
	mallPage._move_page._exchange_item_page:clearAllItem()
	nu = #real_data
	for _, item_info in ipairs(real_data) do	
		mallPage._move_page._exchange_item_page:append_item(createGridByItemInfo(item_info, "exchange"))
	end

	mallPage._move_page._exchange_item_page:refresh_view()
end



local function request_yb_money_data(page)
	doCommand( 'props', 'lists', { page = page, kind = 3 }, on_get_yb_money_data,24*60*60 )
end

local function request_yb_item_data(page)
	doCommand( 'props', 'lists', { page = page, kind = 2 }, on_get_yb_item,24*60*60 )
end

local function request_gold_item_data(page)
	doCommand( 'props', 'lists', { page = page, kind = 4 }, on_get_gold_item,24*60*60 )
end

local function request_exchange_item_data(page)
	doCommand( 'props', 'lists', { page = page, kind = 5 }, on_get_exchange_item,24*60*60 )
end

local mall_desc_info = {  --单元格调整
	item_width = 480,
	item_height = 121,
	column_cnt = 1,
	x_space = 0,
	y_space = 0,
}

local function createMovePage(parent, x, y)
	local config = MallConfig.move_page_config
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config.view_height)
	ret_panel._move_grp:setVMovable(true)

	local yb_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, mall_desc_info)
	yb_page:refresh_view()

	local yb_item_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, mall_desc_info)
	yb_item_page:refresh_view()

	local gold_item_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, mall_desc_info)
	gold_item_page:refresh_view()
	
	local exchange_item_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, mall_desc_info)
	exchange_item_page:refresh_view()

	yb_page:setPosition(0, config.inner_y)
	ret_panel._move_grp:appendItem(yb_page)
	yb_item_page:setPosition(0, config.inner_y)
	ret_panel._move_grp:appendItem(yb_item_page)
	gold_item_page:setPosition(0, config.inner_y)
	ret_panel._move_grp:appendItem(gold_item_page)
	exchange_item_page:setPosition(0, config.inner_y)
	ret_panel._move_grp:appendItem(exchange_item_page)
	ret_panel._move_grp:selectPage(1)

	ret_panel._yb_page = yb_page 
	ret_panel._yb_item_page = yb_item_page 
	ret_panel._gold_item_page = gold_item_page 
	ret_panel._exchange_item_page = exchange_item_page 

	local function get_y_limit(obj)
		return config.inner_y 
	end

	yb_page.getYLimit = get_y_limit
	yb_item_page.getYLimit = get_y_limit
	gold_item_page.getYLimit = get_y_limit
	exchange_item_page.getYLimit = get_y_limit
	
	return ret_panel
end

local function createTopScrollPanel(parent, x, y)
	local ret_panel = createNode( parent, point( x, y ) )

	local o = createSprite( ret_panel, res( 0, 0, MallConfig.head_scroll.res ) )
	o:setAnchorPoint(0, 0)
	o:setPosition(0, -23)

	local _scroll_text = LIGHT_UI.clsRecycleScroll:New(ret_panel, 10, -15, 460, 50)
	
	local txt = {
		[1] = '骰友比赛，娱乐无穷！',
		[2] = '惊险刺激，回味无穷。',
		[3] = '',
	}
	
	local obj = createLabel( nil, text( 0, 0, txt[i], "c3" ) )
	_scroll_text:addInnerObj( obj )
	
	local function setString( content )
		for i = 3, 2, -1 do
			txt[i] = txt[i-1]
		end
		txt[1] = content
		
		obj:setString( string.format( '              %s             %s             %s', txt[1], txt[2], txt[3] ) )
		_scroll_text:tryStopScroll()
		_scroll_text:startScroll()
	end
	
	ret_panel.setString = setString
	ret_panel.scroll_text = _scroll_text
	
	return ret_panel
end

local function createMallPagePanel(parent, x, y, isBasePanel) -- 杞崲鎴愪笘鐣屽潗鏍囦竴瀹氳缁撶偣鐨勭埗缁撶偣璋冪敤
	local ret_panel
	if isBasePanel then
		if isBasePanel == true then
			ret_panel = createBasePanel( parent, x, y )
		end
	else
		ret_panel = createContainerChildPanel( parent, x, y )
	end

	local c, c2 = MallConfig, nil
	local o = nil

	local function on_back(btn)
		closeMallPagePanel()
		--MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)		
	end
	
	if isBasePanel then
		if isBasePanel == true then
			ret_panel._onBack = on_back
			ret_panel._head_text:setString( '道具商城' )
		end
	end
	--ret_panel._onBack = on_back	
	
	
	local function onClickPack(obj, x, y)
		
		if mallPage._onBack then
			USER_PACKAGE_PANEL.showPackageInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, mallPage._onBack, target_uid, 2 )
		else
			USER_PACKAGE_PANEL.showPackageInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, nil, nil, 1 )
		end
		closeMallPagePanel()
	end

	config = c.pack_btn_config
	ret_panel._package_btn = LIGHT_UI.clsButton:New(ret_panel, config.x, config.y, config.res)
	ret_panel._package_btn.onTouchEnd = onClickPack
	
	c2 = c.head_scroll
	ret_panel._top_scroll_panel = createTopScrollPanel(ret_panel, c2.x, c2.y)
	ret_panel.setScroll = ret_panel._top_scroll_panel.setString
	ret_panel.setScroll( '商城大热卖，欢迎选购' )

	ret_panel._move_page = createMovePage(ret_panel, 0, 0)

	local function on_sel_big_page(move_obj)
		if move_obj:getCurPage() == 1 then
			request_yb_money_data(1)
		end
		if move_obj:getCurPage() == 2 then
			request_yb_item_data(1)
		end
		if move_obj:getCurPage() == 3 then
			request_gold_item_data(1)
		end
		if move_obj:getCurPage() == 4 then		
			request_exchange_item_data(1)
		end
		ret_panel._move_hint:selItem(move_obj:getCurPage())
	end

	ret_panel._move_page._move_grp.onSelPage = on_sel_big_page

	local item_list = {
		[1] = {
			["txt"] = "元宝",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 20,
		},
		[2] = {
			["txt"] = "元宝道具",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 20,
		},
		[3] = {
			["txt"] = "VIP专柜",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 20,
		},
		[4] = {
			["txt"] = "奖励兑换",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 20,
		},
	}

	config = MallConfig.move_hint_config
	local total_width = 480
	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, config.x, config.y, "tab_bg_image.png", item_list, total_width, config.res, "tiny_black_pixel.png")
	local bg = ret_panel._move_hint:getBGObj()
	bg:setScaleX(total_width)
	--ret_panel._move_hint:selItem(1)
	ret_panel._move_hint:selItem( 2 )		
	ret_panel._move_page._move_grp:selectPage( 2 )

	local function on_click( obj, idx )
		mallPage._move_hint:selItem( idx )		
		mallPage._move_page._move_grp:selectPage( idx )
	end

	ret_panel._move_hint.onClick = on_click
	
	ret_panel.closePanel = closeMallPagePanel

	return ret_panel
end

function switchTabBar( index )
	if mallPage then
		mallPage._move_hint:selItem( index )		
		mallPage._move_page._move_grp:selectPage( index )
	end
end

function showMallPagePanel( parent, x, y, back_func, target_uid2, isBasePanel )
	if not mallPage then
		mallPage = createMallPagePanel(parent, x, y, isBasePanel)
	else
		mallPage:setVisible(true)
	end
	
	if isBasePanel then
		mallPage.basePanel = isBasePanel
	end
	
	request_yb_money_data(1)
	
	if back_func then mallPage._onBack = back_func end
	
	if target_uid2 then 
		target_uid = target_uid2
	else
		target_uid = GLOBAL.uid
	end
	return mallPage
end

function closeMallPagePanel()
	--mallPage:setVisible(false)
	if mallPage then
		mallPage._top_scroll_panel.scroll_text:Destroy()
	end
	clear({mallPage})
	mallPage = nil
end