LAYOUT = Import("layout/about.lua")
local layoutConfig = LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local panel = nil

local function createPanel(parent, x, y)
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = RegConfig, nil
	local o = nil

	local function on_back(btn)
		closePanel()
		--[[local o = Import("lua/setting.lua")
		o.showPanel(HTTP_CLIENT.getRootNode(), 0, 0)--]]
		if PANEL_CONTAINER.closeChildPanel( nil, 15 ) then
			local SETTING = Import("lua/setting.lua")							
			PANEL_CONTAINER.addChild( SETTING.showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
		end	
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '关于软件' )
	
	local remark_txt = LIGHT_UI.clsMultiLabel:New(ret_panel, layoutConfig["text_config"]["x"], layoutConfig["text_config"]["y"], layoutConfig["text_config"]["text"], GLOBAL_FONT, 18, 450, 1)
	remark_txt:setTextColor(102, 102, 102)

	return ret_panel
end

function showPanel(parent, x, y)
	dailyCount('SettingdAbout')--关于软件
	if not panel then
		panel = createPanel(parent, x, y)
	else
		panel:setVisible(true)
	end
end

function closePanel()
	--panel:setVisible(false)
	clear({panel})
	panel = nil
end