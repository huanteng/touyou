
ROOM_FACE_LAYOUT = Import("layout/room_face.lua")
local RoomFaceConfig = ROOM_FACE_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local FaceZOrder = 110

local function sendFaceMsg(face_id)
	tcp_message(1,face_id)
	--doCommand( 'room', 'emotion', { room_id = id,id = face_id }, onSend)
end

function showFaceBtn(_root)
	if _root then
		local function onClick()
			local c = RoomMatchConfig			
			if _root:IsWatch() == false then
				showRoomFace(_root)
			else
				showMessage( _root, "旁观状态下不允许操作", {ms = 3000} )
			end
		end				
		local o = createButton(_root,RoomFaceConfig.face_btn,onClick)
	end
end

function hideRoomFace(_root)
	assert(_root)
	if _root.RoomFace then
		clear({_root.RoomFace})
		_root.RoomFace = nil
	end
end

function showRoomFace(_root)
	if _root.RoomFace then	
		clear({_root.RoomFace})		
		_root.RoomFace = nil				
	end
	local function onClose()
		hideRoomFace(_root)
	end
	local c = { x = 0,y = 0}
	local c2 = RoomFaceConfig
	local order = _root:getCOObj():getChildrenCount() + FaceZOrder
	_root.RoomFace = createNode(_root,c,nil,order)	
	--o = createSprite(_root.RoomFace,c2.room_face_bg)
	local function onClick()
	end
	o = createButton(_root.RoomFace,c2.room_face_bg,onClick)		
	for i = 1,tonumber(c2.face_info.count) do	
		local c3 = c2['face_sel_'..i]			
		local function OnClick()
			sendFaceMsg(i)
			hideRoomFace(_root)
		end
		local btn = createButton( _root.RoomFace, c3, OnClick )		
		local a = createSprite(_root.RoomFace,c2["face_sel_"..i])
		a_play(a:getSprite(),c2["action_face_"..i],true)	
	end	
	
	local function onClose()
		hideRoomFace(_root)
	end	
	local close =createButton(_root.RoomFace,c2.close_btn,onClose)						
end

function showFace(_root,desk_id,face_id)
	if not desk_id then
		return 
	end
	local c = { x = 0,y = 0}
	if not _root.FaceInfo then		
		local order = _root:getCOObj():getChildrenCount() + FaceZOrder
		_root.FaceInfo = createNode(_root,c,nil,order)	
	end
		
	if _root.FaceInfo.desk_id then		
		clear({_root.FaceInfo.desk_id})
		_root.FaceInfo.desk_id = nil
		
	end		
	_root.FaceInfo.desk_id = createNode(_root.FaceInfo,c)
	c = RoomFaceConfig	
	local function onHide()
		clear({_root.FaceInfo.desk_id})
		_root.FaceInfo.desk_id = nil
	end
	--���鶯��
	local a = createSprite(_root.FaceInfo.desk_id,c["face_show_"..desk_id])
	a_play(a:getSprite(),c["action_face_"..face_id],true)		
	f_delay_do(_root.FaceInfo.desk_id,onHide,nil,c.face_info.stay_time)	
	
end