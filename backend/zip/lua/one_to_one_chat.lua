-- one_to_one_chat page panel

local chat_page = nil
local chat_list = {}

local cur_chatlist_page = 1
local max_chatlist_page = 5

local uid2 = 0
local name2 = '对方'


local max_fsay_page = 50

--发送聊天内容
local send_content = ""

-----------------------------------------------------------------
--函数声明
local request_chat_list
-----------------------------------------------------------------


ONETOONE_CHAT_LAYOUT = Import("layout/one_to_one_chat.lua")
local OneToOneChatConfig = ONETOONE_CHAT_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local chat_desc_info  = OneToOneChatConfig.chat_desc_info

local total_width = chat_desc_info.item_width
local total_height= chat_desc_info.item_height

local chat_send_height = chat_desc_info.chat_send_height

--3秒与取一次消息
local function startTimer()
    local function onTimer()
    request_chat_list(cur_chatlist_page)
    end
	TIMER[ "timer" ] = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( onTimer, 3, false)
end
	
------------------------------------------------------------------------------------------
--发送聊天信息回调
local function on_get_send_cb(data)
	local real_data = data
	request_chat_list(cur_chatlist_page)
end
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--发送聊天信息
local function sned_chat_detail(msg)
	local function filterChar( content )
		local i = 1
		local str = " "
		while(i <= string.len(content)) do
			local b = string.sub(content,1,i)		
			if b ~= str then
				return true
			end
			str = str .. " "
			i = i + 1
		end
		return false
	end	

	if filterChar( msg ) == true then
		doCommand( 'message', 'send', { user = uid2,content = msg}, on_get_send_cb )
	else			
		showMultiMessage( chat_page, "输入不能为空...", {ms = '3000'})
	end
	
end
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--创建输入框
local function createMessagePage(parent, x,y )
	local ret_panel = createNode( parent, point( x, y ) )
	local c, c2 = OneToOneChatConfig, nil
	local o = nil
	
	createGrid( ret_panel, c.input_bg )
	local function fot_show_bug( data )
		--send_content = tostring(data)
		--AdjustString(ret_panel._message_input,data,c.input_label.sx)	
		sned_chat_detail(tostring(data))	
	end

	local function after_submit_input( data )
		local dataCount =string.len(data)
		if dataCount <= 0 then
			return
		end
		
		f_delay_do( ret_panel, fot_show_bug, data, 0.1 )
	end
	
	local function show_multi_text_input()	
		xymodule.mulitiInput( "", after_submit_input )
		--after_submit_input( " " )
	end
	createSprite(ret_panel,c.bg)
	--create9Button( ret_panel, c.message, show_multi_text_input )
	--[[o = createEdit( ret_panel, c.message )
	o:active(true)
	o:setMaxInputChar(12)--设置输入字数限制--]]	
	--ret_panel._message_input = createLabel( ret_panel, c.input_label )
	--AdjustString(ret_panel._message_input,c.input_label.text,c.input_label.sx)	
	--ret_panel._message_input:setString('')
	
		
	local function get_size()
		--return _input_bg:getContentSize()
		return { ["width"] = total_width, ["height"] =  chat_send_height, }
	end
	--[[	
	local function on_send_click(btn_obj)
		--local msg = ret_panel._message_input:getString()		
		if string.len(send_content) > 0 then
			sned_chat_detail(send_content)
			send_content = ""
			ret_panel._message_input:setString("")
		end
		--chat_page._msg_chat_page._message_input:clean()		
	end]]--
	local function test()
		local str = ("测试一下很长很长的聊天内容，看看能否发送出去，并且能正确显示出来")
		f_delay_do( ret_panel, sned_chat_detail, str, 0.1 )
	end

	create9Button( ret_panel, c.send_btn, show_multi_text_input )
	--create9Button( ret_panel, c.send_btn, test )
	
	ret_panel.getContentSize = get_size
	xCenter(ret_panel)
	
	return ret_panel
end
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--创建聊天信息结点
local function createItemByListInfo(info)
	
	local tr = LIGHT_UI.clsTr:New( OneToOneChatConfig )
	local nodeObj = tr._RootNode
	nodeObj._user_id = info.id
	
	local function back()
		showPanel( HTTP_CLIENT.getRootNode(), 0, 0 )
	end

	local function on_click(msg_obj, x, y)
		--[[
		if info.sender ~= GLOBAL.uid then 
			PLAYINFO = Import("lua/player_info.lua")
			PLAYINFO.showByUserId(info.sender, back )
			closeOneToOneChatPagePanel()
			clearTimer( 'timer' )
		end
		]]
	end
	
	tr:setMsgDelegate( chat_page._move_chat_page._move_chat_grp, on_click )
	
	tr:addSprite( 'logo', info.logo )
	local n = tr:addLabel( 'name', info.sender_name )
	
	if info.online == 1 then
		tr:addSprite( 'online' )
	end
	
	local sex_res = info.sex == '0' and 'man.png' or 'woman.png'	
	local s = tr:addSprite( 'sex', sex_res )
	
	local t = tr:addLabel( 'time', info.time )
	
	
	if string.len( info.content ) < 1 then info.content = ' ' end
	local c = tr:addMultiLabel( 'content', info.content )
	
	--[[local height = s:getContentSize().height + n:getContentSize().height + 
	               t:getContentSize().height + c:getContentSize().height--]]
	tr:setHeight( 'content', OneToOneChatConfig.tr.split )
	--tr:setHeight( 'content', height )
	
	
	return nodeObj
end
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--获取聊天列表
local function on_get_chat_list(data)
	chat_list = {}
	if not chat_page then
		return
	end
	chat_page._move_chat_page._friend_say_page:clearAllItem()
	local real_data = data
	local temp = {}
	local data_len = table.getn(real_data)
	while data_len > 0 do
			table.insert(chat_list, real_data[data_len])
			chat_page._move_chat_page._friend_say_page:append_item(createItemByListInfo(real_data[data_len]))
			data_len = data_len - 1
	end			
	if chat_list ~= nil then
		chat_page._move_chat_page._friend_say_page:refresh_view()
	end
end
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--获取具体聊天内容
local function on_get_chat_detail(data)
	local real_data = data
	for _, list_info in ipairs(real_data) do
		table.insert(fsay_list, list_info)
		chat_page._move_chat_page._friend_say_page:append_item(createItemByListInfo(list_info))
	end

	chat_page._move_chat_page._friend_say_page:refresh_view()
		
end
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--请求
function request_chat_list(page,loading)
    local strConversation = string.format("%s,%s",GLOBAL.uid, uid2 )
	if loading then
		doCommand( 'message', 'lists', { page = cur_chatlist_page,conversation=strConversation}, on_get_chat_list,nil,{loading = 0} )
	else
		doCommand( 'message', 'lists', { page = cur_chatlist_page,conversation=strConversation}, on_get_chat_list )
	end
end
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--请求获取具体聊天内容
local function request_chat_detail(recvused_id,loading)
	if loading then
		doCommand( 'message', 'detail', { id = recvused_id}, on_get_chat_detail,nil,{loading=0} )
	else
		doCommand( 'message', 'detail', { id = recvused_id}, on_get_chat_detail )
	end
end
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--返回
local function on_back(btn)
	chat_page.setVisible(false)
end
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--关闭
function closeOneToOneChatPagePanel()
	--chat_page:setVisible(false)
	clear({chat_page})
	chat_page = nil
end
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--聊天列表页
local function createChatPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)
	local config = OneToOneChatConfig["move_page_config"]

	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_chat_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config.view_height)
	ret_panel._move_chat_grp:setVMovable(true)

	--local friend_say_page = LIGHT_UI.clsRowLayout:New(ret_panel, 0, 0, chat_desc_info)
	local friend_say_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, chat_desc_info)
	friend_say_page._use_dynamic_size = true 
	friend_say_page:refresh_view()

	local function onFOverTop(layout)
		cur_chatlist_page = 1
		request_chat_list(cur_chatlist_page,0)
	end

	local function onFOverBottom(layout)
	end

	friend_say_page.onHMoveOverTop = onFOverTop
	friend_say_page.onHMoveOverBottom = onFOverBottom


	local function get_y_limit(obj)
		return config.inner_y 
	end

	friend_say_page:setPosition(0, config.inner_y)
	ret_panel._move_chat_grp:appendItem(friend_say_page)
	ret_panel._move_chat_grp:selectPage(1)
	friend_say_page.getYLimit = get_y_limit
		
	ret_panel._friend_say_page = friend_say_page 

	return ret_panel
end
------------------------------------------------------------------------------------------

local back_func = nil

--初始化
function init( data )
	uid2 = data.uid2
	--uid2 = 18003
	name2 = data.name2

	if data.back_func then
		back_func = data.back_func
	end
end	

------------------------------------------------------------------------------------------
--创建面板
local function cratePanel(parent, x, y)
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = OneToOneChatConfig, nil
	local o = nil

	local function on_back(btn)
		clearTimer( 'timer' )
		closeOneToOneChatPagePanel()
		
		if back_func then
			back_func()
		else
			--MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
			local MSG = Import( "lua/message.lua" )			
			--MSG.showMsgPagePanel( HTTP_CLIENT.getRootNode(), 0, 0)
			if PANEL_CONTAINER.closeChildPanel( nil, 13 ) then
				PANEL_CONTAINER.addChild( MSG.showMsgPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			end
		end
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( name2 )
	
	
	config = OneToOneChatConfig["move_page_config"]
	ret_panel._move_chat_page = createChatPage(ret_panel, config.x, config.y)
	
	config = OneToOneChatConfig["send_page_config"]
	ret_panel._msg_chat_page = createMessagePage(ret_panel, config.x, config.y)
	
	config = OneToOneChatConfig["move_hint_config"]	

	cur_asay_page = 1

	local function on_click( obj, idx )
	end	
	return ret_panel
end
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--创建/显示
function showPanel( parent, x, y )
	if not chat_page then
		chat_page = cratePanel(parent, x, y)
	else
		chat_page._head_text:setString( name2 )
		chat_page:setVisible(true)
	end		
	request_chat_list(cur_chatlist_page,0)
	--设置3秒取一次消息
	startTimer()
end
------------------------------------------------------------------------------------------
