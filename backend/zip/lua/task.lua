-- task

TASK_LAYOUT = Import("layout/task.lua")
local TaskConfig = TASK_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local taskPage = nil

--[[
introduce
name
logo
status
id
--]]

local task_desc_info = { --单一单元格规范设定
	item_width = 480,
	item_height = 115,
	column_cnt = 1,
	x_space = 0,
	y_space = 0,
}

local function createMoveTaskPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = TaskConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_task_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 5, winSize.width, 733)
	ret_panel._move_task_grp:setVMovable(true)

	local task_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, task_desc_info)
	task_page:refresh_view()

	task_page:setPosition(0, config["inner_y"])
	ret_panel._move_task_grp:appendItem(task_page)
	ret_panel._move_task_grp:selectPage(1)

	ret_panel._task_page = task_page

	local function get_y_limit(obj)
		return config["inner_y"] 
	end

	task_page.getYLimit = get_y_limit
	
	return ret_panel
end

local function createTaskPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createContainerChildPanel( parent, x, y )

	local c, c2 = TaskConfig, nil
	local o = nil

	local function on_back(btn)
		closeTaskPagePanel()
		MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	--ret_panel._onBack = on_back
	--ret_panel._head_text:setString( '获得金币' )
	
	c2 = c.move_page_config
	ret_panel._move_task_page = createMoveTaskPage(ret_panel, c2.x, c2.y )
	ret_panel.closePanel = closeTaskPagePanel

	return ret_panel
end

local CHALLENGE_PAGE = Import("lua/challenge.lua")
local DETAIL_PAGE = Import("lua/task_detail.lua")
local function createTaskItem(info)
	local tr = LIGHT_UI.clsTr:New( TaskConfig )
	local nodeObj = tr._RootNode
	
	local function on_task_click(msg_obj, x, y)
		closeTaskPagePanel()
		DETAIL_PAGE.init( { id = info.id } )
		DETAIL_PAGE.showDetailPagePanel(HTTP_CLIENT.getRootNode(), 0, 0 )
	end
	
	tr:setMsgDelegate( taskPage._move_task_page._move_task_grp, on_task_click )

	tr:addSprite( 'logo', info.logo )
	tr:addLabel( 'name', info.name )
	--
	if tonumber(info.id) == 20 then 
		tr:addMultiLabel( 'introduce', getCharString(info.introduce,12) .. "..")
	elseif tonumber(info.id) == 35 then 
		tr:addMultiLabel( 'introduce', getCharString(info.introduce,12) .. "..")
	elseif tonumber(info.id) == 33 then 
		tr:addMultiLabel( 'introduce', getCharString(info.introduce,12) .. "..")
	else 	
		tr:addMultiLabel( 'introduce', getCharString(info.introduce,12) .. ".." )
	end 
	--
	local function onTaskFinish( data )
		showMessage( taskPage, data.memo )
	end
	
	local function on_task(btn, x, y)
		if btn._status == 1 then
			doCommand( 'task', 'finish', { id = info.id }, onTaskFinish,nil,{ loading = 0} )
			doCommand( 'task', 'lists', {}, on_task_list,nil,{ loading = 0} )
		else
			on_task_click()
		end
	end
	
	local c, c2 = TaskConfig, nil
	
	local _status = tonumber(info.status)
	c2 = c.btn
	if _status == 0 then
		c2.text = '未完成'
	elseif _status == 1 then
		c2.text = '领  取'
	elseif _status == 2 then
		c2.text = '已领取'
	end
	local task_btn = create9Button( nodeObj, c2, on_task )
	
	setMsgDelegate(task_btn, taskPage._move_task_page._move_task_grp, on_task)
	
	tr:setHeight( 'logo', 59 )

	return nodeObj
end

function on_task_list(data)
	if not taskPage then
		return
	end
	local row_page = taskPage._move_task_page._task_page
	row_page:clearAllItem()
	for _, task_info in pairs(data.data) do
		local item = createTaskItem(task_info)
		row_page:append_item(item)
	end
	row_page:refresh_view()
end

local refresh = nil
function init( data )
	if data.refresh then
		refresh = true
	end
end	

function showTaskPagePanel(parent, x, y, needRefresh )
	if not taskPage then
		taskPage = createTaskPagePanel(parent, x, y)
	else
		taskPage:setVisible(true)
	end

	local TTL = refresh and 0 or 60
	if needRefresh and needRefresh == true then
		doCommand( 'task', 'lists', {}, on_task_list )
	else
		doCommand( 'task', 'lists', {}, on_task_list, TTL )
	end
	
	return taskPage
end

function closeTaskPagePanel()
	--taskPage:setVisible(false)
	clear({taskPage})
	taskPage = nil
end

function refreshData()
	doCommand( 'task', 'lists', {}, on_task_list )
end

