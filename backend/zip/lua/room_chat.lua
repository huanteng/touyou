ROOM_CHAT_LAYOUT = Import("layout/room_chat.lua")
local RoomChatConfig = ROOM_CHAT_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local ChatZOrder = 100
local ChatInfoZOrder = 90

--local RoomChat = nil
--local MsgInfo = nil

local function sendChatMsg(msg)
	tcp_message(0,msg)
	--doCommand( 'room', 'say', { room_id = id,content = msg }, onSend)
end

function showChatBtn(_root)
	if _root then
		local function onClick()
			if _root:IsWatch() == false then
				showRoomChat(_root)
			else
				showMessage( _root, "旁观状态下不允许操作", {ms = 3000} )
			end
		end				
		local o = createButton(_root,RoomChatConfig.chat_btn,onClick)
	end
end

function hideRoomChat(_root)
	assert(_root)
	if _root.RoomChat then
		clear({_root.RoomChat})
		_root.RoomChat = nil
	end
end

function showRoomChat(_root)
	if _root.RoomChat then	
		clear({_root.RoomChat})		
		_root.RoomChat = nil				
	end
	local c = { x = 0,y = 0}
	local c2 = RoomChatConfig
	local order = _root:getCOObj():getChildrenCount() + ChatZOrder
	_root.RoomChat = createNode(_root,c,nil,order)	
	--o = createSprite(_root.RoomChat,c2.rootm_chat_bg)	
	local function onClick()
	end
	o = createButton(_root.RoomChat,c2.rootm_chat_bg,onClick)	
	for i = 1,tonumber(c2.msg_info.count) do	
		local c3 = c2['msg_btn'..i]			
		local function OnClick()
			--sendChatMsg(c3.text)
			sendChatMsg(i)
			hideRoomChat(_root)
			--sendChatMsg(_root:GetRoomID(),'聊天一\n下聊天')
		end
		local btn = create9Button( _root.RoomChat, c3, OnClick )			
	end	
	
	local function onClose()
		hideRoomChat(_root)
	end	
	local close =createButton(_root.RoomChat,c2.close_btn,onClose)							
end

--显示消息
function showMsg(_root,desk_id,msg)
	--旁观无法发言
	if not desk_id then
		return 
	end
	
	if not _root.MsgInfo then
		local c = { x = 0,y = 0}		
		local order = _root:getCOObj():getChildrenCount() + ChatInfoZOrder
		_root.MsgInfo = createNode(_root,c,nil,order)	
	end
		
	if _root.MsgInfo.desk_id then		
		clear({_root.MsgInfo.desk_id})
		_root.MsgInfo.desk_id = nil
		
	end
	c = RoomChatConfig
	_root.MsgInfo.desk_id = createNode(_root.MsgInfo,c)
	local c2 = c['msg_show'..desk_id]
	local o = createSprite(_root.MsgInfo.desk_id,c2)

	local edit = createMultiLabel( _root.MsgInfo.desk_id, c['detail'..desk_id] )
	--local msg_filter = string.gsub( msg, "<\/?[^>]*>",  '\r\n' )
	--msg_filter = string.gsub( msg_filter, "&nbsp;", "")		
	local c3 = c['msg_btn'..tonumber(msg)]			
	edit:setString(c3.text)	
	local function onHide()
		clear({_root.MsgInfo.desk_id})
		_root.MsgInfo.desk_id = nil
	end
	f_delay_do(_root.MsgInfo.desk_id,onHide,nil,c.msg_info.stay_time)	
	
end

