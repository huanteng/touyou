-- game hall panel

local gameHallPanel = nil

GAME_HALL_LAYOUT = Import("layout/main.lua")
local GameHallConfig = GAME_HALL_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local function normalAllBtn(panel)
	panel._main_page_btn:setNormalStatus()
	panel._friend_btn:setNormalStatus()
	panel._message_btn:setNormalStatus()
	panel._renew_btn:setNormalStatus()
end

local MAIN_PAGE = Import("lua/main.lua")
local FRIEND_PAGE = Import("lua/friend.lua")
local MSG_PAGE = Import("lua/message.lua")
local NEWS_PAGE = Import("lua/moving.lua")

local function set_default_button()
	normalAllBtn( gameHallPanel._bottom_panel )
	gameHallPanel._bottom_panel._main_page_btn:setClickStatus()	
end

--local function update_message_num()
function update_message_num()		
	local msg_size = 0
	local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_2", "{}" )
	local db_bin = json.decode( db_txt );
	for key, data in ipairs( db_bin ) do
		if not db_bin[key].readed then msg_size = msg_size + 1 end
	end
	local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_3", "{}" )
	local db_bin = json.decode( db_txt );
	for key, data in ipairs( db_bin ) do
		if not db_bin[key].readed then msg_size = msg_size + 1 end
	end
	local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_4", "{}" )
	local db_bin = json.decode( db_txt );
	for key, data in ipairs( db_bin ) do
		if not db_bin[key].readed then msg_size = msg_size + 1 end
	end

	if msg_size > 0 then
		local ret_panel = gameHallPanel._bottom_panel
		local x = 319
		local y = 65
		local offset = 0
		if msg_size > 9 then offset = 2	end
		local test1 = LIGHT_UI.clsSprite:New( ret_panel, x, y, "tab_unread_bg.png" )
		local test2 = LIGHT_UI.clsLabel:New( ret_panel, x - offset, y, msg_size, GLOBAL_FONT, 18 )
		test2:setTextColor(255, 255, 255)
	end
end

local function createBottomPanel(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	ret_panel._bottom_pic = LIGHT_UI.clsSprite:New(ret_panel, 0, 0, "mmfooter_bg.png")
	ret_panel._bottom_pic:setAnchorPoint(0, 0)

	local align_tbl = {}
	local function onMainPage(btn, x, y)
		normalAllBtn(ret_panel)
		btn:setClickStatus()
		MAIN_PAGE.closeAllPage()
		MAIN_PAGE.showMainPagePanel()
	end
	ret_panel._main_page_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 4, "tab_address_normal.png", "tab_address_pressed.png") 
	ret_panel._main_page_btn:setAnchorPoint(0, 0)
	ret_panel._main_page_btn:setString("") --九宫格导航主页的设置
	ret_panel._main_page_btn.onTouchEnd = onMainPage
	table.insert(align_tbl, ret_panel._main_page_btn)
	ret_panel._main_page_btn:setClickStatus()

	local function onFriend(btn, x, y)
		closeGameHallPanel()
		normalAllBtn(ret_panel)
		btn:setClickStatus()
		MAIN_PAGE.closeAllPage()
		FRIEND_PAGE.showFriendPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
	ret_panel._friend_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 4, "tab_find_frd_normal.png", "tab_find_frd_pressed.png") 
	ret_panel._friend_btn:setAnchorPoint(0, 0)
	ret_panel._friend_btn:setString("")    --九宫格导航好友的设置
	ret_panel._friend_btn.onTouchEnd = onFriend
	table.insert(align_tbl, ret_panel._friend_btn)

	local function onMessage(btn, x, y)
		closeGameHallPanel()
		normalAllBtn(ret_panel)
		btn:setClickStatus()
		MAIN_PAGE.closeAllPage()
		MSG_PAGE.showMsgPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
	ret_panel._message_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 4, "tab_weixin_normal.png", "tab_weixin_pressed.png") 
	ret_panel._message_btn:setAnchorPoint(0, 0)
	ret_panel._message_btn:setString("")  --九宫格导航消息的设置
	ret_panel._message_btn.onTouchEnd = onMessage
	table.insert(align_tbl, ret_panel._message_btn)

	local function onRenew(btn, x, y)
		closeGameHallPanel()
		normalAllBtn(ret_panel)
		btn:setClickStatus()
		MAIN_PAGE.closeAllPage()
		NEWS_PAGE.showNewsPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end
	ret_panel._renew_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 4, "tab_dynamic_normal.png", "tab_dynamic_pressed.png") 
	ret_panel._renew_btn:setAnchorPoint(0, 0)
	ret_panel._renew_btn:setString("") --九宫格导航动态的设置
	ret_panel._renew_btn.onTouchEnd = onRenew
	table.insert(align_tbl, ret_panel._renew_btn)	

	LIGHT_UI.alignXCenterForObjList(align_tbl, 60)

	return ret_panel
end

local function createGameHallPanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	ret_panel._bottom_panel = createBottomPanel(ret_panel, 0, 0)

	return ret_panel
end

function showGameHallPanel(parent, x, y)
	if not gameHallPanel then
		gameHallPanel = createGameHallPanel(parent, x, y)
	else
		gameHallPanel:setVisible(true)
	end
	set_default_button()
	update_message_num()
end

function closeGameHallPanel()
	--if gameHallPanel then gameHallPanel:setVisible(false) end
	if gameHallPanel then
		clear({gameHallPanel})
		gameHallPanel = nil
	end
end

