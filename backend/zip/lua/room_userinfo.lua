ROOM_USERINFO_LAYOUT = Import("layout/room_userinfo.lua")
local RoomUserInfiConfig = ROOM_USERINFO_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local UserInfo = nil


local function getUserInfo(_root,user_id)
	local function onUserInfo(data)
		if data.code > 0 then
			d(data.data)
			local c = RoomUserInfiConfig
			local order = _root:getCOObj():getChildrenCount() + 110
			local p = createNode(_root,{x = 0,y = 0},nil,order)
			local function onClose()
				clear({p})
			end
			local o = createButton(p,c.bg_btn,nil)			 
			local s = createButton(p,c.info_btn,nil) --	
			local close =createButton(p,c.close_btn,onClose)
			local attent = createLabel(p,c.attention_text)
			local function onAttention(btn)
				local function on_friend()																						
				end					
				if btn:getStatus() == 2 then
					attent:setString('关注')
					doCommand( 'user', 'delfriend', { friend = user_id }, on_friend )
					btn:setNormalStatus()
				else
					doCommand( 'user', 'addfriend', { friend = user_id }, on_friend )
					btn:setDownStatus()
					attent:setString('取消')
				end
			end
			local b = createButton(p,c.attention,onAttention) --
			b.onDownChangeSprite = empty_function
			b.onUpChangeSprite = empty_function
			if data.data.frendship == 1 then
				attent:setString('取消')
				b:setDownStatus()
			end
			c.name.text = data.data.name		
			local n = createLabel(p,c.name) --
			--n:setString(data.data.name)
			local logo_res = c.logo
			logo_res.res = data.data.logo
			createSprite(p,logo_res,nil)
			local sex_res = deepcopy(c.sex)
			local sex = tostring(data.data.sex)
			if sex == '1' then
				sex_res.res = 'woman.png'
			else
				sex_res.res = 'man.png'
			end
			createSprite(p,sex_res)			
			if  tonumber(data.data.vip_id) > 0 then
				local vip = c.vip
				vip.res = string.format( 'vip%i.png', data.data.vip_id)
				--vip.res = string.format( 'vip%i.png', 1)
				createSprite(p,vip)
			end
			if data.data.is_real ~= '0'	then		
				createSprite(p,c.video)
			end
			--获利			
			o = createLabel(p,c.profit)
			local str = data.data.profit
			if not str or type(str) ~= 'string' then
				o:setString("盈利:??")
			else
				o:setString(string.format("盈利:%d",data.data.profit))
			end
			
			--心情
			local mood = getCharString(data.data.mood,13)
			if string.len(data.data.mood) > string.len(mood) then
				mood = mood .. "..."
			end
			o = createLabel(p,c.mood)			
			o:setString(mood)
			--胜负
			o = createLabel(p,c.win_lose)
			o:setString(string.format("%s胜 %s负",data.data.win,data.data.lose))		
		end			
	end
	doCommand( 'user', 'detail', { id = user_id }, onUserInfo )
end

function showRoomUserInfo(_root,uid)
	getUserInfo(_root,uid)
end