-- light ui
SCREEN_480_800 = 1
CurScreenSize = SCREEN_480_800

function db_fmt(value)
	saved = {}
	if type(value) == type({}) then
		local retval = ''
		retval = retval .. '{'
		local visited = {}
		for i, v in ipairs(value) do
			if saved[v] and type(v) == type({}) then
				retval = retval .. '<visited>' .. ', '
			else
				saved[v] = true
				retval = retval .. db_fmt( v, saved ) .. ', '
			end
			visited[i] = true
		end
		for k, v in pairs(value) do
			if visited[k] == nil then
				if saved[v] and type(v) == type({}) then
					retval = retval .. '[' .. db_fmt(k, saved) .. '] = ' .. '<visited>' .. ', '
				else
					saved[v] = true
					retval = retval .. '[' .. db_fmt(k,saved) .. '] = ' .. db_fmt( v,saved ) .. ', '
				end
			end
		end
		retval = retval .. '}'
		return retval 
	elseif type(value) == type('') then
		return(string.format("'%s'",value))
	elseif value ~= nil then
		return(string.format("%s",tostring(value)))					
	elseif value == nil then
		return 'nil'
	end
	return "<unknown>"
end

function print_byte(str)
	print("begin byte", str, string.len(str))
	local ByteCount = string.len(str)
	local ByteIndex = 1
	while (ByteIndex <= ByteCount) do
		print(string.byte(string.sub(str, ByteIndex, ByteIndex)))
		ByteIndex = ByteIndex + 1
	end
	print("end byte")
end

-- light_ui 注意脚本对象的释放 调用Destroy
local MOVE_HORIZON = 0
local MOVE_VERTICAL = 1

function alignXCenter(panel)
	local winSize = CCDirector:sharedDirector():getWinSize()
	local size = panel:getContentSize()
	local cur_x, cur_y = panel:getPosition()
	panel:setPosition((winSize.width - size.width)/2, cur_y)
end

function alignXCenterForObjList(obj_list, x_gap)
	local item_size = obj_list[1]:getContentSize()
	local item_cnt = table.maxn(obj_list)
	local sum_width = item_size.width * item_cnt + (item_cnt - 1) * x_gap
	local winSize = CCDirector:sharedDirector():getWinSize()
	local startx = (winSize.width - sum_width) / 2
	local x_step = item_size.width + x_gap
	for _, obj in ipairs(obj_list) do
		local x, y = obj:getPosition()
		obj:setPosition(startx, y)
		startx = startx + x_step
	end
end

local shake_interval = 1/30 
function shakeObj(obj, move_delta, over_cb)
	local function do_shake()
		local x, y = obj._shake_info.org_x, obj._shake_info.org_y
		local cur_shake = (-1) * obj._shake_info.shake_delta
		x = x + cur_shake
		obj:setPosition(x, y)
		obj._shake_info["shake_delta"] = cur_shake
		obj._shake_info.shake_times = obj._shake_info.shake_times - 1
		if obj._shake_info.shake_times <= 0 then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(obj._shake_info.shake_cb)
			obj:setPosition(obj._shake_info.org_x, obj._shake_info.org_y)
			obj._shake_info = nil
			if over_cb then
				over_cb()
			end
		end
	end

	if obj._shake_info and obj._shake_info.shake_cb then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(obj._shake_info.shake_cb)
		obj._shake_info = nil
	end
	obj._shake_info = {} 
	obj._shake_info["shake_times"] = 20 
	obj._shake_info["shake_delta"] = math.abs(move_delta)
	local x, y = obj:getPosition()
	obj._shake_info["org_x"] = x
	obj._shake_info["org_y"] = y
	obj._shake_info["shake_cb"] = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(do_shake, shake_interval, false)
end

local move_interval = 1/60
function moveObj(obj, velocity, move_delta, end_cb)
	if obj._move_cb then
		return
	end

	local sum_delta = 0
	local function do_move()
		local inc_delta = velocity * move_interval 
		sum_delta = sum_delta + math.abs(inc_delta)
		if sum_delta > move_delta then
			local dec_delta = sum_delta - move_delta
			if velocity > 0 then
				inc_delta = inc_delta - dec_delta
			else
				inc_delta = inc_delta + dec_delta
			end
			sum_delta = move_delta
		end
		local x, y = obj:getPosition()
		x = x + inc_delta
		obj:setPosition(x, y)
		if sum_delta >= move_delta then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(obj._move_cb)
			obj._move_cb = nil
			if end_cb then
				end_cb()
			end
		end
	end

	obj._move_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(do_move, move_interval, false)
end

local function makeObjMouseEventHandle2(obj)
        local function onTouch(eventType, x, y)
		if eventType == CCTOUCHBEGAN then
			return obj:onTouchBegan(x, y)
		elseif eventType == CCTOUCHMOVED then
			return obj:onTouchMove(x, y)
		else
			return obj:onTouchEnd(x, y)
		end
        end

        obj:registerScriptTouchHandler(onTouch, false, 0, true)
        obj:setTouchEnabled(true)
end

local function makeObjMouseEventHandle(obj)
        local function onTouch(eventType, x, y)
		if eventType == CCTOUCHBEGAN then
			obj:onTouchBegan(x, y)
			return true
		elseif eventType == CCTOUCHMOVED then
			return obj:onTouchMove(x, y)
		else
			return obj:onTouchEnd(x, y)
		end
        end

        obj:registerScriptTouchHandler(onTouch, false, 0, true)
        obj:setTouchEnabled(true)
end

function clsObject:setContentSize( width, height )
	-- by dda, 20130619
end

function clsObject:setAnchorPoint( x, y )
	-- by dda, 20130619
end

clsRowLayout = clsObject:Inherit()

function clsRowLayout:__init__(parent, x, y, desc_info)
	Super(clsRowLayout).__init__(self)

	self._RootPanel = CCNode:create()
	self._RootPanel:setAnchorPoint(CCPointMake(0, 1))

	if parent then
		parent:addChild(self)
		self._RootPanel:setPosition(x, y)
	end

	self._item_width = desc_info["item_width"]
	self._item_height = desc_info["item_height"]
	self._column_cnt = desc_info["column_cnt"]
	self._x_space = desc_info["x_space"]
	self._y_space = desc_info["y_space"]
	self._visible_tag = true 
	self._use_dynamic_size = false

	self._view_item_list = {}
	self._item_list = {} 
end

function clsRowLayout:clearAllItem()
	for _, item in pairs(self._item_list) do
		item:removeFromParentAndCleanup(true)
	end

	self._item_list = {}
	self._view_item_list = {}
end

function clsRowLayout:getItemList()
	return self._item_list
end

--[[function clsRowLayout:replaceItem( key, item )
	self._item_list[ key ]:removeFromParentAndCleanup(true)
	self._item_list[ key ] = item
	self._RootPanel:addChild(self._item_list[ key ]:getCOObj())
	self._item_list[ key ]:setAnchorPoint(0, 1)
	self._item_list[ key ]:setPosition(0, 0)
	self._item_list[ key ]:setVisible(false)
end--]]

function clsRowLayout:clearItem( key )
	self._item_list[ key ]:removeFromParentAndCleanup(true)
	table.remove( self._item_list, key )		
end

function clsRowLayout:getCOObj()
	return self._RootPanel
end

function clsRowLayout:doFunc(func)
	for _, item in pairs(self._item_list) do
		func(item)
	end
end

function clsRowLayout:getPosition()
	return self._RootPanel:getPosition()
end

function clsRowLayout:setPosition(x, y)
	self._RootPanel:setPosition(x, y)
end

function clsRowLayout:cal_pos_by_ij(i, j)
	return (j-1) * (self._item_width + self._x_space), -(i-1) * (self._item_height + self._y_space)	
end

function clsRowLayout:append_item(item)
	table.insert(self._item_list, item)
	self._RootPanel:addChild(item:getCOObj())
	item:setAnchorPoint(0, 1)
	item:setPosition(0, 0)
	item:setVisible(false)
	return item 
end

function clsRowLayout:get_item(row, column)
	local idx = (row-1) * self._column_cnt + column
	item_cnt = table.maxn(self._item_list)
	if (item_cnt <= 0) then
		return nil 
	end
	if (item_cnt >= idx) then
		return self._item_list[idx]
	else
		return nil 
	end
end

function clsRowLayout:get_item_by_func(func)
	for _, item in ipairs(self._item_list) do
		if func(item) then
			return item
		end
	end

	return nil
end

function clsRowLayout:_get_append_row(append_list)
	local row_cnt = table.maxn(append_list)
	if row_cnt <= 0 then
		return 1 
	end

	local last_row = append_list[row_cnt]
	local last_row_item_cnt = table.maxn(last_row)
	if last_row_item_cnt < self._column_cnt then
		return row_cnt
	else
		return row_cnt + 1
	end
end

function clsRowLayout:_do_append_item(item, append_list)
	local append_row = self:_get_append_row(append_list)
	local row_cnt = table.maxn(append_list)
	if row_cnt < append_row then
		table.insert(append_list, {})
	end

	local row_info = append_list[append_row]
	table.insert(row_info, item)
end

function clsRowLayout:hideAll()
	for _, item in ipairs(self._item_list) do
		item:getCOObj():setVisible(false)
		item:getCOObj():setPosition(0, 0)
	end
end

function clsRowLayout:filter_item()
	self._view_item_list = {} 
	for _, item in ipairs(self._item_list) do
		self:_do_append_item(item, self._view_item_list)
	end
end

local function refresh_view_by_fix(row_obj)
	for row_idx, row_info in ipairs(row_obj._view_item_list) do
		for column_idx, item in ipairs(row_info) do
			local x, y = row_obj:cal_pos_by_ij(row_idx, column_idx)
			item:getCOObj():setPosition(x, y)
			item:getCOObj():setVisible(true)
		end
	end
end

local function refresh_view_by_dyn(row_obj)
	local pre_height = 0
	for row_idx, row_info in ipairs(row_obj._view_item_list) do
		local max_height = 0
		for column_idx, item in ipairs(row_info) do
			local cur_x = 0
			local cur_y = 0
			if column_idx == 1 then
				cur_x = 0
				cur_y = -pre_height
			else
				local pre_idx = column_idx - 1
				local pre_item = row_info[pre_idx]
				local pre_x, pre_y = pre_item:getPosition()
				local pre_size = pre_item:getContentSize()
				cur_x = pre_x + pre_size.width + row_obj._x_space
				cur_y = pre_y
			end
			item:setPosition(cur_x, cur_y)
			item:setVisible(true)
			local item_size = item:getContentSize()
			if math.abs(cur_y) + item_size.height > max_height then
				max_height = math.abs(cur_y) + item_size.height
			end
		end
		pre_height = max_height
	end
end

function clsRowLayout:refresh_view()
	self._view_item_list = {}
	self:hideAll()
	self:filter_item()
	if not self._use_dynamic_size then
		refresh_view_by_fix(self)
	else
		refresh_view_by_dyn(self)
	end
end

function clsRowLayout:setVisible(tag)
	self._visible_tag = tag
	self._RootPanel:setVisible(tag)
end

function clsRowLayout:getVisible()
	return self._visible_tag
end

function clsRowLayout:getContentSize()
	if self._use_dynamic_size then
		local last_item = self._item_list[table.maxn(self._item_list)]
		if not last_item then
			return { ["width"] = self._item_width, ["height"] = self._item_height, }
		end
		local x, y = last_item:getPosition()
		local size = last_item:getContentSize()
		return { ["width"] = size.width, ["height"] = size.height + math.abs(y)}
	else
		local ret_tbl = {}
		local row_cnt = table.maxn(self._view_item_list)
		ret_tbl["width"] = self._column_cnt * (self._item_width + self._x_space) - self._x_space
		ret_tbl["height"] = row_cnt * (self._item_height + self._y_space) - self._y_space
		return ret_tbl
	end
end
--------------------------------------------------------------------------
clsSimpleButton = clsObject:Inherit()

function clsSimpleButton:__init__(parent, x, y, normal_pic, click_pic)
	normal_pic = getResFilename( normal_pic )
	click_pic = getResFilename( click_pic )
	
	Super(clsSimpleButton).__init__(self)
	local menuItem = CCMenuItemImage:create(normal_pic, click_pic)
        local function onTouch(eventType, x, y)
		if eventType == CCTOUCHBEGAN then
			self:onTouchBegan(x, y)
			return true
		elseif eventType == CCTOUCHMOVED then
			self:onTouchMove(x, y)
			return nil
		else
			self:onTouchEnd(x, y)
			return nil
		end
        end
	menuItem:setPosition(0, 0)
	self._rootNode = CCNode:create()
	self._BtnObj = CCMenu:createWithItem(menuItem)
	self._BtnObj:registerScriptTouchHandler(onTouch, false, 0, true)
	self._BtnObj:setPosition(0, 0)
	self._rootNode:addChild(self._BtnObj)
	self._BtnTxt = CCLabelTTF:create("", GLOBAL_FONT, 16)
	self._menuItem = menuItem
	self._rootNode:addChild(self._BtnTxt)
	local size = menuItem:getContentSize()
	self._BtnTxt:setAnchorPoint(CCPointMake(0.5, 0.5))
	self._BtnTxt:setPosition(0, 0)

	if parent then
		parent:addChild(self)
		self._rootNode:setPosition(x, y)
	end
end

function clsSimpleButton:setNormalImg(img) -- 传入 clsSprite
	self._menuItem:setNormalImage(img)
end

function clsSimpleButton:setSelectedImg(img) -- clsSprite
	self._menuItem:setSelectedImage(img)
end

function clsSimpleButton:setAnchorPoint(x, y)
	self._menuItem:setAnchorPoint(CCPointMake(x, y))
	local size = self._menuItem:getContentSize()
	local x_offset = size.width / 2 - size.width * x
	local y_offset = size.height / 2 - size.height * y
	self._BtnTxt:setPosition(x_offset, y_offset)
end

function clsSimpleButton:setOpacity(op)
	self._menuItem:setOpacity(op)
end

function clsSimpleButton:getPosition()
	return self._rootNode:getPosition()
end

function clsSimpleButton:setButtonScaleX(x)
	self._menuItem:setScaleX(x)
end

function clsSimpleButton:setButtonScaleY(y)
	self._menuItem:setScaleY(y)
end

function clsSimpleButton:setButtonScale(scale)
	self._menuItem:setScale(scale)
end

function clsSimpleButton:convertToWorldSpace(x, y)
	local point = self._rootNode:convertToWorldSpace(CCPointMake(x, y))
	return point.x, point.y
end

function clsSimpleButton:getContentSize()
	return self._menuItem:getContentSize()
end

function clsSimpleButton:setString(text)
	if not text or type(text) ~= 'string' then
		return
	else
		self._BtnTxt:setString(text)
	end
	
end

function clsSimpleButton:getText()
	return self._BtnTxt:getString()
end

function clsSimpleButton:setTextY(yvalue)
	local x, y = self._BtnTxt:getPosition()
	self._BtnTxt:setPosition(x, yvalue)
end

function clsSimpleButton:getCOObj()
	return self._rootNode
end

function clsSimpleButton:setPosition(x, y)
	self._rootNode:setPosition(x, y)
end

function clsSimpleButton:setTextColor(r, g, b)
	self._BtnTxt:setColor(ccc3(r, g, b))
end

function clsSimpleButton:setVisible(tag)
	self._rootNode:setVisible(tag)
end

function clsSimpleButton:onTouchBegan(x, y)
end

function clsSimpleButton:onTouchMove(x, y)
end

function clsSimpleButton:onTouchEnd(x, y)
end

function clsSimpleButton:removeFromParentAndCleanup(tag)
	self._rootNode:removeFromParentAndCleanup(tag)
end
-------------------------------------------------------------
clsLabel = clsObject:Inherit()

function clsLabel:__init__(parent, x, y, text, font, size)
	Super(clsLabel).__init__(self)
	self._RootLabel = CCLabelTTF:create(text, font, size)
	if parent then
		parent:addChild(self)
		self._RootLabel:setPosition(x, y)
	end
end

function clsLabel:getCOObj()
	return self._RootLabel
end

function clsLabel:setTextColor(r, g, b)
	self._RootLabel:setColor(ccc3(r, g, b))
end

function clsLabel:setVisible(tag)
	self._RootLabel:setVisible(tag)
end

function clsLabel:setString(value)
	if not value or type(value) ~= 'string' then 
		--self._RootLabel:setString("bug label nil")
		--CCMessageBox("label nil",'')
	--elseif string.len(tostring(value)) <= 0 then 
		--self._RootLabel:setString("label strlen 0")
		--CCMessageBox("label nil",'')
		return 
	else
		self._RootLabel:setString(value)
	end
end

function clsLabel:getString()
	return self._RootLabel:getString()
end

function clsLabel:setPosition(x, y)
	self._RootLabel:setPosition(x, y)
end

function clsLabel:getPosition()
	return self._RootLabel:getPosition()
end

function clsLabel:getContentSize()
	return self._RootLabel:getContentSize()
end

function clsLabel:setAnchorPoint(x, y)
	self._RootLabel:setAnchorPoint(CCPointMake(x, y))
end

function clsLabel:removeFromParentAndCleanup(tag)
	self._RootLabel:removeFromParentAndCleanup(tag)
end
--------------------------------------------------------------------
local function getCharString2(txt,count, pos )
	local str = ""
	local cnt = 0
	local tmp_pos = pos + 1	
	local i = 1
	while(i <= string.len(txt)) do
		local len = string.len(txt)
		local b = txt:byte(i)				
		if b > 128 then
			str = string.sub(txt,1,i+2)
			i = i + 3
		else
			str = string.sub(txt,1,i)
			i = i + 1
		end
		cnt = cnt + 1
		
		if cnt >= count then
			break
		end
		tmp_pos = tmp_pos + 1
	end
	return string.sub(txt, pos, tmp_pos - 1), tmp_pos - pos
end

local function isAChar(char)
	local char_value = string.byte(char)	
	--print( char_value )
	return char_value >= 192 or (char_value >= 0 and char_value <= 127)
end

local byteCount = 0
local function judgeTextByte(txt, len, pos)
	local tmp_pos = pos + 1	
	byteCount = 0	
	while (tmp_pos <= len) do
		print( "byteCount:" .. byteCount )
		byteCount = byteCount + 1	
		if isAChar(string.sub(txt, tmp_pos, tmp_pos)) then
			if byteCount ~= 1 then
				if byteCount ~= 3 then
					if byteCount ~= 5 then
						return false
					end
				end
			end
			byteCount = 0
		end
		tmp_pos = tmp_pos + 1
	end
	if len == 0 then
		return true
	end
	print( string.sub(txt, tmp_pos - byteCount, tmp_pos ) )	
	if byteCount ~= 1 then
		if byteCount ~= 3 then
			if byteCount ~= 5 then
				return false
			end
		end
	end
	
	return true
end

local function getAChar(txt, len, pos)
	local tmp_pos = pos + 1		
	while (tmp_pos <= len) do
		--local char_value = string.byte( string.sub(txt, tmp_pos, tmp_pos ) )		
		if isAChar(string.sub(txt, tmp_pos, tmp_pos)) then
			return string.sub(txt, pos, tmp_pos - 1), tmp_pos - pos 			
		end
		tmp_pos = tmp_pos + 1
	end
	return string.sub(txt, pos, tmp_pos - 1), tmp_pos - pos 
end

local function adjustTextByWidth(obj)
	local text = obj._text
	local text_len = string.len(text)
	local char_idx = 1
	local idx = 1
	local row_idx = 1
	local cur_row_height = 0
	local pre_row_y = 0
	local pre_row_height = 0
	local segment_str_list = {} -- idx 之前
	local start_idx = 1
	
	--[[local result = judgeTextByte(text, text_len, idx)
	if result == false then
		text = "该文本含有非法字符，无法正常显示"
	else
		text = tostring( result )
	end--]]
	--text = judgeTextByte(text, text_len, idx)
	
	while (idx <= text_len) do
		local char, char_byte_cnt = getAChar(text, text_len, idx)
		if obj._password_char then
			obj._SizeLabel:setString(obj._password_char)
		elseif char and char ~= -1 then		
			obj._SizeLabel:setString(char)
		else
			obj._SizeLabel:setString( "bug label nil" )
		end					
		
		-- by dda, 20130626
		--local char_size = obj._SizeLabel:getPixelSize()
		local char_size = obj._SizeLabel:getContentSize()
		
		if cur_row_height < char_size.height then
			cur_row_height = char_size.height
		end
		if not obj._text_rec[row_idx] then
			obj._text_rec[row_idx] = {}
		end
		local row_info = obj._text_rec[row_idx]

		if char_idx == 1 then
			row_info[char_idx] = {
				["x"] = 0,
				["y"] = pre_row_y + pre_row_height,
				["char_width"] = char_size.width,
				["char_height"] = char_size.height,
			}
			idx = idx + char_byte_cnt
			char_idx = char_idx + 1
		else
			local pre_char_info = row_info[char_idx - 1]
			
			-- by dda, 20130712，加回车支持
			--if ((pre_char_info["x"] + pre_char_info["char_width"] + char_size.width) > obj._view_width) then
			if ((pre_char_info["x"] + pre_char_info["char_width"] + char_size.width) > obj._view_width) or char == '\n' then
				pre_row_y = pre_row_y + pre_row_height
				pre_row_height = cur_row_height
				row_idx = row_idx + 1
				char_idx = 1
				cur_row_height = 0
				local sub_str = string.sub(text, start_idx, idx - 1)
				table.insert(segment_str_list, sub_str)
				start_idx = idx
			else
				row_info[char_idx] = {
					["x"] = pre_char_info["x"] + pre_char_info["char_width"],
					["y"] = pre_char_info["y"],
					["char_width"] = char_size.width,
					["char_height"] = char_size.height,
				}
				idx = idx + char_byte_cnt
				char_idx = char_idx + 1
			end
		end
	end
	local sub_str = string.sub(text, start_idx)
	table.insert(segment_str_list, sub_str)
	local dis_str = table.concat(segment_str_list, "\n")
	if obj._password_char then
		local str_cnt = string.len(dis_str)
		local pass_list = {}
		for i = 1, str_cnt do
			table.insert(pass_list, obj._password_char)
		end
		dis_str = table.concat(pass_list)
	end		
	
	if char == -1 then
		obj._RootLabel:setString( "bug label nil" )
	elseif dis_str then		
		obj._RootLabel:setString(dis_str)
	else
		obj._RootLabel:setString( "bug label nil" )
	end
end

clsMultiLabel = clsObject:Inherit() -- bug ??? enter key
function clsMultiLabel:__init__(parent, x, y, text, font, size, view_width, view_height)
	Super(clsMultiLabel).__init__(self)
	self._RootNode = CCNode:create()

	self._RootLabel = CCLabelTTF:create("", font, size, CCSizeMake(0, 0), kCCTextAlignmentLeft)
	self._RootLabel:setAnchorPoint(CCPointMake(0, 1))
	self._RootLabel:setPosition(0, 0)
	
	self._SizeLabel = CCLabelTTF:create("", font, size, CCSizeMake(0, 0), kCCTextAlignmentLeft)
	self._SizeLabel:setAnchorPoint(CCPointMake(0, 1))
	self._SizeLabel:setPosition(0, 0)
	self._SizeLabel:setVisible(false)

	self._RootNode:addChild(self._RootLabel)
	self._RootNode:addChild(self._SizeLabel)

	if parent then
		parent:addChild(self)
		self._RootNode:setPosition(x, y)
	end

	self._text_rec = {}

	self._view_width = view_width
	self._view_height = view_height
	self._text = text
	adjustTextByWidth(self)
	self._password_char = nil
end

function clsMultiLabel:setPasswordChar(char)
	self._password_char = char
	
end

function clsMultiLabel:getCOObj()
	return self._RootNode
end

function clsMultiLabel:setTextColor(r, g, b)
	self._RootLabel:setColor(ccc3(r, g, b))
end

function clsMultiLabel:setVisible(tag)
	self._RootNode:setVisible(tag)
end

function clsMultiLabel:getText()
	return self._text
end

function clsMultiLabel:getString()
	-- by dda, 20130708
	return self._text
end

function clsMultiLabel:getPosInfo()
	return self._text_rec
end

function clsMultiLabel:getViewSize()
	return self._view_width, self._view_height
end

function clsMultiLabel:setPosition(x, y)
	self._RootNode:setPosition(x, y)
end

function clsMultiLabel:getPosition()
	-- by dda, 20130712
	return self._RootNode:getPosition()
end

function clsMultiLabel:getCharInfoByIdx(idx)
	local tmp_idx = 1
	for row_idx, row_info in ipairs(self._text_rec) do
		for column, char_info in ipairs(row_info) do
			if tmp_idx == idx then
				return char_info
			end
			tmp_idx = tmp_idx + 1
		end
	end
	return nil
end

function clsMultiLabel:setString(txt)
	if not txt or type(txt) ~= 'string' then 
		--self._text = ("bug label nil")
		--CCMessageBox("label nil",'')
	--elseif string.len(tostring(txt)) <= 0 then 
		--self._text = ("label strlen 0")
		--CCMessageBox("label nil",'')
		return
	else
		self._text = txt
		if self then
			adjustTextByWidth(self)
		end
	end
	
end

function clsMultiLabel:getContentSize()
	-- by dda, 20130626
	--return self._RootLabel:getPixelSize()
	return self._RootLabel:getContentSize()
end

function clsMultiLabel:getPixelSize()
	-- by dda, 20130626
	return self._RootLabel:getPixelSize()
end

------------------------------------------------------------------------
STAGE_FREEZE = 0
STAGE_DECIDE = 1
STAGE_FOLLOW = 2
STAGE_AUTOMOVE = 3

local function clearMoveStatus(obj)
	if obj._move_cb then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(obj._move_cb)
		obj._move_cb = nil
	end
	obj._obj_stage = STAGE_FREEZE
	obj._move_type = nil 
	obj._begin_x = nil
	obj._begin_y = nil
	obj._last_move_x = nil
	lastMoveY = nil
	obj._last_time = nil
	obj._touch_end_time = nil
	obj._im_velocity = nil
	obj._touch_began_tag = false
	obj._h_nearest = nil
	obj._auto_sum = 0
	for _, attr_info in pairs(obj._attr_list) do
		attr_info["can_auto_move"] = true
	end
end

local time_interval = 1/60
local topType = 3
local function moveInnerObj(obj, inc_delta)
	
	local real_move = inc_delta
	local middle_view = obj._view_width / 2
	for idx, inner_obj in ipairs(obj._item_list) do
		local x, y = inner_obj:getPosition()		

		if obj._move_type == MOVE_HORIZON then
			local pre_x = x
			x = x + inc_delta
			local item_size = inner_obj:getContentSize()
			local view_width = obj._view_width
			local middle_x = (view_width - item_size["width"]) / 2
			if (middle_x == pre_x) or ((middle_x > pre_x) and (middle_x <= x)) or ((middle_x < pre_x) and (middle_x >= x)) then
				obj:onHCrossMiddle(idx, x-middle_x)
			end
			local page_left_x = x - middle_x
			local page_right_x = x + item_size["width"] + middle_x
			if (page_left_x >= 0 and page_left_x <= middle_view) or (page_right_x >= middle_view and page_right_x <= obj._view_width) then
				obj:onHNearest(idx)
			end
		else
			if (not obj._v_move_idx) or (obj._v_move_idx == idx) then
				y = y + inc_delta				
				--print( y )	
				if inner_obj._view_item_list then				
					for k, v in ipairs(inner_obj._view_item_list) do
						if obj.tipHeight then
							if v[ 1 ].tip then
								if y < obj.tipHeight - 70 then
									v[ 1 ].tip:setString( "松开刷新" )								
									--topType = 1
								elseif y < obj.tipHeight then								
									v[ 1 ].tip:setString( "下拉刷新" )
									--topType = 2
								else
									v[ 1 ].tip:setString( "等待刷新" )
									--topType = 3
								end
							end
							if v[ 1 ].arrow then
								if y < obj.tipHeight - 70 then
									--v[ 1 ].tip:setString( "松开刷新" )
									if topType == 2 then
										a_rotate(v[ 1 ].arrow:getCOObj(),1,-180)
									end
									topType = 1
								elseif y < obj.tipHeight then								
									--v[ 1 ].tip:setString( "下拉刷新" )
									if topType == 1 then
										a_rotate(v[ 1 ].arrow:getCOObj(),1,0)
									end
									topType = 2
								else
									--v[ 1 ].tip:setString( "等待刷新" )
									topType = 3
								end
							end
									
						end
					end
				end
				
				if inner_obj.getYLimit and y < inner_obj:getYLimit() then
					obj._attr_list[idx]["can_auto_move"] = false
					y = inner_obj:getYLimit()
					inner_obj:setPosition(x, y)
					if inner_obj.onHMoveOverTop and obj._obj_stage == STAGE_FOLLOW then
						inner_obj:onHMoveOverTop()	
					end
					if haveToundEnd == true then
						clearMoveStatus(obj)
					end
					if obj.tipHeight then
						if y < obj.tipHeight then
							if haveToundEnd == true then
								obj.resetListPos()
							end																									
						end
					end				
					return real_move
				end

				local size = inner_obj:getContentSize()
				if size["height"] <= obj._view_height then
					--CCMessageBox("here",'')	
					if y > obj._view_height then
						y = obj._view_height
					end
				else
					
					if y > size["height"] then
						y = size["height"]
						inner_obj:setPosition(x, y)
						if inner_obj.onHMoveOverBottom and obj._obj_stage == STAGE_FOLLOW then
							inner_obj:onHMoveOverBottom()
						end
						
						clearMoveStatus(obj)						
						if obj.tipHeight then
							if y < obj.tipHeight then
								if haveToundEnd == true then
									obj.resetListPos()
								end																									
							end
						end
						return real_move
					end
				end
			end
		end
		if not obj._move_type then -- 在cross里面是有可能改变这个状态的
			return real_move
		end

		inner_obj:setPosition(x, y)
	end
	return real_move
end

local function aniMovePanel(obj, initVelocity, mul_factor, acc)
	if table.maxn(obj._item_list) <= 0 then
		clearMoveStatus(obj)
		return
	end

	acc = acc * mul_factor
	tmp_velocity = initVelocity
	local function do_move()	
		local inc_delta = tmp_velocity * time_interval
		if inc_delta > 0 then
			inc_delta = inc_delta + 10
		else
			inc_delta = inc_delta - 10
		end	
		local limit = 35
		if obj._move_type == MOVE_HORIZON then limit = 60 end
		if inc_delta > limit then inc_delta = limit end
		if inc_delta < -limit then inc_delta = -limit end
		
		moveInnerObj(obj, inc_delta)

		tmp_velocity = tmp_velocity + acc * time_interval
		if ((tmp_velocity*initVelocity) <= 0) then
			clearMoveStatus(obj)
			obj:onMoveEnd(initVelocity)
		end
	end
	if math.abs(initVelocity) > 0 then
		obj._move_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(do_move, time_interval, false)
		obj._obj_stage = STAGE_AUTOMOVE 
	end
end

clsMoveGroup = clsObject:Inherit()

function clsMoveGroup:isAutoMove()
	return self._obj_stage == STAGE_AUTOMOVE
end

function clsMoveGroup:onTouchBegan(x, y)
	clearMoveStatus(self)
	haveToundEnd = false
	if table.maxn(self._item_list) <= 0 then
		return
	end

	if self._obj_stage == STAGE_AUTOMOVE then
		if self._move_type == MOVE_HORIZON and not self._h_breakable then
			return
		end
		if self._move_type == MOVE_VERTICAL and not self._v_breakable then
			return
		end
	end
	--clearMoveStatus(self)
	self._begin_x = x
	self._begin_y = y
	self._touch_began_tag = true	
end

function clsMoveGroup:onHCrossMiddle(idx, cross_delta)
end

function clsMoveGroup:onHNearest(idx)
	self._h_nearest = idx
end

function clsMoveGroup:preHMove(direction)
end

local h_direction_delta = 3 
local v_direction_delta = 0 
local max_move_speed = 2000
local min_move_speed = 1000
local freeze_time_value = 100000 

local lastValue = nil
local function checkSame( val1, val2 )
	local temp = val1 - val2
	if not lastValue then
		lastValue = temp
		return false
	else
		if lastValue == temp then
			return true
		else
			lastValue = temp
			return false
		end			
	end
end

function clsMoveGroup:onTouchMove(x, y)
	--if not self._touch_began_tag then
	if self._touch_began_tag == false then
		return
	end	
	--CCMessageBox( "xy:" .. x ,'')	
	if not self._move_type then
		local curx, cury = x, y
		if math.abs(curx - self._begin_x) > h_direction_delta then
			self._move_type = MOVE_HORIZON
			if not self._h_movable then
				clearMoveStatus(self)				
				return
			end
		end
		if self._slideDir then
			if self._slideDir ~= 0 then
				if math.abs(cury - self._begin_y) > v_direction_delta then
					self._move_type = MOVE_VERTICAL
					if not self._v_movable then
						clearMoveStatus(self)	
						return
					end
				end
			end
		else
			if math.abs(cury - self._begin_y) > v_direction_delta then
				self._move_type = MOVE_VERTICAL
				if not self._v_movable then
					clearMoveStatus(self)	
					return
				end
			end
		end
		if self._move_type then
			self._last_move_x = x
			lastMoveY = y
			self._obj_stage = STAGE_FOLLOW
			if self._move_type == MOVE_HORIZON then			
				local dir = ""
				if curx - self._begin_x > 0 then
					dir = 'left'
				else
					dir = 'right'
				end	
				if self:preHMove(dir) == false then
					clearMoveStatus(self)	
					return
				end			
			end
			self._auto_sum = 0
		else
			self._obj_stage = STAGE_DECIDE
		end
	end

	if self._move_type then
		if self._move_type == MOVE_HORIZON then		
			local x_delta = x - self._last_move_x
			--CCMessageBox( "x_delta:" .. x_delta ,'')
			if x_delta == 0 then
				return
			end
			if self._auto_sum * x_delta < 0 then
				self._auto_sum = 0
			end
			
			local limit = 25
			if self._move_type == MOVE_HORIZON then limit = 60 end
			if x_delta > limit then x_delta = limit end
			if x_delta < -limit then x_delta = -limit end
			
			self._auto_sum = self._auto_sum + moveInnerObj(self, x_delta) 
			self._last_move_x = x
			local last_time = self._last_time
			self._last_time = httpMainModule.gettimeofdayCocos2d()
			if last_time and self._last_time - last_time > freeze_time_value then
				self._auto_sum = 0
			end
			if last_time then
				self._im_velocity = x_delta*1000000 / (self._last_time - last_time)
			else
				if x_delta > 0 then
					self._im_velocity = max_move_speed 
				else
					self._im_velocity = -max_move_speed 
				end
			end
		else
		--CCMessageBox("y" .. y,'')		
			local y_delta = y - lastMoveY	
			if checkSame( y, lastMoveY ) == true then
				return
			end
			lastMoveY = y		
				
			if self._auto_sum * y_delta < 0 then
				self._auto_sum = 0
			end
			
			local limit = 10
			if self._move_type == MOVE_HORIZON then limit = 60 end
			if y_delta > limit then y_delta = limit end
			if y_delta < -limit then y_delta = -limit end									
			self._auto_sum = self._auto_sum + moveInnerObj(self, y_delta)			
			
			local last_time = self._last_time
			self._last_time = httpMainModule.gettimeofdayCocos2d()
			if last_time and self._last_time - last_time > freeze_time_value then
				self._auto_sum = 0
			end
			if last_time then
				self._im_velocity = y_delta*1000000 / (self._last_time - last_time)
			else
				self._im_velocity = 0
			end
		end
	end		
end

--local freezeTime = 300400		--在PC上测试使用
--local freezeTime = 500 		--在手机上测试时使用
local freezeTime = 1000
local autoSumDelta = 15 
function clsMoveGroup:checkAutoMove()
	if self._move_type == MOVE_VERTICAL and self._v_move_idx and (not self._attr_list[self._v_move_idx]["can_auto_move"]) then
		return false
	end

	if not self._last_time then
		return false
	end
	
	local delta_time = self._touch_end_time - self._last_time	
	--CCMessageBox("delta_time" .. delta_time,'')
	--CCMessageBox("self._auto_sum" .. self._auto_sum,'')
	if delta_time >= freezeTime or math.abs(self._auto_sum) < autoSumDelta then	
		return false
	else
		return true
	end
end
haveToundEnd = false
function clsMoveGroup:onTouchEnd(x, y)
	haveToundEnd = true
	--if not self._touch_began_tag then
	for idx, inner_obj in ipairs(self._item_list) do
		if inner_obj._view_item_list then
			for k, v in ipairs(inner_obj._view_item_list) do
				
				if topType == 1 then
					topType = 3
					if v[ 1 ].tip then
						v[ 1 ].tip:setString( "正在刷新" )
					end
					if v[ 1 ].arrow then
						v[ 1 ].arrow:setVisible( false )
					end						
					if v[ 1 ].loading then	
						v[ 1 ].loading:setVisible( true )
						if v[ 1 ].loading:getSprite() then
						--v[ 1 ]createSprite(  ret_panel, {x = 20, y = 50, res = 'white_dot.png'})
							a_play(v[ 1 ].loading:getSprite(),{x = 20, y = 50,width = 23.75,	height = 22, count = 4, across = 1, row = 4, res = 'loading_progress.png', time = 0.5},true)
						else
							v[ 1 ].loading = createSprite(  ret_panel, {x = 170, y = 30, res = 'white_dot.png'})
							a_play(v[ 1 ].loading:getSprite(),{x = 20, y = 50,width = 23.75,	height = 22, count = 4, across = 1, row = 4, res = 'loading_progress.png', time = 0.5},true)
						end
					end
					self.touchEnd()
				elseif topType == 2 then
					topType = 3
					self.resetListPos()			
				end
			end
		end
	end	
	if self._touch_began_tag == false then
		clearMoveStatus(self)
		return
	end
	self._touch_began_tag = false
	if not self._move_type then
		clearMoveStatus(self)
		return
	end
	self._touch_end_time = httpMainModule.gettimeofdayCocos2d()
	if not self._im_velocity then
		clearMoveStatus(self)
		return
	end
	if (not self:checkAutoMove()) or (self._im_velocity == 0) then
	--if self._im_velocity == 0 then
		clearMoveStatus(self)
		return
	end
	local mul_factor = 1
	
	if self._im_velocity > 0 then
		mul_factor = -1
	end
		
	if math.abs(self._im_velocity) > max_move_speed then
		self._im_velocity = -mul_factor * max_move_speed
	end
	if math.abs(self._im_velocity) < min_move_speed then
		self._im_velocity = -mul_factor * min_move_speed
	end

	if self._move_type == MOVE_HORIZON then
		aniMovePanel(self, self._im_velocity, mul_factor, self._h_acc)
	else
		aniMovePanel(self, self._im_velocity, mul_factor, self._v_acc)
	end
end

function clsMoveGroup:getPosition()
	return self._RootPanel:getPosition()
end

function clsMoveGroup:clearAllItem()
	for _, item in pairs(self._item_list) do
		item:removeFromParentAndCleanup(true)
	end

	self._item_list = {}
end

local lastMoveY

function clsMoveGroup:__init__(parent, x, y, touch_width, touch_height )
	Super(clsMoveGroup).__init__(self)

	self._RootPanel = CCClipLayer:create()
	self._RootPanel:set_msg_rect(0, 0, touch_width, touch_height)
	makeObjMouseEventHandle(self)

	self._item_list = {}
	self._attr_list = {}

	self._view_width = touch_width
	self._view_height = touch_height

	self._move_cb = nil
	self._obj_stage = STAGE_FREEZE 
	self._move_type = nil 
	self._begin_x = nil
	self._begin_y = nil
	self._last_move_x = nil
	lastMoveY = nil
	self._last_time = nil
	self._touch_end_time = nil
	self._im_velocity = nil
	self._touch_began_tag = false
	self._h_nearest = nil

	self._h_movable = true
	self._v_movable = true
	self._h_acc = 1000
	self._v_acc = 1000
	self._h_breakable = true 
	self._v_breakable = true
	self._v_move_idx = nil

	if parent then
		parent:addChild(self)
		self._RootPanel:setPosition(x, y)
	end
end

function clsMoveGroup:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
	self._RootPanel:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
end

function clsMoveGroup:setTouchEnabled(tag)
	self._RootPanel:setTouchEnabled(tag)
end

function clsMoveGroup:setVisible(tag)
	self._RootPanel:setVisible(tag)
end

function clsMoveGroup:setPosition(x, y)
	self._RootPanel:setPosition(x, y)
end

function clsMoveGroup:setTouchEnabled(tag)
	self._RootPanel:setTouchEnabled(tag)
end

function clsMoveGroup:isMoving()
	return self._obj_stage == STAGE_AUTOMOVE
end

function clsMoveGroup:setMoveVertical()
	self._move_type = MOVE_VERTICAL
end

function clsMoveGroup:onMoveEnd(initVelocity)
end

function clsMoveGroup:getCOObj()
	return self._RootPanel
end

function clsMoveGroup:appendItem(item)
	table.insert(self._item_list, item)
	self._RootPanel:addChild(item:getCOObj())
	table.insert(self._attr_list, {["can_auto_move"] = true})
end

function clsMoveGroup:getMoveStage()
	return self._obj_stage
end
------------------------------------------------------
clsMoveHorizonPageGroup = clsMoveGroup:Inherit()
--slideDirection 滑动方向 不传 可横竖向滑 0 只横向滑
function clsMoveHorizonPageGroup:__init__(parent, x, y, touch_width, touch_height, slideDirection )
	Super(clsMoveHorizonPageGroup).__init__(self, parent, x, y, touch_width, touch_height)
	self._v_movable = false
	self._h_acc = 0 
	self._h_breakable = false 
	self._cur_page = nil
	self._slideDir = slideDirection
end

function clsMoveHorizonPageGroup:setVMovable(tag)
	self._v_movable = tag
end

local autoVelocity = 1500
function clsMoveHorizonPageGroup:checkAutoMove()
	if self._move_type == MOVE_HORIZON then
		if not self._last_time then
			return false
		end
		local delta_time = self._touch_end_time - self._last_time
		if delta_time >= freezeTime then
			local nearest_item = self._item_list[self._h_nearest]
			local x, y = nearest_item:getPosition()
			local item_size = nearest_item:getContentSize()
			local middle_x = (self._view_width - item_size["width"]) / 2
			self._im_velocity = nil 
			if middle_x == x then
				clearMoveStatus(self)
				return false
			end
			if middle_x > x then
				self._im_velocity = autoVelocity 
			else
				self._im_velocity = -autoVelocity
			end
		end
		return true
	else
		return Super(clsMoveHorizonPageGroup).checkAutoMove(self)
	end
end


function clsMoveHorizonPageGroup:appendItem(item)
	Super(clsMoveHorizonPageGroup).appendItem(self, item)
	item:setVisible(false)
end

function clsMoveHorizonPageGroup:getItem(item_idx)
	return self._item_list[item_idx]
end

local function setToNeighbor(grp_obj, item, sign, offset)
	local startx = sign * grp_obj._view_width
	local size = item:getContentSize()
	local curx, cury = item:getPosition()
	item:setPosition(startx + (grp_obj._view_width - size.width)/2 + offset, cury)
end

local function hideAll(grp_obj)
	for _, item in ipairs(grp_obj._item_list) do
		item:setVisible(false)
	end
end

function clsMoveHorizonPageGroup:onHCrossMiddle(idx, cross_delta)
	if not self._item_list[idx]:getVisible() then
		return
	end
	if self._obj_stage == STAGE_FOLLOW and self._cur_page == idx then
		if (idx == 1 and cross_delta > 0) or (idx == table.maxn(self._item_list) and cross_delta < 0) then
			self:selectPage(idx)
			clearMoveStatus(self)
		end
	end
	if self._obj_stage == STAGE_AUTOMOVE or (self._obj_stage == STAGE_FOLLOW and idx ~= self._cur_page) then
		self:selectPage(idx)
		clearMoveStatus(self)
	end
end

function clsMoveHorizonPageGroup:onSelPage()
end

function clsMoveHorizonPageGroup:getPreEle()
	return self._item_list[self._cur_page - 1]
end

function clsMoveHorizonPageGroup:getAfterEle()
	return self._item_list[self._cur_page + 1]
end

function clsMoveHorizonPageGroup:preHMove(direction)
	if self.pMove then
		self.pMove()
	end
	local pre_ele = self:getPreEle()
	local after_ele = self:getAfterEle()
	if pre_ele == nil and direction == 'left' then
		return false
	elseif after_ele == nil and direction == 'right' then
		return false
	end
	if pre_ele then
		pre_ele:setVisible(true)
		setToNeighbor(self, pre_ele, -1, 0)
	end
	if after_ele then
		after_ele:setVisible(true)
		setToNeighbor(self, after_ele, 1, 0)
	end
	
	return true
end

function clsMoveHorizonPageGroup:selectPage(page_idx)
	hideAll(self)
	local item = self._item_list[page_idx]
	item:setVisible(true)
	alignXCenter(item)
	self._cur_page = page_idx
	self._v_move_idx = page_idx
	self:onSelPage()
end

function clsMoveHorizonPageGroup:getCurPage()
	return self._cur_page
end

-------------------------------------------------------------
clsRecycleHMoveGroup = clsMoveHorizonPageGroup:Inherit()

function clsRecycleHMoveGroup:__init__(parent, x, y, touch_width, touch_height)
	Super(clsRecycleHMoveGroup).__init__(self, parent, x, y, touch_width, touch_height)
	self._v_movable = true
end

function clsRecycleHMoveGroup:getPreEle()
	local pre_idx = self._cur_page - 1
	if pre_idx <= 0 then
		pre_idx = table.maxn(self._item_list)
	end
	return self._item_list[pre_idx]
end

function clsRecycleHMoveGroup:onHCrossMiddle(idx, cross_delta)
	if not self._item_list[idx]:getVisible() then
		return
	end
	if idx == self._cur_page then
		if cross_delta > 0 then
			local pre_ele = self:getPreEle()
			pre_ele:setVisible(true)
			setToNeighbor(self, pre_ele, -1, cross_delta)
		elseif cross_delta < 0 then
			local after_ele = self:getAfterEle()
			after_ele:setVisible(true)
			setToNeighbor(self, after_ele, 1, cross_delta)
		end
	end
	if self._obj_stage == STAGE_AUTOMOVE or (self._obj_stage == STAGE_FOLLOW and idx ~= self._cur_page) then
		self:selectPage(idx)
		clearMoveStatus(self)
	end
end

function clsRecycleHMoveGroup:preHMove(direction)
end

function clsRecycleHMoveGroup:getAfterEle()
	local after_idx = self._cur_page + 1
	if after_idx > table.maxn(self._item_list) then
		after_idx = 1
	end
	return self._item_list[after_idx]
end

----------------------------------------------------
clsAttachMoveGroup = clsMoveGroup:Inherit()

function clsAttachMoveGroup:__init__(parent, x, y)
	Super(clsAttachMoveGroup).__init__(self, parent, x, y)
end

function clsAttachMoveGroup:onTouchBegan(x, y)
	self._begin_obj_x, self._begin_obj_y = self:getCOObj():getPosition()
	self._begin_touch_x, self._begin_touch_y = x, y
end

function clsAttachMoveGroup:onTouchMove(x, y)
	local inc_x = x - self._begin_touch_x
	local inc_y = y - self._begin_touch_y
	if (self._move_type == MOVE_HORIZON) then
		self:getCOObj():setPosition(self._begin_obj_x + inc_x, self._begin_obj_y)
	else
		self:getCOObj():setPosition(self._begin_obj_x, self._begin_obj_y + inc_y)
	end
end

function clsAttachMoveGroup:onTouchEnd(x, y)
end
--------------------------------------------------------
clsNode = clsObject:Inherit()

function clsNode:__init__(parent, x, y,nZOrder)
	Super(clsNode).__init__(self)
	self._RootNode = CCNode:create()
	if parent then	
		parent:addChild(self,nZOrder)								
		self._RootNode:setPosition(x, y)
	end
end

function clsNode:getCOObj()
	return self._RootNode
end

function clsNode:removeFromParentAndCleanup(tag)
    if self._RootNode then
	self._RootNode:removeFromParentAndCleanup(tag)
	end
end
--[[
function clsNode:addChild(child)
	self._RootNode:addChild(child:getCOObj())
	table.insert(self._son_list, child)
end	
]]
function clsNode:addChild(child,nZOrder)
	if nZOrder then	
		self._RootNode:addChild(child:getCOObj(),nZOrder,tonumber(nZOrder))		
	else
		self._RootNode:addChild(child:getCOObj())	
	end
	table.insert(self._son_list, child)
end	

function clsNode:setAnchorPoint(x, y)
	self._RootNode:setAnchorPoint(CCPointMake(x, y))
end

function clsNode:setPosition(x, y)
	self._RootNode:setPosition(x, y)
end

function clsNode:isVisible()
	return self._RootNode:isVisible()
end

function clsNode:getPosition()
	return self._RootNode:getPosition()
end

function clsNode:getContentSize()
	return { ["width"] = 0, ["height"] = 0 }
end

function clsNode:setRotation(rot)
	self._RootNode:setRotation(rot)
end

function clsNode:setVisible(tag)
	self._RootNode:setVisible(tag)
end

function clsNode:getVisible()
	return self._RootNode:isVisible()
end

function clsNode:setTag(tag)
	self._RootNode:setTag(tag)
end

function clsNode:removeAllChildrenWithCleanup(tag)
	self._RootNode:removeAllChildrenWithCleanup(tag)
end

-------------------------------------------------------
clsClipLayer = clsObject:Inherit()

function clsClipLayer:__init__(parent, x, y)
	Super(clsClipLayer).__init__(self)
	self._RootLayer = CCClipLayer:create()
	if parent then
		parent:addChild(self)
		self._RootLayer:setPosition(x, y)
	end
	makeObjMouseEventHandle(self)
end

function clsClipLayer:set_msg_rect(x, y, width, height)
	self._RootLayer:set_msg_rect(x, y, width, height)
end

function clsClipLayer:setPosition(x, y)
	self._RootLayer:setPosition(x, y)
end

function clsClipLayer:convertToWorldSpace(x, y)
	local point = self._RootLayer:convertToWorldSpace(CCPointMake(x, y))
	return point.x, point.y
end

function clsClipLayer:addChild(child)
	self._RootLayer:addChild(child:getCOObj())
	table.insert(self._son_list, child)
end

function clsClipLayer:getCOObj()
	return self._RootLayer
end

function clsClipLayer:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
	self._RootLayer:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
end

function clsClipLayer:setTouchEnabled(tag)
	self._RootLayer:setTouchEnabled(tag)
end

function clsClipLayer:onTouchBegan(x, y)
end

function clsClipLayer:onTouchMove(x, y)
end

function clsClipLayer:onTouchEnd(x, y)
end

function clsClipLayer:getVisible()
	return self._RootLayer:isVisible()
end

function clsClipLayer:setVisible(tag)
	self._RootLayer:setVisible(tag)
end
-----------------------------------------------
clsLayer = clsObject:Inherit()

function clsLayer:__init__(parent, x, y)
	Super(clsLayer).__init__(self)
	self._RootLayer = CCLayer:create()
	if parent then
		parent:addChild(self)
		self._RootLayer:setPosition(x, y)
	end
	makeObjMouseEventHandle(self)
	--makeObjMouseEventHandle2(self)
end

function clsLayer:setPosition(x, y)
	self._RootLayer:setPosition(x, y)
end

function clsLayer:addChild(child)
	self._RootLayer:addChild(child:getCOObj())
	table.insert(self._son_list, child)
end

function clsLayer:getCOObj()
	return self._RootLayer
end

function clsLayer:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
	self._RootLayer:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
end

function clsLayer:setTouchEnabled(tag)
	self._RootLayer:setTouchEnabled(tag)
end

function clsLayer:onTouchBegan(x, y)
end

function clsLayer:onTouchMove(x, y)
end

function clsLayer:onTouchEnd(x, y)
end

function clsLayer:setVisible(tag)
	self._RootLayer:setVisible(tag)
end
---------------------------------------------------
clsScaleSprite = clsObject:Inherit()
function clsScaleSprite:__init__(parent, x, y, filename, full, rect)
	-- 20130719，待认证
	filename = getResFilename( filename )
	
	Super(clsScaleSprite).__init__(self)
	self._RootNode = CCNode:create()
	if parent then
		parent:addChild(self)
		self._RootNode:setPosition(x, y)
	end
	if filename then
		self._RootSprite = CCScale9Sprite:create(filename, full, rect)
		self._RootSprite:setPosition(0, 0)
		self._RootNode:addChild(self._RootSprite)
	end
end

function clsScaleSprite:setSpriteContentSize(width, height)
	self._RootSprite:setContentSize(CCSizeMake(width, height))
end

function clsScaleSprite:setPosition(x, y)
	self._RootNode:setPosition(x, y)
end

function clsScaleSprite:getPosition()
	return self._RootNode:getPosition()
end

function clsScaleSprite:setAnchorPoint(x, y)
	self._RootSprite:setAnchorPoint(CCPointMake(x, y))
end

function clsScaleSprite:getContentSize()
	return self._RootSprite:getContentSize()
end

function clsScaleSprite:getCOObj()
	return self._RootNode
end

function clsScaleSprite:setVisible(tag)
	self._RootNode:setVisible(tag)
end

function clsScaleSprite:removeFromParentAndCleanup(tag)
	self:Destroy()
	self._RootNode:removeFromParentAndCleanup(tag)
end

-------------------------------------------------
clsSprite = clsObject:Inherit()

function clsSprite:__init__( parent, x, y, filename, cb, nZOrder )
--	filename = getResFilename( filename )
	
	Super(clsSprite).__init__(self)
	self._RootNode = CCNode:create()
	if parent then
		if not nZOrder then
			parent:addChild(self)
		else
			parent:addChild(self, nZOrder)
		end
		self._RootNode:setPosition(x, y)
	end
	if filename then
		-- by dda, 20130722
		--[[self._RootSprite = CCSprite:create(filename)
		self._RootSprite:setPosition(0, 0)
		self._RootNode:addChild(self._RootSprite)--]]
		self:setImage( filename, cb )
	end
end

-- dda 20130722加cb参数
function clsSprite:setImage( filename, cb,photo )
	-- 完整文件名，不带后辍的文件名
	local full_filename, name = nil, nil
	--要请求的url
	local url = nil
	-- 要保存的文件是属于资源文件还是缓存？
	local cache = nil
	
	local function complete( filename )		
		if self._RootSprite then
			self._RootSprite:removeFromParentAndCleanup(true)
		end
		self._RootSprite = CCSprite:create(filename)
		assert(self._RootSprite)
		self._RootSprite:setPosition(0, 0)
		self._RootNode:addChild(self._RootSprite)
		
		if self._width then
			self:setContentSize( self._width, self._height )
		end
		
		if cb then
			cb( self )
		end
	end
	local function down( data )
		--[[
		if cache then
			if isJPGData(data) then
				full_filename = name .. 'jpg'
			elseif isPNGData(data) then
				full_filename = name .. 'png'
			else
				return
			end
		else
			full_filename = name
		end
		
--		print( "name:",name )
		
		if tonumber(data) == 408 then
			return
		end
		WriteByte( full_filename, data )
		]]
		if self._RootNode:retainCount() > 1 then		
			--complete( full_filename )	
			complete(data)					
		end
		self._RootNode:release()
	end
	
	local function findCache( name )
		local list = { 'jpg', 'png' }
		
		for _,v in pairs( list ) do
			local full_filename = name .. v
			local file,err = io.open( full_filename )
			if file then
				file:close()
				return full_filename
			end
		end
		
		return nil
	end
	
	local function checkFileExists( filename )
		local file,err = io.open( filename )
		if file then
			file:close()
		end
		return file
	end
	
	--第三方登录融错
	if not filename then
		return
	else
		if string.lower( string.sub( filename, 1, 7 ) ) == 'http://' then
			url = filename
			name = GetCachePath() .. GetMD5( url ) .. '.'
			full_filename = findCache( name )
			cache = true
		else
			if not photo then
				full_filename = getResFilename( filename )
			else
				full_filename = filename
			end
		
			if not checkFileExists( full_filename ) then
				url = GLOBAL.interface .. 'res/' .. filename
				name = ""			
				local t = split(full_filename,'.')			
				for count = 1, #t - 1 do
					name = name .. tostring(t[count])
					name = name .. "."
				end				
				full_filename = nil
				cache = false
			end
		end							
	end

	
	if full_filename then
		complete( full_filename )
	else
		self._RootNode:retain()
		HTTP_REQUEST.http_download(url,name,down)
		--HTTP_REQUEST.http_request( url, down )
	end
end
--新增接口
function clsSprite:setSprite(sprite)
	if self._RootSprite then
		self._RootSprite:removeFromParentAndCleanup(true)
	end
	self._RootSprite = sprite
	self._RootSprite:setPosition(0, 0)
	self._RootNode:addChild(self._RootSprite)
end

function clsSprite:setPosition(x, y)
	self._RootNode:setPosition(x, y)
end

function clsSprite:getPosition()
	return self._RootNode:getPosition()
end

function clsSprite:setAnchorPoint(x, y)
	-- dda 于20130708加if
	if self._RootSprite then
		self._RootSprite:setAnchorPoint(CCPointMake(x, y))
	end
end

function clsSprite:setScaleX(x)
	self._RootSprite._scalex = x
	self._RootSprite:setScaleX(x)
end

function clsSprite:setScaleY(y)
	--加此判断是考虑到远程更新资源速度过慢而导致对象没有初始化完成，解决办法应该是所有相应方法在收到对象完成初始化消息之后再调用
	if self._RootSprite then
		self._RootSprite._scaley = y
		self._RootSprite:setScaleY(y)		
	end
end

function clsSprite:setScale(scale)
	self._RootSprite._scalex = scale
	self._RootSprite._scaley = scale
	self._RootSprite:setScale(scale)
end

function clsSprite:setContentSize(width, height)
	self._width = width
	self._height = height
	
	if self._RootSprite then
		local size = self._RootSprite:getContentSize()
		
		self._RootSprite:setScaleX( width / size.width )
		self._RootSprite:setScaleY( height / size.height )
	end
end

function clsSprite:getContentSize()
	if self._width then
		return { width = self._width, height = self._height, }
	else
		if self._RootSprite then
			return self._RootSprite:getContentSize()
		else
			return { width = 0, height = 0, }
		end
	end
	--[[if not self._RootSprite then
		return { width = 0, height = 0, }
	end
	
	local size = self._RootSprite:getContentSize()

	if self._RootSprite._scalex then
		size.width = size.width * self._RootSprite._scalex
	end

	if self._RootSprite._scaley then
		size.height = size.height * self._RootSprite._scaley
	end
	
	return size--]]
end

function clsSprite:getCOObj()
	return self._RootNode
end

function clsSprite:getSprite()
	return self._RootSprite
end

function clsSprite:release()
	self._RootNode:release()
end

function clsSprite:setRotation(rot)
	self._RootNode:setRotation(rot)
end

function clsSprite:setVisible(tag)
	self._RootNode:setVisible(tag)
end

function clsSprite:addChild(child)
	-- by dda, 20130723
	self._RootNode:addChild(child:getCOObj())
end

function clsSprite:removeFromParentAndCleanup(tag)
	self:Destroy()
	self._RootNode:removeFromParentAndCleanup(tag)
	self._RootNode = nil
end

----------------------------------------------------------
clsRotSprite = clsObject:Inherit()
function clsRotSprite:__init__(parent, x, y, filename, txt_list, init_rot)
	Super(clsRotSprite).__init__(self)
	self._RootNode = CCNode:create()
	if parent then
		parent:addChild(self)
		self._RootNode:setPosition(x, y)
	end
	filename = getResFilename( filename )
	self._RotSprite = CCSprite:create(filename)
	self._RotSprite:setAnchorPoint(CCPointMake(0.5, 0.5))
	self._RotSprite:setPosition(0, 0)
	self._RootNode:addChild(self._RotSprite)

	self._txt_list = txt_list
	if self._txt_list then
		self._txt_obj_list = {}
		local txt_cnt = table.maxn(txt_list)
		local rot_step = 360 / txt_cnt
		local temp_rot = init_rot
		for _, txt in ipairs(txt_list) do
			local txt_obj = CCLabelTTF:create(txt, GLOBAL_FONT, 21)
			txt_obj:setAnchorPoint(CCPointMake(0, 0.5))
			txt_obj:setRotation(temp_rot)
			txt_obj:setColor(ccc3(0, 0, 0))
			self._RootNode:addChild(txt_obj)
			temp_rot = temp_rot - rot_step
			table.insert(self._txt_obj_list, txt_obj)
		end
	end
end

function clsRotSprite:getCOObj()
	return self._RootNode
end

function clsRotSprite:setRotation(rot) -- 旋转度数
	self._RootNode:setRotation(rot)
end

function clsRotSprite:getRotation()
	return self._RootNode:getRotation()
end


function autoRot(rot_obj, init_rot_speed, max_rot_speed, add_rot_acc, dec_rot_acc, need_rot, end_cb)
	local rot_cb = nil

	local tmp_velocity = init_rot_speed 
	local stage = 0
	local sum_rot = 0
	local function do_rot()
		local inc_delta = tmp_velocity * time_interval

		local cur_rot = rot_obj:getRotation()
		cur_rot = cur_rot + inc_delta
		sum_rot = sum_rot + inc_delta
		if sum_rot > need_rot then
			sum_rot = need_rot
			cur_rot = need_rot
		end
		rot_obj:setRotation(cur_rot)
		if sum_rot >= need_rot then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(rot_cb)
			rot_cb = nil
			end_cb()
		end

		if stage == 0 then
			tmp_velocity = tmp_velocity + add_rot_acc * time_interval
		else
			tmp_velocity = tmp_velocity + dec_rot_acc * time_interval
		end
		if tmp_velocity >= max_rot_speed then
			stage = 1
		end
		if ((tmp_velocity * init_rot_speed) <= 0) then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(rot_cb)
			rot_cb = nil
			end_cb()
		end
	end
	if math.abs(init_rot_speed) > 0 then
		rot_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(do_rot, time_interval, false)
	end
	
	return rot_cb
end

function isPNGURL(url)
	local url_len = string.len(url)
	if string.sub(url, url_len - 2) == "png" then
		return true
	else
		return false
	end
end

function isJPGData(data)
	local first_byte = string.byte(string.sub(data, 1, 1))
	local sec_byte = string.byte(string.sub(data, 2, 2))
	return (first_byte == 255 and sec_byte == 216)
end

function isPNGData(data)
	local first_byte = string.byte(string.sub(data, 1, 1))
	local sec_byte = string.byte(string.sub(data, 2, 2))
	return (first_byte == 137 and sec_byte == 80)
end

------------------------------------------------------
--[[clsHTTPSprite = clsObject:Inherit()

local http_sprite_list = {
}

local refresh_cb = nil
local function doRefresh()
	for _, sprite_obj in pairs(http_sprite_list) do
		sprite_obj:refreshPic()
	end
	CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(refresh_cb)
	refresh_cb = nil
end

function refreshAllHttpSprite()
	refresh_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(doRefresh, 0.1, false)
end

-- 137 80

local function genSprite(obj, data,url)
	local strFilename = GetMD5(url)	
	if isPNGData(data) then
		strFilename = strFilename..'.png'
	elseif isJPGData(data) then
		strFilename = strFilename..'.jpg'
	end
		
	local filename = GetCachePath()..strFilename
	local file,err=io.open(filename)
	if not file then
		WriteByte(filename,data)
	else	
		file:close()	
	end		
	
	obj._Sprite = CCSprite:create(filename)		
end

local function createHTTPSpriteByData(obj, data,url)
	genSprite(obj, data,url)
	
	-- by dda, 20130630，改为居中
	--obj._Sprite:setAnchorPoint(CCPointMake(0, 1))
	
	obj._Sprite:setPosition(0, 0)
	obj._RootNode:addChild(obj._Sprite)
	-- 20130703，因setContenteSize中已经执行
	--if obj._scale_width and obj._scale_height then
	--	local pic_size = obj._Sprite:getContentSize()
	--	obj._RootNode:setScaleX(obj._scale_width / pic_size.width)
	--	obj._RootNode:setScaleY(obj._scale_height / pic_size.height)
	--end
	-- by dda, 20130628
	--if obj._cb then obj._cb( obj._Sprite ) end
	if obj._cb then obj._cb( obj ) end
end

local function setHTTPSprite(obj, url)
	local function pic_cb(data)
		if not obj._RootNode:isValid() then
			return
		end

		if obj:isDestroy() then
			return
		end

		createHTTPSpriteByData(obj, data,url)
		HTTPPicCache[url] = data
	end

	obj._url = url

	if not HTTPPicCache[url] then
		HTTP_REQUEST.http_request(url, pic_cb)
	else
		local pic_data = HTTPPicCache[url]
		createHTTPSpriteByData(obj, pic_data,url)		
	end
end

function clsHTTPSprite:__init__(parent, x, y, url, cb)
	Super(clsHTTPSprite).__init__(self)

	self._RootNode = CCNode:create()
	if parent then
		parent:addChild(self)
		self._RootNode:setPosition(x, y)
	end

	if not url then
		return
	end
	
	if cb then
		self._cb = cb
	else
		self._cb = nil
	end

	self._scale_width = nil 
	self._scale_height = nil 
	self._is_local = false

--	table.insert(http_sprite_list, self) -- ??? 其他接口也要加
	
	setHTTPSprite(self, url)
end

function clsHTTPSprite:refreshPic()
	if self._is_local then
		return
	end

	if self._Sprite then
		self._Sprite:removeFromParentAndCleanup(true)
		self._Sprite = nil
	end
	setHTTPSprite(self, self._url)
end

function clsHTTPSprite:addChild(child)
	self._RootNode:addChild(child:getCOObj())
end

function clsHTTPSprite:setAnchorPoint(x, y)
	-- by dda, 20130703
	if self._Sprite then
		self._Sprite:setAnchorPoint(CCPointMake(x, y))
	end
end

function clsHTTPSprite:setLocal(tag)
	self._is_local = tag
end

function clsHTTPSprite:getCOObj()
	return self._RootNode
end

function clsHTTPSprite:getSprite()
	return self._Sprite
end

function clsHTTPSprite:setScalePixel(width, height)
	self._scale_width = width
	self._scale_height = height
	if self._Sprite then
		local pic_size = self._Sprite:getContentSize()
		self._RootNode:setScaleX(width / pic_size.width)
		self._RootNode:setScaleY(height / pic_size.height)
	end
end

function clsHTTPSprite:getContentSize()
	-- by dda, 20130628
	if self._Sprite then
		return self._Sprite:getContentSize()
	else
		return { width = 0, height = 0, }
	end
end

function clsHTTPSprite:setContentSize(width, height)
	-- by dda, 20130629
	--self:setScalePixel(width, height)
	self._scale_width = width
	self._scale_height = height
	if self._Sprite then
		local pic_size = self._Sprite:getContentSize()
		--print(pic_size.width,  pic_size.height)
		self._Sprite:setScaleX(width / pic_size.width)
		self._Sprite:setScaleY(height / pic_size.height)
	end
end

function clsHTTPSprite:setScaleX(sx)
	self._RootNode:setScaleX(sx)
end

function clsHTTPSprite:setScaleY(sy)
	self._RootNode:setScaleY(sy)
end

function clsHTTPSprite:setScale(scale)
	self._RootNode:setScale(scale)
end

function clsHTTPSprite:setVisible(tag)
	self._RootNode:setVisible(tag)
end

function clsHTTPSprite:getVisible()
	return self._RootNode:isVisible()
end

function clsHTTPSprite:setPosition(x, y)
	local point = self._RootNode:convertToWorldSpace(CCPointMake(x, y))
	--print( point.x, point.y	)
	--print(debug.traceback	)
	
	
	self._RootNode:setPosition(x, y)
end

function clsHTTPSprite:getPosition()
	return self._RootNode:getPosition()
end

function clsHTTPSprite:setImage(url)
	if self._url == url then
		return
	end
	if self._Sprite then
		self._Sprite:removeFromParentAndCleanup(true)
	end
	setHTTPSprite(self, url)
end

function clsHTTPSprite:removeFromParentAndCleanup(tag)
	self._RootNode:removeFromParentAndCleanup(tag)
end
]]

---------------------------------------------------------------
local cur_active_edit = nil
local function activeEdit(obj)
	if cur_active_edit and (not cur_active_edit:isDestroy()) then
		cur_active_edit:active(false)
	end

	obj:active(true)
	cur_active_edit = obj
end

function clearActEdit()
	cur_active_edit = nil
end

function getByteCount(str, char_cnt)
	local ByteCount = string.len(str)
	local ByteIndex = 1
	local tmp_char_cnt = 0
	local tmp_byte_cnt = 0
	while (ByteIndex <= ByteCount) do
		local TempSubStr = string.sub(str, ByteIndex, ByteIndex)
		if isAChar(TempSubStr) then
			tmp_char_cnt = tmp_char_cnt + 1
			if tmp_char_cnt > char_cnt then
				return tmp_byte_cnt
			end
		end
		tmp_byte_cnt = tmp_byte_cnt + 1
		ByteIndex = ByteIndex + 1
	end

	return tmp_byte_cnt
end

local function blinkCursor(editObj)
	if editObj:isDestroy() then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(editObj._BlinkHandle)
		return
	end

	local visible_tag = editObj._CursorImg:isVisible()
	visible_tag = not visible_tag
	editObj._CursorImg:setVisible(visible_tag)
end

local function setCursorByInsertIdx(editObj, strLen, insertIndex)
	local cur_x, cur_y = editObj._CursorImg:getPosition()
	if editObj._insert_idx <= 0 then
		cur_x = 0
		cur_y = 0
	else
		if strLen then
			for i = insertIndex, insertIndex + strLen - 1 do
				local char_info = editObj._OrgLabel:getCharInfoByIdx(i)
				if not char_info then
					return
				end
				cur_x = char_info["x"] + char_info["char_width"]
				--CCMessageBox(tostring(cur_x),'')
				--print(tostring(char_info["char_width"]))
				cur_y = char_info["y"]
				if i > insertIndex then
					editObj._insert_idx = editObj._insert_idx + 1
				end
			end				
		else
			local char_info = editObj._OrgLabel:getCharInfoByIdx(editObj._insert_idx)
			if not char_info then
				return
			end
			cur_x = char_info["x"] + char_info["char_width"]
			cur_y = char_info["y"]
		end
	end				
	
	local view_width, view_height = editObj._OrgLabel:getViewSize()
	editObj._CursorImg:setPosition(cur_x, view_height - cur_y)
end

local function doInsertStr(editObj, str)
	local text = editObj._OrgLabel:getText()
	--if limitType and limitType ~= 1 then	
		local pre_str = "" 
		if editObj._insert_idx > 0 then
			pre_str = string.sub(text, 1, getByteCount(text, editObj._insert_idx))
		end
		local tail_str = ""
		local char_count = editObj._RootEdit:getCharCount()
		if editObj._insert_idx < char_count then
			if editObj._insert_idx <= 0 then
				tail_str = string.sub(text, 1)
			else
				tail_str = string.sub(text, getByteCount(text, editObj._insert_idx)+1)			
			end
		end
		text = pre_str .. str .. tail_str		

	editObj._OrgLabel:setString(text)
	editObj._RootEdit:setString(text)
end

local function findInsertIdxByPos(editObj, x, y)
	local view_width, view_height = editObj._OrgLabel:getViewSize()
	y = view_width - y
	local ret_idx = editObj._RootEdit:getCharCount() 
	local tmp_idx = 1
	for row_idx, row_info in ipairs(editObj._OrgLabel:getPosInfo()) do
		for column, char_info in ipairs(row_info) do
			if (x < char_info["x"] + char_info["char_width"]) and (y < char_info["y"] + char_info["char_height"]) then
				ret_idx = tmp_idx 
				return tmp_idx - 1
			end
			if tmp_idx == ret_idx then
				return ret_idx
			end
			tmp_idx = tmp_idx + 1
		end
	end
	return ret_idx
end

local function isEnter(str)
	return string.len(str) == 1 and string.byte(str) == 10
end

local function onTextInput(editObj, str, limitType)
	if isEnter(str) and (not editObj._canEnter) then
		editObj:active(false)
		return
	end

	--if editObj._max_char and editObj._RootEdit:getCharCount() >= editObj._max_char then
	if editObj._max_char then
		local text = editObj._OrgLabel:getText()
		if limitType and limitType == 1 then
			--print( string.len(tostring(text)) )
			if string.len(tostring(text)) > 12 then
				return
			end
		end
		local n = 0
		local i = 1
		while(i <= string.len(tostring(text))) do 
		--for i = 1,string.len(tostring(text)) do
			local b = text:byte(i)				
			if b >= 128 then
			    i = i + 3
			else
				i = i + 1
			end
			n = n + 1			
		end	
		if n >= editObj._max_char then
			return
		end		
	end

	doInsertStr(editObj, str)

	editObj._insert_idx = editObj._insert_idx + 1
	
	stringLen = string.len(str)
	
	local strLen = 0
	local i = 1
	while(i <= stringLen) do
		
		local b = str:byte(i)				
		if b > 128 then
			i = i + 3
		else			
			i = i + 1
		end			
		strLen = strLen + 1
	end
	
	--CCMessageBox("strLen:" .. tostring(strLen),'')

	setCursorByInsertIdx(editObj, strLen, editObj._insert_idx)
end

local function doDeleteChar(editObj)
	local text = editObj._OrgLabel._text
	local pre_str = ""
	if editObj._insert_idx > 1 then
		pre_str = string.sub(text, 1, getByteCount(text, editObj._insert_idx - 1))
	end
	local tail_str = ""
	local char_count = editObj._RootEdit:getCharCount()
	if editObj._insert_idx < char_count then
		tail_str = string.sub(text, getByteCount(text, editObj._insert_idx)+1)
	end
	text = pre_str .. tail_str
	editObj._OrgLabel:setString(text)
	editObj._RootEdit:setString(text)
end

local function onDeleteText(editObj)
	if editObj._insert_idx <= 0 then
		return
	end

	doDeleteChar(editObj)

	editObj._insert_idx = editObj._insert_idx - 1

	setCursorByInsertIdx(editObj)
end

local function checkLetter( char )
	local charByte = string.byte( char )
	if charByte >= 65 then
		if charByte <= 90 then
			return true
		end
	end
	if charByte >= 97 then
		if charByte <= 122 then
			return true
		end
	end
	return false
end

local function checkLimitChar( char )
	local charByte = string.byte( char )
	--print( charByte )
	if charByte >= 48 then
		if charByte <= 57 then
			return true
		end
	end
	
	if checkLetter( char ) == true then
		return true
	end
	
	return false
end

clsEdit = clsObject:Inherit()
local charCount = 0
local stepCount = 0
local step = {}
--limitType 限制类型
function clsEdit:__init__(parent, x, y, font, font_size, dis_width, dis_height, limitType ) -- 左上角点对齐
	local function onInput(str)
		if limitType and limitType == 2 then
			local sign = checkLimitChar( str )
			if sign == false then		
				return
			end
		end	
		
		if limitType and limitType == 1 then
			local charByte = string.byte( str )
			--print( charByte )
			if charByte <= 127 then
				if checkLetter( str ) == false then
					return
				end
			end
		end
		
		onTextInput(self, str, limitType )								
	end

	local function onDelete()
		onDeleteText(self)		
	end

	Super(clsEdit).__init__(self)
	self._RootClipPanel = CCClipLayer:create()
	self._RootEdit = CCTextFieldTTF:textFieldWithPlaceHolder("", CCSizeMake(0, 0), kCCTextAlignmentLeft, font, font_size)
	self._RootEdit:setPosition(0, 0)
	self._RootEdit:setVisible(false)
	self._RootClipPanel:addChild(self._RootEdit)
	self._OrgLabel = clsMultiLabel:New(nil, 0, 0, "", font, font_size, dis_width, dis_height)
	self._OrgLabel:setPosition(0, dis_height)
	self._RootClipPanel:addChild(self._OrgLabel:getCOObj())
	self._CursorImg = CCSprite:create( getResFilename( "cursor.png" ) )
	self._CursorImg:setPosition(0, dis_height)
	self._CursorImg:setAnchorPoint(CCPointMake(0, 1))
	self._CursorImg:setVisible(false)
	self._RootClipPanel:addChild(self._CursorImg)
	self._BlinkInterval = 1 / 2 
	self._RootClipPanel:set_msg_rect(0, 0, dis_width, dis_height)

	self._insert_idx = 0
	self._max_char = nil 
	self._canEnter = false 

	if parent then
		parent:addChild(self)
		self._RootClipPanel:setPosition(x, y-dis_height)
	end

	makeObjMouseEventHandle(self)
	self._RootEdit:registerScriptInputHandler(onInput, onDelete)
	self._RootEdit:setVisible(false)
end

function clsEdit:setTextColor(r, g, b)
	self._OrgLabel:setTextColor(r, g, b)
end	

function clsEdit:setPasswordChar(char)
	self._OrgLabel:setPasswordChar(char)
	self.ownPassword = true
	
end

function clsEdit:setString(str)
	if not str or type(str) ~= "string"  then
		return
	else
		self.defaultValue = str
		self._OrgLabel:setString(str)
	end
	
end

function clsEdit:getText()
	if self._OrgLabel then
		return self._OrgLabel:getText()
	end
end

function clsEdit:active(tag)
	local function doBlink()	
		blinkCursor(self)
	end

	if tag == true then
		--[[if self._BlinkHandle then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(self._BlinkHandle)
			self._BlinkHandle = nil 
		end--]]
		
		if TIMER and TIMER['_BlinkHandle'] then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(TIMER['_BlinkHandle'])
		end

		self._insert_idx = 0 
		setCursorByInsertIdx(self)
		self._RootEdit:attachWithIME()
		--self._BlinkHandle = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(doBlink, self._BlinkInterval, false)
		TIMER['_BlinkHandle'] = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(doBlink, self._BlinkInterval, false)
	else
		--[[if self._BlinkHandle then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(self._BlinkHandle)
			self._RootEdit:detachWithIME()
		end--]]
		if TIMER and TIMER['_BlinkHandle'] then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(TIMER['_BlinkHandle'])
			TIMER['_BlinkHandle'] = nil			
		end
		self._RootEdit:detachWithIME()
	end
	self._CursorImg:setVisible(tag)	
	
	if not tag then
		if self._OrgLabel:getText() == "" then
			if self.ownPassword then
				self._OrgLabel:setPasswordChar( nil )
			end
			self._OrgLabel:setString( self.defaultValue )
		end
	else
		if self.ownPassword then
			if self._OrgLabel:getText() ~= "" then
				self._OrgLabel:setPasswordChar( "*" )
			end
		end
	end
	
end

function clsEdit:clearCursor()
	if TIMER and TIMER['_BlinkHandle'] then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(TIMER['_BlinkHandle'])
	end
end

function clsEdit:onTouchBegan( x, y ) 
	x = x - 40
	activeEdit(self)

	local insert_idx = findInsertIdxByPos(self, x, y) -- pos bug ???	
	self._insert_idx = insert_idx
	setCursorByInsertIdx(self) -- pos bug ???
	
	
	if self._OrgLabel then
		if self._OrgLabel:getText() == self.defaultValue then
			self._insert_idx = 0
			self._OrgLabel:setString("")
			setCursorByInsertIdx(self)
		end
	end
	--self._RootEdit:setString("")
	
	
end

function clsEdit:onTouchMove(x, y)
end

---------------------------------------------------------------------
--增加光标还原到初始位置
--add by xr 2013-06-18
function clsEdit:clean()
	self._insert_idx = 0
	self._OrgLabel:setString("")
	self._RootEdit:setString("")
	setCursorByInsertIdx(self)
end
---------------------------------------------------------------------

---------------------------------------------------------------------
--增加输入字数限制,避免出现多行BUG
--add by xr 2013-06-18
function clsEdit:setMaxInputChar(nMax)
    self._max_char = nMax
end
---------------------------------------------------------------------

function clsEdit:onTouchEnd(x, y)
end

function clsEdit:setPosition(x, y)
	self._RootClipPanel:setPosition(x, y)
end

function clsEdit:setTouchEnabled(tag)
	self._RootClipPanel:setTouchEnabled(tag)
end

function clsEdit:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
	self._RootClipPanel:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
end

function clsEdit:getCOObj()
	return self._RootClipPanel
end

function clsEdit:attachWithIME()
	self._RootEdit:attachWithIME()
end
---------------------------------------------------------------
clsButton = clsObject:Inherit()

function clsButton:__init__(parent, x, y, normal_pic, click_pic)
	Super(clsButton).__init__(self)
	if not click_pic then
		click_pic = normal_pic
	end
	
	normal_pic = getResFilename( normal_pic )
	click_pic = getResFilename( click_pic )

	self._normal_sprite = CCSprite:create(normal_pic)
	self._normal_sprite:setAnchorPoint(CCPointMake(0, 0))
	self._normal_sprite:setPosition(0, 0)
	self._click_sprite = CCSprite:create(click_pic)
	self._click_sprite:setAnchorPoint(CCPointMake(0, 0))
	self._click_sprite:setPosition(0, 0)
	local pic_size = self._normal_sprite:getContentSize()
	self._txt_label = CCLabelTTF:create("", GLOBAL_FONT, 16)
	self._txt_label:setPosition(pic_size.width/2, pic_size.height/2)
	local size = self._normal_sprite:getContentSize()
	self._RootMsgPanel = CCClipLayer:create()	
	self._RootMsgPanel:set_msg_rect(0, 0, size.width, size.height)
	self._RootMsgPanel:addChild(self._normal_sprite)
	self._RootMsgPanel:addChild(self._click_sprite)
	self._RootMsgPanel:addChild(self._txt_label)

	if parent then
		parent:addChild(self)
		self._RootMsgPanel:setPosition(x, y)
	end

	self:setNormalStatus()

	makeObjMouseEventHandle(self)
end	

function clsButton:setVisible(tag)
	self._RootMsgPanel:setVisible(tag)
end

function clsButton:getContentSize()
	-- by dda
	--return self._normal_sprite:getContentSize()
	local size = self._normal_sprite:getContentSize()
	return { width = size.width * (self._normal_sprite._scalex or 1),
			 height = size.height * (self._normal_sprite._scaley or 1),
		}
end

function clsButton:setContentSize( width, height )
	-- by dda
	local size = self._normal_sprite:getContentSize()
	
	self._normal_sprite._scalex = width / size.width
	self._normal_sprite._scaley = height / size.height
	
	self:setSpriteScaleX( width / size.width )
	self:setSpriteScaleY( height / size.height )
end

function clsButton:setOpacity(op)
	self._normal_sprite:setOpacity(op)
	self._click_sprite:setOpacity(op)
end

function clsButton:setSpriteScaleX(x)
	self._normal_sprite:setScaleX(x)
	self._click_sprite:setScaleX(x)
	local size = self._normal_sprite:getContentSize()
	local sy = self._normal_sprite:getScaleY()
	self._RootMsgPanel:set_msg_rect(0, 0, size.width*x, size.height*sy)
	self._txt_label:setPosition(size.width*x/2, size.height*sy/2)
end

function clsButton:setSpriteScaleY(y)
	self._normal_sprite:setScaleY(y)
	self._click_sprite:setScaleY(y)
	local size = self._normal_sprite:getContentSize()
	local sx = self._normal_sprite:getScaleX()
	self._RootMsgPanel:set_msg_rect(0, 0, size.width*sx, size.height*y)
	self._txt_label:setPosition(size.width*sx/2, size.height*y/2)
end

function clsButton:getPosition()
	return self._RootMsgPanel:getPosition()
end

function clsButton:setPosition(x, y)
	self._RootMsgPanel:setPosition(x, y)
end

function clsButton:setString(str)
	if not str or type(str) ~= 'string' then
		return
	else
		self._txt_label:setString(str)
	end
	
end

function clsButton:setAnchorPoint(x, y)
	self._normal_sprite:setAnchorPoint(CCPointMake(x, y))
	self._click_sprite:setAnchorPoint(CCPointMake(x, y))
end

function clsButton:setTextY(yvalue)
	local x, y = self._txt_label:getPosition()
	self._txt_label:setPosition(x, yvalue)
end

function clsButton:setTextColor(r, g, b)
	self._txt_label:setColor(ccc3(r, g, b))
end

function clsButton:setNormalStatus()
	self._normal_sprite:setVisible(true)
	self._click_sprite:setVisible(false)
end

function clsButton:setClickStatus()
	self._normal_sprite:setVisible(false)
	self._click_sprite:setVisible(true)
end

function clsButton:setUnClickStatus()
	self._normal_sprite:setVisible(true)
	self._click_sprite:setVisible(false)
end

function clsButton:getCOObj()
	return self._RootMsgPanel
end

function clsButton:onTouchBegan(x, y)
	self:setClickStatus()
end

function clsButton:onTouchEnd(x, y)
	self:setNormalStatus()
end

function clsButton:onTouchMove(x, y)
end

function clsButton:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
	self._RootMsgPanel:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
end

function clsButton:setTouchEnabled(tag)
	self._RootMsgPanel:setTouchEnabled(tag)
end
---------------------------------------------------------
clsCheckButton = clsButton:Inherit()
function clsCheckButton:__init__(parent, x, y, normal_pic, click_pic)
	Super(clsCheckButton).__init__(self, parent, x, y, normal_pic, click_pic)
	self._checked = false
end

function clsCheckButton:onTouchBegan(x, y)
	self._checked = not self._checked
	self._normal_sprite:setVisible(not self._checked)
	self._click_sprite:setVisible(self._checked)
end

function clsCheckButton:onTouchEnd(x, y)
end
-------------------------------------------------------------
clsStatusButton = clsButton:Inherit()
function clsStatusButton:__init__(parent, x, y, normal_pic, click_pic)
	Super(clsStatusButton).__init__(self, parent, x, y, normal_pic, click_pic)
	self._checked = false
end

function clsStatusButton:onTouchBegan(x, y)
	if not self._checked then
		self:setClickStatus()
	end
	self._checked = true
end

function clsStatusButton:onTouchEnd(x, y)
end
----------------------------------------------------------
clsRecycleScroll = clsObject:Inherit()
function clsRecycleScroll:__init__(parent, x, y, view_width, view_height )
	Super(clsRecycleScroll).__init__(self)

	self._RootClipPanel = CCClipLayer:create()
	self._RootClipPanel:set_msg_rect(0, 0, view_width, view_height)
	self._ani_interval = 1/30
	self._obj_x_gap = 30
	self._inner_obj_list = {}
	if parent then
		parent:addChild(self)
		self._RootClipPanel:setPosition(x, y)
	end

	self._scroll_cb = nil
	self._x_step = 1 
end

local offsetX = nil
function clsRecycleScroll:addInnerObj(obj, labelX )
	local pre_cnt_idx = table.maxn(self._inner_obj_list)
	table.insert(self._inner_obj_list, obj)
	local objx, objy = 0, 0
	offsetX = labelX
	if labelX then
		objx = labelX
	end
	if pre_cnt_idx > 0 then
		local pre_size = self._inner_obj_list[pre_cnt_idx]:getContentSize()
		local prex, prey = self._inner_obj_list[pre_cnt_idx]:getPosition()
		objx = prex + pre_size.width + self._obj_x_gap
	end
	obj:setAnchorPoint(0, 0)
	obj:setPosition(objx, objy)
	self._RootClipPanel:addChild(obj:getCOObj())
end

function clsRecycleScroll:setString(str)
	if not str or type(str) ~= 'string' then
		return
	else
		for _, obj in pairs(self._inner_obj_list) do
			obj:setString(str)
		end
	end
	
end

local function switchToLast(scroll_obj, obj_idx)
	local elecnt = table.maxn(scroll_obj._inner_obj_list)
	if elecnt <= 1 then
		if offsetX then
			scroll_obj._inner_obj_list[obj_idx]:setPosition(offsetX, 0)
		else
			scroll_obj._inner_obj_list[obj_idx]:setPosition(0, 0)
		end
	else
		local obj = scroll_obj._inner_obj_list[obj_idx]
		table.remove(scroll_obj._inner_obj_list, obj_idx)
		table.insert(scroll_obj._inner_obj_list, obj)
		local pre_obj = scroll_obj._inner_obj_list[elecnt - 1]
		local pre_size = pre_obj:getContentSize()
		local prex, prey = pre_obj:getPosition()
		local objx = prex + pre_size.width + scroll_obj._obj_x_gap
		local objy = prey
		if offsetX then
			obj:setPosition(objx + offsetX, objy)
		else
			obj:setPosition(objx, objy)
		end
		
	end		
end	

--delay 开始滚动延时秒数 isRunOne 是否运行一次
function clsRecycleScroll:startScroll( delay, isRunOne )
	local function do_scroll()
		for obj_idx, obj in ipairs(self._inner_obj_list) do
			local curx, cury = obj:getPosition()
			local obj_size = obj:getContentSize()
			curx = curx - self._x_step
			if curx + obj_size.width <= 0 then
				local function startDelay()
					if TIMER[ 'start_delay' ] then					
						CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(TIMER[ 'start_delay' ])
					end						
					TIMER[ 'scroll_cb' ]= CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(do_scroll, self._ani_interval, false)
				end
				if isRunOne then
					--[[if TIMER[ 'scroll_cb' ] then					
						CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(TIMER[ 'scroll_cb' ])
					end--]]
					self:Destroy()
				else
					if delay then
						if TIMER[ 'scroll_cb' ] then					
							CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(TIMER[ 'scroll_cb' ])
						end
						switchToLast(self, obj_idx)
						TIMER[ 'start_delay' ]= CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(startDelay, delay, false)
					else
						switchToLast(self, obj_idx)
					end
				end
				--switchToLast(self, obj_idx)
			else
				obj:setPosition(curx, cury)
			end
		end
	end

	--self._scroll_cb = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(do_scroll, self._ani_interval, false)
	TIMER[ 'scroll_cb' ]= CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(do_scroll, self._ani_interval, false)
end

function clsRecycleScroll:tryStopScroll()
	if TIMER[ 'scroll_cb' ] then
		--CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(self._scroll_cb)
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(TIMER[ 'scroll_cb' ])
		TIMER[ 'scroll_cb' ] = nil
	end
	if TIMER[ 'start_delay' ] then					
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(TIMER[ 'start_delay' ])
		TIMER[ 'start_delay' ] = nil
	end	
	
end

function clsRecycleScroll:Destroy()
	Super(clsRecycleScroll).Destroy(self)
	self:tryStopScroll()
end

function clsRecycleScroll:getCOObj()
	return self._RootClipPanel
end

-----------------------------------------------------------------------
--[[
item_list = {
	[1] = {
		["txt"] = "aa",
		["normal_color"] = { [1] = **, [2] = **, [3] = **, },
		["sel_color"] = { [1] = **, [2] = **, [3] = **, },
		["normal_pic"] = "",
		["sel_pic"] = "",
	},
	...
}]]
clsMoveGrpHint = clsObject:Inherit() -- node以中心点对齐 x_gap是两个node之间的距离 不算内容的宽度
function clsMoveGrpHint:__init__(parent, x, y, bg_file, item_list, total_width, move_file, msg_obj_pic)
	Super(clsMoveGrpHint).__init__(self)

	self._RootNode = CCNode:create()
	if parent then
		parent:addChild(self)
		self._RootNode:setPosition(x, y)
	end

	local hint_width, hint_height = nil, nil
	local item_cnt = table.maxn(item_list)
	if bg_file then
		bg_file = getResFilename( bg_file )
		self._BGImg = CCSprite:create(bg_file)
		self._BGImg:setPosition(0, 0)
		self._BGImg:setAnchorPoint(CCPointMake(0, 0.5))
		self._RootNode:addChild(self._BGImg)
	end

	local x_gap = total_width / ( item_cnt + 1 )
	hint_width, hint_height = (item_cnt - 1) * x_gap, 0

	local startx = x_gap;
	local starty = (hint_height) / 2

	self._item_list = {}
	self._move_img_y = -22
	for idx, item in ipairs(item_list) do
		local hint_item = CCNode:create()
		local function on_click(obj, x, y)
			self:onClick(idx)
		end
		local msg_obj = clsSimpleButton:New(nil, 0, 0, msg_obj_pic, msg_obj_pic )
		msg_obj.onTouchEnd = on_click
		hint_item:addChild(msg_obj:getCOObj())
		local size = nil

		if item["normal_pic"] and item["sel_pic"] then
			item["normal_pic"] = getResFilename( item["normal_pic"] )
			item["sel_pic"] = getResFilename( item["sel_pic"] )
			hint_item._normal_pic = CCSprite:create(item["normal_pic"])
			hint_item._sel_pic = CCSprite:create(item["sel_pic"])
			hint_item:addChild(hint_item._normal_pic)
			hint_item:addChild(hint_item._sel_pic)
			hint_item._sel_pic:setVisible(false)
			size = hint_item._normal_pic:getContentSize()
		end
		if item["txt"] then
			hint_item._txt_label = CCLabelTTF:create(item["txt"], GLOBAL_FONT, item["size"] )
			hint_item._txt_label:setAnchorPoint(CCPointMake(0.5, 0.5))
			hint_item._txt_label._normal_color = item["normal_color"]
			hint_item._txt_label._sel_color = item["sel_color"]
			hint_item._txt_label:setColor(ccc3(item["normal_color"][1], item["normal_color"][2], item["normal_color"][3]))
			hint_item:addChild(hint_item._txt_label)
			size = hint_item._txt_label:getContentSize()
		end

		msg_obj:setButtonScaleX(size.width)
		msg_obj:setButtonScaleY(size.height)

		self._RootNode:addChild(hint_item)
		hint_item:setPosition(startx, starty)
		startx = startx + x_gap-- + size.width / 2
		table.insert(self._item_list, hint_item)
	end

	if move_file then
		move_file = getResFilename( move_file )
		self._MoveImg = CCSprite:create(move_file)
		--self._MoveImg:setAnchorPoint(CCPointMake(0.5, 0)) -- 下中点作为对齐点
		self._MoveImg:setPosition(0, self._move_img_y)
		self._RootNode:addChild(self._MoveImg)
	end

	self._x_gap = x_gap
	self._cur_sel = nil
end

function clsMoveGrpHint:getCOObj()
	return self._RootNode
end

function clsMoveGrpHint:onClick(idx)
	print("oo", idx)
end

function clsMoveGrpHint:setBGScaleX(sx)
	self._BGImg:setScaleX(sx)
end

function clsMoveGrpHint:setBGScaleY(sy)
	self._BGImg:setScaleY(sy)
end

function clsMoveGrpHint:getBGObj()
	return self._BGImg
end

local function selMoveGrpHintItem(hint_item)
	if hint_item._normal_pic then
		hint_item._normal_pic:setVisible(false)
	end
	if hint_item._sel_pic then
		hint_item._sel_pic:setVisible(true)
	end
	if hint_item._txt_label then
		local sel_color = hint_item._txt_label._sel_color
		hint_item._txt_label:setColor(ccc3(sel_color[1], sel_color[2], sel_color[3]))
	end
end

local function unselMoveGrpHintItem(hint_item)
	if hint_item._normal_pic then
		hint_item._normal_pic:setVisible(true)
	end
	if hint_item._sel_pic then
		hint_item._sel_pic:setVisible(false)
	end
	if hint_item._txt_label then
		local normal_color = hint_item._txt_label._normal_color
		hint_item._txt_label:setColor(ccc3(normal_color[1], normal_color[2], normal_color[3]))
	end
end

function clsMoveGrpHint:selItem(sel_idx)
	if self._cur_sel then
		unselMoveGrpHintItem(self._item_list[self._cur_sel])
	end

	self._cur_sel = sel_idx
	selMoveGrpHintItem(self._item_list[self._cur_sel])

	if self._MoveImg then
		local sel_item = self._item_list[self._cur_sel]
		local x, y = sel_item:getPosition()
		self._MoveImg:setPosition(x, self._move_img_y)
	end
end

function clsMoveGrpHint:moveToItem(to_idx)
end

function clsMoveGrpHint:getContentSize()
	local ele_cnt = table.maxn(self._item_list)
	return { ["width"] = self._x_gap * (ele_cnt - 1), ["height"] = 0, }
end

function clsMoveGrpHint:getPosition()
	return self._RootNode:getPosition()
end

function clsMoveGrpHint:setPosition(x, y)
	self._RootNode:setPosition(x, y)
end

------------------------------------------------------------------
clsHSlider = clsObject:Inherit()
function clsHSlider:__init__(parent, x, y, btn_pic, touch_width, touch_height)
	self._RootNode = CCNode:create()
	if parent then
		parent:addChild(self)
		self:setPosition(x, y)
	end

	self._touch_width = touch_width
	self._touch_height = touch_height
	self.on_slide_cb = nil

	self._MovePic = CCSprite:create(btn_pic)
	self._RootNode:addChild(self._MovePic)
	self._MovePic:setPosition(0, 0)
	self._MovePic:setAnchorPoint(CCPointMake(0.5, 0.5))

	self._TouchArea = CCClipLayer:create()
	self._RootNode:addChild(self._TouchArea)
	self._TouchArea:set_msg_rect(0, 0, touch_width, touch_height)
	self._TouchArea:setPosition(0, -touch_height/2)

	makeObjMouseEventHandle(self)
end

function clsHSlider:setSlideCB(cb)
	self.on_slide_cb = cb
end

function clsHSlider:setPosition(x, y)
	self._RootNode:setPosition(x, y)
end

local function check_hslider_x(obj, x)
	if x > obj._touch_width then
		x = obj._touch_width
	end
	if x < 0 then
		x = 0
	end

	return x
end

local function callHSliderCB(obj)
	if not obj.on_slide_cb then
		return
	end
	local pic_x, pic_y = obj._MovePic:getPosition()
	local ret_value = (pic_x / obj._touch_width) * 100
	obj:on_slide_cb(ret_value)
end

function clsHSlider:onTouchBegan(x, y)
	local pic_x, pic_y = self._MovePic:getPosition()
	x = check_hslider_x(self, x)
	self._MovePic:setPosition(x, pic_y)
	callHSliderCB(self)
end

function clsHSlider:onTouchMove(x, y)
	local pic_x, pic_y = self._MovePic:getPosition()
	x = check_hslider_x(self, x)
	self._MovePic:setPosition(x, pic_y)
	callHSliderCB(self)
end

function clsHSlider:onTouchEnd(x, y)
	local pic_x, pic_y = self._MovePic:getPosition()
	x = check_hslider_x(self, x)
	self._MovePic:setPosition(x, pic_y)
	callHSliderCB(self)
end

function clsHSlider:getCOObj()
	return self._RootNode
end

function clsHSlider:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
	self._TouchArea:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
end

function clsHSlider:setTouchEnabled(tag)
	self._TouchArea:setTouchEnabled(tag)
end

----------------------------------------------------------------------

clsFrameButton = clsObject:Inherit()
function clsFrameButton:__init__(parent, x, y, frame_width, frame_height, normal_res, click_res, line_res)
	self._RootNode = CCNode:create()
	if parent then
		parent:addChild(self)
		self._RootNode:setPosition(x, y)
	end

	self._frame_width = frame_width
	self._frame_height = frame_height

	if not normal_res then
		normal_res = "tiny_bg_white_pixel.png"
		click_res = normal_res
	end

	if not line_res then
		line_res = "tiny_line_pixel.png"
	end

	self._MsgButton = clsSimpleButton:New(nil, 0, 0, normal_res, click_res)
	self._RootNode:addChild(self._MsgButton:getCOObj())
	self._MsgButton:setPosition(0, 0)
	self._MsgButton:setButtonScaleX(frame_width)
	self._MsgButton:setButtonScaleY(frame_height)

	local function onTouchBegan(obj, x, y)
		self:onTouchBegan(x, y)
	end
	local function onTouchMove(obj, x, y)
		self:onTouchMove(x, y)
	end
	local function onTouchEnd(obj, x, y)
		self:onTouchEnd(x, y)
	end
	self._MsgButton.onTouchBegan = onTouchBegan
	self._MsgButton.onTouchMove = onTouchMove
	self._MsgButton.onTouchEnd = onTouchEnd

	self._MsgButton:setAnchorPoint(0, 1)

	line_res = getResFilename( line_res )
	self._top_line = CCSprite:create( line_res )
	self._top_line:setAnchorPoint(CCPointMake(0, 1))
	self._top_line:setPosition(0, 0)
	self._RootNode:addChild(self._top_line)
	self._top_line:setScaleX(frame_width)

	self._bottom_line = CCSprite:create( line_res )
	self._bottom_line:setAnchorPoint(CCPointMake(0, 1))
	self._bottom_line:setPosition(0, -frame_height)
	self._RootNode:addChild(self._bottom_line)
	self._bottom_line:setScaleX(frame_width)

	self._left_line = CCSprite:create( line_res )
	self._left_line:setAnchorPoint(CCPointMake(0, 1))
	self._left_line:setPosition(0, 0)
	self._RootNode:addChild(self._left_line)
	self._left_line:setScaleY(frame_height)

	self._right_line = CCSprite:create( line_res )
	self._right_line:setAnchorPoint(CCPointMake(0, 1))
	self._right_line:setPosition(frame_width, 0)
	self._RootNode:addChild(self._right_line)
	self._right_line:setScaleY(frame_height)

	
	if normal_res and frame_height == 1 then
		local temp1 = CCSprite:create( normal_res )
		local temp2 = temp1:getContentSize()
		frame_height = temp2.height
	end
	self._dis_txt = CCLabelTTF:create("", GLOBAL_FONT, 16)
	self._dis_txt:setPosition(frame_width/2, -frame_height/2)
	self._RootNode:addChild(self._dis_txt)
end

function clsFrameButton:setString(str)
	if not str or type(str) ~= 'string' then
		return
	else
		self._dis_txt:setString(str)
	end
	
end

function clsFrameButton:setPosition(x, y)
	self._RootNode:setPosition(x, y)
end

function clsFrameButton:setAnchorPoint(x, y)
	self._RootNode:setAnchorPoint(CCPointMake(x, y))
end

function clsFrameButton:getContentSize()
	-- by dda，因未有需求，未经验证
	return { width = self._frame_width or 0, height = self._frame_height or 0, }
end

function clsFrameButton:setTextColor(r, g, b)
	self._dis_txt:setColor(ccc3(r, g, b))
end

function clsFrameButton:hideLine()
	self._top_line:setVisible(false)
	self._bottom_line:setVisible(false)
	self._left_line:setVisible(false)
	self._right_line:setVisible(false)
end

function clsFrameButton:setPixelScale(width, height)
	self._frame_width = width
	self._frame_height = height 
	self._MsgButton:setButtonScaleX(width)
	self._MsgButton:setButtonScaleY(height)
	self._top_line:setScaleX(width)
	self._bottom_line:setPosition(0, -height)
	self._bottom_line:setScaleX(width)
	self._left_line:setScaleY(height)
	self._right_line:setScaleY(height)
	self._right_line:setPosition(width, 0)
end

function clsFrameButton:setContentSize(width, height)
	-- by dda, 20130712
	self:setPixelScale(width, height)
end

function clsFrameButton:convertToWorldSpace(x, y)
	return self._MsgButton:convertToWorldSpace(x, y)
end

function clsFrameButton:getCOObj()
	return self._RootNode
end

function clsFrameButton:onTouchBegan(x, y)
end

function clsFrameButton:onTouchMove(x, y)
end

function clsFrameButton:onTouchEnd(x, y)
end

-------------------------------------------------------
clsRatioBg = clsObject:Inherit()

function clsRatioBg:__init__(parent, x, y, bg_file, ratio_file, width, height)
	self._RootNode = CCNode:create()
	if parent then
		parent:addChild(self)
		self._RootNode:setPosition(x, y)
	end

	self._BGSprite = CCSprite:create(bg_file)
	self._BGSprite:setPosition(0, 0)
	self._BGSprite:setAnchorPoint(CCPointMake(0, 1))
	self._BGSprite:setScaleX(width)
	self._BGSprite:setScaleY(height)
	self._RootNode:addChild(self._BGSprite)

	self._RatioSprite = CCSprite:create(ratio_file)
	self._RatioSprite:setPosition(0, 0)
	self._RatioSprite:setAnchorPoint(CCPointMake(0, 1))
	self._RatioSprite:setScaleY(height)
	self._RatioSprite:setVisible(false)
	self._RootNode:addChild(self._RatioSprite)

	self._ratio_width = width
	self._raito = 0
end

function clsRatioBg:setRatio(ratio)
	if ratio > 0 then
		self._raito = ratio
		local cur_ratio_width = (ratio / 100) * self._ratio_width
		self._RatioSprite:setScaleX(cur_ratio_width)
		self._RatioSprite:setVisible(true)
	else
		self._RatioSprite:setVisible(false)
	end
end

function clsRatioBg:getCOObj()
	return self._RootNode
end

function clsRatioBg:setVisible(tag)
	self._RootNode:setVisible(tag)
end

function clsRatioBg:setAnchorPoint(x, y)
	self._RootNode:setAnchorPoint(CCPointMake(x, y))
end

-------------------------------------------------
NORMAL_STATUS = 1
DOWN_STATUS = 2
SELECTED_STATUS = 3
DISABLE_STATUS = 4

clsBaseButton = clsObject:Inherit()

function clsBaseButton:getSpriteObj(res)
	res = getResFilename( res )	
	return CCSprite:create(res)
end

function clsBaseButton:__init__(parent, x, y, btn_res_list)
	Super(clsBaseButton).__init__(self)
	self._RootLayer = CCLayer:create()
	self._btn_res_list = btn_res_list
	if parent then
		parent:addChild(self)
		self:setPosition(x, y)
	end
	makeObjMouseEventHandle2(self)

	self._btn_sprite_tbl = {}
	for status, res in pairs(self._btn_res_list) do
		local btn_sprite = self:getSpriteObj(res)
		self._RootLayer:addChild(btn_sprite)
		btn_sprite:setPosition(0, 0)
		btn_sprite:setAnchorPoint(CCPointMake(0.5, 0.5)) -- 默认中心点对齐
		btn_sprite:setVisible(false)
		self._btn_sprite_tbl[status] = btn_sprite
	end

	self._cur_status = NORMAL_STATUS
	self:setStatus(self._cur_status)
end

function clsBaseButton:removeFromParentAndCleanup(tag)
	-- by dda, 20130704
	--self:Destroy()
	self._RootLayer:removeFromParentAndCleanup(tag)
end

function clsBaseButton:setContentSize( width, height )
	-- by dda, 20130620
	local size = self._btn_sprite_tbl[ self._cur_status ]:getContentSize()
	
	local scalex = width / size.width
	local scaley = height / size.height
	
	self._scalex = scalex
	self._scaley = scaley
		
	for status, btn_sprite in pairs(self._btn_sprite_tbl) do
		btn_sprite:setScaleX( scalex )
		btn_sprite:setScaleY( scaley )
	end
end

function clsBaseButton:getContentSize()
	-- by dda, 20130620
	local btn = self._btn_sprite_tbl[ self._cur_status ]
	local size = btn:getContentSize()

	size.width = size.width * ( self._scalex or 1 )
	size.height = size.height * ( self._scaley or 1 )
	
	return size
end

function clsBaseButton:getStatus()
	return self._cur_status
end

function clsBaseButton:setStatus(status)
	for status, btn_sprite in pairs(self._btn_sprite_tbl) do
		btn_sprite:setVisible(false)
	end

	self._btn_sprite_tbl[status]:setVisible(true)
	self._cur_status = status
end

function clsBaseButton:setDownStatus()
	-- by dda
	self:setStatus( DOWN_STATUS )
end

function clsBaseButton:setNormalStatus()
	-- by dda
	self:setStatus( NORMAL_STATUS )
end

function clsBaseButton:getCOObj()
	return self._RootLayer
end

function clsBaseButton:setAnchorPoint(x, y)
	for status, sprite_obj in pairs(self._btn_sprite_tbl) do
		sprite_obj:setAnchorPoint(CCPointMake(x, y))
	end
	if self._str_obj then
	-- by dda, 20130718，保证文字时刻居中
	--	self._str_obj:setAnchorPoint(CCPointMake(x, y))
		local o = self._btn_sprite_tbl[self._cur_status]
		local px, py = o:getPosition()
		local size = o:getContentSize()
		local xo = px + size.width * ( 0.5 - x )
		local yo = py + size.height * ( 0.5 - y )
		self._str_obj:setPosition(xo, yo)
	end
end

-- 20130620，dda加font, fontsize参数
function clsBaseButton:setString(str, font, fontsize )
	if not self._str_obj then
		self._str_obj = CCLabelTTF:create("", font or GLOBAL_FONT, fontsize or 16 )
		self._RootLayer:addChild(self._str_obj)
	end
	if not str or type(str) ~= 'string' then
		return
	else
		self._str_obj:setString(str)
	end		
end

function clsBaseButton:setTextColor(r, g, b)
	if self._str_obj then
		self._str_obj:setColor(ccc3(r, g, b))
	end
end

function clsBaseButton:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
	self._RootLayer:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
end

function clsBaseButton:setTouchEnabled(tag)
	self._RootLayer:setTouchEnabled(tag)
end

function clsBaseButton:onTouchDown(x, y) -- 对外重写接口
end

function clsBaseButton:onTouchInMove(x, y) -- 对外重写接口 暂不使用
end

function clsBaseButton:onTouchOutMove(x, y) -- 对外重写接口 暂不使用
end

function clsBaseButton:onTouchUp(x, y) -- 对外重写接口
end

local function clsBaseButton_CheckCaptureMsg(btn_obj, x, y)
	local node_space_point = btn_obj._RootLayer:convertToNodeSpace(CCPointMake(x, y))
	local cur_sel_sprite = btn_obj._btn_sprite_tbl[btn_obj._cur_status]
	
	-- by dda, 20130701
	--local size = cur_sel_sprite:getContentSize()
	local size = btn_obj:getContentSize()
	
	-- by dda, 20130620，加上拉伸，所以原写法有误
	--size.width = size.width * ( cur_sel_sprite._scalex or 1 )
	--size.height = size.height * ( cur_sel_sprite._scaley or 1 )
	
	local anchor_point = cur_sel_sprite:getAnchorPoint() 
	local offx, offy = anchor_point.x * size.width, anchor_point.y * size.height
	local sprite_space_x = node_space_point.x + offx
	local sprite_space_y = node_space_point.y + offy	
	if sprite_space_x >= 0 and sprite_space_y >= 0 and sprite_space_x <= size.width and sprite_space_y <= size.height then
		return true, sprite_space_x, sprite_space_y
	else
		return false, nil, nil
	end
end

function clsBaseButton:onDownChangeSprite() -- 内部继承切换帧接口
	--dda加
	self:setStatus(DOWN_STATUS)
end

function clsBaseButton:onUpChangeSprite() -- 内部继承切换帧接口
	--dda加
	self:setStatus(NORMAL_STATUS)
end

function clsBaseButton:onTouchBegan(x, y)
	local captured, sprite_space_x, sprite_space_y = clsBaseButton_CheckCaptureMsg(self, x, y)
	if captured then
		self:onDownChangeSprite()
		self:onTouchDown(sprite_space_x, sprite_space_y)
		return true
	else
		return false
	end
end

function clsBaseButton:onTouchMove(x, y)
	--[[local captured, sprite_space_x, sprite_space_y = clsBaseButton_CheckCaptureMsg(self, x, y)
	if captured then
		self:onTouchInMove(sprite_space_x, sprite_space_y)
	else
		self:onTouchOutMove(sprite_space_x, sprite_space_y)
	end]]
end

function clsBaseButton:onTouchEnd(x, y)
	local captured, sprite_space_x, sprite_space_y = clsBaseButton_CheckCaptureMsg(self, x, y)
	if captured then
		self:onTouchUp(sprite_space_x, sprite_space_y)
	end
	self:onUpChangeSprite()
end

function clsBaseButton:setPosition(x, y)
	self._RootLayer:setPosition(x, y)
end

-- 2013-07-03增加getPosition方法 by xr
function clsBaseButton:getPosition()
	return self._RootLayer:getPosition()
end

function clsBaseButton:release()
	self._RootLayer:removeFromParentAndCleanup(true)
end

function clsBaseButton:setVisible(tag)
	self._RootLayer:setVisible(tag)
end

------------------------------------------------------

clsSelectStateSlideButton = clsObject:Inherit()

function clsSelectStateSlideButton:getSpriteObj(res)
	res = getResFilename( res )
	return CCSprite:create(res)
end

function clsSelectStateSlideButton:__init__( parent, x, y, normalRes, downRes, selectRes, selectIndex, callBack, isSelect )
	Super(clsSelectStateSlideButton).__init__(self)
	local btn_res = {
		[LIGHT_UI.NORMAL_STATUS] = normalRes,
		[LIGHT_UI.DOWN_STATUS] = downRes,
	}
	self._RootLayer = CCLayer:create()
	--self._btn_res_list = btn_res
	if parent then
		parent:addChild(self)
		self:setPosition(x, y)
	end
	makeObjMouseEventHandle2(self)
	self._btn_sprite_tbl = {}
	for status, res in pairs(btn_res) do
		--local btn_sprite = self:getSpriteObj(res)
		if parent then
			
			local btn_sprite = self:getSpriteObj(res)
			self._RootLayer:addChild(btn_sprite)
			btn_sprite:setPosition(0, 0)
			btn_sprite:setAnchorPoint(CCPointMake(0.5, 0.5)) -- 默认中心点对齐
			--btn_sprite:setAnchorPoint(0.5, 0.5) -- 默认中心点对齐
			btn_sprite:setVisible(false)
			self._btn_sprite_tbl[status] = btn_sprite
		end
	end
	self.leftScaleSelectState = clsSprite:New( parent, x, y, selectRes )
	self.leftScaleSelectState:setAnchorPoint( 1, 0.5 )
	local size = self.leftScaleSelectState:getContentSize()
	self.leftScaleSelectState:setPosition( x + size.width / 2, y )
	self.rightScaleSelectState = clsSprite:New( parent, x, y, selectRes )
	self.rightScaleSelectState:setAnchorPoint( 0, 0.5 )	
	self.rightScaleSelectState:setPosition( x - size.width / 2, y )
	if not isSelect then
		self.leftScaleSelectState:setScaleX( 0 )
		self.rightScaleSelectState:setScaleX( 0 )
	end
	--self.leftScaleSelectState:setVisible( false )
	--self.rightScaleSelectState:setVisible( false )
	self.index = selectIndex
	self._cur_status = NORMAL_STATUS
	self:setStatus(self._cur_status)
	self.parentObj = parent	
	self.back = callBack
end

function clsSelectStateSlideButton:getCOObj()
	return self._RootLayer
end

function clsSelectStateSlideButton:setPosition(x, y)
	self._RootLayer:setPosition(x, y)
end

function clsSelectStateSlideButton:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
	self._RootLayer:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
end

function clsSelectStateSlideButton:springbackHandler( direction )
	local lPosX, lPosY = self.parentObj.selectBtn.leftScaleSelectState:getPosition()
	local rPosX, rPosY = self.parentObj.selectBtn.rightScaleSelectState:getPosition()
	local count = 0
	local function springback()
		if count == 0 then
			if direction == "right" then
				self.parentObj.selectBtn.leftScaleSelectState:setPosition( lPosX + 5, lPosY )
				self.parentObj.selectBtn.rightScaleSelectState:setPosition( rPosX + 5, rPosY )
			end
			if direction == "left" then
				self.parentObj.selectBtn.leftScaleSelectState:setPosition( lPosX - 5, lPosY )
				self.parentObj.selectBtn.rightScaleSelectState:setPosition( rPosX - 5, rPosY )
			end
		end
		if count == 1 then
			if direction == "right" then
				self.parentObj.selectBtn.leftScaleSelectState:setPosition( lPosX - 5, lPosY )
				self.parentObj.selectBtn.rightScaleSelectState:setPosition( rPosX - 5, rPosY )
			end
			if direction == "left" then
				self.parentObj.selectBtn.leftScaleSelectState:setPosition( lPosX + 5, lPosY )
				self.parentObj.selectBtn.rightScaleSelectState:setPosition( rPosX + 5, rPosY )
			end
		end
		if count == 2 then
			if springbackTimer then
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(springbackTimer)
				springbackTimer = nil
			end
			self.parentObj.selectBtn.leftScaleSelectState:setPosition( lPosX, lPosY )
			self.parentObj.selectBtn.rightScaleSelectState:setPosition( rPosX, rPosY )
		end
		count = count + 1
	end
	if not springbackTimer then
		springbackTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( springback, 0.01, false )
	end
end

function clsSelectStateSlideButton:reduceHandler()
	local count = 0	
	local function reduceSelectBar()
		self.parentObj.selectBtn.leftScaleSelectState:setScaleX( ( 4 - count ) / 4)
		if count >= 4 then
			if reduceTimer then
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(reduceTimer)
				reduceTimer = nil
			end					
			if self.parentObj.selectBtn.index + 1 == self.index then
				self.parentObj.selectBtn = self.parentObj.group[ self.parentObj.selectBtn.index + 1 ]
				self.parentObj.selectBtn.leftScaleSelectState:setScaleX( 1 )
				self:springbackHandler( "right" )
			else
				self.parentObj.selectBtn = self.parentObj.group[ self.parentObj.selectBtn.index + 1 ]
				self.parentObj.selectBtn.leftScaleSelectState:setScaleX( 1 )
				self:slideHandler( self.index, self.parentObj.selectBtn.index )
			end
		end
		
		count = count + 1
	end
	if not reduceTimer then
		reduceTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( reduceSelectBar, 0.01, false )
	end
end

function clsSelectStateSlideButton:expandHandler()
	local count = 0	
	local function expandSelectBar()
		self.parentObj.group[ self.parentObj.selectBtn.index + 1 ].rightScaleSelectState:setScaleX( count / 4 )
		if count >= 4 then
			if expandTimer then
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(expandTimer)
				expandTimer = nil
			end
			
		end
		
		count = count + 1
	end
	if not expandTimer then
		expandTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( expandSelectBar, 0.01, false )
	end
end

function clsSelectStateSlideButton:reverseReduceHandler()
	local count = 0	
	local function reduceSelectBar()
		self.parentObj.selectBtn.rightScaleSelectState:setScaleX( ( 4 - count ) / 4)
		if count >= 4 then
			if reduceTimer then
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(reduceTimer)
				reduceTimer = nil
			end
			if self.parentObj.selectBtn.index - 1 == self.index then
				self.parentObj.selectBtn = self.parentObj.group[ self.parentObj.selectBtn.index - 1 ]
				self.parentObj.selectBtn.rightScaleSelectState:setScaleX( 1 )
				self:springbackHandler( "left" )
			else
				self.parentObj.selectBtn = self.parentObj.group[ self.parentObj.selectBtn.index - 1 ]
				self.parentObj.selectBtn.rightScaleSelectState:setScaleX( 1 )
				self:slideHandler( self.index, self.parentObj.selectBtn.index )
			end
		end	
		count = count + 1
	end
	if not reduceTimer then
		reduceTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( reduceSelectBar, 0.01, false )
	end
end

function clsSelectStateSlideButton:reverseExpandHandler()
	local count = 0	
	local function expandSelectBar()
		self.parentObj.group[ self.parentObj.selectBtn.index - 1 ].leftScaleSelectState:setScaleX( count / 4 )
		if count >= 4 then
			if expandTimer then
				CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(expandTimer)
				expandTimer = nil
			end
			
		end
		
		count = count + 1
	end
	if not expandTimer then
		expandTimer = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( expandSelectBar, 0.01, false )
	end
end

function clsSelectStateSlideButton:slideHandler( clickIndex, selectIndex )
	if clickIndex == selectIndex then
		return
	end
	if clickIndex > selectIndex then
		self.parentObj.selectBtn.rightScaleSelectState:setScaleX( 0 )
		self:reduceHandler()
		self:expandHandler()
		
	end
	if clickIndex < selectIndex then
		self.parentObj.selectBtn.leftScaleSelectState:setScaleX( 0 )
		self:reverseReduceHandler()
		self:reverseExpandHandler()
	end
end	

function clsSelectStateSlideButton:onTouchBegan(x, y)
	local captured, sprite_space_x, sprite_space_y = clsBaseButton_CheckCaptureMsg(self, x, y)
	if captured then
		self:onDownChangeSprite()
		self:onTouchDown(sprite_space_x, sprite_space_y)
		self:slideHandler( self.index, self.parentObj.selectBtn.index )
		self.back( self.index )
		return true
	else
		return false
	end
end	

function clsSelectStateSlideButton:onTouchMove(x, y)

end

function clsSelectStateSlideButton:onTouchEnd(x, y)
	local captured, sprite_space_x, sprite_space_y = clsBaseButton_CheckCaptureMsg(self, x, y)
	--[[if captured then
		self:onTouchUp(sprite_space_x, sprite_space_y)
	end--]]		
	self:onUpChangeSprite()
end

function clsSelectStateSlideButton:getContentSize()
	local btn = self._btn_sprite_tbl[ self._cur_status ]
	local size = btn:getContentSize()

	size.width = size.width * ( self._scalex or 1 )
	size.height = size.height * ( self._scaley or 1 )
	
	return size
end

function clsSelectStateSlideButton:setTouchEnabled(tag)
	self._RootLayer:setTouchEnabled(tag)
end

function clsSelectStateSlideButton:onDownChangeSprite() 
	self:setStatus(DOWN_STATUS)
end

function clsSelectStateSlideButton:setStatus(status)
	for status, btn_sprite in pairs(self._btn_sprite_tbl) do
		btn_sprite:setVisible(false)
	end

	self._btn_sprite_tbl[status]:setVisible(true)
	self._cur_status = status
end

function clsSelectStateSlideButton:onTouchDown(x, y)
end

function clsSelectStateSlideButton:onUpChangeSprite() 
	self:setStatus(NORMAL_STATUS)
end

----------------------------------------

clsSelectStateSlideTabBar = clsObject:Inherit()

function clsSelectStateSlideTabBar:__init__( parent, x, y, normalResGroup, downResGroup, selectResGroup, width, space, line, rowHeight, defaultSelect, fun )
	Super(clsSelectStateSlideTabBar).__init__(self)
	self._RootLayer = CCLayer:create()
	if parent then
		parent:addChild(self)
		self:setPosition(x, y)
	end
		
	local groupY = 0
	local groupX = 0
	self.group = {}
	for k,v in ipairs( selectResGroup ) do	
		if k == defaultSelect then
			self.group[ k - 1 ] = clsSelectStateSlideButton:New( self,groupX * ( width + space ), groupY, normalResGroup[ k ], downResGroup[ k ], v, k - 1, fun, true )
			self.selectBtn = self.group[ k - 1 ]			
		else
			self.group[ k - 1 ] = clsSelectStateSlideButton:New( self, groupX * ( width + space ), groupY, normalResGroup[ k ], downResGroup[ k ], v, k - 1, fun )					
		end	
		if k % line == 0 then
			groupY = groupY - rowHeight
			groupX = 0			
		else			
			groupX = groupX + 1
		end		
	end	
end	

function clsSelectStateSlideTabBar:getCOObj()
	return self._RootLayer
end

function clsSelectStateSlideTabBar:setPosition(x, y)
	self._RootLayer:setPosition(x, y)
end

function clsSelectStateSlideTabBar:addChild(child)
	self._RootLayer:addChild(child:getCOObj())
end

function clsSelectStateSlideTabBar:setVisible(tag)
	self._RootLayer:setVisible(tag)
end

------------------------------------------------

clsSlide = clsObject:Inherit()

local function clsSlide_CheckCaptureMsg(btn_obj, x, y, scaleX )
	local node_space_point = btn_obj._RootLayer:convertToNodeSpace(CCPointMake(x, y))
	local cur_sel_sprite = btn_obj._btn_sprite_tbl[btn_obj._cur_status]
		
	local size = btn_obj:getContentSize()		
	
	local anchor_point = cur_sel_sprite:getAnchorPoint() 
	local offx, offy = anchor_point.x * size.width, anchor_point.y * size.height
	local sprite_space_x = node_space_point.x + offx
	local sprite_space_y = node_space_point.y + offy	
	if sprite_space_x >= 0 and sprite_space_y >= 0 and sprite_space_x <= size.width * scaleX and sprite_space_y <= size.height then
		return true, sprite_space_x, sprite_space_y
	else
		return false, nil, nil
	end
end

function clsSlide:__init__( parent, x, y, slideBlockRes, width, percent, callback )
	Super(clsSlide).__init__(self)
	self._RootLayer = CCLayer:create()
	if parent then
		parent:addChild(self)
		self:setPosition(x, y)
	end
	
	self.initX = x
	self.initY = y
	self.fun = callback
	
	local function clickTrackHandler( t, x, y )
		local captured, sprite_space_x, sprite_space_y = clsSlide_CheckCaptureMsg( self.track, x, y, width / 2 )
		if captured then
			self.slideBlock:setPosition( x - 50 - self.slideBlockWidth / 2, 0 )
			self.progress:setScaleX( ( x - self.initX ) / 2 )
			--print( "percent:" .. ( x - self.initX ) / width )
			if callback then
				callback( ( x - self.initX ) / width )
			end
			return true	
		else	
			return false
		end
	end
	
	local btn_res = {
		[LIGHT_UI.NORMAL_STATUS] = "res/tzje-h_03.jpg",
		[LIGHT_UI.DOWN_STATUS] = "res/tzje-h_03.jpg",
	}
	self.leftCircle = clsSprite:New( self, -7, 0, "res/tzje-h_01.jpg" )
	self.leftCircle:setAnchorPoint( 0, 0.5 )
	self.leftCircle:setPosition( -4, 0 )
	self.rightCircle = clsSprite:New( self, width, 0, "res/tzje-h_05.jpg" )
	self.rightCircle:setAnchorPoint( 0, 0.5 )
	self.rightCircle:setPosition( width, 0 )
	self.leftUpCircle = clsSprite:New( self, -7, 0, "res/tzje-j_01.jpg" )
	self.leftUpCircle:setAnchorPoint( 0, 0.5 )
	self.leftUpCircle:setPosition( -4, 0 )
	self.rightUpCircle = clsSprite:New( self, width, 0, "res/tzje-j_05.jpg" )
	self.rightUpCircle:setAnchorPoint( 0, 0.5 )
	self.rightUpCircle:setPosition( width, 0 )
	if percent ~= 100 then
		self.rightUpCircle:setVisible( false )
	end
	self.track = clsBaseButton:New( self, 0, 0, btn_res )
	self.track:setAnchorPoint( 0, 0.5 )
	self.track._btn_sprite_tbl[ 1 ]:setScaleX( width / 2 )
	self.track._btn_sprite_tbl[ 2 ]:setScaleX( width / 2 )
	self.track.onTouchBegan = clickTrackHandler
	
	self.progress = clsSprite:New( self, 0, 0, "res/tzje-j_03.jpg" )
	self.progress:setAnchorPoint( 0, 0.5 )
	self.progress:setScaleX( width / 2 * percent / 100 )
	
	local function slideBlockBeganHandler( t, x, y )
		local captured, sprite_space_x, sprite_space_y = clsBaseButton_CheckCaptureMsg( self.slideBlock, x, y)
		if captured then
			self.canMove = true
			return true	
		else	
			return false
		end
	end
	local function slideBlockMoveHandler( t, x, y )	
		--print( self.canMove )
		if self.canMove == true then
			--if y <= self.upLimit then
				--if y >= self.downLimit then
					if x < self.leftLimit then
						x = self.leftLimit
						self.canMove = false						
					end
					if x > self.rightLimit then
						x = self.rightLimit
						self.canMove = false						
					end
					local posX, posY = self.slideBlock:getPosition()
					local size = self.slideBlock:getContentSize()
					--if x - 55 >= posX - size.width / 2 then
						--if x - 55 <= posX + size.width / 2 then
							self.slideBlock:setPosition( x - 50 - size.width / 2, 0 )
							self.progress:setScaleX( ( x - self.initX ) / 2 )
							--print( "percent:" .. ( x - self.initX ) / width )
							if callback then
								callback( ( x - self.initX ) / width )
							end
						--end
					--end						
			--[[	else
					self.canMove = false				
				end
			else
				self.canMove = false			
			end	--]]
		else
			if x > self.leftLimit then
				if x < self.rightLimit then
					self.canMove = true
				end
			end				
		end	
	end
	btn_res = {
		[LIGHT_UI.NORMAL_STATUS] = slideBlockRes.res,
		[LIGHT_UI.DOWN_STATUS] = slideBlockRes.on,
	}
	self.slideBlock = clsBaseButton:New( self, width * percent / 100, 0, btn_res )
	--self.slideBlock:setAnchorPoint( 0, 0.5 )
	self.slideBlock.onTouchBegan = slideBlockBeganHandler
	self.slideBlock.onTouchMove = slideBlockMoveHandler	
	local size = self.slideBlock:getContentSize()	
	self.slideBlockWidth = size.width	
	self.upLimit = y + size.height / 2
	self.downLimit = y - size.height / 2
	--self.leftLimit = x  - size.width / 2
	self.leftLimit = x
	--self.rightLimit = x + width - size.width / 2
	self.rightLimit = x + width
	self.canMove = false	
	self.slideWidth = width
	self.slidePercent = percent
end

function clsSlide:getCOObj()
	return self._RootLayer
end

function clsSlide:setPosition(x, y)
	self._RootLayer:setPosition(x, y)
end

function clsSlide:addChild(child)
	self._RootLayer:addChild(child:getCOObj())
end

--切换滑块图片
function clsSlide:switchSlideBlockImage( resconfig )
	local function slideBlockBeganHandler( t, x, y )
		local captured, sprite_space_x, sprite_space_y = clsBaseButton_CheckCaptureMsg( self.slideBlock, x, y)
		if captured then
			self.canMove = true
			return true	
		else	
			return false
		end
	end
	
	local function slideBlockMoveHandler( t, x, y )	
		--print( self.canMove )
		if self.canMove == true then
			--if y <= self.upLimit then
				--if y >= self.downLimit then
					if x < self.leftLimit then
						x = self.leftLimit
						self.canMove = false						
					end
					if x > self.rightLimit then
						x = self.rightLimit
						self.canMove = false						
					end
					local posX, posY = self.slideBlock:getPosition()
					local size = self.slideBlock:getContentSize()
					--if x - 55 >= posX - size.width / 2 then
						--if x - 55 <= posX + size.width / 2 then
							self.slideBlock:setPosition( x - 50 - size.width / 2, 0 )
							self.progress:setScaleX( ( x - self.initX ) / 2 )
							--print( "percent:" .. ( x - self.initX ) / self.slideWidth )
							if self.fun then
								self.fun( ( x - self.initX ) / self.slideWidth )
							end
						--end
					--end						
			--[[	else
					self.canMove = false				
				end
			else
				self.canMove = false			
			end	--]]
		else
			if x > self.leftLimit then
				if x < self.rightLimit then
					self.canMove = true
				end
			end				
		end	
	end
	btn_res = {
		[LIGHT_UI.NORMAL_STATUS] = resconfig.res,
		[LIGHT_UI.DOWN_STATUS] = resconfig.on,
	}
	clear( {self.slideBlock} )
	self.slideBlock = nil
	self.slideBlock = clsBaseButton:New( self, self.slideWidth * self.slidePercent / 100, 0, btn_res )
	self.slideBlock.onTouchBegan = slideBlockBeganHandler
	self.slideBlock.onTouchMove = slideBlockMoveHandler
	local size = self.slideBlock:getContentSize()	
	self.upLimit = self.initY + size.height / 2
	self.downLimit = self.initY - size.height / 2
	--self.leftLimit = self.initX  - size.width / 2
	self.leftLimit = self.initX
	--self.rightLimit = self.initX + self.slideWidth - size.width / 2
	self.rightLimit = self.initX + self.slideWidth
	self.canMove = false	
	
	self.progress:setScaleX( self.slideWidth / 2 * self.slidePercent / 100 )
end

function clsSlide:setVisible(tag)
	self._RootLayer:setVisible(tag)
end

--------------------------------------------------------

clsCustomStatusButton = clsObject:Inherit()

function clsCustomStatusButton:getSpriteObj(res)
	res = getResFilename( res )
	return CCSprite:create(res)
end

function clsCustomStatusButton:__init__(parent, x, y, btn_res_list)
	Super(clsCustomStatusButton).__init__(self)
	self._RootLayer = CCLayer:create()
	self._btn_res_list = btn_res_list
	if parent then
		parent:addChild(self)
		self:setPosition(x, y)
	end
	makeObjMouseEventHandle2(self)

	self._btn_sprite_tbl = {}
	for status, res in pairs(self._btn_res_list) do
		local btn_sprite = self:getSpriteObj(res)
		self._RootLayer:addChild(btn_sprite)
		btn_sprite:setPosition(0, 0)
		btn_sprite:setAnchorPoint(CCPointMake(0.5, 0.5)) -- 默认中心点对齐
		btn_sprite:setVisible(false)
		self._btn_sprite_tbl[status] = btn_sprite
	end

	self._cur_status = NORMAL_STATUS
	self:setStatus(self._cur_status)
end

function clsCustomStatusButton:removeFromParentAndCleanup(tag)
	-- by dda, 20130704
	--self:Destroy()
	self._RootLayer:removeFromParentAndCleanup(tag)
end

function clsCustomStatusButton:setContentSize( width, height )
	-- by dda, 20130620
	local size = self._btn_sprite_tbl[ self._cur_status ]:getContentSize()
	
	local scalex = width / size.width
	local scaley = height / size.height
	
	self._scalex = scalex
	self._scaley = scaley
		
	for status, btn_sprite in pairs(self._btn_sprite_tbl) do
		btn_sprite:setScaleX( scalex )
		btn_sprite:setScaleY( scaley )
	end
end

function clsCustomStatusButton:getContentSize()
	-- by dda, 20130620
	local btn = self._btn_sprite_tbl[ self._cur_status ]
	local size = btn:getContentSize()

	size.width = size.width * ( self._scalex or 1 )
	size.height = size.height * ( self._scaley or 1 )
	
	return size
end

function clsCustomStatusButton:getStatus()
	return self._cur_status
end

function clsCustomStatusButton:setStatus(status)
	for status, btn_sprite in pairs(self._btn_sprite_tbl) do
		btn_sprite:setVisible(false)
	end

	self._btn_sprite_tbl[status]:setVisible(true)
	self._cur_status = status
end

function clsCustomStatusButton:setDownStatus()
	-- by dda
	self:setStatus( DOWN_STATUS )
end

function clsCustomStatusButton:setNormalStatus()
	-- by dda
	self:setStatus( NORMAL_STATUS )
end

function clsCustomStatusButton:getCOObj()
	return self._RootLayer
end

function clsCustomStatusButton:setAnchorPoint(x, y)
	for status, sprite_obj in pairs(self._btn_sprite_tbl) do
		sprite_obj:setAnchorPoint(CCPointMake(x, y))
	end
	if self._str_obj then
	-- by dda, 20130718，保证文字时刻居中
	--	self._str_obj:setAnchorPoint(CCPointMake(x, y))
		local o = self._btn_sprite_tbl[self._cur_status]
		local px, py = o:getPosition()
		local size = o:getContentSize()
		local xo = px + size.width * ( 0.5 - x )
		local yo = py + size.height * ( 0.5 - y )
		self._str_obj:setPosition(xo, yo)
	end
end

-- 20130620，dda加font, fontsize参数
function clsCustomStatusButton:setString(str, font, fontsize )
	if not self._str_obj then
		self._str_obj = CCLabelTTF:create("", font or GLOBAL_FONT, fontsize or 16 )
		self._RootLayer:addChild(self._str_obj)
	end
	if not str or type(str) ~= 'string' then
		return
	else
		self._str_obj:setString(str)
	end		
end

function clsCustomStatusButton:setTextColor(r, g, b)
	if self._str_obj then
		self._str_obj:setColor(ccc3(r, g, b))
	end
end

function clsCustomStatusButton:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
	self._RootLayer:registerScriptTouchHandler(cb, multi_touch, priority, swallow_msg)
end

function clsCustomStatusButton:setTouchEnabled(tag)
	self._RootLayer:setTouchEnabled(tag)
end

function clsCustomStatusButton:onTouchDown(x, y) -- 对外重写接口
end

function clsCustomStatusButton:onTouchInMove(x, y) -- 对外重写接口 暂不使用
end

function clsCustomStatusButton:onTouchOutMove(x, y) -- 对外重写接口 暂不使用
end

function clsCustomStatusButton:onTouchUp(x, y) -- 对外重写接口
end

local function clsCustomStatusButton_CheckCaptureMsg(btn_obj, x, y)
	local node_space_point = btn_obj._RootLayer:convertToNodeSpace(CCPointMake(x, y))
	local cur_sel_sprite = btn_obj._btn_sprite_tbl[btn_obj._cur_status]
	
	-- by dda, 20130701
	--local size = cur_sel_sprite:getContentSize()
	local size = btn_obj:getContentSize()
	
	-- by dda, 20130620，加上拉伸，所以原写法有误
	--size.width = size.width * ( cur_sel_sprite._scalex or 1 )
	--size.height = size.height * ( cur_sel_sprite._scaley or 1 )
	
	local anchor_point = cur_sel_sprite:getAnchorPoint() 
	local offx, offy = anchor_point.x * size.width, anchor_point.y * size.height
	local sprite_space_x = node_space_point.x + offx
	local sprite_space_y = node_space_point.y + offy
	if sprite_space_x >= 0 and sprite_space_y >= 0 and sprite_space_x <= size.width and sprite_space_y <= size.height then
		return true, sprite_space_x, sprite_space_y
	else
		return false, nil, nil
	end
end

function clsCustomStatusButton:onDownChangeSprite() -- 内部继承切换帧接口
	--dda加
	self:setStatus(DOWN_STATUS)
end

function clsCustomStatusButton:onUpChangeSprite() -- 内部继承切换帧接口
	--dda加
	self:setStatus(NORMAL_STATUS)
end

function clsCustomStatusButton:onTouchBegan(x, y)
	local captured, sprite_space_x, sprite_space_y = clsCustomStatusButton_CheckCaptureMsg(self, x, y)
	if captured then
		--self:onDownChangeSprite()
		self:onTouchDown(sprite_space_x, sprite_space_y)
		return true
	else
		return false
	end
end

function clsCustomStatusButton:onTouchMove(x, y)
	--[[local captured, sprite_space_x, sprite_space_y = clsCustomStatusButton_CheckCaptureMsg(self, x, y)
	if captured then
		self:onTouchInMove(sprite_space_x, sprite_space_y)
	else
		self:onTouchOutMove(sprite_space_x, sprite_space_y)
	end]]
end

function clsCustomStatusButton:onTouchEnd(x, y)
	local captured, sprite_space_x, sprite_space_y = clsCustomStatusButton_CheckCaptureMsg(self, x, y)
	if captured then
		self:onTouchUp(sprite_space_x, sprite_space_y)
	end
	--self:onUpChangeSprite()
end

function clsCustomStatusButton:setPosition(x, y)
	self._RootLayer:setPosition(x, y)
end

-- 2013-07-03增加getPosition方法 by xr
function clsCustomStatusButton:getPosition()
	return self._RootLayer:getPosition()
end

function clsCustomStatusButton:release()
	self._RootLayer:removeFromParentAndCleanup(true)
end

function clsCustomStatusButton:setVisible(tag)
	self._RootLayer:setVisible(tag)
end

------------------------------------------------

clsCustomBaseSprite9Button = clsCustomStatusButton:Inherit()

function clsCustomBaseSprite9Button:__init__(parent, x, y, btn_res_list, rect, capinsets)
	self._RootLayer = CCLayer:create()
	self._btn_res_list = btn_res_list
	if parent then
		parent:addChild(self)
		self:setPosition(x, y)
	end
	makeObjMouseEventHandle2(self)

	self._btn_sprite_tbl = {}
	for status, res in pairs(self._btn_res_list) do
		local btn_sprite = self:getSpriteObj(res, rect, capinsets)
		self._RootLayer:addChild(btn_sprite)
		btn_sprite:setPosition(0, 0)
		--btn_sprite:setAnchorPoint(CCPointMake(0.5, 0.5)) -- 默认中心点对齐
		btn_sprite:setVisible(false)
		self._btn_sprite_tbl[status] = btn_sprite
	end

	self._cur_status = NORMAL_STATUS
	self:setStatus(self._cur_status)
end

function clsCustomBaseSprite9Button:getSpriteObj(res, rect, capinsets)
	res = getResFilename( res )
	return CCScale9Sprite:create(res, rect, capinsets)
end

function clsCustomBaseSprite9Button:setSpriteContentSize(width, height)
	for status, btn_sprite in pairs(self._btn_sprite_tbl) do
		btn_sprite:setContentSize(CCSizeMake(width, height))
	end
end

function clsCustomBaseSprite9Button:setContentSize(width, height)
	-- by dda, 20130630
	self:setSpriteContentSize(width, height)
end

function clsCustomBaseSprite9Button:setCapInsets(x, y, width, height)
	for status, btn_sprite in pairs(self._btn_sprite_tbl) do
		btn_sprite:setCapInsets(CCRectMake(x, y, width, height))
	end
end

------------------------------------------------------------

clsBaseSprite9Button = clsBaseButton:Inherit()

function clsBaseSprite9Button:__init__(parent, x, y, btn_res_list, rect, capinsets)
	self._RootLayer = CCLayer:create()
	self._btn_res_list = btn_res_list
	if parent then
		parent:addChild(self)
		self:setPosition(x, y)
	end
	makeObjMouseEventHandle2(self)

	self._btn_sprite_tbl = {}
	for status, res in pairs(self._btn_res_list) do
		local btn_sprite = self:getSpriteObj(res, rect, capinsets)
		self._RootLayer:addChild(btn_sprite)
		btn_sprite:setPosition(0, 0)
		--btn_sprite:setAnchorPoint(CCPointMake(0.5, 0.5)) -- 默认中心点对齐
		btn_sprite:setVisible(false)
		self._btn_sprite_tbl[status] = btn_sprite
	end

	self._cur_status = NORMAL_STATUS
	self:setStatus(self._cur_status)
end

function clsBaseSprite9Button:getSpriteObj(res, rect, capinsets)
	res = getResFilename( res )
	return CCScale9Sprite:create(res, rect, capinsets)
end

function clsBaseSprite9Button:setSpriteContentSize(width, height)
	for status, btn_sprite in pairs(self._btn_sprite_tbl) do
		btn_sprite:setContentSize(CCSizeMake(width, height))
	end
end

function clsBaseSprite9Button:setContentSize(width, height)
	-- by dda, 20130630
	self:setSpriteContentSize(width, height)
end

function clsBaseSprite9Button:setCapInsets(x, y, width, height)
	for status, btn_sprite in pairs(self._btn_sprite_tbl) do
		btn_sprite:setCapInsets(CCRectMake(x, y, width, height))
	end
end

----------------------------------------------------

clsNormalButton = clsBaseButton:Inherit()

function clsNormalButton:__init__(parent, x, y, btn_res_list)
	Super(clsNormalButton).__init__(self, parent, x, y, btn_res_list)
end

function clsNormalButton:onDownChangeSprite()
	self:setStatus(DOWN_STATUS)
end

function clsNormalButton:onUpChangeSprite()
	self:setStatus(NORMAL_STATUS)
end

-------------------------------------------------------------
clsLabelAtlas = clsObject:Inherit()

function clsLabelAtlas:__init__(parent, x, y, file, text,width,height,startCharMap )
	file = getResFilename( file )
	Super(clsLabelAtlas).__init__(self)
	self._RootLabelAtlas = CCLabelAtlas:create(text, file, width, height,startCharMap)
	if parent then
		parent:addChild(self)
		self._RootLabelAtlas:setPosition(x, y)
	end
end

function clsLabelAtlas:getCOObj()
	return self._RootLabelAtlas
end

function clsLabelAtlas:setVisible(tag)
	self._RootLabelAtlas:setVisible(tag)
end

function clsLabelAtlas:setString(value)
	if not value or type(value) ~= 'string' then
		return
	else
		self._RootLabelAtlas:setString(value)
	end			
end

function clsLabelAtlas:getString()
	return self._RootLabelAtlas:getString()
end

function clsLabelAtlas:setPosition(x, y)
	self._RootLabelAtlas:setPosition(x, y)
end

function clsLabelAtlas:getPosition()
	return self._RootLabelAtlas:getPosition()
end

function clsLabelAtlas:getContentSize()
	return self._RootLabelAtlas:getContentSize()
end

function clsLabelAtlas:setAnchorPoint(x, y)
	self._RootLabelAtlas:setAnchorPoint(CCPointMake(x, y))
end

function clsLabelAtlas:removeFromParentAndCleanup(tag)
	self._RootLabelAtlas:removeFromParentAndCleanup(tag)
end
--------------------------------------------------------------------
--通用表格控件，生成一行的表格数据
clsTr = clsObject:Inherit()
--clsTr = {}

function clsTr:__init__( config )
	self._RootNode = createNode( nil, {} ) --CCNode:create()
	self._config = config
	
	local c = BASE_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
	local o = createFrameButton( self._RootNode, c.list_button )
	o:hideLine(0)
	
	self._btn = o
end

function clsTr:getCOObj()
	return self._RootNode
end

function clsTr:setAnchorPoint(x, y)
	self._RootNode:setAnchorPoint(CCPointMake(x, y))
end

function clsTr:setPosition(x, y)
	self._RootNode:setPosition(x, y)
end
	
function clsTr:setVisible(tag)
	self._RootNode:setVisible(tag)
end

function clsTr:getPosition()
	return self._RootNode:getPosition()
end

function clsTr:setMsgDelegate( move_obj, onclick )
	setMsgDelegate( self._btn, move_obj, onclick )
end

--[[function clsTr:getContentSize()
	local size = self._RootNode:getContentSize()
	
	return size
end--]]

--[[
设置表格高度
参数：
	id：参照位置的id
	offset：参照位置的底边距表格底边的距离
]]
function clsTr:setHeight( id, offset )
	local base = BASE_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
	
	local to = PAGE[ id ]
	
	if not to then
		return
	end
--	d(to)
	
	local toy = to.y or 0
	
	local cty = 0
	
	local toay = to.ay or 0.5
	
	local tosy = to.sy
	
	local y0 = toy - toay * tosy
	local height = offset - y0
	
	local function getContentSize(node)
		return { width = base.list_button.sx, height = height,}
	end
	self._RootNode.getContentSize = getContentSize
	
	self._btn:setPixelScale( base.list_button.sx, height )
	
	local c2 = base.list_line
	c2.y = 7 - height
	createSprite( self._RootNode, c2 )
end

function clsTr:removeFromParentAndCleanup(tag)
    if self._RootNode then
	self._RootNode:removeFromParentAndCleanup(tag)
	end
end

function clsTr:addChild(child)
	self._RootNode:addChild(child:getCOObj())
	table.insert(self._son_list, child)
end

function clsTr:addLabel( section, value )
	section = self._config[ section ]
	
	if value then
		section[ 'text' ] = value
	end
	
	return createLabel( self._RootNode, section )
end

function clsTr:addSprite( section, value, callback )
	section = self._config[ section ]
	
	if value then
		section[ 'res' ] = value
	end
	
	return createSprite( self._RootNode, section, callback )
end

function clsTr:addMultiLabel( section, value )
	section = self._config[ section ]
	
	if value then
		section[ 'text' ] = value
	end
	
	return createMultiLabel( self._RootNode, section )
end
--------------------------------------------------------------------

