-- player info

local playerInfoPage = nil

PLAYER_LAYOUT = Import("layout/player_info.lua")
local PlayerConfig = PLAYER_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
local cur_dynamic_page = 1
local max_dynamic_page = 10
local max_pic = 4
local is_friend = false
local is_challenge = false --是否正在斗骰
local challenge_data = nil --挑战房间数据

--一对一聊天
ONETOONE_CHAT_PANEL = Import("lua/one_to_one_chat.lua")

local dynamic_data = {}
--[[
type
user
id

data
""
or
score
user
id
url
name

logo
item
vip_id
sex
name
is_real
time
--]]

local battle_rec_info = {}
--[[
total_lose
total_win
win_goad
goad
integral_score
rich
--]]

local small_pic_info = {}
function getSmallPicData()
	return small_pic_info
end

function getSmallPicInfo(player_id)
	return small_pic_info[player_id]
end

--[[
id
url
comment_times
vote_times
--]]

local all_player_info = {} 
--[[
nick,
charm,
is_real
image
city
logo
lost
degree
challenge_memo
no
age
name
sex
height
bar
confession
win
challenge_gold
vip_expire
income
profession
star
province
id
vip_name
profit
vip_id
marriage
hide_age
birthday
gold
frendship
mood
interest
sum
--]]

local playerId = nil
function getPlayerInfo(user_id)
	return all_player_info[user_id]
end

function setPlayerInfo(user_id, player_info)
	all_player_info[user_id] = player_info
end

local info_desc_info = {
	item_width = 480,
	item_height = 25,
	column_cnt = 1,
	x_space = 20,
	y_space = 40,
}

local function createPlayerSimpleInfo(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)
	local config = PlayerConfig["simple_info_config"]

	ret_panel._top_bg_pic = LIGHT_UI.clsSprite:New(ret_panel, -20, 15, "tiny_black_pixel.png")
	ret_panel._top_bg_pic:setAnchorPoint(0, 1)
	ret_panel._top_bg_pic:setScaleX(480)
	ret_panel._top_bg_pic:setScaleY(140)

	ret_panel._bg_pic = LIGHT_UI.clsSprite:New(ret_panel, -20, -125, "tiny_bg_pixel.png")
	ret_panel._bg_pic:setAnchorPoint(0, 1)
	ret_panel._bg_pic:setScaleX(480)
	ret_panel._bg_pic:setScaleY(1100)

	ret_panel._user_pic = createSprite( ret_panel, point( 50, -55 ) )   --头像位置
	ret_panel._user_pic:setVisible(false)

	ret_panel._remark_txt = LIGHT_UI.clsMultiLabel:New(ret_panel, 130, -35, "", GLOBAL_FONT, 20, 320, 1)
	ret_panel._remark_txt:setVisible(false)

	ret_panel._photo_txt = LIGHT_UI.clsLabel:New(ret_panel, -2, -142, "相册", GLOBAL_FONT, 16)
	ret_panel._photo_txt:setAnchorPoint(0, 1)
	ret_panel._photo_txt:setTextColor(153, 153, 153)

	ret_panel._photo_hint = LIGHT_UI.clsLabel:New(ret_panel, 160, -142, " ", GLOBAL_FONT, 16)
	ret_panel._photo_hint:setAnchorPoint(0, 1)
	ret_panel._photo_hint:setTextColor(153, 153, 153)

	ret_panel._album_pic_list = {}  --相册列表调整位置
	local startx = 46 --起始位置
	local bg_startx = -6
	local starty = -223
	local bg_starty = -172
	local x_step = 115 --图片间距
	local bg_x_step = 115 --图片间距
	local temp_bg_pic = nil
	ret_panel._photo_array = {}
	for i = 1, max_pic do
		temp_bg_pic = LIGHT_UI.clsSprite:New(ret_panel, bg_startx, bg_starty, "tiny_bg_white_pixel.png")
		temp_bg_pic:setScaleX(104)
		temp_bg_pic:setScaleY(102)
		temp_bg_pic:setAnchorPoint(0, 1)
		ret_panel._photo_array[i] = temp_bg_pic
		bg_startx = bg_startx + bg_x_step

		local pic_obj = createSprite(ret_panel, { x = startx, y = starty, sx = 100, sy = 100 } )
		pic_obj._msg_layer = LIGHT_UI.clsClipLayer:New(pic_obj, 0, -140)
		pic_obj._msg_layer:set_msg_rect(0, 0, 140, 140)
		table.insert(ret_panel._album_pic_list, pic_obj)
		startx = startx + x_step
	end

	starty = starty - 75 + 10

	ret_panel._base_info_title = LIGHT_UI.clsLabel:New(ret_panel, -2, starty + 3, "基本信息", GLOBAL_FONT, 16)
	ret_panel._base_info_title:setAnchorPoint(0, 1)
	ret_panel._base_info_title:setTextColor(153, 153, 153)

	startx = 20
	starty = starty - 30
	local y_step = -60
	local x_gap = 400 

	ret_panel._base_info_bg = LIGHT_UI.clsSprite:New(ret_panel, startx - 23, starty + 5, "grxxlb.png")
	ret_panel._base_info_bg:setAnchorPoint(0, 1)

	starty = starty - 20

	ret_panel._sex_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty+7, "性别", GLOBAL_FONT, 22)
	ret_panel._sex_title:setAnchorPoint(0, 1)
	ret_panel._sex_title:setTextColor(153, 153, 153)
	ret_panel._sex_value = LIGHT_UI.clsSprite:New(ret_panel, startx + x_gap - 10, starty -7, "man.png")
	ret_panel._sex_value:setAnchorPoint(1, 1)
	ret_panel._sex_value:setVisible(false)
	starty = starty + y_step

	ret_panel._age_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "年龄", GLOBAL_FONT, 22)
	ret_panel._age_title:setAnchorPoint(0, 1)
	ret_panel._age_title:setTextColor(153, 153, 153)
	ret_panel._age_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, "", GLOBAL_FONT, 22)
	ret_panel._age_value:setAnchorPoint(1, 1)
	ret_panel._age_value:setTextColor(153, 153, 153)
	ret_panel._age_value:setVisible(false)
	starty = starty + y_step

	ret_panel._charm_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "魅力", GLOBAL_FONT, 22)
	ret_panel._charm_title:setAnchorPoint(0, 1)
	ret_panel._charm_title:setTextColor(153, 153, 153)
	ret_panel._charm_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, "", GLOBAL_FONT, 22)
	ret_panel._charm_value:setAnchorPoint(1, 1)
	ret_panel._charm_value:setTextColor(153, 153, 153)
	ret_panel._charm_value:setVisible(false)
	starty = starty + y_step

	ret_panel._city_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "城市", GLOBAL_FONT, 22)
	ret_panel._city_title:setAnchorPoint(0, 1)
	ret_panel._city_title:setTextColor(153, 153, 153)
	ret_panel._city_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, "", GLOBAL_FONT, 22)
	ret_panel._city_value:setAnchorPoint(1, 1)
	ret_panel._city_value:setTextColor(153, 153, 153)
	ret_panel._city_value:setVisible(false)
	starty = starty + y_step - 10

	ret_panel._height_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "身高", GLOBAL_FONT, 22)
	ret_panel._height_title:setAnchorPoint(0, 1)
	ret_panel._height_title:setTextColor(153, 153, 153)
	ret_panel._height_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, "", GLOBAL_FONT, 22)
	ret_panel._height_value:setAnchorPoint(1, 1)
	ret_panel._height_value:setTextColor(153, 153, 153)
	ret_panel._height_value:setVisible(false)
	starty = starty + y_step

	ret_panel._birthday_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "生日", GLOBAL_FONT, 22)
	ret_panel._birthday_title:setAnchorPoint(0, 1)
	ret_panel._birthday_title:setTextColor(153, 153, 153)
	ret_panel._birthday_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, "", GLOBAL_FONT, 22)
	ret_panel._birthday_value:setAnchorPoint(1, 1)
	ret_panel._birthday_value:setVisible(false)
	ret_panel._birthday_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	ret_panel._income_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "收入", GLOBAL_FONT, 22)
	ret_panel._income_title:setAnchorPoint(0, 1)
	ret_panel._income_title:setTextColor(153, 153, 153)
	ret_panel._income_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, "", GLOBAL_FONT, 22)
	ret_panel._income_value:setAnchorPoint(1, 1)
	ret_panel._income_value:setVisible(false)
	ret_panel._income_value:setTextColor(153, 153, 153)
	starty = starty + y_step - 10

	ret_panel._hobby_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "兴趣", GLOBAL_FONT, 22)
	ret_panel._hobby_title:setAnchorPoint(0, 1)
	ret_panel._hobby_title:setTextColor(153, 153, 153)
	ret_panel._hobby_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, "", GLOBAL_FONT, 22)
	ret_panel._hobby_value:setAnchorPoint(1, 1)
	ret_panel._hobby_value:setVisible(false)
	ret_panel._hobby_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	ret_panel._profession_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "职业", GLOBAL_FONT, 22)
	ret_panel._profession_title:setAnchorPoint(0, 1)
	ret_panel._profession_title:setTextColor(153, 153, 153)
	ret_panel._profession_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, "", GLOBAL_FONT, 22)
	ret_panel._profession_value:setAnchorPoint(1, 1)
	ret_panel._profession_value:setVisible(false)
	ret_panel._profession_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	ret_panel._sina_bind_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "微博绑定", GLOBAL_FONT, 22)
	ret_panel._sina_bind_title:setAnchorPoint(0, 1)
	ret_panel._sina_bind_title:setTextColor(153, 153, 153)
	ret_panel._sina_bind_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, "", GLOBAL_FONT, 22)
	ret_panel._sina_bind_value:setVisible(false)
	ret_panel._sina_bind_value:setAnchorPoint(1, 1)
	ret_panel._sina_bind_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	ret_panel._club_title = LIGHT_UI.clsLabel:New(ret_panel, startx, starty, "夜店", GLOBAL_FONT, 22)
	ret_panel._club_title:setAnchorPoint(0, 1)
	ret_panel._club_title:setTextColor(153, 153, 153)
	ret_panel._club_value = LIGHT_UI.clsLabel:New(ret_panel, startx + x_gap, starty, "", GLOBAL_FONT, 22)
	ret_panel._club_value:setVisible(false)
	ret_panel._club_value:setAnchorPoint(1, 1)
	ret_panel._club_value:setTextColor(153, 153, 153)
	starty = starty + y_step

	local function getContentSize(panel)
		return { ["width"] = 440, ["height"] = 1020, }
	end

	ret_panel.getContentSize = getContentSize
	return ret_panel
end

local function createBattleRecord(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	createSprite( ret_panel, PlayerConfig.battle_bg )
	
	
	create9Sprite( ret_panel, PlayerConfig.today_rect )
	createLabel( ret_panel, PlayerConfig.today_left )
	ret_panel._today_right_txt = createLabel( ret_panel, PlayerConfig.today_right_txt )
	--createSprite( ret_panel, PlayerConfig.today_right_pic )

	create9Sprite( ret_panel, PlayerConfig.wealth_rect )
	createLabel( ret_panel, PlayerConfig.wealth_left )
	ret_panel._wealth_right_txt = createLabel( ret_panel, PlayerConfig.wealth_right_txt )


	create9Sprite( ret_panel, PlayerConfig.battle_rect )
	createLabel( ret_panel, PlayerConfig.battle_left )
	ret_panel._battle_right_txt = createLabel( ret_panel, PlayerConfig.battle_right_txt )

	local function getContentSize(panel)
		return { ["width"] = 480, ["height"] = 200, }
	end

	ret_panel.getContentSize = getContentSize

	return ret_panel
end

local function createDynamicItemByInfo( info ) --创建动态
	local ret_panel = LIGHT_UI.clsNode:New( nil, 0, 0 )

	PAGE[ 'xyz' ] = {
			x = 0,
			y = 0,
			sx = 480,
			sy = 0,
			ax = 0,
			ay = 1,
		}

	local height = 0
	local content_obj = nil

	PlayerConfig.dynamic_time_c.text = info.time
	--PlayerConfig.dynamic_time_c.y = -1 * size.height
	local time_obj = createLabel( ret_panel, PlayerConfig.dynamic_time_c )

	if type( info.data) == "string" then
		PlayerConfig.dynamic_content_c.text = clearHTML( info.data )
		content_obj = createMultiLabel( ret_panel, PlayerConfig.dynamic_content_c )
	end
	
	if info.type == "7" then
		PlayerConfig.dynamic_content_c.text = "给" .. info.data.name .. "打了" .. info.data.score .. "分"
		content_obj = createMultiLabel( ret_panel, PlayerConfig.dynamic_content_c )

		PlayerConfig.pic_c.res = info.data.url
		createSprite( ret_panel, PlayerConfig.pic_c )
		height = 150
	end

	if not content_obj then
		PlayerConfig.dynamic_content_c.text = ''
		content_obj = createMultiLabel( ret_panel, PlayerConfig.dynamic_content_c )
	end
	local size = content_obj:getContentSize()	

	if height == 0 then
		height = math.abs( PlayerConfig.dynamic_content_c.y ) + size.height
	end

	height = height + PlayerConfig["simple_info_config"]["dynamic_info_margin"]

	PlayerConfig.dynamic_bg_c.sy = height
	create9Sprite( ret_panel, PlayerConfig.dynamic_bg_c )

	local function getContentSize( node )
		return { ["width"] = 480, ["height"] = height, }
	end

	ret_panel.getContentSize = getContentSize

	return ret_panel
end

local function configDynamicInfo(user_id)
	local news_layout = playerInfoPage._move_info_page._news_layout
	news_layout:clearAllItem()

	local dynamic_info_list = dynamic_data[user_id]
	for _, dynamic_info in pairs(dynamic_info_list) do
		news_layout:append_item(createDynamicItemByInfo(dynamic_info))
	end

	news_layout:refresh_view()
end

local function on_get_dynamic_info(data)
	if playerInfoPage then
		local user_id = playerInfoPage._user_id		
		dynamic_data[user_id] = data.data
		if table.getn( dynamic_data[user_id] ) > 0 then configDynamicInfo(user_id) end
	end
end

local function request_dynamic_info(user_id, page)
	if not page then local page = 1 end
	doCommand( 'user', 'moving', { kind = 3, page = page, size = 20, uid = user_id, version=1 }, on_get_dynamic_info )
end

local function createMoveInfoPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)
	local config = PlayerConfig["move_page_config"]

	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_info_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config.view_height)
	ret_panel._move_info_grp:setVMovable(true)

	local player_simple_info = createPlayerSimpleInfo(nil, 0, 0)

	local news_layout = LIGHT_UI.clsRowLayout:New(nil, 0, 0, info_desc_info)
	news_layout._use_dynamic_size = true 
	news_layout:refresh_view()

	local function onAOverTop(layout)
		news_layout:clearAllItem()
		cur_dynamic_page = 1
		request_dynamic_info(playerInfoPage._user_id,cur_dynamic_page)
	end

	local function onAOverBottom(layout)
		if cur_dynamic_page < max_dynamic_page then
			cur_dynamic_page = cur_dynamic_page + 1
			request_dynamic_info(playerInfoPage._user_id,cur_dynamic_page)
		end
	end

	news_layout.onHMoveOverTop = onAOverTop
	news_layout.onHMoveOverBottom = onAOverBottom

	local battle_record = createBattleRecord(nil, 0, 0)

	local function get_y_limit(obj)
		return config.inner_y 
	end

	player_simple_info:setPosition(0, config.inner_y)
	ret_panel._move_info_grp:appendItem(player_simple_info)
	news_layout:setPosition(0, config.inner_y)
	ret_panel._move_info_grp:appendItem(news_layout)
	battle_record:setPosition(0, config.inner_y)
	ret_panel._move_info_grp:appendItem(battle_record)
	ret_panel._move_info_grp:selectPage(1)

	player_simple_info.getYLimit = get_y_limit
	news_layout.getYLimit = get_y_limit
	battle_record.getYLimit = get_y_limit

	ret_panel._player_simple_info = player_simple_info
	ret_panel._news_layout = news_layout 
	ret_panel._battle_record = battle_record 
	
	return ret_panel
end

local MALL_PAGE = Import("lua/mall.lua")

local function on_add_friend(data)
	dailyCount('PlayerInfoCare')--关注
	if not playerInfoPage then
		return
	end
	playerInfoPage._bottom_panel._focus_btn:setClickStatus()
	showMessage( playerInfoPage, '成功关注' )
	is_friend = true
end

local function on_del_friend(data)
		dailyCount('PlayerInfoUnCare')--取消关注
	playerInfoPage._bottom_panel._focus_btn:setUnClickStatus()
	showMessage( playerInfoPage, '取消关注' )
	is_friend = false
end

local function createBottomPanel(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	ret_panel._bottom_pic = LIGHT_UI.clsSprite:New(ret_panel, 0, 0, "mmfooter_bg.png")
	ret_panel._bottom_pic:setAnchorPoint(0, 0)

	local align_tbl = {}
	local function onGift(btn, x, y)
		btn:setClickStatus()
		if playerInfoPage._user_id then
			
			local function back_func()
				MALL_PAGE.closeMallPagePanel()
				showByUserId( playerId )
			end
			dailyCount('PlayerInfoGift')--送礼

			playerId = playerInfoPage._user_id
			MALL_PAGE.showMallPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, back_func, playerInfoPage._user_id, true )
			closePlayerInfoPagePanel()
		end
	end
	ret_panel._gift_page_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, "toosbar_gift_bt_bg.png", "toosbar_gift_bt_bg.png") 
	ret_panel._gift_page_btn:setAnchorPoint(0, 0)
	ret_panel._gift_page_btn:setString("")  --送礼修改地方
	ret_panel._gift_page_btn.onTouchEnd = onGift
	table.insert(align_tbl, ret_panel._gift_page_btn)

	local function onChat(btn, x, y)
		dailyCount('PlayerInfoChat')--一对一聊天
		btn:setClickStatus()
		--增加一对一聊天功能实现
		if playerInfoPage._user_id then
			--local player_info = getPlayerInfo(playerInfoPage._user_id)
			local player_userid = playerInfoPage._user_id
			local player_info = all_player_info[player_userid]
			--if player_info then

				local function back_func()
					ONETOONE_CHAT_PANEL.closeOneToOneChatPagePanel()
					showByUserId( playerId )
				end

				
				playerId = playerInfoPage._user_id
			    if player_info then	
				ONETOONE_CHAT_PANEL.init( { uid2 = player_userid, name2 = player_info.name, back_func = back_func } )
	            end
				ONETOONE_CHAT_PANEL.showPanel(HTTP_CLIENT.getRootNode(), 0, 0 )
				closePlayerInfoPagePanel()
			--end
		end			
	end
	ret_panel._chat_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, "toosbar_chat_bt_bg.png", "toosbar_chat_bt_bg.png") 
	ret_panel._chat_btn:setAnchorPoint(0, 0)
	ret_panel._chat_btn:setString("")  --聊天修改地方	
	ret_panel._chat_btn.onTouchEnd = onChat
	table.insert(align_tbl, ret_panel._chat_btn)

	
	local function onChallenge(btn, x, y)
		dailyCount('PlayerInfoDuel')--斗骰
		if is_challenge == false then
			btn:setClickStatus()
		else
			btn:setUnClickStatus()
			local ROOMMatch = Import("lua/room.lua")
			ROOMMatch.doQuickGame(GLOBAL.gold,challenge_data.ip,challenge_data.port,
			                      challenge_data.id,challenge_data.no)
			closePlayerInfoPagePanel()
		end
		--[[
		if playerInfoPage._user_id then
			local ROOM_PANEL = Import("lua/room.lua")
			ROOM_PANEL.showPanel(HTTP_CLIENT.getRootNode(), 0, 0, true)		--这个面板不销毁，到时返回时只需要将现在这个新建的面板销毁即可
		end
		]]
	end
	
	local function challengeHandler( btn, x, y )
		local function onUserMoney( data )
			if data.data.drunk >= 3 then
				local function okHandler()
					setExsitAlert( false )
					local function onPropsUses( data )
						if data.code == 1 then
							showMessage( ret_panel, data.memo )	
							onChallenge( btn, x, y )
						else
							showMessage( ret_panel, data.memo )
						end			
					end
					doCommand( 'props', 'uses', { props = 50, quantity = 1 }, onPropsUses )
				end
				createAlert( ret_panel, PlayerConfig.enter_game_alert, okHandler )
			else
				onChallenge( btn, x, y )
			end
		end
		doCommand( 'user', 'money', {}, onUserMoney )
	end
	--兼容1.0版
	if isFileExists(getResPath()..'dis_toosbar_challenge_bt_bg.png') == true then
		
		ret_panel._challenge_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, "toosbar_challenge_bt_bg.png", "dis_toosbar_challenge_bt_bg.png") 
		ret_panel._challenge_btn:setAnchorPoint(0, 0)
		ret_panel._challenge_btn:setString("") --斗骰修改地方
		ret_panel._challenge_btn.onTouchEnd = challengeHandler
		table.insert(align_tbl, ret_panel._challenge_btn)
	end
	

	local function onFocus(btn, x, y)
		if is_friend then
		doCommand( 'user', 'delfriend', { friend = playerInfoPage._user_id }, on_del_friend )	
		else
			doCommand( 'user', 'addfriend', { friend = playerInfoPage._user_id }, on_add_friend )			
		end
	    
		btn:setClickStatus()
	--发送三条消息给它- - 
	--1.邀请视频认证
	--2.邀请上传照片
	--3.邀请完善资料
			
	end

	local focus_pic_1 = "focus_pic.png"
	local focus_pic_2 = "focus_already_pic.png"
	ret_panel._focus_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, focus_pic_1, focus_pic_2) 
	ret_panel._focus_btn:setAnchorPoint(0, 0)
	ret_panel._focus_btn:setString("")  --关注修改的地方
	ret_panel._focus_btn.onTouchEnd = onFocus
	table.insert(align_tbl, ret_panel._focus_btn)

	local function onMore(btn, x, y)
		btn:setClickStatus()
	end
	ret_panel._more_btn = LIGHT_UI.clsStatusButton:New(ret_panel, 0, 5, "toosbar_more_bt_bg.png", "toosbar_more_bt_bg.png") 
	ret_panel._more_btn:setAnchorPoint(0, 0)
	ret_panel._more_btn:setString("") --更多修改的地方
	--ret_panel._more_btn.onTouchEnd = onFocus
	ret_panel._more_btn.onTouchEnd = onFocus
	ret_panel._more_btn:setVisible( false )
	--table.insert(align_tbl, ret_panel._more_btn)

	LIGHT_UI.alignXCenterForObjList(align_tbl, 60)

	return ret_panel
end

local back_func = nil

function init( data )
	uid = GLOBAL.uid
	
	if data.back_func then
		back_func = data.back_func
	end
end	

local function createPlayerInfoPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = PlayerConfig, nil
	local o = nil

	local function on_back(btn)
		closePlayerInfoPagePanel()
		
		if back_func then
			back_func()
		else
		    --把原来跳回偶遇主页给弄到跑回主面板了
			--CHALLENGE_PANEL.showChallengePagePanel(HTTP_CLIENT.getRootNode(), 0, 0) 		
			--MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
			if PANEL_CONTAINER.closeChildPanel( nil, 15 ) then
				local SETTING = Import("lua/setting.lua")							
				PANEL_CONTAINER.addChild( SETTING.showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
			end	
		end
	end

	ret_panel._onBack = on_back
	--ret_panel._head_text:setString( '个人资料' )
		
	config = PlayerConfig["move_page_config"]
	ret_panel._move_info_page = createMoveInfoPage(ret_panel, config.x, config.y)
	
	local function on_sel_page(move_obj)
		ret_panel._move_hint:selItem(move_obj:getCurPage())
	end

	ret_panel._move_info_page._move_info_grp.onSelPage = on_sel_page

	local item_list = {
		[1] = {
			["txt"] = "资料",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
		[2] = {
			["txt"] = "动态",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
		[3] = {
			["txt"] = "战绩",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 22,
		},
	}

	config = PlayerConfig["move_hint_config"]
	local total_width = 480
	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, config.x, config.y, "tab_bg_image.png", item_list, total_width, config.res, "tiny_black_pixel.png")
	local bg = ret_panel._move_hint:getBGObj()
	bg:setScaleX(total_width)
	ret_panel._move_hint:selItem(1)

	local function on_click( obj, idx )
		playerInfoPage._move_hint:selItem( idx )		
		playerInfoPage._move_info_page._move_info_grp:selectPage( idx )
	end

	ret_panel._move_hint.onClick = on_click
	ret_panel._bottom_panel = createBottomPanel(ret_panel, 0, 0)


	--重新载入头部，防止“穿越”
	local BASE_LAYOUT = Import("layout/base.lua")
	local c = BASE_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
	
	local c2 = nil
	local o = nil
	
	ret_panel._head_bg = createSprite( ret_panel, c.head_bg )

	ret_panel._head_back = createButton( ret_panel, c.head_back, on_back )
	
	c2 = c.head_text
	c2.text = ' '
	ret_panel.name = createLabel( ret_panel, c2 )
	
	 

	return ret_panel
end

function showPlayerInfoPagePanel(parent, x, y)
	dailyCount('PlayerInfo')--他人资料
	--if not playerInfoPage then
		playerInfoPage = createPlayerInfoPagePanel(parent, x, y)
	--else
		--playerInfoPage:setVisible(true)
	--end
end

local PLAYER_ALBUM_PAGE = Import("lua/player_album.lua")
local CHALLENGE_PAGE = Import("lua/challenge.lua")

local function configSmallPic(player_id)
	if not playerInfoPage then
		return
	end
	local small_pic_data = small_pic_info[player_id]
	
	local album_list = playerInfoPage._move_info_page._player_simple_info._album_pic_list

	local function on_pic_click(msg_obj, x, y)
		closePlayerInfoPagePanel()
		dailyCount('PlayerInfoPhoto')--玩家相册
		PLAYER_ALBUM_PAGE.showAlbumPagePanel(HTTP_CLIENT.getRootNode(), 0, 0, player_id)
	end

	local function on_no_pic_click(msg_obj, x, y)
		print("kk")
	end

	local idx = 1
	for _, album_info in ipairs(small_pic_data) do
		if idx > max_pic then
			break
		end
		local pic_obj = album_list[idx]
		pic_obj:setVisible(true)
		pic_obj:setImage(album_info["url"])
		playerInfoPage._move_info_page._player_simple_info._photo_array[idx]:setVisible(true)
		idx = idx + 1
		CHALLENGE_PAGE.setMsgDelegate(pic_obj._msg_layer, playerInfoPage._move_info_page._move_info_grp, on_pic_click)
	end
	if idx <= max_pic then
		for i = idx, max_pic do
			local pic_obj = album_list[i]
			pic_obj:setVisible(false)
			playerInfoPage._move_info_page._player_simple_info._photo_array[i]:setVisible(false)
		end
	end
end

local function request_small_pic(player_id)
	local function on_get_small_pic(data)
		small_pic_info[player_id] = data.data.data
		configSmallPic(player_id)
	end

	doCommand( 'album', 'small', { user = player_id, page = 1, kind = 1 }, on_get_small_pic )
end

local function configSimpleInfo(user_id)
	if not playerInfoPage then
		return
	end
	local player_info = all_player_info[user_id]
	local simple_info_panel = playerInfoPage._move_info_page._player_simple_info
	if not LIGHT_UI.isPNGURL(player_info["logo"]) then	
		simple_info_panel._user_pic:setImage(player_info["logo"])
		simple_info_panel._user_pic:setVisible(true)
	end

	local vip_pic_x = 128

	if player_info["vip_id"] ~= "0" then
		local vip_logo = string.format( 'vip%i.png', player_info["vip_id"] )
		simple_info_panel._vip_pic = LIGHT_UI.clsSprite:New(simple_info_panel, vip_pic_x, 0, vip_logo)
		simple_info_panel._vip_pic:setAnchorPoint(0, 1)
	else
		if simple_info_panel._vip_pic then simple_info_panel._vip_pic:setVisible(false) end
	end

	if player_info["is_real"] == "0" then
		simple_info_panel._photo_hint:setString( "还未视频认证" )
		if simple_info_panel._video_auth then simple_info_panel._video_auth:setVisible(false) end
	else
		simple_info_panel._photo_hint:setString( " " )
		
		if player_info["vip_id"] ~= "0" then
			vip_pic_x = vip_pic_x + 60			
		end

		simple_info_panel._video_auth = LIGHT_UI.clsSprite:New( simple_info_panel, vip_pic_x, 5, "vedioauth_pic.png" )
		simple_info_panel._video_auth:setAnchorPoint( 0, 1 )
	end					
	playerInfoPage.name:setString( player_info["name"] )
	
	
	--控制显示用户心情显示字数
	local str = getCharString(player_info["mood"],200);	
	local count = (#str);	
	if count > 167 then
	str = getCharString(player_info["mood"],46) .. "..."
	end		
	simple_info_panel._remark_txt:setString(str)
	simple_info_panel._remark_txt:setVisible(true)
	
	if player_info["sex"] == "1" then 
		simple_info_panel._sex_value:setImage("woman.png")
		else
		simple_info_panel._sex_value:setImage("man.png")
	 end
	
	simple_info_panel._sex_value:setVisible(true)

	--[[simple_info_panel._name_value:setString(player_info["name"])
	simple_info_panel._name_value:setVisible(true)
	ret_panel._head_text:setString( player_info["name"] )--]]
	
	simple_info_panel._age_value:setString(player_info["age"])
	simple_info_panel._age_value:setVisible(true)

	simple_info_panel._charm_value:setString(player_info["charm"])
	simple_info_panel._charm_value:setVisible(true)

	simple_info_panel._city_value:setString(player_info["city"])
	simple_info_panel._city_value:setVisible(true)

	simple_info_panel._height_value:setString(player_info["height"])
	simple_info_panel._height_value:setVisible(true)

	simple_info_panel._birthday_value:setString(player_info["birthday"])
	simple_info_panel._birthday_value:setVisible(true)

	local incomeTxt = getCharString( player_info["income"], 10 )
	simple_info_panel._income_value:setString( incomeTxt )
	simple_info_panel._income_value:setVisible(true)

	simple_info_panel._hobby_value:setString(player_info["interest"])
	simple_info_panel._hobby_value:setVisible(true)
	
	local careerTxt = getCharString( player_info["profession"], 10 )
	simple_info_panel._profession_value:setString( careerTxt )
	simple_info_panel._profession_value:setVisible(true)

	local barTxt = getCharString( player_info["bar"], 10 )
	simple_info_panel._club_value:setString( barTxt )
	simple_info_panel._club_value:setVisible(true)

	if tonumber(player_info["frendship"]) == 1 then
		is_friend = true
		playerInfoPage._bottom_panel._focus_btn:setClickStatus()
	else
		is_friend = false
		playerInfoPage._bottom_panel._focus_btn:setUnClickStatus()
	end
	
	--挑战
	if player_info['roomstate'] then
		challenge_data = {}
		challenge_data.ip = player_info['roomstate'].ip
		challenge_data.port = player_info['roomstate'].port
		challenge_data.id = player_info['roomstate'].id
		challenge_data.no = player_info['roomstate'].no
		
		is_challenge = true
		if playerInfoPage._bottom_panel._challenge_btn then
			playerInfoPage._bottom_panel._challenge_btn:setUnClickStatus()
		end
	else
		challenge_data = nil
		is_challenge = false
		if playerInfoPage._bottom_panel._challenge_btn then
			playerInfoPage._bottom_panel._challenge_btn:setClickStatus()
		end
	end

	local player_id = player_info["id"]
	if not small_pic_info[player_id] then
		request_small_pic(player_id)
	else
		configSmallPic(player_id)
	end
end

local function configBattleInfo(user_id)	
	local battle_rec_page = playerInfoPage._move_info_page._battle_record	
	if battle_rec_page._today_right_pic then battle_rec_page._today_right_pic:setVisible( false ) end
	local battle_rec_data = battle_rec_info[user_id]	
	battle_rec_page._today_right_txt:setString( ( battle_rec_data["profit"] ) )	
	local size = battle_rec_page._today_right_txt:getContentSize()
	local default_x = PlayerConfig.today_right_pic.x
	PlayerConfig.today_right_pic.x = PlayerConfig.today_right_pic.x - size.width
	--battle_rec_page._today_right_pic = createSprite( battle_rec_page, PlayerConfig.today_right_pic )
	PlayerConfig.today_right_pic.x = default_x	
	battle_rec_page._wealth_right_txt:setString( clearHTML( battle_rec_data["rich"] ) )	
	battle_rec_page._battle_right_txt:setString( ( battle_rec_data["win"] ) .. '胜 / ' .. ( battle_rec_data["lose"] ) .. '负' )
	
	return battle_rec_page
end

local function configPanelByData(user_id)
	configSimpleInfo(user_id)
	configDynamicInfo(user_id)
	configBattleInfo(user_id)
end

local function on_get_user_info(data)
	local user_id = data.data.id
	all_player_info[user_id] = data.data
	configSimpleInfo(user_id)
end

function request_player_info(user_id, cb)
	doCommand( 'user', 'detail', { id = user_id }, cb,60*60,{loading = 0} )
end

local userId = nil
local function on_get_battle_rec(data)
	if  playerInfoPage then 
		local user_id = userId
		battle_rec_info[user_id] = data.data
		configBattleInfo(user_id)
	end		
end

local function request_battle_rec(user_id)
	doCommand( 'user', 'score', { uid = user_id }, on_get_battle_rec )
end

function showByUserId(user_id, backCall )
	--if user_id == 
	if backCall then
		back_func = backCall
	end
	showPlayerInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)	
	playerInfoPage._user_id = user_id
	userId = user_id
	--if not all_player_info[user_id] then
		request_player_info(user_id, on_get_user_info)
	--else
		--configSimpleInfo(user_id)
	--end

	--if not battle_rec_info[user_id] then
		request_battle_rec(user_id)
	--else
		--configBattleInfo(user_id)
	--end

	if not dynamic_data[user_id] then
		request_dynamic_info(user_id)
	else
		configDynamicInfo(user_id)
	end
end

function closePlayerInfoPagePanel()
	--playerInfoPage:setVisible(false)	
	clear({playerInfoPage})
	playerInfoPage = nil
end

