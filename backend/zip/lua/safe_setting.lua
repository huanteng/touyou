LAYOUT = Import("layout/safe_setting.lua")
local layoutConfig = LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local panel = nil

local function onUserSetpass( data )
	showMessage( panel, data.memo )
end

local function onUserEmail_code( data )
	showMessage( panel, '已发送验证邮件，请查收', {ms = 3000,y = 500} )
end

local function createPanel(parent, x, y)
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = layoutConfig, nil
	local o = nil

	local function on_back(btn)
		closePanel()
		--SETTING = Import("lua/setting.lua")
		if PANEL_CONTAINER.closeChildPanel( nil, 15 ) then
			local SETTING = Import("lua/setting.lua")							
			PANEL_CONTAINER.addChild( SETTING.showPanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )	
		end	
		--SETTING.showPanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '安全设置' )
	
	for i=1,2 do
		local bg = replaceTable( c.bg, c[ 'bg_'..i] )
		createGrid( ret_panel, bg )
	end
	
	for i=1,6 do
		local label = replaceTable( c.label, c[ 'label_'..i] )
		local func = i == 5 and createMultiLabel or createLabel
		func( ret_panel, label )
	end
	
	local old_pass = createEdit( ret_panel, c.old_pass )
	local new_pass = createEdit( ret_panel, c.new_pass )
	local email = createEdit( ret_panel, c.email )
	
	local function doUserSetpass()
		local old = old_pass:getText()
		local new = new_pass:getText()
		
		if old == '' then
			showMessage( ret_panel, '请输入旧密码' )
			return
		end
		
		if new == '' then
			showMessage( ret_panel, '请输入新密码' )
			return
		end
		
		doCommand( 'user', 'setpass', { old = old, new = new }, onUserSetpass )
	end
	
	local function doUserEmail_code(btn_obj)	
		local email = email:getText()
		
		if email == '' then
			showMessage( ret_panel, '请输入邮箱', {ms = 3000,y = 500} )
			return
		end
		
		doCommand( 'user', 'email_code', { email = email }, onUserEmail_code )
	end
	
	create9Button( ret_panel, c.pass_btn, doUserSetpass )
	create9Button( ret_panel, c.email_btn, doUserEmail_code )
	
	return ret_panel
end

function showPanel(parent, x, y)
	dailyCount('SettingbSafety')--安全设置
	if not panel then
		panel = createPanel(parent, x, y)
	else
		panel:setVisible(true)
	end
end

function closePanel()
	--panel:setVisible(false)
	clear({panel})
	panel = nil
end