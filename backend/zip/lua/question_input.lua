-- question input

QUESTION_INPUT_LAYOUT = Import("layout/question_input.lua")
local QuestionConfig = QUESTION_INPUT_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local hobbyPage = nil

local USER_PAGE = Import("lua/user_info.lua")

local function onQuestionAnswer(data)
	showMessage( hobbyPage, data.memo )
end

local row_desc_info = {
	item_width = 115,
	item_height = 50,
	column_cnt = 3,
	x_space = 30,
	y_space = 30,
}
--空字符串过滤函数
local function filterChar( content )
			local i = 1
			local str = " "
			while(i <= string.len(content)) do
				local b = string.sub(content,1,i)		
				if b ~= str then
					return true
				end
				str = str .. " "
				i = i + 1
			end
			return false
		end	
		
-- 问题数据
local question_data = nil

local function createHobbyPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = QuestionConfig, nil
	local o = nil

	local function on_back(btn)
		closeHobbyPagePanel()
		--USER_PAGE.showUserInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		if PANEL_CONTAINER.closeChildPanel( nil, 16 ) then	--随意传个非空的table，该参数如果为空，就表明需要清除子面板，否则，就会考虑第2个参数							
			PANEL_CONTAINER.addChild( USER_INFO_PANEL.showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
		end
		ok_func( "back" )
	end

	ret_panel._onBack = on_back

	ret_panel._desc_txt = createMultiLabel( ret_panel, c.content )
	
	local function for_show_bug( data )
		--限制显示					
		local dataCount =string.len(data)
		if dataCount > 0 then
			if dataCount < 46 then
			local str = getCharString(data,46)
			ret_panel._user_input_label:setString( str )
			else
			local str = getCharString(data,46) .. ".."
			ret_panel._user_input_label:setString( str )
			end
		end
		
	end

	local function after_submit_input( data )
		f_delay_do( ret_panel, for_show_bug, data, 1 )
	end

	local function show_multi_text_input()
		xymodule.mulitiInput( "", after_submit_input )			
	end

	create9Button( ret_panel, c.multi_text_btn_c, show_multi_text_input )
	ret_panel._user_input_label = createMultiLabel( ret_panel, c.user_input_label_c )		
	
	local function onOk(obj, x, y)
		closeHobbyPagePanel()
		if PANEL_CONTAINER.closeChildPanel( nil, 16 ) then	--随意传个非空的table，该参数如果为空，就表明需要清除子面板，否则，就会考虑第2个参数							
			PANEL_CONTAINER.addChild( USER_INFO_PANEL.showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
		end
		--USER_PAGE.showUserInfoPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
		
		--输入为空时不修改信息
		if filterChar(ret_panel._user_input_label:getString()) then
			ok_func( ret_panel._user_input_label:getString() )
		end
		
	end
	
	createButton( ret_panel, c.btn, onOk )

	ret_panel._sel_row = LIGHT_UI.clsRowLayout:New(ret_panel, 37, 670, row_desc_info)

	return ret_panel
end

	

function init( data )
	question_data = data.data
	ok_func = data.ok_func
end

local function configByData()
	hobbyPage._head_text:setString( question_data.title )
	hobbyPage._desc_txt:setString( question_data.content )
	
end

function showQuestionPagePanel(parent, x, y)
	if not hobbyPage then
		hobbyPage = createHobbyPagePanel(parent, x, y)
	else
		hobbyPage:setVisible(true)
	end

	configByData()
end

function closeHobbyPagePanel()
	--hobbyPage:setVisible(false)
	clear({hobbyPage})
	hobbyPage = nil
end

