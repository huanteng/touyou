-- lucky rot panel 

local luckyPage = nil

LUCKY_LAYOUT = Import("layout/lucky_rot.lua")
local LuckyConfig = LUCKY_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local cur_lucky_star_cnt = nil
local rot_result = nil
local can_rot = true
local rot_action = nil
--[[
result
description
--]]

local rot_degree_data = {
	[0] = 140,
	[1] = 315,
	[2] = 200,
	[3] = 170, -- 财神卡
	[4] = 140,
	[5] = 110, -- 100金币
	[6] = 80, -- 再抽一次
	[7] = 50, -- 10金币
	[9] = 350, -- 200金币
	[10] = 320, -- 魔王卡
	[11] = 290, -- 魔王卡
}

--local CONFIRM_PAGE = Import("lua/confirm_dlg.lua")
local PACKAGE_PAGE = Import("lua/user_package.lua")

local function onLuckyExecute(data)
	
	local function onOk()
		
		PACKAGE_PAGE.showPackageInfoPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, nil, nil, 4 )
		closeLuckyPagePanel()
	end
	
	can_rot = false
	local function on_rot_end()	
		can_rot = true
		--local op = {ms = 4000}
		--showMessage( luckyPage, data.data.description,op )	
		LuckyConfig.alert.content.text = data.data.description
		createAlert(luckyPage,LuckyConfig.alert,onOk )	
	end
	if data.code > 0 then
	    rot_result = data.data
	    playWav( LuckyConfig["lucky_sound"].sound, LuckyConfig["lucky_sound"].ms )
	    local offset_degree = rot_degree_data[tonumber(rot_result.result)]
	    rot_action = LIGHT_UI.autoRot(luckyPage._lucky_rot_page._rot_pic, 100, 1000, 300, -218, 3600+offset_degree, on_rot_end)
	    cur_lucky_star_cnt = cur_lucky_star_cnt - 1
	    luckyPage._left_money_btn:setString(string.format("剩余 %s 幸运星", cur_lucky_star_cnt))	
	    doCommand( 'lucky', 'num', {}, on_get_star_cnt )
	else
		createAlert(luckyPage,LuckyConfig.alert2 )
	    --showMessage(luckyPage,data.memo)
	end
end

local function createLuckyRot(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local function onClickRot(btn, x, y)
		if not cur_lucky_star_cnt then
			return
		end
		if can_rot == true then
			doCommand( 'lucky', 'execute', {}, onLuckyExecute )
		end
	end

	local txt_list = {
		[1] = "               VIP体验卡",
		[2] = "               魔王卡",
		[3] = "               200金币",
		[4] = "               神秘礼品",
		[5] = "               10金币",
		[6] = "               再抽一次",
		[7] = "               100金币",
		[8] = "               谢谢参与",
		[9] = "               财神卡",
		[10] = "               20金币",
		[11] = "               10000金币",
		[12] = "               谢谢参与",
	}
	ret_panel._rot_pic = LIGHT_UI.clsRotSprite:New(ret_panel, 0, 0, "dial_default.png", txt_list, -15)
	ret_panel._begin_rot_btn = LIGHT_UI.clsButton:New(ret_panel, -57, -60, "dial_arrows.png")
	ret_panel._begin_rot_btn.onTouchEnd = onClickRot

	return ret_panel
end

local REWARD_LIST_PAGE = Import("lua/reward_list.lua")

local function createLuckyPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createContainerChildPanel( parent, x, y )

	local c, c2 = RegConfig, nil
	local o = nil

	local function on_back(btn)
		closeLuckyPagePanel()
		MAINPAGE_PANEL.showMainPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	--ret_panel._onBack = on_back
	--ret_panel._head_text:setString( '幸运转盘' )

	local function on_reward_list_click(btn)
		closeLuckyPagePanel()
		REWARD_LIST_PAGE.showRewardListPagePanel(HTTP_CLIENT.getRootNode(), 0, 0)
	end

	config = LuckyConfig.reward_list_btn_config
	ret_panel._reward_list_btn = LIGHT_UI.clsButton:New(ret_panel, config.x, config.y, config.res)
	ret_panel._reward_list_btn.onTouchEnd = on_reward_list_click

	config = LuckyConfig.bg_pic_config
	ret_panel._bg_pic = LIGHT_UI.clsSprite:New(ret_panel, config.x, config.y, config.res)
	ret_panel._bg_pic:setAnchorPoint(0, 1)
	
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._lucky_rot_page = createLuckyRot(ret_panel, winSize.width/2, 400)

	config = LuckyConfig.left_money_config
	ret_panel._left_money_btn = LIGHT_UI.clsButton:New(ret_panel, config.x, config.y, config.res)
	ret_panel._left_money_btn:setString("剩余 ? 幸运星")
	ret_panel._left_money_btn:setTextColor(0, 0, 0)
	LIGHT_UI.alignXCenter(ret_panel._left_money_btn)
	
	ret_panel.closePanel = closeLuckyPagePanel

	return ret_panel
end

function on_get_star_cnt(data)
	cur_lucky_star_cnt = data.data.result
	luckyPage._left_money_btn:setString(string.format("剩余 %s 幸运星", cur_lucky_star_cnt))
end

function showLuckyPagePanel(parent, x, y)
	if not luckyPage then
		luckyPage = createLuckyPagePanel(parent, x, y)
	else
		luckyPage:setVisible(true)
	end
	can_rot = true
	doCommand( 'lucky', 'num', {}, on_get_star_cnt )
	return luckyPage
end

function closeLuckyPagePanel()
	if rot_action then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(rot_action)
	end		
	clear({luckyPage})
	luckyPage = nil
	rot_action = nil
	--luckyPage:setVisible(false)
end

