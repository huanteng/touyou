-- msg page

MSG_LAYOUT = Import("layout/message.lua")
local MsgConfig = MSG_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]
local msgPage = nil

local game_list = {}
local cur_game_page = 0
local max_game_page = 5
local chat_list = {}
local cur_chat_page = 0
local max_chat_page = 5
local reply_list = {}
local cur_reply_page = 0
local max_reply_page = 5
local system_list = {}
local cur_system_page = 0
local max_system_page = 5
local total_width = 480
local page_size = 10
local click_delta = 10
local CHALLENGE_PAGE = Import("lua/challenge.lua")
--消息类型红点和数字的显示资源保存在msg_style中
local msg_style = {[2] = {},[3] = {},[4] = {}}

local showName = {}
local function createGameItem( info )                        --创建聊天，回复，系统的部件
	local ret_panel = LIGHT_UI.clsNode:New( nil, 0, 0 )

	PAGE[ 'xyz' ] = {
			x = 0,
			y = 0,
			sx = 0,
			sy = 0,
			ax = 0,
			ay = 0,
		}
	
	PAGE[ 'xyz2' ] = {
			x = 0,
			y = 0,
			sx = 480,
			sy = 0,
			ax = 0,
			ay = 0,
		}

	local function on_item_click(obj, x, y)

		local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_" .. info.kind, "{}" )
		local db_bin = json.decode( db_txt );
		for key, data in ipairs( db_bin ) do
			if data.sender_name == info.sender_name then
				if db_bin[key].readed ~= 1 then
					db_bin[key].readed = 1		--	标志为已读
					break
				end			
			end
		end
		
		db_txt = table2json( db_bin );
		setUserString( "msg_db_" .. GLOBAL.uid .. "_" .. info.kind, db_txt )   --点击的时候就设置消息
		PANEL_CONTAINER.showMessageNum()

		local can_jump = true
		if info.type == "1" then
			--delete_message_item( info )
			can_jump = false
		elseif info.type == "2" then
			--delete_message_item( info )
			can_jump = false
		elseif info.type == "3" then
			--delete_message_item( info )
			can_jump = false
		elseif info.type == "4" then
			--delete_message_item( info )
			can_jump = false
		elseif info.type == "5" then
		elseif info.type == "6" then
		elseif info.type == "7" then
		end

		local function back_func()
			if PANEL_CONTAINER.closeChildPanel( nil, 13 ) then
				PANEL_CONTAINER.addChild( showMsgPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )
			end
			--showMsgPagePanel( HTTP_CLIENT.getRootNode(), 0, 0 )
		end
		
		if can_jump then
			closeMsgPagePanel()
			jump( tonumber( info.jump ), info.jump_data, back_func )    --点击按钮后跳转的页
			
		end
	end

	local whole_bg = createFrameButton( ret_panel, MsgConfig.whole_bg )     --创建框架按钮
	setMsgDelegate( whole_bg, msgPage._move_small_page._move_small_grp, on_item_click )

	local total_height = 90

	MsgConfig.logo.res = info.sender_logo
	createSprite( ret_panel, MsgConfig.logo )

	MsgConfig.name.text = info.sender_name
	createLabel( ret_panel, MsgConfig.name )

	MsgConfig.time.text = os.date( '%Y-%m-%d %H:%M', info.time )
	createLabel( ret_panel, MsgConfig.time )

	MsgConfig.content.text = info.content
	local content = createMultiLabel( ret_panel, MsgConfig.content )   ---创建多行文字显示
	local content_size = content:getContentSize()
	local content_x, content_y = content:getPosition()
	local total_height2 = math.abs( content_y ) + content_size.height
	
	if total_height2 > total_height then
		total_height = total_height2
	end
	
	total_height = total_height + MsgConfig.other.margin	
	MsgConfig.split_line.y = -total_height - MsgConfig.split_line.margin
	createSprite( ret_panel, MsgConfig.split_line )

	total_height = total_height + MsgConfig.split_line.margin + 1 
	whole_bg:setContentSize( MsgConfig.whole_bg.sx, total_height )

	local function getContentSize( node )		
		return { ["width"] = total_width, ["height"] = total_height, }
	end

	ret_panel.getContentSize = getContentSize
	
	return ret_panel
end

local function createChatItem( info )
	return createGameItem( info )
end

local function createReplyItem( info )
	return createGameItem( info )
end

local function createSystemItem( info )
	return createGameItem( info )
end

local function request_game_list( page, size )
	if not size then size = page_size end
	if page == 1 then game_list = {} end
	local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_1", "{}" )
	local db_bin = json.decode( db_txt );
	local start = ( page - 1 ) * size + 1
	local ends = start + size
	local count = 0

	function sort_it( a, b )
		return a.time > b.time
	end
	table.sort( db_bin, sort_it )

	for _, info in ipairs( db_bin ) do
		count = count + 1
		if count >= start and count <= ends then
			table.insert( game_list, info )
			msgPage._move_small_page._game_page:append_item( createGameItem( info ) )
		end
	end

	msgPage._move_small_page._game_page:refresh_view()
end

local function request_chat_list( page, size )
	if not size then size = page_size end           --设置页面大小
	if page == 1 then
		msgPage._move_small_page._chat_page:clearAllItem()
		chat_list = {}
	end            --设置聊天页面
	local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_2", "{}" )   --取得聊天消息
	local db_bin = json.decode( db_txt );                                  --把字符串转换成表
	local start = ( page - 1 ) * size + 1                                
	local ends = start + size
	local count = 0

	function sort_it( a, b )                   --对消息按时间进行排序		
		if not a then
			return true
		end
		if not b then
			return false
		end	
		return tonumber( a.time ) > tonumber( b.time )
	end		
	table.sort( db_bin, sort_it )
		
	if #db_bin == 0 then
		if not msgPage._move_small_page.tip then
			msgPage._move_small_page.tip = createLabel( msgPage._move_small_page, {x = 100, y = 670, ax = 0, ay = 0.5, text = "当前没有任何消息"} )
			xCenter( msgPage._move_small_page.tip )
		end			
	end
	--showName = {}
	for _, info in ipairs( db_bin ) do           
		count = count + 1
		if count >= start and count <= ends then
			msgPage._move_small_page._chat_page:append_item( createChatItem( info ) )
			--[[if #showName == 0 then
				table.insert( showName, info.sender_name )
				table.insert( chat_list, info )         --把信息添加到聊天页表上
				msgPage._move_small_page._chat_page:append_item( createChatItem( info ) )
			else
				local hasExsit = false
				for k, v in ipairs( showName ) do
					if v == info.sender_name then
						hasExsit = true
						break;
					end
				end
				if hasExsit == false then
					table.insert( showName, info.sender_name )
					table.insert( chat_list, info )         --把信息添加到聊天页表上
					msgPage._move_small_page._chat_page:append_item( createChatItem( info ) )
				end
			end--]]
			
		end
	end

	msgPage._move_small_page._chat_page:refresh_view()
end

local function request_reply_list( page, size )
	if not size then size = page_size end                 
	if page == 1 then		
		reply_list = {}
		msgPage._move_small_page._reply_page:clearAllItem()
	end
	local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_3", "{}" )
	local db_bin = json.decode( db_txt );
	local start = ( page - 1 ) * size + 1
	local ends = start + size
	local count = 0

	function sort_it( a, b )
		return a.time > b.time
	end
	table.sort( db_bin, sort_it )
		
	if #db_bin == 0 then
		if not msgPage._move_small_page.tip then
			msgPage._move_small_page.tip = createLabel( msgPage._move_small_page, {x = 100, y = 670, ax = 0, ay = 0.5, text = "当前没有任何消息"} )
			xCenter( msgPage._move_small_page.tip )
		end			
	end

	--showName = {}
	for _, info in ipairs( db_bin ) do
		count = count + 1
		if count >= start and count <= ends then
			msgPage._move_small_page._reply_page:append_item( createReplyItem( info ) )
			--[[if #showName == 0 then
				table.insert( showName, info.sender_name )
				table.insert( reply_list, info )
				msgPage._move_small_page._reply_page:append_item( createReplyItem( info ) )
			else
				local hasExsit = false
				for k, v in ipairs( showName ) do
					if v == info.sender_name then
						hasExsit = true
						break;
					end
				end
				if hasExsit == false then
					table.insert( showName, info.sender_name )
					table.insert( reply_list, info )
					msgPage._move_small_page._reply_page:append_item( createReplyItem( info ) )
				end
			end--]]
		end
	end

	msgPage._move_small_page._reply_page:refresh_view()
end

local function request_system_list( page, size )
	if not size then size = page_size end
	if page == 1 then
		system_list = {}
		msgPage._move_small_page._system_page:clearAllItem()
	end
	local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_4", "{}" )
	local db_bin = json.decode( db_txt );
	local start = ( page - 1 ) * size + 1
	local ends = start + size
	local count = 0

	function sort_it( a, b )
		return a.time > b.time
	end
	table.sort( db_bin, sort_it )	
	
	if #db_bin == 0 then
		if not msgPage._move_small_page.tip then
			msgPage._move_small_page.tip = createLabel( msgPage._move_small_page, {x = 100, y = 670, ax = 0, ay = 0.5, text = "当前没有任何消息"} )
			xCenter( msgPage._move_small_page.tip )
		end			
	end	

	--showName = {}
	for _, info in ipairs( db_bin ) do
		count = count + 1
		if count >= start and count <= ends then
			msgPage._move_small_page._system_page:append_item( createSystemItem( info ) )
			--[[if #showName == 0 then
				table.insert( showName, info.sender_name )
				table.insert( system_list, info )
				msgPage._move_small_page._system_page:append_item( createSystemItem( info ) )
			else
				local hasExsit = false
				for k, v in ipairs( showName ) do
					if v == info.sender_name then
						hasExsit = true
						break;
					end
				end
				if hasExsit == false then
					table.insert( showName, info.sender_name )
					table.insert( system_list, info )
					msgPage._move_small_page._system_page:append_item( createSystemItem( info ) )
				end
			end--]]
		end
	end

	msgPage._move_small_page._system_page:refresh_view()
end

local function refresh_game_list()
	msgPage._move_small_page._game_page:clearAllItem()
	request_game_list( 1 )
end

local function refresh_chat_list()
	msgPage._move_small_page._chat_page:clearAllItem()
	request_chat_list( 1 )
end

local function refresh_reply_list()
	msgPage._move_small_page._reply_page:clearAllItem()
	request_reply_list( 1 )
end

local function refresh_system_list()
	msgPage._move_small_page._system_page:clearAllItem()
	request_system_list( 1 )
end

function delete_message_item( info )
	local db_txt = getUserString( "msg_db_" .. info.kind, "{}" )
	local db_bin = json.decode( db_txt );	

	for key, data in ipairs( db_bin ) do
		if data._id == info._id then
			db_bin[key] = nil
			table.remove( db_bin, key )
			db_txt = table2json( db_bin );
			setUserString( "msg_db_" .. GLOBAL.uid .. "_" .. info.kind, db_txt )
			if info.kind == "1" then refresh_game_list() end
			if info.kind == "2" then refresh_chat_list() end
			if info.kind == "3" then refresh_reply_list() end
			if info.kind == "4" then refresh_system_list() end
			break
		end
	end
end

local function on_accept_cha(data)
end

local function on_reject_cha(data)
end

local small_desc_info = {
	item_width = 480,
	item_height = 100,
	column_cnt = 1,
	x_space = 0,
	y_space = 0,
}

local function createMoveSmallPage(parent, x, y)
	local ret_panel = LIGHT_UI.clsNode:New(parent, x, y)

	local config = MsgConfig["move_page_config"]
	local winSize = CCDirector:sharedDirector():getWinSize()
	ret_panel._move_small_grp = LIGHT_UI.clsMoveHorizonPageGroup:New(ret_panel, 0, 0, winSize.width, config["view_height"])
	ret_panel._move_small_grp:setVMovable(true)

	local chat_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	chat_page._use_dynamic_size = true 
	chat_page:refresh_view()

	local function onCOverTop(layout)
		chat_list = {}
		chat_page:clearAllItem()
		cur_chat_page = 1
		request_chat_list(cur_chat_page)
	end

	local function onCOverBottom(layout)
		if cur_chat_page < max_chat_page then
			cur_chat_page = cur_chat_page + 1
			request_chat_list(cur_chat_page)
		end
	end

	chat_page.onHMoveOverTop = onCOverTop
	chat_page.onHMoveOverBottom = onCOverBottom


	local reply_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	reply_page._use_dynamic_size = true 
	reply_page:refresh_view()

	local function onROverTop(layout)
		reply_list = {}
		reply_page:clearAllItem()
		cur_reply_page = 1
		request_reply_list(cur_reply_page)
	end

	local function onROverBottom(layout)
		if cur_reply_page < max_reply_page then
			cur_reply_page = cur_reply_page + 1
			request_reply_list(cur_reply_page)
		end
	end

	reply_page.onHMoveOverTop = onROverTop
	reply_page.onHMoveOverBottom = onROverBottom

	local system_page = LIGHT_UI.clsRowLayout:New(nil, 0, 0, small_desc_info)
	system_page._use_dynamic_size = true 
	system_page:refresh_view()

	local function onSOverTop(layout)
		system_list = {}
		system_page:clearAllItem()
		cur_system_page = 1
		request_system_list(cur_system_page)
	end

	local function onSOverBottom(layout)
		if cur_system_page < max_system_page then
			cur_system_page = cur_system_page + 1
			request_system_list(cur_system_page)
		end
	end

	system_page.onHMoveOverTop = onSOverTop
	system_page.onHMoveOverBottom = onSOverBottom

	chat_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(chat_page)
	reply_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(reply_page)
	system_page:setPosition(0, config["inner_y"])
	ret_panel._move_small_grp:appendItem(system_page)

	ret_panel._move_small_grp:selectPage(1)

	ret_panel._chat_page = chat_page 
	ret_panel._reply_page = reply_page
	ret_panel._system_page = system_page 

	local function get_y_limit(obj)
		return config["inner_y"] 
	end

	chat_page.getYLimit = get_y_limit
	reply_page.getYLimit = get_y_limit
	system_page.getYLimit = get_y_limit
	
	return ret_panel
end

--创建基础界面。
local function createMsgPagePanel(parent, x, y) -- 转换成世界坐标一定要结点的父结点调用
	local ret_panel = createContainerChildPanel( parent, x, y )   

	local c, c2 = MsgConfig, nil                    
	local o = nil
	
	local function on_back(btn)
		closeMsgPagePanel()
		MAINPAGE_PANEL.showMainPagePanel( HTTP_CLIENT.getRootNode(), 0, 0 )    
		GAMEHALL_PANEL.showGameHallPanel(HTTP_CLIENT.getRootNode(), 0, 0 )     
	end

	--ret_panel._onBack = on_back
	--ret_panel._head_text:setString( '消息' )

	local function onClickRemove(obj, x, y)
		local kind = ret_panel._move_small_page._move_small_grp:getCurPage()
		PANEL_CONTAINER.clearMsgPicNum( getSizeByType( kind + 1 ) )
		    
		setUserString( "msg_db_" .. GLOBAL.uid .. "_" .. ( kind + 1 ), "{}" )   
		if kind == 1 then refresh_chat_list() end           
		if kind == 2 then refresh_reply_list() end          
		if kind == 3 then refresh_system_list() end          
		clearMsgPicNum( kind + 1 )						
		
		showMessage( ret_panel, '成功删除' )
		
	end

	createButton( ret_panel, c.btn, onClickRemove )

	config = MsgConfig["move_page_config"]
	ret_panel._move_small_page = createMoveSmallPage(ret_panel, config["x"], config["y"]) -- 小图标列表 
	
	local function removeTip()
		clear( {msgPage._move_small_page.tip} )
		msgPage._move_small_page.tip = nil
	end

	local function on_sel_small_page(move_obj)
		removeTip()
		ret_panel._move_hint:selItem(move_obj:getCurPage())

		--if move_obj:getCurPage() == 1 and table.maxn( chat_list ) <= 0 then
		if move_obj:getCurPage() == 1 then
			cur_chat_page = 1
			request_chat_list( cur_chat_page )    
		end
		--if move_obj:getCurPage() == 2 and table.maxn( reply_list ) <= 0 then
		if move_obj:getCurPage() == 2 then
			cur_reply_page = 1
			request_reply_list( cur_reply_page )       
		end
		--print( table.maxn( system_list ) )
		--if move_obj:getCurPage() == 3 and table.maxn( system_list ) <= 0 then
		if move_obj:getCurPage() == 3 then
			cur_system_page = 1                             
			request_system_list( cur_system_page )      
		end
	end				

	ret_panel._move_small_page._move_small_grp.onSelPage = on_sel_small_page
	ret_panel._move_small_page._move_small_grp.pMove = removeTip

	local item_list = {
		[1] = {
			["txt"] = "聊天",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 20,
		},
		[2] = {
			["txt"] = "回复",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 20,
		},
		[3] = {
			["txt"] = "系统",
			["normal_color"] = { [1] = 153, [2] = 153, [3] = 153, },
			["sel_color"] = { [1] = 255, [2] = 255, [3] = 255, },
			["size"] = 20,
		},
	}

	config = MsgConfig["move_hint_config"]
	local total_width = 480
	ret_panel._move_hint = LIGHT_UI.clsMoveGrpHint:New(ret_panel, config["x"], config["y"], "tab_bg_image.png", item_list, total_width, config["res"], "tiny_black_pixel.png")
	local bg = ret_panel._move_hint:getBGObj()
	bg:setScaleX(total_width)

	local function on_click( obj, idx )
		msgPage._move_hint:selItem( idx )		
		msgPage._move_small_page._move_small_grp:selectPage( idx )
	end
	
	ret_panel._move_hint.onClick = on_click 
	
	ret_panel.closePanel = closeMsgPagePanel 
	return ret_panel
end

function onChallenge(data_info)
end

----消除添加消息后删除的显示数字和红点
function clearMsgPicNum(i)
	if msg_style then
		if msg_style[i].pic then 
			clear({msg_style[i].pic})				
			msg_style[i].pic = nil		
		end 
		if msg_style[i].num then 		
			clear({msg_style[i].num})
			msg_style[i].num = nil
		end 
	end
end 		

function getSizeByType( i )
	CCUserDefault:sharedUserDefault():flush()
	local msg_size = 0
	local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_" .. i, "{}" )
	local db_bin = json.decode( db_txt );
	for key, data in ipairs( db_bin ) do
		msg_size = msg_size + 1
	end
	return msg_size
end

local defaultValue = false

--更新消息数量用红点显示
--local function update_message_num() 
function update_message_num()
	for i = 2, 4 do
		clearMsgPicNum( i )
	end
	
	CCUserDefault:sharedUserDefault():flush()

	local x = 145
	local y = 730
	local interval = 120

    defaultValue = false
	for i = 2, 4 do	
		
		local msg_size = 0
		local db_txt = getUserString( "msg_db_" .. GLOBAL.uid .. "_" .. i, "{}" )
		local db_bin = json.decode( db_txt );
		for key, data in ipairs( db_bin ) do
			if not db_bin[key].readed then msg_size = msg_size + 1 end
		end
        
		
		if msg_size > 0 then
			setDefaultPageWithMsgRule( i )
			local ret_panel = msgPage
			local offset = 0
			if msg_size > 9 then offset = 2	end
			--判断是那个位置上有消息
			--[[if i == 3 then
			     x = x + 126				
		    elseif i == 4 then
				 x = x + 118										
			end		--]]		
	
			--local test1 = LIGHT_UI.clsSprite:New( ret_panel, x, y, "tab_unread_bg.png" )
			--local test2 = LIGHT_UI.clsLabel:New( ret_panel, x - offset, y, msg_size, GLOBAL_FONT, 18 )
			----test2:setTextColor(255, 255, 255)
			msg_style[i].pic = LIGHT_UI.clsSprite:New( ret_panel, x, y, "tab_unread_bg.png" )
			msg_style[i].num = LIGHT_UI.clsLabel:New( ret_panel, x - offset, y, msg_size, GLOBAL_FONT, 18 )						
			msg_style[i].num:setTextColor(255, 255, 255)
								
		else	
			clearMsgPicNum(i)	  				
		end
		x = x + interval
	end		
	setDefaultPageWithMsgRule( 2 )
end

function setDefaultPageWithMsgRule( i )
	if defaultValue == false then
		defaultValue = true
		msgPage._move_hint:selItem( i - 1 )		        
		msgPage._move_small_page._move_small_grp:selectPage( i - 1 )		
	end
end

function showMsgPagePanel(parent, x, y) 
	dailyCount('Messages')--消息  
	msg_style = {[2] = {},[3] = {},[4] = {}}
	--if not msgPage then
	msgPage = createMsgPagePanel(parent, x, y)	
		--msgPage._move_hint:selItem( 3 )		        
		--msgPage._move_small_page._move_small_grp:selectPage( 3 )  
		
	--[[else
		msgPage:setVisible(true)
	end	--]]
	update_message_num()   
	return msgPage
end

function getMsgPanel()
	return msgPage
end

function closeMsgPagePanel()
	if not msgPage then
		return
	end
	--msgPage:setVisible(false)
	clear({msgPage})
	msgPage = nil
end

