local videoAuth = nil
local back_func = nil
LUCKY_LAYOUT = Import( "layout/video_auth.lua" )
local VideoAuthConfig = LUCKY_LAYOUT.getConfigData()[LIGHT_UI.CurScreenSize]

local function createVideoAuthPagePanel( parent, x, y )
	local ret_panel = createBasePanel( parent, x, y )

	local c, c2 = VideoAuthConfig, nil
	local o = nil

	local function on_back(btn)
		closeVideoAuthPagePanel()
		if back_func then
			back_func()
		else
			local USER_INFO_PANEL = Import( "lua/user_info.lua" )
		--USER_INFO_PANEL.showUserInfoPagePanel( HTTP_CLIENT.getRootNode(), 0, 0 )
			if PANEL_CONTAINER.closeChildPanel( nil, 16 ) then
				PANEL_CONTAINER.addChild( USER_INFO_PANEL.showUserInfoPagePanel(PANEL_CONTAINER.ret_panel.bg, 0, 0) )				
			end	
		end
		
	end

	ret_panel._onBack = on_back
	ret_panel._head_text:setString( '视频认证' )

	PAGE[ 'xyz' ] = {
			x = 0,
			y = 0,
			sx = 480,
			sy = 720,
			ax = 0,
			ay = 0,
		}

	local desc_txt = createMultiLabel( ret_panel, c.desc_txt )
	local success_txt = createMultiLabel( ret_panel, c.success_txt )
	local video_txt = createLabel( ret_panel, c.video_txt )

	ret_panel._success_txt = success_txt
	ret_panel._video_txt = video_txt

	local function on_upload_cb( data )
		data = json.decode( data )
		success_txt:setString( '认证视频已上传，并已进入审核队列，由于排队人数多，审核时间可能较长，请耐心等待...温馨提示：升级VIP可加快审核速度' )
	end

	local function after_take_video2( real_path )
		success_txt:setString( '视频上传需耗费数秒时间，请稍候...所录视频仅供系统审核用，其他任何人不可见' )
		success_txt:setVisible( true )
		video_txt:setVisible( false )
		local url = GLOBAL.interface .. "album.php"
		local post_data = {
			["type"] = "upload",
			["sid"] = GLOBAL.sid,
			["desc"] = "",
			["kind"] = "2",
			["file"] = real_path,
		}

		HTTP_REQUEST.http_upload( url, post_data, on_upload_cb )
	end

	local function after_take_video( real_path )
		closeVideoAuthPagePanel()
		local obj = Import( "lua/after_video_auth.lua" )
		obj.showAfterVideoAuthPagePanel( HTTP_CLIENT.getRootNode(), 0, 0, real_path )
	end

	local function goto_validate(obj, x, y)
		xymodule.take_video( after_take_video )
		--after_take_video( 'xxx' )
	end
		
	create9Button( ret_panel, c.video_btn, goto_validate )

	return ret_panel
end

function showVideoAuthPagePanel( parent, x, y,back_fun )
	
	back_func = back_fun
	
	if not videoAuth then
		videoAuth = createVideoAuthPagePanel( parent, x, y )
	else
		videoAuth._success_txt:setVisible( false )
		videoAuth._video_txt:setVisible( true )
		videoAuth:setVisible( true )
	end
end

function closeVideoAuthPagePanel()
	--videoAuth:setVisible( false )
	clear({videoAuth})
	videoAuth = nil
end