<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$news = load( 'biz.news' );
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'auto_add_album.php' );
		}

		function do_post()
		{
	
			if ( isset($this->input['user']) && is_array($this->input['user']) && isset( $_FILES['file'] ) && isset($this->input['time']) && is_array($this->input['time']) )
			{	

				$tool = load( 'tool' );
				$database = load('database');
				$user = load( 'biz.user' );
				
				$data = array();
				$count = 0;
				foreach( $this->input['user'] as $index => $name )
				{

					$temp = $database->unique( "select id from user where name='".trim($name)."'" );
					if( isset($temp['id']) ) 
					{
			
						$uid = $temp['id'];
						if ( isset( $_FILES['file']['name'][$index] ) && $_FILES['file']['error'][$index] == 0 )
						{							
							$dir = $user->logo_base_dir . $user->logo_dir( $uid );
							if ( ! is_dir( $dir ) ) mkdir( $dir, 0755, true );
							$file = uniqid() . '.jpg';

							if ( move_uploaded_file( $_FILES['file']['tmp_name'][$index], $dir . $file ) )
							{
								$tool->resize_image( $dir . $file, 120, 120, 1, $dir . $file . '.s.jpg' );
								$path = $user->logo_dir( $uid ) . $file;
								$time = strtotime( $this->input['time'][$index] );
								
								$data[] = array('path'=>$path,'complete'=>0,'time'=>$time,'user'=>$uid,'name'=>$name);								
							}
						}
					}
				}

				if( count($this->input['user']) == count($data) )
				{
					$return = $database->add('task_queue', array( 'type'=>6,'data'=>serialize($data),'time'=>$data[0]['time'],'target'=>0,'user'=>0,'status'=>1,'create'=>time(),'creater'=>$this->account ) );
					
					if( $return ) $this->prompt( '添加成功', array( array( 'url' => 'auto_add_album.php', 'name' => '预添加图片', 'default' => true ) ) );
				}
				else
				{
					$this->prompt( '添加失败,请检查用户名是否正确', array( array( 'url' => 'auto_add_album.php', 'name' => '预添加图片', 'default' => true ) ) );
				}
				
			}

		}
	}

	$action = new action();
	$action->run();
?>