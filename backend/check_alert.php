<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$id = isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) ? $this->input['id'] : '';
			$task_queue = load( 'biz.task_queue' );
			$alert_array = $task_queue->database->select( 'select * from alert' . ( $id != '' ? ' where id = ' . $id : '' ) );
			foreach( $alert_array as $alert ) $task_queue->{'handle_'.$alert['id']}( array( 'type' => $alert['id'] ) );
			header( 'location: ' . $_SERVER['HTTP_REFERER'] );
		}
	}

	$action = new action();
	$action->run();
?>