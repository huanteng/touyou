<?php
	if ( isset( $_FILES['imgFile']['name'] ) && $_FILES['imgFile']['size'] > 0 )
	{
		$dict = array( 'png' => '', 'gif' => '', 'jpg' => '', 'bmp' => '' );
		$tmp = explode( '.', $_FILES['imgFile']['name'] );
		$size = count( $tmp );
		$ext = trim( strtolower( $tmp[$size-1] ) );

		if ( isset( $dict[ $ext ] ) )
		{
			$dir = '../upload/' . date( 'Ym' ) . '/';
			if ( ! is_dir( $dir ) ) mkdir( $dir, 0755, true );
			$file = $dir . uniqid() . '.' . $ext;

			if ( move_uploaded_file( $_FILES['imgFile']['tmp_name'], $file ) )
			{
				$file = str_replace( '../', '', $file );
				$result = array( 'error' => 0, 'url' => $file );
			}
			else
			{
				$result = array( 'error' => 1, 'message' => '转移失败' );
			}
		}
	}
	else
	{
		$result = array( 'error' => 1, 'message' => '参数错误' );
	}

	echo json_encode( $result );
?>