<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$file = '../cache/error.log';

			if ( isset( $this->input['clear'] ) )
			{
				if ( is_file( $file ) ) unlink( $file );
				header( 'location: error.php' );
			}
			else
			{
				$error_array = array();

				if ( is_file( $file ) )
				{
					$line_array = explode( "\n\n", file_get_contents( $file ) );

					foreach( $line_array as $line )
					{
						if ( $line != '' )
						{
							$content = '$_data_ = ' . $line . ';';
							eval( $content );
							$error_array[] = $_data_;
						}
					}
				}

				$template = load( 'template', array( 'dir' => 'template/' ) );
				$template->assign( 'error_array', $error_array );
				echo $template->parse( 'error.php' );
			}
		}
	}

	$action = new action();
	$action->run();
?>