<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$news = load( 'biz.news' );
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'add_album.php' );
		}

		function do_post()
		{

			if ( isset( $this->input['user'] ) && $this->input['user'] != '' && isset( $_FILES['file'] ) )
			{	
				
				$database = load('database');
				$temp = $database->unique( "select id from user where name='".$this->input['user']."'" );
				
				if( !isset($temp['id']) ) 
				{
					$this->prompt( '添加失败,此用户名不存在', array( array( 'url' => 'add_album.php', 'name' => '添加图片', 'default' => true ) ) );
				}
				else
				{
					$data = array();
					$data['user'] = $temp['id'];
					$data['name'] = 'file';

					$album = biz('album')->_upload( $data );
					if( $album['code'] == 1 )
					{
						$this->prompt( '添加成功', array( array( 'url' => 'album.php', 'name' => '图片管理', 'default' => true ) ) );
					}
					else
					{
						$this->prompt( '添加失败', array( array( 'url' => 'add_album.php', 'name' => '添加图片', 'default' => true ) ) );
					}
				}
				
			}

		}
	}

	$action = new action();
	$action->run();
?>