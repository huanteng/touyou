<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_template['css']; ?>redmond/jquery-ui-1.7.custom.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script type="text/javascript" src="<?php echo $_template['css']; ?>javascript/jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery-ui-1.7.3.datepicker.js"></script>
<script>
function selected_all()
{
	$('input:checkbox[name=\'id\[\]\']').attr('checked',$('#btn_select_all').attr('checked'));
}
function delete_all()
{
	if( confirm('确定删除吗？') )
	{
		$('#form2').submit();
	}
}

$(function(){
	$('#_start').datepicker({
		inline: false,
		dateFormat: 'yy-mm-dd'
	});
});
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">动态管理</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是动态列表，您可以删除动态。</td>
			  </tr>
			  <form id="form1" method="get">
			  <tr>
				<td align="left" class="left_txt">用户名： <input type="text" name="_name" size="10" value="<?php echo $_name; ?>">
				类型：<select name="_type"><option value="">全部</option><?php foreach($type_dict as $value=>$name){?><option value="<?php echo $value;?>"<?php if($value==$_type) echo ' selected'?>><?php echo $name;?></option><?php }?></select>
				日期：<input type="text" name="_start" id="_start" size="10" value="<?php echo $_start; ?>">
		<input type="submit" value="搜索"></td>
			  </tr>
		      </form>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
				<form id="form2" method="post">
				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
						<tr class="title f1">
							<th width="5%">选择</th>
							<th width="5%">序号</th>
							<th width="10%">时间</th>
							<th width="10%">类型</th>
					        <th>内容</th>
					        <th width="10%">操作</th>
						</tr>
					<?php
						foreach( $data as $index => $value )
						{
					?>
						<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?>>
							<td><input type="checkbox" name="id[]" value="<?php echo $value['id']; ?>"/></td>
							<td><?php echo $value['id']; ?></td>
							<td><?php echo date( 'Y-m-d H:i', $value['time'] ); ?></td>
							<td><?php echo $value['type']; ?></td>
							<td align="left"> <?php echo $value['content']; ?></td>
							<td><table width="80%"><tr align="center"><td><a href="view_moving.php?id=<?php echo $value['item']; ?>&user=<?php echo $value['user']; ?>&type=<?php echo $value['_type']; ?>">查看</a></td></tr></table></td>
						</tr>
					<?php
						}
					?>
					</table>

					<div style="text-align:center; height:15px; padding-top:4px; font-size:12px;">
					<span style="float: left;"><input type="checkbox" onclick="selected_all()" id="btn_select_all" />全选&nbsp;<input type="button" value="删除" onclick="delete_all()" id="btn_delete_all"/></span>

					记录数：<?php echo $bar['total']; ?>  <a href="<?php echo $bar['prev_link']; ?>"><img src="<?php echo $_template['img']; ?>prev.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;
					<?php
						foreach( $bar['number'] as $value )
						{
							if ( $value['is_current'] )
							{
					?>
						<span class="homeStockRise"><strong><?php echo $value['name']; ?></strong></span>&nbsp;
					<?php
							}
							else
							{
					?>
						<a href="<?php echo $value['link']; ?>"><?php echo $value['name']; ?></a>&nbsp;
					<?php
							}
					?>
					<?php
						}
					?>
					<a href="<?php echo $bar['next_link']; ?>"><img src="<?php echo $_template['img']; ?>next.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;[<?php echo $bar['current']; ?>/<?php echo $bar['page']; ?>]</div>

				</td>
			  </tr>

			  <tr><td>&nbsp;</td></tr>
			  <tr>
			  	  <td>
					<fieldset>
					  <legend>说明：</legend>
						<p>用户名 点击可查看用户个人资料</p>
						<p>用户名 右键可调出命令菜单</p>
						<p>操作区</p>
						<p>  --查看 查看动态对应的具体信息</p>
					</fieldset>
				</td>
			  </tr>

			</table></form></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>