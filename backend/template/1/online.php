<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.contextmenu.r2.packed.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>dice.contextmenu.js"></script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">在线管理</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">
	<form method="get">
	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是在线用户列表。</td>
			  </tr>
			  <tr>
				<td align="left" class="left_txt">名称：<input type="text" name="_name" size="10" value="<?php echo $_name; ?>"> 性别：<select name="_sex"><option value="">全部</option><option value="0"<?php if($_sex=='0')echo ' selected';?>>男</option><option value="1"<?php if($_sex=='1')echo ' selected';?>>女</option></select> 身份：<select name="_npc"><option value=""<?php if ( $_npc == '' ) echo ' selected'; ?>>全部</option><option value="0"<?php if ( $_npc == '0' ) echo ' selected'; ?>>非NPC</option><option value="1"<?php if ( $_npc == '1' ) echo ' selected'; ?>>NPC</option></select> <input type="submit" value="搜索">
				<span style="float:right;"><input type="button" value="在线报表" onclick="window.location.href='./online_report.php'"></span>
				</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>

				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
						<tr class="title f1">
							<th>用户</th>
							<th>上次退出</th>
							<th>登录时间</th>
							<th>最近心跳</th>
							<th>在线时长</th>
							<th>性别</th>
							<th>操作</th>
						</tr>
					<?php
						$now = time();
						foreach( $data as $index => $value )
						{
					?>
						<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?>>
							<td><?php if ( $value['npc'] == 1 ) echo '<font color="red">N</font> '; ?><a href="set_user.php?id=<?php echo $value['user']; ?>" context_menu_type="user" uid="<?php echo $value['user']; ?>"><?php echo $value['name']; ?></a></td>
							<td><?php echo $value['logout'] > 0 ? date( 'm-d H:i:s', $value['logout'] ) : ''; ?></td>
							<td><?php echo date( 'm-d H:i:s', $value['login'] ); ?></td>
							<td><?php echo date( 'm-d H:i:s', $value['last'] ); ?></td>
							<td><?php $second = $now - $value['login']; if ( $second >= 86400 ) echo round( $second / 86400, 2 ) . '天'; else if ( $second >= 3600 ) echo round( $second / 3600, 2 ) . '小时'; else if ( $second >= 60 ) echo round( $second / 60 ) . '分钟'; else echo $second . '秒' ?></td>
							<td><?php echo ($value['sex']==0)?'男':'女';?></td>
							<td>
								<table width="80%">
									<tr align="center">
										<td width="50%"><a href="offline.php?id=<?php echo $value['user']; ?>" onclick="return confirm('确认让 <?php echo $value['name']; ?> 下线吗？')">下线</a></td>
										<td><a href="set_user.php?id=<?php echo $value['user']; ?>">修改</a></td>
									</tr>
								</table>
							</td>
						</tr>
					<?php
						}
					?>
					</table>

					<div style="text-align:center; height:15px; padding-top:4px; font-size:12px;">记录数：<?php echo $bar['total']; ?>  <a href="<?php echo $bar['prev_link']; ?>"><img src="<?php echo $_template['img']; ?>prev.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;
					<?php
						foreach( $bar['number'] as $value )
						{
							if ( $value['is_current'] )
							{
					?>
						<span class="homeStockRise"><strong><?php echo $value['name']; ?></strong></span>&nbsp;
					<?php
							}
							else
							{
					?>
						<a href="<?php echo $value['link']; ?>"><?php echo $value['name']; ?></a>&nbsp;
					<?php
							}
					?>
					<?php
						}
					?>
					<a href="<?php echo $bar['next_link']; ?>"><img src="<?php echo $_template['img']; ?>next.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;[<?php echo $bar['current']; ?>/<?php echo $bar['page']; ?>]</div>

				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
		</form>
	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>