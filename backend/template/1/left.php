<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>dice</title>
<script src="<?php echo $_template['js']; ?>prototype.lite.js" type="text/javascript"></script>
<script src="<?php echo $_template['js']; ?>moo.fx.js" type="text/javascript"></script>
<script src="<?php echo $_template['js']; ?>moo.fx.pack.js" type="text/javascript"></script>
<style>
body {
	font:12px Arial, Helvetica, sans-serif;
	color: #000;
	background-color: #EEF2FB;
	margin: 0px;
}
#container {
	width: 100px;
}
H1 {
	font-size: 12px;
	margin: 0px;
	width: 100px;
	cursor: pointer;
	height: 30px;
	line-height: 20px;
}
H1 a {
	display: block;
	width: 100px;
	color: #000;
	height: 30px;
	text-decoration: none;
	moz-outline-style: none;
	background-image: url(<?php echo $_template['img']; ?>menu_bgs.gif);
	background-repeat: no-repeat;
	line-height: 30px;
	text-align: center;
	margin: 0px;
	padding: 0px;
}
.content{
	width: 100px;
	height: 26px;

}
.MM ul {
	list-style-type: none;
	margin: 0px;
	padding: 0px;
	display: block;
}
.MM li {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	list-style-type: none;
	display: block;
	text-decoration: none;
	height: 26px;
	width: 100px;
	padding-left: 0px;
}
.MM {
	width: 100px;
	margin: 0px;
	padding: 0px;
	left: 0px;
	top: 0px;
	right: 0px;
	bottom: 0px;
	clip: rect(0px,0px,0px,0px);
}
.MM a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	background-image: url(<?php echo $_template['img']; ?>menu_bg1.gif);
	background-repeat: no-repeat;
	height: 26px;
	width: 100px;
	display: block;
	text-align: center;
	margin: 0px;
	padding: 0px;
	overflow: hidden;
	text-decoration: none;
}
.MM a:visited {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	background-image: url(<?php echo $_template['img']; ?>menu_bg1.gif);
	background-repeat: no-repeat;
	display: block;
	text-align: center;
	margin: 0px;
	padding: 0px;
	height: 26px;
	width: 100px;
	text-decoration: none;
}
.MM a:active {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	background-image: url(<?php echo $_template['img']; ?>menu_bg1.gif);
	background-repeat: no-repeat;
	height: 26px;
	width: 100px;
	display: block;
	text-align: center;
	margin: 0px;
	padding: 0px;
	overflow: hidden;
	text-decoration: none;
}
.MM a:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	font-weight: bold;
	color: #006600;
	background-image: url(<?php echo $_template['img']; ?>menu_bg2.gif);
	background-repeat: no-repeat;
	text-align: center;
	display: block;
	margin: 0px;
	padding: 0px;
	height: 26px;
	width: 100px;
	text-decoration: none;
}
</style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EEF2FB">
  <tr>
    <td width="100" valign="top"><div id="container">
	  <h1 class="type"><a href="javascript:void(0)">内容管理</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?php echo $_template['img']; ?>menu_topline.gif" width="100" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
		  <li><a href="user.php" target="right_frame">用户管理</a></li>
		  <li><a href="props.php" target="right_frame">道具管理</a></li>
		  <li><a href="user_props.php" target="right_frame">消费管理</a></li>
		  <!--li><a href="message.php" target="right_frame">聊天管理</a></li-->
		  <li><a href="message2.php" target="message2">聊天列表</a></li>
		  <li><a href="notification.php" target="right_frame">消息管理</a></li>
		  <li><a href="friend.php" target="right_frame">关注管理</a></li>
		  <li><a href="pair.php" target="right_frame">单盘比赛</a></li>
		  <li><a href="online.php" target="right_frame">在线管理</a></li>
		  <li><a href="comment1.php" target="right_frame">说两句的</a></li>
          <li><a href="comment2.php" target="right_frame">回复管理</a></li>
		  <li><a href="album.php" target="right_frame">图片管理</a></li>
	      <li><a href="challenge.php" target="right_frame">挑战赛事</a></li>
		  <li><a href="integral.php" target="right_frame">骰魔大赛</a></li>
		  <li><a href="moving.php" target="right_frame">动态管理</a></li>
		  <li><a href="badword.php" target="right_frame">过滤管理</a></li>
		  <li><a href="task_queue.php" target="right_frame">任务队列</a></li>
		  <li><a href="intervene_real.php" target="right_frame">干预列表</a></li>
		  <li><a href="alert.php" target="right_frame">报警管理</a></li>
		  <!--<li><a href="multi.php" target="right_frame">多人比赛</a></li>
		  <li><a href="room.php" target="right_frame">房间管理</a></li>
		  <li><a href="news.php" target="right_frame">文章管理</a></li>
		  <li><a href="alert.php" target="right_frame">报警管理</a></li>
		  <li><a href="data.php" target="right_frame">数据统计</a></li>
		  <li><a href="quick.php" target="right_frame">一键操作</a></li>-->
        </ul>
	  </div>
      <h1 class="type"><a href="javascript:void(0)">系统管理</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?php echo $_template['img']; ?>menu_topline.gif" width="100" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
		  <li><a href="payment.php" target="right_frame">充值管理</a></li>
		  <li><a href="announce.php" target="right_frame">通知管理</a></li>
		  <li><a href="privilege.php" target="right_frame">权限管理</a></li>
          <li><a href="administrator.php" target="right_frame">用户管理</a></li>
		  <?php
			if ( in_array( $user, array( 1, 3, 6, 7 ) ) )
			{
		  ?>
		  <li><a href="configure.php" target="right_frame">配置管理</a></li>
		  <?php
			}
		  ?>
		  <li><a href="backend_log.php" target="right_frame">操作日志</a></li>
		  <!--<li><a href="service.php" target="_blank">服务管理</a></li>-->
		  <li><a href="error.php" target="right_frame">错误记录</a></li>
		  <li><a href="../cgp/host.php?h=touyou_1" target="right_frame">机器性能</a></li>
		  <li><a href="service_performance.php" target="right_frame">服务性能</a></li>
		  <li><a href="add_config.php" target="right_frame">更新设置值</a></li>
        </ul>
      </div>
	  <h1 class="type"><a href="javascript:void(0)">个人设置</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="<?php echo $_template['img']; ?>menu_topline.gif" width="100" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
          <li><a href="password.php" target="right_frame">修改密码</a></li>
		  <li><a href="logout.php" target="_parent" onclick="return confirm( '确认退出' )">退出系統</a></li>
        </ul>
      </div>
    </div>
        <script type="text/javascript">
		var contents = document.getElementsByClassName('content');
		var toggles = document.getElementsByClassName('type');

		var myAccordion = new fx.Accordion(
			toggles, contents, {opacity: true, duration: 400}
		);
		//myAccordion.showThisHideOpen(contents[0]);
	</script>
        </td>
  </tr>
</table>
</body>
</html>