<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
</style>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript">
var user_1 = '<?php echo $user_array[0]['id']; ?>';
var user_2 = '<?php echo $user_array[1]['id']; ?>';
function change_gold_profit( id )
{
	$( '#gold_' + user_1 ).hide();
	$( '#profit_' + user_1 ).hide();
	$( '#gold_' + user_2 ).hide();
	$( '#profit_' + user_2 ).hide();
	$( '#gold_' + id ).show();
	$( '#profit_' + id ).show();
}
function modify()
{
	if ( $( '#ai' ).val() == '' )
	{
		alert( '请选择比赛策略' );
		return false;
	}

	var message = 'user=' + $( '#user' ).val() + '&npc=' + $( '#data_uid' ).val() + '&times_1=' + $( '#data_times_1' ).val() + '&times_2=' + $( '#data_times_2' ).val() + '&bet_1=' + $( '#data_bet_1' ).val() + '&bet_2=' + $( '#data_bet_2' ).val() + '&accept=1&try=1&interval_1=' + $( '#data_interval_1' ).val() + '&interval_2=' + $( '#data_interval_2' ).val() + '&start=' + $( '#data_start' ).val() + '&ai=' + $( '#ai' ).val();
	//alert( message );return;
	$.ajax
	(
		{
			type : "POST",
			url : "set_npc_challenge.php",
			dataType : 'json',
			data : message,
			success : function( data )
				{
					$( '#modify_button' ).attr( 'disabled', false );
					alert( data.message );
				}
		}
	);
}
</script>
<body onload="$( '#data_uid' ).focus()">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">挑战设置</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：可以在此设置NPC挑战用户。</td>
			  </tr>
			   <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td class="left_txt">针对用户：<select id="user" onchange="change_gold_profit(this.value)"><?php foreach( $user_array as $user ) { ?><option value="<?php echo $user['id']; ?>"><?php echo $user['name']; ?></option><?php } ?></select> 金币总额：<span id="gold_<?php echo $user_array[0]['id']; ?>"><?php echo $user_array[0]['gold']; ?></span><span id="gold_<?php echo $user_array[1]['id']; ?>" style="display:none;"><?php echo $user_array[1]['gold']; ?></span> 当日盈利：<span id="profit_<?php echo $user_array[0]['id']; ?>"><?php echo $user_array[0]['profit']; ?></span><span id="profit_<?php echo $user_array[1]['id']; ?>" style="display:none;"><?php echo $user_array[1]['profit']; ?></span></td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">指定挑战者：</td>
							<td width="3%">&nbsp;</td>
							<td width="45%" height="30" class="left_txt"><input type="text" id="data_uid">&nbsp;输入用户名或UID，多个用户用逗号隔开</td>
							<td width="32%" height="30" class="left_txt"></td>
						</tr>
						<tr bgcolor="#E2E7ED">
							<td width="20%" height="30" align="right" class="left_txt2">挑战人次：</td>
							<td width="3%">&nbsp;</td>
							<td width="45%" height="30" class="left_txt"><input type="text" id="data_times_1" size="4" value="2"> ~ <input type="text" id="data_times_2" size="4" value="10">&nbsp;设置对用户发起挑战的人次，上限不要设置太大，以免过分明显</td>
							<td width="32%" height="30" class="left_txt"></td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">挑战金设置：</td>
							<td width="3%">&nbsp;</td>
							<td width="45%" height="30" class="left_txt"><input type="text" id="data_bet_1" size="4" value="300"> ~ <input type="text" id="data_bet_2" size="4" value="1000">&nbsp;设置每次挑战发起的挑战金</td>
							<td width="32%" height="30" class="left_txt"></td>
						</tr>
						<tr bgcolor="#E2E7ED">
							<td width="20%" height="30" align="right" class="left_txt2">比赛策略</td>
							<td width="3%">&nbsp;</td>
							<td width="45%" height="30" class="left_txt"><select id="ai"><option value=""></option><option value="3">高级</option><option value="2">中级</option></select></td>
							<td width="32%" height="30" class="left_txt"></td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">不同NPC挑战发起间隔时间：</td>
							<td width="3%">&nbsp;</td>
							<td width="45%" height="30" class="left_txt"><input type="text" id="data_interval_1" size="4" value="0.5">小时 ~ <input type="text" id="data_interval_2" size="4" value="1.5">小时&nbsp;设置挑战时间间隔，以免调整过于频密，可设小数</td>
							<td width="32%" height="30" class="left_txt"></td>
						</tr>
						<tr bgcolor="#E2E7ED">
							<td width="20%" height="30" align="right" class="left_txt2">开始时间：</td>
							<td width="3%">&nbsp;</td>
							<td width="45%" height="30" class="left_txt"><input type="text" id="data_start" value="<?php echo date( 'Y-m-d H:i:s', time() + 600 ); ?>">&nbsp;设置任务执行开始时间</td>
							<td width="32%" height="30" class="left_txt"></td>
						</tr>
						<tr>
							<td colspan="3" align="center">
								<table width="30%">
									<tr align="center">
										<td><input type="button" value="保存" id="modify_button" onclick="modify()" /></td>
										<td><input type="button" value="返回" onclick="window.history.go(-1)" /></td>
									</tr>
								</table>
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>