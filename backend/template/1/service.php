<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
</style>
<script type="text/javascript">
function service_1()
{
	$( '#btn_1' ).val( '基础服务运行中...' );
	$( '#btn_1' ).unbind();

	$.ajax
	(
		{
			type : "GET",
			url : "_service.php",
			data : "type=1",
			success : function( data )
				{
					if ( data != 'ok' ) alert( data );
					else setTimeout( service_1, 1000 );
				}
		}
	);
}
function service_2()
{
	$( '#btn_2' ).val( '高级服务运行中...' );
	$( '#btn_2' ).unbind();

	$.ajax
	(
		{
			type : "GET",
			url : "_service.php",
			data : "type=2",
			success : function( data )
				{
					if ( data != 'ok' ) alert( data );
					else setTimeout( service_2, 1000 );
				}
		}
	);
}
function service_3()
{
	$( '#btn_3' ).val( '营销服务运行中...' );
	$( '#btn_3' ).unbind();

	$.ajax
	(
		{
			type : "GET",
			url : "_service.php",
			data : "type=3",
			success : function( data )
				{
					if ( data != 'ok' ) alert( data );
					else setTimeout( service_3, 1000 );
				}
		}
	);
}
</script>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css">
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">服务管理</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

<p>关闭本页即可停止运行服务</p>
<p><input type="button" value="启动基础服务" id="btn_1" onclick="service_1()"></p>
<p><input type="button" value="启动高级服务" id="btn_2" onclick="service_2()"></p>
<p><input type="button" value="启动营销服务" id="btn_3" onclick="service_3()"></p>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>