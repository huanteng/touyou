<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
</style>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript">
function modify()
{
	$( '#modify_button' ).attr( 'disabled', true );

	var message = 'status=' + $( '#status' ).val() + '&id=<?php echo $id; ?>';
	if ( $( '#pass' ).val() != '' ) message += '&pass=' + encodeURIComponent( $( '#pass' ).val() );

	$.ajax
	(
		{
			type : "POST",
			url : "set_administrator.php",
			dataType : 'json',
			data : message,
			success : function( data )
				{
					$( '#modify_button' ).attr( 'disabled', false );
					alert( data.message );
					if ( data.status == 0 ) window.location.href = document.referrer;
				}
		}
	);
}
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">修改用户</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：可以在此修改管理员。</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">名称：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><?php echo $name; ?></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr bgcolor="#E2E7ED">
							<td width="20%" height="30" align="right" class="left_txt2">状态：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><select id="status"><?php foreach( $status_dict as $value => $name ) { ?><option value="<?php echo $value; ?>"<?php if ( $status == $value ) echo ' selected'; ?>><?php echo $name; ?></option><?php } ?></select></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">密码：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="password" id="pass"></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3" align="center">
								<table width="30%">
									<tr align="center">
										<td><input type="button" value="修改" id="modify_button" onclick="modify()" /></td>
										<td><input type="button" value="返回" onclick="window.history.go(-1)" /></td>
									</tr>
								</table>
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>