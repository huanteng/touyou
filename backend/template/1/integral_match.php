<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script src="<?php echo $_template['css']; ?>javascript/jquery.js" type="text/javascript"></script>
<script>
function selected_all()
{
	$('input:checkbox[name=\'select\']').attr('checked',$('#btn_select_all').attr('checked'));
}
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">比赛管理</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">
	<form method="get">
	<input type="hidden" name="_start" value="<?php echo $_start; ?>"><input type="hidden" name="_end" value="<?php echo $_end; ?>"><input type="hidden" name="history" value="<?php echo $history; ?>">
	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是骰魔大赛列表。</td>
			  </tr>
			   <tr>
				<td align="left" class="left_txt">用户名 <input type="text" name="_name" size="10" value="<?php echo $_name; ?>"> <input type="submit" value="搜索"></td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>

				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
						<tr class="title f1">
							<th width="5%">序号</th>
							<th width="10%">时间</th>
							<th width="5%">积分</th>
					        <th>内容</th>
						</tr>
					<?php
						foreach( $data as $index => $value )
						{
					?>
						<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?>>
							<td><?php echo $value['id']; ?></td>
							<td><?php echo date( 'Y-m-d H:i', $value['time'] ); ?></td>
							<td><?php echo $value['style']; ?></td>
							<td align="left">&nbsp;<a target="_blank" href="get_pair.php?id=<?php echo $value['vs']; ?>"><?php echo $value['name1']; ?><?php echo ($value['winner']==$value['user_1'])?'<font color="red">(赢)</font>':''; ?> 与 <?php echo $value['name2']; ?><?php echo ($value['winner']==$value['user_2'])?'<font color="red">(赢)</font>':''; ?> 比赛</a>&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:blue;" href="towin.php?id=<?php echo $value['vs']; ?>">设npc赢</a></td>
						</tr>
					<?php
						}
					?>
					</table>

					<div style="text-align:center; height:15px; padding-top:4px; font-size:12px;">记录数：<?php echo $bar['total']; ?>  <a href="<?php echo $bar['prev_link']; ?>"><img src="<?php echo $_template['img']; ?>prev.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;
					<?php
						foreach( $bar['number'] as $value )
						{
							if ( $value['is_current'] )
							{
					?>
						<span class="homeStockRise"><strong><?php echo $value['name']; ?></strong></span>&nbsp;
					<?php
							}
							else
							{
					?>
						<a href="<?php echo $value['link']; ?>"><?php echo $value['name']; ?></a>&nbsp;
					<?php
							}
					?>
					<?php
						}
					?>
					<a href="<?php echo $bar['next_link']; ?>"><img src="<?php echo $_template['img']; ?>next.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;[<?php echo $bar['current']; ?>/<?php echo $bar['page']; ?>]</div>

				</td>
			  </tr>

			  <tr><td>&nbsp;</td></tr>
			  <tr>
			  	  <td>
					<fieldset>
					  <legend>说明：</legend>
						<p>用户名 点击可查看用户个人资料</p>
						<p>用户名 右键可调出命令菜单</p>
						<p>操作区</p>
						<p>  --用户1/2 点击可查看该条信息相关两用户所有比赛条目</p>
						<p>  --营销 对用户进行挑战设置</p>
					</fieldset>
				</td>
			  </tr>

			</table></td>
		  </tr>
		</table>
		</form>
	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>