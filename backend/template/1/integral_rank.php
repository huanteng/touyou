<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
</style>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript">

</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">获奖名单</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：可以在此查看骰魔大赛 <span style="color:#FF6600;"><?php echo date( 'Y-m-d H:00', $time ); ?> - <?php echo date( 'Ym-d H:15', $time ); ?></span> 的具体获奖名单。</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
					<table width="50%" border="1">
						<tr align="center">
							<td>排名</td>
							<td>用户</td>
							<td>积分</td>
							<td>赢 / 输</td>
							<td>金币</td>
							<td>操作</td>
						</tr>
					<?php
						foreach( $data as $value )
						{
					?>
						<tr align="center">
							<td><?php echo $value['rank']; ?></td>
							<td><?php if ( $value['npc'] == 1 ) echo '<font color="red">N</font> '; ?><?php echo $value['name']; ?></td>
							<td><?php echo $value['score']; ?></td>
							<td><?php echo $value['win']; ?> / <?php echo $value['lost']; ?></td>
							<td><?php echo $value['gold']; ?></td>
							<td><a href="../_back/vs.php?user=<?php echo $value['name']; ?>&table=vs_history&type=1&start=<?php echo date( 'Y-m-d H:00', $time ); ?>">查看比赛</a></td>
						</tr>
					<?php
						}
					?>
					<?php if ( count( $real_array ) ) echo '<tr><td colspan="10"><br/>&nbsp;</td></tr>'; ?>
					<?php
						foreach( $real_array as $value )
						{
					?>
						<tr align="center">
							<td><?php echo $value['rank']; ?></td>
							<td><?php echo $value['name']; ?></td>
							<td><?php echo $value['score']; ?></td>
							<td><?php echo $value['win']; ?> / <?php echo $value['lost']; ?></td>
							<td><?php echo $value['gold']; ?></td>
							<td><a href="integral_match.php?history=<?php echo $history; ?>&_start=<?php echo $_start; ?>&_end=<?php echo $_end; ?>&_name=<?php echo $value['name']; ?>">查看比赛</a></td>
						</tr>
					<?php
						}
					?>
					</table>

				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>