<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
</style>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>common.js"></script>	
<script type="text/javascript">
function remove_item(id)
{
	$('#t'+id).remove();
}	
	
function check()
{
	for(i=1;i<=2;i++)
	{
		if ( $( '#user_'+i ).val() == '' )
		{
			alert( '请输入第'+i+'用户名' );
			$( '#user_'+i ).focus();
			return false;
		}
		
		if ( $( '#file_'+i ).val() == '' )
		{
			alert( '请选择需要上传的文件' );
			$( '#file_'+i ).focus();
			return false;
		}
		
		var s = $( '#file_'+i ).val();
		var sss = s.lastIndexOf(".");
	    if(sss < 0)
	    {
		     alert("文件格式不正确");
		     $( '#file_'+i ).focus();
		     return false;
	    }
	    
	    var var1 = s.substring(sss+1) ;
	    if(var1 != "jpg")
	    {
	       alert("暂时只支持 JPG 格式图片");
	       $( '#file_'+i ).focus();
	       return false;
		}
	}
	
	return true;
}
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">预添加图片（营销）</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：可以在此预添加图片，图片将在计划时间内显示。</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
				<form method="post" onSubmit="return check()" enctype="multipart/form-data">
							
						<?php for($i=1;$i<=4;$i++){?>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" id="t<?php echo $i;?>">		
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">用户名：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="user_<?php echo $i;?>" name="user[]"></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">上传文件：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="file" id="file_<?php echo $i;?>" name="file[]"></td>
							<td width="45%" height="30" class="left_txt">&nbsp;暂时只支持 JPG 格式图片</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">时间：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="time_<?php echo $i;?>" name="time[]" value="<?php echo date('Y-m-d H:i:s',time()+$i*600+rand(0,59));?>">&nbsp;<a href="javascript:add_time(300,<?php echo $i;?>)" title="增加5分钟">+5m</a>&nbsp;&nbsp;<a href="javascript:add_time(3600,<?php echo $i;?>)" title="增加1小时">+1h</a>&nbsp;&nbsp;<a href="javascript:add_time(-300,<?php echo $i;?>)" title="减少5分钟">-5m</a>&nbsp;&nbsp;<a href="javascript:add_time(-3600,<?php echo $i;?>)" title="减少1小时">-1h</a>&nbsp;&nbsp;<a href="javascript:add_time(0,<?php echo $i;?>)" title="自定义">+?m</a></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr><td width="20%" height="30" align="right" class="left_txt2">&nbsp;</td><td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;<a title="删除这项" href="#" onclick="remove_item(<?php echo $i;?>);return false;" >删除</a></td></tr>
						<tr bgcolor="#E2E7ED"><td colspan="4">&nbsp;</td></tr>
					</table>	
						<?php }?>	
							
					<table width="100%" border="0" cellspacing="0" cellpadding="0">			
						<tr>
							<td colspan="3" align="center"><br/>
								<table width="30%">
									<tr align="center">
										<td><input type="submit" value="添加"/></td>
										<td><input type="button" value="返回" onclick="window.history.go(-1)" /></td>
									</tr>
								</table>
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</form>			
				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>