<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_template['css']; ?>redmond/jquery-ui-1.7.custom.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery-ui-1.7.3.datepicker.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.contextmenu.r2.packed.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>dice.contextmenu.js"></script>
<script>
function selected_all()
{
	$('input:checkbox[name=\'id\[\]\']').attr('checked',$('#btn_select_all').attr('checked'));
}
function delete_all()
{
	if( confirm('确定删除吗？') )
	{
		$('#form2').submit();
	}
}

function set_logo(id,uid)
{
	if( confirm('确定设置这张图片为头像吗？') )
	{
		$.ajax({
			url: "../_back/data/album.php?method=set_logo&id="+id+"&uid="+uid,
			dataType: "json",
			success: function (data) {
				alert('设置成功');
			}
		});	
	}
}

$(function(){
	$('#start_date,#end_date').datepicker({
		inline: false,
		dateFormat: 'yy-mm-dd'
	});
});
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">图片管理</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是图片列表，您可以<a href="add_album.php" style="color:#FF6600;">添加</a>或修改图片。</td>
			  </tr>
	          <form id="form1" method="get">
			  <tr>
				<td align="left" class="left_txt">
		用户名： <input type="text" name="_name" size="10" value="<?php echo $_name; ?>">
		性别：
		<select name="_sex">
			<option value=""<?php if($_sex!='0' || $_sex!='1') echo ' selected'?>>全部</option>
			<option value="0"<?php if($_sex=='0') echo ' selected'?>>男</option>
			<option value="1"<?php if($_sex=='1') echo ' selected'?>>女</option>
		</select>
		NPC：
		<select name="_npc">
			<option value=""<?php if($_npc!='0' || $_npc!='1') echo ' selected'?>>全部</option>
			<option value="1"<?php if($_npc=='1') echo ' selected'?>>是</option>
			<option value="0"<?php if($_npc=='0') echo ' selected'?>>否</option>
		</select>
		时间：<input type="text" name="_start" id="start_date" size="18" value="<?php echo $_start; ?>"> ～ <input type="text" name="_end" id="end_date" size="18" value="<?php echo $_end; ?>">
		<input type="submit" value="搜索">
		<span style="float:right;"><input type="button" value="自动发布图片" onclick="window.location.href='auto_add_album.php'"></span>
				</td>
			  </tr>
			  </form>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
				<form id="form2" method="post">
				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
						<tr class="title f1">
							<th width="90%" colspan="20">说明：这里可以管理用户上传的单张照片，包括删除、评分查看、评分设置</th>
						</tr>
						<tr align="center">
					<?php
						foreach( $data as $index => $value )
						{
							if( $index % 4 == 0 )
							{
					?>
						</tr><tr align="center" class="f2">
					<?php
							}
					?>
							<td width="15%"><a href="auto_vote_album.php?id=<?php echo $value['id']; ?>"><img border="0" src="<?php echo $value['path']; ?>.s.jpg" width="120" height="120" /></a><br/><input type="checkbox" name="id[]" value="<?php echo $value['id']; ?>"/>选择&nbsp;<?php if($value['type']==2) echo '视频认证照';else if($value['type']==1) echo '个人照片';else echo '其它照片'; ?><br/><input type="button" value="设为头像" onclick="set_logo(<?php echo $value['id']; ?>,<?php echo $value['user']; ?>)"/></td>
							<td width="10%">
								<table width="100%">
								<tr align="center"><td>大小：<?php echo $value['size']; ?> KB</td></tr>
								<tr align="center"><td>上传人：<?php if ( $value['npc'] == 1 ) echo '<font color="red">N</font> '; ?><a context_menu_type="user" uid="<?php echo $value['user']; ?>" href="./set_user.php?id=<?php echo $value['user']; ?>"><?php echo $value['name']; ?></a></td></tr>
								<tr align="center"><td>时间：<?php echo date('m-d H:i:s',$value['time']); ?></td></tr>
								<tr align="center"><td>被评次数：<?php echo $value['times']; ?></td></tr>
								<tr align="center"><td>平均分：<?php echo $value['times'] > 0 ? round( $value['score'] / $value['times'], 2 ) : 0; ?></td></tr>
								<tr align="center"><td><a href="auto_vote_album.php?id=<?php echo $value['id']; ?>">评论管理</a></td></tr>
								<tr align="center"><td><a href="./album.php?uid=<?php echo $value['user']; ?>">全部</a></td></tr>
								</table>
							</td>

					<?php
						}
					?>
						</tr>
					</table>

					<div style="text-align:center; height:25px; padding-top:4px; font-size:12px;">

					<span style="float: left;"><input type="checkbox" onclick="selected_all()" id="btn_select_all" />全选&nbsp;<input type="button" value="删除" onclick="delete_all()" id="btn_delete_all"/></span>

					记录数：<?php echo $bar['total']; ?>  <a href="<?php echo $bar['prev_link']; ?>"><img src="<?php echo $_template['img']; ?>prev.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;
					<?php
						foreach( $bar['number'] as $value )
						{
							if ( $value['is_current'] )
							{
					?>
						<span class="homeStockRise"><strong><?php echo $value['name']; ?></strong></span>&nbsp;
					<?php
							}
							else
							{
					?>
						<a href="<?php echo $value['link']; ?>"><?php echo $value['name']; ?></a>&nbsp;
					<?php
							}
					?>
					<?php
						}
					?>
					<a href="<?php echo $bar['next_link']; ?>"><img src="<?php echo $_template['img']; ?>next.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;[<?php echo $bar['current']; ?>/<?php echo $bar['page']; ?>]</div>

				</td>
			  </tr>

			  <tr><td>&nbsp;</td></tr>
			  <tr>
			  	  <td>
					<fieldset>
					  <legend>说明：</legend>
						<p>用户名 点击可查看用户个人资料</p>
						<p></p>
						<p>用户名 右键可调出命令菜单</p>
						<p></p>
						<p>操作区</p>
					    <p></p>
					    <p>     --全部 点击可查看改发布者全部“说两句”内容</p>
					    <p>     --评分管理 查看该照片的所有评分及进行单一评分设置</p>
					</fieldset>
				</td>
			  </tr>

			</table></form></td>
		  </tr>
		</table>
	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>