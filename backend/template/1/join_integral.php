<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
</style>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript">
function create()
{
	if ( $( '#name' ).val() == '' )
	{
		alert( '请输入用户名' );
		$( '#name' ).focus();
		return false;
	}

	$( '#create_button' ).attr( 'disabled', true );

	var message = 'name=' + encodeURIComponent( $( '#name' ).val() ) + '&time=' + encodeURIComponent( $( '#time' ).val() );

	$.ajax
	(
		{
			type : "POST",
			url : "join_integral.php",
			dataType : 'json',
			data : message,
			success : function( data )
				{
					$( '#create_button' ).attr( 'disabled', false );
					alert( data.message );
					if ( data.status == 0 ) window.location.href = document.referrer;
				}
		}
	);
}
</script>
<body onload="$( '#name' ).focus()">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">手工报名</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：可以在此报名参加骰魔大赛事。</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">时间：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30">
								<select id="time">
								<?php
									$start = strtotime( date( 'Y-m-d H:00:00' ) );
									if ( intval( date( 'i' ) ) > 15 ) $start += 3600;
									$end = $start + 24 * 3600;

									for( $time = $start; $time <= $end; $time += 3600 )
									{
								?>
									<option value="<?php echo date( 'Y-m-d H:00:00', $time ); ?>"><?php echo date( 'Y-m-d H:00:00', $time ); ?></option>
								<?php
									}
								?>
								</select>
							</td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr bgcolor="#E2E7ED">
							<td width="20%" height="30" align="right" class="left_txt2">用户：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="name"></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3" align="center">
								<table width="30%">
									<tr align="center">
										<td><input type="button" value="添加" id="create_button" onclick="create()" /></td>
										<td><input type="button" value="返回" onclick="window.history.go(-1)" /></td>
									</tr>
								</table>
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>