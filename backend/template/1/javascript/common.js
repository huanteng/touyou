var total_time_item = 0;
function nozeros( value )
{
	return value.length > 1 && value.substr( 0, 1 ) == "0" ? value.substr( 1 ) : value;
}
function unixtimetodate( value )
{
	var theDate = new Date( value * 1000 );
	return theDate.getFullYear() + '-' + ( theDate.getMonth() + 1 ) + '-' + theDate.getDate() + ' ' + theDate.getHours() + ':' + theDate.getMinutes() + ':' + theDate.getSeconds();
}
function datetounixtime( value )
{
	var temp = value.split( " " );
	var temp1 = temp[0].split( "-" );
	var temp2 = temp[1].split( ":" );

	var year = temp1[0];
	var month = nozeros( temp1[1] ) - 1;
	var day = nozeros( temp1[2] );

	var hour = nozeros( temp2[0] );
	var minute = nozeros( temp2[1] );
	var second = nozeros( temp2[2] );

	var humDate = new Date( year, month, day, hour, minute, second );
	return ( humDate.getTime() / 1000 );
}
function CheckNumber( s )
{
var reg = /^\-?[0-9]+\.?[0-9]{0,9}$/;
return reg.test( s );
}
function add_time( second, item )
{
	if ( second == 0 )
	{
		var minute = window.prompt( '请输入要延后的分钟，负数表示提前' );

		if ( minute != null && CheckNumber( minute ) )
		{
			add_time( minute * 60, item );
		}
	}
	else
	{
		$( "#time_" + item ).val( unixtimetodate( datetounixtime( $( "#time_" + item ).val() ) + second ) );

		for( var i = item + 1; i < total_time_item; i++ )
		{
			if ( document.getElementById( "time_" + i ) != null )
			{
				$( "#time_" + i ).val( unixtimetodate( datetounixtime( $( "#time_" + i ).val() ) + second ) );
			}
		}
	}
}

String.prototype.format = function(args) { 
if (arguments.length>0) { 
var result = this; 
if (arguments.length == 1 && typeof (args) == "object") { 
for (var key in args) { 
var reg=new RegExp ("({"+key+"})","g"); 
result = result.replace(reg, args[key]); 
} 
} 
else { 
for (var i = 0; i < arguments.length; i++) { 
if(arguments[i]==undefined) 
{ 
return ""; 
} 
else 
{ 
var reg=new RegExp ("({["+i+"]})","g"); 
result = result.replace(reg, arguments[i]); 
} 
} 
} 
return result; 
} 
else { 
return this; 
} 
} 

/* 使用ajax方式post，将json结果交回调函数处理
 * file：接受post的文件名
 * data：postdata，为key=value数组
 * func：回调函数
 */
function ajax_post(file, data, func)
{
	var postdata = "";
	for( var k in data )
	{
		if( postdata != "" ) postdata += "&";
		postdata += k + "=" + data[k];
	}
	$.ajax
	(
		{
			type : "POST",
			url : file,
			dataType : 'json',
			async : false,
			data : postdata,
			success : function(data) {func(data);}
		}
	);
}

