<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script src="<?php echo $_template['css']; ?>javascript/jquery.js" type="text/javascript"></script>
<script>
function selected_all()
{
	$('input:checkbox[name=\'select\']').attr('checked',$('#btn_select_all').attr('checked'));
}
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">骰魔大赛</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">
	<form method="get">
	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是赛事列表，您可以查看<a target="_blank" href="integral_active.php" style="color:#FF6600;">最新状态</a>。</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>

				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
						<tr class="title f1">
							<th width="8%">序号</th>
							<th width="8%">时间</th>
							<th width="8%">人数</th>
							<th width="8%">NPC</th>
							<th width="8%">非NPC</th>
							<th width="10%">比赛</th>
							<th width="10%">奖池</th>
							<th width="10%">利润</th>
							<th width="10%">状态</th>
					        <th>操作</th>
						</tr>
					<?php
						foreach( $data as $index => $value )
						{
					?>
						<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?>>
							<td><?php echo $value['id']; ?></td>
							<td><?php echo date( 'm-d H:i', $value['time'] ); ?></td>
							<td><?php echo $value['person']; ?></td>
							<td><?php echo $value['npc']; ?></td>
							<td><?php echo $value['real']; ?></td>
							<td><?php echo $value['match']; ?></td>
							<td><?php echo $value['gold']; ?></td>
							<td><?php echo $value['profit']; ?></td>
							<td><?php echo $value['status']; ?></td>
							<td>
								<table width="80%">
									<tr align="center">
										<td><a target="_blank" href="integral_rank.php?history=<?php echo $value['_status'] == 3 ? '1' : '0'; ?>&time=<?php echo $value['time']; ?>">获奖名单</a></td>
										<td><a target="_blank" href="integral_match.php?history=<?php echo $value['_status'] == 3 ? '1' : '0'; ?>&_start=<?php echo $value['time']; ?>&_end=<?php echo ( $value['time'] + 1800 ); ?>">相关比赛</a></td>
									</tr>
								</table>
							</td>
						</tr>
					<?php
						}
					?>
					</table>

					<div style="text-align:center; height:15px; padding-top:4px; font-size:12px;">记录数：<?php echo $bar['total']; ?>  <a href="<?php echo $bar['prev_link']; ?>"><img src="<?php echo $_template['img']; ?>prev.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;
					<?php
						foreach( $bar['number'] as $value )
						{
							if ( $value['is_current'] )
							{
					?>
						<span class="homeStockRise"><strong><?php echo $value['name']; ?></strong></span>&nbsp;
					<?php
							}
							else
							{
					?>
						<a href="<?php echo $value['link']; ?>"><?php echo $value['name']; ?></a>&nbsp;
					<?php
							}
					?>
					<?php
						}
					?>
					<a href="<?php echo $bar['next_link']; ?>"><img src="<?php echo $_template['img']; ?>next.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;[<?php echo $bar['current']; ?>/<?php echo $bar['page']; ?>]</div>

				</td>
			  </tr>

			  <tr><td>&nbsp;</td></tr>
			  <tr>
			  	  <td>
					<fieldset>
					  <legend>说明：</legend>
						<p></p>
					</fieldset>
				</td>
			  </tr>

			</table></td>
		  </tr>
		</table>
		</form>
	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>