<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.blockUI.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.contextmenu.r2.packed.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>dice.contextmenu.js"></script>
<script type="text/javascript">
function selected_all()
{
	$('input:checkbox[name=\'id\[\]\']').attr('checked',$('#btn_select_all').attr('checked'));
}
function cancel_all()
{
	if( confirm('确定忽略吗？') )
	{
		$('input:checkbox[name=\'id\[\]\']').each(function(){
		
			if( $(this).attr('checked') == true )
			{
				$.ajax
				(
					{
						type : "GET",
						url : "cancel_alert.php",
						dataType : 'html',
						async : true,
						data : 'type='+$(this).attr('t')+'&id='+$(this).val(),
						success : function( result )
							{
								mark( $(this).val() );
							}
					}
				);
			}
				
		});
	}
}	
function mark( id )
{
	$( '#tr_' + id ).css( 'background-color', 'gray' );
}
var message_id = 0;
function reply( id )
{
	message_id = id;
	$( '#reply_div_from' ).text( $( '#content_' + id ).text() );
	$.blockUI( { css: { width : '450px', cursor : null }, message : $( '#reply_div' ) } );
	$( '#reply_div_to' ).focus();
}
function _reply()
{
	var sex = '';
	var temp = [] ;
	if( $("#male").attr('checked') == true ) temp.push( 0 );
	if( $("#female").attr('checked') == true ) temp.push( 1 );
	sex = temp.join(',');

	$.ajax
	(
		{
			type : "POST",
			url : "comment2.php",
			dataType : 'json',
			async : false,
			data : 'op=reply&name=' + encodeURIComponent( $( '#reply_npc' ).val() ) + '&content=' + encodeURIComponent( $( '#reply_div_to' ).val() ) + '&id=' + message_id + '&time=' + $( '#time_1' ).val() + '&sex='+sex,
			success : function( result )
				{
					$( '#reply_div_to' ).val( '' );
					$.unblockUI();
				}
		}
	);

}
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">监控列表</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">
	<form method="get">
	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是<?php echo $base['name']; ?>的监控，你也可以<a href="check_alert.php?id=<?php echo $base['id']; ?>" style="color:#FF6600;">点击这里</a>重新检查监控。 <input type="button" value="返回" onclick="window.history.go(-1)" /></td>
			  </tr>
			  <tr>
				<td align="left" class="left_txt"></td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>

				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">

				<?php
					if ( $base['id'] == 1000 )
					{
				?>
					<tr class="title f1">
						<th width="8%">选择</th>
						<th width="8%">时间</th>
						<th width="10%">非NPC</th>
						<th width="10%">NPC</th>
						<th>内容</th>
						<th width="10%">操作</th>
					</tr>
					<?php
						foreach( $extend as $index => $value )
						{
					?>
					<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?> id="tr_<?php echo $value['id']; ?>">
						<td><input type="checkbox" name="id[]" value="<?php echo $value['id']; ?>" t="<?php echo $base['id'];?>"/></td>
						<td><?php echo $value['time']; ?></td>
						<td><a context_menu_type="user" uid="<?php echo $value['sender'] ?>" href="./set_user.php?id=<?php echo $value['sender'] ?>"><?php echo $value['sender_name']; ?></a></td>
						<td><font color="red">N</font> <a context_menu_type="user" uid="<?php echo $value['receiver'] ?>" href="./set_user.php?id=<?php echo $value['receiver'] ?>"><?php echo $value['receiver_name']; ?></a></td>
						<td align="left"><?php echo $value['content']; ?></td>
						<td>
							<table width="80%">
								<tr align="center">
									<td><a href="message.php?_title=<?php echo urlencode( '聊天：' . $value['sender_name'] . ' 与 ' . $value['receiver_name'] ); ?>&owner=<?php echo $value['receiver']; ?>&conversation=<?php echo $value['sender']; ?>,<?php echo $value['receiver']; ?>" target="_blank" onclick="mark( <?php echo $value['id']; ?> )">处理</a></td>
									<td><a href="cancel_alert.php?type=1000&id=<?php echo $value['id']; ?>" onclick="mark( <?php echo $value['id']; ?> );return confirm('确认忽略？')">忽略</a></td>
								</tr>
							</table>
						</td>
					</tr>
					<?php
						}
					?>
				<?php
					}
				?>

				<?php
					if ( $base['id'] == 1001 )
					{
				?>
					<tr class="title f1">
						<th width="8%">选择</th>
						<th width="8%">时间</th>
						<th width="10%">非NPC</th>
						<th width="10%">NPC</th>
						<th>说两句</th>
						<th width="8%">操作</th>
					</tr>
					<?php
						foreach( $extend as $index => $value )
						{
					?>
					<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?> id="tr_<?php echo $value['id']; ?>">
						<td><input type="checkbox" name="id[]" value="<?php echo $value['id']; ?>" t="<?php echo $base['id'];?>"/></td>
						<td><?php echo $value['time']; ?></td>
						<td><a context_menu_type="user" uid="<?php echo $value['sender'] ?>" href="./set_user.php?id=<?php echo $value['sender'] ?>"><?php echo $value['sender_name']; ?></a></td>
						<td><font color="red">N</font> <a context_menu_type="user" uid="<?php echo $value['npc_id'] ?>" href="./set_user.php?id=<?php echo $value['npc_id'] ?>"><?php echo $value['npc_name']; ?></a></td>
						<td align="left"> <?php echo $value['content']; ?></td>
						<td>
							<table width="80%">
								<tr align="center">
									<td><a href="comment2.php?_title=<?php echo urlencode( '说两句：' . $value['npc_name'] . ' 被回复' ); ?>&parent=<?php echo $value['id']; ?>" target="_blank" onclick="mark( <?php echo $value['id']; ?> )">处理</a></td>
									<td><a href="cancel_alert.php?type=1001&id=<?php echo $value['id']; ?>" onclick="mark( <?php echo $value['id']; ?> );return confirm('确认忽略？')">忽略</a></td>
								</tr>
							</table>
						</td>
					</tr>
					<?php
						}
					?>
				<?php
					}
				?>

				<?php
					if ( $base['id'] == 1002 )
					{
				?>
					<tr class="title f1">
						<th width="8%">选择</th>
						<th width="8%">时间</th>
						<th width="10%">非NPC</th>
						<th width="10%">NPC</th>
						<th>内容</th>
						<th width="8%">操作</th>
					</tr>
					<?php
						foreach( $extend as $index => $value )
						{
					?>
					<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?> id="tr_<?php echo $value['id']; ?>">
						<td><input type="checkbox" name="id[]" value="<?php echo $value['id']; ?>" t="<?php echo $base['id'];?>"/></td>
						<td><?php echo $value['time']; ?></td>
						<td><a context_menu_type="user" uid="<?php echo $value['user'] ?>" href="./set_user.php?id=<?php echo $value['user'] ?>"><?php echo $value['name']; ?></a></td>
						<td><font color="red">N</font> <a context_menu_type="user" uid="<?php echo $value['sub_parent_user'] ?>" href="./set_user.php?id=<?php echo $value['sub_parent_user'] ?>"><?php echo $value['sub_parent_name']; ?></a></td>
						<td align="left">
							<?php if( isset($value['sub_parent_name']) ) echo  '回复了'.$value['sub_parent_name'].'的回复：';
							else echo  '回复：';?><span id="content_<?php echo $value['id']; ?>"><?php echo $value['content']; ?></span>
							<br/><?php if( isset($value['sub_parent_name']) && isset($value['sub_parent_content']) ) echo $value['sub_parent_name'] .'的回复:'.$value['sub_parent_content']; ?>
							<br/>原主题:<?php echo isset( $value['parent_content'] ) ? $value['parent_content'] : ''; ?>
						</td>
						<td>
							<table width="80%">
								<tr align="center">
									<td><a href="#" onclick="mark( <?php echo $value['id']; ?> );reply(<?php echo $value['id']; ?>)">处理</a></td>
									<td><a href="cancel_alert.php?type=1002&id=<?php echo $value['id']; ?>" onclick="mark( <?php echo $value['id']; ?> );return confirm('确认忽略？')">忽略</a></td>
								</tr>
							</table>
						</td>
					</tr>
					<?php
						}
					?>
				<?php
					}
				?>
				
				
				<?php
					if ( $base['id'] == 1009 )
					{
				?>
					<tr class="title f1">
						<th width="8%">选择</th>
						<th width="8%">时间</th>
						<th width="10%">名字</th>
						<th>内容</th>
						<th width="8%">操作</th>
					</tr>
					<?php
						foreach( $extend as $index => $value )
						{
					?>
					<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?> id="tr_<?php echo $value['id']; ?>">
						<td><input type="checkbox" name="id[]" value="<?php echo $value['id']; ?>" t="<?php echo $base['id'];?>"/></td>
						<td><?php echo $value['time']; ?></td>
						<td><a context_menu_type="user" uid="<?php echo $value['user'] ?>" href="./set_user.php?id=<?php echo $value['user'] ?>"><?php echo $value['name']; ?></a></td>
						<td align="left"><?php echo isset( $value['content'] ) ? $value['content'] : ''; ?></td>
						<td>
							<table width="80%">
								<tr align="center">
									<td><a href="#" onclick="mark( <?php echo $value['id']; ?> );reply(<?php echo $value['id']; ?>)">处理</a></td>
									<td><a href="cancel_alert.php?type=1009&id=<?php echo $value['id']; ?>" onclick="mark( <?php echo $value['id']; ?> );return confirm('确认忽略？')">忽略</a></td>
								</tr>
							</table>
						</td>
					</tr>
					<?php
						}
					?>
				<?php
					}
				?>
				

				</table>
				<div style="text-align:center; height:15px; padding-top:4px; font-size:12px;">
					<span style="float: left;"><input type="checkbox" onclick="selected_all()" id="btn_select_all" />全选&nbsp;<input type="button" value="忽略" onclick="cancel_all()" id="btn_delete_all"/></span>
				</div>

				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
		</form>
	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>



<div id="reply_div" style="display:none;">
	<table width="450">
		<tr>
			<td align="left"><span id="reply_div_from"></span></td>
		</tr>
		<tr>
			<td align="left"> NPC：<input id="reply_npc" />不填则随机选择&nbsp;<input type="checkbox" id="male">男<input type="checkbox" id="female">女</td>
		</tr>
		<tr>
			<td align="left"> 回复：<textarea id="reply_div_to" rows="2" cols="48"></textarea></td>
		</tr>
		<tr>
			<td align="left"> 时间：<input type="text" value="<?php echo date( 'Y-m-d H:i:00', time() + 120 ); ?>" id="time_1">&nbsp;&nbsp;<a href="javascript:add_time(300,1)" title="增加5分钟">+5m</a>&nbsp;&nbsp;<a href="javascript:add_time(3600,1)" title="增加1小时">+1h</a>&nbsp;&nbsp;<a href="javascript:add_time(-300,1)" title="减少5分钟">-5m</a>&nbsp;&nbsp;<a href="javascript:add_time(-3600,1)" title="减少1小时">-1h</a>&nbsp;&nbsp;<a href="javascript:add_time(0,1)" title="自定义">+?m</a></td>
		</tr>
		<tr>
			<td align="center"><input type="button" value="提交" onclick="_reply()"> <input type="button" value="取消" onclick="$.unblockUI()"></td>
		</tr>
	</table>
</div>


</body>
</html>