<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title><?php echo isset( $_title ) ? urldecode( $_title ) : 'dice'; ?></title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}

/* 表情 */
.f_menu .f_curr{ background:#ccc;border-radius: 0.5em 0.5em 0.5em 0.5em;}

.f_menu{ background:#fff; width:90%;  border-bottom:solid 1px #333;  padding:6px 5%; padding-bottom:1px; overflow:hidden;}
.f_menu ul li{ float:left; width:30%; text-align:center;list-style:none;}
.f_menu ul{ margin:0px; padding:0px; overflow:hidden;}

.c_face{ background:#fff; clear:left;  width:100%; overflow-y:auto; height:150px;}
.c_face ul{ margin:0px; margin-top:2%; padding-left:0px;}
.c_face ul li{ float:left; height:35px; width:35px; margin:2% 4%;cursor:pointer;list-style:none;}
/* 表情 end */
-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.blockUI.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>common.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.contextmenu.r2.packed.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>dice.contextmenu.js"></script>
<script type="text/javascript">
function selected_all()
{
	$('input:checkbox[name=\'id\[\]\']').attr('checked',$('#btn_select_all').attr('checked'));
}
function delete_all()
{
	if( confirm('确定删除吗？') )
	{
		$('#form2').submit();
	}
}

var message_id = 0;
function reply( id )
{
	message_id = id;
	$( '#reply_div_from' ).text( $( '#content_' + id ).text() );
	$.blockUI( { css: { width : '450px', cursor : null }, message : $( '#reply_div' ) } );
	$( '#reply_div_to' ).focus();	
	$('.blockUI').css('margin-top','-165px');
}
function _reply()
{
	$.ajax
	(
		{
			type : "POST",
			url : "message.php",
			dataType : 'json',
			async : false,
			data : 'id=' + message_id + '&user=' + $( '#sender_' + message_id ).val() + '&content=' + encodeURIComponent( $( '#reply_div_to' ).val() ) + '&from=' + $( '#receive_' + message_id ).val() + '&time=' + $( '#time_1' ).val(),
			success : function( result )
				{
					if( result.code == -1 ) alert('提交失败');
					else alert('提交成功');

					$( '#reply_div_to' ).val( '' );
					$.unblockUI();
					//window.location.href = 'message.php';
				}
		}
	);

}

$(function(){

	$(".f_menu li").bind( "click", function(event) {
		$(".f_menu li").removeClass('f_curr');
		$(this).addClass('f_curr');
		$('#face_1,#face_2,#face_3').hide();
		$('#face_'+$(this).attr('index')).show();
	});
	
	$(".c_face img").bind( "click", function(event) {	
		var face = $(this).attr('face');	
		$('#reply_div_to').val( $('#reply_div_to').val()+face );
	});

});
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">聊天列表</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是聊天信息列表。</td>
			  </tr>
			<form id="form1" method="get">
			  <tr>
				<td align="left" class="left_txt">用户名： <input type="text" name="_name" size="10" value="<?php echo $_name; ?>"> 发送身份： <select name="_from"><option value=""<?php if ( $_from == '' ) echo ' selected'; ?>>全部</option><option value="0"<?php if ( $_from == '0' ) echo ' selected'; ?>>非NPC</option><option value="1"<?php if ( $_from == '1' ) echo ' selected'; ?>>NPC</option></select> 接收身份： <select name="_to"><option value=""<?php if ( $_to == '' ) echo ' selected'; ?>>全部</option><option value="0"<?php if ( $_to == '0' ) echo ' selected'; ?>>非NPC</option><option value="1"<?php if ( $_to == '1' ) echo ' selected'; ?>>NPC</option></select> <input type="submit" value="搜索">
				<span style="float:right;"><input type="button" value="发起聊天" onclick="window.location.href='auto_add_message.php'"></span>
				</td>
			  </tr>
			</form>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
				<form id="form2" method="get" action="del_message.php">
				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
						<tr class="title f1">
							<th width="5%">选择</th>
							<th width="10%">时间</th>
							<th width="5%">状态</th>
							<th width="15%">发送人</th>
							<th width="15%">接收人</th>
							<th>内容</th>
							<th width="10%">操作</th>
						</tr>
					<?php
						foreach( $data as $index => $value )
						{
					?>
						<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?>>
							<td><input type="checkbox" name="id[]" value="<?php echo $value['id']; ?>"/></td>
							<td><?php echo $value['time']; ?></td>
							<td><?php echo $value['read'] == 1 ? '已' : '未'; ?></td>
							<td><img src="<?php echo $value['logo']; ?>" style="width:40px;height:40px;vertical-align: inherit;float:left;padding-left:5px;">&nbsp;<?php if ( $value['send_npc'] == 1 ) echo '<font color="red">N</font> '; ?><a context_menu_type="user" uid="<?php echo $value['sender']; ?>" href="set_user.php?id=<?php echo $value['sender']; ?>"><?php echo $value['sender_name']; ?></a></td>
							<td><img src="<?php echo $value['receiver_logo']; ?>" style="width:40px;height:40px;vertical-align: inherit;float:left;padding-left:5px;">&nbsp;<?php if ( $value['receive_npc'] == 1 ) echo '<font color="red">N</font> '; ?><a context_menu_type="user" uid="<?php echo $value['receiver']; ?>" href="set_user.php?id=<?php echo $value['receiver']; ?>"><?php echo $value['receiver_name']; ?></a></td>
							<td align="left">&nbsp;&nbsp;<span id="content_<?php echo $value['id']; ?>"><?php echo $value['content2']; ?></span><input type="hidden" id="sender_<?php echo $value['id']; ?>" value="<?php echo $value['sender']; ?>"><input type="hidden" id="receive_<?php echo $value['id']; ?>" value="<?php echo $value['receiver']; ?>"></td>
							<td>
								<table width="80%">
									<tr align="center">
										<td width="50%"><a href="javascript:reply(<?php echo $value['id']; ?>)">回复</a></td>
										<td width="50%"><a href="./message.php?owner=<?php echo $value['sender']; ?>&conversation=<?php echo $value['sender']; ?>,<?php echo $value['receiver']; ?>">处理</a></td>
									</tr>
								</table>
							</td>
						</tr>
					<?php
						}
					?>
					</table>

					<div style="text-align:center; height:15px; padding-top:4px; font-size:12px;">
					<span style="float: left;"><input type="checkbox" onclick="selected_all()" id="btn_select_all" />全选&nbsp;<input type="button" value="删除" onclick="delete_all()" id="btn_delete_all"/></span>

					记录数：<?php echo $bar['total']; ?>  <a href="<?php echo $bar['prev_link']; ?>"><img src="<?php echo $_template['img']; ?>prev.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;
					<?php
						foreach( $bar['number'] as $value )
						{
							if ( $value['is_current'] )
							{
					?>
						<span class="homeStockRise"><strong><?php echo $value['name']; ?></strong></span>&nbsp;
					<?php
							}
							else
							{
					?>
						<a href="<?php echo $value['link']; ?>"><?php echo $value['name']; ?></a>&nbsp;
					<?php
							}
					?>
					<?php
						}
					?>
					<a href="<?php echo $bar['next_link']; ?>"><img src="<?php echo $_template['img']; ?>next.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;[<?php echo $bar['current']; ?>/<?php echo $bar['page']; ?>]</div>

				</td>
			  </tr>
			</table></form></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
<div id="reply_div" style="display:none;">
	<table width="450">
		<tr>
			<td align="left"> 原文：<span id="reply_div_from"></span></td>
		</tr>
		<tr>
			<td align="left"> 回复：<textarea id="reply_div_to" rows="2" cols="48"></textarea></td>
		</tr>
		<tr>
			<td align="left">
		
			<!--表情-->
			<div class="f_menu">
        	<ul>
            	<li class="f_curr" index=1><img src="../frontend/expression/emoji_express_btn.png"  ></li>
                <li index=2><img src="../frontend/expression/face_title_robot_express_btn.png"  ></li>
                <li index=3><img src="../frontend/expression/emo_im_happy_go.png"  ></li>
            </ul>
        </div>
        <div class="c_face" id="face_1" >
            <ul>
                <li><img src="../frontend/expression/goemoji_e41c.png" face="(L)" /></li>
				<li><img src="../frontend/expression/goemoji_e418.png" face="(K)" /></li>
				<li><img src="../frontend/expression/goemoji_e415.png" face="8-)" /></li>
				<li><img src="../frontend/expression/goemoji_e106.png" face="*-*" /></li>
				<li><img src="../frontend/expression/goemoji_e409.png" face=")-(" /></li>
				<li><img src="../frontend/expression/goemoji_e405.png" face="^>^" /></li>
				<li><img src="../frontend/expression/goemoji_e40b.png" face=":-O" /></li>
				<li><img src="../frontend/expression/goemoji_e40c.png" face=":S" /></li>
				<li><img src="../frontend/expression/goemoji_e40d.png" face=":A" /></li>
				<li><img src="../frontend/expression/goemoji_e105.png" face="-_o" /></li>
				<li><img src="../frontend/expression/goemoji_e107.png" face=":(" /></li>
				<li><img src="../frontend/expression/goemoji_e40f.png" face=":'[" /></li>
				<li><img src="../frontend/expression/goemoji_e108.png" face="'-_-" /></li>
				<li><img src="../frontend/expression/goemoji_e402.png" face="^_o" /></li>
				<li><img src="../frontend/expression/goemoji_e403.png" face="|-)" /></li>
				<li><img src="../frontend/expression/goemoji_e410.png" face="XoX" /></li>
				<li><img src="../frontend/expression/goemoji_e411.png" face="IoI" /></li>
				<li><img src="../frontend/expression/goemoji_e416.png" face=":@" /></li>
				<li><img src="../frontend/expression/goemoji_e419.png" face="(E)" /></li>
				<li><img src="../frontend/expression/goemoji_e531.png" face="(F)" /></li>
				<li><img src="../frontend/expression/goemoji_e111.png" face="(qq)" /></li>
				<li><img src="../frontend/expression/goemoji_e329.png" face="(-*)" /></li>
				<li><img src="../frontend/expression/goemoji_e327.png" face="(H)" /></li>
				<li><img src="../frontend/expression/goemoji_e31d.png" face="(Np)" /></li>
				<li><img src="../frontend/expression/goemoji_e31e.png" face="(M)" /></li>
				<li><img src="../frontend/expression/goemoji_e31f.png" face="(Hc)" /></li>
				<li><img src="../frontend/expression/goemoji_e034.png" face="(R)" /></li>
				<li><img src="../frontend/expression/goemoji_e035.png" face="(Dm)" /></li>
				<li><img src="../frontend/expression/goemoji_e00e.png" face="(G)" /></li>
				<li><img src="../frontend/expression/goemoji_e011.png" face="(Y)" /></li>
				<li><img src="../frontend/expression/goemoji_e01b.png" face="(C)" /></li>
				<li><img src="../frontend/expression/goemoji_e01d.png" face="(Fl)" /></li>
				<li><img src="../frontend/expression/goemoji_e10e.png" face="(Cr)" /></li>
				<li><img src="../frontend/expression/goemoji_e14c.png" face="(S)" /></li>
				<li><img src="../frontend/expression/goemoji_e03c.png" face="(kalaok)" /></li>
				<li><img src="../frontend/expression/goemoji_e33b.png" face="(Ff)" /></li>
				<li><img src="../frontend/expression/goemoji_e015.png" face="(Te)" /></li>
				<li><img src="../frontend/expression/goemoji_e016.png" face="(Bb)" /></li>
            </ul>
        </div>
    	
    	<div class="c_face" style="display:none;" id="face_2" >
            <ul>
				<li><img src="../frontend/expression/emo_im_angel.png" face="{0:-)}" /></li>
				<li><img src="../frontend/expression/emo_im_cool.png" face="{B-)}" /></li>
				<li><img src="../frontend/expression/emo_im_crying.png" face="{:'(}" /></li>
				<li><img src="../frontend/expression/emo_im_embarrassed.png" face="{:-[}" /></li>
				<li><img src="../frontend/expression/emo_im_foot_in_mouth.png" face="{:-!}" /></li>
				<li><img src="../frontend/expression/emo_im_happy.png" face="{:-)}" /></li>
				<li><img src="../frontend/expression/emo_im_kissing.png" face="{:-*}" /></li>
				<li><img src="../frontend/expression/emo_im_laughing.png" face="{:-D}" /></li>
				<li><img src="../frontend/expression/emo_im_lips_are_sealed.png" face="{:-X}" /></li>
				<li><img src="../frontend/expression/emo_im_money_mouth.png" face="{:-$}" /></li>
				<li><img src="../frontend/expression/emo_im_sad.png" face="{:-(}" /></li>
				<li><img src="../frontend/expression/emo_im_surprised.png" face="{=-0}" /></li>
				<li><img src="../frontend/expression/emo_im_tongue_sticking_out.png" face="{:-P}" /></li>
				<li><img src="../frontend/expression/emo_im_undecided.png" face="{:-\}" /></li>
				<li><img src="../frontend/expression/emo_im_winking.png" face="{;-)}" /></li>
				<li><img src="../frontend/expression/emo_im_wtf.png" face="{o_O}" /></li>
				<li><img src="../frontend/expression/emo_im_yelling.png" face="{:O}" /></li>
            </ul>
        </div>
        	
        <div class="c_face" style="display:none;" id="face_3">
            <ul>
				<li><img src="../frontend/expression/emo_im_angel_go.png" face="O:-)" /></li>
				<li><img src="../frontend/expression/emo_im_cool_go.png" face="B-)" /></li>
				<li><img src="../frontend/expression/emo_im_crying_go.png" face=":'(" /></li>
				<li><img src="../frontend/expression/emo_im_embarrassed_go.png" face=":-[" /></li>
				<li><img src="../frontend/expression/emo_im_foot_in_mouth_go.png" face=":-!" /></li>
				<li><img src="../frontend/expression/emo_im_happy_go.png" face=":-)" /></li>
				<li><img src="../frontend/expression/emo_im_kissing_go.png" face=":-*" /></li>
				<li><img src="../frontend/expression/emo_im_laughing_go.png" face=":-D" /></li>
				<li><img src="../frontend/expression/emo_im_lips_are_sealed_go.png" face=":-X" /></li>
				<li><img src="../frontend/expression/emo_im_money_mouth_go.png" face=":-$" /></li>
				<li><img src="../frontend/expression/emo_im_sad_go.png" face=":-(" /></li>
				<li><img src="../frontend/expression/emo_im_surprised_go.png" face="=-0" /></li>
				<li><img src="../frontend/expression/emo_im_tongue_sticking_out_go.png" face=":-p" /></li>
				<li><img src="../frontend/expression/emo_im_undecided_go.png" face=":-\" /></li>
				<li><img src="../frontend/expression/emo_im_winking_go.png" face=";-)" /></li>
				<li><img src="../frontend/expression/emo_im_wtf_go.png" face="o_O" /></li>
				<li><img src="../frontend/expression/emo_im_yelling_go.png" face=":O" /></li>
            </ul>
        </div>	
			<!--表情end-->
		
			</td>
		</tr>
		<tr>
			<td align="left"> 时间：<input type="text" value="<?php echo date( 'Y-m-d H:i:s', time() + 120 ); ?>" id="time_1">&nbsp;&nbsp;<a href="javascript:add_time(300,1)" title="增加5分钟">+5m</a>&nbsp;&nbsp;<a href="javascript:add_time(3600,1)" title="增加1小时">+1h</a>&nbsp;&nbsp;<a href="javascript:add_time(-300,1)" title="减少5分钟">-5m</a>&nbsp;&nbsp;<a href="javascript:add_time(-3600,1)" title="减少1小时">-1h</a>&nbsp;&nbsp;<a href="javascript:add_time(0,1)" title="自定义">+?m</a></td>
		</tr>
		<tr>
			<td align="center"><input type="button" value="提交" onclick="_reply()"> <input type="button" value="取消" onclick="$.unblockUI()"></td>
		</tr>
	</table>
</div>
</body>
</html>