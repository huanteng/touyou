<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_template['css']; ?>redmond/jquery-ui-1.7.custom.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery-ui-1.7.3.datepicker.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.contextmenu.r2.packed.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>dice.contextmenu.js"></script>
<script>
$(function(){
	$('#_time').datepicker({
		inline: false,
		dateFormat: 'yy-mm-dd'
	});
});
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">消费记录</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">
	<form method="get">
	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是用户的消费记录。</td>
			  </tr>
			  <tr>
				<td align="left" class="left_txt">用户名： <input type="text" name="_name" size="10" value="<?php echo $_name; ?>">
	时间： <input type="text" id="_time" name="_time" size="10" value="<?php echo $_time; ?>">
	类型：
	<select name="_type">
		<option value=""></option>
		<option value="0"<?php if($_type=='0')echo ' selected';?>>充值</option>
		<option value="1,2"<?php if($_type=='1,2')echo ' selected';?>>购买</option>
		<option value="5,6"<?php if($_type=='5,6')echo ' selected';?>>挑战</option>
		<option value="9,10"<?php if($_type=='9,10')echo ' selected';?>>积分</option>
		<option value="7"<?php if($_type=='7')echo ' selected';?>>任务</option>
		<option value="8"<?php if($_type=='8')echo ' selected';?>>系统</option>
		<option value="11"<?php if($_type=='11')echo ' selected';?>>手工</option>
	</select>
	类别：<select name="_history"><option value="0"<?php if ( $_history == '0' ) echo ' selected'; ?>>普通</option><option value="1"<?php if ( $_history == '1' ) echo ' selected'; ?>>归档</option></select>
	<input type="submit" value="搜索">
	<span style="float:right;"> <input type="button" value="赠送道具" onclick="window.location.href='send_props.php';"> <input type="button" value="金币余额" onclick="window.location.href='user_props_lists.php'"></span>
	</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>

				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
						<tr class="title f1">
							<th width="10%">时间</th>
							<th width="10%">用户</th>
							<th width="7%">类型</th>
							<th width="10%">道具</th>
							<th width="10%">变化</th>
							<th width="10%">余额</th>
							<th width="23%">备注</th>
						</tr>
					<?php
						foreach( $data as $index => $value )
						{
					?>
						<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?>>
							<td><?php echo date( 'm-d H:i', $value['time'] ); ?></td>
							<td><?php if ( $value['npc'] == 1 ) echo '<font color="red">N</font> '; ?><a context_menu_type="user" uid="<?php echo $value['user']; ?>" href="set_user.php?id=<?php echo $value['user']; ?>"><?php echo $value['name']; ?></a></td>
							<td><?php echo $type_dict[$value['type']]; ?></td>
							<td><?php echo $value['props']; ?></td>
							<td><?php if ( $value['change'] > 0 ) echo '+'; ?><?php echo $value['change']; ?></td>
							<td><?php echo $value['balance']; ?></td>
							<td align="left">&nbsp;&nbsp;<?php echo $value['memo']; ?></td>
						</tr>
					<?php
						}
					?>
					</table>

					<div style="text-align:center; height:15px; padding-top:4px; font-size:12px;">记录数：<?php echo $bar['total']; ?>  <a href="<?php echo $bar['prev_link']; ?>"><img src="<?php echo $_template['img']; ?>prev.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;
					<?php
						foreach( $bar['number'] as $value )
						{
							if ( $value['is_current'] )
							{
					?>
						<span class="homeStockRise"><strong><?php echo $value['name']; ?></strong></span>&nbsp;
					<?php
							}
							else
							{
					?>
						<a href="<?php echo $value['link']; ?>"><?php echo $value['name']; ?></a>&nbsp;
					<?php
							}
					?>
					<?php
						}
					?>
					<a href="<?php echo $bar['next_link']; ?>"><img src="<?php echo $_template['img']; ?>next.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;[<?php echo $bar['current']; ?>/<?php echo $bar['page']; ?>]</div>

				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
		</form>
	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>