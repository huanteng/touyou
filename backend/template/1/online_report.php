<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_template['css']; ?>redmond/jquery-ui-1.7.custom.css" rel="stylesheet" type="text/css"/>	
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery-ui-1.7.3.datepicker.js"></script>
<script type="text/javascript">
$(function(){
	$('#_start,#_end').datepicker({
		inline: false,
		dateFormat: 'yy-mm-dd'
	});
});
</script>	
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">在线报表</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">
	<form method="get">
	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是在线报表列表。(默认显示当天)</td>
			  </tr>
			  <tr>
				<td align="left" class="left_txt">开始日期：<input type="text" id="_start" name="_start" size="10" value="<?php echo $_start; ?>"> 结束日期：<input type="text" id="_end" name="_end" size="10" value="<?php echo $_end; ?>"><input type="submit" value="查询">
				</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>

				<table width="80%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
						<tr class="title f1">
							<th colspan="6">日期：<?php echo date('Y-m-d H:i:s',$start); ?> ～ <?php echo date('Y-m-d H:i:s',$end); ?></th>
						</tr>
						<tr class="title f1">
							<th width="20%">小时段</th>
							<th width="20%">非NPC在线数</th>
							<th>比例</th>
							<th width="20%">NPC在线数</th>
							<th>比例</th>
						</tr>
					<?php
						for($index=0;$index<24;$index++)
						{
					?>
						<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?>>
							<td><?php echo $index;?>:00 - <?php echo $index+1;?>:00</a></td>
							<td><?php echo $data['hour'.$index][0]; ?></td>
							<td><span style="float:left; background-color:red; width:<?php echo round($data['hour'.$index][0]/($data['total_0']==0?1:$data['total_0'])*100,2); ?>%"> <?php echo round($data['hour'.$index][0]/($data['total_0']==0?1:$data['total_0'])*100,2); ?>% </span></td>
							<td><?php echo $data['hour'.$index][1]; ?></td>
							<td><span style="float:left; background-color:red; width:<?php echo round($data['hour'.$index][1]/($data['total_1']==0?1:$data['total_1'])*100,2); ?>%"> <?php echo round($data['hour'.$index][1]/($data['total_1']==0?1:$data['total_1'])*100,2); ?>% </span></td>
						</tr>
					<?php
						}
					?>
						<tr align="center"><td>总数：</td><td><?php echo $data['total_0']; ?></td><td></td><td><?php echo $data['total_1']; ?></td><td></td></tr>
					</table>

					

				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
		</form>
	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>