<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
</style>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript">
function password()
{
	if ( $( '#old' ).val() == '' )
	{
		alert( '请输入旧密码' );
		$( '#old' ).focus();
		return false;
	}

	if ( $( '#new' ).val() == '' )
	{
		alert( '请输入新密码' );
		$( '#new' ).focus();
		return false;
	}

	$( '#submit_button' ).disabled = true;

	$.ajax
	(
		{
			type : "POST",
			url : "password.php",
			dataType : 'json',
			data : 'old=' + encodeURIComponent( $( '#old' ).val() ) + '&new=' + encodeURIComponent( $( '#new' ).val() ),
			success : function( data )
				{
					$( '#submit_button' ).disabled = false;
					alert( data.message );
					if ( data.status == 0 ) window.top.location.href = 'logout.php';
				}
		}
	);
}
</script>
<body onload="$( '#old' ).focus()">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">修改密碼</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：用户可在次修改密码，建议定期执行。</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">旧密码：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="password" id="old"></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr bgcolor="#E2E7ED">
							<td width="20%" height="30" align="right" class="left_txt2">新密码：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="password" id="new" onkeydown="if(event.keyCode==13)password();"></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3" align="center">
								<table width="30%">
									<tr align="center">
										<td><input type="button" value="提交" id="submit_button" onclick="password()" /></td>
										<td><input type="button" value="返回" onclick="window.history.go(-1)" /></td>
									</tr>
								</table>
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>