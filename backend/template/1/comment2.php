<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title><?php echo isset( $_title ) ? urldecode( $_title ) : 'dice'; ?></title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.blockUI.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>common.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.contextmenu.r2.packed.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>dice.contextmenu.js"></script>
<script>
function selected_all()
{
	$('input:checkbox[name=\'id\[\]\']').attr('checked',$('#btn_select_all').attr('checked'));
}
function delete_all()
{
	if( confirm('确定删除吗？') )
	{
		$('#form2').submit();
	}
}

var message_id = 0;
function reply( id )
{
	message_id = id;
	$( '#reply_div_from' ).text( $( '#content_' + id ).text() );
	$.blockUI( { css: { width : '450px', cursor : null }, message : $( '#reply_div' ) } );
	$( '#reply_div_to' ).focus();
}
function _reply()
{

	var sex = [] ;
	if( $("#male").attr('checked') == true ) sex.push( 0 );
	if( $("#female").attr('checked') == true ) sex.push( 1 );

	$.ajax
	(
		{
			type : "POST",
			url : "comment2.php",
			dataType : 'json',
			async : false,
			data : 'op=reply&name=' + $( '#reply_npc').val() + '&content=' + encodeURIComponent( $( '#reply_div_to' ).val() ) + '&id=' + message_id + '&time=' + $( '#time_1' ).val() + '&sex='+sex.join(','),
			success : function( result )
				{
					if( result.code == -1 ) alert('提交失败');
					else alert('提交成功');

					$( '#reply_div_to' ).val( '' );
					$.unblockUI();
				}
		}
	);

}
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">回复管理</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是回复列表。</td>
			  </tr>
			<form method="get">
			  <tr>
				<td align="left" class="left_txt">用户名: <input type="text" name="name" size="10" value="<?php echo $name; ?>"> 
		发布者: <select name="sender_npc">
			<option value=""<?php if($sender_npc!='0' || $sender_npc!='1') echo ' selected'?>>全部</option>
			<option value="1"<?php if($sender_npc=='1') echo ' selected'?>>NPC</option>
			<option value="0"<?php if($sender_npc=='0') echo ' selected'?>>非NPC</option>
		</select> 
		<input type="submit" value="搜索"></td>
			  </tr>
			</form>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
				<form id="form2" method="post">
				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
						<tr class="title f1">
							<th width="3%">选择</th>
							<th width="7%">时间</th>
							<th width="7%">类型</th>
							<th width="10%">发布者</th>
							<th width="10%">接收者</th>
							<th>内容</th>
							<th width="8%">操作</th>
						</tr>
					<?php
						foreach( $data as $index => $value )
						{
					?>
						<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?>>
							<td><input type="checkbox" name="id[]" value="<?php echo $value['id']; ?>"/></td>
							<td><?php echo $value['time']; ?></td>
							<td><?php if($value['sub_parent']!=0 && $value['parent']!=0) echo '回复';elseif($value['sub_parent']==0 && $value['parent']!=0) echo '回复说两句';else echo ''; ?></td>
							<td><img src="<?php echo $value['logo']; ?>" style="width:40px;height:40px;vertical-align: inherit;float:left;padding-left:5px;">&nbsp;<?php if ( $value['npc'] == 1 ) echo '<font color="red">N</font> '; ?><a context_menu_type="user" uid="<?php echo $value['user'] ?>" href="./set_user.php?id=<?php echo $value['user'] ?>"><?php echo $value['name'] ?></a></td>
							<td><img src="<?php echo $value['receiver_logo']; ?>" style="width:40px;height:40px;vertical-align: inherit;float:left;padding-left:5px;">&nbsp;<?php if ( $value['receiver_npc'] == 1 ) echo '<font color="red">N</font> '; ?><a href="set_user.php?id=<?php echo $value['receiver'];?>"><?php echo $value['receiver_name'];?></a></td>
							<td align="left">
							<?php if( isset($value['sub_parent_name']) ) echo  '回复了'.$value['sub_parent_name'].'的回复：';
						else echo  '回复：';?><span id="content_<?php echo $value['id']; ?>"><?php echo $value['content']; ?></span>
						<br/>原主题:<?php echo isset( $value['parent_content'] ) ? $value['parent_content'] : ''; ?>
						<br/><?php if( isset($value['sub_parent_name']) && isset($value['sub_parent_content']) ) echo $value['sub_parent_name'] .'的回复:'.$value['sub_parent_content']; ?>
							</td>
							<td>
								<table width="80%">
									<tr align="center"><td><a href="./auto_publish_comment2.php?id=<?php echo $value['parent'] ?>">查看</a></td>
									<td><a href="javascript:reply(<?php echo $value['id']; ?>)">回复</a></td>
									</tr>
								</table>
							</td>
						</tr>
					<?php
						}
					?>

					</table>

					<div style="text-align:center; height:15px; padding-top:4px; font-size:12px;">

					<span style="float: left;"><input type="checkbox" onclick="selected_all()" id="btn_select_all" />全选&nbsp;<input type="button" value="删除" onclick="delete_all()" id="btn_delete_all"/></span>

					记录数：<?php echo $bar['total']; ?>  <a href="<?php echo $bar['prev_link']; ?>"><img src="<?php echo $_template['img']; ?>prev.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;
					<?php
						foreach( $bar['number'] as $value )
						{
							if ( $value['is_current'] )
							{
					?>
						<span class="homeStockRise"><strong><?php echo $value['name']; ?></strong></span>&nbsp;
					<?php
							}
							else
							{
					?>
						<a href="<?php echo $value['link']; ?>"><?php echo $value['name']; ?></a>&nbsp;
					<?php
							}
					?>
					<?php
						}
					?>
					<a href="<?php echo $bar['next_link']; ?>"><img src="<?php echo $_template['img']; ?>next.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;[<?php echo $bar['current']; ?>/<?php echo $bar['page']; ?>]</div>

				</td>
			  </tr>

			  <tr><td>&nbsp;</td></tr>
			  <tr>
			  	  <td>
					<fieldset>
					  <legend>说明：</legend>
						<p>用户名 点击可查看用户个人资料	</p>
						<p>用户名 右键可调出命令菜单		</p>
						<p>操作区							</p>
						<p>  --查看 查看该条信息相关的主题	</p>
						<p>  --回复 对当前语句进行快速回复	</p>
					</fieldset>
				</td>
			  </tr>

			</table></form></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

<div id="reply_div" style="display:none;">
	<table width="450">
		<tr>
			<td align="left"><span id="reply_div_from"></span></td>
		</tr>
		<tr>
			<td align="left"> NPC：<input name="replay_npc" id="reply_npc" />不填则随机选择&nbsp;<input type="checkbox" id="male">男<input type="checkbox" id="female">女</td>
		</tr>
		<tr>
			<td align="left"> 回复：<textarea id="reply_div_to" rows="2" cols="48"></textarea></td>
		</tr>
		<tr>
			<td align="left"> 时间：<input type="text" value="<?php echo date( 'Y-m-d H:i:00', time() + 120 ); ?>" id="time_1">&nbsp;&nbsp;<a href="javascript:add_time(300,1)" title="增加5分钟">+5m</a>&nbsp;&nbsp;<a href="javascript:add_time(3600,1)" title="增加1小时">+1h</a>&nbsp;&nbsp;<a href="javascript:add_time(-300,1)" title="减少5分钟">-5m</a>&nbsp;&nbsp;<a href="javascript:add_time(-3600,1)" title="减少1小时">-1h</a>&nbsp;&nbsp;<a href="javascript:add_time(0,1)" title="自定义">+?m</a></td>
		</tr>
		<tr>
			<td align="center"><input type="button" value="提交" onclick="_reply()"> <input type="button" value="取消" onclick="$.unblockUI()"></td>
		</tr>
	</table>
</div>

</body>
</html>