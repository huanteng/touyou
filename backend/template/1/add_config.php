<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
</style>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>common.js"></script>
<script type="text/javascript">
function create()
{
	/* 2do,检查数据有效性
	if ( $( '#name' ).val() == '' )
	{
		alert( '请输入名称' );
		$( '#name' ).focus();
		return false;
	}

	if ( $( '#data' ).val() == '' )
	{
		alert( '请输入内容' );
		$( '#data' ).focus();
		return false;
	}

	if ( $( '#code' ).val() == '' )
	{
		alert( '请输入编码' );
		$( '#code' ).focus();
		return false;
	}
	*/
	
	var user = $("#user").val().split('_')[1];
	var section = $("#section").val();
	var key = $("#key").val();
	var value = encodeURIComponent( $("#value").val() );

	$( '#create_button' ).attr( 'disabled', true );

	var data = {user: user, section: section, key: key, value: value};
	var func = function(data){
		alert( data.code+"\n"+data.message );
		$( '#create_button' ).attr( 'disabled', false );
	};
	ajax_post( 'add_config.php', data, func );
}

function user(s)
{
	$("#user").val(s);
}
function quick(type)
{
	var section = 'config';
	var key = 'url';
	var value = '';
	value = ( type == 'product' ) ? 'http://touyou.mobi/interface/;http://saiyou.mobi/interface/;http://shaiyou.mobi/interface/' : 'http://192.168.1.100:8080/backend/interface/';
	
	$("#section").val(section);
	$("#key").val(key);
	$("#value").val(value);
}

</script>
<body onload="$( '#name' ).focus()">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">更新配置值</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：在此更新特定用户的配置值。</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr bgcolor="#E2E7ED">
							<td width="20%" height="30" align="right" class="left_txt2">接收人：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="user" value="dda_237" size="60" /></td>
							<td width="45%" height="30" class="left_txt">快速设置：<?php foreach( array('全部' => '全部_0', 'dda' => 'dda_237', '小左' => 'decade_226') as $key => $value ) {?><a href="javascript:user('<?php echo $value ?>')"><?php echo $key ?></a> <?php } ?></td>
						</tr>
						<tr>
                          <td height="30" align="right" class="left_txt2">section：</td>
						  <td>&nbsp;</td>
						  <td height="30"><input name="section" type="text" id="section" value="" size="60" /></td>
						  <td height="30" class="left_txt">快速设置：<a href="javascript:quick('product')">产品机接口</a> <a href="javascript:quick('test')">测试机接口</a></td>
					  </tr>
						<tr bgcolor="#E2E7ED">
                          <td height="30" align="right" class="left_txt2">key：</td>
						  <td>&nbsp;</td>
						  <td height="30"><input type="text" id="key" size="60" /></td>
						  <td height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">value：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="value" size="60"></td>
							<td width="45%" height="30" class="left_txt">如果值为空，表示将删除此键值</td>
						</tr>
						<tr>
							<td colspan="3" align="center">
								<input type="button" value="确认" id="create_button" onclick="create()" /> 
								<input type="button" value="返回" onclick="window.history.go(-1)" />							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>