<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript">
function submit_queue()
{
	var sex = '';
	var temp = [] ;
	if( $("#male").attr('checked') == true ) temp.push( 0 );
	if( $("#female").attr('checked') == true ) temp.push( 1 );
	sex = temp.join(',');
	
	$.ajax
	(
		{
			type : "POST",
			url : "auto_vote_album.php",
			dataType : 'json',
			async : false,
			data : 'target=<?php echo $info['id']; ?>&start_time=' + $( '#start_time' ).val() + '&interval_1=' + $( '#interval_1' ).val() + '&interval_2=' + $( '#interval_2' ).val() + '&score_1=' + $( '#score_1' ).val() + '&score_2=' + $( '#score_2' ).val() + '&times=' + $( '#times' ).val() + '&sex=' + sex,
			success : function( result )
				{
					alert( '设置成功' );
					//window.location.href = 'album.php';
				}
		}
	);
}
</script>
<body onload="$('#times').focus()">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">评分管理</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">
	<form method="get">
	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：这里可以管理用户上传的照片，包括删除及评分管理、设置</td>
			  </tr>

			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>

			  <tr>
				<td class="left_txt">
					<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
						<tr>
							<td><a href="<?php echo $album_object->get_url( $info['path'] ); ?>" target="_blank" title="点击查看全图"><img src="<?php echo $album_object->get_url( $info['path'] ); ?>" style="height:300px;border:0px;"></a></td>
							<td>
								<table>
									<tr><td>大小：<?php echo $info['size']; ?> KB</td></tr>
									<tr><td>上传人：<?php echo $info['name']; ?></td></tr>
									<tr><td>时间：<?php echo date( 'Y-m-d H:i:s', $info['time'] ); ?></td></tr>
								</table>
								<table>
									<tr><td colspan="8">当前任务队列</td></tr>
									<tr>
										<td>序号</td>
										<td>状态</td>
										<td>开始时间</td>
										<td>操作</td>
									</tr>
									<?php
										foreach( $task_queue as $index => $queue )
										{
									?>
									<tr>
										<td><?php echo ( $index + 1 ); ?></td>
										<td><?php echo $queue['status'] == 1 ? '未开始' : ( $queue['status'] == 2 ? '运行中' : '已完成' ); ?></td>
										<td><?php echo date( 'm-d H:i', $queue['time'] ); ?></td>
										<td><a href="#">查看</a></td>
									</tr>
									<?php
										}
									?>
								</table>
							</td>
							<td>
								<table>
									<tr><td>评分设置：</td></tr>
									<tr><td>评分次数：<input type="text" id="times" value="5"></td></tr>
									<tr><td>开始时间：<input type="text" id="start_time" value="<?php echo date( 'Y-m-d H:i:s', time() + 600 ); ?>"></td></tr>
									<tr><td>间隔时间：<input type="text" id="interval_1" value="3" size="4">～<input type="text" id="interval_2" value="9" size="4"> 分钟</td></tr>
									<tr><td>评分范围：<input type="text" id="score_1" value="5" size="4">～<input type="text" id="score_2" value="10" size="4"> 分</td></tr>
									<tr><td>NPC性别：<input type="checkbox" id="male" />男<input type="checkbox" id="female" />女</td></tr>	
									<tr><td align="center"><input type="button" value="返回" onclick="window.history.go(-1)"> <input type="button" value="确定" onclick="submit_queue()"></td></tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			  </tr>

			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>

			   <tr>
				<td class="left_txt">评分总人次：<?php echo $info['times']; ?> 相片平均分：<?php echo $info['times'] > 0 ? round( $info['score'] / $info['times'], 2 ) : 0; ?></td>
			  </tr>

			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>

			  <tr>
				<td>

				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
					<?php
						foreach( $vote_history as $index => $value )
						{
					?>
						<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?>>
							<td><img src="<?php echo $value['logo']; ?>" width="60px;" height="60px;"></td>
							<td><?php echo $value['name']; ?> 评了 <span style="color:red;"><?php echo $value['score']; ?></span> 分</td>
							<td><?php echo date( 'Y-m-d H:i:s', $value['time'] ); ?></td>
						</tr>
					<?php
						}
					?>
					</table>

					<div style="text-align:center; height:15px; padding-top:4px; font-size:12px;">记录数：<?php echo $bar['total']; ?>  <a href="<?php echo $bar['prev_link']; ?>"><img src="<?php echo $_template['img']; ?>prev.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;
					<?php
						foreach( $bar['number'] as $value )
						{
							if ( $value['is_current'] )
							{
					?>
						<span class="homeStockRise"><strong><?php echo $value['name']; ?></strong></span>&nbsp;
					<?php
							}
							else
							{
					?>
						<a href="<?php echo $value['link']; ?>"><?php echo $value['name']; ?></a>&nbsp;
					<?php
							}
					?>
					<?php
						}
					?>
					<a href="<?php echo $bar['next_link']; ?>"><img src="<?php echo $_template['img']; ?>next.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;[<?php echo $bar['current']; ?>/<?php echo $bar['page']; ?>]</div>

				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
		</form>
	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>