<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_template['css']; ?>redmond/jquery-ui-1.7.custom.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery-ui-1.7.3.datepicker.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.contextmenu.r2.packed.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>dice.contextmenu.js"></script>
<script>
function jump_page()
{
	var curr_href = window.parent.right_frame.location.href;
	window.parent.right_frame.location.href=curr_href+'&page='+document.getElementById('jump_page_input').value;
}

$(function(){
	$('#_start').datepicker({
		inline: false,
		dateFormat: 'yy-mm-dd'
	});
});
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">单盘比赛</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">
	<form method="get">
	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是单盘比赛的列表。</td>
			  </tr>
			  <tr>
				<td align="left" class="left_txt">类别：<select name="history"><option value="1"<?php if ( $history == 1 ) echo ' selected'; ?>>历史</option><option value="0"<?php if ( $history != 1 ) echo ' selected'; ?>>当前</option></select>
				类型：
				<select name="type"><option value="">全部</option><option value="1"<?php if ( $type == 1 ) echo ' selected'; ?>>骰魔大赛</option><option value="2"<?php if ( $type == 2 ) echo ' selected'; ?>>挑战</option></select>
				<!--对战类型：
				<select name="vs_type">
					<option>-</option>
					<option value="1">非NPC vs 非NPC</option>
					<option value="2">非NPC vs NPC</option>
					<option value="3">NPC vs NPC</option>
				</select>-->
				用户名：
				<input type="text" name="name" value="<?php echo $name;?>"/>
				日期：
				<input type="text" name="_start" id="_start" value="<?php echo $_start;?>"/>
				<input type="submit" value="搜索"></td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>

				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt">
						<tr class="title f1">
							<th width="8%">时间</th>
							<th width="6%">类型</th>
							<th width="4%">轮次</th>
							<th width="12%">用户1</th>
							<th width="16%">用户2</th>
							<th width="11%">骰1</th>
							<th width="12%">骰2</th>
							<th width="14%">赛金</th>
							<th width="17%">操作</th>
						</tr>
					<?php
						$nows = time();
						foreach( $data as $index => $value )
						{
					?>
						<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?>>
							<td><?php echo date( 'm-d H:i:s', $value['start'] ); ?></td>
							<td><?php echo $value['type']; ?></td>
							<td><?php echo $value['round']; ?></td>
							<td><?php if ( $value['user_1_npc'] == '1' ) echo '<font color="red">N</font> '; ?><a context_menu_type="user" uid="<?php echo $value['user_1']; ?>" href="set_user.php?id=<?php echo $value['user_1']; ?>"><?php echo $value['user_1_name']; ?></a><?php if ( $value['user_1'] == $value['first'] ) echo ' (先)'; ?><?php if ( isset( $value['winner'] ) && $value['user_1'] == $value['winner'] ) echo ' (赢)'; ?>&nbsp;&nbsp;(<?php echo $value['user_1_props']; ?>)</td>
							<td><?php if ( $value['user_2_npc'] == '1' ) echo '<font color="red">N</font> '; ?><a context_menu_type="user" uid="<?php echo $value['user_2']; ?>" href="set_user.php?id=<?php echo $value['user_2']; ?>"><?php echo $value['user_2_name']; ?></a><?php if ( $value['user_2'] == $value['first'] ) echo ' (先)'; ?><?php if ( isset( $value['winner'] ) && $value['user_2'] == $value['winner'] ) echo ' (赢)'; ?>&nbsp;&nbsp;(<?php echo $value['user_2_props']; ?>)</td>
							<td><?php echo $value['user_1_1'].$value['user_1_2'].$value['user_1_3'].$value['user_1_4'].$value['user_1_5']; ?><?php if ( $value['must_win'] == '1' ) echo '<br>('. $value['old'] .')'; ?></td>
							<td><?php echo $value['user_2_1'].$value['user_2_2'].$value['user_2_3'].$value['user_2_4'].$value['user_2_5']; ?><?php if ( $value['must_win'] == '2' ) echo '<br>('. $value['old'] .')'; ?></td>
							<td><?php echo $value['bet']; ?></td>
							<td>
								<table width="80%">
									<tr align="center">
										<td><a target="_blank" href="get_pair.php?id=<?php echo $value['id']; ?>">查看</a></td>
										<td><a href="towin.php?id=<?php echo $value['id']; ?>">设npc赢</a></td>
										<td><a href="clear_match.php?id=<?php echo $value['id']; ?>" onclick="return confirm('确定删除？')">删除</a></td>
									</tr>
								</table>
							</td>
						</tr>
					<?php
						}
					?>
					</table>

					<div style="text-align:center; height:15px; padding-top:4px; font-size:12px;">记录数：<?php echo $bar['total']; ?>  <a href="<?php echo $bar['prev_link']; ?>"><img src="<?php echo $_template['img']; ?>prev.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;
					<?php
						foreach( $bar['number'] as $value )
						{
							if ( $value['is_current'] )
							{
					?>
						<span class="homeStockRise"><strong><?php echo $value['name']; ?></strong></span>&nbsp;
					<?php
							}
							else
							{
					?>
						<a href="<?php echo $value['link']; ?>"><?php echo $value['name']; ?></a>&nbsp;
					<?php
							}
					?>
					<?php
						}
					?>
					<a href="<?php echo $bar['next_link']; ?>"><img src="<?php echo $_template['img']; ?>next.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;[<?php echo $bar['current']; ?>/<?php echo $bar['page']; ?>]

					<input type="text" value="" id="jump_page_input" size="2"/>&nbsp;<a href="#" onclick="jump_page()">跳转</a>
					</div>

				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
		</form>
	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>