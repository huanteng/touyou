<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title><?php echo isset( $_title ) ? urldecode( $_title ) : 'dice'; ?></title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_template['css']; ?>jquery.autocomplete.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}

/* 表情 */
.f_menu .f_curr{ background:#ccc;border-radius: 0.5em 0.5em 0.5em 0.5em;}

.f_menu{ background:#fff; width:90%;  border-bottom:solid 1px #333;  padding:6px 5%; padding-bottom:1px; overflow:hidden;}
.f_menu ul li{ float:left; width:30%; text-align:center;list-style:none;}
.f_menu ul li img{ width:20px; height:20px; }
.f_menu ul{ margin:0px; padding:0px; overflow:hidden;}

.c_face{ background:#fff; clear:left;  width:100%; overflow-y:auto; height:150px;}
.c_face ul{ margin:0px; margin-top:2%; padding-left:0px;}
.c_face ul li{ float:left; height:20px; width:20px; margin:2% 4%;cursor:pointer;list-style:none;}
.c_face ul li img{ width:20px; height:20px; }
/* 表情 end */
#content {
	height:300px;
}

-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<!--script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.blockUI.js"></script-->
<script type="text/javascript" src="<?php echo $_template['js']; ?>common.js"></script>
<!--script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.contextmenu.r2.packed.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>dice.contextmenu.js"></script-->
<script type='text/javascript' src='<?php echo $_template['js']; ?>jquery.autocomplete.pack.js'></script>
<script type='text/javascript' src='<?php echo $_template['js']; ?>jscroll.js'></script>
<script type="text/javascript">
"option explicit"
var max_id = 0;
var data = [];
var config = [];
var id = 1;
var user = 0;
var user_name = "";
var npc = 0;
var npc_name = ""
// return true if any empty
function check_empty()
{
	for (var i = 0; i < arguments.length; i=i+2) 
	{ 
		if(arguments[i]=="") 
		{ 
			alert(arguments[i+1]);
			return true; 
		}
	}
	return false;
}
function post(data, func)
{
	ajax_post( 'message2_ajax.php', data, func );
}

function delay( second )
{
	if ( second == 0 )
	{
		var minute = window.prompt( '请输入要延后的分钟，负数表示提前' );

		if ( minute != null && CheckNumber( minute ) )
		{
			delay( minute * 60 );
		}
	}
	else
	{
		$("#delay").val( eval($("#delay").val()) + second );
	}
}

function challenge(data)
{
	var content = "";
	var type = 0;
	var html = "";
	
	content = data.content.substring(3, data.content.length);
	type = eval(content.substring(0,1));
	content = content.substring(2, data.content.length);
	
	/*			i. 系统_1_dda向你挑战1000金币。接受 拒绝
			ii. 系统_0_挑战开始
			iii. 系统_2_挑战结束，金币1000，dda赢 继续 退出
*/
	switch(type)
	{
		case 0:
			html = "{0}和{1}{2} <a href='javascript:challenge_ok({3})'>确定</a>".format(data.sender_name, data.receiver_name, content, data.id);
			html += "&nbsp;&nbsp;<a href='javascript:challenge_must_win({0}, {1}, {2})'>设置必赢</a>".format(data.id, data.receiver, data.sender);
			break;
		case 1:
			var gold = content.substring( 6 );

			html = "{0}向{1}{2} <a href='javascript:challenge_accept({3}, {4}, {5},{6},0)'>接受</a>&nbsp;&nbsp;<a href='javascript:challenge_reject({3}, {4}, {5})'>拒绝</a>&nbsp;&nbsp;<a href='javascript:challenge_accept({3}, {4}, {5},{6},1)'>接受并NPC必赢</a>".format(data.sender_name,
			data.receiver_name, content, data.id, data.receiver, data.sender, gold);
			break;
		case 2:
			html = "{0}向{1}{2} <a href='javascript:challenge_next({3}, {4}, {5})'>继续</a>&nbsp;&nbsp;<!--a href='javascript:challenge_send(\"no_continue\", {3}, {4}, {5})'>不继续</a-->".format(data.sender_name, data.receiver_name, content, data.id, data.receiver, data.sender);
			html += "&nbsp;&nbsp;<a href='javascript:challenge_unblock({0}, {1}, {2})'>解除接管</a>".format(data.id, data.receiver, data.sender);
			break;
		default:;
	}
	if(html!="")
	{
		html = '<div id="challenge{0}">{1}</div>'.format(data.id, html);
		$("#challenge").append( html );
	}
}

function challenge_ok(id)
{
	var d = {type:'challenge_ok', id: id};
	var func = function( result )
	{
	}

	post( d, func );
	
	$("#challenge"+id).remove();
}

function challenge_accept(id, npc, sender, gold, must_win)
{
	var d = {type:'challenge_accept', id: id, npc: npc, sender: sender, gold:gold, must_win: must_win};
	var func = function( result )
	{
		alert("代码："+result.code+"\n信息："+result.memo);
	}

	post( d, func );

	$("#challenge"+id).remove();
}

function challenge_reject(id, npc, sender)
{
	var d = {type:'challenge_reject', id: id, npc: npc, sender: sender};
	var func = function( result )
	{
		alert("代码："+result.code+"\n信息："+result.memo);
	}

	post( d, func );

	$("#challenge"+id).remove();
}

function challenge_must_win(id, npc, sender)
{
	var d = {type:'challenge_must_win', id: id, npc: npc, user: sender};
	var func = function( result )
	{
		alert("代码："+result.code+"\n信息："+result.memo);
	}

	post( d, func );

	$("#challenge"+id).remove();
}

function challenge_unblock(id, npc, sender)
{
	var d = {type:'challenge_unblock', id: id, npc: npc, user: sender};
	var func = function( result )
	{
		alert("代码："+result.code+"\n信息："+result.memo);
	}

	post( d, func );

	$("#challenge"+id).remove();
}

function challenge_next(id, npc, sender)
{
	var d = {type:'challenge_next', id: id, npc: npc, sender: sender};
	var func = function( result )
	{
		alert("代码："+result.code+"\n信息："+result.memo);
	}

	post( d, func );

	$("#challenge"+id).remove();
}

function get()
{
	url = "message2_ajax.php?type=0&id=" + max_id;
	$.getJSON(url, function(json){
		for( var index in json )
		{
			var row = json[index];
			var key = "";
			var text = "";
			var count = 0;
			var selected = "";
			// 是否要移动位置（如果是npc所发出，则不必
			var is_move = false;
			
			key = ( row.sender_npc == '1' ) ? (row.receiver_name + '-->' + row.sender_name) : (row.sender_name + '-->' + row.receiver_name);
			
			is_move = ( row.receiver_npc == '1' );

			if( !row.content )
			{
				row.content = '新版内容乱码';
			}

			// 挑战拦截
			if( row.content.substring(0, 3) == '系统_' )
			{
				challenge(row);
			}
			
			if( data[key] == undefined )
			{
				if( row.sender_npc == '1' ) continue;
				data[key] = [];
				config[key] = [];
				count = 0;
			}
			else
			{
				if( $("#users option[@selected]").val() == key ) selected = ' selected="selected"';
					
				if( is_move )
				{
					var chat = $("#users option[@value='" + key + "']");
					text = chat.text();
					var arr = text.replace(")","").split("(");
					if( arr.length == 2 ) count = eval(arr[1]);
					chat.remove();
				}
			}
			if( is_move )
			{
				if( row.read == '0' && row.receiver_npc == '1' )
				{
					count++;
				}
				if( count != 0 )
					count = "("+count+")";
				else
					count = "";

				var vip = '';
				if( row.vip != 0 ) vip = '【vip' + row.vip + '】';
				
				option = '<option value="{0}"{3}>{4}{1}{2}</option>'.format(key, key, count, selected, vip);
				$("#users").prepend(option);
			}
			
			data[key][data[key].length] = row;
			
	    	if( max_id < eval(row.id) ) max_id = eval(row.id);

        	if( selected != '' ) $("#users").click();
	    }
	    
		setTimeout(get, 2000);
    });
}

function reply()
{
	var chat = $("#users option[@selected]").val();
	var content = $("#reply_content").val();
	var time = $("#delay").val();
	
	if( check_empty( chat, "请从左边选择对话"
		, content, "请输入内容"
		, time, "请输入延时时间" 
		) ) return;
	
	var d = {type:'reply', id: id, user: user, content: encodeURIComponent( content ), from: npc, time: time};
	var func = function( result )
		{
			if( result.code == -1 ) alert('提交失败');
			else
			{
				if( npc != 850 )
				{
					var old_content = $("#content").html();
					old_content = $(old_content.replace(/<br>/g, "\n")).text().replace(/\n/g, "<br>");
					$("#content").html( "{3}<br>{0} {1}【队列】<br>{2}<br>".format(npc_name, time, content, old_content ) ).jscroll();
					var key = user_name + '-->' + npc_name;
					data[ key ][ data[key].length] = { 'sender': npc, 'receiver': user, 'sender_name': npc_name, 'receiver_name': user_name, 'time': time+'【队列】', 'content': content};
				}
					
				$("#reply_content").val('');
				
				// 处理下一条
				if( $("#next").attr("checked") )
				{
					var options = $("#users")[0].options;
			        var count = options.length;
			        var i;
			        for(i=1;i<count;++i)
			        {
			        	if( options[i].value != options[i].text )
			        	{
			        		options[i].selected = true;
			        		$("#users").click();
			        		break;
			        	}
			        }
				}
			}
		}
	post( d, func );
}

get();

$(function(){
	$(".f_menu li").bind( "click", function(event) {
		$(".f_menu li").removeClass('f_curr');
		$(this).addClass('f_curr');
		$('#face_1,#face_2,#face_3').hide();
		$('#face_'+$(this).attr('index')).show();
	});
	
	$(".c_face img").bind( "click", function(event) {	
		var face = $(this).attr('face');	
		$('#reply_content').val( $('#reply_content').val()+face );
	});

	$("#users").click(function(){
		var chat = $("#users option[@selected]").val();
		$("#users option[@selected]").text(chat);
		var content = "";
		var d = data[chat];
		
		if ( d[0].sender_npc == '1' )
		{
			user = d[0].receiver;
			user_name = d[0].receiver_name;
			npc = d[0].sender;
			npc_name = d[0].sender_name;
		}
		else
		{
			user = d[0].sender;
			user_name = d[0].sender_name;
			npc = d[0].receiver;
			npc_name = d[0].receiver_name;
		}
	
		var count = d.length;
		for(var i=0;i<count;++i)
		{
			content += "<br>{0} {1}<br>{2}<br>".format(d[i].sender_name, d[i].time, d[i].content);
		}
		
		var delay = 0;
		if( npc != 850 )
		{
			var d = {type:'is_online', id: npc};
			var func = function( result )
			{
				if( result.code == '1' )
				{
					delay = Math.floor(Math.random() * 60);
				}
				else
				{
					delay = 28800 + Math.floor(Math.random() * 60);
					content +="<br>注意：本npc离线<br>";
				}
			}
			post( d, func );
		}
		
		$("#content").html(content).jscroll();
		$("#delay").val( delay );

	}).dblclick(function(){
		$("#ignore").click();
	});
	
	$("#ignore").click(function(){
		var d = {type:'ignore', receiver: npc, sender: user};
		var func = function( result ){};
		post( d, func );
		
        var chat = $("#users option[@selected]").val();
        
        var options = $("#users")[0].options;
        var count = options.length;
        var i;
        for(i=1;i<count;++i)
        {
        	if( options[i].selected )
        	{
        		if( i == count - 1 )  i--;
        		break;
        	}
        }
        
		$("#users option[@selected]").remove();
		delete data[chat]
		delete config[chat];
        options[i].selected = true;
        $("#users").click();
	});

	$("#friend").click(function(){
		if( user == 0 || npc == 0 )
		{
			alert("请先选择对话对象");
			return;
		}
		
		var data = {type:'2', user:user, npc:npc};
		var func = function(data){
			alert(data.message);
		};
		post( data, func );
	});
	
	$("#pause").click(function(){
		if( user > 0 )
		{
		var data = {type:'forbid_user',uid:user};
		var func = function(data){
			alert(data.message);
		};
		post( data, func );
		} 
		else
		{
			alert('请先选择对话对象，才能禁止');
		}
			
	});
	
	$("#info1").click(function(){
		window.open( "set_user.php?id=" + user );
	});
	$("#info2").click(function(){
		window.open( "set_user.php?id=" + npc );
	});
	$("#history").click(function(){
	//	window.open( "message.php?owner=" + user + "&conversation=" + user + "," + npc );
		window.open( "../_back/message_history.php?sender=" + user + "&receiver=" + npc );
	});
	
	$("#quick_reply").click(function(){
		var text = $("#quick_reply option[@selected]").attr('value');
		$("#reply_content").val($("#reply_content").val()+text);
	});
	
	$("#npc").autocomplete("message2_ajax.php?type=1001&_npc=1", {
		width: 260,
		selectFirst: false
	});

	$("#name").autocomplete("message2_ajax.php?type=1001&_npc=0", {
		width: 260,
		selectFirst: false,
		formatResult: function(data, value) {
			return value.split(".")[0];
		}
	});
	
	$("#add").click(function(){
		var key = "";
		var npc = $("#npc").val().split("_");
		var name = $("#name").val().split("_");
		key = name[0] + '-->' + npc[0];
		
		if(npc[1] == undefined)
		{
			alert("请从下拉表选择NPC");
			return;
		}
		if(name[1] == undefined)
		{
			alert("请从下拉表选择接收者");
			return;
		}
			
		if( data[key] == undefined )
		{
			var option = "";
			option += '<option value="' + key + '">' + key + '</option>';
			$("#users").append(option);
			data[key] = [{id: "0", receiver: name[1], receiver_name:name[0],receiver_npc:"0",sender:npc[1], sender_name:npc[0],sender_npc:"1",content:"", time:"初始化"}];
			config[key] = [];
		}
		
		$("#users option[@value='"+key+"']").attr("selected", "selected");
        $("#users").click();
	});
	
	$("#reply_content").keyup(function(event){
		var val = $("input[name='send'][@checked]").val(); 
		if(val == "return")
		{
			if(event.keyCode == 13)
			{
				var s = $("#reply_content").val();
				s = s.replace(/\n/g, "");
				$("#reply_content").val( s );
            	reply();
            }
		}
		else
		{
			if(window.event.ctrlKey&&window.event.keyCode==13)
            reply();
		}
	});

	$("#challenge_block_remove").click(function(){
		challenge_unblock(0, npc, user)
	});
	
	$("#request").click(function(){
		var gold = prompt("请输入挑战金币：", "1000");
		if(gold == "") return;

		var data = {type:'challenge_request', npc:npc, uid:user, gold: gold};
		var func = function(data){
			alert(data.memo);
		};
		post( data, func );
	});
	
});

function challenge_block( remark )
{
	if( user == 0 || npc == 0 )
	{
		alert("请先选择对话对象");
		return;
	}

	var data = {type:'challenge_block', user:user, npc:npc, remark:remark};
	var func = function(data){
		alert(data.memo);
	};
	post( data, func );
}

function setTime()
{
	var today=new Date();
	var h=today.getHours();
	var m=today.getMinutes();
	var s=today.getSeconds();
	$('#time').html(h+":"+m+":"+s);

	setTimeout( setTime, 1000 );
}
setTimeout( setTime, 1000 );
</script>
<body>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">聊天列表</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr valign="top">
			<td><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td colspan="3" class="left_txt">说明：以下是“真人--NPC”对话，你也可以：查看“<a href="message.php?_name=&amp;_from=0&amp;_to=0" target="_blank">真人--真人对话</a>”、屏蔽词设置。</td>
			  </tr>

			  <tr>
				<td colspan="3" align="left" class="left_txt">NPC： 
				  <input name="npc" type="text" id="npc" size="10"> (快速：<a href="#" onclick="$('#npc').val('骰妖子_850');return false;">骰妖子</a>)
				  发送给：
				  <input name="name" type="text" id="name" size="10" />
			    <input type="submit" id="add" value="添加"></td>
			  </tr>
			  <tr>
				<td colspan="3" align="left" class="left_txt">挑战接管：<div id="challenge"></div></td>
			  </tr>
			<tr>
				<td height="20" colspan="3"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr valign="top">
				<td width="28%" rowspan="4"><select name="users" size="30" id="users">
				  <option>----------------------------------------------</option>
				  </select>
				  <br>提示：双击快速关闭<br>
					当前时间：<span id="time"></span>
				</td>
			    <td colspan="2" align="left"><input name="ignore" type="button" id="ignore" value="关闭" />
			    <input type="button" id="history" value="历史" />
                  <input type="button" id="info1" value="资料(真人)" />
                  <input type="button" id="info2" value="资料(NPC)" />
                  <input type="button" id="friend" value="关注" />
                  <!--input type="button" id="ignore_same" value="关闭同内容" title="如果TA对很多NPC说了相同的话，则在处理了一部分后，点此快速关闭此人相同内容的对话" /-->
                  <input name="pause" type="button" id="pause" value="禁止帐号" />
                  <input type="button" id="request" value="发起挑战" />
                  <!--挑战处理：<input type="button" onclick="challenge_block('接管')" value="接管" />
				  <input type="button" onclick="challenge_block('接受')" value="接受" />
				  <input type="button" onclick="challenge_block('拒绝')" value="拒绝" />
                  <input type="button" id="challenge_block_remove" value="解除" /--></td>
			  </tr>
			  <tr>
			    <td height="91" colspan="2">
		          <!--textarea name="content" cols="80" rows="15" id="content"></textarea-->
  <div id="content">
  </div>
		          </td>
		      </tr>
			  <tr>
			    <td width="47%">
		          <textarea id="reply_content" cols="80" rows="3"></textarea>
		          <br>
				  延迟（秒）：<input type="text" value="5" id="delay">
				  &nbsp;&nbsp;<a href="javascript:delay(5)" title="增加5秒钟">+5s</a>&nbsp;&nbsp;<a href="javascript:delay(300)" title="增加5分钟">+5m</a>&nbsp;&nbsp;<a href="javascript:delay(-5)" title="减少5秒钟">-5s</a>&nbsp;&nbsp;<a href="javascript:delay(-300)" title="减少5分钟">-5m</a>&nbsp;&nbsp;<a href="javascript:delay(0)" title="自定义">+?m</a>
				  <br>
				  <!--表情-->
			<div class="f_menu">
        	<ul>
            	<li class="f_curr" index=1><img src="../frontend/expression/emoji_express_btn.png"  ></li>
                <li index=2><img src="../frontend/expression/face_title_robot_express_btn.png"  ></li>
                <li index=3><img src="../frontend/expression/emo_im_happy_go.png"  ></li>
            </ul>
            	  <div class="c_face" id="face_1" >
            <ul>
                <li><img src="../frontend/expression/goemoji_e41c.png" face="(L)" /></li>
				<li><img src="../frontend/expression/goemoji_e418.png" face="(K)" /></li>
				<li><img src="../frontend/expression/goemoji_e415.png" face="8-)" /></li>
				<li><img src="../frontend/expression/goemoji_e106.png" face="*-*" /></li>
				<li><img src="../frontend/expression/goemoji_e409.png" face=")-(" /></li>
				<li><img src="../frontend/expression/goemoji_e405.png" face="^>^" /></li>
				<li><img src="../frontend/expression/goemoji_e40b.png" face=":-O" /></li>
				<li><img src="../frontend/expression/goemoji_e40c.png" face=":S" /></li>
				<li><img src="../frontend/expression/goemoji_e40d.png" face=":A" /></li>
				<li><img src="../frontend/expression/goemoji_e105.png" face="-_o" /></li>
				<li><img src="../frontend/expression/goemoji_e107.png" face=":(" /></li>
				<li><img src="../frontend/expression/goemoji_e40f.png" face=":'[" /></li>
				<li><img src="../frontend/expression/goemoji_e108.png" face="'-_-" /></li>
				<li><img src="../frontend/expression/goemoji_e402.png" face="^_o" /></li>
				<li><img src="../frontend/expression/goemoji_e403.png" face="|-)" /></li>
				<li><img src="../frontend/expression/goemoji_e410.png" face="XoX" /></li>
				<li><img src="../frontend/expression/goemoji_e411.png" face="IoI" /></li>
				<li><img src="../frontend/expression/goemoji_e416.png" face=":@" /></li>
				<li><img src="../frontend/expression/goemoji_e419.png" face="(E)" /></li>
				<li><img src="../frontend/expression/goemoji_e531.png" face="(F)" /></li>
				<li><img src="../frontend/expression/goemoji_e111.png" face="(qq)" /></li>
				<li><img src="../frontend/expression/goemoji_e329.png" face="(-*)" /></li>
				<li><img src="../frontend/expression/goemoji_e327.png" face="(H)" /></li>
				<li><img src="../frontend/expression/goemoji_e31d.png" face="(Np)" /></li>
				<li><img src="../frontend/expression/goemoji_e31e.png" face="(M)" /></li>
				<li><img src="../frontend/expression/goemoji_e31f.png" face="(Hc)" /></li>
				<li><img src="../frontend/expression/goemoji_e034.png" face="(R)" /></li>
				<li><img src="../frontend/expression/goemoji_e035.png" face="(Dm)" /></li>
				<li><img src="../frontend/expression/goemoji_e00e.png" face="(G)" /></li>
				<li><img src="../frontend/expression/goemoji_e011.png" face="(Y)" /></li>
				<li><img src="../frontend/expression/goemoji_e01b.png" face="(C)" /></li>
				<li><img src="../frontend/expression/goemoji_e01d.png" face="(Fl)" /></li>
				<li><img src="../frontend/expression/goemoji_e10e.png" face="(Cr)" /></li>
				<li><img src="../frontend/expression/goemoji_e14c.png" face="(S)" /></li>
				<li><img src="../frontend/expression/goemoji_e03c.png" face="(kalaok)" /></li>
				<li><img src="../frontend/expression/goemoji_e33b.png" face="(Ff)" /></li>
				<li><img src="../frontend/expression/goemoji_e015.png" face="(Te)" /></li>
				<li><img src="../frontend/expression/goemoji_e016.png" face="(Bb)" /></li>
            </ul>
        </div>
    	
    	<div class="c_face" style="display:none;" id="face_2" >
            <ul>
				<li><img src="../frontend/expression/emo_im_angel.png" face="{0:-)}" /></li>
				<li><img src="../frontend/expression/emo_im_cool.png" face="{B-)}" /></li>
				<li><img src="../frontend/expression/emo_im_crying.png" face="{:'(}" /></li>
				<li><img src="../frontend/expression/emo_im_embarrassed.png" face="{:-[}" /></li>
				<li><img src="../frontend/expression/emo_im_foot_in_mouth.png" face="{:-!}" /></li>
				<li><img src="../frontend/expression/emo_im_happy.png" face="{:-)}" /></li>
				<li><img src="../frontend/expression/emo_im_kissing.png" face="{:-*}" /></li>
				<li><img src="../frontend/expression/emo_im_laughing.png" face="{:-D}" /></li>
				<li><img src="../frontend/expression/emo_im_lips_are_sealed.png" face="{:-X}" /></li>
				<li><img src="../frontend/expression/emo_im_money_mouth.png" face="{:-$}" /></li>
				<li><img src="../frontend/expression/emo_im_sad.png" face="{:-(}" /></li>
				<li><img src="../frontend/expression/emo_im_surprised.png" face="{=-0}" /></li>
				<li><img src="../frontend/expression/emo_im_tongue_sticking_out.png" face="{:-P}" /></li>
				<li><img src="../frontend/expression/emo_im_undecided.png" face="{:-\}" /></li>
				<li><img src="../frontend/expression/emo_im_winking.png" face="{;-)}" /></li>
				<li><img src="../frontend/expression/emo_im_wtf.png" face="{o_O}" /></li>
				<li><img src="../frontend/expression/emo_im_yelling.png" face="{:O}" /></li>
            </ul>
        </div>
        	
        <div class="c_face" style="display:none;" id="face_3">
            <ul>
				<li><img src="../frontend/expression/emo_im_angel_go.png" face="O:-)" /></li>
				<li><img src="../frontend/expression/emo_im_cool_go.png" face="B-)" /></li>
				<li><img src="../frontend/expression/emo_im_crying_go.png" face=":'(" /></li>
				<li><img src="../frontend/expression/emo_im_embarrassed_go.png" face=":-[" /></li>
				<li><img src="../frontend/expression/emo_im_foot_in_mouth_go.png" face=":-!" /></li>
				<li><img src="../frontend/expression/emo_im_happy_go.png" face=":-)" /></li>
				<li><img src="../frontend/expression/emo_im_kissing_go.png" face=":-*" /></li>
				<li><img src="../frontend/expression/emo_im_laughing_go.png" face=":-D" /></li>
				<li><img src="../frontend/expression/emo_im_lips_are_sealed_go.png" face=":-X" /></li>
				<li><img src="../frontend/expression/emo_im_money_mouth_go.png" face=":-$" /></li>
				<li><img src="../frontend/expression/emo_im_sad_go.png" face=":-(" /></li>
				<li><img src="../frontend/expression/emo_im_surprised_go.png" face="=-0" /></li>
				<li><img src="../frontend/expression/emo_im_tongue_sticking_out_go.png" face=":-p" /></li>
				<li><img src="../frontend/expression/emo_im_undecided_go.png" face=":-\" /></li>
				<li><img src="../frontend/expression/emo_im_winking_go.png" face=";-)" /></li>
				<li><img src="../frontend/expression/emo_im_wtf_go.png" face="o_O" /></li>
				<li><img src="../frontend/expression/emo_im_yelling_go.png" face=":O" /></li>
            </ul>
        </div>	
			<!--表情end-->				</td>
			    <td width="25%" valign="top"><input name="button" type="button" onclick="reply()" value="发送" />
		        <br>
		        <input name="send" type="radio" value="return" checked="checked" />
		        回车<br>
		        <input type="radio" name="send" value="ctrlreturn" />
		        CTRL+回车<br>
		        <input type="checkbox" id="next" value="1" />
		        回复后自动处理下一条</td>
		      </tr>
			  <tr><td><select size="5" id="quick_reply">
			      <option>你好</option>
			      <option>你是哪里的？</option>
				 </select>
			    </td>
			    <td><input type="submit" name="Submit2" value="增加自动回复" id="Submit2" /></td>
		      </tr>
			</table></td>
		  </tr>
	  </table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>

</body>
</html>