<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
</style>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>ajaxfileupload.js"></script>
<script type="text/javascript">
function set_base()
{
	var message = '';

	if ( $( '#name' ).val() != '' ) message = 'name=' + encodeURIComponent( $( '#name' ).val() );
	if ( $( '#pass' ).val() != '' ) message += '&pass=' + encodeURIComponent( $( '#pass' ).val() );
	if ( $( '#mood' ).val() != '' ) message += '&mood=' + encodeURIComponent( $( '#mood' ).val() );
	if ( $( '#province' ).val() != '' ) message += '&province=' + encodeURIComponent( $( '#province' ).val() );
	if ( $( '#city' ).val() != '' ) message += '&city=' + encodeURIComponent( $( '#city' ).val() );
	if ( $( '#birthday' ).val() != '' ) message += '&birthday=' + encodeURIComponent( $( '#year' ).val() + '-'+ $( '#moon' ).val() + '-' + $( '#day' ).val() );
	if ( $( '#sex' ).val() != '' ) message += '&sex=' + encodeURIComponent( $( '#sex' ).val() );
	if ( $( '#npc' ).val() != '' ) message += '&npc=' + encodeURIComponent( $( '#npc' ).val() );
	if ( $( '#qq' ).val() != '' ) message += '&qq=' + encodeURIComponent( $( '#qq' ).val() );
	if ( $( '#mobile' ).val() != '' ) message += '&mobile=' + encodeURIComponent( $( '#mobile' ).val() );
	if ( $( '#bar' ).val() != '' ) message += '&bar=' + encodeURIComponent( $( '#bar' ).val() );
	if ( $( '#email' ).val() != '' ) message += '&email=' + encodeURIComponent( $( '#email' ).val() );
	if ( $( '#height' ).val() != '' ) message += '&height=' + encodeURIComponent( $( '#height' ).val() );
	if ( $( '#interest' ).val() != '' ) message += '&interest=' + encodeURIComponent( $( '#interest' ).val() );
	if ( $( '#profession' ).val() != '' ) message += '&profession=' + encodeURIComponent( $( '#profession' ).val() );
	if ( $( '#nick' ).val() != '' ) message += '&nick=' + encodeURIComponent( $( '#nick' ).val() );
	if ( $( '#marriage' ).val() != '' ) message += '&marriage=' + encodeURIComponent( $( '#marriage' ).val() );
	if ( $( '#confessions' ).val() != '' ) message += '&confessions=' + encodeURIComponent( $( '#confessions' ).val() );
	if ( $( '#income' ).val() != '' ) message += '&income=' + encodeURIComponent( $( '#income' ).val() );

	if ( message != '' )
	{
		$( '#modify_button' ).attr( 'disabled', true );

		$.ajax
		(
			{
				type : "POST",
				url : "add_user.php",
				dataType : 'json',
				data : message,
				success : function( data )
					{
						$( '#modify_button' ).attr( 'disabled', false );
						alert( data.message );
						if ( data.status == 0 ) window.location.href = './user.php';
					}
			}
		);
	}
}

$(function(){
	$('#info_table tr:odd').css('background-color','#E2E7ED');
});
</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">添加用户</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：可以在此添加用户。</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" id="info_table">
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">名称：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="name"></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">密码：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="pass" value="123"></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2"></td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">心情：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="mood" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">省份：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="province" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">城市：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="city" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">生日：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30">
							<select name="year" id="year">
								<option value=''>-</option>
								<?php for($i=date('Y');$i>1920;$i--){?>
								<option value="<?php echo $i?>"<?php if($year==$i)echo ' selected'?>><?php echo $i?></option>
								<?php }?>
							</select>
							-
							<select name="moon" id="moon">
								<option value=''>-</option>
								<?php for($i=1;$i<=12;$i++){?>
								<option value="<?php echo $i?>"<?php if($moon==$i)echo ' selected'?>><?php echo $i?></option>
								<?php }?>
							</select>
							-
							<select name="day" id="day">
								<option value=''>-</option>
								<?php for($i=1;$i<=31;$i++){?>
								<option value="<?php echo $i?>"<?php if($day==$i)echo ' selected'?>><?php echo $i?></option>
								<?php }?>
							</select>
							</td>
							<td width="45%" height="30" class="left_txt">&nbsp;如：1994-6-30</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">性别：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><select id="sex"><option value="0">男</option><option value="1">女</option></select></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">NPC：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><select id="npc"><option value="1" selected>是</option><option value="0">否</option></select></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">QQ：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="qq" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">手机：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="mobile" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">蒲场：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="bar" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">邮件：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="email" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">身高：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="height" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">体型：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">职业：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="profession" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">昵称：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="nick" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2"></td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">情感状况：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="marriage" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">爱好：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="interest" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">我的自白：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><textarea id="confessions"></textarea></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>
						<tr>
							<td width="20%" height="30" align="right" class="left_txt2">收入：</td>
							<td width="3%">&nbsp;</td>
							<td width="32%" height="30"><input type="text" id="income" value=""></td>
							<td width="45%" height="30" class="left_txt">&nbsp;</td>
						</tr>		
						<tr>
							<td colspan="3" align="center">
								<table width="30%">
									<tr align="center">
										<td><input type="button" value="确定" id="modify_button" onclick="set_base()" /></td>
										<td><input type="button" value="返回" onclick="window.history.go(-1)" /></td>
									</tr>
								</table>
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>