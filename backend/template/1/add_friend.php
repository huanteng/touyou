<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>common.js"></script>	
<script type="text/javascript">
function submit()
{	
	if( $( '#friend' ).val().length == 0 )
	{
		alert('关注人不能空！');
		$( '#friend' ).focus();
		return false;
	}
	
	if( $( '#user' ).val().length == 0 )
	{
		alert('粉丝不能空！');
		$( '#user' ).focus();
		return false;
	}
	
	$.ajax
	(
		{
			type : "POST",
			url : "add_friend.php",
			dataType : 'json',
			async : false,
			data : 'user=' + $( '#user' ).val() + '&friend='+ $( '#friend' ).val(),
			success : function( result )
			{
				alert( result.message );
			}
		}
	);
}
</script>

<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">添加关注关系</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">
		<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：</td>
			  </tr>
			  <tr>
				<td align="left" class="left_txt">
					关注人：<input type="text" name="friend" id="friend" size="10" value="">&nbsp;<br/>
					粉丝：<input type="text" name="user" id="user" size="10" value="">&nbsp;
					<input type="button" value="添加" onclick="submit()"><br/>
				</td>
			  </tr>
		</table>
				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>

	</td>
    <td background="template/1/images//mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>