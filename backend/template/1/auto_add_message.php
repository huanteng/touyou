<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.a2:link{ color:#0A34BC; text-decoration: none;}
.a2:visited{ color:#0A34BC; text-decoration: none;}
.a2:hover{ color:#0A34BC; text-decoration: underline;}
.a2:active{color:#0A34BC; text-decoration: none;}
-->
</style>
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo $_template['js']; ?>common.js"></script>
<script type="text/javascript">
function selected_all()
{
	$('input:checkbox[name=\'id\[\]\']').attr('checked',$('#btn_select_all').attr('checked'));
}
function delete_all()
{
	if( confirm('确定删除吗？') )
	{
		$('#form2').submit();
	}
}
function submit_queue()
{
	if( $( '#user' ).val().length == 0 )
	{
		alert('接收人不能空！');
		$( '#user' ).focus();
		return false;
	}

	if( $( '#content' ).val().length == 0 )
	{
		alert('内容空着呢，亲！');
		$( '#content' ).focus();
		return false;
	}

	if( $( '#content' ).val().length >= 160 )
	{
		alert('内容过长，只支持80个字啊，亲！');
		$( '#content' ).focus();
		return false;
	}

	var sex = '';
	var temp = [] ;
	if( $("#male").attr('checked') == true ) temp.push( 0 );
	if( $("#female").attr('checked') == true ) temp.push( 1 );
	sex = temp.join(',');

	$.ajax
	(
		{
			type : "POST",
			url : "auto_add_message.php",
			dataType : 'json',
			async : false,
			data : 'user=' + $( '#user' ).val() + '&from='+ $( '#from' ).val() +'&sex=' + sex + '&time=' + $( '#time_1' ).val() + '&content=' + encodeURIComponent($( '#content' ).val()),
			success : function( result )
			{
				alert( result.message );

				if( result.status == 0 )
				{
					var temp = [];
					temp.push('<tr align="center"><td><input type="checkbox" name="id[]" value=""/></td>');
					temp.push('<td>'+result.id+'</td><td>'+$('#time_1').val()+'</td>');
					temp.push('<td>'+$('#name').val()+'</td>');
					temp.push('<td>'+$('#content').val()+'</td>');
					temp.push('<td><table width="60%"><tr align="center"><td><a href="./auto_publish_comment2.php?id='+result.id+'&type=task_queue5">营销</a></td></tr></table></td></tr>');

					$('#list').append( temp.join('') );
				}
			}
		}
	);
}
</script>

<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">发起聊天</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：以下是发起聊天列表，您可以添加预发布。</td>
			  </tr>
			  <tr>
				<td align="left" class="left_txt">
					<form id="form1" method="post">
					接收人：<input type="text" name="user" id="user" size="10" value="">&nbsp;只支持非NPC<br/>
					发送人：<input type="text" name="from" id="from" size="10" value="">&nbsp;&nbsp;不填为随机NPC<input type="checkbox" name="sex[]" id="male" value="0">男<input type="checkbox" name="sex[]" id="female" value="1">女<br/>
					内容：<input type="text" name="content" id="content" size="80" value=""><br/>
					发布时间：<input type="text" name="time" id="time_1" size="20" value="<?php echo date('Y-m-d H:i:s',time()+900);?>">&nbsp;&nbsp;<a href="javascript:add_time(300,1)" title="增加5分钟">+5m</a>&nbsp;&nbsp;<a href="javascript:add_time(3600,1)" title="增加1小时">+1h</a>&nbsp;&nbsp;<a href="javascript:add_time(-300,1)" title="减少5分钟">-5m</a>&nbsp;&nbsp;<a href="javascript:add_time(-3600,1)" title="减少1小时">-1h</a>&nbsp;&nbsp;<a href="javascript:add_time(0,1)" title="自定义">+?m</a>&nbsp;&nbsp;如不填写，既为马上发布，会直接在条目管理生成条目<br/>
					<input type="button" value="发布" onclick="submit_queue()"><br/>
					</form>
				</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
				<form id="form2" method="post">
				<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="left_txt" id="list" style="display:none;">
						<tr class="title f1">
							<th width="5%">选择</th>
							<th width="5%">序号</th>
							<th width="10%">时间</th>
							<th width="10%">发送人</th>
							<th width="10%">接收人</th>
							<th>内容</th>
							<th width="15%">操作</th>
						</tr>
					<?php
						foreach( $data as $index => $value )
						{
					?>
						<tr align="center"<?php if ( $index % 2 != 0 ) echo ' class="f2"'; ?>>
							<td><input type="checkbox" name="id[]" value="<?php echo $value['id']; ?>"/></td>
							<td><?php echo $value['id']; ?></td>
							<td><?php echo date( 'Y-m-d H:i', $value['time'] ); ?></td>
							<td><?php echo $value['sender']; ?></td>
							<td><?php echo $value['user']; ?></td>
							<td><?php echo $value['content']; ?></td>
							<td><table width="60%"><tr align="center"><td><a href="./auto_publish_comment2.php?id=<?php echo $value['id']; ?>&type=task_queue5">营销</a></td></tr></table></td>
						</tr>
					<?php
						}
					?>
					</table>

					<div style="text-align:center; height:15px; padding-top:4px; font-size:12px; display:none;">

					<span style="float: left;">
						<input type="hidden" name="op" value="delete" />
						<input type="checkbox" onclick="selected_all()" id="btn_select_all" />全选&nbsp;
						<input type="button" value="删除" onclick="delete_all()" id="btn_delete_all"/>
					</span>

					记录数：<?php echo $bar['total']; ?>  <a href="<?php echo $bar['prev_link']; ?>"><img src="<?php echo $_template['img']; ?>prev.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;
					<?php
						foreach( $bar['number'] as $value )
						{
							if ( $value['is_current'] )
							{
					?>
						<span class="homeStockRise"><strong><?php echo $value['name']; ?></strong></span>&nbsp;
					<?php
							}
							else
							{
					?>
						<a href="<?php echo $value['link']; ?>"><?php echo $value['name']; ?></a>&nbsp;
					<?php
							}
					?>
					<?php
						}
					?>
					<a href="<?php echo $bar['next_link']; ?>"><img src="<?php echo $_template['img']; ?>next.gif" alt="" width="12" height="12" border="0" /></a>&nbsp;[<?php echo $bar['current']; ?>/<?php echo $bar['page']; ?>]</div>

				</td>
			  </tr>
			</table></form></td>
		  </tr>
		</table>
		</form>
	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>