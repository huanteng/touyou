<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>dice</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #F8F9FA;
}
</style>
<link href="<?php echo $_template['css']; ?>skin.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $_template['js']; ?>jquery.js"></script>
<script type="text/javascript">

</script>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="17" valign="top" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/left-top-right.gif" width="17" height="29" /></td>
    <td valign="top" background="<?php echo $_template['img']; ?>/content-bg.gif"><table width="100%" height="31" border="0" cellpadding="0" cellspacing="0" class="left_topbg" id="table2">
      <tr>
        <td height="31"><div class="titlebt">查看比赛</div></td>
      </tr>
    </table></td>
    <td width="16" valign="top" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/nav-right-bg.gif" width="16" height="29" /></td>
  </tr>
  <tr>
    <td valign="middle" background="<?php echo $_template['img']; ?>/mail_leftbg.gif">&nbsp;</td>
    <td valign="top" bgcolor="#F7F8F9">

	<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td valign="top"><table style="margin-top:5px;" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td class="left_txt">说明：可以在此查看比赛的具体内容。</td>
			  </tr>
			  <tr>
				<td height="20"><table width="100%" height="1" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
				  <tr>
					<td></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td>
					<p>类型：<?php echo $type_name; ?> 时间：<?php echo date( 'Y-m-d H:i:s', $start ); ?></p>
					<p>用户1：<?php if ( $user_1_npc == '1' ) echo '<font color="red">N</font> '; ?><?php echo $user_1_name; ?><?php if ( isset( $winner ) && $user_1 == $winner ) echo ' (赢)'; ?>
						<?php if ( $must_win == '1' ) echo $old . ' => '; ?>[ <?php echo $user_1_1; ?>、<?php echo $user_1_2; ?>、<?php echo $user_1_3; ?>、<?php echo $user_1_4; ?>、<?php echo $user_1_5; ?> ]</p>
					<p>用户2：<?php if ( $user_2_npc == '1' ) echo '<font color="red">N</font> '; ?><?php echo $user_2_name; ?><?php if ( isset( $winner ) && $user_2 == $winner ) echo ' (赢)'; ?>
						<?php if ( $must_win == '2' ) echo $old . ' => '; ?>[ <?php echo $user_2_1; ?>、<?php echo $user_2_2; ?>、<?php echo $user_2_3; ?>、<?php echo $user_2_4; ?>、<?php echo $user_2_5; ?> ]</p>
					<hr>
					<?php
						foreach( $shout as $value )
						{
							if ( $value['close'] == 0 )
							{
					?>
					<p><?php echo date( 'H:i:s', $value['time'] ); ?>，<?php echo $value['user'] == $user_1 ? $user_1_name : $user_2_name; ?>：<?php echo $value['quantity']; ?> 个 <?php echo $value['number']; ?><?php if ( $value['zhai'] == '1' ) echo ' (斋)'; ?><?php if ( $value['managed'] == '1' ) echo ' (系统代喊)'; ?></p>
					<?php
							}
							else
							{
					?>
					<p><?php echo date( 'H:i:s', $value['time'] ); ?>，<?php echo $value['user'] == $user_1 ? $user_1_name : $user_2_name; ?>：开<?php if ( $value['managed'] == '1' ) echo ' (系统代开)'; ?></p>
					<?php
							}
						}
					?>
					<?php // if ( isset( $value['style'] ) && $value['style'] != '0' ) { ? ><p><br/>胜出方式：< ?php if ( $value['style'] == '1' ) echo '普通'; ? >< ?php if ( $value['style'] == '2' ) echo '斋'; ? >< ?php if ( $value['style'] == '3' ) echo '破斋'; ? ></p>< ?php }
					?>
				</td>
			  </tr>
			</table></td>
		  </tr>
		</table>

	</td>
    <td background="<?php echo $_template['img']; ?>/mail_rightbg.gif">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_leftbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_left2.gif" width="17" height="17" /></td>
    <td background="<?php echo $_template['img']; ?>/buttom_bgs.gif"><img src="<?php echo $_template['img']; ?>/buttom_bgs.gif" width="17" height="17"></td>
    <td valign="bottom" background="<?php echo $_template['img']; ?>/mail_rightbg.gif"><img src="<?php echo $_template['img']; ?>/buttom_right2.gif" width="16" height="17" /></td>
  </tr>
</table>
</body>
</html>