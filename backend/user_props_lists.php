<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$this->input['_npc'] = isset( $this->input['_npc'] ) ? $this->input['_npc'] : '';
			$this->input['_sex'] = isset( $this->input['_sex'] ) ? $this->input['_sex'] : '';
			$this->input['props'] = 1;

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$props = load( 'biz.props' );

			$template->appoint( $props->user_props_lists( $this->input ) );
			$template->appoint( $this->input );
			echo $template->parse( 'user_props_lists.php' );
		}
	}

	$action = new action();
	$action->run();
?>