<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$album = load( 'biz.album' );
				$task_queue = load( 'biz.task_queue' );
				$template = load( 'template', array( 'dir' => 'template/' ) );

				$done_array = array();
				$task = $album->database->unique( 'select * from task_queue where id = ' . $this->input['id'] );
				$photo = $album->database->unique( 'select album.*, user.name from album, user where album.user = user.id and album.id = ' . $task['target'] );
				$task['data'] = unserialize( $task['data'] );

				foreach( $task['data']['done'] as $log_id ) $done_array[] = $album->database->unique( 'select vote_album_log.*, user.name from vote_album_log, user where vote_album_log.user = user.id and vote_album_log.id = ' . $log_id );

				$template->assign( 'task', $task );
				$template->assign( 'photo', $photo );
				$template->assign( 'status_dict', $task_queue->status_dict() );
				$template->assign( 'done_array', $done_array );
				$template->assign( 'album_object', $album );

				echo $template->parse( 'task_item_3.php' );
			}
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '参数错误' );

			if ( isset( $this->input['task_id'] ) && isset( $this->input['start_time'] ) && isset( $this->input['score_1'] ) && isset( $this->input['score_2'] ) && isset( $this->input['interval_1'] ) && isset( $this->input['interval_2'] ) && isset( $this->input['times'] ) )
			{
				$database = load( 'database' );
				$info = $database->unique( 'select * from task_queue where id = ' . $this->input['task_id'] );

				if ( isset( $info['id'] ) && $info['status'] == 1 && $info['time'] > time() )
				{
					$data = array( 'score_1' => $this->input['score_1'], 'score_2' => $this->input['score_2'], 'interval_1' => $this->input['interval_1'], 'interval_2' => $this->input['interval_2'], 'times' => $this->input['times'], 'done' => array() );

					$queue = array( 'create' => time(), 'type' => 3, 'time' => strtotime( $this->input['start_time'] ), 'status' => 1, 'data' => serialize( $data ) );
					$database->set( 'task_queue', $queue, array( 'id' => $info['id'] ) );

					$result = array( 'status' => 0, 'message' => '成功修改' );
				}
				else
				{
					$result = array( 'status' => -2, 'message' => 'id错误' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>