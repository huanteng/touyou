<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'add_configure.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入名称' );

			if ( isset( $this->input['name'] ) && $this->input['name'] != '' )
			{
				if ( isset( $this->input['data'] ) && $this->input['data'] != '' )
				{
					if ( isset( $this->input['data'] ) && $this->input['data'] != '' )
					{
						$configure = load( 'biz.configure' );
						$configure->database->add( 'configure', $this->input );
						$result = array( 'status' => 0, 'message' => '成功添加配置参数' );
					}
					else
					{
						$result = array( 'status' => -3, 'message' => '请输入编码' );
					}
				}
				else
				{
					$result = array( 'status' => -2, 'message' => '请输入内容' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>