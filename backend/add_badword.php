<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'add_badword.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入违禁' );

			if ( isset( $this->input['old'] ) && $this->input['old'] != '' )
			{
				$badword = load( 'biz.badword' );
				$badword->database->add( 'badword', $this->input );
				$result = array( 'status' => 0, 'message' => '成功添加过滤' );
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>