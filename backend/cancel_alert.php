<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['type'] ) && is_numeric( $this->input['type'] ) && isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$database = load( 'database' );

				if ( $this->input['type'] == 1000 )
				{
					$info = $database->unique( 'select * from message where id = ' . $this->input['id'] );
					$database->command( "update message set `read` = 1 where receiver = " . $info['receiver'] . ' and sender = ' . $info['sender'] );
				}
				else if ( $this->input['type'] == 1001 || $this->input['type'] == 1002 || $this->input['type'] == 1009 )
				{
					$database->command( 'update comment set `check` = 1 where id = ' . $this->input['id'] );
				}
			}

			header( 'location: check_alert.php?id=' . $this->input['type'] );
		}
	}

	$action = new action();
	$action->run();
?>