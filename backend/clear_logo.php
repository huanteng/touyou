<?php
	require '../library/config.php';
	$final = array( 'code' => 0, 'data' => '参数错误' );
	$user = load( 'biz.user' );
	$name = 'logo_file';

	if ( isset( $_REQUEST['user'] ) && is_numeric( $_REQUEST['user'] ) )
	{
		$user = load( 'biz.user' );
		$info = $user->database->unique( 'select logo from user where id = ' . $_REQUEST['user'] );

		if ( isset( $info['logo'] ) && $info['logo'] != '' )
		{
			$real = $user->logo_base_dir . $info['logo'];
			if ( is_file( $real ) ) unlink( $real );
		}

		$user->database->command( "update user set logo = '' where id = " . $_REQUEST['user'] );
		$final = array( 'code' => 1, 'data' => '操作成功' );
	}

	echo json_encode( $final );
?>