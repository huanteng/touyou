<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$news = load( 'biz.news' );
			$template = load( 'template', array( 'dir' => 'template/' ) );
			$template->assign( 'cate_dict', $news->get_cate_dict() );
			echo $template->parse( 'add_news.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入标题' );

			if ( isset( $this->input['title'] ) && $this->input['title'] != '' )
			{
				if ( isset( $this->input['content'] ) && $this->input['content'] != '' )
				{
					$this->input['content'] = $_REQUEST['content'];

					$news = load( 'biz.news' );
					$this->input['time'] = time();
					$news->database->add( 'news', $this->input );
					
					$result = array( 'status' => 0, 'message' => '成功添加文章' );
				}
				else
				{
					$result = array( 'status' => -1, 'message' => '请输入内容' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>