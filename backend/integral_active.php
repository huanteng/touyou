<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			$database = load( 'database' );

			$current = $database->unique( 'select time from integral_prize_pool order by time desc limit 1' );
			$data = array();

			if ( time() >= $current['time'] )
			{
				$data = $database->select( 'select * from integral_rank where time = ' . $current['time'] );

				foreach( $data as $key => $rank )
				{
					$exisit = $database->unique( 'select vs from integral_match where winner = 0 and ( user_1 = ' . $rank['user'] . ' or user_2 = ' . $rank['user'] . ' ) limit 1' );
					$data[$key]['vs'] = isset( $exisit['vs'] ) ? $exisit['vs'] : 0;
					$role = $database->unique( 'select name, npc from user where id = ' . $rank['user'] );
					$data[$key]['npc'] = $role['npc'] == 1;
					$data[$key]['name'] = $role['name'];
				}
			}

			$template->assign( 'data', $data );
			echo $template->parse( 'integral_active.php' );
		}
	}

	$action = new action();
	$action->run();
?>