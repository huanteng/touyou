<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$this->input['history'] = 1;

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$challenge = load( 'biz.challenge' );

			$template->appoint( $challenge->lists( $this->input ) );
			$template->appoint( $this->input );

			echo $template->parse( 'challenge.php' );
		}
	}

	$action = new action();
	$action->run();
?>