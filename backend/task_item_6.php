<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['id'] = isset( $this->input['id'] ) ? $this->input['id'] : 0;

			$database = load('database');
			$info = $database -> unique("select data,status from task_queue where id=".$this->input['id']);

			if( $info['data'] && $info['status'] > 1 )
			{
				$info['data'] = unserialize($info['data']);
				foreach( $info['data'] as $value) $user[] = $value['user'];
				header('Location: album.php?uid='.join(',',$user));
			}
			else
			{
				$this->prompt( '这个任务还没完成，不能查看', array( array( 'url' => 'task_queue.php', 'name' => '任务队列', 'default' => true ) ) );
			}
		}
	}

	$action = new action();
	$action->run();
?>