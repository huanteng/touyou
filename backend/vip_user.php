<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$this->input['_npc'] = isset( $this->input['_npc'] ) ? $this->input['_npc'] : '';

			$user = load( 'biz.user' );
			$template = load( 'template', array( 'dir' => 'template/' ) );
			$result = $user->list_vip( $this->input );

			$template->appoint( $result );
			$template->appoint( $this->input );

			echo $template->parse( 'vip_user.php' );
		}
	}

	$action = new action();
	$action->run();
?>