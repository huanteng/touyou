<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$database = load( 'database' );
			$navigation = load( 'navigation' );
			$template = load( 'template', array( 'dir' => 'template/' ) );
						
			$task_queue = $database->select( 'select * from task_queue where type = 4 and status = 1 order by id asc' );
			$result = $database->unique("select count(*) as total from task_queue where type = 4");
			$result['bar'] = $navigation->compute( $result['total'], 20 );
			
			foreach( $task_queue as $index => $value )
			{
				$data = isset($value['data']) ? unserialize($value['data']) : array();
				
				if( $data['sender'] != 0 )
				{
					$temp = $database->unique("select name from user where id = '".$data['sender']."'");
					$value['sender'] = isset($temp['name']) ? $temp['name'] : '';
				}
				
				if( $data['user'] != 0 )
				{
					$temp = $database->unique("select name from user where id = '".$data['user']."'");
					$value['user'] = isset($temp['name']) ? $temp['name'] : '';
				}
				
				$value['content'] = isset($data['content']) ? $data['content'] : '';
				
				$task_queue[$index] = $value;
			}			
			
			$template->assign( 'data', $task_queue );
			$template->assign( 'bar', $result['bar'] );
			echo $template->parse( 'auto_add_message.php' );
		}
		
		function do_post()
		{
			$result = array( 'code' => -1, 'message'=>'参数错误' );

			if ( isset( $this->input['user'] ) && $this->input['user'] != '' && isset( $this->input['from'] ) && isset( $this->input['content'] ) && isset( $this->input['time'] ) && isset( $this->input['sex'] ) )
			{
				$database = load( 'database' );

				$user_info = $database->unique( "select id,npc from user where name = '" . $this->input['user'] ."'" );
				
				if( $this->input['from'] != '' )
				{
					$from_info = $database->unique( "select id from user where name = '" . $this->input['from'] ."'" );
					if( !isset($from_info['id']) ) $this->input['from'] = '';
				}
				
				if( $this->input['from'] == '' )
				{
					$sex = in_array( $this->input['sex'], array('0,1','1','0','1,0') ) ? $this->input['sex'] : '0,1';
					$from_info = $database->unique( "select id,npc from user where npc = 1 and sex in ( ".$sex." ) order by rand() limit 1" );
				}
				
				if( isset( $user_info['npc'] ) && $user_info['npc'] == 1 )
				{
					$result = array( 'code' => -2,'message' => '接收人必须是真人' );
				}
				elseif( isset($user_info['id']) && isset($from_info['id']) )
				{

					$id = $database->add( 'task_queue', array( 'user' => $user_info['id'], 'create' => time(), 'creater'=>$this->account,
						'type' => 4, 'time' => strtotime( $this->input['time'] ), 'status' => 1, 'target' => $user_info['id'],
						'data' => serialize( array( 'uid' => $from_info['id'], 'user' => $user_info['id'], 'content' => $this->input['content'] ) ) ) );

					if ( $id > 0 )
					{
						$result = array( 'code' => 1,'message' => '添加成功' );
					}
				
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>