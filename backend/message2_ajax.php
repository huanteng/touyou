<?php
require 'backend.php';

class action extends backend
{
	function do_get()
	{
		$db = load( 'database' );

		if ( $this->input['type'] == 0 )
		{
			$message = load( 'biz.message' );
			$result = $message->npc_chat( $this->input );

			echo json_encode( $result );
		}
		elseif( $this->input['type'] == 1001 )
		{
			$this->input['_name'] = isset( $this->input['q'] ) ? $this->input['q'] : '';
			$this->input['_sex'] = isset( $this->input['_sex'] ) ? $this->input['_sex'] : '';
			$this->input['_npc'] = isset( $this->input['_npc'] ) ? $this->input['_npc'] : '';
			$this->input['channel'] = isset( $this->input['channel'] ) ? $this->input['channel'] : '';
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;

			$where = $this->input['_name'] != '' ? " and name like '" . $this->input['_name'] . "%'" : '';

			if( $this->input['_sex'] != '' ) $where .= ' and sex = '.$this->input['_sex'];
			if( $this->input['_npc'] == '0' )
			{
				$where .= ' and npc = '.$this->input['_npc'];
			}
			else
			{
				$where .= " and (npc = 1 or name = '骰妖子')";
			}
			if( $this->input['channel'] != '' ) $where .= " and channel = '".$this->input['channel']."'";

			$user = load( 'biz.user' );

			$result = $user->lists( $page, $where, 30 );

			foreach ($result['data'] as $row) {
				echo $row['name'].'_'.$row['id'].'|'.$row['id']."\n";
			}
		}
	}

	function do_post()
	{
		$function = 'ajax_' .$this->input['type'];
		unset( $this->input['type'] );
		echo json_encode( $this->$function( $this->input ) );
	}

	// 回复
	function ajax_reply($in)
	{
		$result = array( 'code' => -1 );

		if ( isset( $in['id'] ) && isset( $in['user'] ) && isset( $in['from'] ) && isset( $in['content'] ) && isset( $in['time'] ) )
		{
			$db = load( 'database' );

			$sql = "select time from task_queue where status=1 and creater = '" . $this->account . "' and target= '$in[user]' order by id desc";
			$task_queue = $db->unique( $sql );

			// 2do，此处检测不出数据。时间值不对
			if( isset($task_queue['time']) )
			{
				$in['time'] = $task_queue['time'] + $in['time'];
			}
			else
			{
				$in['time'] = time() + $in['time'];
			}

			$id = $db->add( 'task_queue', array( 'user' => $in['user'], 'create' => time(), 'creater'=>$this->account, 'type' => 4, 
				'time' => $in['time'], 'status' => 1, 'target' => $in['user'], 'data' => serialize( array( 'uid' => $in['from'],
					'user' => $in['user'], 'content' => addslashes( $in['content'] ) ) ) ) );

			if ( $id > 0 )
			{
				$db->command( "update message set `read` = 1 where receiver = " . $in['from'] . ' and sender = ' . $in['user'] );
				$result['code'] = 1;
			}
		}

		return $result;
	}

	// 关注
	function ajax_2($in)
	{
		$result = array( 'code' => -1 );

		$firend = load('biz.friend');
		$result['code'] = $firend->create( array('uid'=>$in['npc'],'friend'=>$in['user']) );

		switch( $result['code'] )
		{
			case 0: $result['message'] = '已经关注'; break;
			case 1: $result['message'] = '关注成功'; break;
			case -1: $result['message'] = '参数不完整'; break;
			case -3: $result['message'] = '参数错误'; break;
			default: $result['message'] = '关注成功'; break;
		}

		return $result;
	}
	// <editor-fold defaultstate="collapsed" desc="ajax_challenge_ok，挑战信息，不作作何响应，仅删除">
	function ajax_challenge_ok($in)
	{
		$user = load( 'biz.user' );
		$user->db->command( 'delete from message where id = ' . $in['id']);
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ajax_challenge_accept，接受挑战">
	function ajax_challenge_accept($in)
	{
		$result = array( 'code' => -1 );

		$user = load( 'biz.user' );
		
		$challenge = load( 'biz.challenge' );
		$result['code'] = $challenge->accept( array( 'uid' => $in['npc'], 'user' => $in['sender'], 'gold' => $in['gold'] ) );

		// <editor-fold defaultstate="collapsed" desc="必赢">
		if( $in['must_win'] == 1 )
		{
			$vs = $user->db->get1( '*', 'vs', array( '0' => "( user_1 = $in[npc] and user_2 = $in[sender] ) or ( user_2 = $in[npc] and user_1 = $in[sender] )" ), 'id desc' );
			if ( isset( $vs['id'] ) )
			{
				$user->db->set( 'vs', array( 'must_win' => ($vs['user_1'] == $in['npc']) ? 1 : 2 ), array( 'id' => $vs[ 'id' ] ) );
			}
		}
		// </editor-fold>

		$user->db->command( 'delete from message where id = ' . $in['id']);

		switch($result['code'])
		{
			case -1: $result['memo'] = "缺少sid"; break;
			case -2: $result['memo'] = "sid不正确"; break;
			case -3: $result['memo'] = "挑战不存在"; break;
			case -4: $result['memo'] = "投注额大于余额"; break;
			case 1: $result['memo'] = "成功应战";break;
			default: $result['memo'] = "错误，code=" .$result['code'];
		}

		return $result;
	}
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="ajax_challenge_reject，拒绝挑战">
	function ajax_challenge_reject($in)
	{
		$result = array( 'code' => -1 );
		
		$challenge = load( 'biz.challenge' );
		$result = $challenge->reject( array( 'uid' => $in['npc'], 'user' => $in['sender'] ) );

		$challenge->db->command( 'delete from message where id = ' . $in['id']);

		return $result;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ajax_challenge_next，继续挑战">
	function ajax_challenge_next( $in )
	{
		$result = array( 'code' => -1 );

		$challenge = load( 'biz.challenge' );
		$result = $challenge->next( array( 'uid' => $in['npc'] ) );

		$challenge->db->command( 'delete from message where id = ' . $in['id']);

		switch($result['code'])
		{
			case -1: $result['memo'] = "sid错误"; break;
			case -2: $result['memo'] = "你没有相关挑战赛的数据"; break;
			case -3: $result['memo'] = "金币不足"; break;
			case -4: $result['memo'] = "提交失败"; break;
			case 1: $result['memo'] = "成功应战";break;
			default: $result['memo'] = "错误，code=" .$result['code'];
		}

		return $result;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ajax_challenge_must_win，设置必赢">
	function ajax_challenge_must_win($in)
	{
		$user = load( 'biz.user' );
		$vs = $user->db->get1( '*', 'vs', array( '0' => "( user_1 = $in[user] and user_2 = $in[npc] ) or ( user_1 = $in[npc] and user_2 = $in[user] )" ), 'id desc' );
		if ( isset( $vs['id'] ) )
		{
			$user->db->set( 'vs', array( 'must_win' => ($vs['user_1'] == $in['npc']) ? 1 : 2 ), array( 'id' => $vs[ 'id' ] ) );
		}

		$user->db->command( 'delete from message where id = ' . $in['id']);

		return array( 'code' => 1, 'memo' => '设置成功' );
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="ajax_challenge_unblock，解除设置">
	function ajax_challenge_unblock( $in )
	{
		$db = load( 'database' );
		$db->del( 'user_value', array( 'uid' => $in[ 'npc' ], 'key' => '挑战', 'target_uid' => $in[ 'user' ] ) );

		if( $in['id'] != 0 )
			$db->command( 'delete from message where id = ' . $in['id']);

		return array( 'code' => 1, 'memo' => '成功解除' );
	}
	// </editor-fold>

	function ajax_ignore($in)
	{
		$db = load( 'database' );
		$db->command( "update message set `read` = 1 where receiver = " . $in['receiver'] . ' and sender = ' . $in['sender'] );

		$this->ajax_challenge_unblock( array( 'id' => 0, 'user'=>$in['sender'], 'npc' => $in['receiver'] ) );
	}
	// 查询npc状态
	function ajax_is_online($in)
	{
		$result = array( 'code' => 0 );
		$db = load( 'database' );

		$in = $db->unique( "select user from online where user = " . $in['id'] );
		$result['code'] = isset( $in['user'] ) ? 1 : 0;

		return $result;
	}

	// <editor-fold defaultstate="collapsed" desc="挑战设置">
	function ajax_challenge_block($in)
	{
		$now = time();

		$user_value = load( 'biz.user_value' );

		$where = array( 'uid' => $in[ 'npc' ], 'target_uid' => $in[ 'user' ], 'key' => '挑战', '0' => 'expires>' . $now );

		$data = $user_value->get1( '*', $where );
		
		if( isset( $data['id'] ) )
		{
			$where = array( 'id' => $data[ 'id' ] );
			if( $data[ 'remark' ] == $in[ 'remark' ] )
			{
				$count = $data[ 'value' ] + 5;
				$where[ 'value' ] = $count;
			}
			else
			{
				$count = 5;
				$where[ 'value' ] = $count;
				$where[ 'remark' ] = $in[ 'remark' ];
			}

			$where[ 'expires' ] = $data[ 'expires' ] + 3600;
			$user_value->set( $where );
		}
		else
		{
			$count = 5;
			$where[ 'value' ] = $count;
			$where[ 'expires' ] = time() + 3600;
			$where[ 'remark' ] = $in[ 'remark' ];
			$user_value->add( $where );
		}

		$time = load( 'time' );
		$expires = $time->format( $where['expires'] );
		$result = array( 'code' => 1, 'memo' => "设置成功，以后{$count}次挑战将被{$in[ 'remark' ]}，直至{$expires}。你可在过程中解除。");

		return $result;
	}
	// </editor-fold>


	// 发起挑战
	// 参数：npc, uid, gold
	function ajax_challenge_request( $in )
	{
		extract( $in );

		$room = biz( 'room' );

		if( $room->is_playing( $npc ) )
		{
			return $room->out( -1, 'NPC在房间比赛或挑战比赛中' );
		}

		if( $room->is_playing( $uid ) )
		{
			return $room->out( -2, '对方在房间比赛或挑战比赛中' );
		}

		if( biz('props')->have( 1, $gold ) )
		{
			return $room->out( -3, '对方金币不足' );
		}

		$now = time();

		$challenge_send = biz( 'challenge_send' );
		$info = $challenge_send->get1( '*', array( 'uid' => $npc, 'uid2' => $uid ) );
		if( isset( $info[ 'id' ] ) )
		{
			$challenge_send->set( array( 'id' => $info[ 'id' ], 'challenge_time' => $now ) );
			return $room->out( -4, '已经存在未发出挑战请求，系统已经自动发送' );
		}

		biz( 'challenge_send' )->add( array( 'uid' => $npc, 'uid2' => $uid, 'props_id' => 1,
			'count1' => $in['gold'], 'count2' => $gold, 'challenge_time' => $now - 1, 'expires' => $now + 86400  ) );

		return $room->out( 1, '操作成功' );
	}

	function ajax_forbid_user($in)
	{
		$result = array( 'code' => 0, 'message' => '失败' );
		$forbid_user = load( 'biz.forbid_user' );
		$in['reason'] = '封停-禁言';
		$forbid_user->add( $in );
		$result['message'] = '成功';

		return $result;
	}

}

$action = new action();
$action->run();
?>