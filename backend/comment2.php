<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['name'] = isset( $this->input['name'] ) ? $this->input['name'] : '';
			$this->input['sender_npc'] = isset( $this->input['sender_npc'] ) ? $this->input['sender_npc'] : '';

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$comment = load( 'biz.comment' );
			$result = $comment->replys2( $this->input );

			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'comment2.php' );
		}

		function do_post()
		{
			$option = isset( $this->input['op'] ) ? $this->input['op'] : '';

			if($option == '')
			{

				if ( isset( $this->input['id'] ) && is_array( $this->input['id'] ) )
				{
					$comment = load( 'biz.comment' );
					
					foreach( $this->input['id'] as $id )
					{
						$comment->delete_reply( $id );
					}			
				}

				$this->prompt( '操作已成功', array( array( 'url' => 'comment2.php', 'name' => '回复管理', 'default' => true ) ) );

			}
			elseif( $option == 'reply' )
			{
				$database = load( 'database' );
				$result = array( 'status' => -1, 'message' => '参数错误' );

				if (  isset($this->input['id']) && is_numeric($this->input['id']) && isset( $this->input['name'] ) && isset( $this->input['content'] ) && $this->input['content'] != '' && isset( $this->input['time'] ) && $this->input['time'] != '' )
				{
					$user = 0;

					$temp = $database->unique( "select id from user where name = '" . $this->input['name'] . "'" );
					if ( isset( $temp['id'] ) ) $user = $temp['id'];

					$comment_info = $database->unique( 'select sender from comment where id = ' . $this->input['id'] );

					$sex = isset( $this->input['sex'] ) && in_array( $this->input['sex'],array('0,1','1,0','0','1') ) ? $this->input['sex'] : '';

					$data = array( array( 'complete' => 0, 'sender' => ( $user > 0 ? $user : '' ), 'content' => $this->input['content'], 'sex'=> $sex ) );
					$queue = array( 'type' => 2, 'create' => time(),'time' => strtotime( $this->input['time'] ), 'status' => 1, 'target' => $this->input['id'], 'data' => serialize( $data ), 'user' => $comment_info['sender'], 'creater'=>$this->account );

					$database->add( 'task_queue', $queue );
					$database->command( 'update comment set `check` = 1 where id = ' . $this->input['id'] );

					$result = array( 'status' => 0, 'message' => '成功添加' );
				}

				echo json_encode( $result );
			}
		}
	}

	$action = new action();
	$action->run();
?>