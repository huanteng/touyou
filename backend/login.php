<?php
	require '../library/controller.php';

	class action extends controller
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'login.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请填写用户名和密码' );

			if ( isset( $this->input['name'] ) && isset( $this->input['pass'] ) && $this->input['name'] != '' && $this->input['pass'] != '' )
			{
				$user = load( 'biz.administrator' );
				$info = $user->get_by_name( $this->input['name'] );

				if ( isset( $info['name'] ) )
				{
					if ( $info['status'] == 0 )
					{
						if ( $info['pass'] == $user->password( $this->input['pass'] ) )
						{
							$user->database->command( 'update administrator set last = ' . time() . ' where id = ' . $info['id'] );
							$user->database->add( 'backend_log', array( 'administrator' => $info['name'], 'time' => time(), 'content' => '登录' ) );

							$cookie = load( 'cookie' );
							$result = array( 'status' => 0, 'message' => '欢迎进入系统' );						
							$cookie->set( 'account', $info['id'], true );
						}
						else
						{
							$result = array( 'status' => -4, 'message' => '密码不正确' );
						}
					}
					else
					{
						$result = array( 'status' => -3, 'message' => '用户被暂停' );
					}
				}
				else
				{
					$result = array( 'status' => -2, 'message' => '用户不存在' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>