<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'password.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入旧密码' );

			if ( isset( $this->input['old'] ) && $this->input['old'] != '' )
			{
				if ( isset( $this->input['new'] ) && $this->input['new'] != '' )
				{
					$user = load( 'biz.administrator' );
					$info = $user->get_by_id( $this->account );

					if ( $info['pass'] == $user->password( $this->input['old'] ) )
					{
						$info['pass'] = $this->input['new'];

						$user->set( $info );
						$result = array( 'status' => 0, 'message' => '成功设置新密码' );
						$cookie = load( 'cookie' );
						$cookie->set( 'password', '' );
					}
					else
					{
						$result = array( 'status' => -3, 'message' => '旧密码不正确' );
					}
				}
				else
				{
					$result = array( 'status' => -2, 'message' => '请输入新密码' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>