<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$configure = load( 'biz.configure' );
				$info = $configure->get_by_id( $this->input['id'] );

				if ( isset( $info['id'] ) )
				{
					$template = load( 'template', array( 'dir' => 'template/' ) );
					$template->appoint( $info );
					echo $template->parse( 'set_configure.php' );
				}
			}
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入名称' );

			if ( isset( $this->input['name'] ) && $this->input['name'] != '' )
			{
				if ( isset( $this->input['data'] ) && $this->input['data'] != '' )
				{
					if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
					{
						$configure = load( 'biz.configure' );
						$old = $configure->database->unique( 'select data from configure where id = ' . $this->input['id'] );
						$this->input['prev_data'] = $old['data'];

						$configure->set( $this->input );
						$result = array( 'status' => 0, 'message' => '成功设置配置参数' );
					}
					else
					{
						$result = array( 'status' => -3, 'message' => '请传输id参数' );
					}
				}
				else
				{
					$result = array( 'status' => -2, 'message' => '请输入内容' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>