<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$database = load( 'database' );
				$database->command( 'delete from user_props where user = ' . $this->input['id'] . ' and props in ( 3, 4, 5 )' );
			}

			header( 'location: ' . $_SERVER['HTTP_REFERER'] );
		}
	}

	$action = new action();
	$action->run();
?>