<?php
require '../library/controller.php';

class action extends controller
{
	var $before = array( 'get' => array(), 'post' => array() );

	function microtime_float()
	{
	   list($usec, $sec) = explode(" ", microtime());
	   return ((float)$usec + (float)$sec);
	}

	function do_get()
	{
		if ( isset( $this->input['type'] ) && is_numeric( $this->input['type'] ) )
		{
			$start = $this->microtime_float();

			switch( $this->input['type'] )
			{
				case '1':
					$user = load( 'biz.user' );
					//$match = load( 'biz.match' );

					$user->kick_offline();
					//$match->send_challenge_announce();
					//$match->auto_play();

					break;
				
				case '3':
					biz( 'task_queue' )->execute('1,9,10,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1013,1014,1015,1017,1019,1019,1020,1022,2,3,4,5,6,7,8');

					break;
				

				case '7':
					$npc = biz( 'npc' );
					//$npc->change_challenge_performance();
					$npc->focus_real();
					//$npc->simulate_play();
					$npc->execute( '0,7' );

					biz( 'task_queue' )->execute('1011,1012,1016,1023');
					
					break;

				

				default :
					echo 'error';
			}

			$end = $this->microtime_float();
			$database = load( 'database' );
			$old = $database->unique( 'select current from service_run_time where id = ' . $this->input['type'] );
			$update = 'update service_run_time set current = ' . ( $end - $start ) . ', time = ' . time();
			if ( isset( $old['current'] ) ) $update .= ', prev = ' . $old['current'];
			$update .= ' where id = ' . $this->input['type'];
			$database->command( $update );

//			echo '<pre>';
//			print_r($database->sql);
			echo 'ok';
		}
	}
}

$action = new action();
$action->run();
?>