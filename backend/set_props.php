<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$props = load( 'biz.props' );
				$info = $props->get_by_id( $this->input['id'] );

				if ( isset( $info['id'] ) )
				{
					$template = load( 'template', array( 'dir' => 'template/' ) );
					$template->appoint( $info );
					echo $template->parse( 'set_props.php' );
				}
			}
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入名称' );

			if ( isset( $this->input['name'] ) && $this->input['name'] != '' )
			{
				if ( isset( $this->input['price'] ) && is_numeric( $this->input['price'] ) )
				{
					if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
					{
						$props = load( 'biz.props' );
						$props->set( $this->input );
						$result = array( 'status' => 0, 'message' => '成功设置配置道具' );
					}
					else
					{
						$result = array( 'status' => -4, 'message' => '请传输id参数' );
					}
				}
				else
				{
					$result = array( 'status' => -3, 'message' => '请输入价格' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>