<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$name = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$type = isset( $this->input['_type'] ) ? $this->input['_type'] : '';
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;

			$_start = isset( $this->input['_start'] ) ? $this->input['_start'] : '';
			$_end = isset( $this->input['_end'] ) ? $this->input['_end'] : '';

			$start = ( $_start != '' ) ? strtotime($_start) : '';
			$end = ( $_end != '' ) ? strtotime($_end.' 23:59:59') : $start+86399;

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$moving = load( 'biz.moving' );
			$result = $moving->lists( array('page'=>$page,'name'=>$name,'type'=>$type,'start'=>$start,'end'=>$end) );
			
			$template->assign( '_name', $name );
			$template->assign( '_type', $type );
			$template->assign( '_start', $_start );
			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->assign( 'type_dict', $moving->get_type_dict() );
			$template->appoint( $this->input );
			echo $template->parse( 'moving.php' );
		}
		
		function do_post()
		{			
			if ( isset( $this->input['id'] ) && is_array( $this->input['id'] ) )
			{
				$database = load( 'database' );
				$database->command( 'delete from `moving` where id in ( ' . join(',',$this->input['id']) . ' )' );
			}
			$this->prompt( '操作已成功', array( array( 'url' => 'moving.php', 'name' => '动态管理', 'default' => true ) ) );
		}
	}

	$action = new action();
	$action->run();
?>