<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['id'] = isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) ? $this->input['id'] : 0;
			$this->input['history'] = isset( $this->input['history'] ) ? $this->input['history'] : 0;
			$this->input['type'] = isset( $this->input['type'] ) ? $this->input['type'] : '';
			$this->input['user'] = isset( $this->input['user'] ) && is_numeric( $this->input['user'] ) ? $this->input['user'] : 0;
			$this->input['name'] = isset( $this->input['name'] ) ? $this->input['name'] : '';
			$this->input['_start'] = isset( $this->input['_start'] ) ? $this->input['_start'] : '';
			$this->input['start'] = strtotime($this->input['_start']);
			$this->input['end'] = $this->input['start'] + 86400;
			
			$template = load( 'template', array( 'dir' => 'template/' ) );
			$match = load( 'biz.match' );
			$result = $match->lists( $this->input );

			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'pair.php' );
		}
	}

	$action = new action();
	$action->run();
?>