<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'add_friend.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '必须填写关注人和粉丝' );
			
			if ( isset($this->input['user']) && $this->input['user'] != '' && isset( $this->input['friend'] ) && $this->input['friend'] != '' )
			{
				$user = load('biz.user');
				$database = load( 'database' );
				
				$user1 = $database->unique("select id from user where name='".$this->input['user']."'");
				$user2 = $database->unique("select id from user where name='".$this->input['friend']."'");
				
				if( $user1['id'] > 0 && $user2['id'] > 0 )
				{ 
					$firend = load('biz.friend');
					$firend->create( array('uid'=>$user1['id'],'friend'=>$user2['id']) );
				}
				
				$result = array( 'status' => 0, 'message' => '添加成功' );
			}
			
			echo json_encode($result);
		}
	}

	$action = new action();
	$action->run();
?>