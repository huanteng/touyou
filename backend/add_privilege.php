<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'add_privilege.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入名称' );

			if ( isset( $this->input['name'] ) && $this->input['name'] != '' )
			{
				$privilege = load( 'biz.privilege' );
				$privilege->database->add( 'privilege', $this->input );
				$result = array( 'status' => 0, 'message' => '成功添加权限' );
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>