<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$privilege = load( 'biz.privilege' );
				$privilege->del( $this->input['id'] );
			}

			$this->prompt( '操作已成功', array( array( 'url' => 'privilege.php', 'name' => '权限管理', 'default' => true ) ) );
		}
	}

	$action = new action();
	$action->run();
?>