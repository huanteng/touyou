<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$badword = load( 'biz.badword' );
			$result = $badword->lists( $this->input );

			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'badword.php' );
		}

		function do_post()
		{
			if ( isset( $this->input['ids'] ) && count( $this->input['ids'] ) )
			{
				$badword = load( 'biz.badword' );

				foreach( $this->input['ids'] as $id ) $badword->del( array( 'id' => $id ) );
			}

			$this->prompt( '操作已成功', array( array( 'url' => isset($_SERVER['http_referer'])?$_SERVER['http_referer']:'badword.php', 'name' => '过滤管理', 'default' => true ) ) );
		}
	}

	$action = new action();
	$action->run();
?>