<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$database = load( 'database' );
			$template = load( 'template', array( 'dir' => 'template/' ) );

			$this->input['_start'] = $this->input['time'];
			$this->input['_end'] = $this->input['_start'] + 1200;
			$rank_table = 'integral_rank';
			$history = isset($this->input['$history']) ? $this->input['$history'] : 0;
			
			$exisit = $database->unique( 'select id from ' . $rank_table . ' where time = ' . $this->input['time'] . ' limit 1' );
			if ( ! isset( $exisit['id'] ) ) $rank_table = 'integral_rank_history';

			$not_in = '0';
			$data = $database->select( 'select * from ' . $rank_table . ' where rank > 0 and time = ' . $this->input['time'] . ' order by rank asc limit 10' );

			foreach( $data as $key => $value )
			{
				$not_in .= ',' . $value['user'];
				$role = $database->unique( 'select npc, name from user where id = ' . $value['user'] );
				$data[$key]['npc'] = $role['npc'];
				$data[$key]['name'] = $role['name'];
			}

			$real_array = $database->select( 'select ' . $rank_table. '.*, user.name from ' . $rank_table . ', user where user.id = ' . $rank_table . '.user and ' . $rank_table . '.time = ' . $this->input['time'] . ' and user.id not in ( ' . $not_in  .') and user.npc = 0 order by ' . $rank_table . '.rank asc' );

			$template->assign( 'data', $data );
			$template->assign( 'real_array', $real_array );
			$template->appoint( $this->input );

			echo $template->parse( 'integral_rank.php' );
		}
	}

	$action = new action();
	$action->run();
?>