<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;

			//$where = $this->input['_name'] != '' ? " and name like '%" . $this->input['_name'] . "%'" : '';
			$where = '';

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$announce = load( 'biz.announce' );
			$result = $announce->lists( $page, $where );

			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->assign( 'mode_dict', $announce->get_mode_dict() );
			$template->assign( 'type_dict', $announce->get_type_dict() );
			$template->appoint( $this->input );
			echo $template->parse( 'announce.php' );
		}
	}

	$action = new action();
	$action->run();
?>