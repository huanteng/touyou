<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$database = load( 'database' );
				$base = $database->unique( 'select * from alert where id = ' . $this->input['id'] );

				if ( isset( $base['id'] ) )
				{
					$tool = load( 'tool' );
					$template = load( 'template', array( 'dir' => 'template/' ) );
					$ids = $base['data'] != '' ? unserialize( $base['data'] ) : array( 0 );

					if ( $base['id'] == 1000 )
					{
						$extend = $database->select( 'select id, sender, receiver, content, time from message where id in ( ' . implode( ',', $ids ) . ' ) order by id desc' );

						foreach( $extend as $key => $val )
						{
							$temp = $database->unique( 'select name from user where id = ' . $val['sender'] );
							$extend[$key]['sender_name'] = $temp['name'];

							$temp = $database->unique( 'select name from user where id = ' . $val['receiver'] );
							$extend[$key]['receiver_name'] = $temp['name'];

							$extend[$key]['time'] = $tool->format_time( $extend[$key]['time'] );
						}
					}
					else if ( $base['id'] == 1001 )
					{
						$extend = $database->select( 'select id, sender as npc_id, content from comment where id in ( ' . implode( ',', $ids ) . ' ) order by id desc' );

						foreach( $extend as $key => $val )
						{
							$temp = $database->unique( 'select name from user where id = ' . $val['npc_id'] );
							$extend[$key]['npc_name'] = $temp['name'];

							$temp = $database->unique( 'select time, sender from comment where parent = ' . $val['id'] . ' order by time desc limit 1' );
							$extend[$key]['time'] = $tool->format_time( $temp['time'] );
							$extend[$key]['sender'] = $temp['sender'];

							$temp = $database->unique( 'select name from user where id = ' . $extend[$key]['sender'] );
							$extend[$key]['sender_name'] = $temp['name'];
						}
					}
					else if ( $base['id'] == 1002 )
					{
						$comment = load( 'biz.comment' );
						$result = $comment->replys2( array( 'sub_parent' => implode( ',', $ids ), 'size' => count( $ids ), 'orderby' => ' order by id desc' ) );
						foreach($result['data'] as $value)
						{
							$value['sub_parent_user'] = isset($value['sub_parent_user']) ? $value['sub_parent_user'] : '';
							$value['sub_parent_name'] = isset($value['sub_parent_name']) ? $value['sub_parent_name'] : '';
							$extend[] = $value;
						}			
					}
					else if ( $base['id'] == 1009 )
					{
						$comment = load( 'biz.comment' );
						$result = $comment->lists2( array( 'id' => implode( ',', $ids ), 'size' => count( $ids ), 'orderby' => ' order by id desc' ) );
						$extend = $result['data'];
					}
					
					$template->assign( 'base', $base );
					$template->assign( 'extend', $extend );
					echo $template->parse( 'handle_alert.php' );
				}
			}
		}
	}

	$action = new action();
	$action->run();
?>