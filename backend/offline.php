<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$user = load( 'biz.user' );
				$user->_logout( $this->input['id'] );
			}

			header( 'location: ' . $_SERVER['HTTP_REFERER'] );
		}
	}

	$action = new action();
	$action->run();
?>