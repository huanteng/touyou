<?php
require 'backend.php';

class action extends backend
{
	function do_get()
	{
		if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
		{
			$db = load( 'database' );
			$vs = $db->unique( 'select user_1, user_2 from vs where id = ' . $this->input['id'] );

			if ( isset( $vs['user_1'] ) )
			{
				$temp_1 = $db->unique( 'select npc from user where id = ' . $vs['user_1'] );
				$temp_2 = $db->unique( 'select npc from user where id = ' . $vs['user_2'] );

				if ( $temp_1['npc'] == '1' && $temp_2['npc'] == '0' )
				{
					$db->command( 'update vs set must_win = 1 where id = ' . $this->input['id'] );
				}
				else if ( $temp_2['npc'] == '1' && $temp_1['npc'] == '0' )
				{
					$db->command( 'update vs set must_win = 2 where id = ' . $this->input['id'] );
				}

				// 20130131临时，仅保留一天
				$db->command( 'update vs set must_win = 1 where user_1 = 836 and id = ' . $this->input['id'] );
				$db->command( 'update vs set must_win = 2 where user_2 = 836 and id = ' . $this->input['id'] );


			}
		}

		header( 'location: ' . $_SERVER['HTTP_REFERER'] );
	}
}

$action = new action();
$action->run();
?>