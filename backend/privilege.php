<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;

			$where = $this->input['_name'] != '' ? " and name like '%" . $this->input['_name'] . "%'" : '';

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$privilege = load( 'biz.privilege' );
			$result = $privilege->lists( $page, $where );

			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'privilege.php' );
		}
	}

	$action = new action();
	$action->run();
?>