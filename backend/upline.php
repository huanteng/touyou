<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$user = load( 'biz.user' );
				$npc = $user->database->unique( 'select name from user where id = ' . $this->input['id'] );
				if ( isset( $npc['name'] ) ) $user->login( array( 'name' => $npc['name'], 'npc' => true, 'pass' => '123' ) );
			}

			header( 'location: ' . $_SERVER['HTTP_REFERER'] );
		}
	}

	$action = new action();
	$action->run();
?>