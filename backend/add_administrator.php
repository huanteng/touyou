<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$user = load( 'biz.administrator' );
			$template = load( 'template', array( 'dir' => 'template/' ) );
			$template->assign( 'status_dict', $user->get_status_dict() );

			echo $template->parse( 'add_administrator.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请输入用户名' );

			if ( isset( $this->input['name'] ) && $this->input['name'] != '' )
			{
				if ( isset( $this->input['pass'] ) && $this->input['pass'] != '' )
				{
					$user = load( 'biz.administrator' );

					$this->input['pass'] = $user->password( $this->input['pass'] );
					$user->database->add( 'administrator', $this->input );
					$result = array( 'status' => 0, 'message' => '成功添加新用戶' );
				}
				else
				{
					$result = array( 'status' => -2, 'message' => '请输入密码' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>