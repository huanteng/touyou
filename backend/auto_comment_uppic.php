<?php
//预发布说两句上传图片
require 'backend.php';

class action extends backend
{
	function do_post()
	{
		
		$name = 'comment_pic';

		if ( isset( $_FILES[$name] ) && $_FILES[$name]['error'] == 0 && isset( $this->input['user'] ) && is_numeric( $this->input['user'] ) )
		{
			$user = load( 'biz.user' );

			$dir = $user->logo_base_dir . $user->logo_dir( $this->input['user'] );
			if ( ! is_dir( $dir ) ) mkdir( $dir, 0755, true );
			$file = uniqid() . '.jpg';

			if ( move_uploaded_file( $_FILES[$name]['tmp_name'], $dir . $file ) )
			{
				$tool = load( 'tool' );
				$tool->resize_image( $dir . $file, 120, 120, 1, $dir . $file . '.s.jpg' );

				$path = $user->logo_dir( $this->input['user'] ) . $file;
				
				$database = load('database');
				$result = $database->add( 'comment_pic', array( 'comment' => 0, 'path' => $path ) );
			}
			else
			{
				$result = -2;
			}
		}

		echo json_encode( array('id'=>$result) );
	}
}

$action = new action();
$action->run();
?>