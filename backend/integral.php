<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$prize_pool = load( 'biz.prize_pool' );

			$template->appoint( $prize_pool->lists( $this->input ) );
			$template->appoint( $this->input );

			echo $template->parse( 'integral.php' );
		}
	}

	$action = new action();
	$action->run();
?>