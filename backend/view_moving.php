<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$type = isset( $this->input['type'] ) ? $this->input['type'] : 0;
			$id = isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) ? $this->input['id'] : 0;
			$user = isset( $this->input['user'] ) && is_numeric( $this->input['user'] ) ? $this->input['user'] : 0;

			//array( '1' => '上传照片', '2' => '发表说两句', '3' => '修改宣言', '4' => '比赛胜利', '5' => '比赛落败', '6' => '相片分享', '7' => '相片评分' );
			$file_dict = array( '1' => 'auto_vote_album', '2' => 'comment1', '3' => '', '4' => 'challenge', '5' => 'challenge', '6' => 'auto_vote_album', '7' => 'auto_vote_album' );
			
			if( $type > 0 && ($id > 0 || $user > 0) )
			{
				switch($type)
				{
					case 1:
					case 6:
					case 7:
						$url = $file_dict[ $type ].'.php?id='.$id;
						break;
					case 2:
					case 3:
					case 4:
					case 5:	
						$url = $file_dict[ $type ].'.php?uid='.$user;
						break;
				}
				
				header("location:".$url);
			}
		}
	}

	$action = new action();
	$action->run();
?>