<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			echo $template->parse( 'add_config.php' );
		}

		function do_post()
		{
			$result = array( 'code' => 0, 'message' => '错误' );

			if ( isset( $this->input['section'] ) && $this->input['section'] != '' )
			{
				$database = load( 'database' );
				
				$data = array
					(
					'user' => $this->input['user'],
					'time' => time(),
					'section' => $this->input['section'],
					'key' => $this->input['key'],
					'value' => $this->input['value']
					);
				$database->add( 'config', $data );
				
				$data = array
					(
					'user' => $this->input['user'],
					'mode' => 0,
					'type' => 13,
					'data' => '',
					'time' => time(),
					'expire' => time()+86400,
					'sender' => 850,
					'del' => 0
					);
				
				$database->add( 'announce', $data );
				
				$result = array( 'code' => 0, 'message' => '成功添加' );
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>