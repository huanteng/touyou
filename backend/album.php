<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$uid = isset( $this->input['uid'] ) ? $this->input['uid'] : 0;
			$name = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$sex = isset( $this->input['_sex'] ) && is_numeric($this->input['_sex']) ? $this->input['_sex'] : '';
			$npc = isset( $this->input['_npc'] ) && is_numeric($this->input['_npc']) ? $this->input['_npc'] : '';
			$start = isset( $this->input['_start'] ) ? $this->input['_start'] : '';
			$end = isset( $this->input['_end'] ) ? $this->input['_end'] : '';
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$album = load( 'biz.album' );
			$result = $album->lists( array('uid'=>$uid,'name'=>$name,'page'=>$page,'sex'=>$sex,'npc'=>$npc,'start_time'=>strtotime($start),'end_time'=>strtotime($end)) );

			$template->assign( '_name', $name );
			$template->assign( '_sex', $sex );
			$template->assign( '_npc', $npc );
			$template->assign( '_start', $start );
			$template->assign( '_end', $end );
			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'album.php' );
		}
		
		function do_post()
		{
			if ( isset( $this->input['id'] ) && is_array($this->input['id']) )
			{
				$album = biz( 'album' );

				foreach( $this->input['id'] as $id )
				{
					$album->remove( array( 'id'=>$id, 'uid' => 0, 'manager' => true ) );
				}
			}
			$this->prompt( '操作已成功', array( array( 'url' => $_SERVER['HTTP_REFERER'], 'name' => '图片管理', 'default' => true ) ) );
		}
	}

	$action = new action();
	$action->run();
?>