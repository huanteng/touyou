<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			$template->assign( 'user', $this->account );
			echo $template->parse( 'left.php' );
		}
	}

	$action = new action();
	$action->run();
?>