<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_array( $this->input['id'] ) )
			{
				$database = load( 'database' );
				$database->command( 'delete from message where id in ( ' . join(',',$this->input['id']) . ' )' );
			}

			$this->prompt( '操作已成功', array( array( 'url' => 'message.php', 'name' => '消息管理', 'default' => true ) ) );
		}
	}

	$action = new action();
	$action->run();
?>