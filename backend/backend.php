<?php
	require '../library/controller.php';

	class backend extends controller
	{
		var $debug = true;
		var $before = array( 'get' => array( 'no_html_cache' => true, 'check_account' => false ), 'post' => array( 'check_account' => false ) );
		var $account = '';

		function check_account()
		{
			$result = true;
			$cookie = load( 'cookie' );
			$this->account = $cookie->get( 'account', true );
			
			// �¾ɰ���ݣ���ʱ����д
			//if ( ! is_numeric( $this->account ) )
			if ( $this->account == '' )
			{
				$result = false;

				$template = load( 'template', array( 'dir' => 'template/' ) );
				echo $template->parse( 'login.php' );
			}

			return $result;
		}

		function pagebar( $data, $style = 1 )
		{
			$template = load( 'template', array( 'style' => $style, 'dir' => 'template/' ) );
			$template->assign( 'bar', $data );
			return $template->parse( 'pagebar.php' );
		}

		function no_html_cache()
		{
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			header('Cache-Control: no-store, no-cache, must-revalidate');
			header('Cache-Control: post-check=0, pre-check=0', false );
			header('Pragma: no-cache');

			return true;
		}

		function prompt( $data, $url, $file = 'prompt.php', $time = 3 )
		{
			$default = '';

			foreach( $url as $index => $value )
			{
				if ( isset( $value['default'] ) )
				{
					$default = $index;
					break;
				}
			}

			$template = load( 'template', array( 'style' => 1, 'dir' => 'template/' ) );
			$template->assign( 'data', $data );
			$template->assign( 'time', $time );
			$template->assign( 'url', $url );
			$template->assign( 'default', $default );
			echo $template->parse( $file );
		}
	}
?>