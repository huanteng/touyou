<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$uid = isset( $this->input['uid'] ) && is_numeric($this->input['uid']) ? $this->input['uid'] : '';
			$name = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$notification = load( 'biz.notification' );
			$result = $notification->lists( array('backend'=>1,'page'=>$page, 'name'=>$name, 'uid'=>$uid) );

			$template->assign( '_name', $name );
			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->assign( 'type_dict', $notification->get_type_dict() );
			$template->appoint( $this->input );
			echo $template->parse( 'notification.php' );
		}

		function do_post()
		{
			if ( isset( $this->input['id'] ) && is_array( $this->input['id'] ) )
			{
				$database = load( 'database' );
				$database->command( 'delete from `notification` where id in ( ' . join(',',$this->input['id']) . ' )' );
			}

			$this->prompt( '操作已成功', array( array( 'url' => 'notification.php', 'name' => '消息管理', 'default' => true ) ) );
		}
	}

	$action = new action();
	$action->run();
?>