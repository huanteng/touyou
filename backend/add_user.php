<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$template = load( 'template', array( 'dir' => 'template/' ) );
			$template->assign( 'year', date('Y') );
			$template->assign( 'moon', date('m') );
			$template->assign( 'day', date('d') );
			echo $template->parse( 'add_user.php' );
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '至少填写名字和密码' );

			if ( isset( $this->input['name'] ) && $this->input['name'] != '' && isset($this->input['pass']) && $this->input['pass'] != '' )
			{
				$user = load('biz.user');
				$database = load( 'database' );
				$id = $user->register( array( 'name'=>$this->input['name'], 'pass'=>$this->input['pass'] ) );

				if( $id > 0 )
				{
					unset( $this->input['pass'] );
					$this->input['birthday'] = strtotime( $this->input['birthday'] );

					if ( $this->input['npc'] == '1' )
					{
						$this->input['version'] = 2;
						$this->input['creater'] = $this->account;
					}
					$database->set( 'user', $this->input, array( 'id' => $id ) );

					$result = array( 'status' => 0, 'message' => '添加成功' );
				}
				elseif( $id == -2 )
				{
					$result = array( 'status' => 0, 'message' => '用户名重复' );
				}
				else
				{
					$result = array( 'status' => 0, 'message' => '添加失败' );
				}
			}

			echo json_encode($result);
		}
	}

	$action = new action();
	$action->run();
?>