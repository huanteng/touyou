<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$database = load( 'database' );
			$navigation = load( 'navigation' );
			$template = load( 'template', array( 'dir' => 'template/' ) );

			$task_queue = $database->select( 'select * from task_queue where type = 1 and status = 1 order by id asc' );
			$result = $database->unique("select count(*) as total from task_queue where type = 1");
			$result['bar'] = $navigation->compute( $result['total'], 20 );

			foreach( $task_queue as $index => $value )
			{
				$value['data'] = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $value['data'] );
				$data = unserialize($value['data']);
				$task_queue[$index]['name'] = isset($data['name']) && $data['name']!='' ? $data['name']: '随机未定';
				$task_queue[$index]['content'] = $data['content'];
			}

			$template->assign( 'data', $task_queue );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'auto_publish_comment1.php' );
		}

		function do_post()
		{
			$database = load( 'database' );

			$result = array( 'status' => -1, 'message' => '参数错误' );

			if( isset( $this->input['op'] ) && $this->input['op'] == 'get_id_from_name' )
			{
				if( $this->input['name'] != '' )
				{
					$info = $database->unique( "select * from user where name = '" . $this->input['name'] . "' and npc = 1" );
					if( $this->input['name'] == '骰妖子' ) //例外：骰妖子不是NPC
					{
						$info['id'] = 850;
						$info['name'] = '骰妖子';
					}
				}
				else
				{
					$sex = ( $this->input['sex'] != '' ) ? $this->input['sex'] : '0,1';
					$info = $database->unique( "select * from user where npc = 1 and sex in (".$sex.") order by rand() limit 1" );
				}
				$return = isset( $info['id'] ) ? array('id'=>$info['id'], 'name' => $info[ 'name' ]) : '';
				echo json_encode( $return );
			}
			elseif ( isset( $this->input['op'] ) && $this->input['op'] == 'publish' )
			{

				if (  isset( $this->input['name'] ) && isset( $this->input['content'] ) && $this->input['content'] != '' && isset( $this->input['sex'] ) && isset( $this->input['time'] ) && $this->input['time'] != '' )
				{
					$user = -1;
					if( $this->input['user'] != '' && is_numeric($this->input['user']) )
					{
						$user = $this->input['user'];
					}
					elseif( $this->input['name'] != '' )
					{
						$temp = $database->unique( "select id from user where name = '" . $this->input['name'] . "' and npc = 1" );
						if( isset($temp['id']) ) $user = $temp['id'];
						else $result = array( 'status' => -1, 'message' => '不支持非 NPC 用户或者用户不存在' );
						
						if( $this->input['name'] == '骰妖子' ) $user = 850;//例外：骰妖子不是NPC
					}
					else $user = 0;

					if( $user > -1 )
					{
						$comment_pic_id = isset($this->input['comment_pic_id']) ? $this->input['comment_pic_id'] : 0;

						$data = array( 'name' => $this->input['name'], 'content' => $this->input['content'], 'sex'=>$this->input['sex'], 'comment_pic_id' => $comment_pic_id );

						$queue = array( 'type' => 1, 'create' => time(),'time' => strtotime($this->input['time']), 'status' => 1, 'target' => '', 'data' => serialize( $data ), 'user'=>$user, 'creater'=>$this->account  );
						$return_id = $database->add( 'task_queue', $queue );
						$result = array( 'status' => 0, 'message' => '成功添加', 'id' => $return_id );
					}
				}

				echo json_encode( $result );

			}
			elseif ( $this->input['op'] == 'delete' )
			{
				if( isset( $this->input['id'] ) && $this->input['id'] != '' && is_array( $this->input['id'] ) && count( $this->input['id'] ) )
				{
					$database = load( 'database' );
					$database->command( "delete from task_queue where id in (".join(',',$this->input['id']).")" );
				}
				$this->prompt( '操作已成功', array( array( 'url' => 'auto_publish_comment1.php', 'name' => '说两句自动发布管理', 'default' => true ) ) );
			}
		}
	}

	$action = new action();
	$action->run();
?>