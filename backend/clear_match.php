<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$database = load( 'database' );
				$info = $database->unique( 'select id from vs where id = ' . $this->input['id'] );

				if ( isset( $info['id'] ) )
				{
					$database->command( 'delete from vs where id = ' . $this->input['id'] );
					$database->command( 'delete from shout_log where vs = ' . $this->input['id'] );
					$database->command( 'delete from challenge_match where vs = ' . $this->input['id'] );
				}
			}

			header( 'location: ' . $_SERVER['HTTP_REFERER'] );
		}
	}

	$action = new action();
	$action->run();
?>