<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['uid'] = isset( $this->input['uid'] ) && is_numeric( $this->input['uid'] ) ? $this->input['uid'] : 0;
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$this->input['_from'] = isset( $this->input['_from'] ) ? $this->input['_from'] : '';
			$this->input['_to'] = isset( $this->input['_to'] ) ? $this->input['_to'] : '';
			$this->input['_backend'] = 1;

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$message = load( 'biz.message' );
			$result = $message->lists( $this->input );

			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'message.php' );
		}

		function do_post()
		{
			$result = array( 'code' => -1 );

			if ( isset( $this->input['id'] ) && isset( $this->input['user'] ) && isset( $this->input['from'] ) && isset( $this->input['content'] ) && isset( $this->input['time'] ) )
			{
				$database = load( 'database' );

				$info = $database->unique( 'select npc from user where id = ' . $this->input['from'] );

//				if ( isset( $info['npc'] ) && $info['npc'] == 1 )
//				{
					$id = $database->add( 'task_queue', array( 'user' => $this->input['user'], 'create' => time(), 'creater'=>$this->account,
						'type' => 4, 'time' => strtotime( $this->input['time'] ), 'status' => 1, 'target' => $this->input['user'],
						'data' => serialize( array( 'uid' => $this->input['from'], 'user' => $this->input['user'], 'content' => $this->input['content'] ) ) ) );

					if ( $id > 0 )
					{
						$database->command( "update message set `read` = 1 where receiver = " . $this->input['from'] . ' and sender = ' . $this->input['user'] );
						$result['code'] = 1;
					}
			//	}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>