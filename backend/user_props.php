<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$uid = isset( $this->input['uid'] ) && is_numeric( $this->input['uid'] ) ? $this->input['uid'] : 0;
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$this->input['_time'] = isset( $this->input['_time'] ) ? $this->input['_time'] : '';
			$this->input['_type'] = isset( $this->input['_type'] ) ? $this->input['_type'] : '';
			$this->input['_history'] = isset( $this->input['_history'] ) ? $this->input['_history'] : '0';
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;

			$where = '';

			if ( $this->input['_name'] != '' )
			{
				$database = load( 'database' );
				$user = $database->unique( "select id from user where name = '" . $this->input['_name'] . "'" );
				if ( ! isset( $user['id'] ) ) $user['id'] = 0;
				$where .= ' and user = ' . $user['id'];
			}

			if ( $this->input['_time'] != '' )
			{
				$where .= ' and time between ' . strtotime( $this->input['_time'] ) . ' and ' . ( strtotime( $this->input['_time'] ) + 86399 );
			}

			if( $this->input['_type'] != '' )
			{
				$where .= " and `type` in (".$this->input['_type'].")";
			}

			if( $uid > 0 )
			{
				$where = " and user = ".$uid;
			}

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$props = load( 'biz.props' );
			$result = $props->list_user_log( $page, $where, 24, $this->input['_history'] );

			foreach( $result['data'] as $key => $value )
			{
				$temp = $props->database->unique( 'select npc from user where id = ' . $value['user'] );
				$result['data'][$key]['npc'] = $temp['npc'];
			}

			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->assign( 'type_dict', $props->get_type_dict() );
			$template->appoint( $this->input );
			echo $template->parse( 'user_props.php' );
		}
	}

	$action = new action();
	$action->run();
?>