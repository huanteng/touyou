<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$service_name = array();
			$service_name[1] = '清除掉线';
			$service_name[3] = '队列任务';
			$service_name[7] = '排行变化/关注真人';

			$database = load( 'database' );
			$template = load( 'template', array( 'dir' => 'template/' ) );

			$template->assign( 'service_name', $service_name );
			$template->assign( 'service_array', $database->select( 'select * from service_run_time order by current desc' ) );

			echo $template->parse( 'service_performance.php' );
		}
	}

	$action = new action();
	$action->run();
?>