<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$this->input['_sex'] = isset( $this->input['_sex'] ) ? $this->input['_sex'] : '';
			$this->input['_npc'] = isset( $this->input['_npc'] ) ? $this->input['_npc'] : '';
			$this->input['channel'] = isset( $this->input['channel'] ) ? $this->input['channel'] : '';
			$page = isset( $this->input['page'] ) && is_numeric( $this->input['page'] ) ? $this->input['page'] : 1;

			$where = $this->input['_name'] != '' ? " and name like '%" . $this->input['_name'] . "%'" : '';

			if( $this->input['_sex'] != '' ) $where .= ' and sex = '.$this->input['_sex'];
			if( $this->input['_npc'] != '' ) $where .= ' and npc = '.$this->input['_npc'];
			if( $this->input['channel'] != '' ) $where .= " and channel = '".$this->input['channel']."'";

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$user = load( 'biz.user' );
			$result = $user->lists( $page, $where );

			$template->assign( 'data', $result['data'] );
			$template->assign( 'bar', $result['bar'] );
			$template->appoint( $this->input );
			echo $template->parse( 'user.php' );
		}
	}

	$action = new action();
	$action->run();
?>