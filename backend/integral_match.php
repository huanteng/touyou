<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$this->input['_name'] = isset( $this->input['_name'] ) ? $this->input['_name'] : '';
			$this->input['history'] = isset( $this->input['history'] ) ? $this->input['history'] : '1';
			$this->input['_start'] = isset( $this->input['_start'] ) && $this->input['_start'] != '' ? $this->input['_start'] : '';
			$this->input['_end'] = isset( $this->input['_end'] ) && $this->input['_end'] != '' ? $this->input['_end'] : '';

			$template = load( 'template', array( 'dir' => 'template/' ) );
			$integral = load( 'biz.integral' );

			$template->appoint( $integral->lists( $this->input ) );
			$template->appoint( $this->input );

			echo $template->parse( 'integral_match.php' );
		}
	}

	$action = new action();
	$action->run();
?>