<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$user = load( 'biz.administrator' );
				$info = $user->get_by_id( $this->input['id'] );

				if ( isset( $info['id'] ) )
				{
					$template = load( 'template', array( 'dir' => 'template/' ) );
					$template->appoint( $info );
					$template->assign( 'status_dict', $user->get_status_dict() );
					echo $template->parse( 'set_administrator.php' );
				}
			}
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请选择状态' );

			if ( isset( $this->input['status'] ) && is_numeric( $this->input['status'] ) )
			{
				if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
				{
					$user = load( 'biz.administrator' );
					$user->set( $this->input );
					$result = array( 'status' => 0, 'message' => '成功设置用户' );
				}
				else
				{
					$result = array( 'status' => -2, 'message' => '请传输id参数' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>