<?php
	
	header('Location: ../_back/user_detail.php?uid='.$_GET['id']);
	
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$database = load( 'database' );
				$info = $database->unique( 'select * from user where id = ' . $this->input['id'] );

				if ( isset( $info['id'] ) )
				{

					$user = load( 'biz.user' );
					$info['logo'] = $user->logo( $this->input['id'] );

					$template = load( 'template', array( 'dir' => 'template/' ) );
					$template->assign( 'year', date('Y',$info['birthday']) );
					$template->assign( 'moon', date('m',$info['birthday']) );
					$template->assign( 'day', date('d',$info['birthday']) );
					$template->appoint( $info );
					echo $template->parse( 'set_user.php' );
				}
			}
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '请传输id参数' );

			if ( isset( $this->input['id'] ) && is_numeric( $this->input['id'] ) )
			{
				$user = load( 'biz.user' );
				$database = load( 'database' );

				if ( isset( $this->input['birthday'] ) && strtotime( $this->input['birthday'] ) ) $this->input['birthday'] = strtotime( $this->input['birthday'] );
				if ( isset( $this->input['pass'] ) && $this->input['pass'] != '' ) $this->input['pass'] = $user->code( $this->input['pass'] );

				if ( $this->input['npc'] == '1' ) $this->input['version'] = 2;
				$database->set( 'user', $this->input, array( 'id' => $this->input['id'] ) );
				$result = array( 'status' => 0, 'message' => '修改成功' );
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>