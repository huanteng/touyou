<?php
	require 'backend.php';

	class action extends backend
	{
		function do_get()
		{
			$database = load( 'database' );
			$user = load('biz.user');
			$info = $database->unique( 'select album.*, user.name from album, user where album.user = user.id and album.id = ' . $this->input['id'] );
			$info['size'] = 0;
			$info['size'] = round( filesize( $user->logo_base_dir . $info['path'] ) / 1024, 2 );
			
			if ( isset( $info['id'] ) )
			{
				$album = load( 'biz.album' );
				$template = load( 'template', array( 'dir' => 'template/' ) );
				$result = $album->list_vote_log( $this->input );
				$task_queue = $database->select( 'select * from task_queue where type = 3 and target = ' . $info['id'] . ' order by id asc' );

				$template->assign( 'info', $info );
				$template->assign( 'vote_history', $result['data'] );
				$template->assign( 'bar', $result['bar'] );
				$template->assign( 'task_queue', $task_queue );
				$template->assign( 'album_object', $album );

				echo $template->parse( 'auto_vote_album.php' );
			}
			else
			{
				echo 'id error';
			}
		}

		function do_post()
		{
			$result = array( 'status' => -1, 'message' => '参数错误' );

			if ( isset( $this->input['target'] ) && isset( $this->input['start_time'] ) && isset( $this->input['score_1'] ) && isset( $this->input['score_2'] ) && isset( $this->input['interval_1'] ) && isset( $this->input['interval_2'] ) && isset( $this->input['times'] ) && isset( $this->input['sex'] ) )
			{
				$database = load( 'database' );
				$info = $database->unique( 'select user from album where id = ' . $this->input['target'] );

				if ( isset( $info['user'] ) )
				{
					$sex = in_array( $this->input['sex'], array('0,1','0','1','1,0') ) ? $this->input['sex'] : '0,1';
					
					$data = array( 'score_1' => $this->input['score_1'], 'score_2' => $this->input['score_2'], 'interval_1' => $this->input['interval_1'], 'interval_2' => $this->input['interval_2'], 'times' => $this->input['times'], 'sex'=>$sex, 'done' => array() );

					$queue = array( 'user' => $info['user'], 'create' => time(), 'type' => 3, 'time' => strtotime( $this->input['start_time'] ), 'status' => 1, 'target' => $this->input['target'], 'data' => serialize( $data ), 'creater'=>$this->account );

					$database->add( 'task_queue', $queue );
					$result = array( 'status' => 0, 'message' => '成功添加' );
				}
			}

			echo json_encode( $result );
		}
	}

	$action = new action();
	$action->run();
?>