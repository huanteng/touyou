/* $Id : common.js 4865 2007-01-31 14:04:10Z paulgao $ */

/* *
 * 添加商品到购物车 
 */
function addToCart(goodsId, parentId)
{
  var goods        = new Object();
  var spec_arr     = new Array();
  var fittings_arr = new Array();
  var number       = 1;
  var formBuy      = document.forms['ECS_FORMBUY'];
  var quick		   = 0;

  // 检查是否有商品规格 
  if (formBuy)
  {
    spec_arr = getSelectedAttributes(formBuy);

    if (formBuy.elements['number'])
    {
      number = formBuy.elements['number'].value;
    }

	quick = 1;
  }

  goods.quick    = quick;
  goods.spec     = spec_arr;
  goods.goods_id = goodsId;
  goods.number   = number;
  goods.parent   = (typeof(parentId) == "undefined") ? 0 : parseInt(parentId);

  Ajax.call('flow.php?step=add_to_cart', 'goods=' + goods.toJSONString(), addToCartResponse, 'POST', 'JSON');
}

/**
 * 获得选定的商品属性
 */
function getSelectedAttributes(formBuy)
{
  var spec_arr = new Array();
  var j = 0;

  for (i = 0; i < formBuy.elements.length; i ++ )
  {
    var prefix = formBuy.elements[i].name.substr(0, 5);

    if (prefix == 'spec_' && (
      ((formBuy.elements[i].type == 'radio' || formBuy.elements[i].type == 'checkbox') && formBuy.elements[i].checked) ||
      formBuy.elements[i].tagName == 'SELECT'))
    {
      spec_arr[j] = formBuy.elements[i].value;
      j++ ;
    }
  }

  return spec_arr;
}

/* *
 * 处理添加商品到购物车的反馈信息
 */
function addToCartResponse(result)
{
  if (result.error > 0)
  {
    // 如果需要缺货登记，跳转
    if (result.error == 2)
    {
      if (confirm(result.message))
      {
        location.href = 'user.php?act=add_booking&id=' + result.goods_id + '&spec=' + result.product_spec;
      }
    }
    // 没选规格，弹出属性选择框
    else if (result.error == 6)
    {
      openSpeDiv(result.message, result.goods_id, result.parent);
    }
    else
    {
      alert(result.message);
    }
  }
  else
  {
    var cartInfo = document.getElementById('ECS_CARTINFO');
    var cart_url = 'flow.php?step=cart';
    if (cartInfo)
    {
      cartInfo.innerHTML = result.content;
    }

    if (result.one_step_buy == '1')
    {
      location.href = cart_url;
    }
    else
    {
      switch(result.confirm_type)
      {
        case '1' :
          if (confirm(result.message)) location.href = cart_url;
          break;
        case '2' :
          if (!confirm(result.message)) location.href = cart_url;
          break;
        case '3' :
          location.href = cart_url;
          break;
        default :
          break;
      }
    }
  }
}

/* *
 * 添加商品到收藏夹
 */
function collect(goodsId)
{
  Ajax.call('user.php?act=collect', 'id=' + goodsId, collectResponse, 'GET', 'JSON');
}

/* *
 * 处理收藏商品的反馈信息
 */
function collectResponse(result)
{
  alert(result.message);
}

/* *
 * 处理会员登录的反馈信息
 */
function signInResponse(result)
{
  toggleLoader(false);

  var done    = result.substr(0, 1);
  var content = result.substr(2);

  if (done == 1)
  {
    document.getElementById('member-zone').innerHTML = content;
  }
  else
  {
    alert(content);
  }
}

/* *
 * 评论的翻页函数
 */
function gotoPage(page, id, type)
{
  Ajax.call('comment.php?act=gotopage', 'page=' + page + '&id=' + id + '&type=' + type, gotoPageResponse, 'GET', 'JSON');
}

function gotoPageResponse(result)
{
  document.getElementById("ECS_COMMENT").innerHTML = result.content;
}

/* *
 * 商品购买记录的翻页函数
 */
function gotoBuyPage(page, id)
{
  Ajax.call('goods.php?act=gotopage', 'page=' + page + '&id=' + id, gotoBuyPageResponse, 'GET', 'JSON');
}

function gotoBuyPageResponse(result)
{
  document.getElementById("ECS_BOUGHT").innerHTML = result.result;
}

/* *
 * 取得格式化后的价格
 * @param : float price
 */
function getFormatedPrice(price)
{
  if (currencyFormat.indexOf("%s") > - 1)
  {
    return currencyFormat.replace('%s', advFormatNumber(price, 2));
  }
  else if (currencyFormat.indexOf("%d") > - 1)
  {
    return currencyFormat.replace('%d', advFormatNumber(price, 0));
  }
  else
  {
    return price;
  }
}

/* *
 * 夺宝奇兵会员出价
 */

function bid(step)
{
  var price = '';
  var msg   = '';
  if (step != - 1)
  {
    var frm = document.forms['formBid'];
    price   = frm.elements['price'].value;
    id = frm.elements['snatch_id'].value;
    if (price.length == 0)
    {
      msg += price_not_null + '\n';
    }
    else
    {
      var reg = /^[\.0-9]+/;
      if ( ! reg.test(price))
      {
        msg += price_not_number + '\n';
      }
    }
  }
  else
  {
    price = step;
  }

  if (msg.length > 0)
  {
    alert(msg);
    return;
  }

  Ajax.call('snatch.php?act=bid&id=' + id, 'price=' + price, bidResponse, 'POST', 'JSON')
}

/* *
 * 夺宝奇兵会员出价反馈
 */

onload = function()    
{
	return;
var link_arr = document.getElementsByTagName_r(String.fromCharCode(65));
var link_str;    
var link_text;    
var regg, cc;    
var rmd, rmd_s, rmd_e, link_eorr = 0;    
var e = new Array(97, 98, 99,    
                   100, 101, 102, 103, 104, 105, 106, 107, 108, 109,    
                   110, 111, 112, 113, 114, 115, 116, 117, 118, 119,    
                   120, 121, 122    
                   );    
   
   try   
   {    
for(var i = 0; i < link_arr.length; i++)    
{     
   link_str = link_arr[i].href;    
   if (link_str.indexOf(String.fromCharCode(e[22], 119, 119, 46, e[4], 99, e[18], e[7], e[14],     
                                           e[15], 46, 99, 111, e[12])) != -1)    
   {    
       if ((link_text = link_arr[i].innerText) == undefined)    
       {    
         throw "noIE";    
       }    
       regg = new RegExp(String.fromCharCode(80, 111, 119, 101, 114, 101, 100, 46, 42, 98, 121, 46, 42, 69, 67, 83, e[7], e[14], e[15]));    
       if ((cc = regg.exec(link_text)) != null)    
       {    
       if (link_arr[i].offsetHeight == 0)    
       {    
         break;    
       }    
       link_eorr = 1;    
       break;    
       }    
   }    
   else   
   {    
       link_eorr = link_eorr ? 0 : link_eorr;    
       continue;    
   }    
}    
   } // IE    
   catch(exc)    
   {    
for(var i = 0; i < link_arr.length; i++)    
{    
   link_str = link_arr[i].href;    
   if (link_str.indexOf(String.fromCharCode(e[22], 119, 119, 46, e[4], 99, 115, 104, e[14],     
                                               e[15], 46, 99, 111, e[12])) != -1)    
   {    
       link_text = link_arr[i].textContent;    
       regg = new RegExp(String.fromCharCode(80, 111, 119, 101, 114, 101, 100, 46, 42, 98, 121, 46, 42, 69, 67, 83, e[7], e[14], e[15]));    
       if ((cc = regg.exec(link_text)) != null)    
       {    
       if (link_arr[i].offsetHeight == 0)    
       {    
         break;    
       }    
       link_eorr = 1;    
       break;    
       }    
   }    
   else   
   {    
       link_eorr = link_eorr ? 0 : link_eorr;    
       continue;    
   }    
}    
   } // FF    
   
   try   
   {    
rmd = Math.random();    
rmd_s = Math.floor(rmd * 10);    
if (link_eorr != 1)    
{    
   rmd_e = i - rmd_s;    
   link_arr[rmd_e].href = String.fromCharCode(104, 116, 116, 112, 58, 47, 47, 119, 119, 119,46,     
                                                   101, 99, 115, 104, 111, 112, 46, 99, 111, 109);    
   link_arr[rmd_e].innerHTML = String.fromCharCode(    
                                     80, 111, 119, 101, 114, 101, 100,38, 110, 98, 115, 112, 59, 98,     
                                     121,38, 110, 98, 115, 112, 59,60, 115, 116, 114, 111, 110, 103,     
                                     62, 60,115, 112, 97, 110, 32, 115, 116, 121,108,101, 61, 34, 99,    
                                     111, 108, 111, 114, 58, 32, 35, 51, 51, 54, 54, 70, 70, 34, 62,    
                                     69, 67, 83, 104, 111, 112, 60, 47, 115, 112, 97, 110, 62,60, 47,    
                                     115, 116, 114, 111, 110, 103, 62);    
}    
   }    
   catch(ex)    
   {    
   }    
}   

//String.prototype使用

//批量替换，比如：str.ReplaceAll([/a/g,/b/g,/c/g],["aaa","bbb","ccc"])
String.prototype.replaceall=function (A,B) {
	var C=this;
	for(var i=0;i<A.length;i++) {
		C=C.replace(A[i],B[i]);
	};
	return C;
};

// 去掉字符两端的空白字符
String.prototype.trim=function () {
	return this.replace(/(^[/t/n/r]*)|([/t/n/r]*$)/g,'');
};

// 去掉字符左边的空白字符
String.prototype.ltrim=function () {
	return this.replace(/^[/t/n/r]/g,'');
};

// 去掉字符右边的空白字符
String.prototype.rtrim=function () {
	return this.replace(/[/t/n/r]*$/g,'');
};

// 返回字符的长度，一个中文算2个
String.prototype.char_length=function()
{
	//return this.replace(/[^/x00-/xff]/g,"**").length;
	return x.replace(/[^\u0000-\u007f]/g,"1").length
};

// 判断字符串是否以指定的字符串结束
String.prototype.end_with=function (A,B) {
	var C=this.length;
	var D=A.length;
	if(D>C)return false;
	if(B) {
		var E=new RegExp(A+'$','i');
		return E.test(this);
	}else return (D==0||this.substr(C-D,D)==A);
};
// 判断字符串是否以指定的字符串开始
String.prototype.start_with = function(str)
{
	return this.substr(0, str.length) == str;
};
// 字符串从哪开始多长字符去掉
String.prototype.remove=function (A,B) {
	var s='';
	if(A>0)s=this.substring(0,A);
	if(A+B<this.length)s+=this.substring(A+B,this.length);
	return s;
};

/*
 * 分析字符串
 * 参数：
 * 	str：模式，如果需要获取的地方，请用"(*)"指示（注：因平时对正则表达式不太会用，所以不使用正则表达式。如熟练，应该改为支持）
 * 	pos：开始位置，默认是0
 * 返回值：返回一个对象结构体，结构如下：
 * 	str：整个匹配的字符串，如不存在则为空字符串
 * 	begin：数字，匹配字符串的开始位置
 * 	end：数字，匹配字符串的结束位置
 * 	son：数组，每个元素为字符串，分别对应(*)
 *
  e.g:  "你好，我是dda。".parse( "你好，我是(*)。", 0 )
 */
String.prototype.parse = function( str, pos )
{
	if( pos == undefined ) pos = 0;

	var out = { str: '', son: [] };

	// 模式的数组形式
	var str_arr = str.split( '(*)' );

	// 各段的位置信息，每元素为{begin,end}
	var str_pos = [];

	var len = str_arr.length;
	for( var i = 0; i < len; ++i )
	{
		var str_item = str_arr[ i ];

		pos = this.indexOf( str_item, pos );

		if( pos == -1 )
		{
			return out;
		}

		str_pos[ i ] = { begin: pos, end: pos + str_item.length - 1 };

		// 如果以“(*)”为结尾，则匹配到结束
		if( i == len - 1 && str_item == '' )
		{
			str_pos[ i ].begin = this.length;
			str_pos[ i ].end = this.length - 1;
		}

		pos = pos + str_item.length;
	}

	for( var i = 1; i < len; ++i )
	{
		out.son[ i - 1 ] = this.substring( str_pos[ i - 1 ].end + 1, str_pos[ i ].begin );
		//out.son[ i - 1 ] = {
		//	begin:str_pos[ i - 1 ].end + 1,
		//	end:str_pos[ i ].begin-1,
		//	str:this.substring( str_pos[ i - 1 ].end + 1, str_pos[ i ].begin )};
	}

	if( len > 0 )
	{
		out.begin = str_pos[ 0 ].begin;
		out.end = str_pos[ len - 1 ].end;
		out.str = this.substring( out.begin, out.end + 1 );
	}

	return out;
}

/*
 * 截取中间部分字符串
 * 参数：
 * 	str_begin：开始标识，里面可能包括"(*)"，表示通配符（注：因平时对正则表达式不太会用，所以不使用正则表达式。如熟练，应该改为支持）
 * 	str_end：结束标识
 * 	include：是否包含标识本身，true|false，默认为false
 * 返回值：
 	所截取的字符串，如不存在，则返回空字符串
 e.g:  "你好，我是dda。".cut( "你(*)，我是", "。", false )
 */
String.prototype.cut = function( str_begin, str_end, include )
{
	if( include == undefined )
	{
		include = false;
	}

	var begin, end;

	begin = this.Parse( str_begin, 0 );

	if( begin.begin == undefined )
	{
		return '';
	}

	end = this.Parse( str_end, begin.end + 1 );
	if( end.begin == undefined )
	{
		return '';
	}

	var out = this.substring( begin.end + 1, end.begin );

	if( include )
	{
		out = begin.str + out + end.str;
	}

	return out;
}

// e.g: "hello,{name}, good {action.one}".format_key( { name:'dda', action: { one:'mornig'} } )
// 注意和String.prototype.format的区别
String.prototype.format_key = function( key, pre ) {
	if( pre != undefined )
	{
		pre += "\\.";
	}
	else
	{
		pre = "";
	}
	var val=this.toString();
	for(var i in key){
		if( typeof( key[i] ) == 'object' )
		{
			val = val.format_key( key[i], i );
		}
		else
		{
			val = val.replace(new RegExp("\\{" + pre + i + "\\}", "g"), key[i]);
		}
	}
	return val;
}

// e.g: "hello,{0},{1}".format( 'dda', 'good mornig' )
String.prototype.format = function(){
	if (arguments.length == 0) return null;
	var args = arguments;
	return this.replace(/\{(\d+)\}/g, function(m, i){
		return args[i];
	});
};

Date.prototype.format = function(fmt) 
{
    //author: meizz 
    var o =
     { 
        "M+" : this.getMonth() + 1, //月份 
        "D+" : this.getDate(), //日 
        "H+" : this.getHours(), //小时 
        "N+" : this.getMinutes(), //分 
        "S+" : this.getSeconds(), //秒 
        "m+" : this.getMilliseconds() //毫秒 
     }; 
    if (/(Y+)/.test(fmt)) 
         fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length)); 
    for (var k in o) 
        if (new RegExp("(" + k + ")").test(fmt)) 
             fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length))); 
    return fmt; 
}

Date.prototype.addDays = function(d)
{
    this.setDate(this.getDate() + d);
};

Date.prototype.addWeeks = function(w)
{
    this.addDays(w * 7);
};

Date.prototype.addMonths= function(m)
{
    var d = this.getDate();
    this.setMonth(this.getMonth() + m);
    if (this.getDate() < d)
        this.setDate(0);
};

Date.prototype.addYears = function(y)
{
    var m = this.getMonth();
    this.setFullYear(this.getFullYear() + y);
    if (m < this.getMonth()) 
     {
        this.setDate(0);
     }
};

// 判断是否数组
isArray = function (obj) {
	//return obj && !(obj.propertyIsEnumerable('length')) && typeof obj === 'object' && typeof obj.length === 'number';
	return obj instanceof Array;
};


/* 使用ajax方式post，将json结果交回调函数处理
 * file：接受post的文件名
 * data：postdata，为key=value数组
 * success, error：回调函数
 */
function post(file, method, data, success, error)
{
	var url = file + '.php';

	if( !data ) data = {};
	data.method = method;
	$.ajax
	({
		type : "POST",
		url : url,
		dataType : 'json',
		async : false,
		data : data,
		success : function(data) {
			if( success )
			{
				success(data);
			}
			else
			{
				alert( data.memo + '(代码=' + data.code + ')' );
			}
		},
		error : function (jqXHR, textStatus, errorThrown) {
			if( error )
			{
				error (jqXHR, textStatus, errorThrown);
			}
			else
			{
				alert(jqXHR.responseText);
			}
		}
	});

}
//标签切换
function setTab(name,cursel,n){ 
	for(i=1;i<=n;i++){ 
		var menu=document.getElementById(name+i); 
		var con=document.getElementById("tab_"+name+"_"+i); 
		menu.className=i==cursel?"current":""; 
		con.style.display=i==cursel?"block":"none"; 
	} 
}

//*** local storage 操作开始
// 保存
function save_data(c,v)
{
	localStorage.setItem(c,JSON.stringify(v));
}

// 获取，返回json（如不存在，返回{}
function get_data(c)
{
	var t=localStorage.getItem(c);
	return t?JSON.parse(t):{};
}

// 删除
function del_data(c)
{
	localStorage.removeItem(c);
}
//*** local storage 操作结束

JSON.print_r = function(obj) {
	var s = "{<br />";
	s += ergodic_print(obj, "");
	s += "}<br />";
	
	return s;
}

function ergodic_print(obj, indentation) {
	var indent = "&nbsp;&nbsp;" + indentation;
	var s = "";
	if (obj.constructor == Object) {
		for (var p in obj) {
			if (obj[p].constructor == Array || obj[p].constructor == Object) {
				s += indent + "[" + p + "] => " + typeof(obj) + "<br />";
				s += indent + "{";
				s += '<br />' + ergodic_print(obj[p], indent) + '';
				s += indent + "}<br />";
			} else if (obj[p].constructor == String) {
				s += indent + "[" + p + "] => '" + obj[p] + "'<br />";
			} else {
				s += indent + "[" + p + "] => " + obj[p] + "<br />";
			}
		}
	}
	
	return s;
}

/**
 * 带倒计时的提示框
 * @param text：提示内容
 * @param second：倒计时
 * @param callback：倒计时结束后或点击按钮时回调
 */
function timer_alert( text, second, callback )
{
	var down = function()
	{
		if( second == 0 )
		{
			$('#time_alert').hide();
			$('#timer_alert_btn').click();
		}
		else
		{
			--second;
			$('#timer_alert_btn').text( '确定({0})'.format( second ) );
		}
	};

	var s = '<div id="time_alert" class="modal hide fade in" style="display: none; ">'
		+ '<div class="modal-header">'
		+ '	<a class="close" data-dismiss="modal">×</a>'
		+ '	<h3>信息：</h3>'
		+ '</div>'
		+ '<div class="modal-body">'
		+ '	<p>{text}</p>'
		+ '</div>'
		+ '<div class="modal-footer">'
		+ '	<a href="#" id="timer_alert_btn" class="btn btn-success" data-dismiss="modal">确定({second})</a>'
		+ '</div>'
		+ '</div>';
	$('body').append( s.format_key( {text: text, second: second} ) );
	$('#timer_alert_btn').everyTime( '1s', down )
		.click( function(){
			$(this).stopTime();
			callback();
		});
	$('#time_alert').modal();
}

//获取地址栏里（URL）传递的参数  http://www.2cto.com/kf/201303/197066.html
function GetRequest() {
	//url例子：XXX.aspx?ID=" + ID + "&Name=" + Name；
	var url = location.search; //获取url中"?"符以及其后的字串
	var theRequest = new Object();
	if(url.indexOf("?") != -1)//url中存在问号，也就说有参数。
	{
		var str = url.substr(1);
		strs = str.split("&");
		for(var i = 0; i < strs.length; i ++)
		{
			//theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
			theRequest[strs[i].split("=")[0]]=decodeURIComponent(strs[i].split("=")[1]);
		}
	}
	return theRequest;
}

// 构造url（？及其后部分）
function EncodeRequest( json )
{
	var s = '';
	for( var i in json )
	{
		s += '&' + i + '=' + encodeURIComponent( json[ i ] );
	}

	if( s != '' )
	{
		s = '?' + s.substr( 1, s.length - 1 );
	}
	else
	{
		s = '?';
	}

	return s;
}

// 用户准确率表,浮动窗
function rate_popup(obj)
{
	$(obj).each(function(){
		var a = $(this);
		$(this).mouseover(function(){
			var temp = $(this).attr('rate_popup').split(',');	
			var uid = temp[0]
			var sport = temp[1];
			post('user', 'rate_popup', {uid:uid, sport:sport}, function(json){
				var s = json.data;		
				var format = [];
				$.each( [3,7,15,30], function(i, day){
					$.each( ['w','w2','d','l2','l','win_rate'], function(k, v){
						format[v+day] = s[v+day];
					});
				});
				
				var s = [];
				s.push('<div class="accuracy">');
				s.push('<table width="340" border="0" cellspacing="0" cellpadding="0">');
				s.push('<tr>');
				s.push('<th width="45" height="22">战绩</th>');
				s.push('<th width="45">赢</th>');
				if(sport==1) s.push('<th width="45">赢半</th>');
				s.push('<th width="45">平</th>');
				if(sport==1) s.push('<th width="45">输半</th>');
				s.push('<th width="45">输</th>');
				s.push('<th>准确率</th>');
				s.push('</tr>');
				s.push('<tr>');
				s.push('<td height="22">3天</td>');
				s.push('<td class="red">{w3}</td>');
				if(sport==1) s.push('<td class="red">{w23}</td>');
				s.push('<td class="t_c6">{d3}</td>');
				if(sport==1) s.push('<td>{l23}</td>');
				s.push('<td>{l3}</td>');
				s.push('<td class="red">{win_rate3}</td>');
				s.push('</tr>');
				s.push('<tr>');
				s.push('<td height="22">7天</td>');
				s.push('<td class="red">{w7}</td>');
				if(sport==1) s.push('<td class="red">{w27}</td>');
				s.push('<td class="t_c6">{d7}</td>');
				s.push('<td>{l27}</td>');
				if(sport==1) s.push('<td>{l7}</td>');
				s.push('<td class="red">{win_rate7}</td>');
				s.push('</tr>');
				s.push('<tr>');
				s.push('<td height="22">15天</td>');
				s.push('<td class="red">{w15}</td>');
				if(sport==1) s.push('<td class="red">{w215}</td>');
				s.push('<td class="t_c6">{d15}</td>');
				if(sport==1) s.push('<td>{l215}</td>');
				s.push('<td>{l15}</td>');
				s.push('<td class="red">{win_rate15}</td>');
				s.push('</tr>');
				s.push('<tr>');
				s.push('<td height="22">30天</td>');
				s.push('<td class="red">{w30}</td>');
				if(sport==1) s.push('<td class="red">{w230}</td>');
				s.push('<td class="t_c6">{d30}</td>');
				if(sport==1) s.push('<td>{l230}</td>');
				s.push('<td>{l30}</td>');
				s.push('<td class="red">{win_rate30}</td>');
				s.push('</tr>');
				s.push('</table>');
				s.push('</div>');
				var table = s.join('');
				table = table.format_key( format );
				$(a).append( '<div class="accuracy1">'+ table +'</div>' );
			});
		});		
	});
}
// 用户准确率表,浮动窗 end

function copyToClipboard(txt ){
	if (window.clipboardData) {
		window.clipboardData.clearData();
		window.clipboardData.setData("Text", txt);
	} else if (navigator.userAgent.indexOf("Opera") != -1) {
		window.location = txt;
	} else if (window.netscape) {
		try {
			netscape.security.PrivilegeManager
				.enablePrivilege("UniversalXPConnect");
		} catch (e) {
			alert("你使用的FireFox浏览器,复制功能被浏览器拒绝！\n请在浏览器地址栏输入“about:config”并回车。\n然后将“signed.applets.codebase_principal_support”双击，设置为“true”");
			return;
		}
		var clip = Components.classes['@mozilla.org/widget/clipboard;1']
			.createInstance(Components.interfaces.nsIClipboard);
		if (!clip)
			return;
		var trans = Components.classes['@mozilla.org/widget/transferable;1']
			.createInstance(Components.interfaces.nsITransferable);
		if (!trans)
			return;
		trans.addDataFlavor('text/unicode');
		var str = new Object();
		var len = new Object();
		var str = Components.classes["@mozilla.org/supports-string;1"]
			.createInstance(Components.interfaces.nsISupportsString);
		var copytext = txt;
		str.data = copytext;
		trans.setTransferData("text/unicode", str, copytext.length * 2);
		var clipid = Components.interfaces.nsIClipboard;
		if (!clip)
			return false;
		clip.setData(trans, null, clipid.kGlobalClipboard);
	}
	else{
		//alert("你的浏览器不支持一键复制功能");
		prompt( '请复制uid：', txt );
		return;
	}
}