<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(27);
?>

    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
						<a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						类型：<input id="type" class="mini-combobox" style="width:150px;" textField="text" valueField="id" 
    data="[{id:0,text:'主题'},{id:1,text:'回复'}]" value="<?php echo $_GET['type'];?>" showNullItem="true" allowInput="false"/>
    					用户：<div id="uid" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id"
					        url="data/user.php?method=search&sortField=name&sortOrder=asc" value="" text=""  onvaluechanged="onValueChanged">     
					        <div property="columns">
					            <div header="名字" field="name" width="30"></div>
								<div header="NPC" field="npc" width="10"></div>
								<div header="登录" field="login" width="25"></div>
					        </div>
					    </div>
                        内容：<input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:150px;" onenter="onKeyEnter"/>   
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div width="10" type="checkcolumn"></div>
            <div field="id" width="20" headerAlign="center" allowSort="true">id</div>
            <div field="time" width="30" headerAlign="center" allowSort="true">时间</div>
			<div field="type" width="30" headerAlign="center" allowSort="true">类型</div>
            <div field="uid" displayfield="name" width="30" headerAlign="center" allowSort="false">发布者</div>
			<div field="npc" displayfield="sender_name" width="10" headerAlign="center" allowSort="false">NPC</div>
			<div field="content" headerAlign="center" allowSort="false">内容</div>
			<div field="pic" width="80" headerAlign="center" allowSort="false">图片</div>
			<div field="replys" width="20" headerAlign="center" allowSort="true">回复数</div>
			<div field="action" width="50" headerAlign="center" allowSort="false">操作</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'comment';}
</script>

<script type="text/javascript">
function marketing(uid, id)
{
	var func = function(){
		tab('tab$27','说两句','comment.php');
	}
	post( "audit", "add", {uid: uid, value: id, type: 1}, func, function(){} );
}
	
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	var data={};
	<?php
	$s = '';
	foreach( $_GET as $k => $v )
	{
		$s .= "data.{$k} = '{$v}';\n";
	}
	echo $s;
	?>
	grid.load(data);
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;

		var html = "";
		switch( field )
		{
			case "type":
				if( value == '0' ) html = '主题';
				else if( value == '1' ) html = '回复';
				break;
				
			case "npc":
				if( value == '1' ) html = '√';
				break;
	    		
			case "pic":
				if( e.record.pic > 0 ) html = '<a href="'+record.large+'" target="_blank"><img src="'+record.small+'" /></a>';
				else html = null;
				break;
		
			case "action":
				html = '<a href="javascript:void();" onclick=\'tab("detail' + record.uid +'", "'+record.name.replace('"','')+'说两句营销", "../backend/auto_publish_comment2.php?id='+e.record.id+'")\'>营销</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="marketing('+ record.uid +','+e.record.id+');return false;">营销(新)</a>';
				break;
	    }
		if( html != "" ) e.cellHtml = html;


	});
}

function search() {
    var q = mini.get("q").getValue();
    var uid = mini.get("uid").getValue();
	var type = mini.get("type").getValue();
    grid.load({ q: q, uid:uid, type:type });
}
function onKeyEnter(e) {
    search();
}

function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>