<?php require 'head.php'; ?>
    
<div style="padding:5px;text-align:center;">
	<label>关键字：<input name="q" id="q" class="mini-textbox" style="width:160px;"/></label>
	<a class="mini-button" onclick="onSearchClick()">查找</a>
</div>

	<div id="datagrid1" class="mini-datagrid" style="width:98%;height:60%;" 
        url="data/administrator.php?method=sel_administrator" idField="id"
        allowResize="true" pageSize="10" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
    	<div property="columns">
            <div type="checkcolumn"></div>
        	<div field="id" displayField="id" width="120">id</div>
            <div field="name" width="100" allowSort="true">用户名</div>
            <div field="last" width="120" headerAlign="center" allowSort="true">登录</div>
        </div>
    </div>
    
    <div style="padding:15px;text-align:center;">   
        <a class="mini-button" onclick="onOk" style="width:60px;margin-right:20px;">确定</a>       
        <a class="mini-button" onclick="onCancel" style="width:60px;">取消</a>       
    </div>

<?php require 'bottom.php'; ?>
    
<script type="text/javascript">
    mini.parse();
    
    //////////////////////////////////////        

    function SetData(data) {
        //跨页面调用，克隆数据更安全
        data = mini.clone(data);

        grid.load();
        grid.deselectAll();
    }
    function GetData() {
        var rows = grid.getSelecteds();
        var ids = [], texts = [];
        for (var i = 0, l = rows.length; i < l; i++) {
            var row = rows[i];
            ids.push(row.id);
            texts.push(row.name);
        }

        var data = {};
        data.id = ids.join(",");
        data.text = texts.join(",");
        return data;
    }
    function CloseWindow(action) {            
        if (window.CloseOwnerWindow) return window.CloseOwnerWindow(action);
        else window.close();
    }
    function onOk(e) {
        CloseWindow("ok");
    }
    function onCancel(e) {
        CloseWindow("cancel");
    }
    /////////////////////////////////////

    var grid = mini.get("datagrid1");

    function onSearchClick(e) {
    	var q = mini.get("q");
        grid.load({
            q: q.value
        });
    }

</script>
