<?php
require 'head.php';

check_privilege(0);
check_privilege(83);
?>

<link rel="stylesheet" type="text/css" hrf="js/jqplot/jquery.jqplot.min.css" />

<fieldset id="fd2" style="width:98%;">
	<legend><label>图表</label></legend>
	<div class="fieldset-body">
		<div id="chart1" style="margin-top:5px; margin-left:5px; width:98%; height:300px;"></div>
	</div>
</fieldset>
<div class="description">
	<h3>说明</h3>
</div>

<script type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
<script>
	$(document).ready(function () {

		var chart_data = new Array(3);

		post('rich', 'search', {pageSize:20}, function(data){

			$.each( data.data , function(i,t){

				chart_data[0] = [t.time,t.user];
				chart_data[1] = [t.time,t.user];
				//chart_data[2] = [3,3];

			});

		}, function(){});


		// Lines can be drawn as solid, dashed or dotted with the "linePattern" option.
		// The default is "solid".  Other basic options are "dashed" and "dotted".
		plot1 = $.jqplot("chart1", chart_data, {
		  seriesDefaults:{
			  linePattern: 'dashed',
			  showMarker: false,
			  shadow: false
		  },
			axes:{
        xaxis:{
          renderer:$.jqplot.DateAxisRenderer,
          tickOptions:{
            formatString:'%b&nbsp;%#d'
          } 
        },
        yaxis:{
          tickOptions:{
            formatString:'$%.2f'
            }
        }
      }
		});
	});
</script>

