<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(102); ?>

<div style="width:98%;">
	<div id="form1">
		<table class="form-table" border="0" cellpadding="1" cellspacing="1">
			<tr>
              <td class="label">位置：</td>
			  <td><div id="position1" class="mini-checkbox" checked="true" text="9宫格"></div>
			  <div id="position2" class="mini-checkbox" checked="true" readOnly="false" text="商城"></div>			  </td>
		  </tr>
			<tr>
              <td class="label">广告语: </td>
			  <td><input class="mini-textbox" id="ad" name="ad" style="width:200px" required="true" /></td>
		  </tr>
			<tr>
              <td class="label">跳转jump：</td>
			  <td><input id="jump" name="jump" class="mini-textbox" style="width:200px" value="1600" vtype="int" /></td>
		  </tr>
			<tr>
              <td class="label">执行时间：</td>
			  <td ><input id="time" name="time" class="mini-datepicker" style="width:200px" showtime="true"
							format="yyyy-MM-dd HH:mm:ss" /></td>
		  </tr>
			<tr>
              <td class="label">数据：</td>
			  <td ><input id="jump_data" name="jump_data" class="mini-textarea" style="width:600px;height:160px">
			    （每行为key=value串；请正确填写）<br />
				<div id="is_json" name="is_json" class="mini-checkbox" checked="true" text="JSON格式" trueValue="1" falseValue="0"></div></td>
		  </tr>
			<tr>
				<td>&nbsp;</td>
				<td><a class="mini-button" onclick="submitForm()" style="width: 150px;">保存</a></td>
			</tr>
		</table>
	</div>
</div>

<div class="description">
	<h3>说明</h3>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
mini.parse();

var form = new mini.Form("form1");

function submitForm()
{
	form.validate();
	if (form.isValid() == false) return;

	var ad = mini.get("ad").getValue();
	var jump = mini.get("jump").getValue();
	var jump_data = mini.get("jump_data").getValue();
	var is_json = mini.get("is_json").getValue();

	var data = [];

	if( mini.get("position1").getChecked() )
	{
		data.length += 3;
		data[data.length-3] = { section:'home', key: 'activity_words', value: ad, is_json: is_json };
		data[data.length-2] = { section:'home', key: 'activity_jump', value: jump, is_json: is_json };
		data[data.length-1] = { section:'home', key: 'activity_jump_data', value: jump_data, is_json: is_json };
	}

	if( mini.get("position2").getChecked() )
	{
		data.length += 3;
		data[data.length-3] = { section:'market', key: 'activity_words', value: ad, is_json: is_json };
		data[data.length-2] = { section:'market', key: 'activity_jump', value: jump, is_json: is_json };
		data[data.length-1] = { section:'market', key: 'activity_jump_data', value: jump_data, is_json: is_json };
	}

	var postdata = {status:1,
		type: 7,
		time: mini.get("time").getText(),
		data: data
		}
	var value = mini.encode(postdata);
	post( 'task_queue', "set_ad", value, function (text) {
			alert(text.message);
			location.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

init_form( 'form1', 'task_queue', "init_ad" );

</script>