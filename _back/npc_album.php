<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(29); ?>
<?php $module = 'npc_album'; ?>

<div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
	 url="data/<?php echo $module; ?>.php?method=search" idField="user"
	 allowResize="true" pageSize="10" allowAlternating="true"
	 allowCellSelect="true" multiSelect="true"       
	 showEmptyText="true" contextMenu="#gridMenu"
	 >
	<div property="columns">      
		<div field="album" width="200" headerAlign="center">相片</div>
		<div field="action" width="100" headerAlign="center" allowSort="false">分配给</div>
	</div>
</div>

<div class="description">
	<h3>说明</h3>
	<ul>
		<li>图片为采集得来，需要审核，审核时，直接选择该图片分配给男NPC，还是女，如不符合的图片，请直接删除；</li>
		<li>审核后的图片，系统自动随机上传；</li>
		<li>未上传的图片也可手工删除，但不影响已经发布的相片；</li>
	</ul>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
	user_context_column = { uid: 'name' };

	//////覆盖
	function module() { return '<?php echo $module; ?>';}
	//////覆盖结束

	function onDrawcell (e) {
		var row = e.record,
		column = e.column,
		field = e.field,
		value = e.value;

		var html = "";
		switch( field )
		{
			case 'album':
				var album = row.album;
				html += '<table><tr>';
				var td = '<td><a href="{0}" target="_blank"><img src="{0}" style="width:80px;height:80px;border:0px;"/></a><br/>';

				if( row.uid )
				{
					td += '<a href="javascript:void(0);" onclick="upload({1})">上传</a> ';
				}
				td += '<a href="javascript:void(0);" onclick="del(\'id\',{1})">删除</a></td>';

				var len = album.length;
				for( var i = 0; i < len; ++i )
				{
					var pic = album[ i ];
					html += String.format( td, 'http://touyou.mobi/frontend/logo/npc/' + pic.path, pic.id );
				}
				html += '</tr></table>';
				break;
			case 'action':
				if( row.name )
				{
					html = row.name + '&nbsp;';
				}
				else
				{
					html = String.format( '<a href="javascript:void();" onclick="add_npc({0},0)">加为男NPC</a>' +
						'&nbsp;<a href="javascript:void();" onclick="add_npc({0},1)">加为女NPC</a>', row.id );
				}
				html += String.format( '&nbsp;<a href="javascript:void();" onclick="del(\'parent_id\',{0})">删除整组图片</a>', row.parent_id );
				break;
		}
		
		if( html != "" ) e.cellHtml = html;
	}
		
	function add_npc(id,sex)
	{
		$.ajax({
			url: "data/grab_album.php?method=add_npc&grab_user=" + id + "&sex="+sex,
			type : "POST",
			dataType : 'json',
			success: function (text) {
				if(text.code==1)
				{

				}
				else 
				{
					alert(text.message);
				}
			
			}
		});
	}

	function del( kind, id )
	{
		var str = ( kind == 'id' ) ? '将要删除本照片，你确定么？' : '将要删除本组所有照片，你确定么？';
		if( confirm( str ) )
		{
			var data = {}
			data[kind] = id;
			post( module(), 'del', data, function () {
					grid.reload();
				},function (jqXHR, textStatus, errorThrown) {
					alert(jqXHR.responseText);
				}
			);
		}
	}
	
	function upload( id )
	{
		if( confirm( '你将要上传这张相片，确定么？' ) )
		{
			post( module(), 'upload', {id:id}, function () {
					grid.reload();
				},function (jqXHR, textStatus, errorThrown) {
					alert(jqXHR.responseText);
				}
			);
		}
	}

</script>
