<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(12); ?>
	
	<?php $module = 'picture'; ?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        url="data/administrator.php?method=search" idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >    
        <div property="columns">
            <div field="id" width="20" headerAlign="center" allowSort="true">id</div>
			<div field="time" width="50" headerAlign="center" allowSort="true">时间</div> 
            <div field="pic" width="120" headerAlign="center" allowSort="true">图片</div>                      
            <div field="type" width="30" headerAlign="center" allowSort="true">类型</div>                      
            <div field="dir" width="50" headerAlign="center">目录</div>
			<div field="path" width="100" headerAlign="center">路径</div>
			<div field="related" width="10" headerAlign="center">相关id</div>
			<div field="memo" width="50" headerAlign="center">备注</div>
			<div field="action" width="30" headerAlign="center">操作</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return '<?php echo $module; ?>';}

$('#toolbar .mini-button:lt(3),#toolbar .separator,#q').hide();
$('#search').html('<a class="mini-button" onclick="tab(\'picture_upload\',\'图片上传\')">图片上传</a>');

function onDrawcell (e) {
	var record = e.record,
		column = e.column,
		field = e.field,
		value = e.value;

	var html = "";
	switch( field )
	{
		case "pic":
			html = '<img src="../'+record.dir+record.path+'" style="width:40px;height:40px;"/>';
			break;
	}
	if( html != "" ) e.cellHtml = html;
}
</script>
