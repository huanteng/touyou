<?php 
require 'head.php';
require 'user_context.php';
?>
<?php check_privilege(0); ?>
<?php check_privilege(60); ?>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=report');
	grid.load();
}

function search() {
    var start = mini.get("start").getFormValue();
    var end = mini.get("end").getFormValue();
    grid.load({ start: start,end: end });
}

function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
    				<td style="white-space:nowrap;" id="search">   					
                        开始日期：<input id="start" class="mini-datepicker" value="" />
    					结束日期：<input id="end" class="mini-datepicker" value="" />       
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div field="user" displayField="name" width="50" headerAlign="center" allowSort="false">名称</div>
			<div field="register" width="50" headerAlign="center" allowSort="false">注册时间</div>
            <div field="count" width="50" headerAlign="center" allowSort="false">充值次数</div>
			<div field="success" width="50" headerAlign="center" allowSort="false">成功次数</div>
			<div field="failure" width="50" headerAlign="center" allowSort="false">失败次数</div>
			<div field="total" width="50" headerAlign="center" allowSort="false">充值总额</div>
			<div field="last_pay" width="50" headerAlign="center" allowSort="false">最后充值时间</div>
        	<div field="money_quantity" width="30" headerAlign="center" allowSort="false">金币余额</div>
			<div field="bullion_quantity" width="30" headerAlign="center" allowSort="false">元宝余额</div>	
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'payment';}
</script>