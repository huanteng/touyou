<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(19); ?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;

		var html = "";
		switch( field )
		{
			case "logo":
				html = '<img src="'+e.record.logo+'" width="20px;" height="20px;"/>';
				break;
		
			case "action":
				html = '<a href="props_give.php?props_id='+e.record.id+'">赠送</a>';
				html += '&nbsp;<a href="javascript:void();" onclick=\'window.parent.showTab({id:"props' + record.id+'", text:"'+record.name+'持有情况", url: "user_props.php?props='+record.id+'"})\'>持有</a>';
				break;
	    }
		if( html != "" ) e.cellHtml = html;

	});
}

function search() {
    var q = mini.get("q").getValue();
    grid.load({ q: q });
}
function onKeyEnter(e) {
    search();
}

function add() {
	// 此函数最好在具体实现中覆盖，以实现默认值
    var newRow = {};
    grid.addRow(newRow, 0);
}
function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}
function truncate() {
    grid.loading("清空中，请稍候...");
	post( module(), "truncate", '', function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-add" onclick="add()" plain="true">增加</a>
                        <a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
                        <input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:150px;" onenter="onKeyEnter"/>   
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div type="checkcolumn" width="10"></div>
            <div field="id" width="10" headerAlign="center" allowSort="true">id
            </div>
			<div field="logo" width="10" headerAlign="center" allowSort="false">图标
            </div>
        	<div field="name" width="30" headerAlign="center" allowSort="true">名称
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
            <div field="price" width="20" headerAlign="center" allowSort="true">价格
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>	
        	<div field="kind" displayField="kind_name" width="20" headerAlign="center" allowSort="true">类型
                <input property="editor" class="mini-combobox" style="width:100%;" textField="text" valueField="id" 
					   url="data/props.php?method=kind" />
            </div>
        	<div field="short_desc" width="80" headerAlign="center" allowSort="true">简介
                <input property="editor" class="mini-TextArea" style="width:100%;" />
            </div>
        	<div field="description" width="100" headerAlign="center" allowSort="true">描述
                <input property="editor" class="mini-TextArea" style="width:100%;" />
            </div>
        	<div field="gift" width="20" headerAlign="center" allowSort="true">奖励
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
			<div field="memo" width="50" headerAlign="center" allowSort="true">完成提示
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
			<div field="day" width="10" headerAlign="center" allowSort="true">有效期
				<input property="editor" class="mini-textbox" style="width:100%;" />
			</div>
			<div field="have" width="20" headerAlign="center" allowSort="true">持有</div>
			<div field="seq" width="10" headerAlign="center" allowSort="true">顺序
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
			<div field="discount" width="10" headerAlign="center" allowSort="true">折扣
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div type="checkboxcolumn" field="is_hot" trueValue="1" falseValue="0" width="10" headerAlign="center">热销</div>
			<div type="checkboxcolumn" field="is_list" trueValue="1" falseValue="0" width="10" headerAlign="center">列表显示</div>
			<div type="checkboxcolumn" field="need_use" trueValue="1" falseValue="0" width="10" headerAlign="center">需使用</div>
            <div type="checkboxcolumn" field="status" trueValue="1" falseValue="0" width="10" headerAlign="center">状态</div>
            <div field="action" width="30" headerAlign="center" allowSort="false">操作
            </div>	
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
			<li>“奖励”字段约定:1、每行为其中一种道具，各行的道具均可能中。即：可能奖励多种道具；<br>
				2、每行为两部分组成，第一部分为概率，为一个少于100的数字；第二部分为奖励，之间还可能是多种道具，相互之间用逗号隔开；<br>
				 	  (如果第一部分忽略，表示100%)<br>
				3、道具表达方式为id=数量，其中，数量如果为随机，用-隔开；<br>
				4、约定各行的道具id均只出现一次，否则会可能被覆盖</li>
			<li>“折扣”列为1时，表示无折扣，其它值表示有折扣。</li>
			<li>有效期的单位为天数，0表示不限。</li>
       </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'props';}
</script>