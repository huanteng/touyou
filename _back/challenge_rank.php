<?php
require 'top.php';
require 'user_context.php';

check_privilege(0);
check_privilege(56);
?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div width="10" type="checkcolumn"></div>
            <div field="id" width="20" headerAlign="center" allowSort="true">id</div>
        	<div field="user" displayField="name" name="user" width="50" headerAlign="left" >用户</div>
			<div field="npc" width="20" headerAlign="left" >NPC</div>
        	<div field="win" width="20" headerAlign="center" allowSort="true">胜利
        		<input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="lost" width="20" headerAlign="center" allowSort="true">失败
        		<input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="gain" width="20" headerAlign="center" allowSort="true">盈利
            	<input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="all_win" width="20" headerAlign="center" allowSort="true">胜利次数
        		<input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="all_lost" width="20" headerAlign="center" allowSort="true">失败次数
        		<input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>
<script type="text/javascript">
	user_context_column = { user: 'user' };
	function module() { return 'challenge_rank';}

	function onDrawcell (e) {
		grid.on("drawcell", function (e) {
			var record = e.record,
				column = e.column,
				field = e.field,
				value = e.value;

			var html = "";
			switch( field )
			{
				case "user":
					html = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail' + record.id+'", text:"'+record.name+'", url: "user_detail.php?uid='+record.id+'"})\'>'+record.name+'</a>';
					break;

				case "npc":
					html = ( record.npc == '1' ) ? '√' : '';
					break;
			}
			if( html != "" ) e.cellHtml = html;

		});
	}
</script>

<?php require 'bottom.php'; ?>

