<?php require 'top.php'; ?>

<?php check_privilege(0); ?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:75%;" 
        idField="id"
        allowResize="true" pageSize="10" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div type="checkcolumn"></div>
            <div field="id" width="20" headerAlign="center" allowSort="true">id
            </div>
            <div field="time" width="20" headerAlign="center" allowSort="true">时间
            </div>
        	<div field="user" width="20" headerAlign="center" allowSort="true">用户
            </div>
        	<div field="sex" width="20" headerAlign="center" allowSort="true">性别
            </div>
        	<div field="score" width="20" headerAlign="center" allowSort="true">盈利
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
            <div field="total" width="20" headerAlign="center" allowSort="true">场次
            </div>	
        	<div field="win" width="20" headerAlign="center" allowSort="true">胜利
            </div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'integral_rank';}

grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
	    if (field == "sex") {
	        e.cellHtml = (value == '0') ? '男' : '女';
	    }

	});
</script>