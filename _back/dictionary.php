<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(22); ?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div type="checkcolumn"></div>
            <div field="id" width="20" headerAlign="center" allowSort="true">id
            </div>
            <div field="section" width="50" headerAlign="center" allowSort="true">类型
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
            <div field="key" width="50" headerAlign="center" allowSort="true">关键字
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="value" width="100" headerAlign="center" allowSort="true">值
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
        	<div field="remark" headerAlign="center" allowSort="true">备注
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
        	<div field="orderby" width="20" headerAlign="center" allowSort="true">排序
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
			<div type="checkboxcolumn" field="is_json" trueValue="1" falseValue="0" width="20" headerAlign="center">json</div>
        	<div field="time" width="30" headerAlign="center" allowSort="true">时间
            </div>
            	
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'dictionary';}
</script>