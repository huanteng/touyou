<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(69); ?>

    <div style="width:98%;">

		<div id="form1">
			<table>

				<tr>
					<td><label for="pwd$text">目标用户：</label></td>
					<td>
						<div id="user" class="mini-TextBoxList" style="width:350px;"  popupWidth="250" textField="name" valueField="id" 
					        url="data/user.php?method=search&sortField=name&sortOrder=asc" value="" text=""  onvaluechanged="onValueChanged" required="true" requiredErrorText="不能为空">     
					        <div property="columns">
					            <div header="名字" field="name" width="30"></div>
								<div header="NPC" field="npc" width="10"></div>
								<div header="登录" field="login" width="25"></div>
					        </div>
					    </div>&nbsp;可以输入多用户	
					</td>
				</tr>
				<tr>
					<td><label for="pwd$text">任务：</label></td>
					<td>
						<input id="task_id" class="mini-combobox" style="width:150px;" textField="name" valueField="id" 
    url="data/task.php?method=drop" value="<?php echo $_GET['task_id'];?>" allowInput="true" showNullItem="true" nullItemText="请选择..." required="true" requiredErrorText="必须选择道具"/>
					</td>
				</tr>

				<tr>
					<td></td>
					<td>
						<input value="分配" type="button" onclick="submitForm()" />
					</td>
				</tr>
			</table>
		</div>
		
		
	</div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
	mini.parse();

	function submitForm() {
		var form = new mini.Form("#form1");

		form.validate();
		if (form.isValid() == false) return;

		//提交数据
		var uid = mini.get("#user").getValue();
		var task_id = mini.get("#task_id").getValue();
	
		$.ajax({
			url: "data/task.php?method=assign",
			type: "post",
			data: { uid: uid, task_id: task_id },
			dataType: "json",
			success: function (text) {
				alert("成功");
			}
		});
	}
</script>