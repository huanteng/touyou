<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(13); ?>

<a class="mini-button" iconCls="icon-add" onclick="onAddBefore()" plain="true">插入节点(前)</a>
<a class="mini-button" iconCls="icon-add" onclick="onAddAfter()" plain="true">插入节点(后)</a>
<a class="mini-button" iconCls="icon-add" onclick="onAddNode()" plain="true">插入节点(子节点)</a>
<a class="mini-button" iconCls="icon-edie" onclick="onEditNode()" plain="true">编辑节点</a>
<!--服务器端未实现a class="mini-button" iconCls="icon-remove" onclick="onRemoveNode()" plain="true">删除节点</a-->
<span class="separator"></span>
<a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
<!--span class="separator"></span>
<a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a-->	
<ul id="tree1" class="mini-tree" url="data/admin_function.php?method=order" style="width:200px;padding:5px;" 
    showTreeIcon="true" textField="name" idField="id" parentField="parent_id" resultAsTree="false"  
    allowDrag="true" allowDrop="true" 
    >
</ul>
    
<script type="text/javascript">
    function onAddBefore(e) {
        var tree = mini.get("tree1");
        var node = tree.getSelectedNode();

        var newNode = {};
        tree.addNode(newNode, "before", node);
    }
    function onAddAfter(e) {
        var tree = mini.get("tree1");
        var node = tree.getSelectedNode();

        var newNode = {};
        tree.addNode(newNode, "after", node);
    }
    function onAddNode(e) {
        var tree = mini.get("tree1");
        var node = tree.getSelectedNode();

        var newNode = {};
        tree.addNode(newNode, "add", node);
    }
    function onEditNode(e) {
        var tree = mini.get("tree1");
        var node = tree.getSelectedNode();
        
        tree.beginEdit(node);            
    }
    function onRemoveNode(e) {

        var tree = mini.get("tree1");
        var node = tree.getSelectedNode();

        if (node) {
            if (confirm("确定删除选中节点?")) {
                tree.removeNode(node);
            }
        }
    }
    function onMoveNode(e) {
        var tree = mini.get("tree1");
        var node = tree.getSelectedNode();

        alert("moveNode");
    }
    function save() {
        var tree = mini.get("tree1");
        var data = tree.getData();
        var value = mini.encode(data);
        
				post( module(), "save_order", value, function (text) {
					alert("更新成功");
					reload();
				},function (jqXHR, textStatus, errorThrown) {
					alert(jqXHR.responseText);
				}
				);
    }
    function reload()
    {
    	var tree = mini.get("tree1");
    	tree1.load("data/admin_function.php?method=order");
    }
</script>

  
<div class="description">
    <h3>说明</h3>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'admin_function';}

</script>
