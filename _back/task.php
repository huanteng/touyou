<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(69); ?>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;

		if (field == "action") {
	        e.cellHtml = '<a href="task_assign.php?task_id='+e.record.id+'">分配</a>';
	    }

	});
}

function search() {
    var type = mini.get("type").getValue();
    grid.load({ type:type });
}
function onKeyEnter(e) {
    search();
}

function add() {
	// 此函数最好在具体实现中覆盖，以实现默认值
    var newRow = {};
    grid.addRow(newRow, 0);
}
function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}
function truncate() {
    grid.loading("清空中，请稍候...");
	post( module(), "truncate", '', function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-add" onclick="add()" plain="true">增加</a>
                        <a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
    					类型：<input id="type" class="mini-combobox" style="width:150px;" textField="text" valueField="id" 
    url="data/task.php?method=type" value="" allowInput="true" showNullItem="true" nullItemText="请选择..."/>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>
	
	<?php $module = 'task'; ?>
  
    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="user"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
			<div type="checkcolumn" width="10"></div>			
			<div field="id" width="10" headerAlign="center" allowSort="true">id
            </div>
			<div field="type" displayField="type_name" width="15" allowSort="true">类型
				<input property="editor" class="mini-combobox" style="width:150px;" textField="text" valueField="id" 
    url="data/task.php?method=type" value="" />
            </div>
			<div field="name" width="30" headerAlign="center" allowSort="true">任务名
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
			<div field="introduce" width="35" headerAlign="center" allowSort="true">简介
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
			<div field="description" width="35" headerAlign="center" allowSort="true">说明
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
			<div field="data" width="20" headerAlign="center" allowSort="true">默认数据
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
			<div field="gift" width="20" headerAlign="center" allowSort="true">奖励
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
			<div field="text" width="20" headerAlign="center" allowSort="true">按钮文字
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
			<div field="jump" width="15" headerAlign="center" allowSort="true">点击跳转
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
			<div field="jump_data" width="15" headerAlign="center" allowSort="true">跳转参数
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
			<div field="memo" width="50" headerAlign="center" allowSort="true">完成提示
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
			<div type="checkboxcolumn" field="is_finish_del" trueValue="1" falseValue="0" width="10" headerAlign="center">完成后删除</div>
			<div field="expires" width="25" allowSort="true">过期时间
				<input property="editor" class="mini-datepicker" value="" />
            </div>
			<div type="checkboxcolumn" field="status" trueValue="1" falseValue="0" width="10" headerAlign="center" allowSort="true">状态</div>
			<div field="action" width="20" allowSort="false">操作</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
		<ul>
			<li>“奖励”可填多个，每行一个，每行格式为“道具id=数量”</li>
		</ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
//////覆盖
function module() { return '<?php echo $module; ?>';}
</script>
