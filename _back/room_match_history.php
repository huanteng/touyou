<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(19); ?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();

	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;

		var html = "";
		switch( field )
		{
			case 'name1':
				html = ( record.name1 == '' ) ? '' : ( record.name1 + '<br>' + record.way1 );
				break;

			case 'name2':
				html = ( record.name2 == '' ) ? '' : ( record.name2 + '<br>' + record.way2 );
				break;

			case 'name3':
				html = record.name3 ? ( record.name3 + '<br>' + record.way3 ) : '';
				break;

			case 'name4':
				html = ( record.name4 == '' ) ? '' : ( record.name4 + '<br>' + record.way4 );
				break;

			case "action":
				html = '<a href="room_match_history_detail.php?id='+e.record.id+'">查看</a>';
//				html += '&nbsp;<a href="javascript:void();" onclick=\'window.parent.showTab({id:"props' + record.id+'", text:"'+record.name+'持有情况", url: "user_props.php?props='+record.id+'"})\'>持有</a>';
				break;
	    }
		if( html != "" ) e.cellHtml = html;

	});
}

function search() {
    var q = mini.get("q").getValue();
    grid.load({ q: q });
}
function onKeyEnter(e) {
    search();
}

function add() {
	// 此函数最好在具体实现中覆盖，以实现默认值
    var newRow = {};
    grid.addRow(newRow, 0);
}
function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}
function truncate() {
    grid.loading("清空中，请稍候...");
	post( module(), "truncate", '', function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束

function onShowRowDetail(e) {
	var grid = e.sender;
	var row = e.record;

	var employee_grid = mini.get("employee_grid");
	var detailGrid_Form = document.getElementById("detailGrid_Form");

	var td = grid.getRowDetailCellEl(row);
	td.appendChild(detailGrid_Form);
	detailGrid_Form.style.display = "block";

	employee_grid.load({ match_id: row.id });
}

</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-add" onclick="add()" plain="true">增加</a>
                        <!--a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a-->
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
                        <input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:150px;" onenter="onKeyEnter"/>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;"
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"
        showEmptyText="true" onshowrowdetail="onShowRowDetail"
    >
        <div property="columns">
			<div type="expandcolumn" >#</div>
            <div field="id" width="10" headerAlign="center" allowSort="true">id</div>
			<div field="no" width="30" headerAlign="center" allowSort="true">房间号</div>
            <div field="name1" width="20" headerAlign="center" allowSort="true">用户1</div>
			<div field="name2" width="20" headerAlign="center" allowSort="true">用户2</div>
			<div field="name3" width="20" headerAlign="center" allowSort="true">用户3</div>
			<div field="name4" width="20" headerAlign="center" allowSort="true">用户4</div>
        	<div field="make" width="80" headerAlign="center" allowSort="true">开始时间</div>
        	<div field="gold" width="100" headerAlign="center" allowSort="true">底金</div>
        	<div field="winner_name" width="20" headerAlign="center" allowSort="true">胜出者</div>
			<div field="action" width="30" headerAlign="center" allowSort="false">操作</div>
        </div>
    </div>

	<div id="detailGrid_Form" style="display:none;">
        <div id="employee_grid" class="mini-datagrid" style="width:100%;height:150px;"
			showPager="false"
            url="data/room_shout.php?method=search"
        >
            <div property="columns">
				<div field="id" width="40" headerAlign="center" align="right">id</div>
                <div field="name" width="40" headerAlign="center" align="right">用户名</div>
                <div field="way" width="60" align="left" headerAlign="center">动作</div>
                <div field="time" width="60" align="left" headerAlign="center">时间</div>
            </div>
        </div>
    </div>


    <div class="description">
        <h3>说明</h3>
        <ul>
			<li></li>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'room_match_history';}
</script>