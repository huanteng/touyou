<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(60); ?>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/payment.php?method=report2');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
        if (field == "week") {
	        e.cellHtml = '第'+e.record.week+'周';
	    }

	});
}

function search() {
    var date = mini.get("date").getFormValue();
    grid.load({ date: date });
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
		<div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">					    
                    </td>
                    <td style="white-space:nowrap;" id="search">   					
                        日期：<input id="date" class="mini-datepicker" value="" />       
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>

	<div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div field="week" width="50" headerAlign="center" allowSort="false">第几周</div>
            <div field="two" width="50" headerAlign="center" allowSort="false">2元</div>           
            <div field="ten" width="50" headerAlign="center" allowSort="false">10元</div> 
            <div field="thirty" width="50" headerAlign="center" allowSort="false">30元</div>     
        	<div field="fifty" width="50" headerAlign="center" allowSort="false">50元</div>
			<div field="onehundred" width="50" headerAlign="center" allowSort="false">100元</div> 
			<div field="total" width="50" headerAlign="center" allowSort="false">总额</div>
			<div field="day" width="50" headerAlign="center" allowSort="false">天数
            </div>
        </div>
    </div>
<div id="mychart"></div>
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>