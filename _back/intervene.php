<?php
require 'top.php';
require 'user_context.php';

check_privilege(0);
check_privilege(5);
?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        url="data/intervene_real.php?method=search" idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div width="10" type="checkcolumn"></div>
        	<div field="id" width="20">id</div>
        	<div field="uid" displayField="name" width="30" headerAlign="left" allowSort="true">用户
                <input property="editor" class="mini-buttonedit" onbuttonclick="onButtonEdit" style="width:100%;"/>
            </div>
			<div field="type" displayField="type_name" width="30" headerAlign="left" allowSort="true">类型
				<input id="type" property="editor" class="mini-combobox" style="width:150px;" textField="text" valueField="id"
    url="data/intervene.php?method=type" value="" required="true" allowInput="true" showNullItem="true" nullItemText="请选择..."/>
            </div>
			<div field="gold_count" width="20" headerAlign="center" allowSort="false">持有金币</div>
            <div field="gold" width="20" headerAlign="center" allowSort="true">已赢金币</div>
			<div field="win" width="20" headerAlign="center" allowSort="true">连胜</div>
			<div field="rate" width="30" allowSort="true">干预率%
				<input property="editor" class="mini-textbox" style="width:100%;"/>
			</div>
			<div field="times" width="30" allowSort="true">增强数
				<input property="editor" class="mini-textbox" style="width:100%;"/>
			</div>
			<div field="live" width="30" allowSort="true">放生数
				<input property="editor" class="mini-textbox" style="width:100%;"/>
			</div>
            <div field="target" width="20" headerAlign="center" allowSort="true">目标金币
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>                      
            <div field="time" width="30" headerAlign="center" allowSort="true" dateFormat="MM-dd HH:NN">更新时间
            </div>                      
            
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        	<li>当<b>目标金币&lt;0</b>时，系统会删除本记录。</li>
        	<li>当<b>干预率=0</b>时，表示只记录，未进行干预。</li>
			<li>增强数：如果增强数>0，当上调干预率时，干预率增速翻倍，并且增强数-1。</li>
			<li>放生数：如果放生数>0，当下调干预率时，会直接下调到0，并且放生数-1。</li>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
//////覆盖
user_context_column = { uid: 'uid' };
function module() { return 'intervene';}

function add() {
	var newRow = { rate: '63', gain: '20000', gold: '0' };
	grid.addRow(newRow, 0);
}
//////覆盖结束

function onButtonEdit(e) {
	var data = null;
	var fn = function (data) {
			grid.cancelEdit();
            var row = grid.getSelected();
            grid.updateRow(row, {
                uid: data.id,
                name: data.text
            });
		};
    sel_user( data, fn );
}


</script>
