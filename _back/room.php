<?php
require 'head.php';
require 'user_context.php';
?>
<?php check_privilege(0); ?>
<?php check_privilege(19); ?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();

	grid.on("drawcell", function (e) {
	    var row = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;

		var viewName = function(uid,name, npc)
		{
			var html = '';

			if( name )
			{
				html = '<span title="uid=' + uid + '"';
				if( !npc )
				{
					html += ' style="color: red;"';
				}
				
				html += '>' + name + '</span>';
			}
			return html;
		}

		var html = "";
		switch( field )
		{
			case 'name1':
				html = viewName( row.uid1, row.name1, row.npc1 );

				break;
			case 'name2':
				html = viewName( row.uid2, row.name2, row.npc2 );

				break;
			case 'name3':
				html = viewName( row.uid3, row.name3, row.npc3 );

				break;
			case 'name4':
				html = viewName( row.uid4, row.name4, row.npc4 );

				break;
			case "action":
//				html = '<a href="props_give.php?props_id='+row.id+'">赠送</a>';
//				html += '&nbsp;<a href="javascript:void();" onclick=\'window.parent.showTab({id:"props' + row.id+'", text:"'+row.name+'持有情况", url: "user_props.php?props='+row.id+'"})\'>持有</a>';
				break;
			default:
				break;
	    }
		if( html != "" ) e.cellHtml = html;

	});
}

function search() {
    var q = mini.get("q").getValue();
	var level = mini.get("level").getValue();
    grid.load({ q: q, level: level });
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						类型：<input id="level" class="mini-combobox" width="60" textField="text" valueField="id"
								   url="data/room.php?method=level" />
                        <input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:150px;" onenter="onKeyEnter"/>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;"
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div type="checkcolumn" width="10"></div>
            <div field="id" width="14" headerAlign="center" allowSort="true">id</div>
			<div field="level" displayField="level_name" width="20" headerAlign="center" allowSort="true">级别
                <input property="editor" class="mini-combobox" style="width:100%;" textField="text" valueField="id"
					url="data/room.php?method=level" />
            </div>
        	<div field="no" width="14" headerAlign="center" allowSort="true">房间号</div>
            <div field="person" width="14" headerAlign="center" allowSort="true">人数</div>
			<div field="real" width="14" headerAlign="center" allowSort="true">真人</div>
			<div field="name1" width="30" headerAlign="center">用户1</div>
			<div field="name2" width="30" headerAlign="center">用户2</div>
			<div field="name3" width="30" headerAlign="center">用户3</div>
			<div field="name4" width="30" headerAlign="center">用户4</div>
			<div field="ip" width="50" headerAlign="center">IP</div>
			<div field="time" width="50" headerAlign="center" allowSort="true">更新时间</div>
        	<div field="gold" width="100" headerAlign="center" allowSort="true">底金</div>
        	<div field="status" width="20" headerAlign="center" allowSort="true">状态</div>
			<div field="action" width="30" headerAlign="center" allowSort="false">操作</div>
        </div>
    </div>

    <div class="description">
        <h3>说明</h3>
        <ul>
			<li></li>
        </ul>
    </div>

<script type="text/javascript">
	user_context_column = { uid1: 'name1', uid2: 'name2', uid3: 'name3', uid4: 'name4' };
	function module() { return 'room';}
</script>
<?php require 'bottom.php'; ?>
