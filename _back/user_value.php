<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(64);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
	    if (field == "npc") {
	        e.cellHtml = (value == '1') ? '√' : '';
	    }

	});

}

function search() {
    var q = mini.get("q").getValue();
    var t = $('#type').val();    
    var must_win = $('#must_win').val();
    var table = mini.get("table").getValue();
    
    grid.load({ q: q, type: t, must_win: must_win, table: table });
}
function onKeyEnter(e) {
    search();
}

function add() {
	// 此函数最好在具体实现中覆盖，以实现默认值
    var newRow = {};
    grid.addRow(newRow, 0);
}
function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}
function truncate() {
    grid.loading("清空中，请稍候...");
	post( module(), "truncate", '', function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-add" onclick="add()" plain="true">增加</a>
                        <a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
                        用户：<input id="q" class="mini-buttonedit" onbuttonclick="onButtonEdit" allowInput="false"/>   
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div type="checkcolumn"></div>
            <div field="id" width="20" headerAlign="center" allowSort="true">id
            </div>
        	<div field="time" width="30" headerAlign="center" allowSort="true">时间
                <input class="mini-datepicker" showTime="true" style="width:100%;" />
            </div>
    		<div field="uid" displayField="name" width="30" headerAlign="center" allowSort="true">用户
                <input property="editor" class="mini-buttonedit" onbuttonclick="onButtonEdit" style="width:100%;" />
            </div>
            <div field="npc" width="10" headerAlign="center" allowSort="true">NPC
            </div>
        	<div field="key" width="30" headerAlign="center" allowSort="true">关键字
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
    		<div field="value" width="30" headerAlign="center" allowSort="true">值
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="data" width="30" headerAlign="center" allowSort="true">数据
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="expires" width="30" headerAlign="center" allowSort="true">有效时间
                <input property="editor" class="mini-datepicker" showTime="true" style="width:100%;" />
            </div>
        	<div field="remark" width="120" headerAlign="center" allowSort="true">备注
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'user_value';}

function onButtonEdit(e) {
	var data = null;
	var fn = function (data) {
			grid.cancelEdit();
            var row = grid.getSelected();
            grid.updateRow(row, {
                uid: data.id,
                name: data.text
            });
		};
    sel_user( data, fn );
}
</script>