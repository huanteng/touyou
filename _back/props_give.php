<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(67); ?>
<style>.plus,.egg{display:none;}</style>
<div style="width:98%;">

	<div id="form1">
		<table>
			<tr>
				<td><label for="username$text">赠送人：</label></td>
				<td>
					<div id="user" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id"
						 url="data/user.php?method=autocomplete" value="0" text="系统"  onvaluechanged="onValueChanged" required="true" requiredErrorText="不能为空">
						<div property="columns">
							<div header="名字" field="name" width="30"></div>
							<div header="NPC" field="npc" width="10"></div>
							<div header="登录" field="login" width="25"></div>
						</div>
					</div>
					&nbsp;&nbsp;快速填写：<a href="javascript:_user('0','系统')">系统</a>, <a href="javascript:_user('850','骰妖子')">骰妖子</a>，<a href="javascript:_user('237','DDA')">DDA</a>
					&nbsp;<input type="button" value="随机NPC" onclick="npc('');"/>
					&nbsp;<input type="button" value="男NPC" onclick="npc(0);"/>
					<input type="button" value="女NPC" onclick="npc(1);"/>
				</td>    
			</tr>
			<tr>
				<td><label for="pwd$text">目标用户：</label></td>
				<td>
					<div id="target" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id" 
						 url="data/user.php?method=autocomplete" value="<?php echo isset($_GET['target'])?$_GET['target']:''; ?>" text="<?php echo isset($_GET['target_name'])?$_GET['target_name']:''; ?>"  onvaluechanged="onValueChanged" required="true" requiredErrorText="不能为空">
						<div property="columns">
							<div header="名字" field="name" width="30"></div>
							<div header="NPC" field="npc" width="10"></div>
							<div header="登录" field="login" width="25"></div>
						</div>
					</div>	
				</td>
			</tr>
			<tr>
				<td><label for="pwd$text">道具：</label></td>
				<td>
					<input id="props" class="mini-combobox" style="width:150px;" textField="name" valueField="id"
						   url="data/props.php?method=drop&havegold=true" value="<?php echo $_GET['props_id']; ?>" allowInput="true" showNullItem="true" nullItemText="请选择..." required="true" requiredErrorText="必须选择道具"/>	
				</td>
			</tr>
			<tr>
				<td><label for="count">数量：</label></td>
				<td>
					<input id="count" class="mini-Textbox" style="width:150px;" required="true" requiredErrorText="不能为空" value="1"/>	
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>模拟购买行为：</td>
				<td>
					<input id="manual" class="mini-CheckBox" style="width:150px;" value="0" allowInput="true" showNullItem="true" required="true" requiredErrorText="必须选择" onclick="manual_pay"/>
					当勾选时，将产生一条自己购买的充值记录；类型为“人工”。以便于充值统计。（跟用户自己购买一样）
				</td>
			</tr>
			<tr class="plus">
				<td><label for="money">金额（元）：</label></td>
				<td>
					<input id="money" class="mini-Textbox" style="width:150px;" value=""/>
					&nbsp;&nbsp;金额数和道具数量要换算好；如 1个VIP礼包，金额是10
				</td>
			</tr>
			<tr class="plus">
				<td>备注：</td>
				<td>
					<input id="memo" class="mini-TextArea" style="width:150px;" value="线下充值"/>
					&nbsp;&nbsp;填入银行相关信息、订单号等
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>彩蛋：</td>
				<td>
					<input id="is_egg" class="mini-CheckBox" style="width:150px;" value="0" allowInput="true" showNullItem="true" required="true" requiredErrorText="必须选择" onclick="manual_egg"/>
					当勾选时，彩蛋内容将发送给目标用户
				</td>
			</tr>
			<tr class="egg">
				<td>彩蛋内容：</td>
				<td>
					<input id="egg" class="mini-TextArea" style="width:150px;" value=""/>
					&nbsp;&nbsp;填入彩蛋内容
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input value="赠送" type="button" onclick="submitForm()" />
				</td>
			</tr>
		</table>
	</div>


</div>

<div class="description">
	<h3>说明</h3>
	<ul>
		<li>当赠送人为系统时，将不产生动态，也不对目标用户产生消息</li>
	</ul>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
	mini.parse();

function npc(sex, obj)
{
	if( obj == '' || obj == undefined ) obj = 'user';

	func = function (data) {
		mini.get(obj).set({value: data.id, text: data.name});
	};

	post( "npc", "get_npc", {sex: sex}, func, function(){} );
}

function manual_pay()
{
	var m = mini.get("#manual").getValue();

	if(m)		
	{
		$('.plus').show();
	}
}

function manual_egg()
{
	var m = mini.get("#is_egg").getValue();

	if(m)		
	{
		$('.egg').show();
	}
}

function submitForm() {
	var form = new mini.Form("#form1");

	form.validate();
	if (form.isValid() == false) return;

	//提交数据
	var uid = mini.get("#user").getValue();
	var target_uid = mini.get("#target").getValue();
	var props_id = mini.get("#props").getValue();
	var count = mini.get("#count").getValue();
	var manual = mini.get("#manual").getValue();
	var money = mini.get("#money").getValue();
	var memo = mini.get("#memo").getValue();
	var egg = mini.get("#egg").getValue();
		
	$.ajax({
		url: "data/props.php?method=give",
		type: "post",
		data: { uid: uid, target_uid: target_uid, props_id: props_id, count: count, manual: manual, money: money, memo: memo, egg: egg },
		dataType: "json",
		success: function (text) {
			if( text.code != 1 ) alert(text.memo);
			else alert("赠送成功");
		}
	});
}
	
function _user(value,text)
{
	mini.get("#user").setValue(value);
	mini.get("#user").setText(text);
}
</script>