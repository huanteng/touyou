<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(99); ?>

<style>
	#container {
		background: #FFF;
		padding: 5px;
		margin-bottom: 20px;
		border-radius: 5px;
		clear: both;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}

	.centered { margin: 0 auto; }

	.box {
		margin: 5px;
		padding: 5px;
		background: #D8D5D2;
		font-size: 11px;
		line-height: 1.4em;
		float: left;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}

	.box h2 {
		font-size: 14px;
		font-weight: 200;
	}

	.box img,
	#tumblelog img {
		display: block;
		width: 100%;
	}

	.rtl .box {
		float: right;
		text-align: right; 
		direction: rtl;
	}

	.col1 { width: 80px; }
	.col2 { width: 180px; }
	.col3 { width: 280px; }
	.col4 { width: 380px; }
	.col5 { width: 480px; }

	.col1 img { max-width: 80px; border: 0px; }
	.col2 img { max-width: 180px; border: 0px; }
	.col3 img { max-width: 280px; border: 0px; }
	.col4 img { max-width: 380px; border: 0px; }
	.col5 img { max-width: 480px; border: 0px; }
</style>

<div style="width:98%;" >

	<div id="container" class="clearfix">


		<!--div class="box photo col3">
			<a href="http://www.flickr.com/photos/nemoorange/5013039951/" title="Stanley by Dave DeSandro, on Flickr"><img src="http://farm5.static.flickr.com/4113/5013039951_3a47ccd509.jpg" alt="Stanley" /></a>
			sky <a class="mini-button">通过</a> <a class="mini-button">删除</a>
		</div>

		<div class="box photo col3">
			<a href="http://www.flickr.com/photos/nemoorange/5013039885/" title="Officer by Dave DeSandro, on Flickr"><img src="http://farm5.static.flickr.com/4131/5013039885_0d16ac87bc.jpg" alt="Officer" /></a>
			sky <a class="mini-button">通过</a> <a class="mini-button">删除</a>
		</div>

		<div class="box photo col3">
			<a href="http://www.flickr.com/photos/nemoorange/5013039583/" title="Tony by Dave DeSandro, on Flickr"><img src="http://farm5.static.flickr.com/4086/5013039583_26717f6e89.jpg" alt="Tony" /></a>
			sky <a class="mini-button">通过</a> <a class="mini-button">删除</a>
		</div-->


	</div> <!-- #container -->

	<div>
		<div class="box photo col2">
			<a class="mini-button" onclick="audit_pass_all()">全部通过</a>
		</div>
	</div>

</div>

<div class="description" style="float:left; width:100%;">
	<h3>说明</h3>
	<ul>
		<li>页面5分钟自动刷新</li>
		<li>一般情况下为空白，新图片自动出现</li>
		<li>点击图片通过审核，并且不再出现</li>
		<li>删除：如果是说两句图片，将同时删除说两句</li>
	</ul>
</div>

<?php require 'bottom.php'; ?>
<script src="js/jquery.masonry.min.js"></script>
<script type="text/javascript">
	
	$('.mini-toolbar').remove();
	
	function audit_pass(id)
	{
		$.ajax({
			url: "data/audit.php?method=pass&id="+id,
			dataType: "json",
			success: function (data) {
				$('#div_'+id).remove();
			}
		});	
	}
	
	function audit_pass_all()
	{
		var badpic = 0;	
		$('#container img').each(function(){
			if( $(this).attr('src')  == '.s.jpg' ) badpic++;
		});

		if(badpic>0)
		{
			alert('有烂图，不能全部通过，请重新检查！');	
		}
		else
		{
			$.ajax({
				url: "data/audit.php?method=pass_all&type=2,4",
				dataType: "json",
				success: function (data) {
					$('#container div').remove();
				}
			});	
		}
		
	}
	
	function audit_del(id)
	{
		if( confirm('确定删除这张图片吗？') )
		{
			$.ajax({
				url: "data/audit.php?method=del_pic&id="+id,
				dataType: "json",
				success: function (data) {
					$('#div_'+id).remove();
				}
			});	
		}
	}
	
	function load()
	{
		$.ajax({
			url: "data/audit.php?method=album",
			dataType: "json",
			success: function (data) {
				var html='';
				$(data).each(function(i,row){
					
					html += '<div class="box photo col2" id="div_'+row.id+'">';
					html += '<a href="javascript:void(0);" onclick="audit_pass('+row.id+')"><img src="'+row.path+'.s.jpg" /></a>';
					html += '&nbsp;&nbsp;类型：'+((row.type==2) ? '相片' : '说两句的相片')+'&nbsp;&nbsp;';
					html += '<br/><a href="javascript:tab(\'user_detail\',\''+row.name+'\',\'user_detail.php?uid='+row.uid+'\')">'+row.name+'</a>&nbsp;&nbsp;';
					html += '<a href='+row.path+' target="_blank">查看</a>&nbsp;&nbsp;';
					html += '<a href="javascript:void(0);" onclick="audit_del('+row.id+')">删除</a>&nbsp;&nbsp;';
					//html += '<a href="javascript:void(0);" onclick="forbid_user('+row.uid+','+row.id+')">禁止帐号</a>&nbsp;&nbsp;';
					//html += '<a href="javascript:void(0);" onclick="forbid_mobile('+row.uid+','+row.id+')">禁止手机</a>';
					html += '</div>';				
					
				});
				$('#container').html(html);
			}
		});	

		setTimeout('load()',60000*5);
	}
	
	$(function(){

		var $container = $('#container');
		$container.imagesLoaded( function(){
			$container.masonry({
				itemSelector : '.box'
			});
		});

	});

	load();
</script>