<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(71); ?>

<div style="width:98%;">

	<br/>

	<div id="form1">
		<table>
			<tr>
				<td width="10%">&nbsp;<label for="uid">发送人：</label></td>
				<td>
					<div id="uid" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id" 
						 url="data/user.php?method=search&sortField=name&sortOrder=asc" value="" text="" required="true">     
						<div property="columns">
							<div header="名字" field="name" width="30"></div>
							<div header="NPC" field="npc" width="10"></div>
							<div header="登录" field="login" width="25"></div>
						</div>
					</div>
					<input type="button" value="随机NPC" onclick="npc('');"/>&nbsp;
					<input type="button" value="男NPC" onclick="npc(0);"/>&nbsp;
					<input type="button" value="女NPC" onclick="npc(1);"/>
				</td>
				<td width="20%">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;<label for="content">发送内容：</label></td>
				<td>
					<input id="content" class="mini-TextArea" style="width:300px;height:150px;" required="true" requiredErrorText="内容不能为空" value=""/>
				</td>
				<td>&nbsp;	<a href="#" onclick="rand_content();return false;">不知道说什么？</a><input type="hidden" id="mood_id"/></td>
			</tr>
			<tr>
				<td>&nbsp;<label>图片：</label></td>
				<td>
					<input type="file" name="comment_pic" id="comment_pic">
					<input type="hidden" name="comment_pic_id" id="comment_pic_id">
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;<label>发送时间：</label></td>
				<td>
					<input id="time" value="<?php echo date('Y-m-d H:i:s'); ?>" style="width:150px;"/>
					<a href="#" onclick="set_time('now');return false;" title="即时发布">即时</a>&nbsp;&nbsp;
					<a href="javascript:set_time(5)" title="后延5分钟">+5m</a>&nbsp;&nbsp;
					<a href="javascript:set_time(60)" title="后延1小时">+1h</a>&nbsp;&nbsp;
					<a href="javascript:set_time(-5)" title="提前5分钟">-5m</a>&nbsp;&nbsp;
					<a href="javascript:set_time(-60)" title="提前1小时">-1h</a>&nbsp;&nbsp;
					<a href="javascript:set_time('?')" title="自定义时间">+?m</a>
				</td>
				<td>&nbsp;默认即时发送</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input id="submit" value="发送" type="button" onclick="submitForm()" />
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</div>


</div>

<div class="description">
	<h3>说明</h3>
</div>

<?php require 'bottom.php'; ?>
<script type="text/javascript" src="js/ajaxfileupload.js"></script>	
<script type="text/javascript">
	
	function rand_content()
	{
		func = function (data) {
			mini.get('content').set({value: data.content});
			$('#mood_id').val(data.id);
		};
		
		post( "mood", "rand", {}, func, faile );
	}
	
	function faile(jqXHR, textStatus, errorThrown)
	{
		alert(jqXHR.responseText);
	}

	function npc(sex, obj)
	{
		if( obj == '' || obj == undefined ) obj = 'uid';

		func = function (data) {
			mini.get(obj).set({value: data.id, text: data.name});
		};

		post( "npc", "get_npc", {sex: sex}, func, faile );
	}

	function set_user( uid, name, obj )
	{
		if(obj == '' || obj == undefined) obj = 'uid';
		mini.get(obj).set( {value:uid, text: name} );
	}

	function set_time( minute, obj )
	{
		if(obj == '' || obj == undefined) obj = '#time';
		else obj = '#'+obj;
	
		if ( minute == '?' )
		{
			var minute = window.prompt( '请输入要延后的分钟，负数表示提前', 5 );

			if ( minute != null && CheckNumber( minute ) )
			{
				set_time( minute , 'time' );
				return;
			}
		}
		else if ( minute == 'now' )
		{
			$(obj).val( mini.formatDate ( new Date(), "yyyy-MM-dd HH:mm:ss" ) );
			return;
		}
		else if ( minute == '上贴+5' )
		{
			$(obj).val('+5');
			return;
		}

		var old = $(obj).val();
		if( old == '' )
		{
			set_time( 'now' );
		}
	
		if( old.substr(0,1) == '+' )
		{
			minute = parseInt( old.substr(1) ) + parseInt( minute );
			if( minute < 0 ) minute = 0;
			$(obj).val( '+' + minute );
		}
		else
		{
			var old = mini.parseDate( $(obj).val() );
			old.setMinutes( old.getMinutes() + parseInt(minute) );
			$(obj).val( mini.formatDate ( old, "yyyy-MM-dd HH:mm:ss" ));
		}
	}
	//调整时间程序结束

	function upload_pic()
	{
		var uid = mini.get("#uid").getValue();

		$('#comment_pic_id').after('<span id="pic_info" style="color:red;">图片上传中...请稍后</span>');

		$.ajaxFileUpload
		({
			url : 'data/comment.php?method=upload_pic',
			secureuri : false,
			fileElementId : 'comment_pic',
			dataType : 'json',
			data : { user : uid, name: 'comment_pic' },
			success: function( data )
			{
				$( '#comment_pic_id' ).val( data.code );

				$( '#pic_info' ).remove();

				if( $( '#comment_pic_id' ).val() > 0 ) submit_value();
				else alert('图片上传失败');
			}
		})
	}

	mini.parse();
	function submit_value() 
	{
		var form = new mini.Form("#form1");

		form.validate();
		if (form.isValid() == false) return;

		$('#submit').attr('disabled','disabled');
		
		//提交数据
		var uid = mini.get("#uid").getValue();
		var content = mini.get("#content").getValue();
		var comment_pic_id = $("#comment_pic_id").val();
		var time = $('#time').val();
		var mood_id = $('#mood_id').val();	//抓取回来的心情id，非必须的，提交是为了记录使用过，以免重复使用同一个心情

		func = function (data) {
			alert('发送成功');
			$('#submit').removeAttr("disabled");
			mini.get("#content").setValue('');
			$( '#comment_pic_id' ).val('');
		};
	
		var data = { uid: uid, content: content, comment_pic_id: comment_pic_id, time: time, mood_id: mood_id };
		post( "comment", "say", data, func, faile );
	}

	function submitForm()
	{
		var uid = mini.get("#uid").getValue();
		if( uid != '' && $('#comment_pic').val() != '' )
		{
			upload_pic();
		}
		else
		{
			submit_value();
		}
	}
</script>