<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(52); ?>
	<?php $module = 'challenge_log'; ?>
  
    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:75%;" 
        url="data/<?php echo $module; ?>.php?method=search" idField="user"
        allowResize="true" pageSize="10" allowAlternating="true"
        allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div type="checkcolumn"></div>
            <div field="type" width="120" headerAlign="center" allowSort="true">类型</div>                      
        	<div field="name1" width="120" headerAlign="center" allowSort="true">用户1</div>
            <div field="name2" width="120" headerAlign="center" allowSort="true">用户2</div>    
            <div field="gold" width="120" headerAlign="center" allowSort="true">金币</div>
            <div field="time" width="120" headerAlign="center" allowSort="true">时间</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        	<li>如果数据过多，你可<a href="javascript:void()" onclick="truncate();">点击清空</a>。</li>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
//////覆盖
function module() { return '<?php echo $module; ?>';}

//////覆盖结束

</script>
