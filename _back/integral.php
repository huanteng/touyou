<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(31);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
        switch( field )
		{
			case "action":
				e.cellHtml = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"integral'+record.id+'", text:"骰魔大赛获奖名单", url: "../backend/integral_rank.php?history='+((record._status==3)?1:0)+'&time='+record.time+'"})\'>获奖名单</a>';
				e.cellHtml += '&nbsp;<a href="javascript:void();" onclick=\'window.parent.showTab({id:"integral'+record.id+'", text:"骰魔大赛相关比赛", url: "../backend/integral_match.php?history='+((record._status==3)?1:0)+'&_start='+record.time+'&_end='+(record.time+3600)+'"})\'>相关比赛</a>';
				break;
		}

	});

}

function search() {
    var time = mini.get("time").getFormValue();

    grid.load({ time: time });
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:60%;" id="toolbar">
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
                        日期：<input id="time" class="mini-datepicker" value="" format="yyyy-MM-dd H:mm" showTime="true" style="width:150px;"/>   
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div field="id" width="20" headerAlign="center" allowSort="true">id</div>
        	<div field="datetime" width="30" headerAlign="center" allowSort="true">时间</div>
			<div field="person" width="30" headerAlign="center" allowSort="true">人数</div>
        	<div field="npc" width="30" headerAlign="center" allowSort="false">NPC</div>
    		<div field="real" width="30" headerAlign="center" allowSort="false">非NPC</div>
			<div field="match" width="30" headerAlign="center" allowSort="false">比赛</div>
			<div field="gold" width="30" headerAlign="center" allowSort="false">奖池</div>
			<div field="profit" width="30" headerAlign="center" allowSort="false">利润</div>
			<div field="gem" width="30" headerAlign="center" allowSort="false">送出宝石</div>
			<div field="_status" displayField="status" width="30" headerAlign="center" allowSort="false">状态</div>
        	<div field="action" width="60" headerAlign="center" allowSort="false">操作</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'integral';}
</script>