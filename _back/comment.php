<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(27); ?>
<style>
.left_box,.first_box,.reply_box{overflow: hidden; min-height: 75px;}
.reply_box2{border-style:groove; border-width:1pt; border-color: #a0c0ff; background-color: #a0c0ff;}
.first_content{width:70%;}
.img_box{
	margin: 5px;
	padding: 5px;
	background: #D8D5D2;
	font-size: 11px;
	line-height: 1.4em;
	float: left;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	width: 120px;
	float:right;
}
.img_box img{max-width: 120px; border: 0px;}
.gray{background-color: #e0e0e0;}
.yellow{background-color: #ffffc0;}
.light_gray{color: #c0c0c0;}
.title {
	font-size: 12px;
	line-height: 25px;
	font-weight: 600;
	font-family: sans-serif;
	text-rendering: optimizeLegibility;
}
#replys hr{border:1px dashed #ccc; height:1px}
</style>
<div class="mini-splitter" style="width:98%;height:100%;">
	<div size="35%" showCollapseButton="true">
		<div id="list" style="width:100%;height:98%;position:absolute; overflow:auto"></div>
		<!--a class="mini-button" onclick="pass_all">全部忽略</a-->
	</div>
	<div showCollapseButton="false">
		<div class="mini-splitter" style="width:100%;height:100%">
			<div size="65%" showCollapseButton="true">
				<div id="replys" title="内容区" iconCls="icon-add" style="width:100%;height:100%;position:absolute; overflow:auto"
				 showCollapseButton="true"
				>
					先在左边列表选择对话

				</div>
			</div>
			<div showCollapseButton="false">
				
				<!--营销-->
				<div id="queue_box" style="width:100%;height:100%;position:absolute; overflow:auto;display:none;">
					
					
					<?php for($i=0;$i<=5;$i++){?>
					<div id="queue_<?php echo $i;?>"<?php echo $i%2==0?' class="gray"':''?>>
					回复人：<div id="q_uid_<?php echo $i;?>" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id"
					url="data/user.php?method=autocomplete" value="" text="">
						<div property="columns">
							<div header="名字" field="name" width="30"></div>
							<div header="NPC" field="npc" width="10"></div>
							<div header="登录" field="login" width="25"></div>
						</div>
					</div>
					<a href="javascript:void(0);" onclick="set_user(850,'骰妖子','q_uid_<?php echo $i;?>');">骰妖子</a>&nbsp;
					<input type="button" value="随机NPC" onclick="npc('','q_uid_<?php echo $i;?>')"/>&nbsp;
					<input type="button" value="随机男NPC" onclick="npc(0,'q_uid_<?php echo $i;?>')"/>&nbsp;
					<input type="button" value="随机女NPC" onclick="npc(1,'q_uid_<?php echo $i;?>')"/>
					<br/>
					回复内容：<br/>
					<input id="q_content_<?php echo $i;?>" class="mini-TextArea" style="width:250px;height:50px;"/>
					<br/>
					回复时间：<input id="q_time_<?php echo $i;?>" style="width:150px;" value="+<?php echo $i*rand(1,10);?>"/><br/>
					<a href="#" onclick="set_time('now','q_time_<?php echo $i;?>');return false;" title="即时发布">即时</a>&nbsp;&nbsp;
					<a href="javascript:set_time(5,'q_time_<?php echo $i;?>')" title="后延5分钟">+5m</a>&nbsp;&nbsp;
					<a href="javascript:set_time(60,'q_time_<?php echo $i;?>')" title="后延1小时">+1h</a>&nbsp;&nbsp;
					<a href="javascript:set_time(-5,'q_time_<?php echo $i;?>')" title="提前5分钟">-5m</a>&nbsp;&nbsp;
					<a href="javascript:set_time(-60,'q_time_<?php echo $i;?>')" title="提前1小时">-1h</a>&nbsp;&nbsp;
					<a href="javascript:set_time('上贴+5','q_time_<?php echo $i;?>')" title="[自动计算]上一篇回复后的5分钟后">上贴+5m</a>&nbsp;&nbsp;
					<a href="javascript:set_time('?','q_time_<?php echo $i;?>')" title="自定义时间">+?m</a>

					<br/>
					<!--a href="#" onclick="add_queue('up');return false;">上增加</a>&nbsp;&nbsp;
					<a href="#" onclick="add_queue('down');return false;">下增加</a-->&nbsp;&nbsp;
					<span style="float:right;"><a href="#" onclick="del_queue('<?php echo $i;?>');return false;">删除</a></span>
					<br/>
					<hr/>
					</div>
					<?php }?>
					
					
					
					
					
					<input type="button" onclick="queue_save()" value="保存" />&nbsp;&nbsp;<input type="button" onclick="queue_close()" value="关闭" />&nbsp;&nbsp;
				</div>
				<!--//营销-->
				
				
				
			</div>
		</div>
	</div>        
</div>

<!--div class="description">
	<h3>说明</h3>
	<ul>
		<li>左边对话双击忽略</li>
	</ul>
</div-->

<!--//回复窗口-->
<div id="win1" class="mini-window" title="回复窗" style="width:600px;height:300px;" 
    showMaxButton="true"
    showToolbar="true" showFooter="true" showModal="false" allowResize="true" allowDrag="true"
    >

    <!--input id="id" type="hidden" value=""/>
	<input id="comment_id" type="hidden" value=""/-->
	<!--参与过的NPC-->
	<!--管理人员-->

	回复人：<div id="uid" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id"
			 url="data/user.php?method=autocomplete" value="" text="">
		<div property="columns">
			<div header="名字" field="name" width="30"></div>
			<div header="NPC" field="npc" width="10"></div>
			<div header="登录" field="login" width="25"></div>
		</div>
	</div>
	<a href="javascript:void(0);" onclick="set_user(850,'骰妖子','uid');">骰妖子</a>&nbsp;
	<input type="button" value="随机NPC" onclick="npc('')"/>&nbsp;
	<input type="button" value="随机男NPC" onclick="npc(0)"/>&nbsp;
	<input type="button" value="随机女NPC" onclick="npc(1)"/>
	<br/>
	回复内容：<br/>
	<div id="sub_parent"></div>
	<textarea id="reply_content" cols="60" rows="3"></textarea>
	<br/>
	回复时间：<input id="time" style="width:150px;"/> &nbsp;&nbsp;
	<a href="#" onclick="set_time('now');return false;" title="即时发布">即时</a>&nbsp;&nbsp;
	<a href="javascript:set_time(5)" title="后延5分钟">+5m</a>&nbsp;&nbsp;
	<a href="javascript:set_time(60)" title="后延1小时">+1h</a>&nbsp;&nbsp;
	<a href="javascript:set_time(-5)" title="提前5分钟">-5m</a>&nbsp;&nbsp;
	<a href="javascript:set_time(-60)" title="提前1小时">-1h</a>&nbsp;&nbsp;
	<a href="javascript:set_time('上贴+5')" title="[自动计算]上一篇回复后的5分钟后">上贴+5m</a>&nbsp;&nbsp;
	<a href="javascript:set_time('?')" title="自定义时间">+?m</a>
	<br/>
	其它： 
	<div id="ignore_other" class="mini-checkbox" checked="true" text="回复后关闭本主题"></div>&nbsp;
	<div id="ignore_pre" class="mini-checkbox" checked="false" text="切换主题时，自动忽略上一篇主题"></div>
	<br/>
	<br/>
	<input type="button" value="回复" style="height:25px;width:50px;" onclick="answer()"/>
	<input name="send" type="radio" value="return" checked="checked" />
	回车 	<input type="radio" name="send" value="ctrlreturn" />
	CTRL+回车<br>
</div>
<!--//回复窗口结束-->

<?php require 'bottom.php'; ?>

<script type="text/javascript">
// 要回复的root贴子id
var parent = 0;
// 要回复的贴子id
var sub_parent = '';
// 审核表id
var audit_id = 0;
// 审核最大id
var max_audit_id = 0;
// 列表数据，为数组，每行结构{audit_id:{info,parent}}
var list = {};
// 贴子数据，为json结构
var article = {};

//function setBGColor(obj,color) {
//  obj.style.backgroundColor=color;
// }
// function setBGColor2(obj) {
//  document.getElementByIdx(obj).style.backgroundColor='#FFFFFF';
// }
function CheckNumber( s )
{
	var reg = /^\-?[0-9]+\.?[0-9]{0,9}$/;
	return reg.test( s );
}
function set_time( minute, obj )
{
	if(obj == '' || obj == undefined) obj = '#time';
	else obj = '#'+obj;
	
	if ( minute == '?' )
	{
		var minute = window.prompt( '请输入要延后的分钟，负数表示提前', 5 );

		if ( minute != null && CheckNumber( minute ) )
		{
			set_time( minute , 'time' );
			return;
		}
	}
	else if ( minute == 'now' )
	{
		$(obj).val( mini.formatDate ( new Date(), "yyyy-MM-dd HH:mm:ss" ) );
		return;
	}
	else if ( minute == '上贴+5' )
	{
		$(obj).val('+5');
		return;
	}

	var old = $(obj).val();
	if( old == '' )
	{
		set_time( 'now' );
	}
	
	if( old.substr(0,1) == '+' )
	{
		minute = parseInt( old.substr(1) ) + parseInt( minute );
		if( minute < 0 ) minute = 0;
		$(obj).val( '+' + minute );
	}
	else
	{
		var old = mini.parseDate( $(obj).val() );
		old.setMinutes( old.getMinutes() + parseInt(minute) );
		$(obj).val( mini.formatDate ( old, "yyyy-MM-dd HH:mm:ss" ));
	}
}
//调整时间程序结束

function npc(sex, obj)
{
	if( obj == '' || obj == undefined ) obj = 'uid';
	
	if( parent == '' )
	{
		alert('请先选择对话');
		return;
	}

	var data = mini.encode( {id: parent, sex: sex} );
	func = function (data) {
			mini.get(obj).set({value: data.uid, text: data.name});
		};

	post( "comment", "npc", data, func, faile );
}

function get()
{
	var data = max_audit_id;

	post( "audit", "comment", data, get2, function(){} );

	// 20130929，移到get2中执行
	// setTimeout(get, 10000);
}

function get2( data )
{
	var content = '';
	var index = 0;
	for( id in data )
	{
		// 避免data成员函数而出错，不知产生原因
		if( isNaN(id) ) continue;

		var row = data[id];
		list[id] = row;
				
		content += String.format( "<div class='left_box{1}' id='audit_{0}' onclick='view({0});return false;' ondblclick='ignore({0});return false;' onmouseover=\"audit_over({0})\" onmouseout=\"audit_out({0})\">\n\
<div>", id, ((index%2==0)?'':' gray') );
			
		var del_comment_id=0;
		if( row.sub )
		{	
			var sub = row.sub;
			del_comment_id = sub.i;

			content +=  String.format( "&nbsp;{0}&nbsp;<span class='light_gray'>回复</span>{2}：<span class='title'>{1}</span><br>", sub.n, sub.c, ((sub.n2!=''&&sub.n2!=undefined)?'&nbsp;'+sub.n2+'&nbsp;':'') );	
		}
		else
		{
			del_comment_id = row.i;
		}
		
		content += String.format( "&nbsp;主题({0})：<span class='title'>{1}</span>", row.n, row.c );
		
		//忽略按钮
		ignore_html = String.format( "<div id='tool_{0}' style='display:none;'>&nbsp;<a href='#' onclick='ignore({0});return false;'>[忽略]</a>  \n\
<a href='#' onclick='ignore_article({0});return false;'>[忽略本主题]</a>  \n\
<a href='#' onclick='ignore_user({0});return false;'>[忽略本人]</a>\n\
<a href='#' onclick='del_comment({0},{1});event.cancelBubble=true;return false;'>[删除]</a></div>", id, del_comment_id );

		content += "</div>"+ignore_html+"</div><hr>";

		if( max_audit_id < id ) max_audit_id = parseInt(id);
		
		index++;
	}

	$("#list").prepend( content );

	setTimeout(get, 10000);
}
var over_audit_id = 0;
var over_audit_color = '#fff';
function audit_over( audit_id )
{
	over_audit_id = audit_id;
	over_audit_color = $( '#audit_' + audit_id ).css( 'backgroundColor' );
	$( '#audit_' + audit_id ).css( 'backgroundColor','#ffffc0' );
	$( '#tool_' + audit_id ).css( 'display', "block" );
}
function audit_out( audit_id )
{
	$( '#audit_' + audit_id ).css( 'backgroundColor', over_audit_color );
	$( '#tool_' + audit_id ).css( 'display', "none" );
}

//function publish()
//{
//	id = list[audit_id].i;
//	tab( id, "说两句营销", "../backend/auto_publish_comment2.php?id="+id);
//}

function publish2()
{
	$('#queue_box').show();

	$('.mini-splitter-pane1-button:eq(1)').click();
}

function ignore( id )
{
	$("#audit_" + id).next('hr').remove();
	$("#audit_" + id).remove();
	$('#replys').html( '' );
	delete list[id];

	var data = id;
	var func = function(){};

	post( "audit", "ignore", data, func, faile );
}

function ignore_article( id )
{// 忽略本主题
	var article_id = list[id].i;

	// 要忽略的audit_id列表，用逗号隔开
	var ignore_id = '';

	for( id in list )
	{
		if( list[id].i != article_id ) continue;

		$("#audit_" + id).next('hr').remove();
		$("#audit_" + id).remove();
		ignore_id += id + ',';

		delete list[id];
	}

	$('#replys').html( '' );
	var data = ignore_id;
	var func = function(){};
	post( "audit", "ignore", data, func, faile );
}

function ignore_user( audit_id )
{// 忽略本人
	var uid = list[audit_id].u;

	// 要忽略的audit_id列表，用逗号隔开
	var ignore_id = '';

	for( id in list )
	{
		if( list[id].u != uid ) continue;
		$("#audit_" + id).next('hr').remove();
		$("#audit_" + id).remove();
		ignore_id += id + ',';

		delete list[id];
	}

	$('#replys').html( '' );
	var data = ignore_id;
	var func = function(){};
	post( "audit", "ignore", data, func, faile );
}

function view( id )
{
	if( mini.get('ignore_pre').checked )
	{
		if( audit_id != 0 && audit_id != id )
		{
			ignore_article( audit_id );
		}
	}

	audit_id = id;

	setTimeout("$( '#audit_' + audit_id ).css( 'background-color','#ffffc0' ).removeClass('gray').addClass('yellow selected')", 1500);

	parent = sub_parent = list[id].i;	

	var data = sub_parent;

	$('#replys').html('<div>下载中</div>');

	post( "audit", "view_comment", data, view2, faile );
}

function view2(data)
{	

	$('#replys').html( '' );

	article = data;

	var name = '';

	var first = data.first;

	var link_name = function( is_npc, uid, name ){
		return is_npc ? String.format( "<a href='#' onclick='set_user({0},\"{1}\",\"uid\");event.cancelBubble=true;return false;'>{1}</a>", uid, name ) : name;
	};

	name = link_name( first.npc, first.u, first.n );
	var ignore = String.format( "<br><hr><div>\n\
<a href='#' onclick='ignore_article({0});return false;'>[忽略本主题]</a></div>", audit_id );

	var img_html = '';
	if( first.small )
	{
		img_html = String.format( "<div class='img_box'><a href='{0}' target='_blank'><img src='{1}'></a></div>", first.large, first.small );
	}
	//主题区
	var html = String.format( "<div class='first_box'>{2}\n\
	<div class='first_content'>&nbsp;主题（{0}）：<span style='float:right;'>{3}</span><br>\n\
	&nbsp;<span class='title'>{1}</span><br>\n\
&nbsp;<div style='float:right;'><a href='#' class='mini-button' style='height:25px;width:50px;' onclick='publish2();return false;'>营销</a>&nbsp;<a href='#' class='mini-button' style='height:25px;width:50px;' onclick='showReplyWindow();return false;'>回复</a></div></div></div>",
		name, first.c, img_html, first.t );

	html += ignore + '<hr>';

	$(data.queue).each(function(i,t){
		name = link_name( 1, t.u, t.n );

		html += String.format( "<br><hr>&nbsp;{0}<b> 将于 {1}</b>：<br>&nbsp;{2}", name, t.t, t.c );
	});
	
	var index=0;
	for(i in data.list )
	{
		// 避免data成员函数而出错，不知产生原因
		if( isNaN(i) ) continue;

		var t = data.list[i];
		name = link_name( t.npc, t.u, t.n );
	
		// 回复xxx
		var pre1 = '';
		// xxx说：
		var pre2 = '';
		//回复按钮的代码
		var select_parent_html = select_parent_html = String.format( " onclick='select_parent({0}, 0, 0, 0);return false;'" , i);
		if( t.pre )
		{
			var pre = t.pre;
			var pre_name = link_name( pre.npc, pre.u, pre.n );
			pre1 = String.format( "&nbsp;<span class='light_gray'>回复</span> {0}：", pre_name );
			pre2 = String.format( "<br><div class='reply_box2'>&nbsp;{1}&nbsp;{0}&nbsp;说：{2}</div>",
				pre_name, pre.t, pre.c );
					
			select_parent_html = String.format( " onclick='select_parent({0}, {1}, {2}, \"{3}\");return false;'" , i , pre.npc, pre.u, pre.n );
		}
		//回复列表
		html += String.format( "<div id='reply_{7}' class='reply_box{6}'{9}>\n\
		<div style='float:right;'>\n\
<a href='#' onclick='tab(\"user_detail{10}\",\"资料\",\"user_detail.php?uid={10}\");event.cancelBubble=true;return false;'>资料</a>\n\
<a href='#' onclick='tab(\"comment_lists{10}\",\"历史\",\"comment_lists.php?uid={10}\");event.cancelBubble=true;return false;'>历史</a>\n\
<a href='#' class='mini-button' style='height:25px;width:50px;' onclick='del_comment({8},{7});event.cancelBubble=true;return false;'>删除</a>&nbsp;\n\
<a href='#' class='mini-button' style='height:25px;width:50px;'{9}>回复</a></div>\n\
<div style='width:70%;'>&nbsp;{0}&nbsp;{1}<br>&nbsp;{4}{2}{5}&nbsp;</div></div><hr>",
			name, t.t, t.c, i, pre1, pre2, ((index%2==0)?'':' gray'), t.i, audit_id, select_parent_html, t.u );
			
		index++;
	};

	html += ignore;
	
	$('#replys').html( html );

	$('#time').val('+' + (5 + Math.floor(Math.random()*60)) );
	set_user('', '', '');
	$('#sub_parent').html('');
	$("#reply_content").val('');
	
	$( '#reply_'+list[audit_id].sub.i).removeClass('gray').addClass('yellow');
}

function answer()
{	
	var uid = mini.get('uid').getValue();
	var content = $("#reply_content").val();
	var time = $('#time').val();

	if( check_empty( parent, "请选择要回复的贴子"
		, content, "请输入内容"
		) ) return;
	
	var data = mini.encode({ id: sub_parent, uid: uid, content: content, time: time });

    post( "comment", "answer", data, answer2, faile);
}

function answer2( data )
{
	//alert( data.memo );
	
	var old = "#audit_" + audit_id;
	$( old ).next('hr').remove();
	$( old ).remove();
	$('#sub_parent').html('');

	if( mini.get("ignore_other").checked )
	{
		ignore_article( audit_id );
	}
	
	hideReplyWindow();
	
	// 显示第一条
	for( id in list )
	{
		view( id );
		break;
	}
}

function faile(jqXHR, textStatus, errorThrown)
{
	alert(jqXHR.responseText);
}

function set_user( uid, name, obj )
{
	if(obj == '' || obj == undefined) obj = 'uid';
	mini.get(obj).set( {value:uid, text: name} );
}

function select_parent( id , npc, uid, name )
{
	var item = article.list[id];
	sub_parent = item.i;
	var html = String.format( "<font color='red'>回复{0}：{1}</font>", item.n, item.c );
	$('#sub_parent').html( html );
	
	if(npc==1 && name!=item.n)
	{
		set_user( uid, name );
	}
	
	showReplyWindow();
}

function showReplyWindow() 
{
	var win = mini.get("win1");
	win.show();
	$("#reply_content").focus();
}

function hideReplyWindow() 
{
	var win = mini.get("win1");
	win.hide();
}

function del_comment( audit_id, comment_id )
{
	if( confirm("确定要删除这条回复吗？") )
	{
		var data = {id: audit_id, value: comment_id};
		post( "audit", "del_comment", data, function(){

			$('#audit_'+audit_id).next('hr').remove();
			$('#audit_'+audit_id).remove();	

			$('#reply_'+comment_id).next('hr').remove();
			$('#reply_'+comment_id).remove();		
			reload();		

		}, faile);
	}	
}

function queue_save()
{
	if(parent == 0) {alert('先选择主题');return false;}
	var data;
	var content = [];
	var time = [];
	var uid = [];
	$('#queue_box textarea').each(function(i,t){

		if( $(this).val() != '' )
		{
			content.push( $(this).val() ); 
			time.push( $('#q_time_'+i).val() );
			uid.push( mini.get('q_uid_'+i).getValue() );
			
			$(this).val('');
			$('#q_time_'+i).val('');
			mini.get('q_uid_'+i).setValue('');
		}
		
	});
	
	if(content.length > 0) 
	{
		data = mini.encode({ id: parent, uid: uid, content: content, time: time });
		post( "comment", "answer_queue", data, function(){
			queue_close();
			ignore_article( audit_id );
		}, faile);
	}
}

function del_queue(id)
{
	$('#queue_'+id).remove();
}

function queue_close()
{
	$('#queue_box').hide();
	$('.mini-splitter-pane2-button:eq(0)').click();
}

$("#reply_content").keyup(function(event){
	if( $("input[name='send'][@checked]")[0].checked )
	{
		if(!window.event.ctrlKey&&event.keyCode == 13)
		{
			var s = $("#reply_content").val();
			s = s.replace(/\n/g, "");
			$("#reply_content").val( s );
			answer();
		}
	}
	else
	{
		if(window.event.ctrlKey&&window.event.keyCode==13)
		answer();
	}
});

$(function(){

	get();
	
});
</script>