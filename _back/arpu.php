<?php
require 'head.php';

check_privilege(0);
check_privilege(83);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});
////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	
	var data={};
	<?php if( isset($_GET['user']) ){ ?>	
	data.user = '<?php echo $_GET['user'];?>';
	<?php }?>
	grid.load(data);

	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;	
	});
}

function search() {	
	var	time = mini.get("#time").getFormValue();	
    grid.load({ time: time });
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
					<!--	时间：<input id="time" name="time" class="mini-datepicker" value="" format="yyyy-MM-dd H:mm" showtime="true" style="width:150px;"/>
                        <a class="mini-button" onclick="search()">查询</a>-->
                    </td>
                </tr>
            </table>           
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
			<div field="mon" width="30" headerAlign="center" allowSort="true">年月</div>
            <div field="total" width="30" headerAlign="center" allowSort="true">充值</div>
			<div field="user_count" width="30" headerAlign="center" allowSort="true">用户数</div>
			<div field="pay_count" width="30" headerAlign="center" allowSort="true">充值数</div>
			<div field="arpu" width="30" headerAlign="center" allowSort="true">充值/用户数</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'arpu';}
</script>