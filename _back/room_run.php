<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(19); ?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();

	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;

		var html = "";
		switch( field )
		{
			case "action":
//				html = '<a href="room_match_history_detail.php?id='+e.record.id+'">查看</a>';
//				html += '&nbsp;<a href="javascript:void();" onclick=\'window.parent.showTab({id:"props' + record.id+'", text:"'+record.name+'持有情况", url: "user_props.php?props='+record.id+'"})\'>持有</a>';
				break;
	    }
		if( html != "" ) e.cellHtml = html;

	});
}

function search() {
    var q = mini.get("q").getValue();
    grid.load({ q: q });
}
function onKeyEnter(e) {
    search();
}

function add() {
	// 此函数最好在具体实现中覆盖，以实现默认值
    var newRow = {};
    grid.addRow(newRow, 0);
}
function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}
function truncate() {
    grid.loading("清空中，请稍候...");
	post( module(), "truncate", '', function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束

</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-add" onclick="add()" plain="true">增加</a>
                        <!--a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a-->
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
                        <input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:150px;" onenter="onKeyEnter"/>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;"
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"
        showEmptyText="true"
    >
        <div property="columns">
            <div field="id" width="10" headerAlign="center" allowSort="true">id</div>
			<div field="run" width="80" headerAlign="center" allowSort="true">执行时间</div>
        	<div field="type" width="100" headerAlign="center" allowSort="true">类型</div>
			<div field="no" width="30" headerAlign="center" allowSort="true">房间号</div>
            <div field="name" width="20" headerAlign="center" allowSort="true">用户</div>
        	<div field="name2" width="20" headerAlign="center" allowSort="true">用户2</div>
        	<div field="data" width="100" headerAlign="center" allowSort="true">数据</div>
			<div field="action" width="30" headerAlign="center" allowSort="false">操作</div>
        </div>
    </div>

	<div class="description">
        <h3>说明</h3>
        <ul>
			<li>正常情况下，本表数据会正常被收发。</li>
			<li>指令在执行后自动删除</li>
			<li>本表数据由程序维护，不宜人工增删。</li>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'room_run';}
</script>