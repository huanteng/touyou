<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(25);
?>
<style>
	body{padding-left: 0px;}
</style>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search&bet=500');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        field = e.field,
	        value = e.value;

		switch( field )
		{
			case "match":
				e.cellHtml = ( record.npc_1 != 1 ) ? record.name_1+' vs '+record.name_2 : record.name_2+' vs '+record.name_1;
				break;
	    
			case "action":
				//e.cellHtml = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"'+record.id+'", text:"'+record.name_1+' vs '+record.name_2+'", url: "../backend/get_pair.php?id='+record.id+'"})\'>查看</a> ';

				var uid = record._uid;
				if( record.must_win == 0 && (record.npc_1 == 1 || record.npc_2 == 1) ) e.cellHtml = '<a href="#" onclick="must_win(' + uid + ')">设npc赢</a>';
				break;
		}

	});
	
	setTimeout(init, 20000);//20秒
}

function must_win( _uid )
{
	var row = grid.getRowByUID(_uid);

	var data = row.id;

	post( 'vs', "must_win", data, function (text) {
		if( text.code < 0 )
		{
			alert(text.memo);
			return;
		}
		else
			alert(text.memo);
		
		var value = 0;
		if( row.npc_1 == 1 ) value = 1;
		if( row.npc_2 == 1 ) value = 2;

		grid.updateRow(row, {
                must_win: value
            });

		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    
    <div id="datagrid1" class="mini-datagrid" style="width:100%;height:100%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div field="match" width="60" headerAlign="center" allowSort="true">比赛</div>
        	<div field="bet" width="20" headerAlign="center" allowSort="true">赛金</div>
        	<div field="action" width="20" headerAlign="center" allowSort="true">必赢</div>
        </div>
    </div>

<script type="text/javascript">
function module() { return 'vs';}
$(function(){
	$('.mini-pager').html('&nbsp;&nbsp;<a href="#" onclick="grid.reload();return false;">刷新</a>')
});
</script>