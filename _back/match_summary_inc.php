<!--//挑战窗口-->
<div id="win1" class="mini-window" title="发起挑战" style="width:500px;height:300px;"
	 showToolbar="true" showFooter="true" showModal="false" allowDrag="true"
	>

	接收人：<input id="uid2" class="mini-textbox"/><span id="receiver"></span><br />
	次数：<input id="challenge_count" class="mini-textbox" value="1"/><br>
	性别：<input id="sex" class="mini-radiobuttonlist" repeatLayout="none"
			textField="text" valueField="id" value="2" data="[{id:'0',text:'男'},{id:'1',text:'女'},{id:'2',text:'随机'}]" />
	或<?php autocomplete_name('sender_uid') ;?><br>
	类型：<input id="props_id" class="mini-radiobuttonlist" repeatLayout="none"
			textField="text" valueField="id" value="1" data="[{id:'1',text:'金币'},{id:'2',text:'元宝'},{id:'40',text:'宝石'}]" />
	挑战金：<input id="count1" class="mini-textbox" value="100"/> ~ <input id="count2" class="mini-textbox" value="100"/><br>
	干预率：<input id="rate" class="mini-textbox" value="0" maxLength="3"/>(发起挑战时同时调整干预率，0表示不调整)<br>

	<a class="mini-button" onclick="send()">确定</a>
	<a href='#' onclick="hide()">取消</a>
	</div>
	<!--//挑战窗口结束-->

<script type="text/javascript">

function show()
{
	var row = grid.getSelected();
	$('#receiver').html( row.name );
	mini.get('uid2').setValue( row.id );
	mini.get("win1").show();
}
function hide()
{
	mini.get("win1").hide();
}
function send()
{
	var data = {};
	var setit = function(n){ data[n] = mini.get(n).value; };

	data.uid = mini.get('sender_uid').value;
	setit( 'uid2' );
	setit( 'challenge_count' );
	setit( 'sex' );
	setit( 'props_id' );
	setit( 'count1' );
	setit( 'count2' );
	setit( 'rate' );

	var func = function( data ) {
		alert( data.memo );
		hide();
	};

	post( 'match_summary', 'send', data, func, hide );
}
</script>