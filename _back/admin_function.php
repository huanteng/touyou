<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(2); ?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div type="checkcolumn" width="10"></div>
            <div field="id" width="10" headerAlign="center" allowSort="true">id
            </div>
      		<div type="comboboxcolumn" field="parent_id" width="50" headerAlign="center" allowSort="true" >父功能
                <input property="editor" class="mini-combobox" style="width:100%;" textField="name" valueField="id" url="data/admin_function.php?method=menu"/>
            </div>  
            <div field="name" width="50" headerAlign="center" allowSort="true">功能名称
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>                      
            <div field="url" width="100" headerAlign="center" allowSort="true">地址
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>                      
            <!--div field="order" width="120" headerAlign="center" allowSort="true">排序
      			<input property="editor" class="mini-spinner" style="width:100%;"  minValue="0" maxValue="250" />
            </div-->                      
            <div field="user" width="200" displayField="user_name" name="user" width="120" headerAlign="center" >权限
                <input property="editor" class="mini-buttonedit" onbuttonclick="onButtonEdit" style="width:100%;"/>
            </div>
            <div type="checkboxcolumn" field="is_view" trueValue="1" falseValue="0" width="15" headerAlign="center">显示</div>
            <!--div type="checkboxcolumn" field="is_new_window" trueValue="1" falseValue="0" width="60" headerAlign="center">新窗口</div-->
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        	<li>建议暂时只是二层树。</li>
        	<li>顺序号越小，越排前。</li>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'admin_function';}

function onButtonEdit(e) {
	var row = grid.getSelected();
	var data = {id: row.user, text: row.user_name};
	var fn = function (data) {
			grid.cancelEdit();
            var row = grid.getSelected();
            grid.updateRow(row, {
                user: data.id,
                user_name: data.text
            });
		};
    
        mini.open({
        url: "sel_administrator.php",
        title: "选择用户",
        width: 650,
        height: 380,
        onload: function () {
            var iframe = this.getIFrameEl();
            iframe.contentWindow.SetData(data);
        },
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();

                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);

                fn(data);

            }
        }
    });
}
</script>
