<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(13); ?>

<a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
<ul id="tree1" class="mini-tree" style="width:200px;padding:5px;"
    showTreeIcon="true" textField="name" idField="id" parentField="parent_id" resultAsTree="false"
    allowDrag="true" allowDrop="true" showCheckBox="true"
	url="data/administrator.php?method=privilege&id=<?php echo $_GET['id'];?>"
    >
</ul>

<script type="text/javascript">

    function save() {
        var tree = mini.get("tree1");

		var data = { id:<?php echo $_GET['id'];?>, value: tree.getValue(true)}
		
		post( module(), "save_privilege", data, function (text) {
			alert("更新成功");
			reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
    }
</script>


<div class="description">
    <h3>说明</h3>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'administrator';}

</script>
