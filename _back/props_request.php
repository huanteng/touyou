<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(63);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
	    if (field == "type") {
	        e.cellHtml = (value == '0') ? '索要' : '赠送';
	    }
	    
	    if (field == "npc") {
	        e.cellHtml = (value == '1') ? '√' : '';
	    }
	    
	    if (field == "target_npc") {
	        e.cellHtml = (value == '1') ? '√' : '';
	    }
	});
}

function search() {
    var type = $("#type").val();
    
    var uid = mini.get("uid").getValue();
    var target_uid = mini.get("target_uid").getValue();
    
    grid.load({ type:type,uid:uid,target_uid:target_uid });
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
    					<a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
    
    					类型：<select id="type"><option value=''></option><option value='0'>索要</option><option value='1'>赠送</option></select>
    					操作用户：<input id="uid"  class="mini-buttonedit" onbuttonclick="onButtonEdit" allowInput="false" />
    					目标用户：<input id="target_uid"  class="mini-buttonedit" onbuttonclick="onButtonEdit" allowInput="false" />
    					
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
        	<div type="checkcolumn"></div>
            <div field="id" width="30" headerAlign="center" allowSort="true">id</div>
        	<div field="type" width="30" headerAlign="center" allowSort="true">类型</div>
            <div field="uid" displayField="name" width="30" headerAlign="center" allowSort="true">操作用户</div>
        	<div field="npc" width="5" headerAlign="center" allowSort="true">NPC</div>
            <div field="target_uid" displayField="target_name" width="30" headerAlign="center" allowSort="true">目标用户</div>
        	<div field="target_npc" width="5" headerAlign="center" allowSort="true">NPC</div>
        	<div field="time" width="50" headerAlign="center" allowSort="true">时间
            </div>	
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
	user_context_column = { uid: 'uid', target_uid: 'target_uid' };
function module() { return 'props_request';}

function onButtonEdit(e) {
    var btnEdit = this;

    mini.open({
        url: bootPATH + "../sel_user.php",                          
        title: "用户列表",
        width: 650,
        height: 380,
        ondestroy: function (action) {
          
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);
                
                btnEdit.setValue(data.id);
                btnEdit.setText(data.text);
            }
        }
    });            
     
}
</script>