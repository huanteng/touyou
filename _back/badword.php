<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(33); ?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:75%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div type="checkcolumn"></div>
            <div field="id" width="120" headerAlign="center" allowSort="true">id
            </div>
            <div field="old" width="120" headerAlign="center" allowSort="true">违禁
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
            <div field="new" width="120" headerAlign="center" allowSort="true">替换
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>	
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'badword';}
</script>