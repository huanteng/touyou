<?php
require 'top.php';

check_privilege(0);
check_privilege(83);
?>
  
    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="12" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
			<div field="month" width="30" headerAlign="center" allowSort="false">时间</div>
			<div field="register_count" width="30" headerAlign="center" allowSort="false">注册人数</div>
			<div field="mobile_register_count" width="30" headerAlign="center" allowSort="false">手机数</div>
			<div field="pay_first_count" width="30" headerAlign="center" allowSort="false">初充人数</div>
			<div field="pay_first_sum" width="30" headerAlign="center" allowSort="false">初充总数</div>
			<div field="pay_count" width="30" headerAlign="center" allowSort="false">充值人数</div>
			<div field="pay_sum" width="30" headerAlign="center" allowSort="false">充值总数</div>
			<div field="pay_first_percent" width="30" headerAlign="center" allowSort="false">初充比例<br/>(初次充值人数/充值人数)</div>
			<div field="pay_register_percent" width="30" headerAlign="center" allowSort="false">转化参数<br/>(初次充值人数/注册人数)</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'payment';}

$('#toolbar .mini-button:lt(3),#toolbar .separator,#q,#search').hide();
//$('#search').prepend('日期：<input id="date" class="mini-datepicker" value="" format="yyyy-MM-dd" style="width:100px;"/>');

function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=report_month');
	var data={};
	<?php
	$s = '';
	foreach( $_GET as $k => $v )
	{
		$s .= "data.{$k} = '{$v}';\n";
	}
	echo $s;
	?>
	grid.load(data);
	
	grid.on( "drawcell", onDrawcell );
}

function search() {
	var date = mini.get("date").getFormValue();
    grid.load({ date: date });
}

function onDrawcell(e) {
    var r = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	
	var html = "";
	switch( field )
	{
		case 'total':
			html = parseInt(r.challenge) + parseInt(r.integral) + parseInt(r.lucky) + parseInt(r.task)
				+ parseInt(r.props) + parseInt(r.pay) + parseInt(r.other) - parseInt(r.challenge2)
				- parseInt(r.integral2) - parseInt(r.props2) - parseInt(r.other2);
			break;
	}
	if( html != "" ) e.cellHtml = html;
}
</script>