<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(18); ?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:75%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
        	<div width="10" type="checkcolumn"></div>
            <div field="id" width="30" headerAlign="center" allowSort="false">id</div>
            <div field="name" width="50" headerAlign="center" allowSort="false">名字</div>
            <div field="register_imei" width="50" headerAlign="center" allowSort="false">注册IMEI</div>
            <div field="login_imei" width="50" headerAlign="center" allowSort="false">登录IMEI</div>
        	<div field="register" width="50" headerAlign="center" allowSort="false">注册时间</div>
        	<div field="login" width="50" headerAlign="center" allowSort="false">登录时间</div>
        	<div field="logout" width="50" headerAlign="center" allowSort="false">退出时间</div>
        	<div field="platform" width="50" headerAlign="center" allowSort="false">手机</div>	
			<div field="is_real" width="50" headerAlign="center" allowSort="false">视频认证</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'user';}

$(function(){
	$('#toolbar .mini-button:lt(1),#toolbar .separator').hide();
	$('#search').remove();
});

function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=puppet');
	var data={};
	<?php
	$s = '';
	foreach( $_GET as $k => $v )
	{
		$s .= "data.{$k} = '{$v}';\n";
	}
	echo $s;
	?>
	grid.load(data);

	grid.on( "drawcell", onDrawcell );
}

function onDrawcell (e) 
{
	var record = e.record,
	column = e.column,
	field = e.field,
	value = e.value;

	var html = "";
	switch( field )
	{
		case "name":
			html = '<a href="javascript:tab(\'user_detail\',\''+record.id+'\',\'user_detail.php?uid='+record.id+'\');">'+record.name+'</a>';
			break;
		case "is_real":
			html = (record.is_real == 1) ? '已认证' : '';
			break;	
	}

	if( html != "" ) e.cellHtml = html;
}
</script>