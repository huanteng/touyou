<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(29); ?>
<?php $module = 'audit'; ?>
<style>
	.container {
		/*background: #FFF;*/
		padding: 5px;
		margin-bottom: 20px;
		border-radius: 5px;
		clear: both;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}

	.centered { margin: 0 auto; }

	.box {
		margin: 5px;
		padding: 5px;
		background: #D8D5D2;
		font-size: 11px;
		line-height: 1.4em;
		float: left;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}

	.box2 {
		margin: 5px;
		padding: 5px;
		background: #ffff80;
		font-size: 11px;
		line-height: 1.4em;
		float: left;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}

	.box h2 {
		font-size: 14px;
		font-weight: 200;
	}

	.box img,
	#tumblelog img {
		display: block;
		width: 100%;
	}

	.rtl .box {
		float: right;
		text-align: right; 
		direction: rtl;
	}

	.col1 { width: 80px; }
	.col2 { width: 180px; }
	.col3 { width: 280px; }
	.col4 { width: 380px; }
	.col5 { width: 480px; }
	.col_s { width: 120px; }

	.col1 img { max-width: 80px; border: 0px; }
	.col2 img { max-width: 180px; border: 0px; }
	.col3 img { max-width: 280px; border: 0px; }
	.col4 img { max-width: 380px; border: 0px; }
	.col5 img { max-width: 480px; border: 0px; }
	.col_s img { max-width: 120px; border: 0px; }
</style>

<div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
	 url="data/<?php echo $module; ?>.php?method=search" idField="user"
	 allowResize="true" pageSize="10" allowAlternating="true"
	 allowCellSelect="true" multiSelect="true"       
	 showEmptyText="true"    
	 >
	<div property="columns">      
		<div field="user" width="50" headerAlign="center" allowSort="false">用户</div>
		<div field="audit" width="60" headerAlign="center" allowSort="false">待审核</div> 
		<div field="other_list" width="250" headerAlign="center" allowSort="false">个人照片</div>
		<div field="action" width="80" headerAlign="center" allowSort="false">操作</div>	
	</div>
</div>

<div class="description">
	<h3>说明</h3>
	<ul>
		<li>视频认证：通过将会设置此人为真实经过验证的用户，不通过此照片将会放进其它照片的相册</li>
		<li>视频认证通过时右边的图片：默认已经在个人照片的相册；勾选放进个人照片相册，不勾选放进其它照片的相册</li>
		<li>个人照片: 通过放进个人照片相册，不通过此照片将会放进其它照片的相册</li>
	</ul>
</div>

<?php require 'bottom.php'; ?>
<script src="js/jquery.masonry.min.js"></script>
<script src="js/jquery.rotate.js"></script>
<script type="text/javascript">
	//////覆盖
	function module() { return '<?php echo $module; ?>';}
	//////覆盖结束

	$(function(){
		$('#toolbar .mini-button:lt(3),#toolbar .separator').hide();
		$('#search').remove();
	});

	function init()
	{
		grid = mini.get("datagrid1");
		grid.setUrl('data/' + module() +'.php?method=video_check');
		var data={};
		<?php
		$s = '';
		foreach ($_GET as $k => $v) {
			$s .= "data.{$k} = '{$v}';\n";
		}
		echo $s;
		?>
		grid.load(data);

		grid.on( "drawcell", onDrawcell );
		
		setTimeout('init()',300000);
	}

	function onDrawcell (e) 
	{
		var record = e.record,
		column = e.column,
		field = e.field,
		value = e.value;

		var html = "";
		switch( field )
		{
			case "user":
				html = '<img src="'+record.logo+'" style="width:120px;height:120px;border:0px;"/>';
				html +='<br/>&nbsp;<a href="javascript:tab(\'user_detail\',\''+record.user+'\',\'user_detail.php?uid='+record.uid+'\');">'+record.user+'</a>&nbsp;'+((record.sex !='' && record.sex==0)?'男':'女')+'<br/>'
				break;
			
			case "audit":
				html = '<div class="box2 photo col_s"><a href="'+record.album_path+'" target="_blank"><img src="'+record.album_path+'.s.jpg" style="width:120px;height:120px;border:0px;" id="img_'+record.id+'"/></a>';
				html += '<br/>&nbsp;'+((record.album_type==2)?'视频认证':'个人照片');	
				html += '&nbsp;<a href="javascript:$(\'#img_'+record.id+'\').rotateLeft();"><img src="../_back/images/left.jpg" style="vertical-align:middle;width:20px;height:20px;"></a>&nbsp;<a href="javascript:$(\'#img_'+record.id+'\').rotateRight();"><img src="../_back/images/right.jpg" style="vertical-align:middle;width:20px;height:20px;"></a>';
				html += '&nbsp;<a href="javascript:void(0);" onclick="del('+record.id+')"  class="mini-button">删除</a></div>';
				break;
				
			case "other_list":
				var img_html='';
				$(record.other_list).each(function(i,n){
                    
					img_html += '<div class="box photo col_s" id="img_'+n.id+'">'
					img_html += '<a href="'+n.path+'" target="_blank">';
					img_html += '<img src="'+n.path+'.s.jpg"/>';
					img_html += '</a>';
					
					if(record.album_type==1 && i==0) img_html += '视频认证照片';
					else if(record.album_type==1 && i>0) img_html += '个人照片';
					
					//暂时实现不了勾选，先改成手动单击设置
					//if(record.album_type==2) img_html += '<input id="box_'+n.id+'" value="'+n.id+'" name="box_'+n.id+'" type="checkbox"/><label for="box_'+n.id+'">移到其它照片</label>';
					if(record.album_type==2) img_html += '<a href="#" class="mini-button" onclick="move_pic('+n.id+');return false;">移到其它照片</a>';	

					img_html += '</div>';
				});
				html += '<div class="container" id="con_'+record.id+'">'+img_html+'</div>';
				break;
				
			case "action": 
				if(record.album_type==2){//视频认证
					html += '&nbsp;<a href="javascript:void();" onclick="pass(\''+record.id+'\',2)" class="mini-button" style="width:50px; height: 25px;"> 通过 </a>&nbsp;&nbsp;<a href="javascript:void();" onclick="donotpass(\''+record.id+'\',2)" class="mini-button" style="width:50px; height: 25px;"> 不通过 </a>';
					html += ((record.puppet_total>0)?'<br/><br/>&nbsp;<a href="#" onclick="tab(\'puppet\',\'疑似马甲\',\'puppet.php?uid='+record.uid+'\');return false;">疑似马甲( '+record.puppet_total+' )</a>&nbsp;已有 '+record.puppet_real_count+' 个疑似马甲通过视频认证':'');
				}
				if(record.album_type==1)
					html += '&nbsp;<a href="javascript:void();" onclick="pass(\''+record.id+'\',1)" class="mini-button" style="width:70px; height: 25px;"> 移到个人相册 </a>&nbsp;&nbsp;<a href="javascript:void();" onclick="donotpass(\''+record.id+'\',1)" class="mini-button" style="width:70px; height: 25px;"> 移到其它相册 </a>';
				break;
			}

			if( html != "" ) e.cellHtml = html;

	}

	function del(id)
	{
		if(confirm("确定删除这张照片吗？"))
		{
			var row = grid.findRow(function(row){
				if(row.id == id) return true;
			});
			grid.removeRow ( row, false );
			
			$.ajax({
				url: "data/audit.php?method=del_pic&id="+id,
				dataType: "json",
				success: function (data) {			
//					reload();
				}
			});	
		}
	}	

	function pass(id,type)
	{
		if(type==1) msg = '确定移到个人照片吗？';
		else msg = '确定通过视频认证吗？\r\n条件：\r\n1、三张个人照片（不一样的背景）;\r\n2、头像与视频人像一致;\r\n3、照片不能裸露，要雅观;\r\n4、要本人视频（性别相符、必须是人像）;\r\n5、视频清晰，面相没有遮挡。';

		if(confirm(msg))
		{
			var row = grid.findRow(function(row){
				if(row.id == id) return true;
			});
			grid.removeRow ( row, false );

			$.ajax({
				url: "data/audit.php?method=pass&id="+id,
				dataType: "json",
				success: function (data) {			
					//reload();
				}
			});	
		}
	}

	function donotpass(id,type)
	{
		if(type==1) msg = '确定移到其它照片吗？';
		else msg = '确定不通过视频认证吗？';

		if(confirm(msg))
		{
			var row = grid.findRow(function(row){
				if(row.id == id) return true;
			});
			grid.removeRow ( row, false );
			
			$.ajax({
				url: "data/audit.php?method=donotpass&id="+id,
				dataType: "json",
				success: function (data) {			
					//reload();
				}
			});	
		}
	}

	function move_pic(id)
	{
		if(confirm("确定移动这张照片到其它照片相册吗？"))
		{
			$.ajax({
				url: "data/album.php?method=set_type&type=0&id="+id,
				dataType: "json",
				success: function (data) {			
					$('#img_'+id).remove();
				}
			});	
		}
	}
</script>
