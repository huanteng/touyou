<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(22); ?>

    <div style="width:98%;">

		<div id="form1">
			<table>
				<tr>
					<td><label for="username$text">发送人：</label></td>
					<td>
						<div id="uid" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id" 
					        url="data/user.php?method=search&sortField=name&sortOrder=asc" value="850" text="骰妖子"  onvaluechanged="onValueChanged" required="true" requiredErrorText="不能为空">     
					        <div property="columns">
					            <div header="名字" field="name" width="30"></div>
								<div header="NPC" field="npc" width="10"></div>
								<div header="登录" field="login" width="25"></div>
					        </div>
					    </div>	
						<a href="#" onclick="mini.get('uid').setValue('156118');mini.get('uid').setText('系统');return false;">系统</a>&nbsp;&nbsp;<a href="#" onclick="mini.get('uid').setValue('850');mini.get('uid').setText('骰妖子');return false;">骰妖子</a>
					</td>    
				</tr>
				<tr>
					<td><label for="pwd$text">接收人范围：</label></td>
					<td>
						<div id="type" class="mini-radiobuttonlist" repeatItems="3" repeatLayout="table" repeatDirection="vertical"
							textField="text" valueField="id" value="0" style="width:150px;"
							data="[{id:'0',text:'所有人'},{id:'1',text:'所有在线'},{id:'2',text:'单个用户'}]" required="true" requiredErrorText="不能为空">
						</div> 
					</td>
				</tr>
				<tr style="display:none;" id="target_tr">
					<td><label for="pwd$text">接收人：</label></td>
					<td>					 
						<div id="target_uid" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id" 
					        url="data/user.php?method=search&sortField=name&sortOrder=asc" value="" text=""  onvaluechanged="onValueChanged" required="true" requiredErrorText="不能为空">     
					        <div property="columns">
					            <div header="名字" field="name" width="30"></div>
								<div header="NPC" field="npc" width="10"></div>
								<div header="登录" field="login" width="25"></div>
					        </div>
					    </div>	
					</td>
				</tr>				
				<tr>
					<td><label for="size">分批发送，每批人数：</label></td>
					<td>
						<input id="size" class="mini-Textbox" style="width:150px;" required="true" requiredErrorText="不能为空" value="100"/>	
					</td>
				</tr>
				<tr>
					<td><label for="count">内容：</label></td>
					<td>
						<input id="content" class="mini-TextArea" style="width:350px;height:200px;" required="true" requiredErrorText="不能为空" value=""/>	
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input value="发送" type="button" onclick="submitForm()" />
					</td>
				</tr>
				<tr>
					<td>进度：</td>
					<td><span id="status"></span></td>
				</tr>
			</table>
		</div>
		
		
	</div>
  	  
    <div class="description">
        <h3>说明</h3>
		<ul>
			<li>点击发送后，页面要保持打开，不能关闭。直到提示发送完毕。</li>
		</ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
	mini.parse();

	var last_uid = 0;
	function submitForm() {
		var form = new mini.Form("#form1");

		form.validate();
		if (form.isValid() == false) return;

		//提交数据
		var uid = mini.get("#uid").getValue();
		var target_uid = mini.get("#target_uid").getValue();
		var content = mini.get("#content").getValue();
		var type = mini.get("#type").getValue();
		var size = mini.get("#size").getValue();
		
		$.ajax({
			url: "data/message.php?method=send",
			type: "post",
			data: { uid: uid, target_uid: target_uid, type: type, last_uid:last_uid, size: size, content: content },
			dataType: "json",
			success: function (text) {
				if( text.code == 1 )
				{
					alert(text.memo);
				}
				else
				{
					$('#status').html( '当前：' + text.last_uid );
					last_uid = text.last_uid;
					setTimeout('submitForm()',0);
				}	
			}
		});
	}
	
	$(function(){
	
		$('#type').click(function(){
			
			if( mini.get("#type").getValue() == 2)
			{
				$('#target_tr').show();
			}
			else
			{
				$('#target_tr').hide();
			}
			
		});
	
	});
</script>