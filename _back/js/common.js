window.alert = function(str) {
	if( str.indexOf("试用到期") < 0 ) mini.alert(str, '提示');
	else return;
}

// 字符串操作
String.format = function(src){
    if (arguments.length == 0) return null;    
    var args = Array.prototype.slice.call(arguments, 1);    
    return src.replace(/\{(\d+)\}/g, function(m, i){    
          return args[i];    
   });    
};


/* 使用ajax方式post，将json结果交回调函数处理
 * file：接受post的文件名
 * data：postdata，为key=value数组
 * success, error：回调函数
 */
function post(module, method, data, success, error)
{
	var url = 'data/' + module + '.php';
	$.ajax
	({
		type : "POST",
		url : url,
		dataType : 'json',
		async : false,
		data : {method: method, data: data},
		success : function(data) {success(data);},
		error : function (jqXHR, textStatus, errorThrown) {error (jqXHR, textStatus, errorThrown);}
	});
	
}

function init_form( form_name, module, method, data, success )
{
	var form = new mini.Form( form_name );
	form.loading();
	post( module, method, data, function (data) {
		form.setData(data);
		form.unmask();
		if(success) success(data);
		},function () {}
		);
}

/*
 *在datagrid.buttonedit中，新窗口打开网页，选择用户
 *调用方法参考 config.php
 *参数：
 * data：初始化数据，json类型，用于新窗口初始化，默认值为null
 * fn：回调函数，一般用于取值处理，默认值为grid.cancelEdit();
                var row = grid.getSelected();
                grid.updateRow(row, {
                    user: data.id,
                    name: data.text
                });
 */
function sel_user(data, fn) {
//    var btnEdit1 = e.sender;

    mini.open({
        url: "sel_user.php",
        title: "选择用户",
        width: 650,
        height: 380,
        onload: function () {
            var iframe = this.getIFrameEl();
            iframe.contentWindow.SetData(data);
        },
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();

                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);

                fn(data);

            }
        }
    });
}

/* 新开tab
 * 参数：
 *	id
 *	text
 *	url
 */
function tab( id, text, url )
{
	if( window.parent.showTab )
	{
		if( id == '' ) id = new Date();
		text = text.replace( '"', '' );
		window.parent.showTab({id:id, text:text, url: url});
	}
	else
	{
		open( url );
	}
}

/* return true if any empty
 * 使用示例
 var chat = $("#users option[@selected]").val();
	var content = $("#reply_content").val();
	var time = $("#delay").val();

	if( check_empty( chat, "请从左边选择对话"
		, content, "请输入内容"
		, time, "请输入延时时间"
		) ) return;
 */
function check_empty()
{
	for (var i = 0; i < arguments.length; i=i+2)
	{
		if(arguments[i]=="")
		{
			alert(arguments[i+1]);
			return true;
		}
	}
	return false;
}

/* 简化查找代码，以便于子页调用
 * 使用方法，searcy_by_id( 'uid,q,name' )
 */
function search_by_id( id ) {
	var data = {};

	for( var key in id )
	{
		data[ key ] = mini.get( id[key] ).getValue();
	}
	grid.load( data );
}