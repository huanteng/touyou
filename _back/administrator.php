<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(3); ?>
	
	<?php $module = 'administrator'; ?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        url="data/administrator.php?method=search" idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >    
        <div property="columns">
            <div type="checkcolumn"></div>
            <div field="id" width="120" headerAlign="center" allowSort="true">id</div>
            <div field="name" width="120" headerAlign="center" allowSort="true">帐号
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>                      
            <div field="pass" width="120" headerAlign="center" allowSort="true">密码
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>                      
            <div field="last" width="120" headerAlign="center">登录</div>
            <div type="checkboxcolumn" field="status" trueValue="0" falseValue="1" width="60" headerAlign="center">状态</div>
			<div field="action" width="30" headerAlign="center">操作</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return '<?php echo $module; ?>';}

function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
		if (field == "action") {
	        e.cellHtml = '<a href="administrator_privilege.php?id='+e.record.id+'" target="_black">权限</a>';
	    }

	});
}
</script>
