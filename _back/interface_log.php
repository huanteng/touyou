<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(62); ?>
<?php
require '../log.conf.php';

function e1( $i, $j, $k )
{
	global $log;
	if( isset($log[$i]) && isset($log[$i][$j]) && isset($log[$i][$j][$k])) echo $log[$i][$j][$k];
}

function e2( $i, $j, $k )
{
	global $log;
	if( isset($log[$i]) && isset($log[$i][$j]) && isset($log[$i][$j][$k])) echo 'Y';
}
?>

设置：<a href="void()" onclick="$('#form1').toggle();return false;">接口</a> <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a><br>
<a href="javascript:start();">开始</a>
<a href="javascript:stop();">停止</a>
<a href="javascript:once();">执行一次</a> <div id="count"></div>

<div id="form1" style="visi">
	<input name="interface" class="mini-checkbox" text="记录接口数据(尽量加用户筛选条件，否则将产生海量数据，系统受不了)" value="<?php if( isset($log['interface'] ) ) echo 'Y'; ?>" trueValue="Y" falseValue="" />，
	<br>条件如下：用户<input name="interface_key_uid" emptyText="请选择" value="<?php e1('interface','key','uid'); ?>" text="<?php e1('interface','key','uid'); ?>" class="mini-buttonedit" onbuttonclick="onButtonEdit" allowInput="true" />
	地址<input name="interface_key_url" class="mini-textbox" value="<?php e1('interface','key','url'); ?>">
	postdata<input name="interface_key_input" class="mini-textbox" value="<?php e1('interface','key','input'); ?>">
	内容<input name="interface_key_response" class="mini-textbox" value="<?php e1('interface','key','response'); ?>">
	SQL<input name="interface_key_sql" class="mini-textbox" value="<?php e1('interface','key','sql'); ?>">(多条件用半角逗号隔开)<br>
	　　　　　　　　　　数据：
	<input name="interface_save_input" class="mini-checkbox" text="postdata" value="<?php e2('interface','save','input'); ?>" trueValue="Y" falseValue="" />　
	<input name="interface_save_response" class="mini-checkbox" text="内容" value="<?php e2('interface','save','response'); ?>" trueValue="Y" falseValue="" />　
	<input name="interface_save_sql" class="mini-checkbox" text="SQL" value="<?php e2('interface','save','sql'); ?>" trueValue="Y" falseValue="" />
	<input name="interface_key_heart" class="mini-checkbox" text="包含心跳信息" value="<?php e2('interface','key','heart'); ?>" trueValue="Y" falseValue="" />

	<!--br><input name="error" class="mini-checkbox" text="记录错误数据（如不选中，则当错误时，记录在/error.log文件中" value="<?php if( isset( $log['error'] ) ) echo 'Y'; ?>" trueValue="Y" falseValue="" />，-->

	<br><input name="back" class="mini-checkbox" text="记录后台数据" value="<?php if( isset( $log['back'] ) ) echo 'Y'; ?>" trueValue="Y" falseValue="" />，
	条件如下：管理员<input name="back_key_uid" class="mini-textbox" emptyText="请选择" value="<?php e1('back','key','uid'); ?>" text="<?php e1('back','key','username'); ?>" allowInput="true" />
	地址<input name="back_key_url" class="mini-textbox" value="<?php e1('back','key','url'); ?>">
	postdata<input name="back_key_input" class="mini-textbox" value="<?php e1('back','key','input'); ?>">
	内容<input name="back_key_response" class="mini-textbox" value="<?php e1('back','key','response'); ?>">
	SQL<input name="back_key_sql" class="mini-textbox" value="<?php e1('back','key','sql'); ?>">(多条件用半角逗号隔开)<br>
	　　　　　　　　　　数据：
	<input name="back_save_input" class="mini-checkbox" text="postdata" value="<?php e2('back','save','input'); ?>" trueValue="Y" falseValue="" />　
	<input name="back_save_response" class="mini-checkbox" text="内容" value="<?php e2('back','save','response'); ?>" trueValue="Y" falseValue="" />　
	<input name="back_save_sql" class="mini-checkbox" text="SQL" value="<?php e2('back','save','sql'); ?>" trueValue="Y" falseValue="" />


</div>
<div id='body'></div>  	  

<?php require 'bottom.php'; ?>

<script type="text/javascript">
//////覆盖
function module() { return 'interface_log';}

//////覆盖结束

var is_stop = false;
var count = 0;

function get()
{
	if( is_stop ) return;

	$("#count").text("执行次数：" + count + "，运行久后，有些浏览器会变慢。如有必要请刷新。");
	++count;
	
	post( module(), "get", '', 
		function (data) 
		{
			var s = '';
			var l = data.length;

			for( var i = 0;i < l; ++i )
			{
				var d;
				d = data[i];
				s = '';
				if( d.uid != 0 ) s += String.format('<li>{0}[{1}]</li>', d.name, d.uid);
				if( d.input != '' )
				{	
					var t = d.input.substr(11);
					t = t.substr(0, t.length-4);
					t = t.replace(/' => '/g, '=').replace(/',\s+'/g, '&');
					s += String.format('<li>input:{0}</li>', t);
				}
				if( d.response != '' ) s += String.format('<li>response:{0}</li>', d.response);
				if( d.sql != '' ) s += String.format('<li>sql:<pre>{0}</pre></li>', d.sql);
				s = String.format('<div class="description"><h3>{0},{1}</h3><ul>{2}<li><a href="{1}" target="t">重新访问1</a>  <a href="{1}?{3}" target="t">重新访问2</a></li></ul></div>', d.time, d.url, s,t);
				$("#body").prepend(s);
			}
        },function (jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			}
			);
	setTimeout(get, 2000);
}
function start()
{
	is_stop = false;
	get();
}

function stop()
{
	is_stop = true;
}

function once()
{
	$('#body').html('');
	is_stop = false;
	get();
	is_stop = true;
}

$('#body').dblclick(function(){
	$(this).html('');
})

function onButtonEdit(e) {
    var btnEdit = this;

    mini.open({
        url: bootPATH + "../sel_user.php",
        title: "用户列表",
        width: 650,
        height: 380,
        ondestroy: function (action) {

            if (action == "ok") {
                var iframe = this.getIFrameEl();

                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);

                btnEdit.setValue(data.id);
                btnEdit.setText(data.text);
            }
        }
    });
}

function save()
{
	var form = new mini.Form("#form1");
	var data = form.getData();      //获取表单多个控件的数据
	var json = mini.encode(data);   //序列化成JSON

	post( module(), "save", json,
		function (data)
		{
			alert(data.message);
        },function (jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			}
		);
}

</script>
