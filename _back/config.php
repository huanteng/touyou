<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(7); ?>
	
	<?php $module = 'config'; ?>
  
    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="user"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div type="checkcolumn"></div>
        	<div field="user" displayField="name" name="user" width="100" headerAlign="left" >用户
                <input property="editor" class="mini-buttonedit" onbuttonclick="onButtonEdit" style="width:100%;"/>
            </div>
            <div field="section" width="100" allowSort="true">Section
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>    
            <div field="key" width="120" headerAlign="center" allowSort="true">Key
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>                      
            <div field="value" width="160" headerAlign="center" allowSort="true">Value
        		<input property="editor" class="mini-textarea" style="width:100%;" minHeight="80"/>
            </div>
            <div type="checkboxcolumn" field="is_send" trueValue="1" falseValue="0" width="20" headerAlign="center">已发</div>
            <div type="checkboxcolumn" field="is_json" trueValue="1" falseValue="0" width="20" headerAlign="center">json</div>
            <div field="memo" width="100" headerAlign="center" allowSort="false">备注
        		<input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
            <div field="time" width="120" headerAlign="center" allowSort="true" dateFormat="MM-dd HH:NN">更新时间</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        	<li>【快速设置】<b>接收人：</b><?php foreach( array('全部' => '0', 'dda' => '237', 'decade' => '226') as $key => $value ) {?><a href="javascript:user('<?php echo $key ?>', '<?php echo $value ?>')"><?php echo $key ?></a> <?php } ?>，<b>接口：</b><a href="javascript:quick('product')">产品机</a> <a href="javascript:quick('test')">测试机</a></li>
        	<li>当<b>Value</b>为空时，表示删除本设置值。</li>
        	<li>当<b>用户</b>为空时，表示对所有人生效。</li>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
//////覆盖
function module() { return '<?php echo $module; ?>';}

function add() {
	var newRow = { user: '0', name: '全部' };
	grid.addRow(newRow, 0);
}
//////覆盖结束

function onButtonEdit(e) {
	var data = null;
	var fn = function (data) {
			grid.cancelEdit();
            var row = grid.getSelected();
            grid.updateRow(row, {
                user: data.id,
                name: data.text
            });
		};
    sel_user( data, fn );
}

function user(name, id)
{
	grid.cancelEdit();
    var row = grid.getSelected();
    grid.updateRow(row, {
        user: id,
        name: name
    });
}
function quick(type)
{
	var section = 'config';
	var key = 'url';
	var value = '';
	value = ( type == 'product' ) ? 'http://touyou.mobi/interface/;http://saiyou.mobi/interface/;http://shaiyou.mobi/interface/' : 'http://192.168.1.100:8080/backend/interface/';
	
	grid.cancelEdit();
    var row = grid.getSelected();
    grid.updateRow(row, {
        section: section,
        key: key,
        value: value,
        user: 237,
        name: 'dda'
    });
}


</script>
