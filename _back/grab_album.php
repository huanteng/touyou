<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(29); ?>
<?php $module = 'grab_album'; ?>

<div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
	 url="data/<?php echo $module; ?>.php?method=search" idField="user"
	 allowResize="true" pageSize="10" allowAlternating="true"
	 allowCellSelect="true" multiSelect="true"       
	 showEmptyText="true"    
	 >
	<div property="columns">      

		<div field="name" width="50" headerAlign="center" allowSort="true">名字</div>
		<div field="uid" width="50" headerAlign="center" allowSort="true">关联用户ID</div>  
		<?php for ($i = 0; $i < 5; $i++) { ?>
			<div field="pic<?php echo $i; ?>" width="80" headerAlign="center" allowSort="true">第<?php echo $i + 1; ?>张相片</div>
		<?php } ?>
		<div field="action" width="100" headerAlign="center" allowSort="false">操作</div>	
	</div>
</div>

<div class="description">
	<h3>说明</h3>
	<ul>
		<li>图片及名字为采集得来；人工审核后可以添加为NPC</li>
		<li>关联用户ID 非0 代表已经添加为NPC;否则代表这个名字和图片还没添加</li>
		<li>首次添加为NPC，默认只上传 3 张图片，并且将第 1 张作为头像</li>
		<li>删除动作会将图片删除和名字删除；但不影响已经添加为npc的用户。如果其中一行已经没有图片，也可以删除。</li>
	</ul>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
	//////覆盖
	function module() { return '<?php echo $module; ?>';}
	//////覆盖结束

	$(function(){
		$('#toolbar .mini-button:lt(3),#toolbar .separator').hide();
		$('#search').append('<a class="mini-button" href="javascript:void(0);" onclick="rand();">&nbsp;随机&nbsp;</a>');
	});

	function onDrawcell (e) {
		var record = e.record,
		column = e.column,
		field = e.field,
		value = e.value;

		var html = "";
		switch( field )
		{
			case "uid":
				if( record.uid != 0 )
				{
					html = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail' + record.uid+'", text:"'+record.name+'", url: "user_detail.php?uid='+record.uid+'"})\'>'+record.uid+'</a>';	
				}
				break;
			case "action":
				if( record.uid == 0 )
				{
					html = '<a href="javascript:void();" onclick="add_npc(\''+record.id+'\',0)">加为男NPC</a>';
					html += '&nbsp;<a href="javascript:void();" onclick="add_npc(\''+record.id+'\',1)">加为女NPC</a>';
				}
				html += '&nbsp;<a href="javascript:void();" onclick="remove_album(\''+record.id+'\')">删除</a>';
				break;
		}
		
		if( html != "" ) e.cellHtml = html;

		for(i=0;i<5;i++)
		{
			eval("var pic_path = record.pic" + i);	
			eval("var pic_id = record.pic_id" + i);
				
			if( field == "pic" + i )
			{
				e.cellHtml =  (pic_path==undefined) ? '' : '<a href="../frontend/g/'+pic_path+'" target="_blank"><img id="pic_'+pic_id+'" src="../frontend/g/'+pic_path+'.s.jpg" style="width:80px;height:80px;border:0px;"/></a><br/><a href="javascript:void(0);" onclick="upload_pic('+pic_id+',\''+pic_path+'\','+record.uid+')">上传这张图片</a>';	
			}
		}
	
	}
		
	function add_npc(id,sex)
	{
		$.ajax({
			url: "data/grab_album.php?method=add_npc&grab_user=" + id + "&sex="+sex,
			type : "POST",
			dataType : 'json',
			success: function (text) {
				if(text.code==1)
				{

				}
				else 
				{
					alert(text.message);
				}
			
			}
		});
	}
	
	function remove_album(id)
	{
		if( confirm('你确定要删除这个人和TA的所有照片') )
		{
			$.ajax({
				url: "data/grab_album.php?method=remove_album&grab_user=" + id,
				type : "POST",
				dataType : 'json',
				success: function (text) {
					if(text.code==1)
					{

					}
					else 
					{
						alert(text.message);
					}
			
				}
			});
			
		}	
	}	
	
	function upload_pic(pic_id,pic_path,uid)
	{		
		if( uid == 0 ) 
		{
			alert('还没添加为NPC，不能上传');
			return false;
		}
		
		if( confirm('你确定上传这张照片？') )
		{
			$.ajax({
				url: "data/grab_album.php?method=move_img&grab_album_id="+pic_id+"&path="+pic_path+"&uid="+uid,
				type : "POST",
				dataType : 'json',
				success: function (text) {
					reload();
				}
			});
			
		}	
	}

	function rand()
	{
		grid.load({ rand: 'rand' });
	}
</script>
