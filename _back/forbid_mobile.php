<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(59); ?>
    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div width="10" type="checkcolumn"></div>
            <div field="id" width="20" headerAlign="center" allowSort="true">id
            </div>
        	<div field="imei" name="user" width="20" headerAlign="left">手机IMEI
        		<input property="editor" class="mini-textbox" style="width:100%;" required="true" requiredErrorText="不能为空"/>
            </div>
        	<div field="begin_time" width="20" headerAlign="center" allowSort="true" dateFormat="yyyy-MM-dd H:mm">开始时间
        		<input property="editor" class="mini-datepicker" format="yyyy-MM-dd H:mm" showTime="true" style="width:100%;" />
            </div>
        	<div field="end_time" width="20" headerAlign="center" allowSort="true" dateFormat="yyyy-MM-dd H:mm">结束时间
        		<input property="editor" class="mini-datepicker" format="yyyy-MM-dd H:mm" showTime="true" style="width:100%;" />
            </div>
        	<div field="reason" width="20" headerAlign="center" allowSort="true">理由
        		<input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'forbid_mobile';}
</script>