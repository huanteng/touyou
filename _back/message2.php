<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<style>
	/* 表情 */
	.f_menu .f_curr{ background:#ccc;border-radius: 0.5em 0.5em 0.5em 0.5em;}

	.f_menu{ background:#fff; width:90%; padding:6px 5%; padding-bottom:1px; overflow:hidden;}
	.f_menu ul li{ float:left; width:30%; text-align:center;list-style:none;}
	.f_menu ul li img{ width:20px; height:20px; }
	.f_menu ul{ margin:0px; padding:0px; overflow:hidden;}

	.c_face{ background:#fff; clear:left;  width:100%; overflow-y:auto; height:150px;}
	.c_face ul{ margin:0px; margin-top:2%; padding-left:0px;}
	.c_face ul li{ float:left; height:20px; width:20px; margin:2% 4%;cursor:pointer;list-style:none;}
	.c_face ul li img{ width:20px; height:20px; }
	/* 表情 end */
</style>
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">

	<div title="挑战接管：" region="west" width="250" >

		<div id="challenge"></div><br/>

		<span>客服咨询</span><br/>
		<select name="users" size="8" id="users1">
			<option>----------------------------------------------</option>
		</select>
		<br/>

		<span>VIP/视频认证/女性用户</span><br/>
		<select name="users" size="8" id="users2">
			<option>----------------------------------------------</option>
		</select>
		<br/>

		<span>半小时没有人搭理的家伙</span><br/>
		<select name="users" size="8" id="users3">
			<option>----------------------------------------------</option>
		</select>
		<br/>

		<span>普通聊天</span><br/>
		<select name="users" size="8" id="users">
			<option>----------------------------------------------</option>
		</select>

		<br>提示：双击快速忽略&nbsp;&nbsp;<a href="#" onclick="tab('view_dict','监控帐号','dictionary.php?q=监控帐号')">监控帐号</a>
		<!--br><input type="checkbox" id="pre_ignore" /><label for="pre_ignore">切换后自动忽略</label-->

		<hr/>
		<div title="真人--NPC" region="north" height="55" >
			NPC： <div id="npc" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id"
					  url="data/user.php?method=search&sortField=name&sortOrder=asc&npc=1&pageSize=100" value="" text="">     
				<div property="columns">
					<div header="名字" field="name" width="30"></div>
					<div header="NPC" field="npc" width="10"></div>
					<div header="登录" field="login" width="25"></div>
				</div>
			</div>
			<br/>
			(快速：<a href="#" onclick="mini.get('npc').set( {value:850, text: '骰妖子'} );return false;">骰妖子</a>)
			发送给：
			<div id="name" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id"
				 url="data/user.php?method=search&sortField=name&sortOrder=asc" value="" text="">     
				<div property="columns">
					<div header="名字" field="name" width="30"></div>
					<div header="NPC" field="npc" width="10"></div>
					<div header="登录" field="login" width="25"></div>
				</div>
			</div>
			<input type="submit" id="add" value="添加">
		</div>


	</div>
	<div title="附加栏" region="east"  showCloseButton="true"  width="350">

		<div class="mini-splitter" vertical="true" style="width:100%;height:100%;">
			<div size="50%" showCollapseButton="false">
				<!--div class="mini-panel" title="大额快速比赛监控（>=500,骰魔大赛忽略）" style="width:100%;height:100%;" showCollapseButton="true" url="vs2.php" bodyStyle="padding:0;" expanded="true" ></div-->
			</div>
			<div showCollapseButton="false">
				<div class="mini-panel" title="当前在线女用户" style="width:100%;height:100%;" showCollapseButton="true" bodyStyle="padding:0;" expanded="true" >
					<div property="toolbar" >
						<input type='button' value='刷新' style='vertical-align:middle;'/>
					</div>
					<table style="display: table; width: 100%;" class="mini-grid-table" cellspacing="0" cellpadding="0">
						<thead><tr><td width="30%">用户</td><td width="30%">状态</td><td>操作</td></tr></thead>
						<tbody id="online_girl"></tbody>
					</table>
				</div>
			</div>        
		</div>

	</div>
	<div title="聊天窗" region="center">

		<div id="content" style="width:99%;height:55%;margin-left: 10px;"></div>

		<hr/>
		<input name="pause" type="button" id="pause" value="禁止帐号" />
		<span style="padding:30px;">&nbsp;</span>
		<input name="ignore" type="button" id="ignore" value="忽略" />
		<input name="ignore_user" type="button" id="ignore_user" value="忽略此人" />
		<span style="padding:30px;">&nbsp;</span>
		<input type="button" id="friend" value="关注" />
		<input type="button" id="request" value="发起挑战" />
		<input type="button" onclick="challenge_block('接管')" value="接管" />
		<input type="button" onclick="challenge_block('接受')" value="接受" />

		<br/>
		<textarea id="reply_content" cols="80" rows="3"></textarea><a href="#" onclick="$('#reply_content').val('');return false;">清空</a>
		<br/>
		延迟（秒）：<input type="text" value="5" id="delay">
		&nbsp;&nbsp;<a href="javascript:delay(5)" title="增加5秒钟">+5s</a>&nbsp;&nbsp;<a href="javascript:delay(300)" title="增加5分钟">+5m</a>&nbsp;&nbsp;<a href="javascript:delay(-5)" title="减少5秒钟">-5s</a>&nbsp;&nbsp;<a href="javascript:delay(-300)" title="减少5分钟">-5m</a>&nbsp;&nbsp;<a href="javascript:delay(0)" title="自定义">+?m</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input style="width:100px;" name="button" type="button" onclick="reply()" value="发送" />
		<br/>
		<input name="send" type="radio" value="return" id="return" checked="checked" />
		<label for="return">回车</label>
		<input type="radio" name="send" value="ctrlreturn" id="ctrlreturn" />
		<label for="ctrlreturn">CTRL+回车</label>
		<input type="checkbox" id="next" value="1" />
		<label for="next">回复后自动处理下一条</label>

		<div style="width:98%;">

			<table width="100%" height="138" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%">

						<!--表情-->
						<div class="f_menu">
							<ul>
								<li class="f_curr" index=1><img src="../frontend/expression/emoji_express_btn.png"  ></li>
								<li index=2><img src="../frontend/expression/face_title_robot_express_btn.png"  ></li>
								<li index=3><img src="../frontend/expression/emo_im_happy_go.png"  ></li>
							</ul>
							<div class="c_face" id="face_1" >
								<ul>
									<li><img style="width:20px;height:20px;" src="../frontend/expression/goemoji_e41c.png" face="(L)" /></li>
									<li><img src="../frontend/expression/goemoji_e418.png" face="(K)" /></li>
									<li><img src="../frontend/expression/goemoji_e415.png" face="8-)" /></li>
									<li><img src="../frontend/expression/goemoji_e106.png" face="*-*" /></li>
									<li><img src="../frontend/expression/goemoji_e409.png" face=")-(" /></li>
									<li><img src="../frontend/expression/goemoji_e405.png" face="^>^" /></li>
									<li><img src="../frontend/expression/goemoji_e40b.png" face=":-O" /></li>
									<li><img src="../frontend/expression/goemoji_e40c.png" face=":S" /></li>
									<li><img src="../frontend/expression/goemoji_e40d.png" face=":A" /></li>
									<li><img src="../frontend/expression/goemoji_e105.png" face="-_o" /></li>
									<li><img src="../frontend/expression/goemoji_e107.png" face=":(" /></li>
									<li><img src="../frontend/expression/goemoji_e40f.png" face=":'[" /></li>
									<li><img src="../frontend/expression/goemoji_e108.png" face="'-_-" /></li>
									<li><img src="../frontend/expression/goemoji_e402.png" face="^_o" /></li>
									<li><img src="../frontend/expression/goemoji_e403.png" face="|-)" /></li>
									<li><img src="../frontend/expression/goemoji_e410.png" face="XoX" /></li>
									<li><img src="../frontend/expression/goemoji_e411.png" face="IoI" /></li>
									<li><img src="../frontend/expression/goemoji_e416.png" face=":@" /></li>
									<li><img src="../frontend/expression/goemoji_e419.png" face="(E)" /></li>
									<li><img src="../frontend/expression/goemoji_e531.png" face="(F)" /></li>
									<li><img src="../frontend/expression/goemoji_e111.png" face="(qq)" /></li>
									<li><img src="../frontend/expression/goemoji_e329.png" face="(-*)" /></li>
									<li><img src="../frontend/expression/goemoji_e327.png" face="(H)" /></li>
									<li><img src="../frontend/expression/goemoji_e31d.png" face="(Np)" /></li>
									<li><img src="../frontend/expression/goemoji_e31e.png" face="(M)" /></li>
									<li><img src="../frontend/expression/goemoji_e31f.png" face="(Hc)" /></li>
									<li><img src="../frontend/expression/goemoji_e034.png" face="(R)" /></li>
									<li><img src="../frontend/expression/goemoji_e035.png" face="(Dm)" /></li>
									<li><img src="../frontend/expression/goemoji_e00e.png" face="(G)" /></li>
									<li><img src="../frontend/expression/goemoji_e011.png" face="(Y)" /></li>
									<li><img src="../frontend/expression/goemoji_e01b.png" face="(C)" /></li>
									<li><img src="../frontend/expression/goemoji_e01d.png" face="(Fl)" /></li>
									<li><img src="../frontend/expression/goemoji_e10e.png" face="(Cr)" /></li>
									<li><img src="../frontend/expression/goemoji_e14c.png" face="(S)" /></li>
									<li><img src="../frontend/expression/goemoji_e03c.png" face="(kalaok)" /></li>
									<li><img src="../frontend/expression/goemoji_e33b.png" face="(Ff)" /></li>
									<li><img src="../frontend/expression/goemoji_e015.png" face="(Te)" /></li>
									<li><img src="../frontend/expression/goemoji_e016.png" face="(Bb)" /></li>
								</ul>
							</div>

							<div class="c_face" style="display:none;" id="face_2" >
								<ul>
									<li><img src="../frontend/expression/emo_im_angel.png" face="{0:-)}" /></li>
									<li><img src="../frontend/expression/emo_im_cool.png" face="{B-)}" /></li>
									<li><img src="../frontend/expression/emo_im_crying.png" face="{:'(}" /></li>
									<li><img src="../frontend/expression/emo_im_embarrassed.png" face="{:-[}" /></li>
									<li><img src="../frontend/expression/emo_im_foot_in_mouth.png" face="{:-!}" /></li>
									<li><img src="../frontend/expression/emo_im_happy.png" face="{:-)}" /></li>
									<li><img src="../frontend/expression/emo_im_kissing.png" face="{:-*}" /></li>
									<li><img src="../frontend/expression/emo_im_laughing.png" face="{:-D}" /></li>
									<li><img src="../frontend/expression/emo_im_lips_are_sealed.png" face="{:-X}" /></li>
									<li><img src="../frontend/expression/emo_im_money_mouth.png" face="{:-$}" /></li>
									<li><img src="../frontend/expression/emo_im_sad.png" face="{:-(}" /></li>
									<li><img src="../frontend/expression/emo_im_surprised.png" face="{=-0}" /></li>
									<li><img src="../frontend/expression/emo_im_tongue_sticking_out.png" face="{:-P}" /></li>
									<li><img src="../frontend/expression/emo_im_undecided.png" face="{:-\}" /></li>
									<li><img src="../frontend/expression/emo_im_winking.png" face="{;-)}" /></li>
									<li><img src="../frontend/expression/emo_im_wtf.png" face="{o_O}" /></li>
									<li><img src="../frontend/expression/emo_im_yelling.png" face="{:O}" /></li>
								</ul>
							</div>

							<div class="c_face" style="display:none;" id="face_3">
								<ul>
									<li><img src="../frontend/expression/emo_im_angel_go.png" face="O:-)" /></li>
									<li><img src="../frontend/expression/emo_im_cool_go.png" face="B-)" /></li>
									<li><img src="../frontend/expression/emo_im_crying_go.png" face=":'(" /></li>
									<li><img src="../frontend/expression/emo_im_embarrassed_go.png" face=":-[" /></li>
									<li><img src="../frontend/expression/emo_im_foot_in_mouth_go.png" face=":-!" /></li>
									<li><img src="../frontend/expression/emo_im_happy_go.png" face=":-)" /></li>
									<li><img src="../frontend/expression/emo_im_kissing_go.png" face=":-*" /></li>
									<li><img src="../frontend/expression/emo_im_laughing_go.png" face=":-D" /></li>
									<li><img src="../frontend/expression/emo_im_lips_are_sealed_go.png" face=":-X" /></li>
									<li><img src="../frontend/expression/emo_im_money_mouth_go.png" face=":-$" /></li>
									<li><img src="../frontend/expression/emo_im_sad_go.png" face=":-(" /></li>
									<li><img src="../frontend/expression/emo_im_surprised_go.png" face="=-0" /></li>
									<li><img src="../frontend/expression/emo_im_tongue_sticking_out_go.png" face=":-p" /></li>
									<li><img src="../frontend/expression/emo_im_undecided_go.png" face=":-\" /></li>
											 <li><img src="../frontend/expression/emo_im_winking_go.png" face=";-)" /></li>
									<li><img src="../frontend/expression/emo_im_wtf_go.png" face="o_O" /></li>
									<li><img src="../frontend/expression/emo_im_yelling_go.png" face=":O" /></li>
								</ul>
							</div>	
							<!--表情end-->			

							<br/>		


					</td>
					<td>
						<select size="10" id="quick_reply">

						</select>
						<a href="#" onclick="tab('dict_message','数据字典','dictionary.php?q=快速回复');return false;">编辑</a>
						<!--input type="submit" name="Submit2" value="增加自动回复" id="Submit2" /-->
					</td>

				</tr>
			</table>
		</div>

	</div>

</div>

<?php require 'bottom.php'; ?>
<script type='text/javascript' src='../_back/js/jscroll.js'></script>
<script type="text/javascript">
	var max_id = 0;
	var data = [];
	var config = [];
	var id = 1;
	var user = 0;
	var user_name = "";
	var npc = 0;
	var npc_name = "";
	var sel_id = "";
	var user_data = [];
	
	// return true if any empty
	function check_empty()
	{
		for (var i = 0; i < arguments.length; i=i+2) 
		{ 
			if(arguments[i]=="") 
			{ 
				alert(arguments[i+1]);
				return true; 
			}
		}
		return false;
	}
	
	function faile(jqXHR, textStatus, errorThrown)
	{
		alert(jqXHR.responseText);
	}

	function delay( second )
	{
		if ( second == 0 )
		{
			var minute = window.prompt( '请输入要延后的分钟，负数表示提前' );

			var reg = /^\-?[0-9]+\.?[0-9]{0,9}$/;
			if ( minute != null && reg.test( minute ) )
			{
				delay( minute * 60 );
			}
		}
		else
		{
			$("#delay").val( eval($("#delay").val()) + second );
		}
	}

	function challenge(data)
	{
		var content = "";
		var type = 0;
		var html = "";
	
		content = data.content.substring(3, data.content.length);
		type = eval(content.substring(0,1));
		content = content.substring(2, data.content.length);
	
		/*			i. 系统_1_dda向你挑战1000金币。接受 拒绝
			ii. 系统_0_挑战开始
			iii. 系统_2_挑战结束，金币1000，dda赢 继续 退出
		 */
		switch(type)
		{
			case 0:
				html = String.format("{0}和{1}{2} <a href='javascript:challenge_ok({3})'>确定</a>",data.sender_name, data.receiver_name, content, data.id);
				html += String.format("&nbsp;&nbsp;<a href='javascript:challenge_must_win({0}, {1}, {2})'>设置必赢</a>",data.id, data.receiver, data.sender);
				break;
			case 1:
				var gold = content.substring( 6 );

				html = String.format("{0}向{1}{2} <a href='javascript:challenge_accept({3}, {4}, {5},{6},0)'>接受</a>&nbsp;&nbsp;<a href='javascript:challenge_reject({3}, {4}, {5})'>拒绝</a>&nbsp;&nbsp;<a href='javascript:challenge_accept({3}, {4}, {5},{6},1)'>接受并NPC必赢</a>",data.sender_name,
				data.receiver_name, content, data.id, data.receiver, data.sender, gold);
				break;
			case 2:
				html = String.format("{0}向{1}{2} <a href='javascript:challenge_next({3}, {4}, {5})'>继续</a>&nbsp;&nbsp;<!--a href='javascript:challenge_send(\"no_continue\", {3}, {4}, {5})'>不继续</a-->",data.sender_name, data.receiver_name, content, data.id, data.receiver, data.sender);
				html += String.format("&nbsp;&nbsp;<a href='javascript:challenge_unblock({0}, {1}, {2})'>解除接管</a>",data.id, data.receiver, data.sender);
				break;
			default:;
		}
		if(html!="")
		{
			html = String.format('<div id="challenge{0}">{1}</div>',data.id, html);
			$("#challenge").append( html );
		}
	}

	function challenge_ok(id)
	{
		var func = function( result )
		{
		}

		post( "message", "challenge_ok", {type:'challenge_ok', id: id}, func, faile );
		$("#challenge"+id).remove();
	}

	function challenge_accept(id, npc, sender, gold, must_win)
	{
		var func = function( result )
		{
			alert("代码："+result.code+"\n信息："+result.memo);
		}

		post( "message", "challenge_accept", {id: id, npc: npc, sender: sender, gold:gold, must_win: must_win}, func, faile );
		$("#challenge"+id).remove();
	}

	function challenge_reject(id, npc, sender)
	{
		var func = function( result )
		{
			alert("代码："+result.code+"\n信息："+result.memo);
		}

		post( "message", "challenge_reject", {id: id, npc: npc, sender: sender}, func, faile );
		$("#challenge"+id).remove();
	}

	function challenge_must_win(id, npc, sender)
	{
		var func = function( result )
		{
			alert("代码："+result.code+"\n信息："+result.memo);
		}

		post( "message", "challenge_must_win", {id: id, npc: npc, user: sender}, func, faile );
		$("#challenge"+id).remove();
	}

	function challenge_unblock(id, npc, sender)
	{
		var func = function( result )
		{
			alert("代码："+result.code+"\n信息："+result.memo);
		}

		post( "message", "challenge_unblock", {id: id, npc: npc, user: sender}, func, faile );
		$("#challenge"+id).remove();
	}

	function challenge_next(id, npc, sender)
	{
		var func = function( result )
		{
			alert("代码："+result.code+"\n信息："+result.memo);
		}

		post( "message", "challenge_next", {id: id, npc: npc, sender: sender}, func, faile );
		$("#challenge"+id).remove();
	}

	function challenge_block( remark )
	{
		if( user == 0 || npc == 0 )
		{
			alert("请先选择对话对象");
			return;
		}
		
		var func = function(data){
			alert(data.memo);
		};
		post( "message", "challenge_block", {user:user, npc:npc, remark:remark}, func, faile );
	}

	function classify(receiver, vip, is_real)
	{
		var select_id;
		if(receiver == 850) select_id = "users1";
		else if( vip != 0 || is_real != 0 ) select_id = "users2";
		else select_id = "users";			
		return select_id;
	}

	function get()
	{
		var func = function( json )
		{	
			for( var index in json )
			{
				var row = json[index];
				var key = "";
				var text = "";
				var count = 0;
				var selected = "";
				// 是否要移动位置（如果是npc所发出，则不必
				var is_move = false;
				
				if( row.sender_npc == '1' )
					user_data[row.receiver] = {id:row.receiver, name: row.receiver_name};
				else
					user_data[row.sender] = {id:row.sender, name: row.sender_name};
						
				var select_id = classify(row.receiver, row.vip, row.is_real);
				
				key = ( row.sender_npc == '1' ) ? (row.receiver_name + '-->' + row.sender_name) : (row.sender_name + '-->' + row.receiver_name);
			
				is_move = ( row.receiver_npc == '1' );

				// 新版部分内容乱码
				if( !row.content )
				{
					row.content = '新版内容乱码';
				}
			
				// 挑战拦截
				if( row.content.substring(0, 3) == '系统_' )
				{
					challenge(row);
				}
			
				if( data[key] == undefined )
				{
					if( row.sender_npc == '1' ) continue;
					data[key] = [];
					config[key] = [];
					count = 0;
				}
				else
				{
					if( $("#"+select_id+" option:selected").val() == key ) selected = ' selected="selected"';
					
					if( is_move )
					{
						var chat = $("#"+select_id+" option[value='" + key + "']");
						text = chat.text();
						var arr = text.replace(")","").split("(");
						if( arr.length == 2 ) count = eval(arr[1]);
						chat.remove();
					}
				}
				if( is_move )
				{
					if( row.read == '0' && row.receiver_npc == '1' )
					{
						count++;
					}
					if( count != 0 )
						count = "("+count+")";
					else
						count = "";

					var vip = '';
					if( row.vip != 0 ) vip = '【vip' + row.vip + '】';
					
					if( row.is_real != 0 ) vip += '【视频认证】';
					if( row.npc_new == 1 ) vip += '【新】';

					option = String.format('<option value="{0}"{3} user="{5}" npc="{6}">{4}{1}{2}</option>', key, key, count, selected, vip, row.sender, row.receiver);
					
					$("#"+select_id).prepend(option);
										
				}

				data[key][data[key].length] = row;
			
				if( max_id < eval(row.id) ) max_id = eval(row.id);

				if( selected != '' ) $("#"+select_id).click();
			}
		}
		
		post( "message", "npc_chat", {max_id: max_id}, func, function(){} );
		setTimeout("get()", 2000);
	}
	
	function reply()
	{
		var chat = $("#"+sel_id+" option:selected").val();
		var content = $("#reply_content").val();
		var time = $("#delay").val();
	
		if( check_empty( chat, "请从左边选择对话", content, "请输入内容", time, "请输入延时时间" ) ) return;
	
		//var d = {id: id, user: user, content: encodeURIComponent( content ), from: npc, time: time};
		var d = mini.encode( {id: id, user: user, content: content, from: npc, time: time} );
		
		var func = function( result )
		{
			if( result.code == -1 ) alert('提交失败');
			else
			{
				//if( npc != 850 )
				//{
				var old_content = $("#content").html();
				old_content = $(old_content.replace(/<br>/g, "\n")).text().replace(/\n/g, "<br>");
				$("#content").html( String.format("{3}<br>{0} {1}【队列】<br>{2}<br>",npc_name, time, content, old_content ) ).jscroll();
				var key = user_name + '-->' + npc_name;
				data[ key ][ data[key].length] = { 'sender': npc, 'receiver': user, 'sender_name': npc_name, 'receiver_name': user_name, 'time': time+'【队列】', 'content': content};
				//}
					
				$("#reply_content").val('');
				
				// 处理下一条
				if( $("#next").attr("checked") )
				{
					var options = $("#"+sel_id)[0].options;
					var count = options.length;
					var i;
					for(i=1;i<count;++i)
					{
						if( options[i].value != options[i].text )
						{
							options[i].selected = true;
							$("#"+sel_id).click();
							break;
						}
					}
				}
			}
		}

		post( "message", "reply", d, func, faile );
	}
	
	function load_quick_reply()
	{
		var func = function(result){
			$(result).each(function(k,v){
				$('#quick_reply').append( '<option>'+v+'</option>' );
			});
		}
		post( "message", "quick_reply", {}, func, faile );
	}
	
	function load_online_girl()
	{
		// 20131001, for debug
		return;

		var func = function(result){
			var str = "";
			
			$(result.data).each(function(k,v){
				str += String.format('<tr{3}><td><a href="#" onclick="show_info({2});return false;">{0}</a></td><td>{1}</td><td><a href="#" onclick="request2({2});return false;">挑战</a>&nbsp;<a href="#" onclick="talk({2},\'{0}\');return false;">聊天</a>&nbsp;<a href="#" onclick="props_give({2},\'{0}\');return false;">送礼</a></td></tr>', v.name , v.status, v.user, ((k==0)?' style="backgroud-color:#ccc;"':''));
			});
			$('#online_girl').html( str );

			setTimeout(load_online_girl, 30000);//30秒
		}	
		post( "online", "search", {sex:1, npc:0}, func, function(){} );
	}
	
	function show_info(uid)
	{
		var html = '';
		var name = '';
		var func = function(result){ 
			name = result.name;
			html += '<div style="float:left;"><img src="'+result.logo+'"/><br/>'+result.name+' ('+result.sex+')</div><div style="float:left;"><p>生日：'+result.birthday+'</p><p>城市：'+result.city+'</p><p>VIP：'+result.vip+'</p><p>视频认证：'+result.is_real+'</p></div>';
		};
		post( "user", "info", {uid: uid}, func, faile );
		
		mini.showMessageBox({
            width: 250,
            title: "个人资料",
            buttons: ["关闭","详细"],
            message: "",
            html: html,
            showModal: false,
            callback: function (action) {
				if(action == '详细') tab('user_detail'+uid, name+'用户资料', 'user_detail.php?uid='+uid);
			}
        });	
	}

	function request2(uid)
	{
		var gold = prompt("请输入挑战金币：", "1000");
		if(gold == "" || gold == null) return;
		else
		{
			var func = function(data){
				alert(data.message);
			};

			post( "message", "challenge_request", {npc:0, uid:uid, gold: gold}, func, faile );
		}
	}

	function talk(uid, user_name)
	{
		var key = "";	
		var npc = [];
		var name = [];

		npc[0] = '骰妖子';
		npc[1] = 850;

		name[0] = user_name;
		name[1] = uid;

		post( "npc", "get_npc", {sex: 0, online:1}, function(result){
			npc[0] = result.name;
			npc[1] = result.id;
		}, faile );

		key = name[0] + '-->' + npc[0];

		if( data[key] == undefined )
		{
			var option = "";
			option += '<option value="' + key + '">' + key + '</option>';
			$("#users").append(option);
			data[key] = [{id: "0", receiver: name[1], receiver_name:name[0],receiver_npc:"0",sender:npc[1], sender_name:npc[0],sender_npc:"1",content:"", time:"初始化"}];
			config[key] = [];
		}

		$("#users option[value='"+key+"']").attr("selected", "selected");
		$("#users").click();
	}
	
	function props_give(uid, name)
	{
		tab('props_give', '赠送道具', 'props_give.php?target='+uid+'&target_name='+name);
	}
	
	function select_click(select_id, chat)
	{
		if( select_id == '' || chat == '' || chat == undefined || chat == '----------------------------------------------' ) return false;
		$("select:not('#"+select_id+"') option").removeAttr('selected');
		$("#"+ select_id + " option:selected").text(chat);
			
		var content = "";
		var d = data[chat];

		if ( d[0].sender_npc == '1' )
		{
			user = d[0].receiver;
			user_name = d[0].receiver_name;
			npc = d[0].sender;
			npc_name = d[0].sender_name;
		}
		else
		{
			user = d[0].sender;
			user_name = d[0].sender_name;
			npc = d[0].receiver;
			npc_name = d[0].receiver_name;
		}

		//title
		//两用户之间的联系状况
		var moving = function( result )
		{
			if(result.length > 0)
			{
				var contact='';
				$.each(result, function(i,r){
					contact += ( r + '<br/>');
				})
				content += '<div style="background:#ffffc0;">'+contact+'</div>';
			}
		}
		post( "message", "moving", {uid1:user, uid2:npc}, moving, faile );
		
		content += String.format('<span> <a href="#" class="user_info" onclick="tab(\'user_detail\',\'{0}\',\'user_detail.php?uid={2}\');return false;">{0}</a> --> <a href="#" onclick="tab(\'user_detail\',\'{1}\',\'user_detail.php?uid={3}\');return false;">{1}</a>  <a href="#" onclick="$(\'.history\').toggle();return false;">历史</a></span><br />', user_name, npc_name, user, npc);

		//聊天历史
		var histroy = function( result )
		{
			var history_list = '';
			$(result.data).each(function(i,h){
				history_list += String.format('<br><span><a href="#" onclick="show_info({0});return false;">{1}</a> {2}<br>{3}</span><br>', h.sender, h.sender_name, h.time, h.content );
			});
			content += '<div class="history">'+history_list+'<br/><span style="color:#ccc;">———— 以上是历史消息 ————</span></div>';
		}
		post( "message", "history", {sender: user, receiver: npc, sortField: 'time', sortOrder: 'asc' }, histroy, faile );
		//聊天历史 end
		
		var count = d.length;
		for(var i=0;i<count;++i)
		{
			var style = (d[i].sender_npc!='1' || d[i].sender!=850)?' style="color:blue;"':'';
			content += String.format('<br /><span{0}><a href="#" class="user_info" onclick="show_info({1});return false;">{2}</a> {3}<br>{4}</span><br />', 
			style, d[i].sender, d[i].sender_name, d[i].time, d[i].content);
		}

		var delay = 0;
		if( npc != 850 )
		{
			var func = function( result )
			{
				if( result.code == '1' )
				{
					delay = Math.floor(Math.random() * 60);
				}
				else
				{
					delay = 28800 + Math.floor(Math.random() * 60);
					content +="<br>注意：本npc离线<br>";
				}
			}
			post( "online", "is_online", {uid: npc}, func, faile );
		}

		$("#content").html(content).jscroll();
		$(".history").hide();
		$("#delay").val( delay );
		//显示最新那句聊天
		if( count > 5 ) $(".jscroll-c").css('top','-'+((count+1) * 55)+'px'); 
	}
	
	function invisible()
	{
		var func = function(){
			
		}	
		post( "message", "invisible", {uid: uids()}, func, faile );	
		//setTimeout(invisible, 300000);//5分钟
	}
	
	function uids()
	{
		var uid_arr = [];
		for(var u in user_data)
		{
			if( !isNaN(u) ) uid_arr.push(user_data[u]['id']);
		}
		var uids = uid_arr.join(",");
		return uids;
	}

	$(function(){
		$(".f_menu li").bind( "click", function(event) {
			$(".f_menu li").removeClass('f_curr');
			$(this).addClass('f_curr');
			$('#face_1,#face_2,#face_3').hide();
			$('#face_'+$(this).attr('index')).show();
		});
	
		$(".c_face img").bind( "click", function(event) {	
			var face = $(this).attr('face');	
			$('#reply_content').val( $('#reply_content').val()+face );
		});

		$("#users").click(function(){
			select_click( 'users', $(this).val() );
			sel_id = 'users';
		});

		$("#users1").click(function(){
			select_click( 'users1', $(this).val() );
			sel_id = 'users1';
		});
		
		$("#users2").click(function(){
			select_click( 'users2', $(this).val() );
			sel_id = 'users2';
		});

		$("#users,#users1,#users2,#users3").dblclick(function(){
			$("#ignore").click();
		});
	
		$("#ignore").click(function(){

			var func = function( result ){};		
			post( "message", "ignore", {receiver: npc, sender: user}, func, faile );
			
			var chat = $("#"+sel_id+" option:selected").val();
			var options = $("#"+sel_id)[0].options;
			var count = options.length;
			var i;
			for(i=1;i<count;++i)
			{
				if( options[i].selected )
				{
					if( i == count - 1 )  i--;
					break;
				}
			}
        
			$("#"+sel_id+" option:selected").remove();
			delete data[chat];
			delete config[chat];
			options[i].selected = true;
			$("#"+sel_id).click();
		});
		
		//忽略此人
		$("#ignore_user").click(function(){
			
			if( user == 0 || npc == 0 )
			{
				alert("请先选择对话对象");
				return;
			}
			
			$("#"+sel_id+" option").each(function(){
				
				if( $(this).attr('user') == user && $(this).attr('user') != undefined )
				{
					var chat = $(this).val();
					$(this).remove();			
					delete data[chat];
					delete config[chat];
					
					var func = function( result ){};		
					post( "message", "ignore", {receiver: $(this).attr('npc'), sender: $(this).attr('user')}, func, faile );						
				}			
			});
			$("#"+sel_id).click();
		});

		$("#friend").click(function(){
			if( user == 0 || npc == 0 )
			{
				alert("请先选择对话对象");
				return;
			}

			var func = function(data){
				alert('关注成功');
			};

			post( "friend", "create", {friend:user, uid:npc}, func, faile );
		});
	
		$("#pause").click(function(){
			if( user > 0 )
			{
				var func = function(data){
					alert(data.message);
				};
				post( "message", "forbid_user", {uid:user}, func, faile );
			} 
			else
			{
				alert('请先选择对话对象，才能禁止');
			}		
		});
	
		$("#info1").click(function(){
			tab( "user_detail"+user, "用户资料", "user_detail.php?uid=" + user );
		});
		$("#info2").click(function(){
			tab( "user_detail"+npc, "NPC资料", "user_detail.php?uid=" + npc );
		});
		$("#history").click(function(){
			tab( "message_history", "历史", "message_history.php?sender=" + user + "&receiver=" + npc );
		});
	
		$("#quick_reply").click(function(){
			var text = $("#quick_reply option:selected").attr('value');
			$("#reply_content").val(text);
		});

		$("#add").click(function(){
			var key = "";	
			var npc = [];
			var name = [];
			
			npc[0] = mini.get('npc').getText();
			npc[1] = mini.get('npc').getValue();
			
			name[0] = mini.get('name').getText();
			name[1] = mini.get('name').getValue();
			
			key = name[0] + '-->' + npc[0];
		
			if(npc[1] == undefined)
			{
				alert("请从下拉表选择NPC");
				return;
			}
			if(name[1] == undefined)
			{
				alert("请从下拉表选择接收者");
				return;
			}
			
			if( data[key] == undefined )
			{
				var option = "";
				option += '<option value="' + key + '">' + key + '</option>';
				$("#users").append(option);
				data[key] = [{id: "0", receiver: name[1], receiver_name:name[0],receiver_npc:"0",sender:npc[1], sender_name:npc[0],sender_npc:"1",content:"", time:"初始化"}];
				config[key] = [];
			}
		
			$("#users option[value='"+key+"']").attr("selected", "selected");
			$("#users").click();
		});
	
		$("#reply_content").keyup(function(event){
			var val = $("input[name='send'][checked]").val(); 
			if(val == "return")
			{
				if(event.keyCode == 13)
				{
					var s = $("#reply_content").val();
					s = s.replace(/\n/g, "");
					$("#reply_content").val( s );
					reply();
				}
			}
			else
			{
				if(window.event.ctrlKey&&window.event.keyCode==13)
					reply();
			}
		});

		$("#challenge_block_remove").click(function(){
			challenge_unblock(0, npc, user)
		});
	
		$("#request").click(function(){
			var gold = prompt("请输入挑战金币：", "1000");
			if(gold!=null)
			{
				var func = function(data){
					alert(data.message);
				};

				post( "message", "challenge_request", {npc:npc, uid:user, gold: gold}, func, faile );	
			}
			else
				return;	
		});
		
		get();
		
		//载入快速回复
		load_quick_reply();
		
		//载入在线女用户
		load_online_girl();
		
		//半小时没有理会的人
		//invisible();
	});
</script>