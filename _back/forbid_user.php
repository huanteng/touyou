<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(58); ?>
    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"  
    >
        <div property="columns">
            <div width="10" type="checkcolumn"></div>
            <div field="id" width="20" headerAlign="center" allowSort="true">id
            </div>
        	<div field="uid" displayField="name" name="uid" width="20" headerAlign="left">用户
        		<input property="editor" class="mini-buttonedit" onbuttonclick="onButtonEdit" style="width:100%;" allowInput="false" />
            </div>
        	<div field="begin_time" width="20" headerAlign="center" allowSort="true" dateFormat="yyyy-MM-dd H:mm">开始时间
        		<input property="editor" class="mini-datepicker" showTime="true" style="width:100%;" />
            </div>
        	<div field="end_time" width="20" headerAlign="center" allowSort="true" dateFormat="yyyy-MM-dd H:mm">结束时间
        		<input property="editor" class="mini-datepicker" showTime="true" style="width:100%;" />
            </div>
        	<div field="reason" headerAlign="center" allowSort="true">理由
        		<input property="editor" class="mini-textarea" style="width:100%;" />
            </div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'forbid_user';}

function onButtonEdit(e) {
	var data = null;
	var fn = function (data) {
			grid.cancelEdit();
            var row = grid.getSelected();
            grid.updateRow(row, {
                uid: data.id,
                name: data.text
            });
		};
    sel_user( data, fn );
}
</script>