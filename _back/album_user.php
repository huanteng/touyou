<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(29); ?>

<style>
	div#page {
		width: 900px;
		height: 1200px;
		background-color: #fff;
		margin: 0 auto;
		text-align: left;
		border-color: #ddd;
		border-style: none solid solid;
		border-width: medium 1px 1px;
	}
	div#container {
		padding: 20px;
	}
</style>
<link href="../_back/js/galleriffic/galleriffic-2.css" rel="stylesheet" type="text/css" />

<div style="width:98%;">

	<div id="page">
		<div id="container">
			<h1><a href="#"></a></h1>
			<h2></h2>

			<!-- Start Advanced Gallery Html Containers -->
			<div id="gallery" class="content">
				<div id="controls" class="controls"></div>
				<div class="slideshow-container">
					<div id="loading" class="loader"></div>
					<div id="slideshow" class="slideshow"></div>
				</div>
				<div id="caption" class="caption-container"></div>
			</div>
			<div id="thumbs" class="navigation">
				<ul class="thumbs noscript">
					<!--<li>图片链接</li><li>图片链接</li>-->
				</ul>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>


</div>

<div class="description">
	<h3>说明</h3>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
$.ajax
({
	type : "POST",
	url : "../_back/data/album.php",
	dataType : 'json',
	async : false,
	data : {method: 'search', uid: '<?php echo $_GET['uid'];?>'},
	success : function(data) {
		var li_array=[];
		var user='';
		$.each(data.data,function(i,row){

			var li = '';
				li = '<li>';
				li += '		<a class="thumb" name="leaf" href="'+row.path+'" title="'+row.path+'">';
				li += '			<img src="'+row.path+'.s.jpg" alt="" />';
				li += '		</a>';
//				li += '		<div class="caption">';
//				li += '			<div class="download">';
//				li += '				<a href="'+row.path+'">原图</a>';
//				li += '			</div>';
//				li += '			<div class="image-title">'+row.time+'</div>';
//				li += '			<div class="image-desc"></div>';
//				li += '		</div>';
				li += '	</li>';
				
			li_array.push(li);
			
			if(i==0) user = row.name;
		});

		$('#thumbs>ul').html( li_array.join('') );
		$('#container>h2').html( user + '的相册' );	
	},
	error : function (jqXHR, textStatus, errorThrown) {error (jqXHR, textStatus, errorThrown);}
});
</script>
<script type="text/javascript" src="../_back/js/galleriffic/jquery.galleriffic.js"></script>
<script type="text/javascript" src="../_back/js/galleriffic/jquery.opacityrollover.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		// We only want these styles applied when javascript is enabled
		$('div.navigation').css({'width' : '300px', 'float' : 'left'});
		$('div.content').css('display', 'block');

		// Initially set opacity on thumbs and add
		// additional styling for hover effect on thumbs
		var onMouseOutOpacity = 0.67;
		$('#thumbs ul.thumbs li').opacityrollover({
			mouseOutOpacity:   onMouseOutOpacity,
			mouseOverOpacity:  1.0,
			fadeSpeed:         'fast',
			exemptionSelector: '.selected'
		});
				
		// Initialize Advanced Galleriffic Gallery
		var gallery = $('#thumbs').galleriffic({
			delay:                     2500,
			numThumbs:                 15,
			preloadAhead:              10,
			enableTopPager:            true,
			enableBottomPager:         true,
			maxPagesToShow:            7,
			imageContainerSel:         '#slideshow',
			controlsContainerSel:      '#controls',
			captionContainerSel:       '#caption',
			loadingContainerSel:       '#loading',
			renderSSControls:          true,
			renderNavControls:         true,
			playLinkText:              '播放',
			pauseLinkText:             '停止',
			prevLinkText:              '&lsaquo; 上一张',
			nextLinkText:              '下一张 &rsaquo;',
			nextPageLinkText:          '下一页 &rsaquo;',
			prevPageLinkText:          '&lsaquo; 上一页',
			enableHistory:             false,
			autoStart:                 false,
			syncTransitions:           true,
			defaultTransitionDuration: 900,
			onSlideChange:             function(prevIndex, nextIndex) {
				// 'this' refers to the gallery, which is an extension of $('#thumbs')
				this.find('ul.thumbs').children()
				.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
				.eq(nextIndex).fadeTo('fast', 1.0);
			},
			onPageTransitionOut:       function(callback) {
				this.fadeTo('fast', 0.0, callback);
			},
			onPageTransitionIn:        function() {
				this.fadeTo('fast', 1.0);
			}
		});
	});
</script>
<!-- We only want the thunbnails to display when javascript is disabled -->
<script type="text/javascript">
	document.write('<style>.noscript { display: none; }</style>');
</script>