<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(90);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
	    if (field == "uid") {
	        e.cellHtml = '<a href="javascript:void();" onclick=\'tab("detail' + record.user+'", "'+record.name+'", "user_detail.php?uid='+record.uid+'")\'>'+record.name+'</a>';
	    }
	});
}

function search() {
	var user = mini.get("user").getValue();
    var q = mini.get("q").getValue();
    grid.load({ q: q, user: user });
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						<?php autocomplete_name() ;?>
						中奖描述或备注：
                        <input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:150px;" onenter="onKeyEnter"/>   
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div field="id" width="10" headerAlign="center" allowSort="true">id
            </div>
        	<div field="time" width="20" headerAlign="center" allowSort="true">时间
            </div>
        	<div field="period" width="10" headerAlign="center" allowSort="true">期
            </div>
            <div field="uid" displayField="name" width="30" headerAlign="center" allowSort="true">名字
			</div>
        	<div field="description" width="30" headerAlign="center" allowSort="false">中奖描述
			</div>
<!--			<div field="type" displayField="type_name" width="30" headerAlign="center" allowSort="true">类型
			</div>-->
        	<div field="remark" displayField="rule_name" width="120" headerAlign="center" allowSort="false">备注
			</div>	
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'event';}
</script>