<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(34);
?>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;

		var html = "";
	    switch( field )
		{
			case "npc":
				html = ( value == '1' ) ? '√' : ' ';
				break;
	    
			case "action":
				html = '<a href="../backend/task_item_'+e.record.type+'.php?id='+e.record.id+'" target="_black">查看</a> ' +
					'<a href="javascript:void()" onclick="run1(' + e.record.id + ')">运行</a>';
				break;
	    }
		if( html != "" ) e.cellHtml = html;
		if(e.columnIndex == 0)
		{
			if( e.row.type > 1000 ) e.cellHtml = '';
		}
	});
}

function search() {
	var table = mini.get("table").getValue();
    var user = mini.get("uid").getValue();
	var type = mini.get("type").getValue();
	var status = mini.get("status").getValue();
	var creater = mini.get("creater").getValue();
    grid.load({ user: user, type: type, status: status, creater:creater, table:table });
}
function onKeyEnter(e) {
    search();
}

function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
	// 保存时状态出错，不知原因，暂弃保存功能
	return;
	
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function run1( id )
{
	grid.loading("运行中，请稍候...");
	post( module(), "run1", id, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">
						<a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
						<a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						<input id="table" name="table" class="mini-radiobuttonlist" data="[{id: 'task_queue', text: '当前'}, {id: 'task_queue_history', text: '历史'}]" value="task_queue"/> | 
						类型：<input id="type" class="mini-combobox" style="width:100px;" textField="text" valueField="id" 
    url="data/task_queue.php?method=type" value="" allowInput="false" showNullItem="true" nullItemText="请选择..."/>  
						状态：<input id="status" class="mini-combobox" style="width:60px;" textField="text" valueField="id" 
    data="[{id: '1', text: '未开始'}, {id: '2', text: '进行中'}, {id: '3', text: '已完成'}, {id: '4', text: '已暂停'}, {id: '5', text: '预发布'}]"
	value="0" allowInput="false" showNullItem="true" nullItemText="请选择..."/>
						<?php autocomplete_name() ;?>
						操作人：<input id="creater" class="mini-combobox" textField="name" valueField="id" url="data/administrator.php?method=drop" allowInput="false" showNullItem="true" nullItemText="请选择..."/>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>
	
	<?php $module = 'task_queue'; ?>
  
    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="user"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
			<div width="10" type="checkcolumn"></div>
			<div field="id" width="20" allowSort="true">ID</div>
			<div field="create" width="25" allowSort="true">创建时间</div>
        	<div field="type" displayField="type_name" width="20" headerAlign="left" >类型</div>
			<div field="time" width="25" allowSort="true">开始时间</div>
			<div field="status" width="20" allowSort="true">状态</div>
            <div field="name" width="20" allowSort="true">目标用户</div>
			<div field="npc" width="10" allowSort="true">NPC</div> 
            <div field="content" headerAlign="center" allowSort="true">内容</div>
			<div field="time1" width="10" headerAlign="center" allowSort="true">秒1
				<input property="editor" class="mini-TextArea" style="width:100%;" />
			</div>
			<div field="time2" width="10" headerAlign="center" allowSort="true">秒2
				<input property="editor" class="mini-TextArea" style="width:100%;" />
			</div>
            <div field="creater" displayField="creater_name" width="30" headerAlign="center" allowSort="true">操作人</div>
			<div field="action" width="30" headerAlign="center" allowSort="false">操作</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
		<ul>
			<li>当前：未执行的队列；历史：已执行结束的队列</li>
			<li>秒1、秒2用来设置重复任务的时间间隔</li>
		</ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
//////覆盖
user_context_column = { user: 'name' };
function module() { return '<?php echo $module; ?>';}
</script>
