<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(18); ?>

<div class="mini-splitter" style="width:98%;height:150%;">
	<div size="70%" showCollapseButton="true">
		<div id="form1" style="margin-left: 10px;">

			<table class="form-table" border="0" cellpadding="1" cellspacing="1">	
				<tr>
					<td class="form-label" style="width:60px;"></td>
					<td style="width:250px"><img id="logo" src=""/><br/><input type="file" id="logo_file" name="logo_file"><br/><a class="mini-button" onclick="clear_logo()">清除头像</a>&nbsp;<a class="mini-button" onclick="upload_logo()">上传头像</a></td>
					<td style="width:350px;height: 300px;" colspan="2">
						注册时间：<input name="register" class="mini-textbox" allowInput="false" /><br/>
						退出时间：<input name="logout" class="mini-textbox" allowInput="false" /><br/>
						最后登录：<input name="login" class="mini-textbox" allowInput="false" /><br/>
						登录次数：<input name="login_times" class="mini-textbox" allowInput="false" /><br/>
						注册IMEI：<input name="register_imei" class="mini-textbox" allowInput="false" /><br/>
						登录IMEI：<input name="login_imei" class="mini-textbox" allowInput="false" />
					</td>    
				</tr>
				<tr>
					<td class="form-label">&nbsp;</td>
					<td><a class="mini-button" onclick="submitForm()">保存修改</a></td>
					<td class="form-label">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="form-label" style="width:60px;">ID: </td>
					<td style="width:150px"><input name="id" class="mini-textbox" allowInput="false" /></td>
					<td class="form-label" style="width:60px;">用户名: </td>
					<td style="width:150px"><input name="name" class="mini-textbox" allowInput="false" /></td>    
				</tr>
				<tr>
					<td class="form-label" style="width:60px;color:red;">改用户名：</td>
					<td style="width:150px">
						<input name="rename" class="mini-textbox" />
					</td>
					<td class="form-label" style="width:60px;">真名：</td>
					<td style="width:150px">
						<input name="real" class="mini-textbox" />
					</td>
				</tr>
				<tr>
					<td class="form-label" style="width:60px;color:red;">改密码：</td>
					<td style="width:150px">
						<input name="newpass" class="mini-textbox" />
					</td>
					<td class="form-label" style="width:60px;"></td>
					<td style="width:150px"></td>
				</tr>
				<tr>
					<td class="form-label" style="width:60px;">&nbsp;</td>
					<td style="width:150px"></td>
					<td class="form-label" style="width:60px;"></td>
					<td style="width:150px"></td>
				</tr>


				<tr>
					<td class="form-label" style="width:60px;">心情：</td>
					<td style="width:150px">
						<input name="mood" class="mini-textbox" />
					</td>
					<td class="form-label" style="width:60px;">NPC：</td>
					<td style="width:150px">
						<input name="npc" class="mini-radiobuttonlist" data="[{id: 0, text: '否'}, {id: 1, text: '是'}]"/>
					</td>
				</tr>
				<tr>
					<td class="form-label" style="width:60px;">QQ：</td>
					<td style="width:150px">
						<input name="qq" class="mini-textbox" />
					</td>
					<td class="form-label" style="width:60px;">手机：</td>
					<td style="width:150px">
						<input name="mobile" class="mini-textbox" />
					</td>
				</tr>
				<tr>
					<td class="form-label">E-mail：</td>
					<td >
						<input name="email" class="mini-textbox"/>
					</td>
					<td class="form-label">邮箱验证：</td>
					<td >
						<input name="email_check" class="mini-radiobuttonlist" data="[{id: 0, text: '未验证'}, {id: 1, text: '已验证'}]"/>
					</td>
				</tr>
				<tr>
					<td class="form-label">生日：</td>
					<td >
						<input name="birthday" class="mini-datepicker" format="yyyy-MM-dd" />
					</td>
					<td class="form-label">性别：</td>
					<td >
						<input name="sex" class="mini-radiobuttonlist" data="[{id: 0, text: '男'}, {id: 1, text: '女'}]"/>
					</td>
				</tr>
				<tr>
					<td class="form-label">年龄：</td>
					<td >
						<input name="age" class="mini-textbox" />
					</td>
					<td class="form-label">年龄秘密：</td>
					<td >
						<input name="hide_age" class="mini-radiobuttonlist" data="[{id: 0, text: '公开'}, {id: 1, text: '隐藏'}]"/>
					</td>
				</tr>
				<tr>
					<td class="form-label">省份：</td>
					<td >
						<input name="province" class="mini-textbox" />
					</td>
					<td class="form-label">城市：</td>
					<td >
						<input name="city" class="mini-textbox" />
					</td>
				</tr>
				<tr>
					<td class="form-label">身高：</td>
					<td >
						<input name="height" class="mini-textbox" />
					</td>
					<td class="form-label">收入：</td>
					<td >
						<input name="income" class="mini-textbox" />
					</td>
				</tr>
				<tr>
					<td class="form-label">兴趣：</td>
					<td >
						<input name="interest" class="mini-textbox" />
					</td>
					<td class="form-label">职业：</td>
					<td >
						<input name="profession" class="mini-textbox" />
					</td>
				</tr>
				<tr>
					<td class="form-label">夜店：</td>
					<td >
						<input name="bar" class="mini-textbox" />
					</td>
					<td class="form-label">形象：</td>
					<td >
						<input name="image" class="mini-textbox" />
					</td>
				</tr>
				<tr>
					<td class="form-label">视频认证：</td>
					<td >
						<input name="is_real" class="mini-radiobuttonlist" data="[{id: 0, text: '否'}, {id: 1, text: '是'}]"/>
					</td>
					<td class="form-label"></td>
					<td >
						
					</td>
				</tr>
				<tr>
					<td class="form-label">当前醉酒度：</td>
					<td >
						<input name="drunk" class="mini-textbox"/>
					</td>
					<td class="form-label"></td>
					<td >

					</td>
				</tr>
				<tr>
					<td class="form-label">&nbsp;</td>
					<td><a class="mini-button" onclick="submitForm()">保存修改</a></td>
					<td class="form-label">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
	<div showCollapseButton="true">

		<div style="margin-left: 10px;">
			<h3>快捷链接：</h3>
			<ul>查看：<a href="javascript:void();" onclick="rush_jump(this)">马甲</a>&nbsp;&nbsp;<a href="javascript:void();" onclick="rush_jump(this)">相册</a></ul>
			<ul>动作：<a href="javascript:void();" onclick="rush_jump(this)">禁止帐号</a>&nbsp;&nbsp;<a href="javascript:void();" onclick="rush_jump(this)">禁止手机</a></ul>	
		</div>

	</div>        
</div>

<div class="description">
	<h3>说明</h3>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript" src="../_back/js/ajaxfileupload.js"></script>
<script type="text/javascript">
	function upload_logo()
	{
		$.ajaxFileUpload
		(
		{
			url : '../backend/upload_logo.php',
			secureuri : false,
			fileElementId : 'logo_file',
			dataType : 'json',
			data : { user : '<?php echo $_GET['uid']; ?>' },
			success: function( data )
			{
				location.reload();
			}
		}
	)
	}	
	
	function clear_logo()
	{
		if ( confirm( '确认清除头像？' ) )
		{
			$.ajax
			(
			{
				type : "POST",
				url : "../backend/clear_logo.php",
				dataType : 'json',
				data : { user : '<?php echo $_GET['uid']; ?>' },
				success : function( data )
				{
					alert( data.data );
					location.reload();
				}
			}
		);
		}
	}	
		
	mini.parse();

	function submitForm() {
		var form = new mini.Form("#form1");

		form.validate();
		if (form.isValid() == false) return;

		//提交数据
		var data = [];
		data[0] = form.getData();      
		var value = mini.encode(data);  
		post( 'user', "save", value, function (text) {		
			alert(text.message);
			location.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
	);
	}	

	function forbid_user(uid)
	{
		if(confirm("确定禁止这个用户吗？"))
		{
			$.ajax({
				url: "data/forbid_user.php?method=add&uid="+uid,
				dataType: "json",
				success: function (data) {			
					alert('成功禁止');
				}
			});	
		}
		return false;
	}

	function forbid_mobile(uid)
	{
		if(confirm("确定禁止这个手机吗？"))
		{
			$.ajax({
				url: "data/forbid_mobile.php?method=add&uid="+uid+"&reason=图片问题",
				dataType: "json",
				success: function (data) {			
					alert('成功禁止');
				}
			});	
		}
		return false;
	}

	function rush_jump(obj)
	{
		var url = '';
		var title = $(obj).html();
	
		if( title == '禁止帐号' ) forbid_user('<?php echo $_GET['uid']; ?>');
		else if( title == '禁止手机' ) forbid_mobile('<?php echo $_GET['uid']; ?>');
		else
		{
			switch( title )
			{
				case '道具': url = 'user_props.php?user='; break;
				case '聊天': url = 'message.php?user='; break;
				case '说两句': url = 'comment_list.php?user='; break;
				case '马甲': url = 'puppet.php?uid='; break;
				case '相册': url = '../backend/album.php?uid='; break;
				case '动态': url = 'moving.php?user='; break;
			}
	
			url += '<?php echo $_GET['uid']; ?>';
			tab(mini.get("name")+'的'+title+'<?php echo $_GET['uid']; ?>', title, url);
		}
	
	}

	var form = new mini.Form("form1");
	form.loading();
	$.ajax({
		url: "data/user.php?method=detail&uid=" + <?php echo $_GET['uid']; ?>,
		success: function (text) {
			var o = mini.decode(text);
			$('#logo').attr('src',o.logo);
			form.setData(o);
			form.unmask();
		}
	});
</script>