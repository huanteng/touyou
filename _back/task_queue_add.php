<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(101); ?>

<div style="width:98%;">
	<div id="form1">
		<table class="form-table" border="0" cellpadding="1" cellspacing="1">
			<tr>
				<td class="label">类型: </td>
				<td><input name="type" class="mini-textbox" style="width:200px" required="true" vtype="int" />（请输入数字）</td>
			</tr>
			<tr>
				<td class="label">目标资源id：</td>
				<td><input name="target" class="mini-textbox" style="width:200px" value="0" />（图片id、说两句id等，大多数任务不需填本参数）</td>
			</tr>
			<tr>
				<td class="label">目标用户uid：</td>
				<td><input name="user" class="mini-textbox" style="width:200px" value="0" vtype="int" /></td>
			</tr>
			<tr>
				<td class="label">执行时间：</td>
				<td ><input name="time" class="mini-datepicker" style="width:200px" showTime="true"
							format="yyyy-MM-dd HH:mm:ss" />（默认为5分钟后）</td>
			</tr>
			<tr>
				<td class="label">数据：</td>
				<td ><input name="data" class="mini-textarea" style="width:600px;height:160px"  />（每行为key=value串；请正确填写）</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><a class="mini-button" onclick="submitForm()" style="width: 150px;">保存</a></td>
			</tr>
		</table>
	</div>
</div>

<div class="description">
	<h3>说明</h3>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
mini.parse();

var form = new mini.Form("form1");

function submitForm() {
	form.validate();
	if (form.isValid() == false) return;

	//提交数据
	var data = [];
	data[0] = form.getData();
	data[0]['status'] = 1;
	var value = mini.encode(data);  
	post( 'task_queue', "save", value, function (text) {
			alert(text.message);
			location.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}
init_form( 'form1', 'task_queue', "init_add" );

</script>