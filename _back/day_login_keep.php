<?php
require 'head.php';

check_privilege(0);
check_privilege(116);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

var bebin_date, end_date;

function get_s_date( dt )
{
	return mini.formatDate( dt, 'yyyyMMdd' );
}

function get_date( s, dec )
{
	var dt = new Date( s.substr(0, 4), s.substr(4, 2)-1, s.substr(6,2) );
	dt.setDate( dt.getDate() - dec );

	return get_s_date( dt );
}

function faile(jqXHR, textStatus, errorThrown)
{
	alert(jqXHR.responseText);
}

function get( begin_date, end_date )
{
	post( "day_login", "keep", { begin_date: begin_date, end_date: end_date }, get2, faile );
}

function get2( json )
{
	var width = 10;
	var tr = '<tr><td></td>';
	for(var i = 0;i < width;++i)
	{
		var plus = ( i == width - 1 ) ? '+' : '';
		tr += '<td>' + i + plus + '日</td>';
	}
	tr += '</tr>';

	for( var reg_date in json )
	{
		var row = json[ reg_date ];
		tr += '<tr><td>' + reg_date + '</td>';
		for(var i = 0;i < width;++i)
		{
			if( row[ i ] )
			{
				tr += '<td>' + row[ i ] + '</td>';
			}
			else
			{
				tr += '<td></td>';
			}
		}
		tr += '</tr>';
	}

	$('#data').html( tr );
}

////// 在具体页面按需覆盖
function init()
{
	end_date = get_s_date( new Date() );
	begin_date = get_date( end_date, 10 );

	get( begin_date, end_date );
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
					</td>
                    <td style="white-space:nowrap;" id="search">
                        用户：<div id="uid" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id"
					        url="data/user.php?method=search&sortField=name&sortOrder=asc" value="" text=""  onvaluechanged="onValueChanged">     
					        <div property="columns">
					            <div header="名字" field="name" width="30"></div>
								<div header="NPC" field="npc" width="10"></div>
								<div header="登录" field="login" width="25"></div>
					        </div>
					    </div>
						日期：<input id="date" class="mini-input" style="width:60px;" allowInput="true" title="请输入，格式为8位数字，如20131225"/>
						<a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <table id="data" border="1"></table>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<script type="text/javascript">
	function module() { return 'day_login';}
</script>
<?php require 'bottom.php'; ?>
