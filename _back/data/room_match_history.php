<?php

require 'backend.php';

class action extends backend
{
	function table()
	{
		return 'room_match_history';
	}

	function search($data)
	{
		$this->check_privilege('0,113');

		$field = '*';
		$table = $this->table();
		$equal = array( 'id', 'room_id', 'uid1', 'uid2', 'uid3', 'uid3' );
		$like = array();
		$q = array('id', 'room_id', 'uid1', 'uid2', 'uid3', 'uid3');

		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		$four = array(1,2,3,4);
		$user = biz( 'user' );
		$room = biz( 'room' );

		foreach( $result['data'] as $k => $v )
		{
			foreach ( $four as $i )
			{
				if( $v[ 'uid' . $i ] != 0 )
					$v[ 'name' . $i ] = $user->get_name_from_id( $v[ 'uid' . $i ] );
			}

			$v[ 'no' ] = $room->get_field_from_id( $v[ 'room_id' ], 'no', '' );
			$v[ 'winner_name' ] = $user->get_name_from_id( $v[ 'winner_uid' ] );

			$result['data'][$k] = $v;
		}

		$result['data'] = $this->format_datetime( $result['data'], 'make', 'm-d H:i:s' );

		return $result;
	}

	

}

$action = new action();
$action->run();
?>