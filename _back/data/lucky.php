<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'lucky';
	}

	function search( $in )
	{
		$this->check_privilege('0,76');

		if (isset($in['user']) && !is_numeric($in['user']) && $in['user']!='') {
			$user = load('biz.user');
			$in['uid'] = $user->get_id_from_name($in['user']);
		} else {
			$in['uid'] = $this->value($in, 'user', 0);
		}

		$field = '*';
		$table = $this->table();
		$equal = array('uid');
		$like = array();
		$q = array('description');
		if ($this->value($in, 'sortField') == '') {
			$in['sortField'] = 'id';
			$in['sortOrder'] = 'desc';
		}

		$result = parent::find($in, $field, $table, $equal, $like, $q);
		$result['data'] = $this->fill_name_from_uid($result['data'], 'uid', 'name', '');
		$result['data'] = $this->format_datetime($result['data'], 'time');
		$result['data'] = $this->fill_field($result['data'], 'type', 'type_name', array('0' => '系统', '1' => '正常'));

		return $result;
	}

}

$action = new action();
$action->run();
?>