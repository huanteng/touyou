<?php
require 'backend.php';

class action extends backend
{

	function upload($in)
	{
		$this->check_privilege( '0,12' );
		
		$type = $this->value($in, 'type');
		$name = $this->value($in, 'name');
		
		//系统载入页面
		if( $type == 1 )
		{
			if ( isset( $_FILES[$name] ) && $_FILES[$name]['error'] == 0 )
			{
				$dir = '../../images/';
				if ( ! is_dir( $dir ) ) mkdir( $dir, 0755, true );
				$file = 'loading.png';
				
				if ( move_uploaded_file( $_FILES[$name]['tmp_name'], $dir . $file ) )
				{
					//更新设置值
					$this->db->set('config', array('value'=>'http://touyou.mobi/images/'.$file,'time'=>time()), array('section'=>'loading'));
					//更新
					$this->db->set('picture', array('path'=>'images/'.$file,'memo'=>$in['memo']), array('type'=>'1'));
				}
			}
		}
		//广告1广告2广告3
		elseif( $type == 2 || $type == 3 || $type == 4 )
		{
			//对应的字典id
			$dict = array(2=>33,3=>39,4=>36);
			$dict_id = isset($dict[$type]) ? $dict[$type] : 0;
			
			if ( isset( $_FILES[$name] ) && $_FILES[$name]['error'] == 0 && $dict_id > 0 )
			{
				$dir = '../../e/images/';
				if ( ! is_dir( $dir ) ) mkdir( $dir, 0755, true );
				$file = 'ad'.date('YmdHis').'.png';
				
				if ( move_uploaded_file( $_FILES[$name]['tmp_name'], $dir . $file ) )
				{		
					//更新设置值
					$this->db->set('dictionary', array('value'=>'http://touyou.mobi/e/images/'.$file,'time'=>time()), array('id'=>$dict_id));
					//更新
					$this->db->set('picture', array('path'=>'e/images/'.$file,'memo'=>$in['memo']), array('type'=>$type));
				}
			}
			
		}
		//道具
		elseif( $type == 5 )
		{
			$props_id = $this->number($in, 'props_id');
			if ( isset( $_FILES[$name] ) && $_FILES[$name]['error'] == 0 && $props_id != 0 )
			{
				$dir = '../../frontend/logo/';
				if ( ! is_dir( $dir ) ) mkdir( $dir, 0755, true );
				
				$file = 'props'.$props_id.'.png';
				
				if ( move_uploaded_file( $_FILES[$name]['tmp_name'], $dir . $file ) )
				{
					//更新
					$this->db->add('picture', array('path'=>'logo/props/'.$file,'memo'=>$in['memo']));
				}
			}
		}
		
		header('location:../picture_upload.php');
	}
}

$action = new action();
$action->run();
?>