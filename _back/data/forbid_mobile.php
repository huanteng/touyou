<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'forbid_mobile';
	}

	function search($data)
	{
		$this->check_privilege('0,59');

		$field = '*';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array('reason');

		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'id';
			$data['sortOrder'] = '';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);
		$result['data'] = $this->format_datetime($result['data'], 'begin_time', 'datetime');
		$result['data'] = $this->format_datetime($result['data'], 'end_time', 'datetime');
		return $result;
	}

	function row($data)
	{
		$this->check_privilege('0,59');

		if ($this->value($data, 'begin_time', '') != '') {
			$data['begin_time'] = strlen($data['begin_time']) > 11 ? strtotime($data['begin_time']) : strtotime(date('Y-') . $data['begin_time']);
		} else {
			$data['begin_time'] = time();
		}

		if ($this->value($data, 'end_time', '') != '') {
			$data['end_time'] = strlen($data['end_time']) > 11 ? strtotime($data['end_time']) : strtotime(date('Y-') . $data['end_time']);
		} else {
			$data['end_time'] = $data['begin_time'] + 30 * 86400;
		}

		return $data;
	}

	// <editor-fold defaultstate="collapsed" desc="添加禁止手机">
	function add($in)
	{
		$reason = $this->value($in, 'reason', '');
		if ($this->number($in, 'uid')) {
			$user = load('biz.user');
			$info = $user->get_from_id($in['uid']);
			if ($this->value($info, 'login_imei')) {
				$forbid_mobile = load('biz.forbid_mobile');
				return $forbid_mobile->add(array('imei' => $info['login_imei'], 'reason' => $reason, 'begin_time' => time(), 'end_time' => time() + 30 * 86400));
			}
		}
	}
	// </editor-fold>
}

$action = new action();
$action->run();
?>