<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'user';
	}

	function search($data)
	{
		$this->check_privilege('0,53');

		if (!isset($data['npc']))
			$data['npc'] = 1;

		$field = 'id as uid, name, login';
		$table = $this->table();
		$equal = array('npc');
		$like = array();
		$q = array('name');
		if (!isset($data['sortField'])) {
			$data['sortField'] = 'id';
			$data['sortOrder'] = '';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		$result['data'] = $this->format_datetime($result['data'], 'login');
		$result['data'] = $this->fill_props($result['data'], 'uid', array(1 => 'gold'));

		return $result;
	}

	/*
	  等待上线的NPC
	 */

	function wait_online($data)
	{
		$this->check_privilege('0,54');

		$data['type'] = 1013;
		$data['status'] = 1;

		$field = 'id,user,time';
		$table = 'task_queue';
		$equal = array('type', 'status');
		$like = array();
		$q = array();
		if (!isset($data['sortField'])) {
			$data['sortField'] = 'time';
			$data['sortOrder'] = 'desc';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		$result['data'] = $this->fill_name_from_uid($result['data'], 'user', 'name', '');
		$result['data'] = $this->format_datetime($result['data'], 'time');
		$result['data'] = $this->fill_props($result['data'], 'user', array(1 => 'gold'));

		return $result;
	}
	
	// <editor-fold defaultstate="collapsed" desc="获得npc">
	function get_npc($in)
	{
		$data = $this->value($in, 'data', $in);
		$sex = $this->number($data, 'sex', '');
		$online = $this->number($data, 'online');
		
		$user = load('biz.user');
		if( $online == 0 )
		{
			if( $sex == 0 || $sex == 1 ) return $user -> get1('id,name,sex', array('npc'=>1, 'sex'=>$sex), 'rand()');
			else return $user -> get1('id,name,sex', array('npc'=>1), 'rand()');
		}
		else
		{
			if( $sex == 0 || $sex == 1 ) 
			{
				$npc = biz('online')->get1('user', array('npc'=>1, 'sex'=>$sex), 'rand()');
				if(isset($npc['user'])) return $user -> get1('id,name,sex', array('id'=>$npc['user']));
			}
		}
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="npc 视频认证列表">
	function video_check($in)
	{
		$out = array();
		
		$in['npc'] = 1;
		$in['pageIndex'] = $this->number($in, 'pageIndex', 0);
		$in['pageSize'] = $this->number($in, 'pageSize', 10);
		$in['sortField'] = 'u.id';
		$in['sortOrder'] = 'desc';
		
		$where = array('u.id=e.id and u.npc=1 and e.is_real=0');
		
		if($this->value($in, 'q') != '')
		{
			$uid = biz('user')->get_id_from_name($in['q']);
			if($uid > 0) $where[] = 'u.id='.$uid;
		}
		
		if($this->value($in, 'sex') != '')
		{
			$where[] = 'u.sex='.$in['sex'];
		}
		
		$where = $this->db->term( $where, ' and ' );
		
		if( $this->value( $in, 'sortField', '') != '' )
		{
			$orderby = $in['sortField'];
			if( isset( $in['sortOrder'] ) ) $orderby .= ' ' . $in['sortOrder'];
		}
		
		if( isset( $in['pageIndex'] ) && isset( $in['pageSize'] ) )
		{
			$limit = ( $in['pageIndex'] * $in['pageSize'] ) . ', ' . $in['pageSize'];
		}
		
		$sql = "select count(1) as total from user u,user_ex e";
		if( $where != '' ) $sql .= ' where ' . $where;
		$data = $this->db->unique( $sql );
		$out['total'] = $data['total'] + 0;
		
		if( $out['total'] == 0 )
		{
			$result['data'] = array();
		}
		else
		{
			$sql = "select * from user u,user_ex e";
			if( $where != '' ) $sql .= ' where ' . $where;
			if( isset( $orderby ) ) $sql .= ' order by ' . $orderby;
			if( isset( $limit ) ) $sql .= ' limit ' . $limit;
			$result['data'] = $this->db->select( $sql );
		}
				
		$album = load('biz.album');
		$user = load('biz.user');
		foreach($result['data'] as $key => $value)
		{
			$value['logo'] = $user->logo($value['id']);
			$out['data'][$key] = $value;
			$temp = $album -> get('id, type, path', array('user'=>$value['id']), 'type desc,id desc', '20');
			$album_list = array();
			foreach($temp as $a_k => $value)
			{
				$album_list[$a_k]['id'] = $value['id'];
				$album_list[$a_k]['type'] = $value['type'];
				$album_list[$a_k]['path'] = config('frontend') . 'logo/' . $value['path'];
			}
			$out['data'][$key]['album_list'] = $album_list;
		}
		return $out;
	}
	// </editor-fold>

}

$action = new action();
$action->run();
?>