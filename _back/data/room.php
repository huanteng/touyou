<?php

require 'backend.php';

class action extends backend
{
	function table()
	{
		return 'room';
	}

	function search($data)
	{
		$this->check_privilege('0,111');

		$field = '*';
		$table = $this->table();
		$equal = array('id', 'level');
		$like = array();
		$q = array('id', 'no');

		if ($this->value($data, 'sortField') == '')
		{
			$data['sortField'] = '`real`';
			$data['sortOrder'] = 'desc';
		}
		elseif( $data['sortField'] == 'real' )
		{
			$data['sortField'] = '`real`';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		$room = biz( 'room' );
		$status = $room->status_dict();
		$level = $room->level_dict();
		$four = array(1,2,3,4);
		$user = biz( 'user' );

		foreach( $result['data'] as $k => $v )
		{
			$v['status'] = $status[ $v[ 'status' ] ];
			$v['level_name'] = $level[ $v[ 'level' ] ];
			foreach ( $four as $i )
			{
				$uid = $v[ 'uid' . $i ];
				if( $uid != 0 )
				{
					$v[ 'name' . $i ] = $user->get_name_from_id( $uid );

					if( $user->is_npc( $uid ) )
					{
						$v[ 'npc' . $i ] = 1;
					}
				}
			}

			$v[ 'ip' ] = $v[ 'server_id' ] . ':' . $v[ 'ip' ] . ':' . $v[ 'port' ];

			unset( $v[ 'port' ] );
						
			$result['data'][$k] = $v;
		}

		$result['data'] = $this->format_datetime( $result['data'], 'time', 'm-d H:i:s' );

		return $result;
	}

	function level($data)
	{
		return $this->keyvalue2combo( biz('room')->level_dict(), 'id', 'text' );
	}

	function status($data)
	{
		return $this->keyvalue2combo( biz('room')->status_dict(), 'id', 'text' );
	}

}

$action = new action();
$action->run();
?>