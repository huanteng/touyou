<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'day_login';
	}

	function search($data)
	{
		$this->check_privilege('0,18');

		$user = load('biz.user');

		if (isset($data['uid']) && !is_numeric($data['uid']) && $data['uid'] != '')
		{
			$data['uid'] = $user->get_id_from_name( $data['uid'] );
		}

		$field = '*';
		$table = $this->table();
		$equal = array('uid', 'reg_date', 'login_date');
		$like = array();
		$q = array();

		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}

		if( isset( $data[ 'q' ] ) )
			$data[ 'q' ] = addslashes( $data[ 'q' ] );

		$result = parent::find($data, $field, $table, $equal, $like, $q );

		foreach( $result['data'] as $k => $v )
		{
			$info = $user->get_from_id( $v['uid'] );
			$v['name'] = $info[ 'name' ];

			$v[ 'dec' ] = ( $v[ 'login_date' ] - $v[ 'reg_date' ] ) / 86400;
			$v[ 'reg_date' ] = date( 'Y-m-d', $v[ 'reg_date' ] );
			$v[ 'login_date' ] = date( 'Y-m-d', $v[ 'login_date' ] );

			$result['data'][$k] = $v;
		}

		return $result;
	}

	/* 统计留存率
		参数：
		begin_date：开始日期
		end_date：结束日期
		返回：多行数据，key：reg_date, value为多行，分别为： login_date => count
	*/
	function keep( $in )
	{
		$this->check_privilege('0,18');

		extract( $in );

		if( !isset( $begin_date ) )
		{
			$end_date = load('time')->today();
			$begin_date = $end_date - 10 * 86400;
		}

		$sql = " SELECT reg_date, login_date, COUNT( * ) as count FROM day_login WHERE reg_date BETWEEN $begin_date and $end_date" .
			' GROUP BY reg_date, login_date order by reg_date';

		$data = load('database')->select( $sql );
		$out = array();

		// 最多只统计日差
		$width = 9;

		foreach( $data as $v )
		{
			$reg_date = date( 'Y-m-d', $v[ 'reg_date' ] );
			$dec = ( $v[ 'login_date' ] - $v[ 'reg_date' ] ) / 86400;

			if( !isset( $out[ $reg_date ] ) )
			{
				$out[ $reg_date ] = array();
			}

			if( $dec > $width )
			{
				if( !isset( $out[ $reg_date ][ $width ] ) )
				{
					$out[ $reg_date ][ $width ] = 0;
				}

				$out[ $reg_date ][ $width ] += $v[ 'count' ];
			}
			else
			{
				$out[ $reg_date ][ $dec ] = $v[ 'count' ];
			}
		}

		return $out;
	}

}

$action = new action();
$action->run();
?>