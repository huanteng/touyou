<?php
require 'backend.php';

class action extends backend
{
	function table() 
    {
		return 'rich';
	}
	
	function search($data)
	{
		$this->check_privilege( '0,83' );

		$other = '';
		if( isset($data['time']) && $data['time'] != '' )
        {
            $start_time = strtotime($data['time']);
            $end_time = $start_time + 3600;
            $other = ' `time` between '.$start_time.' and '.$end_time.' ';
        }
		
		$field = '*';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array();
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'time';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q, 'and', $other );
		$result['data'] = $this->format_datetime( $result['data'], 'time' );
		return $result;
	}
	
}

$action = new action();
$action->run();
?>