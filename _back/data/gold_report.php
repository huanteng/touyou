<?php
require 'backend.php';

class action extends backend
{
	function table() 
    {
		return 'gold_report';
	}
	
	//默认按日查询，以小时统计
	function search($data)
	{
		$this->check_privilege( '0,83' );

		$other = '';
		if( isset($data['date']) && $data['date'] != '' )
        {
            $start_time = strtotime($data['date']);
            $end_time = $start_time + 86399;
            $other = ' `time` between '.$start_time.' and '.$end_time.' ';
        }
		
		$field = '*';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array();
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'time';
			$data['sortOrder'] = 'desc';
		}
		$in['pageSize'] = 24;
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q, 'and', $other );
		$result['data'] = $this->format_datetime( $result['data'], 'time' );
		return $result;
	}
	
	//按月查询，以日统计
	function day_of_month($in)
	{
		$this->check_privilege( '0,83' );

		$start_time = strtotime( date('Y-m-1') );
		if( isset($in['date']) && $in['date'] != '' )
        {
            $time = strtotime( $in['date'] ); 
            $start_time = strtotime( date('Y-m-1',$time) );
        }
        $end_time = strtotime(date('Y-m-t 23:59:59',$start_time));
        
		$other = ' `time` between '.$start_time.' and '.$end_time.' ';
		
		$field = '`time`,challenge,challenge2,integral,integral2,lucky,lucky2,task,props,props2,pay,other,other2';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array();
		if( $this->value( $in, 'sortField' ) == '' )
		{
			$in['sortField'] = 'time';
			$in['sortOrder'] = 'desc';
		}
		$in['pageSize'] = 1000;
		
		$result = parent::find( $in, $field, $table, $equal, $like, $q, 'and', $other );
				
		$new_result = array();
		foreach($result['data'] as $key=>$value)
		{
			$date = date('Y-m-d',$value['time']);
			
			foreach( array('challenge','challenge2','integral','integral2','lucky','lucky2','task','props','props2','pay','other','other2') as $field )
			{	
				if( !isset($new_result[$date][$field]) ) 
				{
					$new_result[$date][$field] = 0;
				}
				else
				{
					$num = isset($value[$field]) ? $value[$field] : 0;
					$new_result[$date][$field] += isset($new_result[$date][$field]) ? $num : 0;
				}	
			}
			//$new_result[$date] = $field_array;
			$new_result[$date]['date'] = $date;	
		}
		
		$result['data'] = array();
		foreach ($new_result as $value)
		{
			$result['data'][] = $value;
		}
		
		$result['total'] = count($new_result);
		
		return $result;
	}
	
	//按月查询，以周统计
	function week_of_month($in)
	{
	}
}

$action = new action();
$action->run();
?>