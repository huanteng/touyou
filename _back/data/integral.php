<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'integral_match';
	}

	function search( $data )
	{
		$this->check_privilege( '0,31' );
		
		$data['page'] = $this->number($data, 'pageIndex') + 1;
		$data['size'] = $this->number($data, 'pageSize');
		
		$prize_pool = load('biz.prize_pool');
		$result = $prize_pool -> lists( $data );
		$result['total'] += 0; 
		return $result;
	}
	
	function match( $data )
	{		
		$this->check_privilege( '0,31' );
		
		$field = '*';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array();

		if( $this->value( $data, 'sortField' ) == '' )	
		{
			$data['sortField'] = 'time';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->fill_name_from_uid( $result['data'], 'user_1', 'user_1', '' );
		$result['data'] = $this->fill_name_from_uid( $result['data'], 'user_2', 'user_2', '' );
		$result['data'] = $this->format_datetime( $result['data'], 'time' );
			
		return $result;
	}

}

$action = new action();
$action->run();
?>