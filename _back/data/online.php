<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'online';
	}

	function search( $data )
	{
		$this->check_privilege( '0,26' );
		
		$data = $this->value($data, 'data', $data);
		
		$field = '*';
		$table = $this->table();
		$equal = array( 'user','npc','sex' );
		$like = array();
		$q = array();
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'last';
			$data['sortOrder'] = 'desc';
		}
		else
		{
			if( $data['sortField'] == 'online' )
			{
				$data['sortField'] = 'login';
			}
		}

		if( !isset( $data[ 'npc' ] ) )
		{
			$data[ 'npc' ] = 0;
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$now = time();
		$user = biz( 'user' );
		$session = biz( 'session' );
		foreach( $result['data'] as $k => $v )
		{
			$uid = $v['user'];
			
			$second = $now - $v['login'];
			if ( $second >= 86400 ) 
				$v['online'] = round( $second / 86400, 2 ) . '天';
			else if ( $second >= 3600 ) 
				$v['online'] = round( $second / 3600, 2 ) . '小时';
			else if ( $second >= 60 ) 
				$v['online'] = round( $second / 60 ) . '分钟';
			else 
				$v['online'] = $second . '秒';

			if( $v[ 'status_time' ] < $now - 60 )
			{
				$v['status'] = '';
			}
			
			$last = $session->get_user_key( $uid, 'online_time', 0 );
			$v[ 'last' ] = date( 'H:i:s', $last );

			$info = $user->get_from_id( $uid );
			$v[ 'version' ] = $info[ 'version' ];

			// <editor-fold defaultstate="collapsed" desc="马甲数">
			/*$imei1 = $info['register_imei'];
			$imei2 = $info['login_imei'];

			$v[ 'puppet' ] = $user->count( array('( register_imei="'.$imei1.'" and register_imei !="" and register_imei !="0" ) or ( login_imei="'.$imei2.'" and login_imei !="" and login_imei !="0" )'));
			*/// </editor-fold>
			
			$result['data'][ $k ] = $v;
		}
		
		$result['data'] = $this->fill_name_from_uid( $result['data'], 'user', 'name', '' );
		$result['data'] = $this->format_datetime( $result['data'], 'login' );
		$result['data'] = $this->format_datetime( $result['data'], 'status_time', 'H:i' );
		$result['data'] = $this->fill_props( $result['data'], 'user', array( 1 => 'gold') );

		return $result;
	}
	
	/*
	 * 是否在线
	 * 参数：uid
	 */
	function is_online( $in )
	{
		$in = $this->value($in, 'data');
		$uid = $this->number($in, 'uid');
		
		$result = array( 'code' => 0 );
		
		$temp = biz('online')->exists( array('user'=>$uid) );
		$result['code'] = $temp ? 1 : 0;

		return $result;
	}
}

$action = new action();
$action->run();
?>