<?php

require 'backend.php';

class action extends backend
{
	function table()
	{
		return 'room_run';
	}

	function search($data)
	{
		$this->check_privilege('0,114');

		$field = '*';
		$table = $this->table();
		$equal = array( 'id', 'room_id', 'uid' );
		$like = array();
		$q = array();

		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		$user = biz( 'user' );
		$room = biz( 'room' );
		$type = biz('room_run')->type_dict();
		
		foreach( $result['data'] as $k => $v )
		{
			if( $v[ 'uid' ] != 0 )
				$v[ 'name' ] = $user->get_name_from_id( $v[ 'uid' ] );
			if( $v[ 'uid2' ] != 0 )
				$v[ 'name2' ] = $user->get_name_from_id( $v[ 'uid2' ] );

			$v[ 'no' ] = $room->get_field_from_id( $v[ 'room_id' ], 'no', '' );
			$v['type'] = $type[ $v[ 'type' ] ];

			$result['data'][$k] = $v;
		}

		$result['data'] = $this->format_datetime( $result['data'], 'run', 'm-d h:i:s' );

		return $result;
	}

	

}

$action = new action();
$action->run();
?>