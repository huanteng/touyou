<?php

require 'backend.php';

class action extends backend
{
	function table()
	{
		return 'match_summary';
	}

	function search($data)
	{
		$this->check_privilege('0,114');

		$field = '*';
		$table = $this->table();
		$equal = array( 'id' );
		$like = array();
		$q = array();

		$user = biz( 'user' );

		if ( $this->value($data, 'sortField') == '') {
			$data['sortField'] = 's';
			$data['sortOrder'] = 'desc';
		}

		if( $data['sortField'] == 's' )
		{
			$data['sortField'] = 's1+s2+s3+cs1+cs2+cs40';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		$props = biz( 'props' );

		$info = array( 'w1'=>0, 'l1'=>0, 'd1'=>0, 's1'=>0, 'w2'=>0, 'l2'=>0, 'd2'=>0,
			's2'=>0, 'w3'=>0, 'l3'=>0, 'd3'=>0, 's3'=>0,'cw1'=>0, 'cl1'=>0, 'cs1'=>0,
			'cw2'=>0, 'cl2'=>0, 'cs2'=>0, 'cw40'=>0, 'cl40'=>0, 'cs40'=>0, );
		foreach( $result['data'] as $k => $v )
		{
			foreach( $info as $k1 => $null)
			{
				$info[ $k1 ] += $v[ $k1 ];
			}

			$v[ 'gold' ] = $props->gold( $v[ 'id' ] );
			$v[ 'name' ] = $user->get_name_from_id( $v[ 'id' ] );

			if( $user->is_online( $v[ 'id' ] ) )
			{
				$v[ 'online' ] = 1;
			}
			
			$result['data'][$k] = $v;
		}

		$info[ 'uid' ] = 0;
		$info[ 'name' ] = '本页统计';

		$result['data'][] = $info;

		return $result;
	}

	/** 向用户发起挑战
	 * 参数：
	 *	uid2: 被挑战者
		challenge_count: 次数
		sex: 性别
		uid: 挑战者
		props_id: 类型
		count1: 挑战金
	 	count2: 挑战金
		rate: 干预率
	 */
	function send( $in )
	{
		$data = $in[ 'data' ];

		$now = time();
		$data[ 'challenge_time' ] = $now;
		$data[ 'expires' ] = $now + 86400;

		$challenge_send = biz('challenge_send');
		$challenge_send->add( $data );
		return $challenge_send->out( '操作成功' );
	}

}

$action = new action();
$action->run();
?>