<?php

require 'backend.php';

class action extends backend
{
	function table()
	{
		return 'npc_album';
	}

	function search( $data )
	{
		$this->check_privilege('0,29');

		$page = isset( $data['pageIndex'] ) ? $data['pageIndex'] : 1;

		if( $page == 0 ) $page = 1;

		$npc_album = biz( 'npc_album' );
		$data = $npc_album->get_page( 'distinct(parent_id)', array(), 'uid', $page );

		$user = biz( 'user' );

		$out = array();
		foreach( $data as $v )
		{
			$album_data = $npc_album->get( '*', array( 'parent_id' => $v['parent_id'] ) );
			$row = array( 'parent_id' => $v['parent_id'], 'album' => array() );
			foreach( $album_data as $v2 )
			{
				if( !isset( $row[ 'uid' ] ) )
				{
					$row[ 'uid' ] = $v2[ 'uid' ];
				}

				if( count( $row[ 'album' ] ) > 5 ) break;

				$row[ 'album' ][] = array( 'id' => $v2[ 'id' ], 'path' => $v2[ 'path' ] );
			}

			if( $row[ 'uid' ] == 0 )
			{
				unset( $row[ 'uid' ] );
			}
			else
			{
				$row[ 'name' ] = $user->get_name_from_id( $row[ 'uid' ] );
			}

			$out[] = $row;
		}

		$out = array( 'data' => $out );

		$info = $npc_album->get1( 'count(distinct(parent_id)) as c',array() );
		$out[ 'total' ] = $info[ 'c' ];

		return $out;
	}
	
	// <editor-fold defaultstate="collapsed" desc="添加为NPC">
	function add_npc($in)
	{
		$this->check_privilege('0,29');

		$sex = $this->number($in, 'sex', 0);
		
		$grab_user = $this->number($in, 'grab_user', 0);
		if ($grab_user != 0) {
			$temp = $this->db->get('name', 'grab_user', array('id' => $grab_user));
			$grab_name = $this->value($temp[0], 'name');

			//检查用户名是否存在
			$user = load('biz.user');
			$temp = $this->db->get('id', 'user', array('name' => $grab_name));

			$cookie = load( 'cookie' );
			$account = $cookie->get( 'account', true);
			
			if (!isset($temp[0]['id'])) {
				$uid = $this->db->add('user', array('name' => $grab_name, 'npc'=>1, 'register' => time(), 'creater' => $account, 'city' => '神秘岛', 'pass' => '4f29ae61a4188f3e45c428f7a9148073', 'sex' => $sex));
				//user_ex
				$this->db->add('user_ex', array('id'=>$uid,'vip'=>0, 'vip_expires'=>0, 'integral'=>0));
				
				$this->db->set('grab_user',array('uid'=>$uid),array('id'=>$grab_user));

				//某个用户图片
				$pic = $this->db->get('*', 'grab_album', array('grab_user' => $grab_user));
				
				$grab_album = load('biz.grab_album');
				$logo_dir = config('logo_base_dir');	
				
				foreach ($pic as $key => $value) {
					
					//用第一张图做logo
					if ($key == 0) {	
						$source = '../../frontend/g/' . $value['path'];
						$target_dir = $user->logo_dir($uid);
						if (!is_dir($logo_dir . $target_dir))
							mkdir($logo_dir . $target_dir, 0755, true);
						$logo_filename = $target_dir . uniqid() . '.jpg';
						copy($source . '.s.jpg', $logo_dir . $logo_filename);
						$this->db->set('user', array('logo' => $logo_filename), array('id' => $uid));
					}
					
					//转移图片
					$grab_album -> move_img( array('uid'=>$uid,'grab_album_id'=>$value['id'],'path'=>$value['path'],'time'=>time()+$key*rand(60,70)) );
					
					if($key>=2)
					{
						break;//只发三张
					}
				}

				$out = array('code' => 1);
			} else {
				$out = array('message' => '系统已经有这个名字，无法添加');
			}
		}
		return $out;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="转移单张图片">
	/*
	 * 参数：
	 * 	id
	 */
	function upload( $in )
	{
		$this->check_privilege('0,29');

		$in = $in[ 'data' ];
		$id = $in[ 'id' ];

		biz('npc_album')->upload( $id );
	}
	// </editor-fold>

	/**
	 * 删除图片
	 * 参数：
	 * 	id：可选，要删除的行id
	 * 	parent_id：可选，要删除的组id
	 */
	// <editor-fold defaultstate="collapsed" desc="删除图片">
	function del( $in )
	{
		$this->check_privilege('0,29');

		$in = $in[ 'data' ];

		$npc_album = biz( 'npc_album' );
		$data = $npc_album->get( '*', $in );
		$path = config( 'logo_base_dir' );
		foreach( $data as $v )
		{
			$source = $path . 'npc/' . $v['path'];
			unlink($source);
			unlink($source . '.s.jpg');	// 20131208，旧数据存在小图，以后可能用程序批量删除，则不需执行本语句

			$npc_album->del( $v[ 'id' ] );
		}

		return $npc_album->out( '删除成功' );
	}
	// </editor-fold>
	
}

$action = new action();
$action->run();
?>