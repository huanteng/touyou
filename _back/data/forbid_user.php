<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'forbid_user';
	}

	function search( $data )
	{		
		$this->check_privilege( '0,58' );
		
		$user = load('biz.user');
		if (isset($data['q']) && !is_numeric($data['q']) && $data['q'] != '') {
			$data['uid'] = $user->get_id_from_name($data['q']);
			unset($data['q']);
		}
		
		$field = '*';
		$table = $this->table();
		$equal = array('uid');
		$like = array();
		$q = array('reason');

		if( $this->value( $data, 'sortField' ) == '' )	
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->fill_name_from_uid( $result['data'], 'uid', 'name', '' );
		
		$result['data'] = $this->format_datetime( $result['data'], 'begin_time', 'datetime' );
		$result['data'] = $this->format_datetime( $result['data'], 'end_time', 'datetime' );	
			
		return $result;
	}

	// <editor-fold defaultstate="collapsed" desc="添加禁止用户登录">
	function add($in)
	{
		$forbid_user = load('biz.forbid_user');
		return $forbid_user->add($in);
	}
	// </editor-fold>

}

$action = new action();
$action->run();
?>