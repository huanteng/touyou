<?php
require 'backend.php';

class action extends backend
{
	function table() 
    {
		return 'event_gift';
	}
	
	function drop()
	{
		$this->check_privilege( '0,76' );
		
		$result = $this->search( array('pageSize'=>100) );
		return $result['data'];
	}
	
	function search($data)
	{
		$this->check_privilege( '0,76' );

		$field = '*';
		$table = 'event_gift';
		$equal = array();
		$like = array();
		$q = array('name');
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = '';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->fill_field($result['data'], 'rule', 'rule_name', array(0=>'特殊规则才可抽',1=>'正常'));
		$props = load('biz.props');
		$result['data'] = $this->fill_field($result['data'], 'props_id', 'props', $props->dict());
        		
		return $result;
	}
	
}

$action = new action();
$action->run();
?>