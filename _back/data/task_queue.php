<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'task_queue';
	}
    
	function search( $in )
	{
		$this->check_privilege( '0,34' );

		$task_queue = biz('task_queue');

		$where = array();
		$other = join(' and ',$where);

		if( isset($in['user']) && $in['user'] != '' && !is_numeric($in['user']) )
		{
			$user = load('biz.user');
			$in['user'] = $user->get_id_from_name( $in['user'] );
		}
		
		$field = '*';
		$table = $task_queue->value( $in, 'table', $this->table() );
		$equal = array( 'type', 'user', 'creater', 'status' );
		$like = array();
		$q = array();
		if( $this->value( $in, 'sortField' ) == '' )
		{
			$in['sortField'] = 'id';
			$in['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $in, $field, $table, $equal, $like, $q, 'and', $other );
				
		$result['data'] = $this->fill_user( $result['data'], 'user', array('name'=>'name','npc'=>'npc') );
		$result['data'] = $this->format_datetime($result['data'], 'create');
		$result['data'] = $this->format_datetime($result['data'], 'time');

		$task_type = $task_queue->type_dict();
		$task_status = $task_queue->status_dict();
		
		$user = biz( 'user' );
		$administrator = biz( 'administrator' );
		
		foreach( $result['data'] as $k => $v )
		{
			if( isset( $task_type[$v['type']] ) )
			{
				$v['type_name'] = $task_type[$v['type']];
			}
			else
			{
				$v['type_name'] = '错误';
				$task_queue->error( '未定义的任务类型。type：' . $v['type'] );
			}

			if( isset( $task_status[$v['status']] ) )
			{
				$v['status'] = $task_status[$v['status']];
			}
			else
			{
				$v['status'] = '错误';
				$task_queue->error( '未定义的状态类型。type：', $v );
			}
			
			$v['user_name'] = ( $v['user'] > 0 ) ? $user->get_name_from_id( $v['user'] ) : '随机未定';
			$v['creater_name'] = ( $v['creater'] > 0 ) ? $administrator->get_name_from_id( $v['creater'] ) : '';
			
			$content = '';
			if ( $v['data'] != '' )
			{
				$v['data'] = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $v['data'] );
				$lize = unserialize($v['data']);
	
				switch( $v['type'] )
				{
					case 1:
					$content = isset( $lize['content'] ) ? $lize['content'] : '';
					break;

					case 2:
					foreach( $lize as $l )
					{
						$content .= isset( $l['content'] ) ? $l['content'].'<br/>' : '';
					}
					break;

					case 4:
					case 8:
					$content = isset( $lize['content'] ) ? $lize['content'] : '';
					break;

					case 6:
					foreach( $lize as $l )
					{
						$content .= isset( $l['name'] ) ? $l['name'].'上传了图片 '.$l['path'].' <br/>' : '';
					}
					break;
				}
			}
			$v['content'] = $content;

			$result['data'][$k] = $v;
		}

		return $result;
	}
	
	function type()
	{
		$task_type = biz('task_queue')->type_dict();
		foreach($task_type as $k => $v )
		{
			$out[] = array('id'=>$k,'text'=>$v);
		}
		return $out;
	}

	// 增加系统队列，初始化
	function init_add($in)
	{	
		$out = array( 'target' => 0, 'user' => 0, 'time' => date('Y-m-d H:i:s', time() + 300) );
		return $out;
	}

	function row($in)
	{
		$in[ 'time' ] = strtotime($in['time']);
		$in[ 'creater' ] = load( 'cookie' )->get( 'account', true);
		
		if( isset( $in[ 'data' ] ) && $in[ 'data' ] != '' )
		{
			$in[ 'data' ] = load( 'string' )->to_json( $in[ 'data' ] );
		}

		return $in;
	}

	function run1( $in )
	{
		$id = $this->php_json_decode( $in['data'] );
		
		biz('task_queue')->run( $id );

		return array( 'code' => 1, 'message' => '成功操作' );
	}

	// <editor-fold defaultstate="collapsed" desc="修改滚动条">
	function init_ad($in)
	{
		$config = biz( 'config' );

		$out = array();

		$data = $config->get1( 'value', array( 'section' => 'home', 'key' => 'activity_words' ) );
		$out[ 'ad' ] = $this->value( $data, 'value' );

		$data = $config->get1( 'value', array( 'section' => 'home', 'key' => 'activity_jump' ) );
		$out[ 'jump' ] = $this->value( $data, 'value' );

		$data = $config->get1( '*', array( 'section' => 'home', 'key' => 'activity_jump_data' ) );
		$out[ 'jump_data' ] = $this->value( $data, 'value' );
		$out[ 'is_json' ] = $this->value( $data, 'is_json', 0 );
		
		$data = biz('task_queue')->get1( 'time', array( 'type' => 7 ), 'id desc' );
		$out[ 'time' ] = date('Y-m-d H:i:s', $this->value( $data, 'time', time() ) );

		return $out;
	}
	function set_ad( $in )
	{
		$data = $this->php_json_decode( $in['data'] );
		$data[ 'time' ] = strtotime( $data[ 'time' ] );
		
		biz('task_queue')->create( $data );

		return array( 'code' => 1, 'message' => '成功操作' );
	}
	// </editor-fold>
	
}

$action = new action();
$action->run();
?>