<?php

require 'backend.php';

class action extends backend
{
	function table()
	{
		return 'challenge_send';
	}

	function search($data)
	{
		$this->check_privilege('0,119');

		$field = '*';
		$table = $this->table();
		$equal = array( 'id', 'uid', 'uid2' );
		$like = array();
		$q = array();

		$user = biz( 'user' );

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		$props = biz( 'props' );

		foreach( $result['data'] as $k => $v )
		{
			$v[ 'gold' ] = $props->gold( $v[ 'id' ] );
			$v[ 'name' ] = ( $v[ 'uid' ] == 0 )? '-' : $user->get_name_from_id( $v[ 'uid' ] );
			$v[ 'name2' ] = $user->get_name_from_id( $v[ 'uid2' ] );
			$v[ 'online' ] = $user->is_online( $v[ 'uid2' ] );

			if( $user->is_online( $v[ 'id' ] ) )
			{
				$v[ 'online' ] = 1;
			}
			
			$result['data'][$k] = $v;
		}

		$result['data'] = $this->format_datetime( $result['data'], 'challenge_time' );
		$result['data'] = $this->format_datetime( $result['data'], 'expires' );
		$result['data'] = $this->format_datetime( $result['data'], 'time' );

		return $result;
	}

	/** 向用户发起挑战
	 * 参数：
	 *	uid2: 被挑战者
		challenge_count: 次数
		sex: 性别
		uid: 挑战者
		props_id: 类型
		count1: 挑战金
	 	count2: 挑战金
		rate: 干预率
	 */
	function send( $in )
	{
		$data = $in[ 'data' ];

		$now = time();
		$data[ 'challenge_time' ] = $now;
		$data[ 'expires' ] = $now + 86400;

		$challenge_send = biz('challenge_send');
		$challenge_send->add( $data );
		return $challenge_send->out( '操作成功' );
	}

}

$action = new action();
$action->run();
?>