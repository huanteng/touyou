<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'challenge_rank';
	}

	function search( $data )
	{		
		$this->check_privilege( '0,56' );
		
		$field = '*';
		$table = $this->table();
		$equal = array( '' );
		$like = array();
		$q = array( '' );

		if( $this->value( $data, 'sortField' ) == '' )	
		{
			$data['sortField'] = 'gain';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->fill_user($result['data'], 'user', array('name' => 'name', 'npc' => 'npc'));
			
		return $result;
	}

}

$action = new action();
$action->run();
?>