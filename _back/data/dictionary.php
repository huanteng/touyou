<?php
require 'backend.php';

class action extends backend
{
	function table() 
	{
		return 'dictionary';
	}

	function search( $data )
	{
		$this->check_privilege( '0,55' );
		
		$field = '*';
		
		$table = $this->table();
		$equal = array( 'section','`key`' );
		$like = array();
		$q = array( 'section','`key`','value','remark' );
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = '';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		$result['data'] = $this->format_datetime( $result['data'], 'time' );
				
		return $result;
	}
	
}

$action = new action();
$action->run();
?>