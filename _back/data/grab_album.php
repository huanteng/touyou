<?php

require 'backend.php';

class action extends backend
{

	function search($data)
	{
		$this->check_privilege('0,29');

		$field = '*';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array('name');
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'asc';
		}
		
		if( $this->value( $data, 'rand' ) == 'rand' )
		{
			$data['sortField'] = 'rand()';
		}
		
		if( $this->value( $data, 'pageSize' ) == '' )
		{
			$data['pageSize'] = '10';
		}
		
		$result = parent::find( $data, $field, 'grab_user', $equal, $like, $q );
		
		foreach ($result['data'] as $key => $value)
		{
			$result['data'][$key]['id'] = $value['id'];
			$result['data'][$key]['name'] = $value['name'];
			$result['data'][$key]['uid'] = $value['uid'];
			
			$album = $this->db->get('*','grab_album',array('grab_user'=>$value['id']));
			foreach($album as $index =>$pic)
			{
				$result['data'][$key]['pic'.$index] = $pic['path'];
				$result['data'][$key]['pic_id'.$index] = $pic['id'];
				if($index>=20) break;
			}
		}
		
		return $result;
	}
	
	// <editor-fold defaultstate="collapsed" desc="添加为NPC">
	function add_npc($in)
	{
		$this->check_privilege('0,29');

		$sex = $this->number($in, 'sex', 0);
		
		$grab_user = $this->number($in, 'grab_user', 0);
		if ($grab_user != 0) {
			$temp = $this->db->get('name', 'grab_user', array('id' => $grab_user));
			$grab_name = $this->value($temp[0], 'name');

			//检查用户名是否存在
			$user = load('biz.user');
			$temp = $this->db->get('id', 'user', array('name' => $grab_name));

			$cookie = load( 'cookie' );
			$account = $cookie->get( 'account', true);
			
			if (!isset($temp[0]['id'])) {
				$uid = $this->db->add('user', array('name' => $grab_name, 'npc'=>1, 'register' => time(), 'creater' => $account, 'city' => '神秘岛', 'pass' => '4f29ae61a4188f3e45c428f7a9148073', 'sex' => $sex));
				//user_ex
				$this->db->add('user_ex', array('id'=>$uid,'vip'=>0, 'vip_expires'=>0, 'integral'=>0));
				
				$this->db->set('grab_user',array('uid'=>$uid),array('id'=>$grab_user));

				//某个用户图片
				$pic = $this->db->get('*', 'grab_album', array('grab_user' => $grab_user));
				
				$grab_album = load('biz.grab_album');
				$logo_dir = config('logo_base_dir');	
				
				foreach ($pic as $key => $value) {
					
					//用第一张图做logo
					if ($key == 0) {	
						$source = '../../frontend/g/' . $value['path'];
						$target_dir = $user->logo_dir($uid);
						if (!is_dir($logo_dir . $target_dir))
							mkdir($logo_dir . $target_dir, 0755, true);
						$logo_filename = $target_dir . uniqid() . '.jpg';
						copy($source . '.s.jpg', $logo_dir . $logo_filename);
						$this->db->set('user', array('logo' => $logo_filename), array('id' => $uid));
					}
					
					//转移图片
					$grab_album -> move_img( array('uid'=>$uid,'grab_album_id'=>$value['id'],'path'=>$value['path'],'time'=>time()+$key*rand(60,70)) );
					
					if($key>=2)
					{
						break;//只发三张
					}
				}

				$out = array('code' => 1);
			} else {
				$out = array('message' => '系统已经有这个名字，无法添加');
			}
		}
		return $out;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="转移单张图片">
	function move_img($in)
	{
		$this->check_privilege('0,29');
		
		$grab_album = load('biz.grab_album');
		$grab_album->move_img($in);
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="删除图片">
	function remove_album($in)
	{
		$this->check_privilege('0,29');
		
		$grab_user = $this->number($in, 'grab_user');
		$pic = $this->db->get('*', 'grab_album', array('grab_user' => $grab_user));
		foreach ($pic as $key => $value) {
			$source = '../../frontend/g/' . $value['path'];
			unlink($source);
			unlink($source . '.s.jpg');
		}
		
		//删除抓取记录
		$this->db->del('grab_user', array('id' => $grab_user));
		$this->db->del('grab_album', array('grab_user' => $grab_user));
	}
	// </editor-fold>
	
}

$action = new action();
$action->run();
?>