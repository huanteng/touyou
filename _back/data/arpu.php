<?php
require 'backend.php';

class action extends backend
{
	
	function search($data)
	{
		$this->check_privilege( '0,83' );

		$result['data'] = $this->db->select('SELECT SUM(binary(money)) total,FROM_UNIXTIME(`time`,\'%Y-%m\') AS mon FROM payment WHERE `status` = 1 GROUP BY FROM_UNIXTIME(`time`,\'%Y-%m\')');
		
		foreach( $result['data'] as $key=> $value )
		{	
			$result['data'][$key]['total'] = round( $result['data'][$key]['total'], 2 );
			
			//充值人数
			$temp = $this->db->unique('SELECT COUNT(DISTINCT(USER)) as count FROM payment WHERE `status` = 1 AND FROM_UNIXTIME(`time`,\'%Y-%m\') = \''.$value['mon'].'\'');	
			$result['data'][$key]['user_count'] = $this->value($temp, 'count', 0);
			
			//充值次数
			$temp = $this->db->unique('SELECT COUNT(1) as count FROM payment WHERE `status` = 1 AND FROM_UNIXTIME(`time`,\'%Y-%m\') = \''.$value['mon'].'\'');	
			$result['data'][$key]['pay_count'] = $this->value($temp, 'count', 0);
			
			//arpu值
			$result['data'][$key]['arpu'] = round( $value['total'] / $result['data'][$key]['user_count'], 2 );
		}
		
		return $result;
	}
	
}

$action = new action();
$action->run();
?>