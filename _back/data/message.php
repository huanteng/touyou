<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'message';
	}

	function search( $data )
	{		
		$this->check_privilege( '0,22' );
		
		if( isset($data['sender']) && is_numeric($data['sender']) ) $data['sender'] = $data['sender']; 
		else unset($data['sender']);
		if( isset($data['receiver']) && is_numeric($data['receiver']) ) $data['receiver'] = $data['receiver']; 
		else unset($data['receiver']);
		
		$field = 'id, sender, receiver,content, time';
		$table = $this->table();
		$equal = array( 'sender', 'receiver' );
		$like = array();
		$q = array();

		if( $this->value( $data, 'sortField' ) == '' )	
		{
			$data['sortField'] = 'time';
			$data['sortOrder'] = 'desc';
		}

		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->fill_name_from_uid( $result['data'], 'sender', 'sender_name', '' );
		$result['data'] = $this->fill_name_from_uid( $result['data'], 'receiver', 'receiver_name', '' );
		$result['data'] = $this->format_datetime( $result['data'], 'time' );
		
		return $result;
	}
	
	function history( $data )
	{	
		$this->check_privilege( '0,22' );
		
		$data = $this->value($data, 'data', $data);
		
		$where = '';
        if( isset($data['uid']) && is_numeric($data['uid']) )
        {
            $where = "sender = ".$data['uid']." or  receiver = ".$data['uid'];       
        }
        else
        {
			
			if( !is_numeric( $data['sender'] ) && $data['sender'] != '' )
			{	
				$user = biz('user');
				$data['sender'] = $user->get_id_from_name( $data['sender'] );
			}
			
			if( !is_numeric( $data['receiver'] ) && $data['receiver'] != '' )
			{	
				$user = biz('user');
				$data['receiver'] = $user->get_id_from_name( $data['receiver'] );
			}
			
            if( $this->value($data, 'sender') != '' && $this->value($data, 'receiver') == '' )
			{
				$where = "sender = ".$data['sender'];
			}
			if( $this->value($data, 'receiver') != '' && $this->value($data, 'sender') == '' )
			{
				$where = "receiver = ".$data['receiver'];
			}
			if( $this->value($data, 'sender') != '' && $this->value($data, 'receiver') != '' )
			{
				$where = "(sender = ".$data['sender']." or sender = ".$data['receiver'].") and (receiver = ".$data['sender']." or receiver = ".$data['receiver'].")";
			}	      
        }
		
		if( $this->number($data, 'read') > 0 ) $where .= ' and read=1';
        
		$data['sortOrder'] = $this->value($data, 'sortOrder', 'desc');	
		if( $this->value( $data, 'sortField', 'time') != '' )
		{
			$orderby = $data['sortField'];
			if( isset( $data['sortOrder'] ) ) $orderby .= ' ' . $data['sortOrder'];
		}
		
		if( isset( $data['pageIndex'] ) && isset( $data['pageSize'] ) )
		{
			$limit = ( $data['pageIndex'] * $data['pageSize'] ) . ', ' . $data['pageSize'];
		}

		$sql = "select count(1) as total from message";
		if( $where != '' ) $sql .= ' where ' . $where;
		$data = $this->db->unique( $sql );
		$result['total'] = $data['total'] + 0;
		
		if( $result['total'] == 0 )
		{
			$result['data'] = array();
		}
		else
		{
			$sql = "select * from message";
			if( $where != '' ) $sql .= ' where ' . $where;
			if( isset( $orderby ) ) $sql .= ' order by ' . $orderby;
			if( isset( $limit ) ) $sql .= ' limit ' . $limit;

			$result['data'] = $this->db->select( $sql );
		}
		
		$result['data'] = $this->fill_user( $result['data'], 'sender', array('name'=>'sender_name','npc'=>'sender_npc') );
		$result['data'] = $this->fill_user( $result['data'], 'receiver', array('name'=>'receiver_name','npc'=>'receiver_npc') );
		$result['data'] = $this->format_datetime( $result['data'], 'time' );

		return $result;
	}
	
	/*
	 * 通过聊天方式发布消息到用户
	 * 三种模式,0全部用户，1全部在线用户，2指定用户
	 */
	function send($in)
	{
		$this->check_privilege( '0,22' );
		
		if( isset($in['uid']) && is_numeric($in['uid']) && isset($in['content']) && $in['content'] != '' && isset($in['type']) )
		{
			$message = load('biz.message');
			$data = array();
			$data['uid'] = $in['uid'];
			$data['content'] = $in['content'];
			
			$in['size'] = $this->number( $in, 'size', 10 );
			$last_uid = $this->number( $in, 'last_uid', 0 );
			$where = ' and id > '.$last_uid; 
			
			if( $in['type'] == 0 )
			{
				$sql = 'select id from user where npc=0 '.$where.' order by id desc limit 1';
				$temp = $this->db->unique($sql);
				$max_id = isset($temp['id']) ? $temp['id'] : 1;
				
				$sql = 'select id from user where npc=0 '.$where.' order by id limit '.$in['size'];
				$temp = $this->db->select($sql);	

				foreach ($temp as $value)
				{
					$last_uid = $this->value($value, 'id', -1);
					if($last_uid != -1)
					{
						$data['user'] = $last_uid;
						$message->send( $data );
					}
				}
				
				if( $last_uid < $max_id )
				{
					$out = array('code'=>'0','memo'=>'发送成功,继续发送'+$sql,'last_uid'=>$last_uid);
				}
				else
				{
					$out = array('code'=>'1','memo'=>'发送成功','last_uid'=>$last_uid);
				}
				return $out;
			}
			else if( $in['type'] == 1 )
			{
				//所有在线
			}
			else if( $in['type'] == 2 && is_numeric($in['target_uid']) )
			{
				$data['user'] = $in['target_uid'];
				$message->send( $data );
				return array('code'=>'1','memo'=>'发送成功');
			}		
		}	
	}
	
	/*
	 * 列出和npc（及骰妖子）的未处理聊天
	 */
	function npc_chat($in)
	{
		$data = $in['data'];
		$max_id = $this->number($data, 'max_id');
		return biz('message')->npc_chat( array('id'=>$max_id) );
	}

	/*
	 *  回复
	 */
	function reply($in)
	{
		$in = json_decode( $in['data'], true );
		
		$result = array( 'code' => -1 );
		
		$task_queue = biz('task_queue');
		$account = load('cookie')->get('account', true);

		if ( isset( $in['id'] ) && isset( $in['user'] ) && isset( $in['from'] ) && isset( $in['content'] ) && isset( $in['time'] ) )
		{
			$temp = $task_queue->get1( 'time', array('status'=>1, 'creater'=> $account, 'target'=> $in['user']), 'id desc' );
			if( isset($temp['time']) )
			{
				$in['time'] = $temp['time'] + $in['time'];
			}
			else
			{
				$in['time'] = time() + $in['time'];
			}

			$id = $task_queue->create( array( 'user' => $in['user'],  'creater'=>$account, 'type' => 4,
				'time' => $in['time'], 'target' => $in['user'], 'allow_repeat' => TRUE, 'data' => array( 'uid' => $in['from'],
					'user' => $in['user'], 'content' => addslashes( $in['content'] )  ) ) );

			if ( $id > 0 )
			{
				$this->db->command( "update message set `read` = 1 where receiver = " . $in['from'] . ' and sender = ' . $in['user'] );

				// 删除未读信息，注意：本处可能过量删除
				$this->db->del( 'pool', array( 'uid' => $in['user'], 'target_uid' => $in['from'] ) );

				$result['code'] = 1;
			}
		}

		return $result;
	}
	
	// <editor-fold defaultstate="collapsed" desc="challenge_ok，挑战信息，不作作何响应，仅删除">
	function challenge_ok($in)
	{
		$in = $in['data'];
		$user = load( 'biz.user' );
		$user->db->command( 'delete from message where id = ' . $in['id']);
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="challenge_accept，接受挑战">
	function challenge_accept($in)
	{
		$in = $in['data'];
		$result = array( 'code' => -1 );

		$user = load( 'biz.user' );
		
		$challenge = load( 'biz.challenge' );
		$result['code'] = $challenge->accept( array( 'uid' => $in['npc'], 'user' => $in['sender'], 'gold' => $in['gold'] ) );

		// <editor-fold defaultstate="collapsed" desc="必赢">
		if( $in['must_win'] == 1 )
		{
			$vs = $user->db->get1( '*', 'vs', array( '0' => "( user_1 = $in[npc] and user_2 = $in[sender] ) or ( user_2 = $in[npc] and user_1 = $in[sender] )" ), 'id desc' );
			if ( isset( $vs['id'] ) )
			{
				$user->db->set( 'vs', array( 'must_win' => ($vs['user_1'] == $in['npc']) ? 1 : 2 ), array( 'id' => $vs[ 'id' ] ) );
			}
		}
		// </editor-fold>

		$user->db->command( 'delete from message where id = ' . $in['id']);

		switch($result['code'])
		{
			case -1: $result['memo'] = "缺少sid"; break;
			case -2: $result['memo'] = "sid不正确"; break;
			case -3: $result['memo'] = "挑战不存在"; break;
			case -4: $result['memo'] = "投注额大于余额"; break;
			case 1: $result['memo'] = "成功应战";break;
			default: $result['memo'] = "错误，code=" .$result['code'];
		}

		return $result;
	}
	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="challenge_reject，拒绝挑战">
	function challenge_reject($in)
	{
		$in = $in['data'];
		$result = array( 'code' => -1 );
		
		$challenge = load( 'biz.challenge' );
		$result = $challenge->reject( array( 'uid' => $in['npc'], 'user' => $in['sender'] ) );

		$challenge->db->command( 'delete from message where id = ' . $in['id']);

		return $result;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="challenge_next，继续挑战">
	function challenge_next( $in )
	{
		$in = $in['data'];
		$result = array( 'code' => -1 );

		$challenge = load( 'biz.challenge' );
		$result = $challenge->next( array( 'uid' => $in['npc'] ) );

		$challenge->db->command( 'delete from message where id = ' . $in['id']);

		switch($result['code'])
		{
			case -1: $result['memo'] = "sid错误"; break;
			case -2: $result['memo'] = "你没有相关挑战赛的数据"; break;
			case -3: $result['memo'] = "金币不足"; break;
			case -4: $result['memo'] = "提交失败"; break;
			case 1: $result['memo'] = "成功应战";break;
			default: $result['memo'] = "错误，code=" .$result['code'];
		}

		$challenge->debug($result);

		return $result;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="challenge_must_win，设置必赢">
	function challenge_must_win($in)
	{
		$in = $in['data'];
		$user = load( 'biz.user' );
		$vs = $user->db->get1( '*', 'vs', array( '0' => "( user_1 = $in[user] and user_2 = $in[npc] ) or ( user_1 = $in[npc] and user_2 = $in[user] )" ), 'id desc' );
		if ( isset( $vs['id'] ) )
		{
			$user->db->set( 'vs', array( 'must_win' => ($vs['user_1'] == $in['npc']) ? 1 : 2 ), array( 'id' => $vs[ 'id' ] ) );
		}

		$user->db->command( 'delete from message where id = ' . $in['id']);

		return array( 'code' => 1, 'memo' => '设置成功' );
	}
	// </editor-fold>

	/*
	 * 解除设置
	 */
	function challenge_unblock( $in )
	{
		$in = $this->value($in, 'data', $in);
		biz('user_value')->del( array( 'uid' => $in[ 'npc' ], 'key' => '挑战', 'target_uid' => $in[ 'user' ] ) );
		if( $in['id'] != 0 ){
			biz('message')->del( array('id'=>$in['id']) );
		}		
		return array( 'code' => 1, 'memo' => '成功解除' );
	}
	
	/*
	 * 忽略
	 * 用作聊天列表页面
	 */
	function ignore($in)
	{
		$data = $this->value($in, 'data', $in);
		
		$sender = $this->number($data, 'sender');
		$receiver = $this->number($data, 'receiver');
		if( $sender > 0 && $receiver > 0 )
		{
			$this->db->set( 'message', array('read'=>1), array('receiver'=>$receiver, 'sender'=>$sender) );
			$this->db->del( 'pool', array( 'uid' => $sender, 'target_uid' => $receiver ) );
			$this->challenge_unblock( array( 'id' => 0, 'user'=>$sender, 'npc' => $receiver ) );
		}	
	}
	
	// <editor-fold defaultstate="collapsed" desc="挑战设置">
	function challenge_block($in)
	{
		$in = $this->value($in, 'data');
		
		$now = time();

		$user_value = load( 'biz.user_value' );

		$where = array( 'uid' => $in[ 'npc' ], 'target_uid' => $in[ 'user' ], 'key' => '挑战', '0' => 'expires>' . $now );

		$data = $user_value->get1( '*', $where );
		
		if( isset( $data['id'] ) )
		{
			$where = array( 'id' => $data[ 'id' ] );
			if( $data[ 'remark' ] == $in[ 'remark' ] )
			{
				$count = $data[ 'value' ] + 5;
				$where[ 'value' ] = $count;
			}
			else
			{
				$count = 5;
				$where[ 'value' ] = $count;
				$where[ 'remark' ] = $in[ 'remark' ];
			}

			$where[ 'expires' ] = $data[ 'expires' ] + 3600;
			$user_value->set( $where );
		}
		else
		{
			$count = 5;
			$where[ 'value' ] = $count;
			$where[ 'expires' ] = time() + 3600;
			$where[ 'remark' ] = $in[ 'remark' ];
			$user_value->add( $where );
		}

		$time = load( 'time' );
		$expires = $time->format( $where['expires'] );
		$result = array( 'code' => 1, 'memo' => "设置成功，以后{$count}次挑战将被{$in[ 'remark' ]}，直至{$expires}。你可在过程中解除。");

		return $result;
	}
	// </editor-fold>


	// 发起挑战
	// 参数：npc, uid, gold
	function challenge_request( $in )
	{
		$in = $this->value($in, 'data', $in);
		
		$result = array( 'code' => 0, 'message' => '失败' );

		$gold = $this->number($in, 'gold', 100);
		if( $in['npc'] == 0 )
		{
			$user = load('biz.user');
			$user_info = $user->get_from_id($in['uid']);
			$sex = $user_info['sex']==1 ? 0 : 1;
			$temp = $user->get1('id', array('sex'=>$sex), 'rand()');
			$in['npc'] = $temp['id'];
				
			if ( biz('props')->gold( $in['npc'] ) < $gold )
			{
				return $this->challenge_request( array('npc'=>0, 'uid'=>$in['uid']) );
			}		
		}
		
		$challenge = load( 'biz.challenge' );
		$result = $challenge->send( array( 'uid' => $in['npc'], 'user' => $in['uid'], 'bet' => $gold ) );

		$result['message'] = $result['code'] < 0 ? '错误：' . $result['memo'] : $result['memo'];

		return $result;
	}
	
	/*
	 * 禁止用户
	 */
	function forbid_user($in)
	{
		$in = $this->value($in, 'data', $in);
		$result = array( 'code' => 0, 'message' => '失败' );
		$forbid_user = load( 'biz.forbid_user' );
		$in['reason'] = '封停-禁言';
		$forbid_user->add( $in );
		$result['message'] = '成功';

		return $result;
	}
	
	/*
	 * 聊天快速回复
	 */
	function quick_reply()
	{
		$content = biz('dictionary')->get_value('聊天', '快速回复');
		return explode("\n", $content);
	}
	
	/*
	 * 是否半小时没有理的人
	 */
	function invisible( $in )
	{
		$in = $this->value($in, 'data', $in);
		
		$message = biz('message');
		$data = $message -> get('*', array('`time`<='.(time()-1800).' and read=0'));
				
		$out['data'] = array();
		foreach ($data as $k =>$v)
		{
			$result = $message->get1('count(*) as c', array('(sender = '.$v['sender'].' and receiver = '.$v['receiver'].') or (sender = '.$v['receiver'].' and receiver = '.$v['sender'].')'));
			if( $result['c'] == 1 )
			{
				$out['data'][] = $v['id'];
			}
		}
		return $out;
	}
	
	/*
	 * 两个用户之间的关系
	 * 用在后台聊天窗，两人聊天时的顶部
	 * 例：
	 * 送礼：2天前 东宫 送给 西宫 一朵玫瑰花  刚刚东宫送给西宫 一袋元宝
	 * 赛事：东宫 和 西宫  比赛38场 25胜13负  盈利5000金币
	 */
	function moving( $in )
	{
		$out = array();
		$in = $this->value($in, 'data', $in);
		$uid1 = $this->number($in, 'uid1');
		$uid2 = $this->number($in, 'uid2');
		
		if( $uid1 > 0 && $uid2 > 0 )
		{
			$user = load('biz.user');
			$name1 = $user -> get_name_from_id($uid1);
			$name2 = $user -> get_name_from_id($uid2);
			
			//送礼
			$log = biz('user_props_log')->get('*', array('user = '.$uid2.' and type = 3 and memo = \''.$name1.'赠送\''));

			if( isset($log[0]['id']) )
			{
				$props = biz('props');
				foreach($log as $k => $v)
				{
					$out[] = date('m-d H:i',$v['time']) .' '. $name1 . ' 送给 ' .$name2.' '.$v['change'] .' "'.$props->get_name_from_id($v['props']) . '"';
				}
			}
			
			//赛事
			$win_vs = $this->db->get1('count(*) as c', 'vs_history', array('type=0 and winner = '.$uid1.' and (user_1 = '.$uid1.' and user_2 = '.$uid2.') or (user_1 = '.$uid2.' and user_2 = '.$uid1.')'));		
			$lose_vs = $this->db->get1('count(*) as c', 'vs_history', array('type=0 and winner = '.$uid2.' and (user_1 = '.$uid1.' and user_2 = '.$uid2.') or (user_1 = '.$uid2.' and user_2 = '.$uid1.')'));
			$win_bet = $this->db->get1('sum(bet) as s', 'vs_history', array('type=0 and winner = '.$uid1.' and (user_1 = '.$uid1.' and user_2 = '.$uid2.') or (user_1 = '.$uid2.' and user_2 = '.$uid1.')'));
			$lose_bet = $this->db->get1('sum(bet) as s', 'vs_history', array('type=0 and winner = '.$uid2.' and (user_1 = '.$uid1.' and user_2 = '.$uid2.') or (user_1 = '.$uid2.' and user_2 = '.$uid1.')'));
			
			if( $win_vs['c']+$lose_vs['c'] > 0 ) $out[] = $name1.' 和 '.$name2.' 比赛'.($win_vs['c']+$lose_vs['c']).'场 '.$win_vs['c'].'胜'.$lose_vs['c'].'负  盈利'.($win_bet['s']-$lose_bet['s']).'金币';
		}
		
		return $out;
	}
}

$action = new action();
$action->run();
?>