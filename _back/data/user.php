<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'user';
	}

	/*
	 * 用户资料
	 */
	function detail($in)
	{
		$this->check_privilege('0,18');
		$user = load('biz.user');
		$user_ex = load('biz.user_ex');
		if ($this->number($in, 'uid') != '') {

			$info = $user->get_from_id($in['uid']);
			$ex = $user_ex->get_from_id($in['uid']);
			$out = array_merge($info, $ex);
			if (isset($out['id'])) {		
				$out['logo'] = $user->logo($in['uid']);

				$out['birthday'] = date('Y-m-d', $out['birthday']);
				$out['register'] = date('m-d H:i:s', $out['register']);
				$out['login'] = date('m-d H:i:s', $out['login']);
				$out['logout'] = date('m-d H:i:s', $out['logout']);
				return $out;
			}
		}
	}
	
	/*
	 * 用户基本信息
	 */
	function info($in)
	{
		$in = $this->value($in, 'data', $in);
		$uid = $this->number($in, 'uid');
		$user = biz('user')->get1( 'id,name,birthday,city,sex', array('id'=>$uid) );
		$user_ex = biz('user_ex')->get1( 'is_real', array('id'=>$uid) );
		$user['logo'] = biz('user')->logo($uid);
		$user['birthday'] = date('Y-m-d', $user['birthday']);
		$user['sex'] = $user['sex'] == 1 ? '女' : '男';
		$user_ex['is_real'] = $user_ex['is_real'] == 1 ? '是' : '否';
		
		//VIP
		$props = biz('user_props')->get1('count(*) as c', array('user = '.$uid.' and props in (3,4,5) and quantity > 0'));
		$user['vip'] = $props['c'] > 0 ? '有' : '无';
		
		return array_merge($user, $user_ex);
	}

	function row($in)
	{
		unset($in['register'], $in['login'], $in['login_times'], $in['register_imei'], $in['login_imei']);

		if ($this->value($in, 'rename') != '') {
			$in['name'] = $in['rename'];
		}

		if ($this->value($in, 'birthday') != '') {
			$in['birthday'] = strtotime($in['birthday']);
		}
		
		return $in;
	}

	function save($data)
	{
		$this->check_privilege('0,18');

		$rows = $this->php_json_decode($data['data']);

		$file = 'biz.' . $this->table();
		$obj = load($file);

		$field = array_flip($obj->field());
		$id = $field['id'];

		foreach ($rows as $row) {
			$state = $this->value($row, '_state', '');
			if ($state == "added" || $id == "") { //新增：id为空，或_state为added
				
				if ( isset( $row['pass'] ) && $row['pass'] != '' )
				{
					$row['pass'] = $obj->code( $row['pass'] );
				}
				
				if( $obj->get_id_from_name($row['name']) > 0 )
					return array('code' => -1, 'message' => '用户名已经存在');
				else
				{
					$uid = $obj->add($this->row($row));
					$this->db->add( 'user_ex', array( 'id' => $uid ) );
				}
			} else if ($state == "removed" || $state == "deleted") {
				$obj->del($row[$id]);
				biz('user_ex')->del($row[$id]);
			} else if ($state == "modified" || $state == "") {  //更新：_state为空或modified
				
				if( $row['rename'] != '' && $obj->get_id_from_name($row['rename']) > 0 )
					return array('code' => -1, 'message' => '用户名已经存在');
				
				if ( isset( $row['newpass'] ) && $row['newpass'] != '' ){
					$user = load('biz.user');
					$row['pass'] = $user->code( $row['newpass'] );
				}

				$ex = array( 'id'=>$row['id'] );
				foreach( array( 'is_real', 'drunk' ) as $v )
				{
					$ex[ $v ] = $row[ $v ];
					unset( $row[ $v ] );
				}

				biz('user_ex')->set( $ex );
				
				$obj->set($this->row($row));
			}
		}
		return array('code' => 1, 'message' => '成功操作');
	}

	function search($data)
	{
	$this->check_privilege('0,18');

	$user = biz('user');
	$other = '';
	//搜索框时用到
	$key = addslashes( $this->value($data, 'key') );
	if ( $key != '') {
		$data['pageIndex'] = 0;
		$data['pageSize'] = 20;
		$other = " name like '%" . $key  . "%'";
	}

	if (isset($data['user']) && !is_numeric($data['user']) && $data['user'] != '') {
		$data['id'] = $user->get_id_from_name($data['user']);
	} else {
		$data['id'] = $this->value($data, 'user', 0);
	}

	$field = '*';
	$table = $this->table();
	$equal = array('id', 'npc', 'creater', 'sex');
	$like = array();
	$q = array('name', 'email', 'city', 'channel', 'version');

	if ($this->value($data, 'sortField') == '') {
		$data['sortField'] = 'id';
		$data['sortOrder'] = 'desc';
	}

	if( isset( $data[ 'q' ] ) )
		$data[ 'q' ] = addslashes( $data[ 'q' ] );

	$result = parent::find($data, $field, $table, $equal, $like, $q, 'and', $other);
	$result['data'] = $this->format_datetime($result['data'], 'register');
	$result['data'] = $this->format_datetime($result['data'], 'login');
	$admin = load('biz.administrator');
	$result['data'] = $this->fill_field($result['data'], 'creater', 'creater_name', $admin->dict());

	if ($this->value($data, 'key') != '')
	{
		return $result['data'];
	}
	else
	{
		foreach( $result['data'] as $k => $v )
		{
			$v['logo'] = $user->logo($v['id']);
			$v['uid'] = $v['id'];
			$result['data'][$k] = $v;
		}

		return $result;
	}
}

	/* 自动完成
	 * 参数：
	 * 	key
	 */
	function autocomplete( $in )
	{
		extract( $in );

		$user = biz('user');
		$key = addslashes( $key );

		$field = 'id,name,login,npc';
		$data = $user->get( $field, array( " name like '%{$key}%'" ), '', 10 );

		$out = array();
		foreach( $data as $k => $v )
		{
			$v[ 'npc' ] = ( $v[ 'npc' ] == 1 ) ? '√' : '';
			$out[] = $v;
		}

		// 相等的在前
		$info = $user->get1( $field, array( 'name' => $key ) );
		if( isset( $info[ 'id' ] ) )
		{
			$out2 = $out;

			$info[ 'npc' ] = ( $info[ 'npc' ] == 1 ) ? '√' : '';
			$out = array( $info );

			foreach( $out2 as $v )
			{
				if( $v[ 'id' ] != $info[ 'id' ] )
				{
					$out[] = $v;
				}
			}
		}

		$out = $this->format_datetime( $out, 'login' );

		return $out;
	}

	function sel_user($data)
	{
		$this->check_privilege('0,18');

		$field = 'id, name, npc, 1 as goal, login';
		$table = $this->table();
		$equal = array('npc');
		$like = array();
		$q = array('name');
		if (!isset($data['sortField'])) {
			$data['sortField'] = 'name';
			$data['sortOrder'] = '';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		$result['data'] = $this->format_datetime($result['data'], 'login');
		$result['data'] = $this->fill_props( $result['data'], 'id', array( 1 => 'gold'), 0 );

		return $result;
	}

	function sid( $in )
	{
		$this->check_privilege('0,18');
		
		$data = load( 'cache' )->get( 'sid', array() );
		$uid = $in['data'];
		
		if ( $uid == 0 ) {
			if( count( $data ) )
			{
				$keys = array_keys( $data );
				shuffle( $keys );
				return $keys[0];
			}
		} else {
			$sid = biz('user')->get_sid_from_uid( $uid );
			if( $sid == '' ) $sid = '无在线';
		}
		
		return '无在线';
	}

	/*
	马甲（傀儡）列表
	参数：uid
	*/
	function puppet($in)
	{
		$this->check_privilege('0,18');
		
		$user = load('biz.user');
		
		$uid = $this->number($in, 'uid');
		if($uid>0)
		{
			$info = $user->get_from_id($uid);

			$imei1 = $info['register_imei'];
			$imei2 = $info['login_imei'];
			
			$result =  $user->get('id, name, register, login, logout, platform,register_imei,login_imei', array('( register_imei="'.$imei1.'" and register_imei !="" and register_imei !="0" ) or ( login_imei="'.$imei2.'" and login_imei !="" and login_imei !="0" )'));

			$result = $this->format_datetime($result, 'register');
			$result = $this->format_datetime($result, 'login');
			$result = $this->format_datetime($result, 'logout');
			
			$user_ex = load('biz.user_ex');
			foreach( $result as $k => $v )
			{
				$result[$k]['is_real'] = $user_ex -> get_field_from_id($v['id'], 'is_real');
			}
			
			$out['data'] = $result;
			$out['total'] = count($result);
			return $out;
		}
	}
}

$action = new action();
$action->run();
?>