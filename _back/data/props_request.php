<?php
require 'backend.php';

class action extends backend
{
	function table() 
	{
		return 'props_request';
	}

	function search( $data )
	{	
		$this->check_privilege( '0,63' );
		
		$field = '*';
		$table = $this->table();
		$equal = array('uid','target_uid','type');
		$like = array();
		$q = array();
		
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'time';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->fill_user( $result['data'], 'uid', array('name'=>'name', 'npc'=>'npc') );
		$result['data'] = $this->fill_user( $result['data'], 'target_uid', array('name'=>'target_name', 'npc'=>'target_npc') );
		
		$result['data'] = $this->format_datetime( $result['data'], 'time' );

		return $result;
	}

}

$action = new action();
$action->run();
?>