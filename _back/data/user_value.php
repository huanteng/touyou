<?php
require 'backend.php';

class action extends backend
{
	function table() 
    {
		return 'user_value';
	}
    
    function search( $data )
	{
		$this->check_privilege( '0,64' );
		
		$field = '*';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array();
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = '';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
        
        $result['data'] = $this->fill_name_from_uid($result['data'], 'uid', 'name', '');
        $result['data'] = $this->format_datetime( $result['data'], 'time' );
        $result['data'] = $this->format_datetime( $result['data'], 'expires' );
				
		return $result;
	}	

	function row( $data )
	{
		$this->check_privilege( '0,64' );
		
		if( $this->value( $data, 'expires', '' ) != '' )
		{
			$data['expires'] = strlen( $data['expires'] ) > 11 ? strtotime( $data['expires'] ) : strtotime( date('Y-') . $data['expires'] );
		}else{
			$data['expires'] = time() + 30*86400;
		}
		return $data;
	}
    
}

$action = new action();
$action->run();
?>