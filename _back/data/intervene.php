<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'intervene';
	}
	function search( $data )
	{
		$this->check_privilege( '0,5' );

		if( isset($data['q']) && !isset( $data['uid'] ) )
		{
			$user = load( 'biz.user' );
			$data['uid'] = $user->get_id_from_name( $data['q'] );
			unset( $data['q'] );
		}
		
		$field = '*';
		$table = $this->table();
		$equal = array( 'uid' );
		$like = array();
		$q = array();
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'target';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->fill_name_from_uid( $result['data'], 'uid', 'name', '用户不存在' );
		$result['data'] = $this->format_datetime( $result['data'], 'time' );
		$result['data'] = $this->fill_props( $result['data'], 'uid', array(1=>'gold_count') );

		$intervene = load('biz.intervene');
		$result['data'] = $this->fill_field( $result['data'], 'type', 'type_name', $intervene->get_type_dict() );

		return $result;
	}
	
	function type()
    {
		$this->check_privilege( '0,5' );

        $intervene = load('biz.intervene');
        foreach( $intervene->get_type_dict() as $key => $value )
        {
            $out[] = array('id'=>$key,'text'=>$value);
        }
        return $out;
    }

}

$action = new action();
$action->run();
?>