<?php
require 'backend.php';

class action extends backend
{
	function table() 
	{
		return 'badword';
	}

	function search( $data )
	{
		$this->check_privilege( '0,33' );
		
		$field = 'id, old, new';
		$table = $this->table();
		$equal = array( 'old' );
		$like = array();
		$q = array( 'old' );
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
				
		return $result;
	}	
}

$action = new action();
$action->run();
?>