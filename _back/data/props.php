<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'props';
	}

	function search($data)
	{
		$this->check_privilege('0,19');

		$field = '*';
		$table = $this->table();
		$equal = array('description', 'short_desc', 'name');
		$like = array('description', 'short_desc', 'name');
		$q = array('description', 'short_desc', 'name');

		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'seq';
			$data['sortOrder'] = '';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		$props = load('biz.props');
		$kind_dict = $props->get_kind_dict();
		foreach ( $result['data'] as $k => $v ) {
			$v[ 'kind_name' ] = $kind_dict[ $v['kind'] ];
			$v[ 'logo' ] = $props->logo( $v['id'] );

			$have = $props->db->get1( 'sum(quantity) as total', 'user_props', array( 'props' => $v['id'] ) );
			$v[ 'have' ] = $props->value( $have, 'total', 0 );

			$result[ 'data' ][ $k ] = $v;
		}

		return $result;
	}

	function sel_props($data)
	{
		$this->check_privilege('0,19');

		$field = 'id, name, price, short_desc';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array('name');
		if (!isset($data['sortField'])) {
			$data['sortField'] = 'name';
			$data['sortOrder'] = '';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		return $result;
	}

	function drop($data)
	{
		$this->check_privilege('0,19');

		$other = '';
		if( $this->value($data, 'havegold') == '' )
		{
			$other = 'id > 1';
		}
		
		$field = 'id, name, price, short_desc';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array();
		
		if ( $this->value($data, 'sortField') == '' ) 
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = '';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q, 'and', $other);

		return $result['data'];
	}

	function row($data)
	{
		$this->check_privilege('0,19');
		
		unset($data['logo']);
		return $data;
	}
	
	/*
	 * 赠送道具
	 * 参数：uid, target_uid, props_id, count
	 * 返回值：code, memo
	 */
	function give($in)
	{
		$this->check_privilege('0,67');
		
		if( is_numeric($in['uid']) && is_numeric($in['target_uid']) && is_numeric($in['count']) )
		{	
			if( isset($in['egg']) && $in['egg'] != '' )
			{
				biz('pool')->notice( array('content'=>$in['egg'], 'uid'=>$in['target_uid']) );
				biz('moving')->create( array('uid'=>$in['target_uid'], 'data'=>$in['egg']) );
				biz('config')->push_broadcast(  biz('user')->get_name_from_id($in['target_uid']).'踢到了彩蛋，获得'.biz('props')->get_name_from_id($in[ 'props_id' ]).'奖励', 1100, 'uid=' . $in['target_uid']);
			}	
			
			//用户线下充值时，产生一条充值记录。类型：手工充值
			if( isset($in['manual']) && $in['manual'] == 'true' )
			{
				$payment = biz( 'payment' );
				$no = $payment->get_no( array( 'uid' => $in['target_uid'], 'target_uid' => $in['target_uid'], 'props' => $in[ 'props_id' ],
					'count' => $in[ 'count' ], 'types' => 1 ) );
				$money = $this->value($in, 'money', 0);
				$memo = $this->value($in, 'memo', '');
				
				$payment->db->set( 'payment', array( 'memo' => $memo ), array( 'no' => $no ) );

				$payment->finish( $no, $money );

				return $payment->out( '成功' );
			}
			else
			{
				return biz('props')->give($in);
			}			
		}
	}

	function kind( $in )
	{
		$data = biz('props')->get_kind_dict($in);

		$out = array();
		foreach( $data as $k => $v )
		{
			$out[] = array( 'id' => $k, 'text' => $v );
		}

		return $out;
	}

}

$action = new action();
$action->run();
?>