<?php

require 'backend.php';

class action extends backend
{

    function table()
    {
        return 'moving';
    }

    function search($data)
    {

		$this->check_privilege( '0,32' );

		$user = biz( 'user' );

		if( isset($data['user']) && !is_numeric( $data['user'] ) && $data['user'] != '' )
		{
			$data['user'] = $user->get_id_from_name( $data['user'] );
		}
		
		$other = '';
		if( isset($data['time']) && $data['time'] != '' )
        {
            $start_time = strtotime($data['time']);
            $end_time = $start_time + 86399;
            $other = ' time between '.$start_time.' and '.$end_time.' ';
        }
        
		$field = '*';
		$table = $this->table();
		$equal = array( 'user','type' );
		$like = array( 'data' );
		$q = array( 'data' );
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}
		elseif( $data['sortField'] == 'name' )
		{
			$data['sortField'] = 'user';
		}

		$result = parent::find( $data, $field, $table, $equal, $like, $q, 'and', $other );

        $moving = biz('moving');
        $type_dict = $moving->get_type_dict();
        		
        foreach ($result['data'] as $k => $v)
		{
			if( $user->is_npc( $v[ 'user' ] ) )
			{
				$v[ 'npc' ] = 1;
			}

            switch( $v['type'] )
			{
				case 1:
					/*
					$v['data'] = unserialize($v['data']);
					$temp = $this->db->unique('select path from album where id = ' . $v['data']['id']);

					if (isset($temp['path'])) {
						$temp['path'] .= '.s.jpg';
						$phy_file = config('logo_base_dir') . $temp['path'];

						if (is_file($phy_file))
							$content = '<img src="' . config('frontend') . 'logo/' . $temp['path'] . '">';
					}
					*/
					$content = '未知类型1';
					break;
				case 2:
					$v['data'] = unserialize($v['data']);
					$content = $v['data']['content'];
					break;

				case 4:
				case 5:
					/*$v['data'] = unserialize($v['data']);

					$temp1 = $this->db->unique('select name from user where id = ' . $v['user']);
					$temp2 = $this->db->unique('select name from user where id = ' . $v['data']['user']);

					if (isset($temp1['name']) && isset($temp2['name'])) {
						$content = $temp1['name'] . '和' . $temp2['name'] . '比赛: ' . (($v['type'] == 4) ? '赢' : '输' . '了') . $v['data']['bet'] . '金币';
					}
					*/
					$content = '未知类型4或5';
					break;

				case 6:
					/*$v['data'] = unserialize($v['data']);
					$content = $v['data']['content'] . '，' . $v['data']['name'] . '获得第' . $v['data']['rank'] . '名，积分' . $v['data']['score'] . '，赢' . $v['data']['win'] . '输' . $v['data']['lost'] . '，奖励' . $v['data']['gold'] . '金币';
					*/
					$content = '未知类型6';
					break;

				case 7:
					$v['data'] = unserialize($v['data']);
					$content = $v['data']['score'] . '分';
					break;

				case 8:
					$v['data'] = unserialize($v['data']);
					$content = '升级为' . $v['data']['props'];
					break;

				default:
					$content = $v['data'];
            }

			$content = strip_tags( $content );
			$v['content'] = $content;
				
            $v['type_name'] = $type_dict[$v['type']];

			$result['data'][$k] = $v;
        }

		$result['data'] = $this->fill_name_from_uid($result['data'], 'user', 'name', '');
		$result['data'] = $this->format_datetime($result['data'], 'time');

        return $result;
    }
    
    function type()
    {
        $moving = load('biz.moving');
        foreach ($moving->get_type_dict() as $key => $value) {
            if($key<3 || $key>6) $result[] = array('id'=>$key,'text'=>$value);
        }
        return $result;
    }

}

$action = new action();
$action->run();
?>