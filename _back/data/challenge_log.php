<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'challenge_log';
	}
	function search( $data )
	{		
		$this->check_privilege( '0,52' );
		
		$field = '*';
		$table = $this->table();
		$equal = array( 'uid1', 'uid2', 'type' );
		$like = array();
		$q = array();
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->fill_name_from_uid( $result['data'], 'uid1', 'name1', '用户不存在' );
		$result['data'] = $this->fill_name_from_uid( $result['data'], 'uid2', 'name2', '用户不存在' );
		$result['data'] = $this->format_datetime( $result['data'], 'time' );
		
		return $result;
	}
	
	function truncate( $data )
	{
		$this->check_privilege( '0,52' );
		
		$sql = 'truncate table ' . $this->table();
		$this->db->command( $sql );
	}
	
	
}

$action = new action();
$action->run();
?>