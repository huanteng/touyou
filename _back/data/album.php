<?php
require 'backend.php';

class action extends backend
{
	function table() 
	{
		return 'album';
	}

	function search( $data )
	{
		$this->check_privilege( '0,29' );
		
		$data['user'] = $this->number($data, 'uid', 0);
		
		$field = '*';
		$table = $this->table();
		$equal = array('user');
		$like = array();
		$q = array();
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->fill_name_from_uid($result['data'], 'user', 'name', '');
		
		foreach( $result['data'] as $key => $value )
		{
			$result['data'][$key]['time'] = date('m-d H:i',$value['time']);
			$result['data'][$key]['path'] = '../frontend/logo/' .$value['path'];
		}
				
		return $result;
	}	
	
	// <editor-fold defaultstate="collapsed" desc="设置头像">
	function set_logo($in)
	{
		$album = load('biz.album');
		return $album -> set_logo($in);
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="改变相册">
	/*
	 * 将照片归类
	 * 参数: type,id 或者 type，ids; 例：ids: 1,2,3
	 * type: 0 其它相册，1个人相册， 2视频认证照片
	 */
	function set_type($in)
	{
		$album = load('biz.album');
		if(isset($in['ids']))
		{
			$id_arr = explode($in['ids'], ',');
			foreach($id_arr as $id)
			{
				$album->set(array('type'=>$in['type'],'id'=>$id));
			}
		}
		else 
		{
			return $album->set(array('type'=>$in['type'],'id'=>$in['id']));
		}	
	}
	// </editor-fold>

}

$action = new action();
$action->run();
?>