<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'lucky_assign';
	}

	function row($in)
	{
		unset($in['gift'], $in['name']);
		return $in;
	}

	function search($data)
	{
		$this->check_privilege('0,76');
		
		$user = load('biz.user');
		if (isset($data['user']) && !is_numeric($data['user'])) {
			$data['uid'] = $user->get_id_from_name($data['user']);
		} else {
			$data['uid'] = $this->value($data, 'user', 0);
		}

		$field = '*';
		$table = 'lucky_assign';
		$equal = array('uid');
		$like = array();
		$q = array();
		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'rank';
			$data['sortOrder'] = 'desc';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);
		$result['data'] = $this->fill_name_from_uid($result['data'], 'uid', 'name', '');

		$gift = load('biz.lucky_gift');
		$result['data'] = $this->fill_field($result['data'], 'gift_id', 'gift', $gift->dict());

		return $result;
	}
	
	function uses($in)
	{
		$lucky = load('biz.lucky');
		$lucky->execute( array( 'uid' => $in['uid'], 'npc' => 1 ) );
	}

}

$action = new action();
$action->run();
?>