<?php

require 'backend.php';

class action extends backend
{
	function table()
	{
		return 'room_match';
	}

	function search($data)
	{
		$this->check_privilege('0,112');

		$field = '*';
		$table = $this->table();
		$equal = array( 'id', 'uid1', 'uid2', 'uid3', 'uid3' );
		$like = array();
		$q = array('id', 'room_id', 'uid1', 'uid2', 'uid3', 'uid3');

		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}

		$uid  = biz('base')->value( $data, 'uid', 0 );
		$other = '';
		if( $uid != 0 )
		{
			$other = "uid1=$uid or uid2=$uid or uid3=$uid or uid4=$uid";
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q, 'and', $other );

		$four = array(1,2,3,4);
		$user = biz( 'user' );
		$room = biz( 'room' );

		foreach( $result['data'] as $k => $v )
		{
			foreach ( $four as $i )
			{
				$uid = $v[ 'uid' . $i ];
				if( $uid != 0 )
				{
					$v[ 'name' . $i ] = $user->get_name_from_id( $uid );

					if( $user->is_npc( $uid ) )
					{
						$v[ 'npc' . $i ] = 1;
					}
				}
			}

			$v[ 'winner_name' ] = $user->get_name_from_id( $v[ 'winner_uid' ] );
			if( $user->is_npc( $v[ 'winner_uid' ] ) )
			{
				$v[ 'winner_npc' ] = 1;
			}

			$v[ 'loser_name' ] = $user->get_name_from_id( $v[ 'loser_uid' ] );
			if( $user->is_npc( $v[ 'loser_uid' ] ) )
			{
				$v[ 'loser_npc' ] = 1;
			}

			$result['data'][$k] = $v;
		}

		$result['data'] = $this->format_datetime( $result['data'], 'time', 'm-d H:i:s' );

		return $result;
	}

	

}

$action = new action();
$action->run();
?>