<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'audit';
	}

	function add( $in )
	{
		$this->check_privilege('0,0');
		$in = $this->value($in, 'data', $in);
		biz('audit')->add($in);
	}
	
	function search($data)
	{
		$this->check_privilege('0,0');

		$field = '*';
		$table = $this->table();
		$equal = array('type');
		$like = array();
		$q = array();
		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}

		$other = biz('base')->value( $data, 'other' );

		$result = parent::find( $data, $field, $table, $equal, $like, $q, 'and', $other );

		return $result;
	}

	/* 待审核的说两句列表
	 * 参数：
	 *	id：上次的id
	 * 返回值：{[audit_id:...}]}
	 * parent中均为缩写，分别表示：
	 *	i：id
	 *	u：uid
	 *	c:content
	 *  t:time
	 *  sub:子数据，暂只是一个，以后如有必要可改写支持数组 
	 */
	function comment( $in )
	{
		$this->check_privilege('0,0');

		$id = $in['data'];
		
		$user = biz('user');
		$comment = biz('comment');
		$tool = load('tool');
		//读取audit， type=1 的数据， 说两句待审核表
		
		if( $id == 0 )
		{
			$data = array('type' => 1, 'sortField' => 'id', 'sortOrder' => '', 
			'pageIndex' =>0, 'pageSize' => 100 );
		}
		else
		{
			$data = array('type' => 1, 'sortField' => 'id', 'sortOrder' => '',
			'pageIndex' =>0, 'pageSize' => 3, 'other' => 'id>' . $id );
		}
		
		$data = $this->search( $data );
		$data = $data[ 'data' ];

		$out = array();
		foreach( $data as $k => $v )
		{
			$info = $comment->get1( 'id as i, content as c, sender as u, receiver as u2, parent as p, time as t', array( 'id' => $v['value'] ) );

			// <editor-fold defaultstate="collapsed" desc="贴子因其它原因被删，清空审核数据">
			if( !isset( $info[ 'i' ] ) )
			{
				biz('audit')->del( $v[ 'id' ] );
				continue;
			}
			// </editor-fold>

			$info['n'] = $user->get_name_from_id( $info['u'] );
			$info['n2'] = $user->get_name_from_id( $info['u2'] );
			$info['t'] = $tool->format_time( $info['t'] );
			
			$parent_id = $info[ 'p' ];
			unset( $info[ 'p' ] );

			if( $parent_id != 0 )
			{
				$parent = $comment->get1( 'id as i, content as c, sender as u', array( 'id' => $parent_id ) );
				if( isset($parent['u']) ) $parent['n'] = $user->get_name_from_id( $parent['u'] );

				$parent['sub'] = $info;
			}
			else
			{
				$parent = $info;
			}
			
			$out[ $v[ 'id' ]] = $parent;
		}
		return $out;
	}

	/* 忽略
	 * 参数：
	 *	data： id，有可能是（用逗号隔开的）多个
	 */
	function ignore( $in )
	{
		$this->check_privilege('0,0');

		$ids = $in['data'];

		$audit = biz( 'audit' );

		foreach ( explode( ',', $ids ) as $id )
		{
			if( $id == '' ) continue;
			$audit->del( $id );
		}

		return $audit->out( 1, '成功' );
	}

	// <editor-fold defaultstate="collapsed" desc="显示首贴和回复，队列的回复">
	/* 参数：
	 *	data：贴子id，注意，未必是首贴
	 * 返回值：
	 * {
	 * first:{..}
	 * list:[]
	 * queue:[]
	 * }
	 *
	 * 其中属性为编写，意义如下：
	 * i:id
	 * u：uid
	 * n：name
	 * c：content
	 * t：time
	 * npc：是否npc,0或1，如有
	 * small：小图地址，如有
	 * large：大图地址，如有
	 * pre：回复谁的，如有
	 */
	function view_comment($in)
	{
		$id = $this->number($in, 'data');

		$comment = biz('comment');
		$user = biz('user');
		$tool = load('tool');
		
		$first = $comment->get_from_id( $id );
		if( !isset($first['id']) ) return FALSE;
		if( $first['parent'] > 0 )
		{
			$first = $comment->get_from_id( $first['parent'] );
			$parent_id = $first['parent'];
		}
		else
		{
			$parent_id = $id;
		}

		$out = array(
			'first' => array(),
			'list' => array(),
			'queue' => array()
		);
		
		// <editor-fold defaultstate="collapsed" desc="主题贴">
		$out[ 'first' ] = array(
			'u' => $first['sender'],
			'n' => $user->get_name_from_id( $first['sender'] ),
			'c' => $first['content'],
			't' => $tool->format_time( $first['time'] ),
			);
		if( $user->is_npc( $first['sender'] ) )
			$out[ 'first' ]['npc'] = 1;

		if ( $first['pic'] > 0 )
			{
				$temp = biz('comment_pic')->get_field_from_id( $first['pic'], 'path' );

				if ( $temp != '' )
				{
					$out[ 'first' ]['large'] = $user->frontend . 'logo/' . $temp;
					$out[ 'first' ]['small'] = $out[ 'first' ]['large'] . '.s.jpg';
				}
			}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="回复列表">
		$list = $comment->get( '*', array( 'parent' => $parent_id ), 'id desc' );
		foreach ( $list as $v )
		{
			$list = array(
				'i' => $v['id'],
				'u' => $v['sender'],
				'n' => $user->get_name_from_id( $v['sender'] ),
				'c' => $v['content'],
				't' => $tool->format_time( $v['time'] ),
			);
			if( $user->is_npc( $v['sender'] ) )
				$list['npc'] = 1;

			if ( $v['sub_parent'] > 0 )
			{
				$temp = $comment->get_from_id( $v['sub_parent'] );

				if ( isset( $temp['id'] ) )
				{
					$list['pre'] = array(
						'u' => $temp['sender'],
						'n' => $user->get_name_from_id( $temp['sender'] ),
						'c' => $temp['content'],
						't' => $tool->format_time( $temp['time'] )
					);
					if( $user->is_npc( $temp['sender'] ) )
						$list['pre']['npc'] = 1;
				}
			}

			$out[ 'list' ][] = $list;
		}
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="队列">
		$queue = biz('task_queue')->get( '*', array( 'type' => 8, 'target' => $parent_id ), 'time desc' );
		foreach ( $queue as $v )
		{
			$data = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $v['data'] );
			$data = unserialize( $data );

			$out[ 'queue' ][] = array(
				'u' => $v['user'],
				'n' => $user->get_name_from_id( $v['user'] ),
				'c' => $data['content'],
				't' => $tool->format_time( $v['time'] )
			);
		}
		// </editor-fold>
		
		return $out;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="待审核的图片列表">
	// 包括普通上传图片和说两句上传图片
	function album($in)
	{
		$out = array();
		$user = load('biz.user');
		$album = load('biz.album');
		$comment_pic = load('biz.comment_pic');
		$result = parent::find($in, '*', $this->table(), array(), array(), array(), 'and', 'type in (2,4)');
		foreach ($result['data'] as $key => $value) {
			$row = $value;

			if ($value['type'] == 2)
				$pic = $album->get1('path', array('id' => $value['value']));
			else
				$pic = $comment_pic->get1('path', array('id' => $value['value']));

			$row['path'] = isset($pic['path']) ? config('frontend') . 'logo/' . $pic['path'] : '';
			$row['name'] = $user->get_name_from_id($value['uid']);
			$out[] = $row;
		}
		return $out;
	}

	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="真实照片审核列表">
	/*
	 * 真实照片审核列表，包括视频认证和个人照片审核
	 */
	function video_check($in)
	{
		$out = $this->search(array('type' => 3, 'sortField' => 'id'));

		$album = load('biz.album');
		$user = load('biz.user');
		
		foreach ($out['data'] as $k => $v) {

			$pic = $album->get_from_id($v['value']);

			$v['album_type'] = $this->value($pic, 'type', 0); //album 里面的type， 0，没验证的图片，1代表真实个人照片，2代表视频认证照片
			$v['album_path'] = isset($pic['path']) ? config('frontend') . 'logo/' . $pic['path'] : '';
			$user_info = $user->get_from_id($v['uid']);
			$v['user'] = $this->value($user_info, 'name', '');
			$v['sex'] = $this->value($user_info, 'sex', '');
			$v['logo'] = $user->logo($v['uid']);
			
			//这个用户的相片
			//album 里面的type， 0，没验证的图片，1代表真实个人照片，2代表视频认证照片
			if ($v['album_type'] == 2)
			{
				$other = $this->db->get('*', 'album', array('type in (1) and user=' . $v['uid']), 'type desc,id');
				
				// <editor-fold defaultstate="collapsed" desc="查询马甲情况">
				$imei1 = $this->value($user_info, 'register_imei');
				$imei2 = $this->value($user_info, 'login_imei');
				$puppet =  $user->get('id', array('id != '.$v['uid'].' and (( register_imei="'.$imei1.'" and register_imei !="" and register_imei !="0" ) or ( login_imei="'.$imei2.'" and login_imei !="" and login_imei !="0" ))'));
				$v['puppet_total'] = count($puppet);
				$user_ex = load('biz.user_ex');
				$v['puppet_real_count'] = 0;
				foreach ($puppet as $pk =>$p)
				{
					$v['puppet_real_count'] += $user_ex->get_field_from_id($p['id'], 'is_real');
				}
				// </editor-fold>
				
			}
			else
			{
				$other = $this->db->get('*', 'album', array('type in (1,2) and user=' . $v['uid'] . ' and id < ' . $v['value']), 'type desc,id', '5');
			}
			
			foreach ($other as $index => $item) {
				$other_list = array();
				$other_list['path'] = isset($item['path']) ? config('frontend') . 'logo/' . $item['path'] : '';
				$other_list['id'] = isset($item['id']) ? $item['id'] : '';
				$v['other_list'][] = $other_list;
			}

			$out['data'][$k] = $v;
		}
		$out['total'] = count($out['data']);
		//print_r($out);die;
		return $out;
	}

	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="通过审核">
	function pass($in)
	{
		$type = $this->number($in, 'type');

		$audit = load('biz.audit');
		//说两句通过的时候直接删除，因为说两句页面没有 audit.id传入，所以特殊处理，本来传入 audit.id 就可以处理
		if ($type == 2)
			return $audit->del2($in);
		else
			return $audit->pass($in);
	}

	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="全部通过审核">
	/*
	 * 根据type或者value
	 */
	function pass_all($in)
	{
		$audit = load('biz.audit');
		if ($this->value($in, 'type')) {
			$type_array = explode(',', $in['type']);
			foreach ($type_array as $v) {
				$audit->del2(array('type' => $v));
			}
		}
	}

	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="不通过审核">
	function donotpass($in)
	{
		$audit = load('biz.audit');
		return $audit->donotpass($in);
	}

	// </editor-fold>
	// <editor-fold defaultstate="collapsed" desc="删除审核中的照片">
	function del_pic($in)
	{
		if ($in['id'] > 0) {
			$audit = load('biz.audit');
			$info = $audit->get_from_id($in['id']);

			if ( isset($info['type']) && ($info['type'] == 2 || $info['type'] == 3) ) { //2普通图片，3视频认证/个人照片
				biz('album')->remove(array('id' => $info['value'], 'uid' => $info['uid']));
			} elseif ( isset($info['type']) && $info['type'] == 4 ) { //说两句的图片
				//删除说两句及图片
				$comment_id = biz('comment_pic')->get_field_from_id($info['value'], 'comment');//通过图片id拿到comment_id
				biz('comment')->del($comment_id);
			}

			return $audit->del2($in);
		}
	}

	// </editor-fold>
	
	/*
	 * 删除指定的说两句
	 * 参数：id，value
	 */
	function del_comment($in)
	{
		$in = $in['data'];
		$id = $this->number($in, 'id');
		$value = $this->number($in, 'value');
		
		if( $id > 0 && $value > 0 )
		{
			$audit = load('biz.audit');
			$audit -> del($id);
			biz('comment') -> del($value);
			
			return $audit->out( 1, '成功' );
		}
	}
	
	// <editor-fold defaultstate="collapsed" desc="npc通过验证">
	function npc_pass($in)
	{
		return biz('audit')->npc_pass($in);
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="已通过视频验证的人">
	function user_is_real_list($in)
	{
		$user = load('biz.user');
		$album = load('biz.album');
		
		$out = array();
		$where = array(0=>'u.id = e.id and e.is_real=1');

		if( $this->value($in, 'q') != '' )
		{
			$in['id'] = $user->get_id_from_name($in['q']);
			if( $in['id'] ) $where[] = 'u.id='.$in['id'];
		}
		
		if( $this->value( $in, 'npc' ) != '' )
		{
			$where[] = 'u.npc='.$in['npc'];
		}
		
		if( $this->value( $in, 'sex' ) != '' )
		{
			$where[] = 'u.sex='.$in['sex'];
		}

		$where = $this->db->term( $where, 'and' );
	
		if( $this->value( $in, 'sortField', '') != '' )
		{
			$orderby = $in['sortField'];
			if( isset( $in['sortOrder'] ) ) $orderby .= ' ' . $in['sortOrder'];
		}
		else
		{
			$orderby = 'u.id desc';
		}
		
		if( isset( $in['pageIndex'] ) && isset( $in['pageSize'] ) )
		{
			$limit = ( $in['pageIndex'] * $in['pageSize'] ) . ', ' . $in['pageSize'];
		}
		$table = 'user u,user_ex e';
		$sql = "select count(1) as total from $table";
		if( $where != '' ) $sql .= ' where ' . $where;
		$data = $this->db->unique( $sql );

		$out['total'] = $data['total'] + 0;

		if( $out['total'] == 0 )
		{
			$result['data'] = array();
		}
		else
		{
			$sql = "select * from $table";
			if( $where != '' ) $sql .= ' where ' . $where;
			if( isset( $orderby ) ) $sql .= ' order by ' . $orderby;
			if( isset( $limit ) ) $sql .= ' limit ' . $limit;
			
			$result['data'] = $this->db->select( $sql );
		}
		
		foreach($result['data'] as $key => $value)
		{
			$out['data'][$key] = $value;
			$out['data'][$key]['logo'] = $user->logo($value['id']);
			
			$temp = $album -> get('id, type, path', array('user'=>$value['id']), 'type desc,id desc', '20');
			$album_list = array();
			foreach($temp as $a_k => $value)
			{
				$album_list[$a_k]['id'] = $value['id'];
				$album_list[$a_k]['type'] = $value['type'];
				$album_list[$a_k]['path'] = config('frontend') . 'logo/' . $value['path'];
			}
			$out['data'][$key]['album_list'] = $album_list;
		}
		
		return $out;
	}
	// </editor-fold>

}

$action = new action();
$action->run();
?>