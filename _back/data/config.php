<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'config';
	}
	
	function search( $data )
	{
		$this->check_privilege( '0,7' );
		
		$field = '*';
		$table = $this->table();
		$equal = array( 'user' );
		$like = array( 'section', 'key', 'value' );
		$q = array( 'section', '`key`', 'value' );
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'time';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->fill_name_from_uid( $result['data'], 'user', 'name', '全部' );
		$result['data'] = $this->format_datetime( $result['data'], 'time' );
		
		return $result;
	}

	function row( $in )
	{
		if( $in['section'] == 'config'  && $in['key'] == 'url' )
		{
			$base = load( 'biz.base' );
			if( !$base->is_manager( $in['user'] ) )
				$in['user'] = 237;
		}
		return $in;
	}
	
}

$action = new action();
$action->run();
?>