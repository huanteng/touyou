<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'interface_log';
	}
	function get( $data )
	{
		$this->check_privilege( '0,62' );
		
		$field = '*';
		$table = $this->table();
		
		$result = $this->db->get( $field, $table );
		
		$result = $this->fill_name_from_uid( $result, 'uid', 'name', '用户不存在' );
		$result = $this->format_datetime( $result, 'time', 'H:i:s' );
		
		foreach( $result as $k => $v )
		{
			$result[$k]['sql'] = stripslashes( $v['sql'] );
		}
		
		$this->truncate( array() );
		
		return $result;
	}
	
	function truncate( $data )
	{
		$this->check_privilege( '0,62' );
		
		$sql = 'truncate table ' . $this->table();
		$this->db->command( $sql );
	}
	
	function save( $data )
	{
		$this->check_privilege( '0,62' );
		
		$data = $this->php_json_decode( $data['data'] );
		
		//print_r($data);die;
		
		$system = array( 'interface', 'back' );
		
		$json = array();
		foreach( $system as $k => $v )
			if( $data[ $v ] != 'false' )
			{
				$json[ $v ] = array( 'key' => array(), 'save' => array() );
			}
			else
			{
				unset( $system[ $k ] );
			}
		
		foreach( $system as $s)
		{
			foreach( array( 'uid', 'url', 'input', 'response', 'sql' ) as $k )
				if( $data[ $s . '_key_' . $k ] != '' ) $json[$s]['key'][ $k ] = $data[ $s . '_key_' . $k ];
				
			foreach( array( 'input', 'response', 'sql' ) as $k )
				if( $data[ $s . '_save_' . $k ] != 'false' ) $json[$s]['save'][ $k ] = '';
		}
		
		if( $data['interface_key_heart'] != 'false' ) $json['interface']['key']['heart'] = $data['interface_key_heart'];

		//if( $data[ 'error' ] != 'false' ) $json[ 'error' ] = array();
		
		$content = "<?php\n";
		$content .= "\$log=unserialize('" . serialize( $json ) . "');\n";
		$content .= "?>";
		//print_r($content);die;
		file_put_contents("../../log.conf.php", $content);
		return array( 'code' => 1, 'message' => '保存成功' );
	}
	
}

$action = new action();
$action->run();
?>