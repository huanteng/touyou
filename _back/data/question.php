<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'question';
	}
    
	function search( $data )
	{
		$this->check_privilege( '0,68' );
		
		$field = '*';
		$table = $this->table();
		$equal = array();
		$like = array('title');
		$q = array('title');
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
        $question = load('biz.question');
		$result['data'] = $this->fill_field( $result['data'], 'type', 'type_name', $question->get_type_dict() );
		
		return $result;
	}
	
	function type()
    {
		$this->check_privilege( '0,68' );
		
        $question = load('biz.question');
        foreach( $question->get_type_dict() as $key => $value )
        {
            $out[] = array('id'=>$key,'text'=>$value);
        }
        return $out;
    }

	
}

$action = new action();
$action->run();
?>