<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'admin_function';
	}
	function privilege()
	{
		return array();
	}
	function search( $data )
	{
		$this->check_privilege( '0,2' );
		
		$field = '*';
		$table = $this->table();
		$equal = array( 'parent_id' );
		$like = array('name', 'url');
		$q = array('name', 'url');
		if( !isset( $data['sortField'] ) )
		{
			$data['sortField'] = 'parent_id, `order`';
			$data['sortOrder'] = '';
		}
		
		if( isset( $data['parent_id'] ) ) unset( $data['pageSize'] );
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		foreach( $result['data'] as $k => $v )
		{
			$sql = "select id, name from administrator where id in( select administrator from administrator_privilege where privilege = '$v[id]')";
			$data = $this->db->select( $sql );
			
			$user = array();
			$user_name = array();
			foreach( $data as $v )
			{
				array_push( $user, $v['id'] );
				array_push( $user_name, $v['name'] );
			}
			$result['data'][$k]['user'] = implode( ',', $user );
			$result['data'][$k]['user_name'] = implode( ',', $user_name );
		}
		
		return $result;
	}
	
	function menu( $data )
	{
		$field = 'id, name';
		$table = 'admin_function';
		$equal = array( 'parent_id', 'is_view' );
		$like = array();
		$q = array();

		$data['is_view'] = 1;
		$data['sortField'] = 'name, `order`';
		$data['sortOrder'] = '';
		
		$data = parent::find( $data, $field, $table, $equal, $like, $q );
		$data['data'][] = array( 'id' => 0, 'name' => '根' );
		return $data['data'];
	}
	
	// 用于左树，只显示有权限的及可视的
	function tree( $data )
	{
		$cookie = load( 'cookie' );
		$account = $cookie->get( 'account', true);
		$data = array();
		if( $account > 0 )
		{
			$sql = "select id, name, url, parent_id, is_new_window from admin_function where is_view = 1 and id in( select privilege from administrator_privilege where administrator = '$account') or id = 15 or parent_id = 15 order by `order`";
			$data = $this->db->select( $sql );
		}
		return $data;
	}
	// 全部显示，用于调整排序
	function order( $data )
	{
		$sql = "select id, name, parent_id from admin_function order by `order`";
		$data = $this->db->select( $sql );
		return $data;
	}
	
	function save( $data )
	{
		$rows = $this->php_json_decode( $data['data'] );
		
		$file = 'biz.' . $this->table();
		$obj = load( $file );
		
		foreach ($rows as $row)
		{
			$id = $this->value( $row, 'id', '' );
		    $state = $this->value( $row, '_state', '' );
			if($state == "added" || $id == ""){ //新增：id为空，或_state为added
				$user = array();
				if( isset( $row['user'] ) )
					explode( $row['user'], ',' );
				unset( $row['user'] );
				
				$id = $obj->add($row);
				foreach( $user as $u )
				{
					$this->db->add( 'administrator_privilege', array( 'administrator' => $u, 'privilege' => $id ) );
				}
			}
			else if ($state == "removed" || $state == "deleted")
			{
				$this->db->del( 'administrator_privilege', array( 'privilege' => $id ) );
				$obj->del($id);
			}
			else if ($state == "modified" || $state == "")  //更新：_state为空或modified
			{
				$new_user = explode( ',', $row['user'] );
				unset( $row['user'] );
				
				$obj->set($row);
				
				$sql = "select administrator from administrator_privilege where privilege = '$id'";
				$old_user = $this->db->select( $sql );
				foreach( $old_user as $i => $o )
					foreach( $new_user as $j => $n )
					{
						if( $o['administrator'] == $n)
						{
							unset( $old_user[$i] );
							unset( $new_user[$j] );
						}
					
					}
				
				foreach( $old_user as $u )
				{
					$this->db->del( 'administrator_privilege', array( 'administrator' => $u['administrator'], 'privilege' => $id ) );
				}
				foreach( $new_user as $u )
				{
					$this->db->add( 'administrator_privilege', array( 'administrator' => $u, 'privilege' => $id ) );
				}
			}
		}
		
		return array( 'code' => 1, 'message' => '成功操作' );
	}	

	// 更新排序
	function save_order( $data )
	{
		global $_pid;
		$_pid = array( '-1' => 0 );
		$rows = $this->php_json_decode( $data['data'] );
				
		$this->save_order_( $rows );
		
		return array( 'code' => 1, 'message' => '成功操作' );
	}
	
	function save_order_( $rows )
	{
		global $_pid;
		
		$file = 'biz.' . $this->table();
		$obj = load( $file );
		
		foreach ($rows as $order => $row)
		{
			$parent_id = $_pid[ $row['_pid'] ];
			
			if( isset( $row['id'] ) )
			{
				$id = $row['id'];
				$data = array( 'id' => $id, 'name' => $row['name'], 'parent_id' => $parent_id, 'order' => $order );
				$obj->set( $data );
			}
			else
			{
				$data = array( 'name' => $row['name'], 'parent_id' => $parent_id, 'order' => $order );
				$id = $obj->add( $data );
			}
			$_pid[ $row['_id'] ] = $id;
			
			if( isset( $row['children'] ) )
			{
				$this->save_order_( $row['children'] );
			}
		}
		
	}
}

$action = new action();
$action->run();
?>