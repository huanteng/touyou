<?php
require 'backend.php';

class action extends backend
{
	function table() 
	{
		return 'partner';
	}

	// <editor-fold defaultstate="collapsed" desc="函数 row">
	function row($in)
	{
		$pass = $this->value($in, 'pass');
		if($pass != '')
		{
			$partner = load('biz.partner');
			$in['pass'] = $partner->password($pass);
		}
		unset($in['last']);
		return $in;
	}
	// </editor-fold>

	function search( $data )
	{
		$this->check_privilege( '0,97' );
		
		$field = 'id, name, `key`,status, last';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array();
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		$result['data'] = $this->format_datetime( $result['data'], 'last' );		
		return $result;
	}	
}

$action = new action();
$action->run();
?>