<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'comment_pic';
	}

	function search($data)
	{
		$this->check_privilege('0,27');

		$field = '*';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array();
		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'id';
			$data['sortOrder'] = 'desc';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		return $result;
	}

}

$action = new action();
$action->run();
?>