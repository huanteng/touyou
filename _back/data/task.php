<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'task';
	}
    
	function search( $data )
	{
		$this->check_privilege( '0,69' );
		
		$field = '*';
		$table = $this->table();
		$equal = array('type');
		$like = array();
		$q = array();
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'expires';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->format_datetime( $result['data'], 'expires' );
				
		$task = load('biz.task');
		$type = $task->type();
		foreach( $result['data'] as $key => $value )
		{
			$result['data'][$key]['status_name'] = $value['status'] == 1 ? '有效' : '无效';
			$result['data'][$key]['type_name'] = isset($type[ $value['type'] ]) ? $type[ $value['type'] ] : '';
		}		
				
		return $result;
	}
	
	/*
	 * 返回全部数据，供菜单下拉框等使用
	 */
	function drop()
	{
		$this->check_privilege('0,69');

		$field = 'id, name';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array();
		if (!isset($data['sortField'])) {
			$data['sortField'] = 'id';
			$data['sortOrder'] = '';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		return $result['data'];
	}
	
	function row( $data )
	{
		$this->check_privilege( '0,69' );
		
		if( $this->value( $data, 'expires', '' ) != '' )
		{
			$data['expires'] = strlen( $data['expires'] ) > 11 ? strtotime( $data['expires'] ) : strtotime( date('Y-') . $data['expires'] );
		}else{
			$data['expires'] = 0;//永不过期
		}
		return $data;
	}
	
	function type()
	{
		$this->check_privilege( '0,69' );
		
		$task = load('biz.task');
		$type = $task->type();
		foreach($type as $key => $value)
		{
			$out[] = array('id'=>$key,'text'=>$value);
		}
		return $out;
	}
	
	/*
	 * 分配任务
	 */
	function assign( $in )
	{
		$this->check_privilege( '0,69' );
		
		if( isset($in['uid']) && isset($in['task_id']) && is_numeric($in['task_id']) )
		{
			$task = load('biz.task');
			$uid_arr = array();
			$uid_arr = explode(',', $in['uid']);
			foreach( $uid_arr as $uid )
			{
				if( is_numeric($uid) ) $task -> assign_by_id( $uid, $in['task_id'] );
			}
		}
	}

	// <editor-fold defaultstate="collapsed" desc="回贴有奖">
	function init_comment($in)
	{
		$data = biz('task_queue')->get1( '*', array( 'type' => '9' ), 'time desc' );
		if( isset( $data[ 'id' ] ) )
		{
			$data['data'] = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $data['data'] );
			$out = unserialize( $data['data'] );
			$out[ 'target' ] = $data[ 'target' ];
			$out[ 'time' ] = $data[ 'time' ];
		}
		else
		{
			$out = biz('task')->get_from_id( 44 );
			$array = explode( '=', $out[ 'data' ] );
			$out[ 'target' ] = $array[1];
			$out[ 'time' ] = time();
		}

		$out[ 'time' ] = date('Y-m-d H:i:s', $out[ 'time' ] );
		
		$out[ 'online' ] = '1';
		return $out;
	}
	function set_comment($in)
	{
		$this->check_privilege( '0,105' );

		$in = $this->php_json_decode( $in[ 'data' ] );

		$data = array( 'type' => 9, 'target' => $in[ 'target' ], 'time' => strtotime( $in[ 'time' ] ),
			'allow_repeat' => TRUE, 'data' => array() ) ;
		foreach( array( 'name', 'introduce', 'description', 'gift', 'count', 'text', 'memo', 'online' ) as $v )
		{
			$data['data'][$v] = $in[ $v ];
		}

		$data[ 'status' ] = ( $in[ 'on_queue' ] == '1' ) ? 5 : 1;
	
		biz('task_queue')->create( $data );

		return biz('base')->out( 1, '操作成功' );
	}
	// </editor-fold>


}

$action = new action();
$action->run();
?>