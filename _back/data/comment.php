<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'comment';
	}

	function search($in)
	{
		$this->check_privilege('0,27');

		$user = load('biz.user');

		if (isset($in['uid']) && !is_numeric($in['uid']) && $in['uid'] != '') {
			$in['sender'] = $user->get_id_from_name($in['uid']);
		} elseif (isset($in['uid']) && is_numeric($in['uid'])) {
			$in['sender'] = $in['uid'];
		}

		$field = '*';
		$table = $this->table();
		$equal = array('sender','type');
		$like = array();
		$q = array('content');
		if ($this->value($in, 'sortField') == '') {
			$in['sortField'] = 'id';
			$in['sortOrder'] = 'desc';
		}

		$result = parent::find($in, $field, $table, $equal, $like, $q);

		$result['data'] = $this->fill_user($result['data'], 'sender', array('name' => 'name', 'npc' => 'npc'));

		foreach ($result['data'] as $k => $v) {
			$v['time'] = date('m-d H:i', $v['time']);

			if ($v['pic'] > 0) {
				$temp = $this->db->unique('select path from comment_pic where id = ' . $v['pic']);

				if (isset($temp['path'])) {
					$v['large'] = $user->frontend . 'logo/' . $temp['path'];
					$v['small'] = $v['large'] . '.s.jpg';
				}
			}
			$v['uid'] = $v['sender'];
			unset($v['sender']);
			$result['data'][$k] = $v;
		}

		return $result;
	}
	
	// <editor-fold defaultstate="collapsed" desc="列表(包括主题，回复，回复队列)">
	function lists($in)
	{
		$this->check_privilege('0,27');
		$out = array();
		$comment = load('biz.comment');
		
		$out[] = $comment->get_from_id($in['id']);
		
		$replys = $comment->replys($in);
		$replys = array_reverse($replys);
		
		foreach($replys as $value)
		{
			$out[] = $value;
		}
		
		$task_queue = load('biz.task_queue');
		$queue_list = $task_queue->search();
		foreach($queue_list as $value)
		{
			$out[] = array();
		}
		return $out;
	}
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="回复列表">
	/*
	 * 说两句的回复列表
	 */
	function reply($in)
	{
		$this->check_privilege('0,27');

		$comment = load('biz.comment');
		$out = $comment->replys($in);
		$out['data'] = array_reverse($out['data']);
		return $out['data'];
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="回复">
	/* 参数：
	 *	id
	 *	uid
	 *	content
	 *	time
	 * 返回值：{code, memo}
	 */
	function answer($in)
	{
		$this->check_privilege('0,27');

		$data = isset($in['data']['decode']) && $in['data']['decode'] == false ?  $in['data'] : $this->php_json_decode( $in['data'] ) ;

		$id = $data['id'];
		$time = $data['time'];

		$comment = biz('comment');
		$task_queue = biz('task_queue');

		// <editor-fold defaultstate="collapsed" desc="计算时间">
		if( $time == '' )
		{
			$time = time();
		}
		elseif( substr( $time, 0, 1 ) == '+' )
		{
			$info = $comment->get_from_id( $id );
			$parent_id = ( $info[ 'parent' ] > 0 ) ? $info[ 'parent' ] : $id;

			$queue = $task_queue->get1( 'time', array( 'type' => 8, 'target' => $parent_id ), 'time desc' );

			$time = $comment->value( $queue, 'time', time() ) + substr( $time, 1 ) * 60;
		}
		else
		{
			$time = strtotime( $time );
		}
		// </editor-fold>

		$account = load( 'cookie' )->get( 'account', true);
		
		load( 'database' )->del( 'audit', array( 'type' => 1, 'value' => $id ) );

		// <editor-fold defaultstate="collapsed" desc="如果没发送人，随机选择一个">
		if( $data[ 'uid' ] == '' )
		{
			if( !isset( $parent_id ) )
			{
				$info = $comment->get_from_id( $id );
				$parent_id = ( $info[ 'parent' ] > 0 ) ? $info[ 'parent' ] : $id;
			}
			
			$temp = $this->npc( array( 'data' => json_encode( array( 'id' => $parent_id, 'sex' => '' ) ) ) );
			$data[ 'uid' ] = $temp[ 'uid' ];
		}
		// </editor-fold>

		$task_queue->create( array('type' => 8, 'data'=> array( 'content' => $data['content'] ), 'target' => $id,
			'time' => $time, 'user' => $data['uid'], 'creater' => $account, 'allow_repeat' => TRUE ) );

		return array( 'code' => 1, 'memo' => '成功' );
	}
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="发表">
	function say($in)
	{
		$data = $in['data'];
		
		$uid = $this->value($data, 'uid');
		if( !is_numeric($uid) ) $uid = biz('user')->get_id_from_name($uid);
		
		$content = $this->value($data, 'content');
		$comment_pic_id = $this->number($data, 'comment_pic_id');
		$time = $this->value($data, 'time');
		$mood_id = $this->number($data, 'mood_id');
		$reply = $this->value($data, 'reply');
				
		// <editor-fold defaultstate="collapsed" desc="计算时间">
		if( $time == '' )
		{
			$time = time();
		}
		elseif( substr( $time, 0, 1 ) == '+' )
		{
			$queue = biz('task_queue')->get1( 'time', array( 'type' => 1 ), 'time desc' );
			$time = biz('comment')->value( $queue, 'time', time() ) + substr( $time, 1 ) * 60;
		}
		else
		{
			$time = strtotime( $time );
		}
		// </editor-fold>

		//type,data,time,target,user,status,creater	
		$name = biz('user')->get_name_from_id($uid);
		$data = array( 'name' => $name, 'content' => $content, 'comment_pic_id' => $comment_pic_id );
			
		$queue_id = biz('task_queue')->create( array('type' => 1, 'data'=> $data, 'target' => 0, 'time' => $time, 'user' => $uid, 'creater' => load( 'cookie' )->get( 'account', true ) ) );
	
		// <editor-fold defaultstate="collapsed" desc="营销回复的处理，加入到队列里面">
//		if( is_array($reply['reply_content']) )
//		{
//			foreach( $reply['reply_content'] as $k => $v )
//			{
//				if( $v != '' )
//				{				
//					// <editor-fold defaultstate="collapsed" desc="计算时间">
//					if( $reply['reply_time'][$k] == '' )
//					{
//						$reply_time = time();
//					}
//					elseif( substr( $reply['reply_time'][$k], 0, 1 ) == '+' )
//					{
//						$queue = biz('task_queue')->get1( 'time', array( 'type' => 2, 'status' => 5, 'target'=> $queue_id ), 'time desc' );
//						$reply_time = biz('comment')->value( $queue, 'time', time() ) + substr( $time, 1 ) * 60;
//					}
//					else
//					{
//						$reply_time = strtotime( $reply['reply_time'][$k] );
//					}
//					// </editor-fold>
//					
//					$reply_data[] = array( 'complete'=>0, 'sender' => $reply['reply_uid'][$k], 'content' => $v, 'time'=>$reply_time );	
//					
//					$reply_queue = array( 'type' => 2, 'time' => $reply_time, 'status' => 5, 'target' => $queue_id, 'data' => $reply_data, 'user'=>$reply['reply_uid'][$k], 'creater'=>load( 'cookie' )->get( 'account', true ) );		
//					biz('task_queue')->create($reply_queue);
//				}
//			}
//		}
		// </editor-fold>
		
		//非必须的: 抓取回来的心情id，记录使用过，以免重复使用同一个心情	
		if($mood_id > 0) biz('mood')->set( array('times'=>1, 'id'=>$mood_id) );
		
	}

	// </editor-fold>
	
	/*
	 * 上传图片
	 * 本来想直接调用 biz/comment的 upload 函数的 ，但因为 $_FILES 无法传过去
	 * 参数：
	 * user：uid
	 * name：$_FILES 的key值
	 */
	function upload_pic( $data )
	{
		$result = -1;
		$name = isset( $data['name'] ) ? $data['name'] : 'file';

		if ( isset( $_FILES[$name] ) && $_FILES[$name]['error'] == 0 && isset( $data['user'] ) && is_numeric( $data['user'] ) )
		{
			$user = load( 'biz.user' );

			$dir = $user->logo_base_dir . $user->logo_dir( $data['user'] );
			if ( ! is_dir( $dir ) ) mkdir( $dir, 0755, true );
			$file = uniqid() . '.jpg';

			if ( move_uploaded_file( $_FILES[$name]['tmp_name'], $dir . $file ) )
			{
				$tool = load( 'tool' );
				$tool->resize_image( $dir . $file, 120, 120, 1, $dir . $file . '.s.jpg' );

				$path = $user->logo_dir( $data['user'] ) . $file;
				$result = $this->db->add( 'comment_pic', array( 'path' => $path ) );
					
				//存入audit,待审核表
				$audit = load('biz.audit');
				$audit -> add( array('uid'=>$data['user'], 'value'=>$result, 'type'=>4) );
				
			}
			else
			{
				$result = -2;
			}
		}

		return array('code'=>$result);
	}

	// </editor-fold>

	/* 获得npc
	 * 参数（加密后的）：
	 *	id：主题贴id
	 *	sex：性别限制，空为不限，0为男，1为女
	 * 返回值：{uid, name}
	 */
	function npc( $in )
	{
		$data = $this->php_json_decode( $in['data'] );
		$id = $data['id'];
		$sex = $data['sex'];
				
		$user = biz('user');
		
		$term = array( 'npc' => 1 );
		if( $sex !== '' )
			$term[ 'sex' ] = $sex;

		$uids1 = biz('comment')->get( 'distinct sender as uid', array( "id=$id or parent=$id") );
		$uids2 = biz('task_queue')->get( 'distinct user as uid', array( 'type' => 8, 'target' => $id ) );
		$uid = $user->implode2( array_merge( $uids1, $uids2 ), '', ',' );
		$term[] = 'id not in(' . $uid . ')';

		$info = $user->get1( '*', $term, 'rand()' );

		return array( 'uid' => $info['id'], 'name' => $info['name'] );
	}
	
	/*
	 * 回复队列（用作营销）
	 */
	function answer_queue($in)
	{
		$in = $this->php_json_decode($in['data']);
		
		$id = $this->number($in, 'id');
		$c = $this->value($in, 'content');
		$t = $this->value($in, 'time');
		$u = $this->value($in, 'uid');

		if($id > 0)
		{
			foreach( $c as $k => $v )
			{
				if($v != '')
				{
					$time = $this->value($t, $k);
					$uid = $this->number($u, $k);
					$data['data'] = array('id'=>$id, 'content'=>$v, 'time'=>$time, 'uid'=>$uid, 'decode'=>false);
					$this->answer( $data );
				}
			}
		}
	}
}

$action = new action();
$action->run();
?>