<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'event';
	}

	function search($data)
	{
		$this->check_privilege('0,90');

		$user = load('biz.user');
		if (isset($data['user']) && !is_numeric($data['user']) && $data['user']!='') {
			$data['uid'] = $user->get_id_from_name($data['user']);
		} else {
			$data['uid'] = $this->value($data, 'user', 0);
		}

		$field = '*';
		$table = $this->table();
		$equal = array('uid');
		$like = array();
		$q = array('description');
		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'time';
			$data['sortOrder'] = 'desc';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);
		$result['data'] = $this->fill_name_from_uid($result['data'], 'uid', 'name', '');
		$result['data'] = $this->format_datetime($result['data'], 'time');
		$result['data'] = $this->fill_field($result['data'], 'type', 'type_name', array('0' => '系统', '1' => '正常'));

		return $result;
	}

}

$action = new action();
$action->run();
?>