<?php

require 'backend.php';

class action extends backend
{
	function table()
	{
		return 'match_summary_log';
	}

	function search($data)
	{
		$this->check_privilege('0,114');

		$field = '*';
		$table = $this->table();
		$equal = array( 'uid' );
		$like = array();
		$q = array();

		$sortField = $this->value($data, 'sortField');
		if ( $sortField == '') {
			$data['sortField'] = 'w1';
			$data['sortOrder'] = 'desc';
		}
		elseif( $sortField == 's' )
		{
			$data['sortField'] = 's1+s2+s3+cs1+cs2+cs40';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		$user = biz( 'user' );
		$props = biz( 'props' );

		foreach( $result['data'] as $k => $v )
		{
			$v[ 'day' ] = date( 'Y-m-d', $v[ 'day' ] );
			$v[ 'name' ] = $user->get_name_from_id( $v[ 'uid' ] );
			$v[ 'gold' ] = $props->gold( $v[ 'id' ] );
			
			$result['data'][$k] = $v;
		}

		return $result;
	}

}

$action = new action();
$action->run();
?>