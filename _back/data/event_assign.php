<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'event_assign';
	}

	function row($in)
	{
		unset($in['gift'], $in['name']);
		
		if( $this->value( $in, 'begin_time', '' ) != '' )
		{
			$in['begin_time'] = strlen( $in['begin_time'] ) > 11 ? strtotime( $in['begin_time'] ) : strtotime( date('Y-') . $in['begin_time'] );
		}
		else
		{
			$in['begin_time'] = time();
		}
		
		return $in;
	}

	function search($data)
	{
		$this->check_privilege('0,90');
		
		$user = load('biz.user');
		if (isset($data['user']) && !is_numeric($data['user'])) {
			$data['uid'] = $user->get_id_from_name($data['user']);
		} else {
			$data['uid'] = $this->value($data, 'user', 0);
		}

		$field = '*';
		$table = 'event_assign';
		$equal = array('uid');
		$like = array();
		$q = array();
		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'rank';
			$data['sortOrder'] = 'desc';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);
		$result['data'] = $this->fill_name_from_uid($result['data'], 'uid', 'name', '');

		$gift = load('biz.event_gift');
		$result['data'] = $this->fill_field($result['data'], 'gift_id', 'gift', $gift->dict());

		$result['data'] = $this->format_datetime( $result['data'], 'begin_time', 'datetime' );
		
		return $result;
	}
	
	function uses($in)
	{
		$event = load('biz.event');
		$event->execute( array( 'uid' => $in['uid'], 'npc' => 1 ) );
	}

}

$action = new action();
$action->run();
?>