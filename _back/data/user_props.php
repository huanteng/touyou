<?php
require 'backend.php';

class action extends backend
{
	function table() 
    {
		return 'user_props';
	}
    
    function search( $data )
	{
		$this->check_privilege( '0,66' );
		
		if ( $this->value($data, 'props') != '' ){
			$where = array( ' user_props.user=user.id' );
			$where[] = 'props = '.$data['props'];	
		} else {
			$where = array( ' user_props.user=user.id' );
		}
		
		if( $this->value($data, 'user') != '' ){
			if( !is_numeric( $data['user'] ) )
			{
				$user = load( 'biz.user' );
				$data['user'] = $user->get_id_from_name( $data['user'] );
			}
			$where[] = 'user in ('.$data['user'].')';
		}
		
		if ( $this->value($data, 'npc') != '' ){
			$where[] = 'npc = '.$data['npc'];
		}

		$where = $this->db->term( $where, ' and ' );
		
		if( $this->value( $data, 'sortField', '') != '' )
		{
			$orderby = $data['sortField'];
			if( isset( $data['sortOrder'] ) ) $orderby .= ' ' . $data['sortOrder'];
		}
		else
		{
			$orderby = ' expire desc,id asc ';
		}
		
		if( isset( $data['pageIndex'] ) && isset( $data['pageSize'] ) )
		{
			$limit = ( $data['pageIndex'] * $data['pageSize'] ) . ', ' . $data['pageSize'];
		}

		$table = ' user_props , user ';
		
		$sql = "select count(1) as total from $table";
		if( $where != '' ) $sql .= ' where ' . $where;
		$data = $this->db->unique( $sql );
		$result['total'] = $this->number($data, 'total', 0);
		
		if( $result['total'] == 0 )
		{
			$result['data'] = array();
		}
		else
		{
			$sql = "select user_props.id,user,props,quantity,expire,name,npc from $table";
			if( $where != '' ) $sql .= ' where ' . $where;
			if( isset( $orderby ) ) $sql .= ' order by ' . $orderby;
			if( isset( $limit ) ) $sql .= ' limit ' . $limit;

			$result['data'] = $this->db->select( $sql );
		}
             
        $temp = $this->db->get('id,name','props');
        foreach($temp as $value){
            $props_data[$value['id']] = $value['name'];
        }
        
        foreach($result['data'] as $key=>$value){
            $result['data'][$key]['props_name'] = isset($props_data[$value['props']]) ? $props_data[$value['props']] : '';
        }
        
		$result['data'] = $this->format_datetime( $result['data'], 'expire' );
				
		return $result;
	} 
	
	/*
	 * 使用道具
	 */
	function use_props( $in )
	{
		$this->check_privilege( '0,66' );
		
		if( isset($in['uid']) && isset($in['id']) )
		{
			$in['count'] = 1;
			return biz( 'props' )->use_props( $in );
		}
	}

	/*
	 * 删除道具
	 */
	function delete( $in )
	{
		$this->check_privilege( '0,66' );

		$account_name = load('cookie')->get( 'account_name', true );

		$user_props = biz( 'user_props' );

		$info = $user_props->get_from_id( $in[ 'data' ] );

		$user_props->add_more( array( 'uid' => $info[ 'user' ], 'props' => array( $info['props'] => -$info['quantity'] ),
			'type' => 0, 'expires' => 0, 'memo' => $account_name . '删除' ) );
		return $user_props->out( '成功删除' );
	}
	
	/*
	 * 用户金币数量列表
	 */
    function lists( $data )
	{
		$this->check_privilege( '0,66' );
		
		$where = array( ' user_props.user=user.id and props = 1' );
		
		if ( $this->value($data, 'user') != '' ){
			$where[] = 'user in ('.$data['user'].')';
		}
		
		if ( $this->value($data, 'npc') != '' ){
			$where[] = 'npc = '.$data['npc'];
		}

		$where = $this->db->term( $where, ' and ' );
		
		if( $this->value( $data, 'sortField', '') != '' )
		{
			$orderby = $data['sortField'];
			if( isset( $data['sortOrder'] ) ) $orderby .= ' ' . $data['sortOrder'];
		}
		else
		{
			$orderby = ' quantity desc ';
		}
		
		if( isset( $data['pageIndex'] ) && isset( $data['pageSize'] ) )
		{
			$limit = ( $data['pageIndex'] * $data['pageSize'] ) . ', ' . $data['pageSize'];
		}
		else
		{
			$limit = '0,10';
		}

		$table = ' user_props , user ';
		
		$sql = "select count(1) as total from $table";
		if( $where != '' ) $sql .= ' where ' . $where;
		$data = $this->db->unique( $sql );
		$result['total'] = $data['total'];
		
		if( $result['total'] == 0 )
		{
			$result['data'] = array();
		}
		else
		{
			$sql = "select user_props.id,user,quantity,name,npc from $table";
			if( $where != '' ) $sql .= ' where ' . $where;
			if( isset( $orderby ) ) $sql .= ' order by ' . $orderby;
			if( isset( $limit ) ) $sql .= ' limit ' . $limit;
			
			$result['data'] = $this->db->select( $sql );
		}
        			
		return $result;
	}
	
}

$action = new action();
$action->run();
?>