<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'online_report';
	}

	function search( $data )
	{
		$this->check_privilege( '0,61' );
		
		$data = $this->value($data, 'data', $data);
		
		$field = '*';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array();
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'id';
			$data['sortOrder'] = '';
		}

		$time = load( 'time' );

		$begin = ( isset( $data['begin'] ) ) ? strtotime( $data['begin'] ) : $time->today();
		$end = ( isset( $data['end'] ) ) ? strtotime( $data['end'] ) : $time->tomorrow();

		$other = "time between $begin and $end";

		$result = parent::find( $data, $field, $table, $equal, $like, $q, 'and', $other );
		
		$result['data'] = $this->format_datetime( $result['data'], 'time' );

		return $result;
	}
	
	/*
	 * 是否在线
	 * 参数：uid
	 */
	function is_online( $in )
	{
		$in = $this->value($in, 'data');
		$uid = $this->number($in, 'uid');
		
		$result = array( 'code' => 0 );
		
		$temp = biz('online')->exists( array('user'=>$uid) );
		$result['code'] = $temp ? 1 : 0;

		return $result;
	}
}

$action = new action();
$action->run();
?>