<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'pre_user';
	}

	function search($in)
	{
		$this->check_privilege('0,118');

		$user = biz('user');

		$field = '*';
		$table = $this->table();
		$equal = array('id', 'sex', 'status');
		$like = array();
		$q = array( 'name','name', 'mood', 'province', 'city' );

		if ($this->value($in, 'sortField') == '') {
			$in['sortField'] = 'id';
			$in['sortOrder'] = 'asc';
		}

		if( isset( $in[ 'q' ] ) )
			$in[ 'q' ] = addslashes( $in[ 'q' ] );

		$result = parent::find( $in, $field, $table, $equal, $like, $q );

		$status = biz('pre_user')->status_dict();
		$sex = $user->sex_dict();
		foreach( $result[ 'data' ] as $k => $v )
		{
			$v[ 'sex' ] = $sex[ $v[ 'sex' ] ];

			if( isset( $status[ $v[ 'status' ] ] ) )
			{
				$v[ 'status' ] = $status[ $v[ 'status' ] ];
			}

			$result[ 'data' ][ $k ] = $v;
		}

		return $result;
	}

	function row($in)
	{
		unset($in['register'], $in['login'], $in['login_times'], $in['register_imei'], $in['login_imei']);

		if ($this->value($in, 'rename') != '') {
			$in['name'] = $in['rename'];
		}

		if ($this->value($in, 'birthday') != '') {
			$in['birthday'] = strtotime($in['birthday']);
		}
		
		return $in;
	}

	function save($data)
	{
		$rows = $this->php_json_decode($data['data']);

		$file = 'biz.' . $this->table();
		$obj = load($file);

		$field = array_flip($obj->field());
		$id = $field['id'];

		foreach ($rows as $row) {
			$state = $this->value($row, '_state', '');
			if ($state == "added" || $id == "") { //新增：id为空，或_state为added
				
				if ( isset( $row['pass'] ) && $row['pass'] != '' )
				{
					$row['pass'] = $obj->code( $row['pass'] );
				}
				
				if( $obj->get_id_from_name($row['name']) > 0 )
					return array('code' => -1, 'message' => '用户名已经存在');
				else
				{
					$uid = $obj->add($this->row($row));
					$this->db->add( 'user_ex', array( 'id' => $uid ) );
				}
			} else if ($state == "removed" || $state == "deleted") {
				$obj->del($row[$id]);
				biz('user_ex')->del($row[$id]);
			} else if ($state == "modified" || $state == "") {  //更新：_state为空或modified
				
				if( $row['rename'] != '' && $obj->get_id_from_name($row['rename']) > 0 )
					return array('code' => -1, 'message' => '用户名已经存在');
				
				if ( isset( $row['newpass'] ) && $row['newpass'] != '' ){
					$user = load('biz.user');
					$row['pass'] = $user->code( $row['newpass'] );
				}

				$ex = array( 'id'=>$row['id'] );
				foreach( array( 'is_real', 'drunk' ) as $v )
				{
					$ex[ $v ] = $row[ $v ];
					unset( $row[ $v ] );
				}

				biz('user_ex')->set( $ex );
				
				$obj->set($this->row($row));
			}
		}
		return array('code' => 1, 'message' => '成功操作');
	}

}

$action = new action();
$action->run();
?>