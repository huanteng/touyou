<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'administrator';
	}

	/*
	 * 返回全部数据，供菜单下拉框等使用
	 */
	function drop()
	{
		$this->check_privilege('0,5');//5 内容管理 权限

		$field = 'id, name';
		$table = $this->table();
		$equal = array();
		$like = array();
		$q = array();
		if (!isset($data['sortField'])) {
			$data['sortField'] = 'id';
			$data['sortOrder'] = '';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		return $result['data'];
	}

	function search( $data )
	{
		$this->check_privilege( '0,3' );//3 管理权限
		
		$field = 'id, name, status, last';
		$table = $this->table();
		$equal = array(  );
		$like = array('name');
		$q = array('name');
		if( $this->value( $data, 'sortField' ) == '' )
		{
			$data['sortField'] = 'name';
			$data['sortOrder'] = '';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		$result['data'] = $this->format_datetime( $result['data'], 'last' );
		
		return $result;
	}
		
	function sel_administrator( $data )
	{
		
		$field = '*';
		$table = $this->table();
		$equal = array();
		$like = array( 'name' );
		$q = array( 'name' );
		if( !isset( $data['sortField'] ) )
		{
			$data['sortField'] = 'name';
			$data['sortOrder'] = '';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->format_datetime( $result['data'], 'last' );
		
		return $result;
	}

	function privilege( $in )
	{
		$sql = "select id, name, parent_id from admin_function order by `order`";
		$data = $this->db->select( $sql );

		$admin_id = $in[ 'id' ];
		foreach( $data as $k => $v )
		{
			if( $v[ 'parent_id' ] != 0 && $this->db->exists( 'administrator_privilege', array( 'administrator' => $admin_id,
				'privilege' => $v[ 'id' ] ) ) )
			{
				$v[ 'checked' ] = TRUE;
			}
			
			$data[ $k ] = $v;
		}

		return $data;
	}

	// 更新权限
	function save_privilege( $data )
	{
		$this->check_privilege( '0,3' );//3 管理权限

		$data = $data['data'];
		$id = $data[ 'id' ];
		$value = $data[ 'value' ];

		$arr = load( 'arr' );
		$db = load( 'database' );

		$old = $db->get( '*', 'administrator_privilege', array( 'administrator' => $id ) );
		$new = explode( ',', $value );

		foreach( $old as $row )
		{
			$func_id = $row[ 'privilege' ];

			$found = FALSE;
			foreach( $new as $k => $v )
			{
				if( $v == $func_id )
				{
					unset( $new[ $k ] );
					$found = TRUE;
				}
			}
			if( !$found )
			{
				$db->del( 'administrator_privilege', array( 'administrator' => $id, 'privilege' => $func_id ) );
			}
		}

		foreach ( $new as $func_id )
		{
			$db->add( 'administrator_privilege', array( 'administrator' => $id, 'privilege' => $func_id ) );
		}

		return array( 'code' => 1, 'message' => '成功操作' );
	}

}

$action = new action();
$action->run();
?>