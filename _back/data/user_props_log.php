<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'user_props_log';
	}

	function search( $in )
	{
		$this->check_privilege( '0,20' );
		
		$props = load('biz.props');

        $field = '*';
        
        $table = $props->value( $in, 'table', 'user_props_log' );

		if( isset( $in['user'] ) && !is_numeric( $in['user'] ) && $in['user'] != '' )
		{
			$user = load( 'biz.user' );
			$in['user'] = $user->get_id_from_name( $in['user'] );
		}
        
		$equal = array('type','props','user');
		$like = array();
		$q = array( 'memo' );
		if( $this->value( $in, 'sortField' ) == '' )
		{
			$in['sortField'] = 'id';
			$in['sortOrder'] = 'desc';
		}

		// <editor-fold defaultstate="collapsed" desc="当按是否npc查询时，重置查询条件">
		$npc = $props->value( $in, 'npc' );
		if( $npc != '' )
		{
			$field = 'upl.*';
			$table = $table . ' upl, user u';
			$other = 'upl.user = u.id and u.npc=' . $npc;
		}
		else
		{
			$other = '';
		}
		// </editor-fold>
		
		$result = parent::find( $in, $field, $table, $equal, $like, $q, 'and', $other );
        
        $props_type = $props->get_type_dict();
        
        $temp = $this->db->get('id,name','props');
        foreach($temp as $value){
            $props_data[$value['id']] = $value['name'];
        }
        
        foreach( $result['data'] as $key=>$value )
        {
            $result['data'][$key]['type_name'] = $props->value( $props_type, $value['type'] );
            $result['data'][$key]['props_name'] = $props->value( $props_data, $value['props'] );
        }
		
		$result['data'] = $this->fill_user( $result['data'], 'user', array('name'=>'name', 'npc'=>'npc') );
		$result['data'] = $this->format_datetime( $result['data'], 'time', 'm-d H:i:s' );
		
		return $result;
	}
	
	function type()
	{
		$this->check_privilege( '0,20' );
		
		$props = load('biz.props');
        foreach($props->get_type_dict() as $id => $value)
        {
            $out[] = array('id'=>$id,'text'=>$value);
        }
		return $out;
	}
}

$action = new action();
$action->run();
?>