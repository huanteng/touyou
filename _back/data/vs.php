<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'vs';
	}

	function search( $in )
	{
		$this->check_privilege('0,25');

		$user = load( 'biz.user' );

		$where = array();
		$other = '';
		if( $user->value( $in, 'must_win', '' ) == 1 )
		{
			$where[] = 'must_win != 0';
		}

		if( isset($in['user']) && is_numeric($in['user']) )
		{
			$uid = $in['user'];
			$where[] = '(user_1 = ' . $uid . ' or user_2 = ' . $uid . ')';
		}
		elseif( isset($in['user']) && $in['user'] != '' )
		{
			$uid = 0;
			$uid = $user->get_id_from_name( $in['user'] );
			$where[] = '(user_1 = ' . $uid . ' or user_2 = ' . $uid . ')';
		}
		
		if( $user->value( $in, 'type' ) == 1 || $user->value( $in, 'type' ) == 2 )
		{
			$where[] = 'type = '.$in['type'];
		}
		
		if( $this->value($in, 'bet') > 0 )
		{
			$where[] = 'bet >='.$in['bet'];
		}

		if( $this->value($in, 'start') != '' )
		{
			$where[] = '`start` >='.strtotime($in['start']). ' and `start` <'.(strtotime($in['start'])+900);
		}
		
		$other = join(' and ',$where);

		$field = '*';
		$table = $user->value( $in, 'table', 'vs' );
		$equal = array();
		$like = array();
		$q = array();
		if( $this->value( $in, 'sortField' ) == '' )
		{
			$in['sortField'] = 'start';
			$in['sortOrder'] = 'desc';
		}

		$result = parent::find( $in, $field, $table, $equal, $like, $q, 'and', $other );

		$result['data'] = $this->fill_user($result['data'], 'user_1', array('name' => 'name_1', 'npc' => 'npc_1'));
		$result['data'] = $this->fill_user($result['data'], 'user_2', array('name' => 'name_2', 'npc' => 'npc_2'));
		$result['data'] = $this->fill_props( $result['data'], 'user_1', array( 1 => 'gold_1') );
		$result['data'] = $this->fill_props( $result['data'], 'user_2', array( 1 => 'gold_2') );
		$result['data'] = $this->format_datetime($result['data'], 'start', 'm-d H:i:s');

		foreach ($result['data'] as $key => $value) {
			$result['data'][$key]['sai_1'] = $value['user_1_1'] . $value['user_1_2'] . $value['user_1_3'] . $value['user_1_4'] . $value['user_1_5'];
			$result['data'][$key]['sai_2'] = $value['user_2_1'] . $value['user_2_2'] . $value['user_2_3'] . $value['user_2_4'] . $value['user_2_5'];
		}

		return $result;
	}

	/* 设置必赢
	 * 参数：
	 *	id：vs id
	 */
	function must_win( $in )
	{
		$id = $in['data'];

		$vs = biz( 'vs' );

		$info = $vs->get_from_id( $id );

		if ( !isset( $info['user_1'] ) )
			return $vs->out( -1, '比赛不存在' );

		$user = biz( 'user' );

		if( $user->is_npc( $info[ 'user_1' ] ) )
		{
			$vs->set( array( 'id' => $info[ 'id' ], 'must_win' => 1 ) );
		}
		elseif( $user->is_npc( $info[ 'user_2' ] ) )
		{
			$vs->set( array( 'id' => $info[ 'id' ], 'must_win' => 2 ) );
		}

		return $vs->out( 'ok' );
	}


}

$action = new action();
$action->run();
?>