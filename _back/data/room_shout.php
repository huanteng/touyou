<?php
require 'backend.php';

class action extends backend
{
	function table()
	{
		return 'room_shout';
	}

	function search($data)
	{
		$this->check_privilege('0,113');

		$field = '*';
		$table = $this->table();
		$equal = array( 'match_id' );
		$like = array();
		$q = array();

		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'id';
			$data['sortOrder'] = '';
		}

		$result = parent::find($data, $field, $table, $equal, $like, $q);

		$four = array(1,2,3,4);
		$user = biz( 'user' );
		
		foreach( $result['data'] as $k => $v )
		{
			$v[ 'name' ] = $user->get_name_from_id( $v[ 'uid' ] );

			if( $v[ 'count' ] == 0 )
				$v[ 'way' ] = '开';
			else
				$v[ 'way' ] = $v[ 'count' ] . '个' . $v[ 'number' ] . ( $v['zhai']==1 ? '斋':'' );

			if( $v[ 'is_act' ] == 1 )
				$v[ 'way' ] .= '，（托管）';
			
			$result['data'][$k] = $v;
		}

		$result['data'] = $this->format_datetime( $result['data'], 'time', 'm-d H:i:s' );

		return $result;
	}

	

}

$action = new action();
$action->run();
?>