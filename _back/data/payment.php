<?php

require 'backend.php';

class action extends backend
{

	function table()
	{
		return 'payment';
	}

	function search($data)
	{
		$this->check_privilege('0,42');

		$start_time = strtotime($this->value($data, 'start', 0));
		$end_time = strtotime($this->value($data, 'end', 0));

		$other = '';
		if ($start_time != '' && $end_time != '')
			$other = "`time` between '$start_time' and '$end_time'";

		$user = load('biz.user');
		if (isset($data['user']) && !is_numeric($data['user']) && $data['user'] != '') {
			$data['user'] = $user->get_id_from_name($data['user']);
		} else {
			$data['user'] = $this->value($data, 'user', 0);
		}

		$payment = biz('payment');

		$field = '*';
		$table = $this->table();
		$equal = array('user', 'status', 'props', 'type');
		$like = array();
		$q = array('memo');
		if ($this->value($data, 'sortField') == '') {
			$data['sortField'] = 'time';
			$data['sortOrder'] = 'desc';
		}

		// <editor-fold defaultstate="collapsed" desc="当按是否npc查询时，重置查询条件">
		$npc = $this->value($data, 'npc');
		if ($npc != '') {
			$field = 'p.*';
			$table = $table . ' p, user u';
			$other = 'p.user = u.id and u.npc=' . $npc;
		} else {
			$other = '';
		}
		// </editor-fold>

		$result = parent::find($data, $field, $table, $equal, $like, $q, 'and', $other);

		$temp = $this->db->get('id,name', 'props');
		foreach ($temp as $value) {
			$props_array[$value['id']] = $value['name'];
		}

		$result['data'] = $this->fill_user($result['data'], 'user', array('name' => 'name', 'npc' => 'npc', 'channel' => 'channel', 'version' => 'version' ));
		$result['data'] = $this->fill_user($result['data'], 'target_uid', array('name' => 'target_name'));
		$result['data'] = $this->fill_field($result['data'], 'type', 'type_name', $payment->get_type_dict());
		$result['data'] = $this->fill_field($result['data'], 'props', 'props_name', $props_array);
		$result['data'] = $this->format_datetime($result['data'], 'time');

		return $result;
	}

	function report($data)
	{
		$this->check_privilege('0,60');

		$start_time = isset($data['start']) ? strtotime($data['start']) : time() - 30 * 86400;
		$end_time = isset($data['end']) ? strtotime($data['end']) : time();

		$temp = $this->db->select("SELECT COUNT(*) AS count, sum(binary(money)) as total, `user` FROM payment WHERE time between '" . $start_time . "' and '" . $end_time . "' GROUP BY `user` ORDER BY total desc");

		$payment = load('biz.payment');
		$user_props = load('biz.user_props');
		$result['data'] = array();
		if (count($temp) > 0) {
			foreach ($temp as $key => $value) {
				$result['data'][$key]['user'] = $value['user'];
				$result['data'][$key]['count'] = $value['count'];
				$result['data'][$key]['total'] = $value['total'];

				$money = $user_props->get1('quantity', array('user' => $value['user'], 'props' => 1));
				$result['data'][$key]['money_quantity'] = $this->number($money, 'quantity', 0);

				$bullion = $user_props->get1('quantity', array('user' => $value['user'], 'props' => 2));
				$result['data'][$key]['bullion_quantity'] = $this->number($bullion, 'quantity', 0);

				$result['data'][$key]['success'] = 0;
				$result['data'][$key]['failure'] = 0;
				$temp = $payment->get1('count(*) as count', array(" status = 1 and user = " . $value['user'] . " and time  between '" . $start_time . "' and '" . $end_time . "' "));
				$result['data'][$key]['success'] = $this->number($temp, 'count');
				$result['data'][$key]['failure'] = $value['count'] - $result['data'][$key]['success'];

				$info = biz('user')->get_from_id($value['user']);
				$result['data'][$key]['name'] = $this->value($info, 'name');
				$result['data'][$key]['register'] = $this->value($info, 'register');

				$temp = $payment->get1('time', array(" status = 1 and user = " . $value['user']), 'time desc');
				$result['data'][$key]['last_pay'] = $this->value($temp, 'time');
			}
			$result['data'] = $this->format_datetime($result['data'], 'register');
			$result['data'] = $this->format_datetime($result['data'], 'last_pay');
		}
		return $result;
	}

	/*
	 * 充值报表
	 * 以充值金额和第几周统计，以月为一个查询
	 * _back/payment_report2.php
	 * 参数：日期
	 */

	function report2($in)
	{
		$this->check_privilege('0,60');

		if ($this->value($in, 'date') != '') {
			$time = strtotime($in['date']);
		} else {
			$time = time();
		}

		//充值金额种类
		$value_type = array('two' => 2, 'ten' => 10, 'thirty' => 30, 'fifty' => 50, 'onehundred' => 100);

		$out[0] = $out[1] = $out[2] = $out[3] = $out[4] = $out[5] = array();
		foreach ($out as $key => $o) {
			$out[$key] = array('two' => 0, 'ten' => 0, 'thirty' => 0, 'fifty' => 0, 'onehundred' => 0, 'week' => 0, 'total' => 0, 'day' => 0);
		}

		for ($day = 1; $day <= date('t', $time); $day++) {
			$day_str = date('Y-m-', $time) . $day;
			$start_time = strtotime($day_str);
			$end_time = $start_time + 86399;
			//echo date('m-d',$start_time);
			//算出第几周
			$month = date('m', $time); // 获取本月
			$year = date('Y', $time); // 获取本年
			$firstday = date('w', mktime(0, 0, 0, $month, 1, $year)); //本月1号星期数
			$firstweek = 7 - $firstday; // 第1周天数 
			$week = ceil(($day - $firstweek) / 7) + 1;
			$week -= 1; //由零开始算
			//echo $week.' ';
			//算出第几周end

			$out[$week]['day'] += 1;
			$out[$week]['week'] = $week + 1;

			$value_start = 0;
			foreach ($value_type as $field => $value) {
				$sql = 'select sum(binary(money)) as total from payment where `time` between ' . $start_time . ' and ' . $end_time . ' and status = 1 and money > ' . $value_start . ' and money <= ' . $value;
				//	echo $sql;
				//	echo '<hr/>';
				$temp = $this->db->unique($sql);

				//	print_r($temp);

				$total = isset($temp['total']) ? $temp['total'] : 0;

				$out[$week]['total'] += $total;
				$out[$week][$field] += $total;

				$value_start = $value;
			}
		}
		ksort($out);
		$result['total'] = count($out);
		$result['data'] = $out;

		//print_r($result);

		return $result;
	}

	/*
	 * 月充值报表
	 * 参数：年份
	 */

	function report_month($in)
	{
		$this->check_privilege('0,60');

		$year = $this->value($in, 'year', date('Y'));

		$data = array();
		for ($mon = 1; $mon <= 12; $mon++) {
			$start_time = strtotime(date($year . '-' . $mon . '-01'));
			if ($start_time > time())
				break;
			$end_time = strtotime(date($year . '-' . $mon . '-t 23:59:59', $start_time));

			$data['month'] = date($year . '-' . $mon, $start_time);
			//注册人数
			$temp = $this->db->unique('select count(1) as total from user where npc = 0 and register between ' . $start_time . ' and ' . $end_time);
			$data['register_count'] = $this->number($temp, 'total', 0);

			//注册人数(以手机计算)
			$temp = $this->db->unique('select count(DISTINCT(register_imei)) as total from user where npc = 0 and register between ' . $start_time . ' and ' . $end_time);
			$data['mobile_register_count'] = $this->number($temp, 'total', 0);

			//第一次充值的人
			$data['pay_first_count'] = 0;
			$data['pay_first_sum'] = 0;
			$result_temp = $this->db->select('select user,count(*) as total from payment where status = 1 and time between ' . $start_time . ' and ' . $end_time . ' group by user having total = 1');
			foreach ($result_temp as $value) {
				$temp = $this->db->unique('select count(1) as total,sum(binary(money)) as sum from payment where status = 1 and user = ' . $value['user']);
				if ($this->number($temp, 'total', 0) == 1) {
					$data['pay_first_count']++;
					$data['pay_first_sum'] += $temp['sum']; //第一次充值的金额
				}
			}

			//充值人数
			$temp = $this->db->unique('select count(DISTINCT(user)) as total from payment where status = 1 and time between ' . $start_time . ' and ' . $end_time);
			$data['pay_count'] = $this->number($temp, 'total', 0);

			//充值总数
			$temp = $this->db->unique('select sum(binary(money)) as total from payment where status = 1 and time between ' . $start_time . ' and ' . $end_time);
			$data['pay_sum'] = round($this->number($temp, 'total', 0), 2);

			//初充比例
			if ($this->number($data, 'pay_count', 0) != 0)
				$data['pay_first_percent'] = round($data['pay_first_count'] / $data['pay_count'] * 100, 2);
			else
				$data['pay_first_percent'] = 0;

			//转化参数
			if ($this->number($data, 'register_count', 0) != 0)
				$data['pay_register_percent'] = round($data['pay_first_count'] / $data['register_count'] * 100, 2);
			else
				$data['pay_register_percent'] = 0;

			$result['data'][] = $data;
		}

		$result['total'] = count($data);

		return $result;
	}

	/*
	 * 充值信息
	 */

	function info()
	{
		$current_start_time = strtotime(date('Y-m-01'));
		$current_end_time = strtotime(date('Y-m-d')) + 86399;

		$current_day = date('d');
		$prev_month = date('m') - 1;
		$prev_year = date('Y');

		if ($prev_month == 0) {
			$prev_month = 12;
			$prev_year = $prev_year - 1;
		}

		if ($current_day > 28) {
			$prev_standard_day = date('t', strtotime($prev_year . '-' . $prev_month . '-01'));
			if ($current_day > $prev_standard_day)
				$prev_day = $prev_standard_day;
			else
				$prev_day = $current_day;
		}
		else {
			$prev_day = $current_day;
		}

		$prev_start_time = strtotime($prev_year . '-' . $prev_month . '-01');
		$prev_end_time = strtotime($prev_year . '-' . $prev_month . '-' . $prev_day) + 86399;

		$payment = biz('payment');
		$current = $payment->get1('sum(money) as money', array("status = 1 and time between " . $current_start_time . " and " . $current_end_time));
		$prev = $payment->get1('sum(money) as money', array("status = 1 and time between " . $prev_start_time . " and " . $prev_end_time));

		$current_real = $this->db->get1( 'sum(quantity) as money', 'user_props p, user u', array('u.id = p.user and u.npc = 0 and (p.expire=0 or p.expire<unix_timestamp())') );

		$percent = $prev['money'] == 0 ? '-' : ( ( round( ( $current['money'] - $prev['money'] ) * 100 / $prev['money'] ) ) . '%' );
		
		return array( round( $current['money'], 2 ), $percent, $current_real['money'], round( $current['money'] * date( 't' ) / date( 'd' ), 2 ) );
	}

}

$action = new action();
$action->run();
?>