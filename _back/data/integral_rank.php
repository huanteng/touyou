<?php
require 'backend.php';

class action extends backend
{
	function table() {
		return 'integral_rank';
	}

	function search( $data )
	{		

		$field = '*';
		$table = $this->table();
		$equal = array( '' );
		$like = array();
		$q = array( '' );

		if( $this->value( $data, 'sortField' ) == '' )	
		{
			$data['sortField'] = 'time';
			$data['sortOrder'] = 'desc';
		}
		
		$result = parent::find( $data, $field, $table, $equal, $like, $q );
		
		$result['data'] = $this->fill_name_from_uid( $result['data'], 'user', 'user', '' );	
		$result['data'] = $this->format_datetime( $result['data'], 'time' );
			
		return $result;
	}

}

$action = new action();
$action->run();
?>