<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(66); ?>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=lists');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
		if (field == "user") {
	        e.cellHtml = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail' + record.user+'", text:"'+record.name+'", url: "user_detail.php?uid='+record.user+'"})\'>'+record.name+'</a>';
	    }
		
	    if (field == "npc") {
	        e.cellHtml = (value == '1') ? '√' : '';
	    }

		if (field == "action") {
	        e.cellHtml = '<a href="javascript:void(0)">干预</a>';
	    }

	});

}

function search() {
    var user = mini.get("uid").getValue();
	var npc = mini.get("#npc").getValue();
    
    grid.load({ user: user, npc: npc });
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						<?php autocomplete_name() ;?>
						NPC：<input id="npc" class="mini-combobox" style="width:150px;" allowInput="true" showNullItem="true" nullItemText="请选择..." data="[{ id: 1, text: 'NPC' }, { id: 0, text: '非NPC'}]"/>       
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div width="10" property="columns">
            <div field="id" width="20" headerAlign="center" allowSort="true">id
            </div>
    		<div field="user" displayField="name" width="30" headerAlign="center" allowSort="true">用户
            </div>
            <div field="npc" width="10" headerAlign="center" allowSort="true">NPC
            </div>
            <div field="quantity" width="30" headerAlign="center" allowSort="true">数量
            </div>
			<div field="action" width="20" headerAlign="center" allowSort="false">操作
            </div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'user_props';}
</script>