<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(53); ?>
<?php $module = 'audit'; ?>
<style>
	.container {
		/*background: #FFF;*/
		padding: 5px;
		margin-bottom: 20px;
		border-radius: 5px;
		clear: both;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}

	.centered { margin: 0 auto; }

	.box {
		margin: 5px;
		padding: 5px;
		background: #D8D5D2;
		font-size: 11px;
		line-height: 1.4em;
		float: left;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
	}

	.box h2 {
		font-size: 14px;
		font-weight: 200;
	}

	.box img,
	#tumblelog img {
		display: block;
		width: 100%;
	}

	.rtl .box {
		float: right;
		text-align: right; 
		direction: rtl;
	}

	.col1 { width: 80px; }
	.col2 { width: 180px; }
	.col3 { width: 280px; }
	.col4 { width: 380px; }
	.col5 { width: 480px; }
	.col_s { width: 120px; }

	.col1 img { max-width: 80px; border: 0px; }
	.col2 img { max-width: 180px; border: 0px; }
	.col3 img { max-width: 280px; border: 0px; }
	.col4 img { max-width: 380px; border: 0px; }
	.col5 img { max-width: 480px; border: 0px; }
	.col_s img { max-width: 120px; border: 0px; }
</style>

<div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
	 url="data/<?php echo $module; ?>.php?method=search" idField="user"
	 allowResize="true" pageSize="10" allowAlternating="true"
	 allowCellSelect="true" multiSelect="true"       
	 showEmptyText="true"    
	 >
	<div property="columns">      
		<div field="npc" width="50" headerAlign="center" allowSort="false">用户</div> 
		<div field="album_list" width="250" headerAlign="center" allowSort="false">照片</div>	
	</div>
</div>

<div class="description">
	<h3>说明</h3>
</div>

<?php require 'bottom.php'; ?>
<script src="js/jquery.masonry.min.js"></script>
<script type="text/javascript">
	//////覆盖
	function module() { return '<?php echo $module; ?>';}
	//////覆盖结束

	$('#toolbar .mini-button:lt(3),#toolbar .separator').hide();
	var npc_html = 'NPC：<input id="npc" class="mini-combobox" style="width:60px;" allowInput="true" showNullItem="true" nullItemText="请选择..." data="[{ id: 1, text: \'NPC\' }, { id: 0, text: \'非NPC\'}]"/>';
	var sex_html = '性别：<input id="sex" class="mini-combobox" style="width:60px;" allowInput="true" showNullItem="true" nullItemText="请选择..." data="[{ id: 0, text: \'男\' }, { id: 1, text: \'女\'}]"/>';
	$('#search').prepend(npc_html+'&nbsp;'+sex_html);

	function init()
	{
		grid = mini.get("datagrid1");
		grid.setUrl('data/' + module() +'.php?method=user_is_real_list');
		var data={};
		<?php
		$s = '';
		foreach( $_GET as $k => $v )
		{
			$s .= "data.{$k} = '{$v}';\n";
		}
		echo $s;
		?>
		grid.load(data);

		grid.on( "drawcell", onDrawcell );
	}

	function onDrawcell (e) {
		var record = e.record,
		column = e.column,
		field = e.field,
		value = e.value;

		var html = "";
		switch( field )
		{
			case "npc":
				html = '<a href="'+record.logo+'" target="_blank"><img src="'+record.logo+'" style="width:120px;height:120px;border:0px;"/></a>';
				html += '<br/>'+((record.npc==1)?'<span style="color:red;">N</span>&nbsp;':'')+'<a href="javascript:tab(\'user_detail\',\''+record.name+'\',\'user_detail.php?uid='+record.id+'\');">'+record.name+'</a>&nbsp;'+((record.sex !='' && record.sex=="0")?'男':'女');
				break;
				
			case "album_list":
                var img_html='';
				$(record.album_list).each(function(i,n){
                    
                    img_html += '<div class="box photo col_s" id="img_'+n.id+'">'
                    img_html += '<a href="'+n.path+'" target="_blank">';
                    img_html += '<img src="'+n.path+'.s.jpg"/>';
                    img_html += '</a>';
					
					if(n.type==2) img_html += '视频认证照片';
					
					if(n.type==0) img_html += '<a href="javascript:move_pic('+n.id+',1)" class="mini-button">移到个人照片</a>';
					else if(n.type==1) img_html += '<a href="javascript:move_pic('+n.id+',0)" class="mini-button">移到其它照片</a>';
						

					img_html += '</div>';
				});
				  html += '<div class="container" id="con_'+record.id+'">'+img_html+'</div>';
				break;
		}

		if( html != "" ) e.cellHtml = html;

	}
	
	function search() 
	{
		var	q = mini.get("q").getValue();
		var npc = mini.get("npc").getValue();
		var sex = mini.get("sex").getValue();

		grid.load({ q:q, npc: npc, sex:sex });
	}
	
	function move_pic(id,type)
	{
		if(confirm("确定移动这张照片到其它照片相册吗？"))
		{
			$.ajax({
				url: "data/album.php?method=set_type&type="+type+"&id="+id,
				dataType: "json",
				success: function (data) {			
					reload();
				}
			});	
		}
	}
	
	function rand()
	{
		grid.load({ rand: 'rand' });
	}
</script>
