<?php
require 'top.php';

check_privilege(0);
check_privilege(83);
?>
  
    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="24" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
			<div field="time" width="30" headerAlign="center" allowSort="true">时间</div>
			<div field="challenge" width="30" headerAlign="center" allowSort="true">挑战赛投入</div>
			<div field="challenge2" width="30" headerAlign="center" allowSort="true">挑战赛消耗</div>
			<div field="integral" width="30" headerAlign="center" allowSort="true">骰魔大赛投入</div>
			<div field="integral2" width="30" headerAlign="center" allowSort="true">骰魔大赛消耗</div>
			<div field="lucky" width="30" headerAlign="center" allowSort="true">抽奖投入</div>
			<div field="lucky2" width="30" headerAlign="center" allowSort="true">抽奖消耗</div>
			<div field="task" width="30" headerAlign="center" allowSort="true">任务</div>
			<div field="props" width="30" headerAlign="center" allowSort="true">道具投入</div>
			<div field="props2" width="30" headerAlign="center" allowSort="true">道具消耗</div>
			<div field="pay" width="30" headerAlign="center" allowSort="true">充值</div>
			<div field="other" width="30" headerAlign="center" allowSort="true">其它投入</div>
			<div field="other2" width="30" headerAlign="center" allowSort="true">其它消耗</div>
			<div field="total" width="30" headerAlign="center" allowSort="false">总投入</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
		<ul><li>时间为截止时间:上一个时间点到当前时间点区间</li></ul>
    </div>
    	
    
	<div id="container" style="width: 98%; height: 600px"></div>	

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'gold_report';}

$('#toolbar .mini-button:lt(3),#toolbar .separator,#q').hide();
$('#search').prepend('日期：<input id="date" class="mini-datepicker" value="" format="yyyy-MM-dd" style="width:100px;"/>');

function search() {
	var date = mini.get("date").getFormValue();
    grid.load({ date: date });
}
var series = {};
var challenge_data = [];
var times = new Array();
times = ['a','b','12:22'];
function onDrawcell(e) {
    var r = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	var times = new Array();
	var html = "";
	var series;
	switch( field )
	{
		case 'time':
			l = times.length;
			//times.push('e.record.time');
			times[l+1] = e.record.time;
			break;
//		case 'challenge':
//			challenge_data.push(record.challenge);
//			break;
		
		case 'total':
			html = parseInt(r.challenge) + parseInt(r.integral) + parseInt(r.lucky) + parseInt(r.task)
				+ parseInt(r.props) + parseInt(r.pay) + parseInt(r.other) - parseInt(r.challenge2)
				- parseInt(r.integral2) - parseInt(r.props2) - parseInt(r.other2);
			break;
	}
	if( html != "" ) e.cellHtml = html;
}
</script>

<script src="js/highcharts/highcharts.js" type="text/javascript"></script>
<script>
	var chart1; // globally available
	
	alert('xxx'+times);
	
$(document).ready(function() {
	
	
	
	alert(times);
	
	
	
	
	
	
	
      chart1 = new Highcharts.Chart({
         chart: {
            renderTo: 'container',
            type: 'spline'
         },
         title: {
            text: '金币消耗报表'
         },
         xAxis: {
            //categories: ['挑战赛', '骰魔大赛', '抽奖', '任务', '道具', '充值', '其它', '总投入']
            categories: times
         },
         yAxis: {
            title: {
               text: '金币数量'
            }
         },
         series: [{
            name: '挑战赛消耗',
            data: [1, 0, 4, 0, 4, 0, 4, 9,5, 7, 3, 0, 4, 0, 4, 2,5, 7, 3, 0, 4, 0, 4, 20]
         }, {
            name: '挑战赛投入',
            data: [5, 7, 3, 0, 4, 0, 4, 2,5, 7, 3, 0, 4, 0, 4, 2,5, 7, 3, 3, 4, 0, 4, 21]
         }]
         
       //  series: series
      });
   });
</script>		