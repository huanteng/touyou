<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(20);
?>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	var data={};
	<?php
	$s = '';
	foreach( $_GET as $k => $v )
	{
		$s .= "data.{$k} = '{$v}';\n";
	}
	echo $s;
	?>
	grid.load(data);
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
		if (field == "name") {
	        e.cellHtml = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail' + record.user+'", text:"'+record.name+'", url: "user_detail.php?uid='+record.user+'"})\'>'+record.name+'</a>';
	    }
		
	    if (field == "npc") {
	        e.cellHtml = (value == '1') ? '√' : '';
	    }
	});
}

function search() {
    var user = mini.get("uid").getValue();
	var npc = mini.get("npc").getValue();
    var type = mini.get("type").getValue();
    var props = mini.get("props").getValue();
    var q = mini.get("q").getValue();
    var table = mini.get("table").getValue();

    grid.load({ user:user, npc:npc, type:type, props:props, q: q, table: table });
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
                        <input id="table" name="table" class="mini-radiobuttonlist" data="[{id: 'user_props_log', text: '当前'}, {id: 'user_props_log_history', text: '历史'}]" value="user_props_log" /> |
						<?php autocomplete_name() ;?>
						NPC：<input id="npc" class="mini-combobox" allowInput="true" showNullItem="true" nullItemText="请选择..." data="[{ id: 1, text: 'NPC' }, { id: 0, text: '非NPC'}]"/>
    					类型：<input id="type" class="mini-combobox" style="width:150px;" textField="text" valueField="id" 
    url="data/user_props_log.php?method=type" value="" allowInput="true" showNullItem="true" nullItemText="请选择..."/>
    					道具：<input id="props" class="mini-combobox" style="width:150px;" textField="name" valueField="id"
    url="data/props.php?method=drop&havegold=true" value="" allowInput="true" showNullItem="true" nullItemText="请选择..."/>
                        备注：<input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:150px;" onenter="onKeyEnter"/>   
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div field="id" width="30" headerAlign="center" allowSort="true">id
            </div>
            <div field="time" width="30" headerAlign="center" allowSort="true">时间
            </div>
            <div field="name" width="30" headerAlign="center" allowSort="true">用户</div>
            <div field="npc" width="10" headerAlign="center" allowSort="false">NPC</div>
        	<div field="type" displayField="type_name" width="30" headerAlign="center" allowSort="true">类型</div>
            <div field="props" displayField="props_name" width="30" headerAlign="center" allowSort="true">道具</div>
        	<div field="`change`" displayField="change" width="30" headerAlign="center" allowSort="true">变化</div>
            <div field="balance" width="30" headerAlign="center" allowSort="true">余额
            </div>
            <div field="memo" headerAlign="center" allowSort="false">备注
            </div>	
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
		<ul><li>当前：10天内的记录；历史：10天前的记录</li></ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
	user_context_column = { user: 'name' };
function module() { return 'user_props_log';}
</script>