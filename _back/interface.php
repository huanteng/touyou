<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(16); ?>
<script language="javascript">
function print_r(theObj) {  
    var retStr = '';  
    if (typeof theObj == 'object') {  
        retStr += '<div">';  
        for (var p in theObj) {  
            if (typeof theObj[p] == 'object') {  
                retStr += '<div><b>['+p+'] => ' + typeof(theObj) + '</b></div>';  
                retStr += '<div style="padding-left:25px;">' + print_r(theObj[p]) + '</div>';  
            } else {  
                retStr += '<div>['+p+'] => <b>' + theObj[p] + '</b></div>';  
            }  
        }  
        retStr += '</div>';  
    }  
    return retStr;  
}  
$(function(){
	$("#post").click(function(){
		$("#content").html("");
		
		var url;
		url = "../interface/" +  $("#file").val() + "?type=" + $("#type").val() + "&" + $("#para").val();
		if($("#sid").val()!="") url += "&sid=" + $("#sid").val();
		var view = $("input[name='view']:checked").val();
		
		$("#url").text(url);
		
		$.ajax({ cache: false 
		, type: "get"
		, url: url
		, dataType: view
		, data: ""
		, success: function(data) {
			if(view == 'json')
				$("#content").html(print_r(data));
			else
				$("#content").html(data);
			}
		});
	
	});
});

function sid(uid)
{
   post( "user", "sid", uid, function (text) {
			$("#sid").val(text);
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}
</script>
</head>

<body>
<form id="form1" name="form1" method="post" action="">

<br>2、请输入接口文件：
  <label>
  <input name="file" type="text" id="file" value="user.php" />
  </label>
<br>3、请输入方法名：
  <input name="type" type="text" id="type" value="login" />
<br>4、请输入sid：
  <input name="sid" type="text" id="sid" value="7984f32a96fcc29c5ecb13fe08726f1ca9eabbd3" /> 
  快速：<?php foreach( array('随机' => '0', 'dda' => '237', 'decade' => '226') as $k => $v ) {?><a href="javascript:sid('<?php echo $v ?>')"><?php echo $k ?></a> <?php } ?>
<br>5、请输入其它参数：
  <input name="para" type="text" id="para" size="80" value=""/>
<br>6、显示：
  <input type="radio" name="view" value="json" />
  JSON
  <input type="radio" name="view" value="text" />
  TEXT

<br>
  <input name="post" type="button" id="post" value="提交" /><div id="url"></div>
</form>
  <div id="content" style="width:auto;height:600"></div>
</body>

</html>
