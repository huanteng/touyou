<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(18); ?>

<div style="width:98%;">
	<div id="form1">
		<table class="form-table" border="0" cellpadding="1" cellspacing="1">
			<tr>
				<td class="label">名字: </td>
				<td><input name="name" class="mini-textbox" style="width:200px" required="true" vtype="char" />（请输入名字）</td>
			</tr>
			<tr>
				<td class="label">密码: </td>
				<td><input name="pass" class="mini-textbox" style="width:200px" required="true" value="touyou.mobi" vtype="char" />（默认：touyou.mobi）</td>
			</tr>
			<tr>
				<td class="label">生日：</td>
				<td ><input name="birthday" class="mini-datepicker" style="width:200px" showTime="false"
							format="yyyy-MM-dd" /></td>
			</tr>
			<tr>
				<td class="label">城市：</td>
				<td><input name="city" class="mini-textbox" style="width:200px" value="神秘岛" vtype="char" /></td>
			</tr>
			<tr>
				<td class="label">NPC：</td>
				<td><input name="npc" class="mini-combobox" style="width:200px;" allowInput="false" showNullItem="false" nullItemText="请选择..." data="[{ id: 1, text: 'NPC' }, { id: 0, text: '非NPC'}]" value="0" vtype="int"/></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><a class="mini-button" onclick="submitForm()" style="width: 150px;">保存</a></td>
			</tr>
			
		</table>
	</div>
</div>

<div class="description">
	<h3>说明</h3>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
mini.parse();

var form = new mini.Form("form1");

function submitForm() {
	form.validate();
	if (form.isValid() == false) return;

	//提交数据
	var data = [];
	data[0] = form.getData();
	data[0]['_state'] = 'added';
	var value = mini.encode(data);  
	post( 'user', "save", value, function (text) {
			alert(text.message);
			location.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}
</script>