<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(32);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	var data={};
	<?php if( isset($_GET['user']) ){ ?>
	data.user = '<?php echo $_GET['user'];?>';
	<?php }?>
	grid.load(data);
	
	grid.on("drawcell", function (e) {
	    var row = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;

		var html = "";
		switch( field )
		{
			case 'name':
				html = String.format( '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail{0}", ' +
					'text:"{1}", url: "user_detail.php?uid={0}"})\'>{1}</a>', row.user, row.name );
				break;
			case "npc":
				html = row.npc ? '√' : '';
				break;

		}
		if( html != "" ) e.cellHtml = html;
	});
}

function search() {
	var user = mini.get('uid').getValue();
    var type = mini.get('type').getValue(); 
    var time = mini.get('time').getFormValue();
	var q = mini.get('q').getFormValue();
    if(time == null) time = '';
    grid.load({ type:type, user:user, time:time, q:q });
}
function onKeyEnter(e) {
    search();
}

function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
    					<a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						<?php autocomplete_name() ;?>
    					类型：<input id="type" class="mini-combobox" style="width:150px;" textField="text" valueField="id" 
    url="data/moving.php?method=type" value="" required="true" allowInput="true" showNullItem="true" nullItemText="请选择..."/> 
    					时间：<input id="time" name="time" class="mini-datepicker" value="" format="yyyy-MM-dd" /> 
                        内容：<input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:150px;" onenter="onKeyEnter"/>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
        	<div type="checkcolumn" width="10"></div>
            <div field="id" width="10" headerAlign="center" allowSort="true">id</div>
            <div field="name" width="30" headerAlign="center" allowSort="true">用户</div>
			<div field="npc" width="10" headerAlign="center" allowSort="false">NPC</div>
        	<div field="type" displayField="type_name" width="20" headerAlign="center" allowSort="true">类型</div>
            <div field="data" displayField="content" headerAlign="center" allowSort="true">内容</div>
        	<div field="time" width="40" headerAlign="center" allowSort="true">时间</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<script type="text/javascript">
	user_context_column = { user: 'name' };
	function module() { return 'moving';}
</script>
<?php require 'bottom.php'; ?>
