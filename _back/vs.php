<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(25);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	
	var data={};
	<?php
	$s = '';
	foreach( $_GET as $k => $v )
	{
		$s .= "data.{$k} = '{$v}';\n";
	}
	echo $s;
	?>
	grid.load(data);

	grid.on("drawcell", function (e) {
	    var record = e.record,
	        field = e.field,
	        value = e.value;

		switch( field )
		{
			case "user_1":
				e.cellHtml = record.name_1 + ((record.user_1==record.winner) ? '(<b style="color:red;">赢</b>)' : '');
				break;
        
			case "user_2":
				e.cellHtml = record.name_2 + ((record.user_2==record.winner) ? '(<b style="color:red;">赢</b>)' : '');
				break;
		
			case "type":
				e.cellHtml = (value == '1') ? '骰魔大赛' : '挑战赛';
				break;
	    
			case "npc_1":
				e.cellHtml = (value == '1') ? '√' : '';
				break;
	    
			case "npc_2":
				e.cellHtml = (value == '1') ? '√' : '';
				break;
	    
			case "must_win":
				if( value == 1 ) e.cellHtml = record.name_1;
				else if( value == 2 ) e.cellHtml = record.name_2;
				else e.cellHtml = '';
				break;
	    
			case "action":
				e.cellHtml = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"'+record.id+'", text:"'+record.name_1+' vs '+record.name_2+'", url: "../backend/get_pair.php?id='+record.id+'"})\'>查看</a> ';

				var uid = record._uid;
				if( record.must_win == 0 && (record.npc_1 == 1 || record.npc_2 == 1) ) e.cellHtml += '<a href="#" onclick="must_win(' + uid + ')">设npc赢</a>';
				break;
		}

	});
}

function must_win( _uid )
{
	var row = grid.getRowByUID(_uid);

	var data = row.id;

	post( 'vs', "must_win", data, function (text) {
		if( text.code < 0 )
		{
			alert(text.memo);
			return;
		}
		
		var value = 0;
		if( row.npc_1 == 1 ) value = 1;
		if( row.npc_2 == 1 ) value = 2;

		grid.updateRow(row, {
                must_win: value
            });

		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function search() {
    var user = mini.get("uid").getValue();
    var type = mini.get("type").getValue();
    var must_win = mini.get("must_win").getValue();
    var table = mini.get("table").getValue();
    
    grid.load({ user:user, type: type, must_win: must_win, table: table });
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:60%;" id="toolbar">
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
                        <input id="table" name="table" class="mini-radiobuttonlist" data="[{id: 'vs', text: '当前'}, {id: 'vs_history', text: '历史'}]" value="vs"/> | 
    					必赢：<input id="must_win" class="mini-combobox" style="width:150px;" textField="text" valueField="id"
    data="[{id: '0', text: '未设置'}, {id: '1', text: '必赢'}]" value="0" allowInput="false" showNullItem="true" nullItemText="请选择..."/>
    					类型：<input id="type" class="mini-combobox" style="width:150px;" textField="text" valueField="id"
    data="[{id: '0', text: '全部'}, {id: '1', text: '骰魔大赛'}, {id: '2', text: '挑战'}]" value="0" allowInput="false" showNullItem="true" nullItemText="请选择..."/>
						<?php autocomplete_name() ;?>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div field="id" width="20" headerAlign="center" allowSort="true">id</div>
        	<div field="start" width="30" headerAlign="center" allowSort="true">时间</div>
        	<div field="type" width="30" headerAlign="center" allowSort="true">类型</div>
    		<div field="user_1" displayField="name_1" width="30" headerAlign="center" allowSort="true">用户1</div>
			<div field="gold_1" width="30" headerAlign="center" allowSort="true">金币1</div>
        	<div field="npc_1" width="10" headerAlign="center" allowSort="true">NPC</div>
    		<div field="user_2" displayField="name_2" width="30" headerAlign="center" allowSort="true">用户2</div>
            <div field="gold_2" width="30" headerAlign="center" allowSort="true">金币2</div>
        	<div field="npc_2" width="10" headerAlign="center" allowSort="true">NPC</div>
        	<div field="bet" width="30" headerAlign="center" allowSort="true">赛金</div>
        	<div field="must_win" width="30" headerAlign="center" allowSort="true">必赢</div>
            <div field="sai_1" width="30" headerAlign="center" allowSort="true">骰1</div>
            <div field="sai_2" width="30" headerAlign="center" allowSort="true">骰2</div>
        	<div field="action" width="30" headerAlign="center" allowSort="false">操作</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<script type="text/javascript">
	user_context_column = { user_1: 'user_1', user_2: 'user_2' };
	function module() { return 'vs';}
</script>
<?php require 'bottom.php'; ?>
