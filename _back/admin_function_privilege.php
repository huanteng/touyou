<?php require 'head.php'; ?>

<?php check_privilege(0); ?>

<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="240" showCollapseButton="true">
        <div class="mini-fit">
            <ul id="tree1" class="mini-tree" url="data/admin_function.php?method=order" style="width:100%;" showTreeIcon="true" textField="name" idField="id" parentField="parent_id" resultAsTree="false">        
            </ul>
        </div>
    </div>
    <div showCollapseButton="true">
        <div class="mini-toolbar" style="padding:2px;border-top:0;border-left:0;border-right:0;">                            
            <a class="mini-button" iconCls="icon-save" plain="true" onclick="saveData()">保存</a>                  
        </div>
        <div class="mini-fit" >
            <div id="grid1" class="mini-datagrid" style="width:100%;height:100%;" 
                borderStyle="border:0;"
                url="data/administrator.php?metchod=sel_administrator"
                showFilterRow="true" allowCellSelect="true" allowCellEdit="true"                
            >
                <div property="columns">
                    <div field="checkbox" width="10" headerAlign="center" allowSort="true">权限
                    </div>
                    <div field="id" width="20" headerAlign="center" allowSort="true">ID
                    </div>      
                    <div field="name" width="30" headerAlign="center" allowSort="true">帐号                        
                    </div>                
                    <div field="status" displayField="status_name" width="30" allowSort="true" renderer="onGenderRenderer" align="center" headerAlign="center">状态
                    </div>                        
                    <div field="last" width="50" headerAlign="center" dateFormat="yyyy-MM-dd" allowSort="true">创建日期</div>                
                </div>
            </div> 
			
			
			<div id="listbox1" class="mini-listbox" style="width:350px;height:200px;"
				textField="name" valueField="id" 
				url="" showCheckBox="true">
				<div property="columns">
                    <div field="id" width="20" headerAlign="center" allowSort="true">ID
                    </div>      
                    <div field="name" width="30" headerAlign="center" allowSort="true">帐号                        
                    </div>                
                    <div field="status" displayField="status_name" width="30" allowSort="true" renderer="onGenderRenderer" align="center" headerAlign="center">状态
                    </div>                        
                    <div field="last" width="50" headerAlign="center" dateFormat="yyyy-MM-dd" allowSort="true">创建日期</div>                
                </div>
			</div>
			
			
			
			
			
			
        </div>
    </div>        
</div>



<script type="text/javascript">
    mini.parse();

    var tree = mini.get("tree1");
    var grid = mini.get("grid1");

    tree.on("nodeselect", function (e) {
        if (e.isLeaf) {
            grid.load({ privilege: e.node.id });
            
            grid.on("drawcell", function (e) {
                var record = e.record,
                    column = e.column,
                    field = e.field,
                    value = e.value;

                if (field == "checkbox") {
                    e.cellHtml = '<input class="mini-checkbox" uid='+e.record.id+' />';
                }
				
				if (field == "status") {
                    e.cellHtml = (e.record.status==0) ? '正常' : '关闭';
                }
            });
            
            
//            $.ajax({
//                url: "data/administrator.php?method=privilege",
//                data: { privilege: e.node.id },
//                type: "POST",
//				dataType: "json",
//                success: function (text) {
//
//					$.each( text, function(i, n){
//$(this).attr("checked",true);
//						$('.mini-checkbox').each(function(k){
//							if( $(this).attr('uid') == n.administrator ) $(this).attr("checked",true);
//							else $(this).attr("checked",false);
//						});
//
//					});
//					
//					
//                },
//                error: function (jqXHR, textStatus, errorThrown) {
//                    alert(jqXHR.responseText);
//                }
//            });
            
            
				$('#listbox1').load('data/user.php?method=search&key=s');
            
            
            
            
            
        } else {
            grid.setData([]);
            grid.setTotalCount(0);
        }
    });
    //////////////////////////////////////////////
    function onNameFilterChanged(e) {
        var textbox = e.sender;
        var key = textbox.getValue();

        var node = tree.getSelectedNode();
        if (node) {
            grid.load({ privilege: node.id, key: key });
        }
    }
    function addRow() {            
        var node = tree.getSelectedNode();
        if (node) {
            var newRow = { name: "New Row" };
            newRow.dept_id = node.id;
            grid.addRow(newRow, 0);
        }
    }
    function removeRow() {
        var rows = grid.getSelecteds();
        if (rows.length > 0) {
            grid.removeRows(rows, true);
        }
    }
    function saveData() {
        var data = grid.getChanges();
        var json = mini.encode(data);
        grid.loading("保存中，请稍后......");
        $.ajax({
            url: "data/administrator_privilege.php",
            data: { data: json },
            type: "post",
            success: function (text) {
                grid.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    }
</script>

  
<div class="description">
    <h3>说明</h3>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'admin_function';}
</script>
