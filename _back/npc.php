<?php
require 'top.php';
require 'user_context.php';

check_privilege(0);
check_privilege(53);
?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div type="checkcolumn"></div>
            <div field="uid" width="120" headerAlign="center" allowSort="true">id
            </div>
            <div field="uid" displayfield="name" width="120" headerAlign="center" allowSort="true">名称
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
            <div field="gold" width="60" headerAlign="center" allowSort="false">金币</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'npc';}
</script>