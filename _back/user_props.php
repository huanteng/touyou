<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(66);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

function module() { return 'user_props';}

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	
	var data={};
	<?php if( isset($_GET['user']) ){ ?>	
	data.user = '<?php echo $_GET['user'];?>';
	<?php }?>
	<?php if( isset($_GET['props']) ){ ?>	
	data.props = '<?php echo $_GET['props'];?>';
	<?php }?>	
	grid.load(data);
	
	grid.on("drawcell", function (e) {
	    var row = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;

		var html = '';
		switch( field )
		{
			case 'npc':
				html = (value == '1') ? '√' : ' ';
				break;

			case 'action':
				html = '<a href="javascript:void(0)" onclick="del('+row.id+')">删除</a>';
				html += ( row.props > 1 && row.quantity > 0 && row.npc == 1) ? '  <a href="javascript:void(0)" ' +
					'onclick="use_props('+row.user+','+row.props+')">使用</a>' : '  ';
				break;
	    }

		if( html != "" )
		{
			e.cellHtml = html;
		}

	});

}

function search()
{
	var	user = mini.get("uid").getValue();
	var npc = mini.get("#npc").getValue();
    var props = mini.get("#props").getValue();
    
    grid.load({ user: user, npc: npc, props: props });
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
                   	    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						<?php autocomplete_name() ;?>
						NPC：<input id="npc" class="mini-combobox" style="width:150px;" allowInput="false" showNullItem="true" nullItemText="请选择..." data="[{ id: 1, text: 'NPC' }, { id: 0, text: '非NPC'}]"/>
                        道具：<input id="props" class="mini-combobox" style="width:150px;" textField="name" valueField="id"
    url="data/props.php?method=drop&havegold=true" value="<?php echo isset($_GET['props'])?$_GET['props']:'';?>" allowInput="false" showNullItem="true" nullItemText="请选择..."/>        
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
        	<div width="10" type="checkcolumn"></div>
            <div field="id" width="20" headerAlign="center" allowSort="true">id</div>
    		<div field="user" displayField="name" width="30" headerAlign="center" allowSort="true">用户</div>
            <div field="npc" width="10" headerAlign="center" allowSort="true">NPC</div>
        	<div field="props" displayField="props_name" width="30" headerAlign="center" allowSort="true">道具</div>
            <div field="quantity" width="30" headerAlign="center" allowSort="true">数量</div>
        	<div field="expire" width="30" headerAlign="center" allowSort="true">有效时间</div>
			<div field="action" width="20" headerAlign="center" allowSort="false">操作</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<script type="text/javascript">
	user_context_column = { user: 'user' };

	function use_props(uid,props)
	{
		if( confirm('确定使用此道具吗？') )
		{
			$.ajax
			({
				type : "POST",
				url : 'data/user_props.php?method=use_props',
				dataType : 'json',
				async : false,
				data : {uid: uid,id:props},
				success : function(data) {
					alert(data.memo);
					grid.reload();
				},
				error : function (jqXHR, textStatus, errorThrown) {error (jqXHR, textStatus, errorThrown);}
			});
		}
	}

	function del( id )
	{
		if( confirm('确定删除此道具吗？') )
		{
			var func = function (data) {
				alert(data.memo);
				grid.reload();
			};

			post( "user_props", "delete", id, func, function(){} );
		}
	}
</script>

<?php require 'bottom.php'; ?>
