<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(76); ?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div width="10" type="checkcolumn"></div>
            <div field="id" width="10" headerAlign="center" allowSort="true">id
            </div>
            <div field="name" width="30" headerAlign="center" allowSort="true">名字
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="props_id" displayField="props" width="30" headerAlign="center" allowSort="true">道具
                <input property="editor" class="mini-combobox" style="width:100%;" textField="name" valueField="id" url="data/props.php?method=drop&havegold=1"/>
            </div>
            <div field="props_count" width="30" headerAlign="center" allowSort="true">数量
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="need" width="30" headerAlign="center" allowSort="true">要求次数
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
			<div field="del" width="30" headerAlign="center" allowSort="true">抵销次数
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
			<li>要求次数：当用户抽奖达到一定次数时，才可能抽中；0表示不限</li>
			<li>抵销次数：当用户抽奖抽中后，减少相应的次数，负数表示增加</li>
			<li>对次数的记录，仅保留一天，如果一天内不抽奖，次数重新起计。</li>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'lucky_gift';}
</script>