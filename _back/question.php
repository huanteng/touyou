<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(68); ?>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
}

function search() {
    var q = mini.get("q").getValue();
    grid.load({ q: q });
}
function onKeyEnter(e) {
    search();
}

function add() {
	// 此函数最好在具体实现中覆盖，以实现默认值
    var newRow = {};
    grid.addRow(newRow, 0);
}

function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}


function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-add" onclick="add()" plain="true">增加</a>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
                        <input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:150px;" onenter="onKeyEnter"/>   
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>
	
	<?php $module = 'question'; ?>
  
    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="user"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
			<div field="id" width="20" allowSort="true">ID
            </div>
        	<div field="type" displayField="type_name" name="user" width="30" headerAlign="left" >类型 
				<input id="type" property="editor" class="mini-combobox" style="width:150px;" textField="text" valueField="id" 
    url="data/question.php?method=type" value="" required="true" allowInput="true" showNullItem="true" nullItemText="请选择..."/>
            </div>
            <div field="title" width="100" allowSort="true">题目
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>    
            <div field="content" headerAlign="center" allowSort="true">内容
                <input property="editor" class="mini-textarea" style="width:100%;" />
            </div>                      
            <div field="option" width="30" headerAlign="center" allowSort="true">选项
        		<input property="editor" class="mini-textarea" style="width:100%;" minHeight="80"/>
            </div>
            <div field="default" width="50" headerAlign="center" allowSort="false">默认值
        		<input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
            <div field="length" width="30" headerAlign="center" allowSort="true" dateFormat="MM-dd HH:NN">长度
                <input property="editor" class="mini-textbox" style="width:100%;"/>
            </div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
//////覆盖
function module() { return '<?php echo $module; ?>';}
</script>
