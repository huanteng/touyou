<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(93); ?>
	
	<?php $module = 'partner'; ?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        url="data/administrator.php?method=search" idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >    
        <div property="columns">
            <div type="checkcolumn"></div>
            <div field="id" width="100" headerAlign="center" allowSort="true">id</div>
            <div field="name" width="100" headerAlign="center" allowSort="true">帐号
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>                      
            <div field="pass" width="100" headerAlign="center" allowSort="true">密码
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
			<div field="key" width="100" headerAlign="center" allowSort="true">关键字(渠道)
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
            <div field="last" width="100" headerAlign="center">登录</div>
            <div type="checkboxcolumn" field="status" trueValue="0" falseValue="1" width="60" headerAlign="center">状态</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return '<?php echo $module; ?>';}
</script>
