<?php
require 'head.php';
require 'user_context.php';
?>
<?php check_privilege(0); ?>
<?php check_privilege(22); ?>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=history');
	
	var data={};
	<?php if( isset($_GET['uid']) ){?>
	data.uid = '<?php echo isset($_GET['uid'])?$_GET['uid']:'';?>';
	<?php } else {?>
	data.sender = '<?php echo isset($_GET['sender'])?$_GET['sender']:'';?>';
	data.receiver = '<?php echo isset($_GET['receiver'])?$_GET['receiver']:'';?>';
	<?php }?>		
	grid.load(data);
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
		if (field == "sender_name") {
	        e.cellHtml = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail' + record.sender+'", text:"'+record.sender_name+'", url: "user_detail.php?uid='+record.sender+'"})\'>'+record.sender_name+'</a>';
	    }
		
		if (field == "receiver_name") {
	        e.cellHtml = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail' + record.receiver+'", text:"'+record.receiver_name+'", url: "user_detail.php?uid='+record.receiver+'"})\'>'+record.receiver_name+'</a>';
	    }
		
	    if (field == "sender_npc" || field == "receiver_npc") {
	        e.cellHtml = (value == '1') ? '√' : '';
	    }
	});
}

function search() {
    var sender = mini.get("#sender").getValue();
    var receiver = mini.get("#receiver").getValue();
    grid.load({ sender: sender, receiver: receiver });
}
function onKeyEnter(e) {
    search();
}

function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">

    					发送人：<div id="sender" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id" 
					        url="data/user.php?method=search&sortField=name&sortOrder=asc" value="" text="">     
					        <div property="columns">
					            <div header="名字" field="name" width="30"></div>
								<div header="NPC" field="npc" width="10"></div>
								<div header="登录" field="login" width="25"></div>
					        </div>
					    </div>

    					接收人：<div id="receiver" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id" 
					        url="data/user.php?method=search&sortField=name&sortOrder=asc" value="" text="">     
					        <div property="columns">
					            <div header="名字" field="name" width="30"></div>
								<div header="NPC" field="npc" width="10"></div>
								<div header="登录" field="login" width="25"></div>
					        </div>
					    </div>
						
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div width="10" type="checkcolumn"></div>
            <div field="id" width="10" headerAlign="center" allowSort="true">id</div>
        	<div field="time" width="30" headerAlign="center" allowSort="true">时间</div>
            <div field="sender_name" width="30" headerAlign="center" allowSort="false">发送人</div>
            <div field="sender_npc" width="5" headerAlign="center" allowSort="false">NPC</div>	                      
            <div field="receiver_name" width="30" headerAlign="center" allowSort="false">接收人</div>
            <div field="receiver_npc" width="5" headerAlign="center" allowSort="false">NPC</div>	
            <div field="content" headerAlign="center" allowSort="false">内容</div>          	
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
	user_context_column = { sender: 'sender_name', receiver: 'receiver_name' };
function module() { return 'message';}
</script>