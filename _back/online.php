<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(26);
?>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
		if (field == "user") {
	        e.cellHtml = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail' + record.user+'", text:"'+record.name+'", url: "user_detail.php?uid='+record.user+'"})\'>'+record.name+'</a>';
	    }
		
	    if (field == "npc") {
	        e.cellHtml = (value == '1') ? '√' : '';
	    }
	    
	    if (field == "sex") {
	        e.cellHtml = (value == '0') ? '男' : '女';
	    }	
	});

}

function search() {
    var q = mini.get("q").getValue();
    var npc = $("#npc").val();
    grid.load({ user: q, npc: npc });
}

function onKeyEnter(e) {
    search();
}

function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束

</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
    					NPC：<select id="npc"><option value=''></option><option value='1'>NPC</option><option value='0' selected>非NPC</option></select>
    
    
                        名字：<div id="q" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id" 
					        url="data/user.php?method=search&sortField=name&sortOrder=asc" value="" text=""  onvaluechanged="onValueChanged">     
					        <div property="columns">
					            <div header="名字" field="name" width="30"></div>
								<div header="NPC" field="npc" width="10"></div>
								<div header="登录" field="login" width="25"></div>
					        </div>
					    </div>   
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >

        <div property="columns">
            <div width="5" type="checkcolumn"></div>
            <div field="user" width="25" headerAlign="center" allowSort="true" >名字</div>
        	<div field="npc" width="10" headerAlign="center" allowSort="true">NPC</div>
			<!--div field="puppet" width="10" headerAlign="center">马甲</div-->
			<div field="gold" width="20" headerAlign="center" allowSort="false">金币</div>
        	<div field="sex" width="10" headerAlign="center" allowSort="true">性别</div>
			<div field="version" width="10" headerAlign="center">版本</div>
			<div field="status" width="10" headerAlign="center" allowSort="true">状态</div>
			<div field="status_time" width="10" headerAlign="center" allowSort="true">状态时间</div>
            <div field="login" width="15" headerAlign="center" allowSort="true">登录时间</div>
            <div field="last" width="15" headerAlign="center" allowSort="true">最近心跳</div>
        	<div field="online" width="15" headerAlign="center" allowSort="true">在线时长</div>
			<div field="sid" width="30" headerAlign="center" allowSort="true">SID
				<input property="editor" class="mini-textarea" style="width:100%;" />
			</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
			<li>输赢金币：本次登录以来的金币输赢</li>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
	user_context_column = { user: 'user' };
function module() { return 'online';}
</script>