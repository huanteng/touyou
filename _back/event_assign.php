<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(90);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
	    if (field == "npc") {
	        e.cellHtml = (value == '1') ? '√' : '';
	    }
		
		if (field == "action") {
	        e.cellHtml = '<a href="javascript:void(0);" onClick="uses('+e.record.uid+')">抽奖</a>';
	    }

	});

}

function search() {
    var user = mini.get("user").getValue();

    grid.load({ user: user });
}
function onKeyEnter(e) {
    search();
}

function add() {
	// 此函数最好在具体实现中覆盖，以实现默认值
    var newRow = {};
    grid.addRow(newRow, 0);
}
function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}
function truncate() {
    grid.loading("清空中，请稍候...");
	post( module(), "truncate", '', function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-add" onclick="add()" plain="true">增加</a>
                        <a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						<?php autocomplete_name() ;?>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div property="columns">
            <div width="10" type="checkcolumn"></div>
            <div field="id" width="20" headerAlign="center" allowSort="true">id
            </div>
    		<div field="uid" displayField="name" width="30" headerAlign="center" allowSort="true">用户
                <input property="editor" class="mini-buttonedit" onbuttonclick="onButtonEdit" style="width:100%;" />
            </div>
            <div field="gift_id" displayField="gift" width="50" headerAlign="center" allowSort="true">奖品
				<input property="editor" class="mini-combobox" style="width:100%;" textField="name" valueField="id" url="data/event_gift.php?method=drop" />
            </div>
			<div field="begin_time" width="50" headerAlign="center" allowSort="true" dateFormat="yyyy-MM-dd H:mm">生效时间
                <input property="editor" class="mini-datepicker" showTime="true" format="yyyy-MM-dd H:mm" style="width:100%;" />
            </div>
			<div field="rank" width="30" headerAlign="center" allowSort="true">顺序
				<input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="remark" width="120" headerAlign="center" allowSort="false">备注
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
			<div field="action" width="50" headerAlign="center" allowSort="false">操作
            </div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
		<ul>
			<li>如果要设置非指定的npc中奖，请在“用户”列输入0</li>
			<li>如果备注为“npc”，会优先被随机系统抽中</li>
		</ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'event_assign';}

function onButtonEdit(e) {
	var data = null;
	var fn = function (data) {
			grid.cancelEdit();
            var row = grid.getSelected();
            grid.updateRow(row, {
                uid: data.id,
                name: data.text
            });
		};
    sel_user( data, fn );
}

function uses(uid)
{
	if( confirm('确定抽奖吗？') )
	{
		$.ajax
		({
			type : "POST",
			url : 'data/event_assign.php?method=uses',
			dataType : 'json',
			async : false,
			data : {uid: uid},
			success : function(data) {
				alert('抽奖成功');
				grid.reload();
			},
			error : function (jqXHR, textStatus, errorThrown) {error (jqXHR, textStatus, errorThrown);}
		});
	}
}
</script>