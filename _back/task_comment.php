<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(105); ?>

<div style="width:98%;">
	<div id="form1">
		<table class="form-table" border="0" cellpadding="1" cellspacing="1">
			<tr>
              <td class="label">任务名：</td>
			  <td><input class="mini-textbox" id="name" name="name" style="width:200px" required="true" /><span class="red">*</span></td>
			</tr>
			<tr>
				<td class="label">执行时间：</td>
				<td ><input name="time" class="mini-datepicker" style="width:200px" showTime="true"
							format="yyyy-MM-dd HH:mm:ss" /></td>
			</tr>
			<tr>
              <td class="label">简介：</td>
			  <td><input class="mini-textbox" name="introduce" style="width:200px" required="true" /><span class="red">*</span></td>
			</tr>
			<tr>
              <td class="label">说明：</td>
			  <td><input name="description" class="mini-textarea" style="width:200px" required="true" /><span class="red">*</span></td>
			</tr>
			<tr>
              <td class="label">贴子id：</td>
			  <td><input name="target" class="mini-textbox" style="width:200px" vtype="int" required="true" /><span class="red">*</span>
				  <br><input name="on_queue" class="mini-checkbox" text="贴子处于队列中，未正式发布" trueValue="1" falseValue="0">
			  </td>
			</tr>
			<tr>
              <td class="label">奖励：</td>
			  <td><input name="gift" class="mini-textbox" style="width:200px" required="true" /><span class="red">*</span></td>
			</tr>
			<tr>
              <td class="label">可完成数量：</td>
			  <td><input name="count" class="mini-textbox" style="width:200px" />“-1”表示不限，当达到数量限制时，不再分配，并且将已分配的任务删除</td>
			</tr>
			<tr>
              <td class="label">按钮文字：</td>
			  <td ><input name="text" class="mini-textbox" style="width:200px" required="true" /><span class="red">*</span></td>
			</tr>
			<tr>
              <td class="label">完成提示：</td>
			  <td><input name="memo" class="mini-textarea" style="width:200px" required="true" /><span class="red">*</span></td>
			</tr>
			<tr>
              <td class="label"></td>
			  <td>
				<input name="online" class="mini-checkbox" text="给在线用户分配本任务" trueValue="1" falseValue="0"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><a class="mini-button" onclick="submitForm()" style="width: 150px;">保存</a></td>
			</tr>
		</table>
	</div>
</div>

<div class="description">
	<h3>说明</h3>
	<ul>
		<li>本功能修改id=44的任务；</li>
		<li>建议每天修改1~2次；</li>
	</ul>
</div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
mini.parse();

var form = new mini.Form("form1");

function submitForm()
{
	form.validate();
	if (form.isValid() == false) return;

	var data = form.getData();
	var value = mini.encode(data);
	post( 'task', "set_comment", value, function (text) {
			alert(text.memo);
			location.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

init_form( 'form1', 'task', "init_comment" );

</script>