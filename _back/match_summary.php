<?php
require 'head.php';
require 'user_context.php';
?>
<?php check_privilege(0); ?>
<?php check_privilege(114); ?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();

	grid.on("drawcell", function (e) {
	    var row = e.record,
	        column = e.column,
	        field = e.field,
			value = e.value;

		var html = "";
		switch( field )
		{
			case 's':
				html = parseInt(row.s1) + parseInt(row.s2) + parseInt(row.s3) + parseInt(row.cs1) + parseInt(row.cs2) + parseInt(row.cs40);
				break;
			case 'name':
				if( row.online )
				{
					html = '<font color="red">' + row.name + '</font>';
				}
				else
				{
					html = row.name;
				}
				break;
			case 'action':
				html += ' <a href="javascript:void()" onclick="show();return false;">挑战</a>';
				break;
	    }
		if( html != "" )
		{
			e.cellHtml = html;
		}
		else
		{
			if( value == 0 )
			{
				e.cellHtml = '';
			}
		}

	});
}

function search() {
    search_by_id({id:'uid'});
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						<?php autocomplete_name() ;?>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;"
        idField="id"
        allowResize="true" pageSize="100" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"
        showEmptyText="true"  contextMenu="#gridMenu"
    >
        <div property="columns">
			<div field="name" width="60" headerAlign="center" allowSort="false">用户名</div>
			<div field="gold" width="40" headerAlign="center">金币</div>
			<div field="s" width="40" headerAlign="center" align="center" allowSort="true">赢利</div>
			<div field="action" width="30" headerAlign="center" allowSort="false">操作</div>
			<div header="屌丝" headerAlign="center">
				<div property="columns">
					<div field="w1" width="40" headerAlign="center" align="center" allowSort="true">赢</div>
					<div field="d1" width="40" headerAlign="center" align="center" allowSort="true">和</div>
					<div field="l1" width="40" headerAlign="center" align="center" allowSort="true">输</div>
					<div field="s1" width="40" headerAlign="center" align="center" allowSort="true">赢利</div>
				</div>
			</div>

			<div header="土豪" headerAlign="center">
				<div property="columns">
					<div field="w2" width="40" headerAlign="center" align="center" allowSort="true">赢</div>
					<div field="d2" width="40" headerAlign="center" align="center" allowSort="true">和</div>
					<div field="l2" width="40" headerAlign="center" align="center" allowSort="true">输</div>
					<div field="s2" width="40" headerAlign="center" align="center" allowSort="true">赢利</div>
				</div>
			</div>

			<div header="富豪" headerAlign="center">
				<div property="columns">
					<div field="w3" width="40" headerAlign="center" align="center" allowSort="true">赢</div>
					<div field="d3" width="40" headerAlign="center" align="center" allowSort="true">和</div>
					<div field="l3" width="40" headerAlign="center" align="center" allowSort="true">输</div>
					<div field="s3" width="40" headerAlign="center" align="center" allowSort="true">赢利</div>
				</div>
			</div>

			<div header="金币挑战" headerAlign="center">
				<div property="columns">
					<div field="cw1" width="40" headerAlign="center" align="center" allowSort="true">赢</div>
					<div field="cl1" width="40" headerAlign="center" align="center" allowSort="true">和</div>
					<div field="cs1" width="40" headerAlign="center" align="center" allowSort="true">赢利</div>
				</div>
			</div>

			<div header="元宝挑战" headerAlign="center">
				<div property="columns">
					<div field="cw2" width="40" headerAlign="center" align="center" allowSort="true">赢</div>
					<div field="cl2" width="40" headerAlign="center" align="center" allowSort="true">和</div>
					<div field="cs2" width="40" headerAlign="center" align="center" allowSort="true">赢利</div>
				</div>
			</div>

			<div header="宝石挑战" headerAlign="center">
				<div property="columns">
					<div field="cw40" width="40" headerAlign="center" align="center" allowSort="true">赢</div>
					<div field="cl40" width="40" headerAlign="center" align="center" allowSort="true">和</div>
					<div field="cs40" width="40" headerAlign="center" align="center" allowSort="true">赢利</div>
				</div>
			</div>
        </div>
    </div>

	<div class="description">
        <h3>说明</h3>
        <ul>
			<li>名字为<font color="red">红色</font>表示在线；</li>
        </ul>
    </div>

<?php require 'match_summary_inc.php'; ?>
<?php require 'bottom.php'; ?>

<script type="text/javascript">
	user_context_column = { id: 'name' };
function module() { return 'match_summary';}
</script>