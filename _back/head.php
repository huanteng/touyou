<?php
// 大体抄自backend
function check_privilege( $function_id )
{
	// 不想因cookie而调用复杂的MVC，如果在MVC修改了key和cookie参数，必须在此同步
	require_once dirname( __FILE__ ) . '/../library/cookie.php';
	$cookie = new cookie( $_COOKIE, 'c]5', 'http://' . $_SERVER['HTTP_HOST']);
	
	$account = $cookie->get( 'account_name', true );
	if( $function_id == 0 ) 
	{
		if ( $account == '' ) die( '未登录。' );
		return;
	}
	
	$privilege = unserialize($cookie->get( 'privilege',true));

	if( !isset( $privilege[ $function_id ] ) ) die( "用户名：$account ，功能号：$function_id ，结果：权限不足。");
}

/* 自动完成用户名，因多处使用，所以统一写
 */
function autocomplete_name( $id = 'uid' )
{
	$s = '用户：<div id="' . $id . '" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id"
					        url="data/user.php?method=autocomplete" value="" text=""  onvaluechanged="onValueChanged">
					        <div property="columns">
					            <div header="名字" field="name" width="30"></div>
								<div header="NPC" field="npc" width="10"></div>
								<div header="登录" field="login" width="25"></div>
					        </div>
					    </div>';
	echo $s;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>骰友管理后台</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" /><link href="css/css.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
    body
    {
        width:100%;height:100%;margin:0;
    }
    </style>
    <script src="js/boot.js" type="text/javascript"></script>
    <script src="js/common.js" type="text/javascript"></script>    
</head>
<body>
