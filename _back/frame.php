<?php require 'head.php'; ?>

<style type="text/css">
body{
    margin:0;padding:0;border:0;width:100%;height:100%;overflow:hidden;
}    
.header
{
    background:url(images/header.gif) repeat-x 0 -1px;
}
</style>

<!--Layout-->
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
    
    <div title="center" region="center" style="border:0;" bodyStyle="overflow:hidden;">
        <!--Splitter-->
        <div class="mini-splitter" style="width:100%;height:100%;" borderStyle="border:0;">
            <div size="180" maxSize="250" minSize="100" showCollapseButton="true" style="border:0;">
                <!--OutlookTree-->
                <div id="leftTree" class="mini-outlooktree" url="data/admin_function.php?method=tree" onnodeselect="onNodeSelect"
                    textField="name" idField="id" parentField="parent_id"  
                >
                </div>
                
            </div>
            <div showCollapseButton="false" style="border:0;">
                <!--Tabs-->
                <div id="mainTabs" class="mini-tabs" activeIndex="0" style="width:100%;height:100%;"      
                        
                >
                    <!--div title="错误" url="../backend/error.php" showCloseButton="true"></div>
                    <div title="报警检查" url="../backend/alert.php" showCloseButton="true"></div>
                    <div title="弹出面板" url="../datagrid/datagrid.html" >        
                    </div>
                    <div title="弹出面板" url="../datagrid/fixedcolumns.html" >        
                    </div-->
                </div>
            </div>        
        </div>
    </div>
</div>

<script type="text/javascript">
mini.parse();

var tree = mini.get("leftTree");

function showTab(node) {
	if( node.is_new_window == '1' )
	{
		open(node.url);
	}
	else
	{
        var tabs = mini.get("mainTabs");

        var id = "tab$" + node.id;
        var tab = tabs.getTab(id);
        if (!tab) {
            tab = {};
            tab.name = id;
            tab.title = node.text + '<a href="' + node.url + '" target="_blank"><img src="js/miniui/themes/icons/zoomin.gif" border="0" height="13"></a><a href="#" onclick="reload(\'' + id + '\')"><img src="js/miniui/themes/icons/reload.png" border="0" height="13"></a>';
            tab.showCloseButton = true;
            //tab.refreshOnClick = true;

            tab.url = node.url;

            tabs.addTab(tab);
        }
        tabs.activeTab(tab);
    }
}

function onNodeSelect(e) {
    var node = e.node;
    var isLeaf = e.isLeaf;

    //if (isLeaf) {
    if (node.url!='') {
        showTab(node);
    }
}

function onClick(e) {
    var text = this.getText();
    alert(text);
}
function onQuickClick(e) {
    tree.expandPath("datagrid");
    tree.selectNode("datagrid");
}
function reload( id )
{
	var tabs = mini.get("mainTabs");
	var tab = tabs.getTab(id);
	tabs.reloadTab(tab);
	return false;
}

showTab({id:'tab$error', text:'错误', url: '../backend/error.php'});
//showTab({id:'-2', text:'报警检查', url: '../backend/alert.php'});
/*showTab({id:'tab$22', text:'聊天列表', url: '../backend/message2.php'});
showTab({id:'tab$99', text:'审核图片', url: 'album_audit.php'});
showTab({id:'tab$27', text:'说两句', url: 'comment.php'});
showTab({id:'tab$100', text:'视频认证', url: 'album_audit_video.php'});
*/
</script>

<?php require 'bottom.php'; ?>