<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(116);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	
	var data={};
	grid.load(data);
	
	grid.on("drawcell", function (e) {
	    var row = e.record,
	        //column = e.column,
	        field = e.field,
	        value = e.value;

		var html = "";
		switch( field )
		{
			case "action":
			//	html = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail' + record.id+'", text:"'+record.name+'", url: "user_detail.php?uid='+record.id+'"})\'>修改</a>';
				break;
	    }
		if( html != "" ) e.cellHtml = html;

	});

}

function search()
{
	var	uid = mini.get("uid").getValue();
	var date = mini.get("date").getValue();
    
    grid.load({ uid: uid, date: date });
}
function onKeyEnter(e) {
    search();
}

////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
					</td>
                    <td style="white-space:nowrap;" id="search">
                        用户：<div id="uid" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id"
					        url="data/user.php?method=search&sortField=name&sortOrder=asc" value="" text=""  onvaluechanged="onValueChanged">     
					        <div property="columns">
					            <div header="名字" field="name" width="30"></div>
								<div header="NPC" field="npc" width="10"></div>
								<div header="登录" field="login" width="25"></div>
					        </div>
					    </div>
						日期：<input id="date" class="mini-input" style="width:60px;" allowInput="true" title="请输入，格式为8位数字，如20131225"/>
						<a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div width="10" property="columns">
            <div field="id" width="10" headerAlign="center" allowSort="true">id</div>
    		<div field="name" width="30" headerAlign="center" allowSort="false">名称</div>
            <div field="reg_date" width="20" headerAlign="center" allowSort="true">注册日期</div>
        	<div field="login_date" width="20" headerAlign="center" allowSort="true">登录日期</div>
			<div field="dec" width="20" headerAlign="center" allowSort="true">日差</div>
			<div field="action" width="20" headerAlign="center" allowSort="false">操作</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
user_context_column = { uid: 'name' };
function module() { return 'day_login';}
</script>