<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(18);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	
	var data={};
	<?php if( isset($_GET['user']) ){ ?>	
	data.user = '<?php echo $_GET['user'];?>';
	<?php }?>
	grid.load(data);
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;

		var html = "";
		switch( field )
		{
			case "name":
				html = '<img src="'+record.logo+'" style="width:40px;height:40px;"/>&nbsp;&nbsp;<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail' + record.id+'", text:"'+record.name+'", url: "user_detail.php?uid='+record.id+'"})\'>'+record.name+'</a>';
				break;
		
			case "npc":
				html = ( value == '1' ) ? '√' : '';
				break;
		
			case "action":
				html = '<a href="javascript:void();" onclick=\'window.parent.showTab({id:"detail' + record.id+'", text:"'+record.name+'", url: "user_detail.php?uid='+record.id+'"})\'>修改</a>';
				break;
	    }
		if( html != "" ) e.cellHtml = html;

	});

}

function search() {
	
	var	q = mini.get("q").getValue();	
	var	user = mini.get("uid").getValue();
	var npc = mini.get("npc").getValue();
	var sex = mini.get("sex").getValue();
    
    grid.load({ q:q, user: user, npc: npc, sex:sex });
}
function onKeyEnter(e) {
    search();
}

function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
						<a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						<?php autocomplete_name() ;?>
						NPC：<input id="npc" class="mini-combobox" style="width:60px;" allowInput="true" showNullItem="true" nullItemText="请选择..." data="[{ id: 1, text: 'NPC' }, { id: 0, text: '非NPC'}]"/>
						性别：<input id="sex" class="mini-combobox" style="width:60px;" allowInput="true" showNullItem="true" nullItemText="请选择..." data="[{ id: 0, text: '男' }, { id: 1, text: '女'}]"/>
                        <input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:150px;" onenter="onKeyEnter"/> 
						<a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div width="10" property="columns">
			<div width="10" type="checkcolumn"></div>
            <div field="id" width="20" headerAlign="center" allowSort="true">id</div>
    		<div field="name" width="30" headerAlign="center" allowSort="false">名称</div>
            <div field="npc" width="5" headerAlign="center" allowSort="true">NPC</div>
        	<div field="register" width="20" headerAlign="center" allowSort="true">注册</div>
            <div field="login" width="20" headerAlign="center" allowSort="true">登录</div>
			<div field="email" width="30" headerAlign="center" allowSort="true">邮箱</div>
        	<div field="city" width="20" headerAlign="center" allowSort="true">城市</div>
			<div field="version" width="20" headerAlign="center" allowSort="true">版本</div>
			<div field="channel" width="20" headerAlign="center" allowSort="true">渠道</div>
			<div field="creater" displayField="creater_name" width="20" headerAlign="center" allowSort="true">创建人</div>
			<div field="action" width="20" headerAlign="center" allowSort="false">操作</div>
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
user_context_column = { uid: 'name' };
function module() { return 'user';}
</script>