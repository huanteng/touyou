<?php
require 'head.php';
require 'user_context.php';
?>
<?php check_privilege(0); ?>
<?php check_privilege(19); ?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();

	grid.on("drawcell", function (e) {
	    var row = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;

		var viewName = function(uid,name, npc)
		{
			var html = '';

			if( name )
			{
				html = '<span title="uid=' + uid + '"';
				if( !npc )
				{
					html += ' style="color: red;"';
				}

				html += '>' + name + '</span>';
			}
			return html;
		}

		var html = "";
		switch( field )
		{
			case 'name1':
				html = viewName( row.uid1, row.name1, row.npc1 );

				break;
			case 'name2':
				html = viewName( row.uid2, row.name2, row.npc2 );

				break;
			case 'name3':
				html = viewName( row.uid3, row.name3, row.npc3 );

				break;
			case 'name4':
				html = viewName( row.uid4, row.name4, row.npc4 );

				break;
			case 'winner_name':
				html = viewName( row.winner_uid, row.winner_name, row.winner_npc );

				break;
			case 'loser_name':
				html = viewName( row.loser_uid, row.loser_name, row.loser_npc );

				break;
            case "is_drunk":
				html = ( row.is_drunk == '1' ) ? '√' : ' ';
				break;

			case "action":
				html = '<a href="room_match_history_detail.php?id='+row.id+'">查看</a>';
//				html += '&nbsp;<a href="javascript:void();" onclick=\'window.parent.showTab({id:"props' + record.id+'", text:"'+record.name+'持有情况", url: "user_props.php?props='+record.id+'"})\'>持有</a>';
				break;
	    }
		if( html != "" ) e.cellHtml = html;

	});
}

function search() {
    var uid = mini.get("uid").getValue();
    grid.load({ uid: uid });
}
function onKeyEnter(e) {
    search();
}

function add() {
	// 此函数最好在具体实现中覆盖，以实现默认值
    var newRow = {};
    grid.addRow(newRow, 0);
}
function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}
function truncate() {
    grid.loading("清空中，请稍候...");
	post( module(), "truncate", '', function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束

function onShowRowDetail(e) {
	var grid = e.sender;
	var row = e.record;

	var employee_grid = mini.get("employee_grid");
	var detailGrid_Form = document.getElementById("detailGrid_Form");

	var td = grid.getRowDetailCellEl(row);
	td.appendChild(detailGrid_Form);
	detailGrid_Form.style.display = "block";

	employee_grid.load({ match_id: row.id });
}

</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">

					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						用户：<div id="uid" class="mini-autocomplete" style="width:150px;"  popupWidth="250" textField="name" valueField="id"
							 url="data/user.php?method=search&sortField=name&sortOrder=asc" value="" text=""  onvaluechanged="onValueChanged">
							<div property="columns">
								<div header="名字" field="name" width="30"></div>
								<div header="NPC" field="npc" width="10"></div>
								<div header="登录" field="login" width="25"></div>
							</div>
						</div>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;"
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"
        showEmptyText="true" onshowrowdetail="onShowRowDetail" contextMenu="#gridMenu"
    >
        <div property="columns">
			<!--div type="expandcolumn" >#</div-->
            <div field="id" width="10" headerAlign="center" allowSort="true">id</div>
            <div field="name1" width="60" headerAlign="center" allowSort="true">用户1</div>
			<div field="name2" width="60" headerAlign="center" allowSort="true">用户2</div>
			<div field="name3" width="60" headerAlign="center" allowSort="true">用户3</div>
			<div field="name4" width="60" headerAlign="center" allowSort="true">用户4</div>
            <div field="winner_name" width="60" headerAlign="center" allowSort="true">赢家</div>
            <div field="loser_name" width="60" headerAlign="center" allowSort="true">输家</div>
            <div field="is_drunk" width="10" headerAlign="center" allowSort="true">灌醉</div>
        	<div field="time" width="80" headerAlign="center" allowSort="true">结束时间</div>
        	<div field="count" width="20" headerAlign="center" allowSort="true">金币</div>
			<div field="action" width="30" headerAlign="center" allowSort="false">操作</div>
        </div>
    </div>

	<div id="detailGrid_Form" style="display:none;">
        <div id="employee_grid" class="mini-datagrid" style="width:100%;height:150px;"
			showPager="false"
            url="data/room_shout.php?method=search"
        >
            <div property="columns">
                <div field="name" width="120" headerAlign="center" align="right">用户名</div>
                <div field="way" width="100" align="left" headerAlign="center">动作</div>
                <div field="time" width="100" align="left" headerAlign="center">时间</div>
            </div>
        </div>
    </div>


    <div class="description">
        <h3>说明</h3>
        <ul>
			<li></li>
        </ul>
    </div>

<script type="text/javascript">
	user_context_column = { uid1: 'name1', uid2: 'name2', uid3: 'name3', uid4: 'name4', winner_uid: 'winner_name', loser_uid: 'loser_name' };
	function module() { return 'room_match';}
</script>

<?php require 'bottom.php'; ?>
