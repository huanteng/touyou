<?php
require 'head.php';
require 'user_context.php';
?>
<?php check_privilege(0); ?>
<?php check_privilege(119); ?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();

	grid.on("drawcell", function (e) {
	    var row = e.record,
	        column = e.column,
	        field = e.field,
			value = e.value;

		var sex = {0:'男', 1:'女', 2:'-'};
		var props = {1:'金币', 2:'元宝', 40:'宝石'};

		var html = "";
		switch( field )
		{
			case 's':
				html = parseInt(row.s1) + parseInt(row.s2) + parseInt(row.s3) + parseInt(row.cs1) + parseInt(row.cs2) + parseInt(row.cs40);
				break;
			case 'uid':
				html = row.name;
				break;
			case 'uid2':
				if( row.online )
				{
					html = '<font color="red">' + row.name2 + '</font>';
				}
				else
				{
					html = row.name2;
				}
				break;
			case 'sex':
				html = sex[ row.sex ];
				break;
			case 'props_id':
				html = props[ row.props_id ];
				break;
			case 'action':
				html += ' <a href="javascript:void()" onclick="show();return false;">挑战</a>';
				break;
	    }
		if( html != "" )
		{
			e.cellHtml = html;
		}

	});
}

function search() {
    search_by_id({id:'uid'});
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						<!--?php autocomplete_name() ;?>
                        <a class="mini-button" onclick="search()">查询</a-->
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;"
        idField="id"
        allowResize="true" pageSize="100" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"
        showEmptyText="true"  contextMenu="#gridMenu"
    >
        <div property="columns">
        	<div field="id" width="14" headerAlign="center" allowSort="true">id</div>
			<div field="uid" width="60" headerAlign="center" allowSort="false">发起者</div>
			<div field="uid2" width="60" headerAlign="center" allowSort="false">接收者</div>
			<div field="props_id" width="60" headerAlign="center" allowSort="true">挑战金</div>
			<div field="count1" width="60" headerAlign="center" allowSort="true">最小金额</div>
			<div field="count2" width="60" headerAlign="center" allowSort="true">最大金额</div>
			<div field="sex" width="60" headerAlign="center" allowSort="true">性别</div>
			<div field="challenge_time" width="60" headerAlign="center" allowSort="true">挑战时间</div>
			<div field="challenge_count" width="60" headerAlign="center" allowSort="true">挑战次数</div>
			<div field="rate" width="60" headerAlign="center" allowSort="true">干预率</div>
			<div field="expires" width="60" headerAlign="center" allowSort="true">过期时间</div>
			<div field="time" width="60" headerAlign="center" allowSort="true">时间</div>
        </div>
    </div>

	<div class="description">
        <h3>说明</h3>
        <ul>
			<li>名字为<font color="red">红色</font>表示在线；</li>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
	user_context_column = { uid: 'name', uid2:'name2' };
function module() { return 'challenge_send';}
</script>