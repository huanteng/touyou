<?php require 'head.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(42); ?>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
	
	grid.on("drawcell", function (e) {
	    var record = e.record,
	        column = e.column,
	        field = e.field,
	        value = e.value;
	    
		if (field == "user") {
			e.cellHtml = '<a href="javascript:void();" onclick=\'tab("detail' + record.user+'", "'+record.name+'", "user_detail.php?uid='+record.user+'")\'>'+record.name+'</a>';
		}
		
        if (field == "status") {
	        e.cellHtml = (value == '1') ? '已支付' : '未支付';
	    }
        
	    //if (field == "used") {
	    //    e.cellHtml = (value == '1') ? '已用' : '-';
	    //}
        
        if (field == "target_uid" && record.target_uid == record.user) {
	        e.cellHtml = '';
	    }
	});
}

function search() {
    var q = mini.get("q").getValue();
    var user = mini.get("uid").getValue();
	var npc = mini.get("npc").getValue();
    var props = mini.get("props").getValue();
    var status = mini.get("status").getValue();
	var start = mini.get("start").getFormValue();
    var end = mini.get("end").getFormValue();
    var type = mini.get("type").getValue();
    grid.load({ q: q, status: status, user:user, npc:npc, props: props, start: start, end: end, type: type });
}
function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;" id="toolbar">
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
						<?php autocomplete_name() ;?>
						NPC：<input id="npc" class="mini-combobox" style="width:60px;" textField="name" valueField="id"
    data="[{'id':0,'name':'非NPC'},{'id':1,'name':'NPC'}]" value="" allowInput="false" showNullItem="true" nullItemText="请选择..."/>
    					状态：<input id="status" class="mini-combobox" style="width:60px;" allowInput="false" showNullItem="true" nullItemText="请选择..." data="[{ id: 1, text: '已支付' }, { id: 0, text: '未支付'}]"/>
                        类型：<input id="type" class="mini-combobox" style="width:60px;" textField="name" valueField="id"
    data="[{'id':0,'name':'支付宝'},{'id':1,'name':'手工'},{'id':2,'name':'盛付通'}]" value="" allowInput="false" showNullItem="true" nullItemText="请选择..."/>
                        道具：<input id="props" class="mini-combobox" style="width:80px;" textField="name" valueField="id"
    url="data/props.php?method=drop" value="" allowInput="false" showNullItem="true" nullItemText="请选择..."/>
                        备注：<input id="q" class="mini-textbox" emptyText="请输入关键字" style="width:60px;" onenter="onKeyEnter"/> 
						开始日期：<input id="start" class="mini-datepicker" value="" style="width:100px;"/>
    					结束日期：<input id="end" class="mini-datepicker" value="" style="width:100px;"/>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div field="time" width="50" headerAlign="center" allowSort="true">时间
            </div>
            <div field="no" width="50" headerAlign="center" allowSort="true">商户订单号
            </div>           
            <div field="user" displayField="name" width="50" headerAlign="center" allowSort="true">用户
            </div>
            <div field="money" width="50" headerAlign="center" allowSort="true">金额
            </div>    
        	<div field="status" width="30" headerAlign="center" allowSort="true">状态
            </div>
            <div field="type" displayField="type_name" width="30" headerAlign="center" allowSort="true">类型
            </div>
            <div field="props" displayField="props_name" width="40" headerAlign="center" allowSort="true">道具
            </div>
        	<div field="count" width="30" headerAlign="center" allowSort="true">数量</div>
            <div field="target_uid" displayField="target_name" width="50" headerAlign="center" allowSort="true">送给
            </div>
			<div field="channel" width="30" headerAlign="center" allowSort="false">渠道</div>
			<div field="version" width="30" headerAlign="center" allowSort="false">版本号</div>
            <div field="memo" headerAlign="center" allowSort="true">备注
            </div>	
        </div>
    </div>
	<div>
	奉献：<span id="p_info0"></span> ，成长：<span id="p_info1"></span> ，真人金币：<span id="p_info2"></span>。预计业绩：<span id="p_info3"></span>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'payment';}

var func = function(data){
	for(var i in data)
	{
		$('#p_info'+i).html(data[i]);
		
	}
	if( data[1].replace('%','') > 0 ) $('#p_info1').attr('style','color:red;');
	else $('#p_info1').attr('style','color:green;');
}
post( "payment", "info", {}, func, function(){} );
</script>