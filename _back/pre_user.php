<?php
require 'top.php';

check_privilege(0);
check_privilege(118);
?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function search()
{
	search_by_id( { q: 'q' } );
}
function onKeyEnter(e) {
    search();
}
////// 在具体页面按需覆盖，结束
</script>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true" contextMenu="#gridMenu"
    >
        <div width="10" property="columns">
			<div width="10" type="checkcolumn"></div>
            <div field="id" width="20" headerAlign="center" allowSort="true">id</div>
    		<div field="status" width="30" headerAlign="center" allowSort="true">状态</div>
			<div field="name" width="30" headerAlign="center" allowSort="true">用户名</div>
			<div field="mood" width="60">心情</div>
			<div field="province" width="30" headerAlign="center" allowSort="true">省份</div>
			<div field="city" width="30" headerAlign="center" allowSort="true">城市</div>
			<div field="birthday" width="30" headerAlign="center" allowSort="true">生日</div>
			<div field="sex" width="30" headerAlign="center" allowSort="true">性别</div>
			<div field="height" width="30" headerAlign="center" allowSort="true">身高</div>
			<div field="interest" width="30" headerAlign="center" allowSort="true">爱好</div>
			<div field="profession" width="30" headerAlign="center" allowSort="true">职业</div>
			<div field="nick" width="30" headerAlign="center" allowSort="true">昵称</div>
			<div field="star" width="30" headerAlign="center" allowSort="true">星座</div>
			<div field="degree" width="30" headerAlign="center" allowSort="true">学历</div>
			<div field="income" width="30" headerAlign="center" allowSort="true">收入</div>
			<div field="marriage" width="30" headerAlign="center" allowSort="true">婚恋</div>
		</div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
user_context_column = { uid: 'name' };
function module() { return 'pre_user';}
</script>