<?php require 'head.php'; ?>
<?php check_privilege(0); ?>

<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search&sender=<?php echo $_GET['sender'];?>'+'&receiver='+<?php echo $_GET['receiver'];?>);
	grid.load();
}

function search() {
    var sender = mini.get("sender").getValue();
    var receiver = mini.get("receiver").getValue();
    grid.load({ sender: sender, receiver: receiver });
}
function onKeyEnter(e) {
    search();
}


function del() {
    var rows = grid.getSelecteds();
    if (rows.length > 0) {
        grid.removeRows(rows, true);
    }
}
function save() {
    var data = grid.getChanges();
    var value = mini.encode(data);

    grid.loading("保存中，请稍候...");
	post( module(), "save", value, function (text) {
			grid.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
		);
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束
</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
        <?php // 默认通用，如不合适，自行用js改写html() ?>
                    <td style="width:100%;" id="toolbar">
                        <a class="mini-button" iconCls="icon-remove" onclick="del()" plain="true">删除</a>
                        <span class="separator"></span>
                        <a class="mini-button" iconCls="icon-save" onclick="save()" plain="true">保存</a>
					    <span class="separator"></span>
					    <a class="mini-button" iconCls="icon-reload" onclick="reload()" plain="true">刷新</a>
                    </td>
                    <td style="white-space:nowrap;" id="search">
                        <!--<select id="sender_npc">
    						<option>发送人身份</option><option value="0">非NPC</option><option value="1">NPC</option>
    					</select>-->
    					发送人：<input id="sender" class="mini-buttonedit" emptyText="发送人名字" onbuttonclick="onButtonEdit"/>
    					<!--<select id="receiver_npc">
    						<option>接收人身份</option><option value="0">非NPC</option><option value="1">NPC</option>
    					</select>-->
    					接收人：<input id="receiver" class="mini-buttonedit" emptyText="接收人名字" onbuttonclick="onButtonEdit2"/>	 
    					<!--<input id="date1" class="mini-datepicker" value="<?php echo date('Y-m-d');?>" />  -->
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>


    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:60%;" 
        idField="id"
        allowResize="true" pageSize="10" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div type="checkcolumn"></div>
            <div field="id" width="30" headerAlign="center" allowSort="true">id
            </div>
 
            <div field="sender_name" width="120" headerAlign="center" allowSort="true">发送人
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>                      
            <div field="receiver_name" width="120" headerAlign="center" allowSort="true">接收人
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
            <div field="content" headerAlign="center" allowSort="true">内容
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>	
            <div field="time" width="120" headerAlign="center" allowSort="true">时间
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
            	
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'message';}

function onButtonEdit(e) {
    var btnEdit = this;

    mini.open({
        url: bootPATH + "../sel_user.php",                          
        title: "用户列表",
        width: 650,
        height: 380,
        ondestroy: function (action) {
          
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);
                
                btnEdit.setValue(data.id);
                btnEdit.setText(data.text);
            }
        }
    });            
     
}

function onButtonEdit2(e) {
    var btnEdit = this;

    mini.open({
        url: bootPATH + "../sel_user.php",                          
        title: "用户列表",
        width: 650,
        height: 380,
        ondestroy: function (action) {
          
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);
                
                btnEdit.setValue(data.id);
                btnEdit.setText(data.text);
            }
        }
    });            
     
}        
        
</script>