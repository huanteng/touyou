<?php require 'top.php'; ?>
<?php check_privilege(0); ?>
<?php check_privilege(90); ?>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"       
        showEmptyText="true"    
    >
        <div property="columns">
            <div width="10" type="checkcolumn"></div>
            <div field="id" width="10" headerAlign="center" allowSort="true">id
            </div>
            <div field="name" width="30" headerAlign="center" allowSort="true">名字
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="props_id" displayField="props" width="30" headerAlign="center" allowSort="true">道具
                <input property="editor" class="mini-combobox" style="width:100%;" textField="name" valueField="id" url="data/props.php?method=drop"/>
            </div>
            <div field="props_count" width="30" headerAlign="center" allowSort="true">数量
                <input property="editor" class="mini-textbox" style="width:100%;" />
            </div>
        	<div field="rule" displayField="rule_name" width="120" headerAlign="center" allowSort="true">抽奖规则
                <input property="editor" class="mini-combobox" style="width:100%;" textField="text" valueField="id" data="[{'id':0,'text':'特殊规则才可抽'},{'id':1,'text':'正常'}]" />
            </div>	
        </div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
        </ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'event_gift';}
</script>