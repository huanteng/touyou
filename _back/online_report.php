<?php
require 'head.php';
require 'user_context.php';

check_privilege(0);
check_privilege(61);
?>
<script type="text/javascript">
// 全局变量，在具体页面再覆盖
var grid;

$(function(){
    mini.parse();
	init();
});

////// 在具体页面按需覆盖
function init()
{
	grid = mini.get("datagrid1");
	grid.setUrl('data/' + module() +'.php?method=search');
	grid.load();
}

function search() {
	var data = {};

	data[ 'begin' ] = mini.get( 'begin' ).getFormValue();
	data[ 'end' ] = mini.get( 'end' ).getFormValue();

	grid.load( data );
}

function onKeyEnter(e) {
    search();
}

function reload() {
	grid.reload();
}
////// 在具体页面按需覆盖，结束

</script>
    <div style="width:98%;">
        <div class="mini-toolbar" style="border-bottom:0;padding:0px;">
            <table style="width:100%;">
                <tr>
                    <td style="white-space:nowrap;" id="search">
    					开始日期：<div class="mini-datepicker" id="begin" showTime="false"></div>
						结束日期：<a class="mini-datepicker" id="end" showTime="false"></a>
                        <a class="mini-button" onclick="search()">查询</a>
                    </td>
                </tr>
            </table>           
        </div>
    </div>

    <div id="datagrid1" class="mini-datagrid" style="width:98%;height:80%;" 
        idField="id"
        allowResize="true" pageSize="20" allowAlternating="true"
        allowCellEdit="true" allowCellSelect="true" multiSelect="true"
    >

        <div property="columns">
            <div field="time" width="25" headerAlign="center" allowSort="true" align="center">时间</div>
        	<div field="npc" width="10" headerAlign="center" allowSort="true" align="center">NPC</div>
			<div field="real" width="20" headerAlign="center" allowSort="true" align="center">非NPC</div>
			<div width="200" headerAlign="center"></div>
		</div>
    </div>
  	  
    <div class="description">
        <h3>说明</h3>
        <ul>
		</ul>
    </div>

<?php require 'bottom.php'; ?>

<script type="text/javascript">
function module() { return 'online_report';}
</script>