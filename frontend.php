<?php
require dirname( __FILE__ ) . '/library/controller.php';

class frontend extends controller
{
	function login_uid()
	{
		$cookie = load('cookie');
		$account = $cookie -> get('account', true);
		$account = unserialize($account);
		return value( $account, 'uid', 0 );
	}

	/**检查是否登录，如无则ajax返回提醒。如有，返回uid。（逻辑上不太合理，但可节省代码。）
	 * 参数：
	 * @param bool $ajax：true（默认）使用ajax式返回，false，页面式跳转返回
	 * @return string
	 */
	function check_login( $ajax = true )
	{
		//发布推介
		$uid = $this->login_uid();

		if( $uid == 0 )
		{
			if( $ajax )
			{
				return $this->ajax_out( -100, '您还没有登录' );
			}
			else
			{
				$this->jump('请先登录','/login.php');
				die;
			}
		}

		return $uid;
	}

	/**检查sign是否正确，如无则结束。（逻辑上不太合理，但可节省代码。）
	 * @param $in：
	 */
	function check_sign( $in )
	{
		$site = biz( 'site' );

		$check = $site->check_sign( $in );

		if( $check[ 'code' ] < 0 )
		{
			$site->log( 0, 0, $check[ 'memo' ], array( 'data' => $in ) );
			echo $check[ 'memo' ];
			die;
		}
	}

	function jump( $text='请不要乱打链接访问哦', $url='/' )
	{
		$this->prompt( array('content'=>$text), $url );
		exit();
	}

	/*
	 * 输出
	 */
	function out($data = array(), $tpl = '') 
	{
		$template = load('template');
		//$template->path = dirname(__FILE__) . '/template2/';
		$template->path = config('dir.template');
		$template->appoint($data);
		if ($tpl == '') {
			$table = $this->table();
			$method = $this->input['method'];
			if ($method != 'home')
				$table .= '_' . $method;
		}else {
			$table = $tpl;
		}
		return $template->parse($table . '.php');
	}

}
?>