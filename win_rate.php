<?php
	$type = isset( $_GET['type'] ) && ( $_GET['type'] == '1' || $_GET['type'] == '2' || $_GET['type'] == '3' || $_GET['type'] == '4' || $_GET['type'] == '5' ) ? $_GET['type'] : 0;
	$uid = isset( $_GET['id'] ) && is_numeric( $_GET['id'] ) ? $_GET['id'] : 0;
	( $type && $uid ) or die( 'argument error' );

	require './library/config.php';

	$db = load('database');
	$result = $db->select( 'select * from user where id = ' . $uid );

	if ( isset( $result['win_rate7'] ) )
	{
		$result['win_rate7'] = $result['win_rate7'] < 0 ? 0 : $result['win_rate7'] * 100;
		$result['total_win'] = $result['w7'] + $result['w27'];
		$result['total_lost'] = $result['l7'] + $result['l27'];
		$result['total_match'] = $result['t7'];
		$result['basketball_win_rate7'] = $result['basketball_win_rate7'] < 0 ? 0 : $result['basketball_win_rate7'] * 100;
		$result['basketball_total_win'] = $result['basketball_w7'];
		$result['basketball_total_lost'] = $result['basketball_l7'];
		$result['basketball_total_match'] = $result['basketball_t7'];
	}
	else
	{
		$result['win_rate7'] = 0;
		$result['basketball_win_rate7'] = 0;
		$result['w7'] = 0;
		$result['d7'] = 0;
		$result['l7'] = 0;
		$result['w27'] = 0;
		$result['d27'] = 0;
		$result['l27'] = 0;
		$result['total_win'] = 0;
		$result['total_lost'] = 0;
		$result['total_match'] = 0;
		$result['basketball_total_win'] = 0;
		$result['basketball_total_lost'] = 0;
		$result['basketball_total_match'] = 0;
	}

	if ( $type == '1' )
	{
		if ( $result['win_rate7'] > $result['basketball_win_rate7'] )
		{
			if ( ( $result['w7'] + $result['d7'] + $result['l7'] + $result['w27'] + $result['l27'] ) > 0 )
			{
				$js = '<div title="近7天胜率：' . $result['win_rate7'] . '% 赢' . $result['w7'] . ' 赢半' . $result['w27'] . ' 平' . $result['d7'] . ' 输半' . $result['l27'] . ' 输' . $result['l7'] . '" style="font-weight:normal;"><a href="u' . $uid . '">胜率：<span>' . $result['win_rate7'] . '%</span>&nbsp;&nbsp;赢<span style="color:#FF0000;">' . $result['total_win'] . '</span> 平<span style="color:#008F00;">' . $result['d7'] . '</span> 输<span style="color:#005CB2;">' . $result['total_lost'] . '</span> 查看>></a></div>';
			}
			else
			{
				$js = '<div><span class="gray">近7天没有发表推介</span></div>';
			}
		}
		else
		{
			if ( ( $result['basketball_w7'] + $result['basketball_d7'] + $result['basketball_l7'] ) > 0 )
			{
				$js = '<div title="近7天胜率：' . $result['basketball_win_rate7'] . '% 赢' . $result['basketball_w7'] . ' 平' . $result['basketball_d7'] . ' 输' . $result['basketball_l7'] . '" style="font-weight:normal;"><a href="u' . $uid . '">胜率：<span>' . $result['basketball_win_rate7'] . '%</span>&nbsp;&nbsp;赢<span style="color:#FF0000;">' . $result['basketball_total_win'] . '</span> 平<span style="color:#008F00;">' . $result['basketball_d7'] . '</span> 输<span style="color:#005CB2;">' . $result['basketball_total_lost'] . '</span> 查看>></a></div>';
			}
			else
			{
				$js = '<div><span class="gray">近7天没有发表推介</span></div>';
			}
		}
	}
	else if ( $type == '2' )
	{
		$js = '<div title="近7天胜率：' . $result['win_rate7'] . '% 赢' . $result['w7'] . ' 赢半' . $result['w27'] . ' 平' . $result['d7'] . ' 输半' . $result['l27'] . ' 输' . $result['l7'] . '" style="font-size:10px;text-align:center;">' . ( $result['total_match'] > 0 ? '胜率' . $result['win_rate7'] . '%' : '---' ) . '</div>';
	}
	else if ( $type == '3' )
	{
		if ( $result['total_match'] > 0 )
		{
			$js = '<a title="近7天胜率：' . $result['win_rate7'] . '% 赢' . $result['w7'] . ' 赢半' . $result['w27'] . ' 平' . $result['d7'] . ' 输半' . $result['l27'] . ' 输' . $result['l7'] . '" style="font-size:10px;" href="u' . $uid . '">' . '胜率' . $result['win_rate7'] . '%</a>';
		}
		else
		{
			$js = '<a title="近7天没有发表推介" style="font-size:10px;" href="u' . $uid . '">---</a>';
		}
	}
	else if ( $type == '4' )//足球
	{
		if ( $result['total_match'] > 0 )
		{
			$result['win_rate3'] = $result['win_rate3'] < 0 ? 0 : $result['win_rate3'] * 100;
			$result['win_rate15'] = $result['win_rate15'] < 0 ? 0 : $result['win_rate15'] * 100;
			$result['win_rate30'] = $result['win_rate30'] < 0 ? 0 : $result['win_rate30'] * 100;

			$js = '<div style="font-size:12px;" class="rate_popup">&nbsp;&nbsp;当前连胜：<span class="t_c3">' . $result['now_keepwin'] . '</span> 历史最大连胜：<span class="t_c3">' . $result['history_keepwin'] . '</span><hr>&nbsp;&nbsp;&nbsp;3天战绩：赢<span class="t_c3">' . $result['w3'] . ' </span>赢半<span class="t_c3">' . $result['w23'] . ' </span>平<span class="t_c6">' . $result['d3'] . ' </span>输半<span class="t_c5">' . $result['l23'] . ' </span>输<span class="t_c5">' . $result['l3'] . ' </span>胜率<span class="t_c3">' . $result['win_rate3'] . '%</span><br/>&nbsp;&nbsp;&nbsp;7天战绩：赢<span class="t_c3">' . $result['w7'] . ' </span>赢半<span class="t_c3">' . $result['w27'] . ' </span>平<span class="t_c6">' . $result['d7'] . ' </span>输半<span class="t_c5">' . $result['l27'] . ' </span>输<span class="t_c5">' . $result['l7'] . ' </span>胜率<span class="t_c3">' . $result['win_rate7'] . '%</span><br/>&nbsp;15天战绩：赢<span class="t_c3">' . $result['w15'] . ' </span>赢半<span class="t_c3">' . $result['w215'] . ' </span>平<span class="t_c6">' . $result['d15'] . ' </span>输半<span class="t_c5">' . $result['l215'] . ' </span>输<span class="t_c5">' . $result['l15'] . ' </span>胜率<span class="t_c3">' . $result['win_rate15'] . '%</span><br/>&nbsp;30天战绩：赢<span class="t_c3">' . $result['w30'] . ' </span>赢半<span class="t_c3">' . $result['w230'] . ' </span>平<span class="t_c6">' . $result['d30'] . ' </span>输半<span class="t_c5">' . $result['l230'] . ' </span>输<span class="t_c5">' . $result['l30'] . ' </span>胜率<span class="t_c3">' . $result['win_rate30'] . '%</span></div>';
		}
		else
		{
			$js = '<a title="近7天没有发表推介" style="font-size:10px;" href="u' . $uid . '">---</a>';
		}

		echo $js;
		die;//ajax
	}
	else if ( $type == '5' )//篮球
	{
		if ( $result['basketball_t7'] > 0 )
		{
			$result['win_rate3'] = $result['basketball_win_rate3'] < 0 ? 0 : $result['basketball_win_rate3'] * 100;
			$result['win_rate7'] = $result['basketball_win_rate7'] < 0 ? 0 : $result['basketball_win_rate7'];
			$result['win_rate15'] = $result['basketball_win_rate15'] < 0 ? 0 : $result['basketball_win_rate15'] * 100;
			$result['win_rate30'] = $result['basketball_win_rate30'] < 0 ? 0 : $result['basketball_win_rate30'] * 100;

			$js = '<div style="font-size:12px;" class="rate_popup">&nbsp;&nbsp;当前连胜：<span class="t_c3">' . $result['basketball_now_keepwin'] . '</span> 历史最大连胜：<span class="t_c3">' . $result['basketball_history_keepwin'] . '</span><hr>&nbsp;&nbsp;&nbsp;3天战绩：赢<span class="t_c3">' . $result['basketball_w3'] . ' </span> </span>平<span class="t_c6">' . $result['basketball_d3'] . ' </span> </span>输<span class="t_c5">' . $result['basketball_l3'] . ' </span>胜率<span class="t_c3">' . $result['win_rate3'] . '%</span><br/>&nbsp;&nbsp;&nbsp;7天战绩：赢<span class="t_c3">' . $result['basketball_w7'] . ' </span> </span>平<span class="t_c6">' . $result['basketball_d7'] . ' </span> </span>输<span class="t_c5">' . $result['basketball_l7'] . ' </span>胜率<span class="t_c3">' . $result['win_rate7'] . '%</span><br/>&nbsp;15天战绩：赢<span class="t_c3">' . $result['basketball_w15'] . ' </span> </span>平<span class="t_c6">' . $result['basketball_d15'] . ' </span> </span>输<span class="t_c5">' . $result['basketball_l15'] . ' </span>胜率<span class="t_c3">' . $result['win_rate15'] . '%</span><br/>&nbsp;30天战绩：赢<span class="t_c3">' . $result['basketball_w30'] . ' </span> </span>平<span class="t_c6">' . $result['basketball_d30'] . ' </span> </span>输<span class="t_c5">' . $result['basketball_l30'] . ' </span>胜率<span class="t_c3">' . $result['win_rate30'] . '%</span></div>';
		}
		else
		{
			$js = '<a title="近7天没有发表推介" style="font-size:10px;" href="u' . $uid . '">---</a>';
		}

		echo $js;
		die;//ajax
	}
?>
document.write( '<?php echo $js; ?>' );