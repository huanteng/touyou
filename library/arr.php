<?php
//一维数组
class arr
{
	/*返回新数组，元素为原数组的随机一部分
     * $arr:原数组
     * $keep_count：保留的数量
	 */
	function part( $arr, $keep_count )
	{
        $key = array_keys( $arr );
        shuffle( $key );
        $count = count( $arr ) - $keep_count;
        
		for( $i = 0; $i < $count; ++$i )
		{
			unset( $arr[ array_pop( $key ) ] );
		}
		return $arr;
	}
	
	// <editor-fold defaultstate="collapsed" desc="make_int，返回一个数组，元素由整数组成">
	function make_int( $min, $max )
	{
		$arr = array();
		for( $i = $min; $i <= $max; ++$i ) $arr[] = $i;
		return $arr;
	}
	// </editor-fold>
 
	// <editor-fold defaultstate="collapsed" desc="make_rand，返回一个数组，元素由随机整数组成">
	/* 
	 * 参数：length，返回数组的长度
	 *	    min, max，每个元素下上限
	 */	
	function make_rand( $length, $min, $max )
	{
		$arr = array();
		for( $i = 0; $i < $length; ++$i ) $arr[] = mt_rand($min, $max);
		return $arr;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="del，删除不要的元素，返回新数组">
	function del( $source, $no )
	{
		foreach( $source as $k => $v )
		{
			if( in_array( $v, $no ) ) unset( $source[$k] );
		}
		return $source;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="keep，仅保留必要的元素，返回新数组">
	function keep( $source, $yes )
	{
		$out = array();
		foreach( $yes as $k )
		{
			if( isset($source[$k]) )
			{
				$out[$k] = $source[$k];
			}
		}
		return $out;
	}
	// </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="json_decode，将记录集的其中一列json_decode">
	/* 参数：
	 *	data
	 *	key
	 * 返回：
	 *	转换后后记录集
	 */
	function json_decode( $data, $key )
	{
		foreach( $data as $k => $v )
		{
			if( $v[ $key ] == '' ) continue;
			$data[ $k ][ $key ] = json_decode( $v[ $key ] );
		}
		return $data;
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="rename_key，将其中一列改名">
	/* 参数：
	 *	data，数组
	 *	old：旧key名
	 *	new：新key名
	 */
	function rename_key( $data, $old, $new )
	{
		foreach( $data as $k => $v )
		{
			$v[ $new ] = $v[ $old ];
			unset( $v[ $old ] );
			$data[$k] = $v;
		}
		return $data;
	}
	// </editor-fold>

	/* 如果不存在，则设置默认值
	 * 参数：
	 *	in：原数组
	 *	default：默认值数组
	 * 返回值：
	 *	新数组，如果原数组中没对应值，则以对应的默认值填充
	 */
	function set_default( $in, $default )
	{
		foreach( $default as $k => $v )
		{
			if( !isset($in[$k]) )
			{
				$in[$k] = $v;
			}
		}
		return $in;
	}
	
	/*
	 * 打乱数组
	 */
	function shuffle_array( $old )
	{
		$new = array();

		while( count( $old ) > 0 )
		{
			$key = array_rand( $old );
			$new[] = $old[$key];
			unset( $old[$key] );
		}

		return $new;
	}

	/** 对字段做html编码。（注：当字段带有双引号、<>等特殊符号时，编辑功能必须事先做html编码，否则无法正常显示）
	 * @param $source：源记录集
	 * @param $field：字段
	 * 	方式一：只改单个字段，可以只传入单个字段的字符串，如 html_encode( $source, 'data' )
	 * 	方式二：改多个字段，则传入字段集
	 * 		如：html_encode( $source, array( 'data1', 'data2' ) )
	 * @return new dataset
	 */
	function html_encode( $source, $field )
	{
		if( !is_array( $field ) )
		{
			$field = array( $field );
		}

		foreach( $source as $k => $v )
		{
			$source[ $k ] = htmlspecialchars( $v );
		}

		return $source;
	}

	/** 如果属性不存在，则设置。（如存在则忽略）
	 * @param $source：原数组
	 * @param $key：属性名
	 * @param $value：属性值
	 * @return 新数组
	 */
	function set_if_empty( $source, $key, $value )
	{
		if( !isset( $source[ $key ] ) )
		{
			$source[ $key ] = $value;
		}
		return $source;
	}

	/** 如果属性为某值，则unset
	 * @param $source：原数组
	 * @param $key：属性名
	 * @param $value：属性值
	 * @return 新数组
	 */
	function unset_if( $source, $key, $value )
	{
		if( isset( $source[ $key ] ) && $source[ $key ] == $value )
		{
			unset( $source[ $key ] );
		}
		return $source;
	}

	// 对给定二维数组按照指定的键值进行排序
	function sort($arr,$keys,$type='asc'){
		$keysvalue = $new_array = array();
		foreach ($arr as $k=>$v){
			$keysvalue[$k] = $v[$keys];
		}
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		reset($keysvalue);
		foreach ($keysvalue as $k=>$v){
			$new_array[$k] = $arr[$k];
		}
		return $new_array;
	}

	/** 对 source 进行分组，每 group 一组，组内的数据打乱。
	 * @param $source：打乱前的数组
	 * @param $group：数字，每组多少个？最后一组可能少于它
	 * @return mixed：打乱后的数组
	 */
	function group_sort( $source, $group )
	{
		$key = array_keys( $source );
		$key_count = count( $key );

		// 进一取整、舍弃取整
		$more = ceil( $key_count / $group );
		$less = floor( $key_count / $group );

		// 乱序数组长度及具体数组
		$len = $group;
		$rand = $this->make_int( 0, $len - 1 );

		$out = array();
		for( $i = 0; $i < $more; ++$i )
		{
			// 乱序数组个数，平时是group个，最后一批是余数
			if( $i == $more - 1 && $more > $less )
			{
				$len = $key_count - $less * $group;
				$rand = $this->make_int( 0, $len - 1 );
			}

			$rand = $this->shuffle_array( $rand );
			for( $j = 0; $j < $len; ++$j )
			{
				// 当前元素名
				$k = $key[ $i * $group + $rand[ $j ] ];
				$out[ $k ] = $source[ $k ];
			}
		}

		return $out;
	}

	/**
	 * 根据value是1或0，分别显示是或否。为简便操作
	 * 返回值：
	 * 	不带参数时，返回{ 0:否，1：是}
	 *  带参数时，返回是或否
	 */
	function yes_no_dict( $value = -1 )
	{
		$data = array( 1 => '是', 0 => '否' );
		if( $value == -1 )
		{
			return $data;
		}
		else
		{
			return $value == 1 ? '是' : '否';
		}
	}

	/** 将数组value串接起来，返回字符串。（注：本方法和base::implode2类似，但可设置前后辍不相同
	 * @param $data：数组
	 * @param string $pre：每个value前辍
	 * @param string $end：每个value后辍
	 * @param string $join：value之间用什么串接
	 * @return string：
	 */
	function implode( $data, $pre = "'",  $end = "'", $join = ',')
	{
		$s = '';
		foreach( $data as $v )
		{
			if( is_array( $v ) )
			{
				$s .= $this->implode( $v, $pre, $end, $join );
			}else
			{
				$s .= $pre . $v . $end . $join;
			}
		}
		if( $s != '' && $join != '' )
		{
			$s = substr($s, 0, -strlen( $join ) );
		}

		return $s;
	}
	
	/** 过滤子value里面的html代码
	 * @param $data：数组
	 * @return 过滤后的新数组
	 */
	function strip_tags( $data )
	{
		foreach( $data as &$v )
		{
			$v = is_array( $v ) ? $this->strip_tags( $v ) : strip_tags( $v );
		}

		return $data;
	}

	/** 将子value前后的空格过滤
	 * @param $data：数组
	 * @return 过滤后的新数组
	 */
	function trim( $data )
	{
		foreach( $data as &$v )
		{
			$v = is_array( $v ) ? $this->trim( $v ) : trim( $v );
		}

		return $data;
	}

	/** // 注：不知为何要在此加addslashes，但如果不加的话，当json含有单引号时，会出现错误
	 * @param $json
	 * @return string
	 */
	function php_json_encode( $json )
	{
		return addslashes( json_encode( $json ) );
	}

	/** 往数组的某个key追回元素。（如果该key不是数组，事先定义为数组）
	 * @param $source
	 * @param $key
	 * @param $value
	 * @return mixed
	 */
	function append( $source, $key, $value )
	{
		if( !isset( $source[ $key ] ) )
		{
			$source[ $key ] = array();
		}
		$source[ $key ][] = $value;
		return $source;
	}

	/** 删除数组中值为 $value 的元素。（可能0个、1个、多个）
	 * @param $source
	 * @param $value
	 * @return 新数组
	 */
	function del_value( $source, $value )
	{
		foreach( $source as $k => $v )
		{
			if( $v == $value )
			{
				unset( $source[ $k ] );
			}
		}

		return $source;
	}
}
?>