<?php
class time
{
	// <editor-fold defaultstate="collapsed" desc="in，判断是否处于两个时间中">
	/* 参数：
	 *	s_start_time：开始时间，字符串形式，如 '2012-11-10 0:00:01'
	 *	s_end_time：结束时间，字符串形式
	 * 返回值：true|false
	 */
	function in( $s_start_time, $s_end_time )
	{
		$now = time();

		return $now >= strtotime( $s_start_time ) && $now < strtotime( $s_end_time );
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="before，检测当前时间是否早于参数">
	/* 参数：
	 *	time：时间，字符串形式，如 '2012-11-10 0:00:01'
	 * 返回值：true|false
	 */
	function before( $time )
	{
		return time() <= strtotime( $time );
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="after，检测当前时间是否晚于参数">
	/* 参数：
	 *	time：时间，字符串形式，如 '2012-11-10 0:00:01'
	 * 返回值：true|false
	 */
	function after( $time )
	{
		return time() >= strtotime( $time );
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="format，格式化时间">
	function format( $timestamp )
	{
		$today = $this->today();

		$s = '';
		if( $timestamp >= $today + 86400 )
		{
			$s = 'm-d H:i';
		}
		elseif( $timestamp >= $today )
		{
			$s = 'H:i';
		}
		elseif( $timestamp >= ( $today - 86400 ) )
		{
			$s = '昨天 H:i';
		}
		elseif( $timestamp >= ( strtotime( date( 'Y-1-1' ) ) ) )
		{
			$s = 'm-d H:i';
		}
		else
		{
			$s = 'Y-m-d H:i';
		}

		return date( $s, $timestamp );
	}
	// </editor-fold>

	/* 返回当天0：00的时间值
	 *
	 */
	function today()
	{
		return strtotime(date('Y-m-d 0:00:00',time()));
	}

	/* 返回第二天0:00的时间值
	 *
	 */
	function tomorrow()
	{
		return $this->today() + 86400;
	}

	/*
	 * 字符串转换为时间值
	 * 参数：
	 * 	str：字符串，形式为：YYYY-MM-DD HH:NN:SS
	 * 返回值：
	 * 	timestamp形式
	 */
	function str2time( $str )
	{
		return strtotime( $str );
	}

	/*
	 * 时间值转换为字符串
	 * 参数：
	 * 	timestamp
	 * 返回值：
	 * 	YYYY-MM-DD HH:NN:SS
	 */
	function time2str( $timestamp )
	{
		return date('Y-m-d H:i:s', $timestamp );
	}

	/*
	 * 返回某天所在周的星期日0:00的时间值
	 * 参数：
	 * 	date：日期，YYYY-MM-DD形式，默认为当天
	 */
	function get_sunday( $date = '' )
	{
		$today = $this->today();
		if( $date == '' )
		{
			$time = $today;
		}
		else
		{
			$time = strtotime( $date );
		}

		// 星期中的第几天，数字表示 0（星期天）到 6（星期六）
		$n = date( 'w', $time );

		return $time - 86400 * $n;
	}
}
?>