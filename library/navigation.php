<?php
	class navigation
	{
		var	$total = 0;
		var	$unit = 0;
		var	$page = 1;
		var	$current = 1;
		var	$separator = '&';
		var	$name = 'page';
		var	$url = '';
		var	$link_argument = '';
		var	$first_link = '#';
		var	$last_link = '#';
		var	$prev_link = '#';
		var	$next_link = '#';
		var $jump = '';
		var $number_size = 5;
		var $number = array();
		var $data = array();

		function compute( $total, $unit, $url = '', $number_size = 10, $data = '', $name = 'page' )
		{
			$this->data = $data != '' ? $data : $_GET;
			$this->unit = $unit;
			$this->total = $total;
			$this->name = $name;
			$this->number_size = $number_size;
			$this->url = $url != '' ? $url : $_SERVER['PHP_SELF'];
			$method = array( 'set_base', 'set_link_arg', 'set_first', 'set_last', 'set_prev', 'set_next', 'set_number' );
			foreach( $method as $function ) $this->$function();
			return array( 'total' => $this->total, 'size' => $this->unit, 'page' => $this->page, 'current' => $this->current, 'link_argument' => $this->link_argument, 'first_link' => $this->first_link, 'last_link' => $this->last_link, 'prev_link' => $this->prev_link, 'next_link' => $this->next_link, 'number' => $this->number );
		}

		function set_base()
		{
			$this->page	= ceil( $this->total / $this->unit );
			$this->data[$this->name] = isset( $this->data[$this->name] ) ? $this->data[$this->name] : 1;
			$this->data[$this->name] = ( $this->data[$this->name] <	1 )	? 1	: $this->data[$this->name];
			$this->current = isset( $this->data[$this->name] ) && is_numeric( $this->data[$this->name] ) ? $this->data[$this->name] : 1;
			$this->current = ( $this->current > $this->page ) ? $this->page : $this->current;
		}

		function set_first()
		{
			$this->first_link = $this->current == 1 ? '#' : $this->link_argument . $this->name . '=1';
		}

		function set_last()
		{
			$this->last_link = $this->current == $this->page ? '#' : $this->link_argument . $this->name . '=' . $this->page;
		}

		function set_prev()
		{
			$this->prev_link = $this->current == 1 ? '#' : $this->link_argument . $this->name . '=' . ( $this->current -1 );
		}

		function set_next()
		{
			$this->next_link = $this->current == $this->page ? '#' :	$this->link_argument . $this->name .	'='	. (	$this->current + 1 );
		}

		function set_jump()
		{
			$this->jump = "<input type=\"text\" size=\"1\" onkeydown=\"if ( event.keyCode == 13 ) window.location.replace('" . $this->link_argument . $this->name . "='+this.value);\">";
			return $this->jump;
		}

		function set_quick()
		{
			$this->jump = '<select name="' . $this->name . "\" onchange=\"window.location.href='" . $this->link_argument . $this->name . "='+this.value;\">";
			for( $count = 1; $count <= $this->page; $count++ )	$this->jump .= '<option value="' . $count . '"' . ( $count == $this->current ? ' selected' : '' ) . '>' . $count . '</option>';
			$this->jump .= '</select>';
			return $this->jump;
		}

		function set_number( $size = 5 )
		{
			$size = $this->number_size;
			$size = $size + 0 == $size ? $size : 5;
			$middle = ceil( $size / 2 );
			$end = $this->current + $middle - 1;
			$end = $end > $this->page ? $this->page : $end;
			$start = $end - $size + 1;
			if ( $start < 1 )
			{
				$end += abs( $start ) + 1;
				$start = 1;
			}
			$end = $end > $this->page ? $this->page : $end;
			$data = array();
			$this->link_argument = preg_replace( '/' . $this->name . '=[0-9]+\?/', '', $this->link_argument );
			for( $count = $start; $count <= $end; $count++ ) $data[] = array( 'name' => $count, 'link' => $this->link_argument . $this->name . '=' . $count, 'is_current' => ( $count == $this->current ? 1 : 0 ) );
			$this->number = $data;
		}

		function set_link_arg()
		{
			$this->link_argument = $this->url . '?';
			unset( $this->data[$this->name] );

			foreach( $this->data as $name => $data )
			{
				if ( is_array( $data ) )
				{
					foreach( $data as $value ) $this->link_argument .= $name . '[]=' . urlencode( $value ) . $this->separator;
				}
				else
				{
					$this->link_argument .= $name . '=' . urlencode( $data ) . $this->separator;
				}
			}
		}
	}
?>