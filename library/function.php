<?php
function biz( $name )
{
	return load( 'biz.' . $name );
}

function config( $name = '' )
{
	static $data = array();

	if( empty( $data ) )
	{
		$data = array(
			'template' => array(),
			'domain' => array(),
			'header' => array(),
			'dir' => array(),
			'cache' => array(),

			'host' => isset( $_SERVER['HTTP_HOST'] ) ? $_SERVER['HTTP_HOST'] : '',
		);

		//domain config
		$temp = explode( '.', strtolower( $data['host'] ) );
		krsort( $temp );$new = array();foreach( $temp as $value ) $new[] = $value;
		$domain = $new[1] == 'com' || $new[1] == 'gov' || $new[1] == 'net' || $new[1] == 'co' || $new[1] == 'me' || $new[1] == 'idv' ? $new[2] . '.' . $new[1] . '.' . $new[0] : $new[1] . '.' . $new[0];

		$data['domain']['root'] = '.' . $domain;

		//base config
		$data['server'] = & $_SERVER;
		$data['cookie'] = & $_COOKIE;
		$data['get'] = & $_GET;
		$data[ 'request' ] = & $_REQUEST;

		// 规范目录
		$temp = pathinfo( dirname( __FILE__ ) . '/' );
		$temp = $temp['dirname'] . '/';

		$data['dir'] = array(
			'root' => $temp,
			'cache' => $temp . 'cache/',
			'library' => $temp . 'library/',
			'js' => $temp . 'js/'
		);

		require_once $data[ 'dir' ][ 'library' ] . 'config.php';

		foreach( $config as $k => $v )
		{
			$data[ $k ] = $v;
		}

		// 终端方式调用
		global $argv;
		if( !empty( $argv ) )
		{
			// 上调内存变量。一些复杂程序如：对阵计算，需要用到很多内存变量
			ini_set('memory_limit','1280M');

			// 约定，终端调用时，参数值只传一个，用双引号括着，如“name=dda&age=20...”
			if( isset( $argv[ 1 ] ) )
			{
				foreach( explode( '&', $argv[ 1 ] ) as $v )
				{
					$line = explode( '=', $v );
					if( $line[ 0 ] != '' )
					{
						$data[ 'get' ][ $line[ 0 ] ] = $line[ 1 ];
					}
				}
			}

			$data[ 'server' ] += array(
				'cmd' => true,
				'HTTP_HOST' => '.',
				'REQUEST_URI' => $data[ 'server' ][ 'SCRIPT_NAME' ],
				'REMOTE_ADDR' => '.',
			);
		}
	}

	return $name == '' ? $data : value( $data, $name, '' );
}

function load( $class, $config = array(), $unique = true )
{
	static $object = array();

	if ( ! isset( $object[$class] ) || ! $unique )
	{
		require_once config( 'dir.library' ) . str_replace( '.', '/', $class ) . '.php';
		$name = explode( '.', $class );
		$size = count( $name );
		$name = $name[$size-1];

		$temp = new $name( $config );

		if ( $name != 'template' ) $object[$class] = & $temp;
	}
	else
	{
		$temp = $object[$class];
	}

	if ( ! is_object( $temp ) ) trigger_error( 'can not load' . $class . ' object.', E_USER_ERROR );
	return $temp;
}

//检查 $key是否存在，如存在则返回，否则返回$default
// 注：key如果包含"."，则表示取子元素
function value($data, $key, $default = '')
{
	if( isset( $data[ $key ] ) )
	{
		return $data[ $key ];
	}

	$temp = explode( '.', $key );

	foreach( $temp as $v )
	{
		if( isset( $data[ $v ] ) )
		{
			$data = $data[ $v ];
		}
		else
		{
			$data = $default;
			break;
		}
	}
	return $data;
}

function timetostr( $time )
{
	return date( 'Y-m-d H:i:s', $time );
}

/** 处理box机制
 * @param $file：文件名，不必带后辍名
 * @param array $data：处理函数中所需用到的变量，可选
 * @param array $option：box处理所需的参数处理，可选，特殊参数如下：
 * 	cachetime：缓存时间，0表示不缓存，默认值为300，单位为秒。（注：当url带有参数cache=new时，所有box都不缓存）
 *	注：当传入的option不是数组时，表示本值为cachetime。这是简写用法
 *
 */
function box( $file, $data = array(), $option = array() )
{
	if( !is_array( $option ) && is_integer( $option ) )
	{
		$option = array( 'cachetime' => $option );
	}
	$cachetime = value( $option, 'cachetime', 300 );

	$no_cache = load('cookie')->get( 'cache' ) == 'new';

	if( $cachetime != 0 )
	{
		$cachename = 'box.' . $file;
		if( count( $data ) > 0 )
		{
			$cachename .= '.' . md5( json_encode( $data ) );
		}

		if( !$no_cache )
		{
			$cachedata = load('data')->get( $cachename, $cachetime );

			if( $cachedata )
			{
				echo $cachedata;
				return;
			}
		}
	}

	include_once 'box.php';

	$action = new action_box();

	if( method_exists( $action, $file ) )
	{
		$data = $action->{$file}( $data );
	}
	else
	{
		$data = array();
	}

	$cachedata = $action->out( $data, 'box_' . $file );

	if( $cachetime != 0 )
	{
		 load('data')->set( $cachename, $cachedata, $cachetime );
	}

	echo $cachedata;
}

/** 带参数时，输出参数并中止，不带参数时，输出sql语句，便于调试
 * @param $s
 */
function d( $s = '' )
{
	if( $s != '' )
	{
		echo '<pre>';
		print_r( $s );
		die;
	}
	load('db')->debug();
}
?>