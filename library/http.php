<?php
class http
{
	function format( $value )
	{
		return addslashes( $value );
	}

	function clean( $array )
	{
		foreach( $array	as &$v )
		{
			$v = is_array( $v ) ? $this->clean( $v ) : $this->format( $v );
		}
		return $array;
	}

	function get_or_post( $url, $post = array(), $referer = '', $cookie = '', $port = 80, $useragent = 'Mozilla/4.0 (compatible; MSIE 5.00; Windows 98)' )
	{
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		//curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 1 );
		curl_setopt( $ch, CURLOPT_PORT, $port );
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_HEADER, 0 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
		curl_setopt( $ch, CURLOPT_USERAGENT, $useragent );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 0 );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 0 );

		if ( $referer != '' ) curl_setopt( $ch, CURLOPT_REFERER, $referer );
		if ( $cookie != '' ) curl_setopt( $ch, CURLOPT_COOKIE, $cookie );

		if ( count( $post ) > 0 )
		{
			$field = '';
			foreach( $post as $key => $value ) $field .= $key . '=' . urlencode( $value ) . '&';
			$field = substr( $field, 0, -1 );
			curl_setopt( $ch, CURLOPT_POST, 1 );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $field );
		}

		$data = curl_exec( $ch );
		curl_close( $ch );
		return $data;
	}

	/** 从远程获得json内容，并转化为json返回，如访问出错或不是json内容，将返回null
	 * @param $url
	 * @return json
	 */
	function get_json( $url )
	{
		$content = $this->get_or_post( $url );
		$data = json_decode( $content, true );

		if( empty( $data ) )
		{
			biz('base')->error( '获取json内容时出错，url：' . $url, $content );
		}

	 	return $data;
	}

	/**
	 * @param $url
	 * @return mix|string
	 */
	function get( $url )
	{
		return file_get_contents( $url );
	}
}
?>