<?php
class string
{
	// <editor-fold defaultstate="collapsed" desc="常规操作">
	function left( $s, $len )
	{
		return substr( $s, 0, $len );
	}

	// </editor-fold>

	/* 类似c#：：format的函数，便于简单做输出
	 * 使用方法：echo format("hello,{0},{1},{2}", 'dda', 'x1', 'x2');
	 *
	 * ****20030406注：本函数好像有问题，会导致payment::finish中执行失败。
	 */
	function format()
	{
		$args = func_get_args();

		if (count($args) == 0) return;
		if (count($args) == 1) return $args[ 0 ];
		$str = array_shift($args);

		$str = preg_replace_callback('/\\{(0|[1-9]\\d*)\\}/', create_function('$match', '$args = '.var_export($args, true).'; return isset($args[$match[1]]) ? $args[$match[1]] : $match[0];'), $str);

		return $str;
	}

	/* 如format类似，但本函数是替换字符串组
	 * 使用方法 echo format_key( 'hello,{name},{do}', array( 'name' => 'dda', 'do' => 'good moring' } );
	 */
	function format_key( $s, $array )
	{
		// 注：本函数可优化，使用正则表达式，可不必循环
		foreach( $array as $k => $v )
		{
			$s = str_replace( '{' . $k . '}', $v, $s );
		}
		return $s;
	}

	/* 将回车分隔的key=value串转为json对象
	 * 一般用在后台编辑，在编辑界面，是=分隔、每行一个key，而在数据库以json形式保存
	 */
	function to_json( $string )
	{
		$arr = explode( "\n", $string );
		$out = array();
		foreach( $arr as $line )
		{
			if( trim( $line) == '' ) continue;
			list($k, $v) = explode( "=", $line );
			$out[ $k ] = $v;
		}
		
		return $out;
	}

	// <editor-fold defaultstate="collapsed" desc="from_json，to_json的反操作">
	/* 返回值：
	 *	字符串，每一行是key=value
	 */
	function from_json( $json )
	{
		$out = array();
		foreach( $json as $k => $v )
		{
			$out[] = $k . '=' . $v;
		}

		return implode( "\n", $out );
	}
	// </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="rand_to_json，将随机设置值，转换为json格式">
	/* 参数：
	 *	string
	 * 返回值：json对象，key为随机值，value为选项组
	 *
	 * 一般用于后台编辑，用于随机抽奖。
	 * 1、每行为一次随机中奖事件，key,value串；key为概率，渐大；value为选项组，多项间用逗号隔开；每个选项中，为id=数量串
	 * 2、数量如果为随机，用-隔开，本函数会随机算出结果返回
	 * 举例：
	 * 78,1=2-8,3=1
	 * 88,1=1
	 * 本函数会返回
	 * Array
		(
			[78] => Array
				(
					[1] => 6
					[3] => 1
				)

			[88] => Array
				(
					[1] => 1
				)

		)


	 */
	function rand_to_json($string)
	{
		// 替换中文符号，以免误写
		$string = str_replace( '，', ',', $string );

		$arr = explode( "\r\n", $string );
		$out = array();
		foreach( $arr as $line )
		{
			if( trim( $line) == '' ) continue;

			$line = explode( ',', $line );
			$k = $line[0];
			$v = array();
			unset( $line[ 0 ] );

			foreach ( $line as $item )
			{
				list($k1, $v1) = explode( "=", $item );
				
				if( strpos( $v1, '-' ) !== FALSE )
				{
					list($k2, $v2) = explode( "-", $v1 );
					$v1 = rand( $k2, $v2 );
				}
				$v[$k1] = $v1;
			}
			$out[ $k ] = $v;
		}

		return $out;
	}
	// </editor-fold>

	/*
	 * 全角转换为半角
	 */
	function make_semiangle($str)
	{
		$str = iconv('utf-8', 'gbk', $str);
		$str = preg_replace('/\xa3([\xa1-\xfe])/e', 'chr(ord(\1)-0x80)', $str);
		$str = iconv( 'gbk', 'utf-8', $str);
		return $str;
	}
	
	/*
	*function：检测字符串是否由纯英文，纯中文，中英文混合组成
	*param string
	*return 1:纯英文;2:纯中文;3:中英文混合
	*/
	function check_str($str='')
	{
		if(trim($str)==''){
			return '';
		}	
		$m=mb_strlen($str,'utf-8');
		$s=strlen($str);
		if($s==$m){
			return 1;
		}
		if($s%$m==0&&$s%3==0){
			return 2;
		}
		return 3;
	}

	function substr($text, $start = 0, $length = 4) 
	{
		if (strlen($text) <= $length * 2)
			return $text;
		else {
			if (preg_match('/^[a-zA-Z0-9_]+$/', $text))
				$length *= 2;
			return mb_substr($text, $start, $length, 'utf-8');
		}
	}

	/** 如果s=comare，则取other
	 * @param $s
	 * @param $compare
	 * @param $other
	 * @return mixed
	 */
	function ifthen( $s, $compare, $other )
	{
		return $s == $compare ? $other : $s;
	}
}
?>