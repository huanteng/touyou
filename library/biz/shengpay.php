﻿<?php
require_once dirname( __FILE__ ) . '/base.php';

class shengpay extends base
{
	var $MsgSender = '146390';
	var $Key = 'huantengtouyou';

	function __construct()
	{
		$host = $_SERVER['HTTP_HOST'];

		// 给对方跳转的url 给浏览器用
		$this->PageUrl = 'http://' . $host . '/payment.php?method=shengpay_page';

		// 给对方post的url 给服务端用
		$this->NotifyUrl = 'http://' . $host . '/payment.php?method=shengpay_finish';
	}

	function post( $in )
	{
		$array=array(
			'Name'=>'B2CPayment',
			'Version'=>'V4.1.1.1.1',
			'Charset'=>'UTF-8',
			'MsgSender' => $this->MsgSender,
			'SendTime'=>date('YmdHis'),
			'OrderTime'=>date('YmdHis'),
			'PayType'=>'',
			'InstCode'=>'',
			'PageUrl'=> $this->PageUrl,
			'NotifyUrl' => $this->NotifyUrl,
			'ProductName'=>'金币',
			'BuyerContact'=>'',
			'BuyerIp'=>'',
			'Ext1'=>'',
			'Ext2'=>'',
			'SignType'=>'MD5',
		);

		$this->init( $array );
		$this->setKey( $this->Key );

		$this->takeOrder( $in[ 'billno' ], $in[ 'money' ] );
	}

	function finish( $in )
	{
		// 参考旧写法，两者无差别对待
		return $this->page( $in );
	}

	/** page跳转检查
	 * @param $in
	 * 返回：json，具体提示信息在memo属性
	 */
	function page( $in )
	{
		$this->setKey( $this->Key );

		if ( $this->returnSign( $in ) )
		{
			$billno = $in[ 'OrderNo' ];
			$money = $in[ 'OrderAmount' ];

			$info = biz( 'payment' )->get1( '*', array( 'billno' => $billno ) );

			//校验通过开始处理订单
			if( $info[ 'money' ] == $money )
			{
				// 注：旧版没加状态判断，这是根据文档而写的。
				$status = $in[ 'TransStatus' ] == '01' ? 1 : 2;

				biz( 'payment' )->finish( $billno, $status );

				return $this->out( 1, 'OK' );
			} else {
				return $this->out( -1, 'Error' );
			}
		}
		else
		{
			$this->log( '', 0, 'MD5校验失败' );

			return $this->out( -2, 'Error' );
		}
	}

	// 以下为原内容
	private $payHost;
	private $debug=false;
	private $params=array(
		'Name'=>'B2CPayment',
		'Version'=>'V4.1.1.1.1',
		'Charset'=>'UTF-8',
		'MsgSender'=>'100894',
		'SendTime'=>'',
		'OrderNo'=>'',
		'OrderAmount'=>'',
		'OrderTime'=>'',
		'PayType'=>'',
		'InstCode'=>'',
		'PageUrl'=>'',
		'NotifyUrl'=>'',
		'ProductName'=>'',
		'BuyerContact'=>'',
		'BuyerIp'=>'',
		'Ext1'=>'',
		'Ext2'=>'',
		'SignType'=>'MD5',
		'SignMsg'=>'',
	);

	/**
	 * old url
	 * sanbox url: $this->payHost='http://mer.mas.sdo.com/web-acquire-channel/cashier.htm';
	 * product url: $this->payHost='http://mas.sdo.com/web-acquire-channel/cashier.htm';
	 *
	 * https://mas.shengpay.com/web-acquire-channel/cashier.htm
	 */
	function init($array=array()){
		if($this->debug)
			$this->payHost='https://mer.mas.shengpay.com/web-acquire-channel/cashier.htm';
		else
			$this->payHost='https://mas.shengpay.com/web-acquire-channel/cashier.htm';
		foreach($array as $key=>$value){
			$this->params[$key]=$value;
		}
	}

	function setKey($key){
		$this->Key=$key;
	}
	function setParam($key,$value){
		$this->params[$key]=$value;
	}

	function takeOrder($oid,$fee){
		$this->params['OrderNo']=$oid;
		$this->params['OrderAmount']=$fee;
		$origin='';
		foreach($this->params as $key=>$value){
			if(!empty($value))
				$origin.=$value;
		}
		$SignMsg=strtoupper(md5($origin.$this->Key));
		$this->params['SignMsg']=$SignMsg;
		$content = '<meta http-equiv = "content-Type" content = "text/html; charset = '.$this->params['Charset'].'"/>
			<form  method="post" action="'.$this->payHost.'" id="dh">';
		foreach($this->params as $key=>$value){
			$content .= '<input type="hidden" name="'.$key.'" value="'.$value.'"/>';
		}

		$develop = config( 'develop' );
		if( $develop )
		{
			$content .= '<input type="submit" name="submit" value="提交到盛付通">';
		}

		$content .= '</form>';

		if( !$develop )
		{
			$content .= '<script>document.getElementById("dh").submit();</script>';
		}

		echo $content;
	}

	function returnSign( $in ){
		$params=array(
			'Name'=>'',
			'Version'=>'',
			'Charset'=>'',
			'TraceNo'=>'',
			'MsgSender'=>'',
			'SendTime'=>'',
			'InstCode'=>'',
			'OrderNo'=>'',
			'OrderAmount'=>'',
			'TransNo'=>'',
			'TransAmount'=>'',
			'TransStatus'=>'',
			'TransType'=>'',
			'TransTime'=>'',
			'MerchantNo'=>'',
			'ErrorCode'=>'',
			'ErrorMsg'=>'',
			'Ext1'=>'',
			'Ext2'=>'',
			'SignType'=>'MD5',
		);
		foreach( $in as $key=>$value){
			if(isset($params[$key])){
				$params[$key]=$value;
			}
		}
		$TransStatus=(int)$in['TransStatus'];
		$origin='';
		foreach($params as $key=>$value){
			if(!empty($value))
				$origin.=$value;
		}
		$SignMsg=strtoupper(md5($origin.$this->Key));
		if($SignMsg==$in['SignMsg'] and $TransStatus==1){
			return true;
		}else{
			return false;
		}
	}



}
?>