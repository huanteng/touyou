<?php
require_once dirname( __FILE__ ) . '/base.php';

class openid extends base
{
	/** 成功登录
	 * @param $type：网站类型
	 * @param $openid：网站对应id，唯一值
	 */
	function login( $type, $openid )
	{
		$info = $this->get1( '*', array( 'openid' => $openid ) );

		if( empty( $info ) )
		{
			$this->register( $type, $openid );
			return;
		}

		$session = $this->hash( $openid . time() );
		$this->set( array( 'id' => $info[ 'id' ], 'session' => $session ) );

		switch( $type )
		{
			case 1:	// qq

				break;

			default:
				$this->error( 'openid::logner时类型错误，type=' . $type . '，openid=' . $openid );
				die( '类型错误' );
		}
	}

}
?>