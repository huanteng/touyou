<?php
require_once dirname( __FILE__ ) . '/base.php';

class payment extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,created,uid,money,billno,type,memo,site,have_notice,product,status,remark';
	}
	function status_dict( $i = '' )
	{
		$data = array(
			0 => '未确认',
			1 => '成功',
			2 => '失败',
		);
		return $this->dict( $data, $i, '错误', '未定义payment::status' );
	}
    
    function type_dict( $i = '' )
	{
		$data = array(
			7 => '盛付通',
		);
		return $this->dict( $data, $i, '错误', '未定义payment::type' );
	}

	/** 增加订单，并返回id
	 *
	 */
	function create( $in )
	{
		if( isset( $in[ 'billno' ] ) )
		{
			$info = $this->get1( 'id', array( 'billno' => $in[ 'billno' ] ) );
			if( !empty( $info ) )
			{
				return $info[ 'id' ];
			}
		}

		if( !isset( $in[ 'type' ] ) )
		{
			$in[ 'type' ] = $this->get_type_by_uid( value( $in, 'uid', 0 ) );
		}

		$in[ 'created' ] = time();
		$id = $this->add( $in );

		if( !isset( $in[ 'billno' ] ) )
		{
			$billno = config( 'site.no' ) . date('Ymd') . substr( '00000' . $id, -5 );
			$this->set( array( 'id' => $id, 'billno' => $billno ) );
		}

		return $id;
	}

	/** 根据uid，返回当前推荐支付方式。通过修改本函数，可实现不同的支付方式
	 * @param $uid, 0表示同行站
	 * @return int
	 */
	function get_type_by_uid( $uid )
	{
		return 7;
	}
	
	/** 支付完成
	 * @param $billno
	 * @param $status
	 * @return array: json
	 */
	function finish( $billno, $status )
	{
		$info = $this->get1( '*', array( 'billno' => $billno ) );

		if( empty( $info ) )
		{
			$this->log( 0, 0, "订单不存在，订单号={$billno}", array( 'type' => 2, 'data' => func_get_args() ) );
			return $this->out( -1, '订单不存在' );
		}
		
		if( $info[ 'status' ] != '0' )
		{
			$this->log( 0, $info[ 'id' ], "订单状态不正确，订单号={$billno}", array( 'type' => 2, 'data' => func_get_args() ) );
			return $this->out( -2, '订单状态不正确' );
		}

		$this->set( array( 'id' => $info[ 'id' ], 'status' => $status ) );

		$this->send( $info[ 'id' ] );

		return $this->out( 'ok' );
	}

	/** 处理（或重发）结果
	 * @param $id
	 * 返回：json
	 */
	function send( $id )
	{
		$info = $this->get_from_id( $id );

		$status = $info[ 'status' ];

		$result = in_array( $status, array( 1, 3 ) ) ? 1 : 0;
		$site = biz('site');
		$http = load( 'http' );
		if( $info[ 'site' ] == '' )	// 本站支付成功
		{
			$this->error( '报告dda：本站支付代码未完成' );
			$out = $this->out( 1, '操作成功' );
		}
		else
		{
			if( $info[ 'site' ] == 'm.tuijie.cc')
			{
				$info[ 'site' ] = 'www.tuijie.cc';
			}
			$url = $site->get_url( $info[ 'site' ], 'payment', 'result', array( 'no' => $info[ 'billno' ],
				'price' => $info[ 'money' ], 'status' => $status ) );
			$result = $http->get_json( $url );

			// 请求结果不正确时，报警（或加入队列持续请求）
			if( value( $result, 'code' ) != 1 )
			{
				biz('alert')->create( array( 'content' => '订单结果无法及时通知来源站，id=' . $id ) );
				$out = $this->out( -1, '无法及时通知来源站' );
			}
			else
			{
				$this->set( array( 'id' => $info[ 'id' ], 'have_notice' => 1 ) );
				$out = $this->out( 1, '操作成功' );
			}
		}

		return $out;
	}
}
?>