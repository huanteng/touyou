<?php
//业务逻辑基类
class base
{
	var $db = null;
	var $table = null;	// 表名，以方便create、remove、modify、search、info方法使用
	var $id = 'id';		// 表的键字段名

	// 字段列表，用逗号隔开，不要出现空格。保留字也不必用"`"包含
	// 主要被add、set、del中实现通用。
	// 调用add、set、del时，不在field中定义的值，将被忽略
	var $field = 'field1,field2';

	/*
	 * 设置时间字段，当add/set时，本字段将自动取当前时间time()。按需重载。
	 * 注：
	 * 	默认值表示没这样的字段；
	 *  add/set中如果不希望本字段取当前时间，可显式传入值以覆盖。
	*/
	var $time_field = null;

	function __construct()
	{
		$this->db = load('db');
	}

	public function __call($method, $args)
	{
		if (isset($this->$method)) {
			$func = $this->$method;
			return $func($args);
		}
	}

	//返回哈希值
	function hash( $string )
	{
		return md5( $string . config('key'));
	}

	/*
	 * 获得数组形式的字段列表，供内部使用，不必重载
	 */
	function _field()
	{
		return array_flip( explode( ',', $this->field ) );
	}

	/*
	 * 增加记录
	 * 参数：
	 * 	in：一维数组，包含以下对应串 字段名=>值
	 * 返回值：
	 * 	自增id
	 */
	function add($in)
	{
		$field = self::_field();

		$time_field = $this->time_field;
		if( !is_null( $time_field ) )
		{
			$in[ $time_field ] = time();
		}

		foreach( $in as $k => $v )
		{
			if (!isset($field[$k]))
			{
				unset($in[$k]);
			}
		}

		return $this->db->add($this->table, $in);
	}

	/* 返回本数据表中符合条件的数量
	 * 参数：
	 * 	term：条件数组
	 */

	function count($term)
	{
		$data = $this->get1('count(*) as c', $term);
		return $data[ 'c' ];
	}
	
	/* 返回本数据表中符合条件的字段总和
	 * 参数：
	 *  $field : 统计的字段
	 * 	term：条件数组
	 */

	function sum( $field, $term )
	{
		$data = $this->get1('sum(`'.$field.'`) as s', $term);
		return $data[ 's' ];
	}

	// 修改资料，返回影响条数
	function set($in)
	{
		$id_field = $this->id;
		$id = $in[ $id_field ];

		if( !is_array( $in ) )
		{
			$this->error( 'set传入错误的参数，', $in );
		}

		unset($in[ $id_field ]);

		return $this->set_by_term( $in, array( $id_field => $id) );
	}

	/** 按条件更新
	 * @param $in：更新项
	 * @param $term：条件
	 * @return 影响条数
	 */
	function set_by_term( $in, $term )
	{
		$field = self::_field();

		$time_field = $this->time_field;
		if( !is_null( $time_field ) )
		{
			$in[ $time_field ] = time();
		}

		$in = $this->to_array( $in );

		foreach( $in as $k => $v )
		{
			if (!is_numeric($k) && !isset($field[$k]))
			{
				unset($in[$k]);
			}
		}

		$term = $this->to_array( $term );

		return $this->db->set($this->table, $in, $term );
	}

	// 删除记录，$id是单个数字
	// 返回值：true|false
	function del($id)
	{
		$out = ( $this->db->del($this->table, array( $this->id => $id)) > 0 );

		return $out;
	}

	/*
	 * 按条件删除，返回true|false
	 */
	function del_by_term( $term )
	{
		$term = $this->to_array( $term );

		$out = ( $this->db->del($this->table, $term ) > 0 );

		return $out;
	}

	/** 查询，可用于复杂sql语句
	 * @param $field：查询的字段项
	 * @param $other：from {table} 后面的sql表达
	 * @param $table：数据表，如需指定，请用本参数
	 *
	 * @return 记录集
	 */
	function select( $field, $other, $table = '' )
	{
		if( $table == '' )
		{
			$table = $this->table;
		}

		$sql = 'select ' . $field . ' from ' . $table . ' ' . $other;
		return $this->db->select( $sql );
	}

	// 通用的查询函数，返回二维参数：
	// $in：数据组，部分特殊变量说明如下
	//	orderby：排序，默认为空
	//	pagesize：页宽，默认为10
	// 		0表示不限，将返回全部数据，即相当于所有的$result['data']
	// 		负数时，返回结果中的-pagesize的数据，即相当于的$result['data']的最新的-pagesize条
	//	 	正数时，返回page页数据及返回数据，其中 nav 翻页数据，data 数据
	//	page：当前页，仅当pagesize>0时才有用
	// $equal：$in中，使用＝作为比较符的字段名列表，分别和form及数据表的字段对应
	// $like：同理，只是和like作为比较符
	// $q：q关键字要检查的字段名列表，均使用like为比较符，字段间用or作为处理
	// $option：额外参数，便于扩充
	//	in：$in中，使用 in(...)来比较
	// 	term：额外的where条件，数组形式，每个元素是一个条件，默认为空。
	//		如果是单个条件，也可以只是一个字符串
	// 	join：条件间关系，默认为and
	//	field：要返回的字段，默认为*
	//	groupby：分组条件，默认为空
	//	table：从哪个表取？默认为空，会自动从table获得，但在join table时，可以通过本参数传入
	// $result['nav'] 翻页数据
	// $result['data'] 数据
	function search( $in, $equal, $like = array(), $q = array(), $option = array() )
	{
		$option = load('arr')->set_default( $option, array( 'term' => array(), 'join' => 'and',
			'groupby' => '' , 'field' => '*', 'table' => '', 'in' => array() ) );

		if( $option[ 'table' ] == '' )
		{
			$option[ 'table' ] = $this->table;
		}

		$term = $option[ 'term' ];
		$term = $this->to_array( $term );

		foreach( $equal as $v )
		{
			if( value( $in, $v ) != '' )
			{
				$term[$v] = $in[$v];
			}
		}

		foreach( $like as $v )
		{
			if( value( $in, $v ) != '' )
			{
				$term[] = "$v like '%" . $in[$v] . "%'";
			}
		}
		if( value($in, 'q') != '' && count( $q ) > 0 )
		{
			$s = array();
			foreach ($q as $v)
			{
				$s[] = "$v like '%" . $in['q'] . "%'";
			}
			$term[] = implode( ' or ', $s );
		}

		$option[ 'in' ] = $this->to_array( $option[ 'in' ] );
		foreach( $option[ 'in' ] as $v )
		{
			if( value( $in, $v ) != '' )
			{
				$term[] = "$v in(" . $in[$v] . ")";
			}
		}

		$where = $this->db->term($term, $option[ 'join' ]);

		$count = ' from ' . $option[ 'table' ];
		if( $where != '' )
		{
			$count .= ' where ' . $where;
		}

		if( $option[ 'groupby' ] != '' )
		{
			$count .= ' group by ' . $option[ 'groupby' ];
		}

		$orderby = value($in, 'orderby');
		if( $orderby != '' )
		{
			$orderby = ' order by ' . $orderby;
		}

		$sql = 'select ' . $option[ 'field' ] . $count . $orderby;

		$result = array();
		$pagesize = value( $in, 'pagesize', 10);
		if ($pagesize == 0) {
			$result['data'] = $this->db->select($sql);
		} elseif ($pagesize < 0) {
			$result = $this->db->select($sql . ' limit ' . (-$pagesize));
		} else {
			if( $option[ 'groupby' ] != '' )
			{
				$base = biz('base');
				$base->table = "(select  distinct({$option[ 'groupby' ]}) $count ) as t";
				$total = $base->count( array() );
			}
			else
			{
				$total = $this->db->total($count);
			}

			$pagecount = ceil( $total / $pagesize );
			$page = value( $in, 'page', 1 );
			$page = is_numeric( $page ) && $page >= 1 ? $page : 1;

			$result = array();
			$result['nav'] = array( 'total' => $total,	// 总记录数
				'pagesize' => $pagesize,	// 页宽
				'page' => $page,			// 当前页
				'pagecount' => $pagecount	// 总页码
			);

			if( $total == 0 )
			{
				$result['data'] = array();
			}
			else
			{
				$sql .= ' limit ' . (( $page - 1 ) * $pagesize) . ', ' . $pagesize;
				$result['data'] = $this->db->select($sql);
			}
		}
		return $result;
	}

	//检查 $key是否存在并且是数字，如存在则返回，否则返回$default
	function number($in, $key, $default = 0)
	{
		$value = value( $in, $key, $default );
		return is_numeric( $value ) ? $value : $default;
	}

	//数组$data，一般只包含一列，最多两列 将数组转为字符串返回，主要应用于in查询
	// 参数说明：
	// pre：前后辍
	// join：用什么符号串接
	function implode2($data, $pre = '', $join = ',')
	{
		$s = '';
		foreach ($data as $row) {
			if (is_array($row)) {
				foreach ($row as $v)
					$s .= $pre . $v . $pre . $join;
			}else
				$s .= $pre . $row . $pre . $join;
		}
		if( $s != '' && $join != '' )
		{
			$s = substr($s, 0, -strlen( $join ) );
		}

		return $s;
	}

	/** 保证参数为数组形式。大部分db操作参数要求为数组，但为了方便调用，也允许仅传入单个字符串，此时，表示单个元素的数组。
	 * @param $str
	 * @return array
	 */
	function to_array( $str )
	{
		if( !is_array( $str ) )
		{
			$str = array( $str );
		}

		return $str;
	}

	// <editor-fold defaultstate="collapsed" desc="数据库操作">
	function get1($field, $term = array(), $orderby = '')
	{
		$data = $this->get( $field, $term, $orderby, 1 );
		return value( $data, '0', array() );
	}

	/** 获得数据
	 * @param $field：字段列表
	 * @param array $term：条件，有两种形式
	 * 	形式一：字符串
	 * 	形式二：多条件形式的数组。
	 * 	注：传入前请自行防注入
	 * @param string $orderby
	 * @param string $limit
	 * @return mixed
	 */
	function get($field, $term = array(), $orderby = '', $limit = '')
	{
		$term = $this->to_array( $term );
		return $this->db->get($field, $this->table, $term, $orderby, $limit);
	}

	/** 获得符合条件的id，并用半角逗号串接，本函数用于简化代码
	 * @param $term：条件
	 * @param $id：id字段名，默认为“id”。如果取其它字段，可传入本参数
	 * @return 用半角逗号串接的id，如不存在，则返回空字符串
	 */
	function get_ids( $term, $id = 'id' )
	{
		$data = $this->get( $id, $term );

		if( empty( $data) )
		{
			return '';
		}
		else
		{
			return $this->implode2( $data, '' );
		}
	}
	
	/*
	 * 返回分页数据
	 * 参数：
	 * $in['page'] , $in['pagesize']
	 * $term : sql where 条件
	 */
	function get_page_data($in, $term)
	{
		$total = $this->count( $term );
		$page = value($in, 'page', 1);
		$pagesize = value($in, 'pagesize', 20);
		$pagecount = ceil( $total / $pagesize );
		
		$out = array( 'total' => $total,	// 总记录数
				'pagesize' => $pagesize,	// 页宽
				'page' => $page,			// 当前页
				'pagecount' => $pagecount,	// 总页码
				'limit' => ( $page - 1 ) * $pagesize . ',' . $pagesize	//get 函数会用到 limit
			);
		return $out;
	}

	// 是否存在符合条件的数据，如存在返回true
	function exists($term = array())
	{
		return $this->db->exists($this->table, $term);
	}

	/* 主要为了便于使用
	 * 参数：
	 * 	name
	 * 	default，可选，如不存在时，默认返回
	 * 	key：可选，查询的name条件字段，默认值为name
	 * 返回值：用户id字段，如不存在返回default
	 * 常用调用：return $user->get_name_from_id( 'dda' )
	 */
	function get_id_from_name($name, $default = '', $key = 'name')
	{
		$id_field = $this->id;
		$info = $this->get1( $id_field, array($key => $name));
		return value($info, $id_field, $default);
	}

	/* 主要为了便于使用
	 * 参数：
	 * 	id
	 * 	default，可选，如不存在时，默认返回
	 * 	name，可选，查询的name字段，默认值为name
	 * 返回值：用户name字段，如不存在返回default
	 */
	function get_name_from_id($id, $default = '', $name = 'name')
	{
		$id_field = $this->id;

		$info = $this->get1($name, array($id_field => $id));
		return value($info, $name, $default);
	}

	/* 主要为了便于使用
	 * 参数：
	 * 	id
	 * 返回值：数组，如无对应值，返回空数组
	 */
	function get_from_id($id)
	{
		return $this->get1('*', array($this->id => $id));
	}

	/* 作用：通过id，返回对应的单个字段的值
	 * 参数：
	 * 	id
	 * 	field：要查询的字段，只支持单字段
	 * 	default：可选，默认为空，如果不存在，返回的默认值
	 * 返回值：该字段的值，或不存在时返回默认值
	 */
	function get_field_from_id($id, $field, $default = '')
	{
		$data = $this->get1($field, array($this->id => $id));
		return value($data, $field, $default);
	}

	/*
	 * 从一个字段条件中，获得记录集，相当于get( '*', array( key => value ) )
	 * 注意：仅为了程序简化，这和get_from_id不同，前者只返回一条数据，而本函数返回记录集
	 */
	function get_from_key( $key, $value )
	{
		return $this->get( '*', array( $key => $value ) );
	}

	/* 记录信息于错误文件中
	 * 不定参数，可以为字符串或对象
	 */
	function error()
	{
		$data = array(
			'time' => date( 'Y-m-d H:i:s', time() ),
			'url' => $_SERVER['SCRIPT_FILENAME']
		);

		$list=func_get_args();
		foreach ( $list as $v )
		{
			$data[] = $v;
		}
		file_put_contents( config('dir.cache') . 'error.php', var_export( $data, true ) . "\n\n", FILE_APPEND );
	}


	/*
	 * 联赛名字和球队名字
	 * 参数：
	 * $data : 数组
	 * [34993] => Array
	  (
	  [sc] => 新潟天鵝乙队
	  [hk] => 新潟天鵝乙隊
	  [en] => Albirex Niigata FC
	  )
	 * $id : 球队ID
	 * $language : 语言，简繁英
	 */

	function index_name($data, $id, $language = 'sc')
	{
		if (isset($data[$id][$language]) && $data[$id][$language] != '')
			$return = $data[$id][$language];
		elseif (isset($data[$id]['hk']) && $data[$id]['hk'] != '')
			$return = $data[$id]['hk'];
		elseif (isset($data[$id]['en']) && $data[$id]['en'] != '')
			$return = $data[$id]['en'];
		else
			$return = ' - ';
		return $return;
	}

	/*
	 * 格式化胜率
	 */

	function rate($rate)
	{
		if ($rate >= 1)
			return '100%';
		elseif ($rate > 0 && $rate < 1)
			return (number_format($rate * 100, 1) . '%');
		else
			return '0%';
	}

	/*
	 * 返回正常输出数组，用于对象间返回时简短写法。
	 * 注意：这和controller::out()有区别
	 *
	 * 有两种参数调用形式
	 * 形式一，只有一个参数：
	 *	data：一般是json格式，用于正常的返回记录集，此时，code=1, memo='成功'
	 *
	 * 形式二，有两个参数：
	 *	code：输出代码，一般来说，负数表示不同的出错代码，正数表示正确
	 *	memo：描述，一般用于表达出错描述。
	 *
	 * 返回值：json{code, data, memo}
	 */
	function out( $s1, $s2 = '' )
	{
		if( $s2 == '' )
			return array( 'code' => 1, 'data' => $s1, 'memo' => '成功' );

		return array( 'code' => $s1, 'data' => array(), 'memo' => $s2 );
	}

	/*
	 * 取两个值中的另一个值
	 * 参数：
	 * 	value1：值1
	 * 	value2：值2
	 * 	current：当前值
	 */
	function other( $value1, $value2, $current )
	{
		return $current == $value1 ? $value2 : $value1;
	}

	/** 通用字典定义
	 * @param $data：字典定义
	 * @param $key：关键字
	 * @param string $default：不存在时返回值
	 * @param string $alert_text：不存在时，报警提示文字，空表示不提示
	 * @return mixed
	 */
	function dict( $data, $key = '', $default = '错误', $alert_text = '未定义' )
	{
		if( $key == '' )
		{
			return $data;
		}

		if( !isset( $data[ $key ] ) )
		{
			if( $alert_text != '' )
			{
				$this->log( 'dict', 0, $alert_text . ', ' . $key );
			}

			$data[ $key ] = $default;
		}

		return $data[ $key ];
	}

	/** 记录log，放在此便于程序简化调用
	 * @param $module：模块，一般是数据表名，或功能名。如果是空字符串，则自动取当前对象名
	 * @param $relate_id：相关id，如无，建议填入0
	 * @param $content：内容
	 * @param array $data：其它参数，参数 log::create
	 * @param $allow_repeat，是否允许重复
	 *
	 * 注：module参数为数字时的特殊用法：
	 * 	此时，表示log调试级别，将用来和当前全局的log调试级别作比较，如符合条件，才记录。（这时，module采用当前对象名，或在data参数中传入）
	 */
	function log( $module, $relate_id, $content, $data = array(), $allow_repeat = true )
	{
		$log = biz( 'log' );

		if( is_int( $module ) )
		{
			if( $log->level < $module )
			{
				return;
			}

			// 当module=0时，表示这条信息很严重，同时在error文件中产生
			if( $module == 0 )
			{
				$this->error( $content );
			}

			$module = value( $data, 'module' );
		}

		if( $module == '' )
		{
			$module = $this->table;
		}

		$data[ 'module' ] = $module;
		$data[ 'relate_id' ] = $relate_id;
		$data[ 'content' ] = $content;

		$log->create( $data, $allow_repeat );
	}

	/** 仅为8和9两级的调试级别简化代码使用。
	 * 调试级别为8时，记录简化日志；
	 * 调试级别为9时，记录详尽日志。其它情况下，请使用log()
	 * @param $relate_id
	 * @param $content
	 * @param $data_data：对应于log::data::data
	 * @param array $data：对应于log的字段，一般不必传入
	 * @param bool $allow_repeat
	 */
	function log89( $relate_id, $content, $data_data, $data = array(), $allow_repeat = true )
	{
		$log = biz( 'log' );
		if( $log->level >= 9 )
		{
			if( !isset( $data[ 'module' ] ) )
			{
				$data[ 'module' ] = $this->table;
			}
			$data[ 'relate_id' ] = $relate_id;
			$data[ 'content' ] = $content;

			if( $log->level >= 9 )
			{
				$data[ 'data' ] = $data_data;
			}
			$log->create( $data, $allow_repeat );
		}
	}

	/** 如果符合条件，则log。之所以这样抽象，是为了代码简单
	 * @param $check：条件，true|false
	 * 其它参数参考log()，一一匹配
	 */
	function iflog( $check, $module, $relate_id, $content, $data = array(), $allow_repeat = true )
	{
		if( $check )
		{
			$this->log( $module, $relate_id, $content, $data, $allow_repeat );
		}
	}


}
?>