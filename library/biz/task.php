<?php
require_once dirname( __FILE__ ) .'/base.php';

class task extends base
{
	function __construct()
	{
		parent::__construct();
		$this->table = __CLASS__;
		$this->field = 'id,type,title,data,run_time,min_second,max_second,time,status,log,remark';
		$this->time_field = 'time';
	}
	
	function status_dict( $i = '' )
	{
		$data = array( '1' => '正常', '2' => '进行中', '3' => '暂停' );

		return $this->dict( $data, $i, '错误', '未定义任务状态，' + $this->table );
	}

	/* 创建任务
	 * 参数：
	 *	type
	 *	data，json格式
	 *	time，执行时间
	 *	title,min_second,remark
	 *  max_second，可选，默认为min_second
	 *  run_time，可选，
	 *	status，可选，默认为1
	 *	allow_repeat：可选，是否允许重复，以type和data为key，默认为false
	 */
	function create( $in )
	{
		$in = load('arr')->set_default( $in, array( 'status' => 1, 'data' => array(), 'min_second' => 0, 'max_second' => 0,
			'allow_repeat' => FALSE ) );

		$in[ 'data' ] = json_encode( $in[ 'data' ] );
		if( $in[ 'allow_repeat' ] === FALSE )
		{
			if( $this->exists( array( 'type' => $in['type'], 'data' => $in[ 'data' ] ) ) )
				return;
		}

		if( !isset( $in[ 'run_time' ] ) )
		{
			$in[ 'run_time' ] = time() + mt_rand( $in[ 'min_second' ], $in[ 'max_second' ] );
		}

		return $this->add( $in );
	}

	/** 获得随机npc列表
	 * @param int $num：数量
	 * @param int $sport：设定7天胜率条件，0表示不限，1足球，2篮球
	 * @param float $rate：具体胜率
	 * @param bool $filter：是否排除自动发布和胜率调整里面的用户
	 * @return { uid1, uld2...}
	 */
	function get_npc( $num = 1, $sport = 0, $rate = 0.4, $filter = false )
	{
		$term = array( 'is_npc' => 1 );

		if( $sport != 0 )
		{
			$field = $sport == 1 ? 'win_rate7' : 'basketball_win_rate7';
			$in = biz('tips_report')->get_ids( $field . '>=' . $rate );

			if( $in == '' )
			{
				return array();
			}

			$term[] = 'uid in(' . $in . ')';
		}

		if( $filter )
		{
			$temp1 = biz('tips_adjust')->get( 'uid', array() );
			$temp2 = biz('tips_fake')->get( 'uid', array() );
			$not_in = $this->implode2( array_merge( $temp1, $temp2 ), '' );

			if( $not_in != '' )
			{
				$term[] = 'uid not in(' . $in . ')';
			}
		}

		$data = biz('users')->get( 'uid', $term, 'rand()', $num );
		$out = array( );
		foreach( $data as $v )
		{
			$out[] = $v[ 'uid' ];
		}

		return $out;
	}

	/** 设置报警状态。部分检查报警类的任务时，最终均通过本方法来设置或删除报警项
	 * @param $is_alert：是否报警，true|false
	 * @param $task_id：task.id
	 * @param $content：报警内容。（当要删除报警时，本参数无意义）
	 * @param string $color：颜色
	 * @param string $url：链接
	 */
	function set_alert( $is_alert, $task_id, $content, $color = '', $url = '' )
	{
		$alert = biz( 'alert' );
		$info = $alert->get1( '*', array( 'task_id' => $task_id ) );
		if( $is_alert )
		{
			$term = array( 'content' => $content );
			if( $url != '' )
			{
				$term[ 'url' ] = $url;
			}

			if( empty( $info ) )
			{
				$term[ 'task_id' ] = $task_id;
				$alert->add( $term );
			}
			else
			{
				$term[ 'id' ] = $info[ 'id' ];
				$alert->set( $term );
			}
		}
		else
		{
			if( !empty( $info ) )
			{
				$alert->del( $info[ 'id' ] );
			}
		}
	}

	/**
	 * @param array $term：条件
	 * @param bool $force：是否强制执行
	 */
	function execute( $term = array(), $force = false )
	{
		$now = time();

		if( !$force )	// 非强制执行
		{
			$term[ 'status' ] = 1;
			$term[] =  'run_time<=' . $now;
		}

		$data = $this->get( '*', $term, 'run_time asc', 10 );

		$log = biz( 'log' );

		foreach( $data as $info )
		{
			$function = 'execute_' . $info[ 'type' ];

			if( !method_exists( $this, $function ) )
			{
				$this->error( 'task::execute不存在对应的函数：', $info );
				continue;
			}

			$info[ 'data' ] = json_decode( $info[ 'data' ], true );

			$log->set_level( $info[ 'log' ] );
			$log->log( 9, $info[ 'id' ], '执行：' . $info[ 'title' ] );

			/**
			 * 返回值说明：
			 * 0：一次性任务
			 * 正数：隔多久秒再执行一次
			 * 空：即无返回值，多次任务正常运行
			 * 数组：要修改的字段集（不含id字段）
			 */
			$out = $this->{$function}( $info );

			if( is_null( $out ) )
			{
				$out = array();
			}
			elseif( !is_array( $out ) )
			{
				if( $out == 0 )
				{
					$this->del( $info['id'] );
				}
				else
				{
					$out = array( 'run_time' => $now + $out );
				}
			}

			if( is_array( $out ) )
			{
				if( isset( $out[ 'data' ] ) )
					$out[ 'data' ] = json_encode( $out[ 'data' ] );

				if( !isset( $out[ 'run_time' ] ) )
				{
					$out[ 'run_time' ] = $now + mt_rand( $info[ 'min_second' ], $info[ 'max_second' ] );
				}

				$out[ 'id' ] = $info['id'];
				$this->set( $out );
			}
		}
	}

	/*
	 * 处理旧数据
	 * 删除7天前的log
	 */
	function execute_9( $info )
	{
		$now = time();

		//设置过期的为无用状态
		biz('tips_fake')->set_by_term( array( 'status' => 0 ), array( 'expire > 0 and expire < ' . $now ) );
		biz('tips_adjust')->set_by_term( array( 'status' => 0 ), array( 'expire > 0 and expire < ' . $now) );
		biz('log')->del_by_term( 'valid_time<' . $now );

		// 以下为兼容旧写法
		$db = load( 'db' );

		$db->command( 'delete from deny_resource where end < ' . $now );

		//设置过期的为无用状态
		$db->command( 'update tips_fake set status = 0 where expire > 0 and expire < ' . $now );
		$db->command( 'update tips_adjust set status = 0 where expire > 0 and expire < ' . $now );

		//删除过期的未知对阵表记录
		$time = strtotime( '-2 day' );
		$db->command( 'delete from unknown_match where time < ' . $time );

		//删除过期的推介点击记录
		$db->command( 'delete from tips_click where time < ' . $time );

		//删除过期的采集错误记录
		$time = strtotime( '-3 day' );
		$db->command( 'delete from collect_error where time < ' . $time );

		//删除过期的用户轨迹
		$time = strtotime( '-7 day' );
		$db->command( 'delete from user_orbit where time < ' . $time );
		$db->command( 'delete from wap_orbit where time < ' . $time );

		//删除过期的比赛时间变化记录
		$db->command( 'delete from vs_time_change where match_time < ' . $time );

		$time = strtotime( '-30 day' );
		//删除过期的通知记录
		$db->command( 'delete from uchome_notification where dateline < ' . $time );

		//删除过期的用户操作记录
		$db->command( 'delete from user_operate where type in ( 4, 5 ) and time < ' . $time );

		//删除过期的用户访问用户的数据
		$db->command( 'delete from uchome_visitor where dateline < ' . $time );

		//删除过期的消息
		$db->command( 'delete from message where type in ( 1, 2 ) and limittime < ' . $now );
		$db->command( 'delete from message where limittime < ' . $time );

	}



}
?>