<?php
// 数据级缓存，抄自文件版cache.php，做了简化
class data
{
	/*
	 * 保存数据
	 * 参数：
	 * 	key：变量名，对应保存为文件名，如果分目录，使用“.”
	 * 	value：数据
	 */
	function set( $key, $value )
	{
		$file = $this->name( $key );

		$info = pathinfo( $file );
		if( !is_dir( $info[ 'dirname' ] ) )
		{
			mkdir( $info[ 'dirname' ], 0755, true );
		}

		$content = '<?php $__cache__ = ' . var_export( $value, true ) . ' ?>';
		return file_put_contents( $file, $content );
	}

	/*
	 * 获得数据
	 * 参数：
	 * 	key：变量名
	 * 	second：有效秒数，如果缓存文件早于这之前，则认为缓存不存在。0（默认值）为不会过期
	 * 	default：当缓存不存在（或失效）时，返回的默认数据
	 */
	function get( $key, $second = 0, $default = FALSE )
	{
		$file = $this->name( $key );

		if( !is_file( $file ) || ( $second > 0 && ( time() - filemtime( $file ) > $second ) ) )
		{
			return $default;
		}

		require $file;
		return $__cache__;
	}

	/*
	 * 删除缓存
	 */
	function del( $key )
	{
		$file = $this->name( $key );
		return load('file')->del( $file );
	}

	/*
	 * 获得缓存名字，外部一般不需显式调用
	 */
	function name( $key ) {
		return config( 'dir.cache' ) . str_replace( '.', '/', $key ) . '.php';
	}
}
?>