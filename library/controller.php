<?php
require_once dirname( __FILE__ ) . '/function.php';

class controller
{
	var $error = array();
	var $input = array();
	var $output_handler = 'ob_gzhandler';
	var $content_type = 'content-type: text/html; charset=utf-8';
	var $content = '';	// 要输出的内容

	function __construct()
	{
		if( !isset( $GLOBALS[ 'mvc_init' ] ) )
		{
			$GLOBALS[ 'mvc_init' ] = true;

			if( !config( 'develop' ) )
			{
				error_reporting (0);
				set_error_handler( array( & $this, 'mark' ) );
			}

			register_shutdown_function( array( & $this, '_finish' ) );
			mb_internal_encoding( 'UTF-8' );

			// 保证$_POST等为未转义的原文，如果要在数据库中使用，要编程防注入，而->input是已经防注入的，可在数据库中使用
			//set_magic_quotes_runtime( 0 );
			if (get_magic_quotes_gpc()) {
				function stripslashes_deep($value)
				{
					$value = is_array($value) ?
								array_map('stripslashes_deep', $value) :
								stripslashes($value);

					return $value;
				}

				$this->input = array_merge( $_POST , $_GET );

				$_POST = array_map('stripslashes_deep', $_POST);
				$_GET = array_map('stripslashes_deep', $_GET);
				$_COOKIE = array_map('stripslashes_deep', $_COOKIE);
				$_REQUEST = array_map('stripslashes_deep', $_REQUEST);
			}
			else
			{
				// 防注入，子函数中可安全使用。而$_POST等未防注入，在数据库中使用时，要手工写好防注入
				$this->input = load('http')->clean( array_merge( $_POST , config( 'get' ) ) );
			}

			if( value( $_GET, 'cache' ) == 'new' )
			{
				load('cookie')->set( 'cache', 'new' );
			}

			if ( $this->output_handler != '' ) ob_start( $this->output_handler );
			if ( $this->content_type != '' ) header( $this->content_type );

			if( !isset( $this->input['method'] ) )
			{
				$this->input['method'] = 'home';
			}
		}
	}

	function run( $in = '' )
	{
		if( $in == '' )
		{
			$in = $this->input;
		}

        $method = $in['method'];
		if ( method_exists( $this, $method ) )
		{
			unset( $in['method'] );
			$this->content = $this->$method( $in );
		}
		else
		{
			biz('error')->error( '未定义方法，method=', $method );
		}
	}

	/*
	 * ajax请求时的输出
	 * 三种调用方式，
	 * 方式一：3 参数
	 * 	code：代码，一般来说，正数表示正确，负数代表不同的出错信息
	 * 	memo：提示信息，字符串
	 * 	data：数组形式的数据
	 *
	 * 方式二：2 参数
	 * 	code
	 * 	memo
	 *
	 * 方式三：1参数，此时认作code=1
	 * 	memo或data
	 */
	function ajax_out( $code, $memo = '', $data = array() )
	{
		$out = array();

		if( is_array( $code ) )
		{
			$out = array( 'code' => 1, 'data' => $code );
		}
		elseif( is_string( $code ) )
		{
			$out = array( 'code' => 1, 'memo' => $code );
		}
		else
		{
			$out = array( 'code' => $code, 'memo' => $memo );

			if( count( $data ) > 0 )
			{
				$out[ 'data' ] = $data;
			}
		}

		return json_encode( $out );
	}

	function prompt( $data, $url, $file = 'prompt.php', $time = 3 )
	{
		$default = '';

		if( is_array($url) )
		{
			foreach( $url as $index => $value )
			{
				if ( isset( $value['default'] ) )
				{
					$default = $index;
					break;
				}
			}
		}

		$template = load( 'template' );
		$template->assign( 'title', '推介网' );
		$template->assign( 'data', $data );
		$template->assign( 'time', $time );
		$template->assign( 'url', $url );
		$template->assign( 'default', $default );
		echo $template->parse( $file );
	}

	function mark( $no, $str, $file, $line )
	{
		$EXIT = FALSE;

		switch ( $no ) {
			//提醒级别
			case E_NOTICE:
			case E_USER_NOTICE:
				$error_type = 'Notice';
				break;
			//警告级别
			case E_WARNING:
			case E_USER_WARNING:
				$error_type = 'Warning';
				break;

			//错误级别
			case E_ERROR:
			case E_USER_ERROR:
				$error_type = 'Fatal Error';
				$EXIT = TRUE;
				break;

			//其他未知错误
			default:
				$error_type = 'Unknown';
				$EXIT = TRUE;
				break;
		}
		$content = "{$error_type}：$file, line=$line, no=$no,<br>$str<br><br>";
		if( config( 'develop' ) )
		{
			echo $content;
		}
		else
		{
			if( $EXIT )
			{
				biz('base')->error( $content );
				die;
			}
		}
	}

	function _finish()
	{
		//* 有些错误要在此捕捉
		$error = error_get_last();

		if ( $error !== NULL )
		{
			$this->mark( $error['type'], $error['message'], $error['file'], $error['line'] );
		}

		$log = load('data')->get( 'error_conf' );

		if ( isset( $log['log'] ) )
		{
			$is_log = true;

			/*$uid = '';
			if( isset( $log['uid'] ) )
			{
				$uid = load( 'cookie' )->get( 'uid', true );
				$is_log = ( $uid == $log['uid'] );
			}
			else
			{
				$is_log = true;
			}
			*/
			$filename = config( 'server.SCRIPT_FILENAME' );
			if( substr( $filename, strlen( $filename ) - 10, 10 ) == '/error.php' )
			{
				$is_log = false;
			}

			if( $is_log && isset( $log['url'] ) )
			{
				$is_log = $this->log_check_in( $log['url'], config( 'server.REQUEST_URI' ) );
			}
			if( $is_log && isset( $log['postdata'] ) )
			{
				$is_log = $this->log_check_in( $log['postdata'], var_export( config( 'request' ), true ) );
			}
			if( $is_log && isset( $log['content'] ) )
			{
				$is_log = $this->log_check_in( $log['content'], $this->content );
			}
			if( $is_log && isset( $log['sql'] ) )
			{
				$is_log = $this->log_check_in( $log['sql'], var_export( load('db')->sql, true ) );
			}

			if ( $is_log )
			{
				//$data = array( 'uid' => $uid );
				$data = array();
				if( isset( $log['save_content'] ) ) $data['content'] = $this->content;
				if( isset( $log['save_sql'] ) ) $data['sql'] = load('db')->sql;

				biz('error')->error( $data );
			}
		}

		echo $this->content;
	}

	// 主要用户log场景。$keys中设定多个过滤条件（用逗号分开），逐一检查是否处于$content中
	function log_check_in( $keys, $content )
	{
		if( $keys == '' ) return true;
		foreach( explode( ',', $keys ) as $key )
		{
			if( strpos( $content, $key ) !== false )
			{
				return true;
			}
		}
		return false;
	}
}
?>