<?php
class form
{
	/** 返回单选框内容
	 * @param $name：控件名字
	 * @param $value：当前选中控件件
	 * @param array $list：控件值列表，默认是或否
	 * @return string：适合显示的html
	 */
	function radio( $name, $value, $list = array( 1 => '是', 0 => '否' ) )
	{
		$out = '';
		foreach( $list as $key => $text )
		{
			$out .= '&nbsp;&nbsp;<label for="' . $name.$key . '"  class="radio"><input type="radio" id="'.$name.$key.'" name="' . $name . '" value="'. $key . '"';
			if( $key == $value )
			{
				$out .= ' checked="checked"';
			}
			$out .= '>' . $text . '</label> ';
		}

		return $out;
	}
	
	/*
	 * 返回多选框内容,name 传入值的集合字符串
	 */
	function checkbox( $name, $value, $list )
	{
		$out = '';
		foreach( $list as $key => $text )
		{
			$out .= '&nbsp;&nbsp;<label class="checkbox inline"><input type="checkbox" id="'.$name.$key.'" name="' . $name . '[]" value="'. $key . '"';
			if( stristr("$value","$key" )  )
			{
				$out .= ' checked';
			}
			$out .= '>' . $text . '</label> ';
		}

		return $out;
	}
	
	/*
	 * 返回下拉单选框内容
	 * option:
	 * empty：多一个空白的选项
	 * attr：附加字符串，如style=""
	 */
	function select( $name, $value, $list, $option=array('empty'=>false, 'attr'=>'') )
	{
		$out = '<select id="' . $name . '1" name="' . $name . '" '. (!empty($option['attr']) ? $option['attr'] : '')  .'>';
		if( (is_bool($option) && $option ) || $option['empty'] ) $out .= '<option value="">-</option>';
		foreach( $list as $key => $text )
		{
			$out .= '<option value="'. $key . '"';
			if( $key == $value )
			{
				$out .= ' selected';
			}
			$out .= '>' . $text . '</option> ';
		}

		return $out.'</select>';
	}
}
?>