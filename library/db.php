<?php
class db
{
	var	$host = '';
	var	$name = '';
	var	$user = '';
	var	$pass = '';
	var $sql = array();
	var	$link = null;

	// 未结束的事务数量
	var $trans = 0;

	// cmd模式
	var $cmd = false;

	function __construct()
	{
		$this->cmd = config( 'server.cmd' ) == true;

		$data = config( 'database' );
		$this->host = $data['host'];
		$this->name = $data['name'];
		$this->user = $data['user'];
		$this->pass = $data['pass'];
		register_shutdown_function( array( & $this, 'disconnect' ) );
		$this->connect();
	}

	function _connect()
	{
		$this->link = mysqli_connect( $this->host, $this->user, $this->pass, $this->name );

		if( !$this->link )
		{
			// 因base构造函数会load db，导致死循环，所以没法直接使用biz('base')->error(  );
			$data = array(
				'time' => date( 'Y-m-d H:i:s', time() ),
				'url' => $_SERVER['SCRIPT_FILENAME']
			);

			$data[] = '无法连接数据库：' . $this->host;

			file_put_contents( config('dir.cache') . 'error.php', var_export( $data, true ) . "\n\n", FILE_APPEND );
			die;
		}

		mysqli_set_charset( $this->link, "utf8" );
	}

	function connect()
	{
		if ( ! is_resource( $this->link ) )
		{
			$this->_connect();
		}
		else
		{
			if ( ! mysqli_ping( $this->link ) )
			{
				mysqli_close( $this->link );
				$this->_connect();
			}
		}
	}

	function disconnect()
	{
		if ( is_resource( $this->link ) )
		{
			mysqli_close( $this->link );
			$this->link = null;
		}

		// 20141119，结束未完成事务，暂未验证是否必要这样加
		while( $this->trans > 0 )
		{
			$this->end();
		}
	}

	function start()
	{
		$this->query( 'begin' );
		++$this->trans;
	}

	// 别名
	function begin()
	{
		$this->start();
	}

	function end( $command = '' )
	{
		$this->query( 'commit' );

		--$this->trans;
	}

	/** 执行sql
	 * @param $sql
	 * @return int
	 * 	出错时，返回 -1
	 * 	否则，返回受影响记录的行数（0表示执行正确，但受影响行数为0）
	 */
	function command( $sql )
	{
		if( $this->query( $sql ) )
		{
			$affected = mysqli_affected_rows( $this->link );
			return $affected > 0 ? $affected : 0;
		}
		else
		{
			return -1;
		}
	}

	/** 执行查询，返回错误或result实例。（实质只是保存在变量中）
	 * @param $sql
	 * @return bool：执行本sql时，是否成功？
	 * 	true：执行时成功
	 * 	false：执行时出错
	 */
	function query( $sql )
	{
		$result = mysqli_query( $this->link, $sql );

		if( $result === false )
		{
			trigger_error(  $sql . ' : ' . mysqli_error( $this->link ), E_USER_ERROR );
			//throw new Exception( $sql . ' : ' . mysqli_error( $this->link ) );
		}

		// cmd模式可能要执行很多语句，防止内存泄露
		if( !$this->cmd )
		{
			$error = mysqli_error( $this->link );
			if( $error != '' )
			{
				$sql .= ': ' . $error;
			}
			$this->sql[] = $sql;
		}

		return $result;
	}

	/** 查询
	 * @param $sql
	 * @param int $time：缓存时间，单位为秒，0表示不缓存。（默认为0）
	 * @return array
	 */
	function select( $sql, $time = 0 )
	{
		if( $time > 0 )
		{
			$data = load( 'data' );

			$key = md5( $sql );

			if( load('cookie')->get( 'cache' ) != 'new' )
			{
				$out = $data->get( $key, $time );

				if( $out )
				{
					return $out;
				}
			}
		}

		$out = array();

		if( $result = $this->query( $sql ) )
		{
			while( $row = mysqli_fetch_assoc( $result ) ) $out[] = $row;
		}
		if ( is_resource( $result ) ) mysqli_free_result( $result );

		if ( $time > 0 )
		{
			$data->set( $key, $out, $time );
		}

		return $out;
	}

	function unique( $sql )
	{
		$data = $this->select( $sql );
		return isset( $data[1] ) ? array() : ( isset( $data[0] ) ? $data[0] : array() );
	}

	function total( $sql, $second = 0 )
	{
		$total = $this->select( 'select count( 1 ) as total ' . $sql, $second );
		return isset( $total[0]['total'] ) ? $total[0]['total'] : 0;
	}

	/* 20141125，保留2个月
	function split( $count, $list, $size, $current )
	{
		$total = $this->total( $count );
		$page = ceil( $total / $size );
		$current = is_numeric( $current ) && $current >= 1 ? $current : 1;
		//20130109，不必，并且这将导致客户端难以控制$current = $current > $page ? $page : $current;
		$offset = $current == 1 ? 0 : ( $current - 1 ) * $size;
		$offset = $offset < 1 ? 0 : $offset;
		$result = array( 'total' => $total, 'page' => $page, 'current' => $current, 'size' => $size, 'offset' => $offset, 'data' => array() );
		if ( $total > 0 ) $result['data'] = $this->select( $list . ' limit ' . $offset . ', ' . $size );
		return $result;
	}
	*/

	function id()
	{
		return mysqli_insert_id( $this->link );
	}

	function add( $table, $array )
	{
		$this->command( 'insert into ' . $table . ' ( `' . join( '`, `', array_keys( $array ) ) . "` ) values ( '" . join( "', '", $array ) . "' )" );
		$id = mysqli_error( $this->link ) != '' ? 0 : $this->id();
		return $id;
	}

	function set( $table, $array, $term )
	{
		$sql = '';
		foreach( $array as $key => $value ) $sql .= ( is_numeric( $key ) ? $value : '`' . $key . "` = '" . $value . "'" ) .', ';
		$sql = 'update ' . $table . ' set ' . substr( $sql, 0, -2 ) . ' where ' . $this->term( $term );
		return $this->command( $sql );
	}

	function del( $table, $term )
	{
		return $this->command( 'delete from ' . $table . ' where ' . $this->term( $term ) );
	}

	function term( $array, $join = 'and' )
	{
		if( !is_array( $array ) ) {
			return '';
		}

		$sql = '';
		foreach( $array as $k => $v )
		{
			$sql .= is_numeric( $k ) ? ( ' ( ' . $v . ' ) ' . $join . ' ' ) : '`' . $k . "` = '" . $v . "' " . $join . " ";
		}
		return ( $sql == '' ) ? '' : substr( $sql, 0, '-' . ( strlen( $join ) + 1 ) );
	}

	/** 返回记录集
	 * @param $field：字段列表，
	 * @param $table：表名
	 * @param array $term：条件，数组形式
	 * @param string $orderby：排序
	 * @param string $limit：限制多少条，默认不限
	 * @param int $second：缓存多少秒，默认不缓存
	 * @return array
	 */
	function get( $field, $table, $term = array(), $orderby = '', $limit = '', $second = 0 )
	{
		$sql = "select $field from $table";
		$where = $this->term( $term );
		if( $where != '' ) $sql .= " where $where";
		if( $orderby != '' ) $sql .= " order by $orderby";
		if( $limit != '' ) $sql .= " limit $limit";

		return $this->select( $sql, $second );
	}

	/** 查找，如存在，返回第一行数据，如不存在，返回空数组
	 * @param $field：见get，下同。
	 * @param $table
	 * @param array $term
	 * @param string $orderby
	 * @param int $second
	 * @return array
	 */
	function get1( $field, $table, $term = array(), $orderby = '', $second = 0 )
	{
		$data = $this->get( $field, $table, $term, $orderby, 1, $second );
		return isset( $data[0] ) ? $data[0] : array();
	}

	function replace( $table, $array )
	{
		return $this->command( 'replace into ' . $table . ' ( `' . join( '`, `', array_keys( $array ) ) . "` ) values ( '" . join( "', '", $array ) . "' )" );
	}
	
	// 是否存在符合条件的数据，返回true|false
	function exists( $table, $term = array() )
	{
		$sql = "select 1 from $table";
		$where = $this->term( $term );
		if( $where != '' ) $sql .= " where $where";
		$data = $this->select( $sql );
		return isset( $data[0] );
	}

	/*
	 * 输出当前sql语句，以便调试
	 */
	function debug()
	{
		echo '<pre>';
		print_r( $this->sql );
		echo '</pre>';
	}
}
?>