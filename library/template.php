<?php
class template
{
	var $data = array();
	var $auto = array();
	var $path = '';

	function __construct()
	{
		$this->path = config('dir.root') . 'template/';
		$this->appoint( $_GET );
	}

	function assign( $key, $value )
	{
		$this->data[$key] = $value;
	}

	function appoint( $array )
	{
		foreach( $array as $key => $value ) $this->assign( $key, $value );
	}

	function parse( $tpl )
	{
		//extract( $this->auto );
		extract( $this->data );
		ob_start();
		require $this->path . $tpl ;
		$_reserve_contents_reserve_ = ob_get_contents();
		ob_end_clean();
		return $_reserve_contents_reserve_;
	}

	/**
	 * @param string $content_file：内容文件名
	 * @param string $header_file：头文件名，如不必头，请使用parse()
	 * @param string $footer_file：尾文件名，同上
	 * @return string
	 */
//	function result( $content_file = '', $header_file = 'header.php', $footer_file = 'footer.php' )
//	{
//		return $this->parse( $header_file ) . $this->parse( $content_file ) . $this->parse( $footer_file );
//	}
}
?>