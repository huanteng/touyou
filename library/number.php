<?php
class number
{
	
	/* 取整，使用四舍五入
	 */
	function round( $num )
	{		
		return round( $num );
	}

	/* 根据设置概率，随机返回结果
	 * 参数：
	 *	percent：为一个少于100的数
	 * 返回值：
	 *	满足条件时，返回true，否则false
	 */
	function maybe( $percent )
	{
		return mt_rand( 0, 100 ) < $percent ;
	}

}
?>