<?php
class cache
{
	var $memcache = null;
	var $debug = false;

	function __construct()
	{
		$this->memcache = new memcache();
		$this->memcache->addServer( '127.0.0.1', 11211 );
		register_shutdown_function( array( & $this, 'disconnect' ) );
	}

	function set( $key, $data, $expire = 900, $zip = 0 )
	{
		if( $this->debug )
		{
			biz('base')->debug( "set $key = " . print_r( $data, true ) . " , $expire" );
		}
		return $this->memcache->set( $key, $data, $zip, $expire );
	}

	function get( $key, $default = FALSE )
	{
		$data = $this->memcache->get( $key );
		if( !$data ) $data = $default;

		if( $this->debug )
		{
			biz('base')->debug( "get $key = " . print_r( $data, true ) );
		}

		return $data;
	}

	function del( $key )
	{
		if( $this->debug )
		{
			biz('base')->debug( 'del ' . $key );
		}
		return $this->memcache->delete( $key );
	}

	function clear()
	{
		return $this->memcache->flush();
	}

	function disconnect()
	{
		$this->memcache->close();
	}
}
?>