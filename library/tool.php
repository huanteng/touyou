<?php
class tool
{
	function qq_notice( $message, $receiver = '94406121', $type = 1, $expire = 0, $other = '' )
	{
		if( config( 'develop' ) )
		{
			return;
		}

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, "content=" . $message . "&sender=&receiver=" . $receiver . "&type=" . $type . '&project=2&expire=' . $expire . '&other=' . $other );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_URL, "http://www.qqdalaba.com/interface/up2.php" );
		$result = curl_exec( $ch );
		curl_close( $ch );
	}

	function replace_badword( $text )
	{
		$from = array();
		$to = array();
		$db = load( 'db' );
		$badword_array = $db->select( 'select * from badword' );

		foreach( $badword_array as $value )
		{
			$from[] = $value['old'];
			$to[] = $value['new'];
		}

		return str_replace( $from, $to, $text );
	}

	function ip_info( $ip = '', $type = 1 )
	{
		$info = array();
		$ip = $ip != '' ? $ip : $_SERVER['REMOTE_ADDR'];

		if ( $type == 1 )
		{
			$content = mb_convert_encoding( $this->get_or_post( 'http://www.hujuntao.com/api/ip/ip.php?ip=' . $ip ), 'UTF-8', 'AUTO' );
			$content = str_replace( 'var IPData = new Array', '$info = array', $content );
			eval( $content );
			$info = $info[1] . $info[2] . $info[3];
		}
		else if ( $type == 2 )
		{
			$content = mb_convert_encoding( $this->get_or_post( 'http://www.youdao.com/smartresult-xml/search.s?type=ip&q=' . $ip ), 'UTF-8', 'GBK' );
			$match = array();
			preg_match( '/<location>(.*?)<\/location>/is', $content, $match );

			if ( isset( $match[0] ) && $match[0] != '' )
			{
				$match = explode( ' ', $match[0] );
				$info = $match[0];
			}
		}

		return $info;
	}

	function rand_code( $length = 8, $type = 0 )
	{
		$text = '';
		$char[0] = '0123456789';
		$char[1] = 'abcdefghijkmnopqrstuvwxyz023456789ABCDEFGHIJKMNOPQRSTUVWXYZ';
		$char[2] = 'abcdefghijklmnopqrstuvwxyz';
		$char[3] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$char[4] = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$size = strlen( $char[$type] );
		while( $length-- > 0 ) $text .= $char[$type][mt_rand( 0, $size - 1 )];
		return $text;
	}

	function sub_gb2312( $text, $start = 0, $length = 16, $suffix = '' )
	{
		$temp = '';
		$real_length = $start + $length;
		for( $i = 0; $i < $real_length; $i++ )
		$temp .= ord( substr( $text, $i, 1 ) ) > 0xa0 ? substr( $text, $i++, 2 ) : substr( $text, $i, 1 );
		return $temp . ( strlen( $text ) > $length ? $suffix : '' );
	}

	function sub_utf8($str,$lenth,$ex='')
	{
		$start = 0;
		$len = strlen($str);
		$r = array();
		$n = 0;
		$m = 0;
		for($i = 0; $i < $len; $i++) {
			$x = substr($str, $i, 1);
			$a = base_convert(ord($x), 10, 2);
			$a = substr('00000000'.$a, -8);
			if ($n < $start){
				if (substr($a, 0, 1) == 0) {
				}elseif (substr($a, 0, 3) == 110) {
				   $i += 1;
				}elseif (substr($a, 0, 4) == 1110) {
				   $i += 2;
				}
				$n++;
			}else{
				 if (substr($a, 0, 1) == 0) {
					 $r[] = substr($str, $i, 1);
				 }elseif (substr($a, 0, 3) == 110) {
					 $r[] = substr($str, $i, 2);
					 $i += 1;
				 }elseif (substr($a, 0, 4) == 1110) {
					$r[] = substr($str, $i, 3);
					$i += 2;
				 }else{
					  $r[] = '';
				 }
				if (++$m >= $lenth){
					break;
				}
			}
		}
		return implode("",$r).$ex;
	}

	function unique_array( $array )
	{
		foreach( $array as $key => $value )	$old[$key] = serialize( $value );
		$temp = array_unique( $old );
		foreach( $temp as $key => $value ) $new[$key] = unserialize( $value );
		return $new;
	}

	function sort_array( & $array, $key, $type )
	{
		$sort = array();
		foreach( $array as $data ) $sort[] = $data[$key];
		array_multisort( $sort, $type, $array );
	}

	function shuffle_array( $old )
	{
		$new = array();

		while( count( $old ) > 0 )
		{
			$key = array_rand( $old );
			$new[] = $old[$key];
			unset( $old[$key] );
		}

		return $new;
	}

	function get_ip( $server )
	{
		$ip = '';
		$data = array( 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR' );

		foreach( $data as $value )
		{
			if ( isset( $server[$value] ) )
			{
				$ip = $server[$value];
				break;
			}
		}

		return $ip;
	}

	function copy_directory( $source, $dest, $over_write = true )
	{
		if ( $handle = opendir( $source ) )
		{
			if ( ! is_dir( $dest ) ) if ( ! $this->create_directory( $dest, 0777 ) ) return false;

			while( $file = readdir( $handle ) )
			{
				if ( $file != '.' && $file != '..')
				{
					$path = $source . '/' . $file;

					if ( is_dir( $path ) )
					{
						$this->copy_directory( $path, $dest . '/' . $file, $over_write );
					}
					else
					{
						if ( ! is_file( $dest . '/' . $file ) || $over_write ) if ( ! copy( $path, $dest . '/' . $file ) ) return false;
					}
				}
			}

			return closedir( $handle );
		}
		else
		{
			return false;
		}
	}

	function delete_directory( $dir )
	{
		if ( $handle = opendir( $dir ) )
		{
			while( false !== ( $file = readdir( $handle ) ) )
			{
				if ( $file != '.' && $file != '..' )
				{
					if ( is_dir( $dir . '/' . $file ) )
					{
						$this->delete_directory( $dir . '/' . $file );
					}
					else
					{
						unlink( $dir . '/' . $file );
					}
				}
			}

			closedir( $handle );
			rmdir( $dir );
		}
	}

	function resize_image( $file_name, $width, $height, $style, $target_file, $water = '', $position = 3, $offset = 5, $transition = 45 )
	{
		if ( file_exists( $file_name ) )
		{
			// 20121109，加if
			if( filesize( $file_name ) < 1 ) return false;

			$info = @ getimagesize( $file_name );
		}
		if ( ! isset( $info['mime'] ) || $info['mime'] == '' ) return false;

		$name = explode( '/', $info['mime'] );
		$function = 'imagecreatefrom' . $name[1];

		if ( function_exists( $function ) )
		{
			$image = $function( $file_name );

			if ( $style == 1 )
			{
				$dst_x = 0;
				$dst_y = 0;

				if ( $info[0] < $info[1] )
				{
					$dst_w = $width;
					$dst_h = round( $width * $info[1] / $info[0] );
					$dst_y = ( $height - $dst_h ) / 2;
				}
				else
				{
					$dst_h = $height;
					$dst_w = round( $height * $info[0] / $info[1] );
					$dst_x = ( $width - $dst_w ) / 2;
				}

				$handle = imagecreatetruecolor( $width, $height );
				$white = imagecolorallocate( $handle, 255, 255, 255 );
				imagefill( $handle, 0, 0, $white );
				imagecopyresampled( $handle, $image, $dst_x, $dst_y, 0, 0, $dst_w, $dst_h, $info[0], $info[1] );
			}
			else if ( $style == 2 )
			{
				$dst_x = 0;
				$dst_y = 0;
				$dst_w = $width;
				$dst_h = $height;

				if ( ( $width / $height - $info[0] / $info[1] ) >= 0 )
				{
					$dst_w = $info[0] * ( $height / $info[1] );
					$dst_x = ( $width - $dst_w ) / 2;
				}
				else
				{
					$dst_h = $info[1] * ( $width / $info[0] );
					$dst_y = ( $height - $dst_h ) / 2;
				}

				$handle = imagecreatetruecolor( $width, $height );
				$white = imagecolorallocate( $handle, 255, 255, 255 );
				imagefill( $handle, 0, 0, $white );
				imagecopyresampled( $handle, $image, $dst_x, $dst_y, 0, 0, $dst_w, $dst_h, $info[0], $info[1] );
			}

			if ( $water != '' )
			{
				$water_handle = imagecreatefromstring( file_get_contents( $water ) );
				$water_info = getimagesize( $water );
				$dict[0] = array( 'x' => ( $width - $water_info[0] ) / 2, 'y' => ( $height - $water_info[1] ) / 2 );
				$dict[1] = array( 'x' => $offset, 'y' => $offset );
				$dict[2] = array( 'x' => $width - $water_info[0] - $offset, 'y' => $offset );
				$dict[3] = array( 'x' => $width - $water_info[0] - $offset, 'y' => $height - $water_info[1] - $offset );
				$dict[4] = array( 'x' => $offset, 'y' => $height - $water_info[1] - $offset );
				$dict[5] = array( 'x' => ( $width - $water_info[0] ) / 2, 'y' => $offset );
				$dict[6] = array( 'x' => $width - $water_info[0] - $offset, 'y' => ( $height - $water_info[1] ) / 2 );
				$dict[7] = array( 'x' => ( $width - $water_info[0] ) / 2, 'y' => $height - $water_info[1] - $offset );
				$dict[8] = array( 'x' => $offset, 'y' => ( $height - $water_info[1] ) / 2 );
				$dict[9] = array( 'x' => mt_rand( 0, $width - $offset ), 'y' => mt_rand( 0, $height - $offset ) );
				imagecopymerge( $handle, $water_handle, $dict[$position]['x'], $dict[$position]['y'], 0, 0, $water_info[0], $water_info[1], $transition );
				imagedestroy( $water_handle );
			}

			if ( $target_file == '' ) header( 'content-type: ' . $info['mime'] );
			$function = 'image' . $name[1];
			$result = $target_file == '' ? $function( $handle ) : $function( $handle, $target_file );
			if ( $target_file != '' ) chmod( $target_file, 0777 );
			imagedestroy( $handle );
			return $result;
		}
	}

	function unique_number( $length = 32 )
	{
		$second = explode( ' ', microtime() );
		$micro = explode( '.', $second[0] );
		$id = $second[1] . $micro[1];
		$length -= strlen( $id );
		while( $length-- > 0 ) $id .= mt_rand( 0, 9 );
		return $id;
	}

	function validate_picture( $content, $font_r = 0, $font_g = 0, $font_b = 0, $back_r = 255, $back_g = 255, $back_b = 255, $width = 80, $height = 20, $type = 'png' )
	{
		$image = imagecreate( $width, $height );
		$back = imagecolorallocate( $image, $back_r, $back_g, $back_b );
		$font = imagecolorallocate( $image, $font_r, $font_g, $font_b );
		$size = strlen( $content );
		for( $i = 0; $i < $size; $i++ )	imagestring( $image, 5, $i * $width / 4 + 5, 4, $content[$i], $font );
		header( 'content-type: image/' . $type );
		$function = 'image' . $type;
		$function( $image );
		imagedestroy( $image );
	}

	function escape( $str )
	{
		preg_match_all( "/[\x80-\xff].|[\x01-\x7f]+/", $str, $newstr );
		$ar = $newstr[0];

		foreach( $ar as $k => $v )
		{
			if ( ord( $ar[$k] ) >= 127 )
			{
				$tmpString = bin2hex( iconv( 'GBK', 'ucs-2', $v ) );
				if ( !eregi( 'WIN', PHP_OS ) ) $tmpString = substr( $tmpString, 2, 2 ) . substr( $tmpString, 0, 2 );
				$reString .= "%u" . $tmpString;
			}
			else
			{
				$reString .= rawurlencode( $v );
			}
		}

		return $reString;
	}

	function unescape( $str )
	{
		$str = rawurldecode( $str );
		preg_match_all( "/%u.{4}|&#x.{4};|&#d+;|.+/U", $str, $r );
		$ar = $r[0];

		foreach( $ar as $k => $v )
		{
			if ( substr( $v, 0, 2 ) == "%u" )
			$ar[$k] = iconv( "UCS-2", "GBK", pack( "H4", substr( $v, -4 ) ) );
			else if ( substr( $v, 0, 3 ) == "&#x" )
			$ar[$k] = iconv( "UCS-2", "GBK", pack( "H4", substr( $v, 3, -1 ) ) );
			else if ( substr( $v, 0, 2 ) == "&#" )
			$ar[$k] = iconv( "UCS-2", "GBK", pack( "n", substr( $v, 2, -1 ) ) );
		}

		return join( "", $ar );
	}

	function xml_to_array( $contents, $get_attributes = 0 )
	{
		$parser = xml_parser_create();
		xml_parser_set_option( $parser, XML_OPTION_CASE_FOLDING, 0 );
		xml_parser_set_option( $parser, XML_OPTION_SKIP_WHITE, 1 );
		xml_parse_into_struct( $parser, $contents, $xml_values );
		xml_parser_free( $parser );
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();
		$current = & $xml_array;

		foreach( $xml_values as $data )
		{
			unset( $attributes, $value );
			extract( $data );
			$result = '';

			if ( $get_attributes )
			{
				$result = array();
				if ( isset( $value ) ) $result['value'] = $value;
				if ( isset( $attributes ) )	foreach( $attributes as $attr => $val ) $result['attr'][$attr] = $val;
			}
			elseif ( isset( $value ) )
			{
				$result = $value;
			}

			if ( $type == 'open' )
			{
				$parent[$level-1] = & $current;

				if ( ! is_array( $current ) || ( ! in_array( $tag, array_keys( $current ) ) ) )
				{
					$current[$tag] = $result;
					$current = & $current[$tag];

				}
				else
				{
					if ( isset( $current[$tag][0] ) )
					{
						array_push( $current[$tag], $result );
					}
					else
					{
						$current[$tag] = array( $current[$tag], $result );
					}

					$last = count( $current[$tag] ) - 1;
					$current = & $current[$tag][$last];
				}

			}
			elseif ( $type == 'complete' )
			{
				if ( ! isset( $current[$tag] ) )
				{
					$current[$tag] = $result;

				}
				else
				{
					if ( (is_array( $current[$tag] ) && $get_attributes == 0 )
							|| ( isset( $current[$tag][0] ) && is_array( $current[$tag][0] ) && $get_attributes == 1 ) )
					{
						array_push( $current[$tag], $result );
					}
					else
					{
						$current[$tag] = array( $current[$tag], $result );
					}
				}

			}
			elseif ( $type == 'close' )
			{
				$current = & $parent[$level-1];
			}
		}

		return( $xml_array );
	}

	function http_get( $domain, $url, $port = 80 )
	{
		 $fp = fsockopen( $domain, $port, $errno, $errmsg );
		 fputs( $fp, "GET " . $url . " HTTP/1.0\r\nHost: " . $domain . "\r\nConnection: Close\r\n\r\n" );
		 fclose( $fp );
	}

	function object_to_array( $object )
	{
		$member = get_object_vars( $object );
		$data = array();
		foreach( $member as $name => $value ) $data[$name] = $value;
		return $data;
	}

	//获取指定标记中的内容
	function get_tag_data($str, $start, $end){
		if ( $start == '' || $end == '' ){
			return;
		}
		$str = explode($start, $str);
		if ( isset( $str[1] ) )
		{
			$str = explode($end, $str[1]);
			return $str[0];
		}
		else
		{
			return '';
		}
	}

	//格式化时间
	function format_time( $timestamp )
	{
		if ( $timestamp >= strtotime( date( 'Y-m-d' ) ) )
		{
			return date( 'H:i', $timestamp );
		}
		else if ( $timestamp >= ( strtotime( date( 'Y-m-d' ) ) - 86400 ) && $timestamp < strtotime( date( 'Y-m-d' ) ) )
		{
			return date( '昨天 H:i', $timestamp );
		}
		else
		{
			return date( 'm-d H:i', $timestamp );
		}
	}
}
?>