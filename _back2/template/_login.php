<?php box( 'top', array( 'title' => '登录' ), 86400 );?>

<div class="container-fluid">
	<div class="row-fluid">

		<div class="row-fluid">
			<div class="span12 center login-header">
				<h2>Welcome to <?= config( 'site.name' ) ?>管理后台</h2>
			</div><!--/span-->
		</div><!--/row-->

		<div class="row-fluid">
			<div class="well span5 center login-box">
				<div class="alert alert-info">
					输入帐号/密码/验证码
				</div>
				<form class="form-horizontal" method="post" action="_login.php?method=login">
					<fieldset>
						<div class="input-prepend" title="帐号" data-rel="tooltip">
							<span class="add-on"><i class="icon-user"></i></span><input autofocus class="input-large span10" name="username" id="username" type="text" value="" />
						</div>
						<div class="clearfix"></div>

						<div class="input-prepend" title="密码" data-rel="tooltip">
							<span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="password" id="password" type="password" value="" />
						</div>
						<div class="clearfix"></div>
						
						<div class="input-prepend" title="验证码" data-rel="tooltip">
							<span class="add-on" style="float:left;"><i class="icon-eye-open"></i></span>
							<input class="input-large span10" name="code" id="code" type="text" value=""  style="width:75px; float:left;" />
							<img src="code.php?rand=<?php echo microtime(); ?>" border="0" onclick= this.src="code.php?rand="+Math.random() style="CURSOR: pointer; float:left;margin-top:4px;" title="看不清？点击更换另一个验证码。" />
						</div>
						<div class="clearfix"></div>

						
						<div class="clearfix"></div>

						<p class="center span5">
							<button type="submit" class="btn btn-primary">登录</button>
						</p>
					</fieldset>
				</form>
			</div><!--/span-->
		</div><!--/row-->
	</div><!--/fluid-row-->

</div><!--/.fluid-container-->

<?php box( 'bottom', '', 86400 ); ?>