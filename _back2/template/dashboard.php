<?php box( 'top',  array( 'title' => '主界面' ), 86400 ); ?>

<div class="container-fluid">
	
<div>
	<ul class="breadcrumb">
		<li></li>
		<li></li>
	</ul>
</div>	
	
<div class="sortable row-fluid">
	<a data-rel="tooltip" title="6 new members." class="well span3 top-block" href="users.php">
		<span class="icon32 icon-red icon-user"></span>
		<div>用户数</div>
		<div><?=$user?></div>
		<span class="notification"><?=$today_user?></span>
	</a>

	<a data-rel="tooltip" title="4 new pro members." class="well span3 top-block" href="users.php?is_npc=0">
		<span class="icon32 icon-color icon-star-on"></span>
		<div>VIP</div>
		<div><?=$vip?></div>
		<span class="notification green"><?=$today_vip?></span>
	</a>

	<a data-rel="tooltip" title="$34 new sales." class="well span3 top-block" href="payment.php">
		<span class="icon32 icon-color icon-cart"></span>
		<div>充值</div>
		<div><?=$money?></div>
		<span class="notification yellow"><?=$percent?></span>
	</a>

	<a data-rel="tooltip" title="12 new messages." class="well span3 top-block" href="#">
		<span class="icon32 icon-color icon-envelope-closed"></span>
		<div>警告</div>
		<div><?=$warning?></div>
		<span class="notification red"><?=$today_warning?></span>
	</a>
</div>

</div>

<?php box( 'bottom', '', 86400 ); ?>