<?php box( 'top',  array( 'title' => '报警管理' ), 86400 ); ?>
<title>报警系统</title>
<?php include_once('_list.php'); ?>

<script type="text/javascript" src="../../js/jquery.timers-1.2.js"></script>
<script type="text/javascript">
function down()
{
	var i = parseInt( $('#timer').text() );

	if( i == 0 )
	{
		location.reload();
	}
	else
	{
		$('#timer').text( i - 1 );
	}
}

$(function(){
	$('#timer').everyTime('1s', 'timer', down)
		.click(function(){
			$(this).stopTime( 'timer' );
		});
});

//报警音乐播放
function play_warning(){
	$('body').append('<div style="display:none;"><audio controls="controls" autoplay="autoplay"><source src="/images/Pm.mp3" type="audio/mpeg" /></audio></div>');
}
</script>

<style>
.alert_red{background: red;}
.alert_yellow{background: yellow;}
.alert_blue{background: blue;}
.alert_gray{background: gray;}
.alert_green{background: green;}
</style>
<div class="row-fluid sortable" id="body">
	<div class="box span12">
		
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 报警系统</h2>
			&nbsp;
			<div class="box-icon"><div id="timer">300</div></div>
		</div>
		<div class="box-content">
		   	<form method="get">
				<div class="input-append">
					关键字：
					<input name="q" size="16" type="text" style="width:90px;">
					<button class="btn btn-success" type="submit">搜索</button>
				</div>
			</form>

			<table class="table table-striped table-bordered">
			  <thead>
			  <tr>
				  <th width="8%"><? up_down( 'id', 'ID' ) ?></th>
				  <th width="8%"><? up_down( 'task_id', '任务ID' ) ?></th>
				  <th><? up_down( 'content', '内容' ) ?></th>
				  <th width="100"><? up_down( 'time', '时间' ) ?></th>
				  <th>操作</th>
			  </tr>
			  </thead>
			  <tbody>
			  	<?php if( $task > 0 ) { ?>
					<style>.alert_-1{background: red;}</style>
					<tr class="alert_-1">
						<td></td>
						<td></td>
						<td>有 <?= $task ?> 个任务超过半小时未及时执行，请检查，或使用网页执行。如有必要请联系技术人员。</td>
						<td></td>
						<td>
							<a href="task.php">查看</a>
						</td>
					</tr>
				<?php } ?>
				<?php foreach($data as $k=>$v){?>
				<tr<?php if($v['color']!='') echo ' class="alert_'.$v['color'].'"'?>>
					<td><?=$v['id']?></td>
					<td><?=$v['task_id']?></td>
					<td><?=$v['content']?></td>
					<td><?=$v['time']?></td>
					<td>
						<?php if($v['task_id']){?><a href="#" onclick="post( 'task', 'check', {id: <?= $v['task_id'] ?>},
							reload );return false;" title="重新检查">重新检查</a><?php }?>
						<?php if($v['url']){?><a href="<?= $v['url'] ?>">查看</a><?php }?>
						<a href="#" onclick="return del( 'alert', <?= $v['id'] ?>)" title="删除"><span class="icon-trash"></span>删除</a>
					</td>
				</tr>
				<?php }?>
			  </tbody>
		  </table>
			<?php box( 'page', $nav, 0 ); ?>
		</div>
	</div><!--/span-->

	<fieldset>
		<legend>说明：</legend>
		<li></li>
	</fieldset>

</div><!--/row-->

<?php box( 'bottom', '', 86400 ); ?>
