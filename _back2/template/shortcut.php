<?php box( 'top',  array( 'title' => '快捷方式' ), 86400 ); ?>
<?php include_once('_list.php'); ?>

<script type="text/javascript">
	function doit( key )
	{
		if( confirm( '本操作可能存在较大风险，继续么？' ) )
		{
			post('shortcut', 'doit',{key: key});
		}

		return false;
	}
</script>

<div class="row-fluid sortable">
	<div class="box span12">
	  <div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i>快捷方式</h2>
	    </div>
		</div><!--/span-->

	<fieldset>
		<legend>说明：本功能仅供开发人员快捷操作，其它人员禁止使用。日常使用中，禁止在此实现伤害性功能，以免被误用。</legend>
		<?php foreach( $data as $key => $text ) { ?>
			<li><a href="#" onclick="return doit('<?= $key ?>');"><?= $text ?></a></li>
		<?php } ?>
	</fieldset>
</div><!--/row-->

<?php box( 'bottom', '', 86400 ); ?>