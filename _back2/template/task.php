<?php box( 'top',  array( 'title' => '任务管理' ), 86400 ); ?>
<?php include_once('_list.php'); ?>


<script type="text/javascript" src="../js/jquery.timers-1.2.js"></script>

<script>
function check( id )
{
	var func = function( data ) {
		var callback = function() {
			location.reload();
		}

		timer_alert( '运行成功', 3, callback );
	};

	if( confirm('执行这个任务吗？') )
	{
		post( 'task', 'check', { id: id }, func );
	}
}
</script>

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 任务管理</h2>
			<div class="box-icon"></div>
		</div>
		<div class="box-content">
		   	<form method="get">
				<div class="input-append">
					状态：<?=$status_select?>
					类型：
					<input name="type" size="16" type="text" style="width:90px;">
					关键字：
					<input name="q" size="16" type="text" style="width:90px;">
					<button class="btn btn-success" type="submit">搜索</button>
					<a href="?method=add" class="btn btn-info">增加</a>
				</div>
			</form>

			<table class="table table-striped table-bordered">
			  <thead>
			  <tr>
				  <th><? up_down( 'id', 'ID' ) ?></th>
				  <th><? up_down( 'type', '类型' ) ?></th>
				  <th><? up_down( 'title', '标题' ) ?></th>
				  <th><? up_down( 'data', '数据' ) ?></th>
				  <th width="100"><? up_down( 'run_time', '下次运行' ) ?></th>
				  <th><? up_down( 'min_second', '最小运行间隔' ) ?></th>
				  <th><? up_down( 'max_second', '最大运行间隔' ) ?></th>
				  <th width="40"><? up_down( 'status', '状态' ) ?></th>
				  <th width="110"><? up_down( 'remark', '备注' ) ?></th>
				  <th>操作</th>
			  </tr>
			  </thead>
			  <tbody>
				<?php foreach($data as $k=>$v){?>
				<tr>
					<td><?=$v['id']?></td>
					<td><?=$v['type']?></td>
					<td><?=$v['title']?></td>
					<td><?=$v['data']?></td>
					<td><?=$v['run_time']?></td>
					<td><?=$v['min_second']?></td>
					<td><?=$v['max_second']?></td>
					<td><?=$status[$v['status']]?></td>	
					<td><?=$v['remark']?></td>
					<td width="120">
						<a href="#" onclick="return check(<?= $v['id'] ?>)" title="运行">运行</a>
						<a href="?method=edit&id=<?= $v['id'] ?>" title="编辑"><span class="icon-edit"></span>编辑</a>
						<a href="#" onclick="return del( 'task', <?= $v['id'] ?>)" title="删除"><span class="icon-trash"></span>删除</a>
					</td>
				</tr>
				<?php }?>
			  </tbody>
		  </table>
			<?php box( 'page', $nav, 0 ); ?>
		</div>
	</div><!--/span-->

	<fieldset>
		<legend>说明：</legend>
		<li>任务系统会自动执行，如有超时没得到执行，属异常情况，<a href="timer.php?id=1&second=300" target="_blank">可点此处用网页执行</a>。</li>
	</fieldset>

</div><!--/row-->

<?php box( 'bottom', '', 86400 ); ?>
