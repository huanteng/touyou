<?php box( 'top',  array( 'title' => '编辑任务' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>

<link href="../css/jquery-ui-timepicker-addon.css" rel="stylesheet">
<script src="../js/jquery-ui-timepicker-addon.js"></script>
<script>
$(function(){
	$('#run_time').datetimepicker({
		dateFormat: "yy-mm-dd",
		timeFormat: "hh:mm:ss"
	});
});
</script>
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 编辑任务</h2>
			<div class="box-icon"><a href="task.php" class="btn btn-round"><i class="icon-remove"></i></a></div>
		</div>
		<div class="box-content">
			<form onsubmit="return submitit(this, 'task', 'edit_save' );">
				<table class="table table-striped table-bordered">
					<tr><td>类型</td><td><input type="text" name="type" value="<?= $type ?>" class="validate[groupRequired[type]]"></td><td></td></tr>
					<tr><td>标题</td><td><input type="text" name="title" value="<?= $title ?>" class="validate[groupRequired[title]]"></td><td>*标题至少要输入一个字。</td></tr>
					<tr><td>数据</td><td><input type="text" name="data" value="<?= $data ?>"></td>
					<td>&nbsp;</td>
					</tr>
					<tr><td>运行时间</td><td><input type="text" id="run_time" name="run_time" class="validate[groupRequired[run_time]]" value="<?= date('Y-m-d H:i:s',$run_time) ?>"></td><td></td></tr>
					<tr><td colspan="3">&nbsp;</td></tr>
					<tr><td>最小间隔</td><td><input type="text" name="min_second" value="<?=$min_second ?>" class="validate[groupRequired[min_second]]"></td><td>* 输入秒数</td>
					</tr>
					<tr><td>最大间隔</td><td><input type="text" name="max_second" value="<?=$max_second ?>" class="validate[groupRequired[max_second]]"></td><td>* 输入秒数</td>
					</tr>
					<tr><td>状态</td><td><?= $status_radio ?></td><td></td></tr>
					<tr><td colspan="3">&nbsp;</td></tr>
					<tr><td>LOG级别</td><td><input type="text" name="log" value="<?= $log ?>" class="validate[custom[integer]]" /> </td><td>运行时记录LOG。0表示不记录，9为最详尽。</td></tr>
					<tr><td>备注</td><td><input type="text" name="remark" value="<?= $remark ?>" /> </td><td></td></tr>
				</table>
				<div class="form-actions">
					<input type="hidden" name="id" value="<?= $id ?>" >
					<input type="submit" class="btn btn-primary" value="保存" />
					<a href="#" onclick="history.back();" class="btn">返回</a>
				</div>
			</form>
		</div>
	</div><!--/span-->

</div><!--/row-->
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script>
<script src="js/jquery.validationEngine.min.js"></script>
<script>$("form").validationEngine();</script>
<?php box( 'bottom', '', 86400 ); ?>