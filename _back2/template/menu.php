<?php box( 'top',  array( 'title' => '菜单' ), 86400 ); ?>

<div class="container-fluid" id='container'>
	<div style="float:left;">
<button class="btn btn-primary" onclick="top.location.reload();return false"><i class="icon-bell icon-white"></i> <?=$admin['username']?></button>
	</div>
<div class="btn-group  pull-right"  style="float:right;">
	<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
		<i class="icon-user"></i><span class="hidden-phone"> 管理员</span>
		<span class="caret"></span>
	</a>
	<ul class="dropdown-menu">
		<li><a target="mainFrame" href="password.php">修改密码</a></li>
		<li class="divider"></li>
		<li><a href="logout.php">退出</a></li>
	</ul>
	

</div>
</div>
	
<div class="box span10"  id='AllMenu'>
	<?php foreach($menu as $k => $v){?>
	<div class="box-header well" data-original-title id='box-header' k="<?=$k?>">
		<h2>
			<i class="icon-list"></i><?=$v['name']?>
			<i class="icon-minus"></i>
		</h2>
	</div>
	<?php foreach($v['child'] as $c){?>
	<div class="menu<?=$k?>" >
		<ul class="dashboard-list">
			<li style="text-align: center;"><a target="mainFrame" href="<?=$c['url'];?>"><?=$c['name']?></a></li>
		</ul>
	</div>
	<?php } }?>
</div><!--/span-->
<div style="float:left; _position:absolute; position:fixed; top:50%; margin-top:-25px; width:18px; height:50px; right:0px;"><button  class='btn' id='left-menu-right' style="width:18px; height:50px; margin:0px; padding:0px;"><i class='icon-circle-arrow-left'></i></button></div>
<script>
	$(function(){
		//菜单上下伸展
		$(".box-header").click(function(){  		
			var k = $(this).attr('k');
			if( $(".menu"+k).css('display') == 'block' ){
				$(this).find('.icon-minus').attr('class','icon-plus');
				$(".menu"+k).hide();
			}	else {
				$(this).find('.icon-plus').attr('class','icon-minus');
				$(".menu"+k).show();
			}			
		}); 
		//菜单左右伸展
		var flag=1;
		$("#left-menu-right").click(function(){ 
		if(flag==2)
		{
			flag=1;
			$(this).find('.icon-circle-arrow-left').attr('class','icon-circle-arrow-right');
			$("#container").hide();
			$("#AllMenu").hide();
			window.parent.frameset1.cols="2%,98%";
		}
		else
		{
			flag=2;
			$(this).find('.icon-circle-arrow-right').attr('class','icon-circle-arrow-left');
			$("#container").show();
			$("#AllMenu").show();
			window.parent.frameset1.cols="15%,85%";
		}
		});
	});
</script>
<?php box( 'bottom', '', 86400 ); ?>

	
	
	