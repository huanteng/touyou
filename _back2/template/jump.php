<?php box( 'top',  array( 'title' => '请稍候...' ), 86400 ); ?>
<?php if ($url) {?>
<meta http-equiv="refresh" content="<?php echo $time;?>;url=<?php echo $url;?>">
<?php } ?>
<script language="javascript">
function fresh ()
{
	$("#s").html($("#s").html()-1);
}

$(function(){
	setInterval("fresh()", 1000);
})
</script>

<div class="row-fluid">
	<div class="span4"></div>
	<div class="span4">
		
	<div class="box">
	<div class="box-header well">
		<h2><i class="icon-bullhorn"></i> 消息</h2>
		<div class="box-icon"></div>
	</div>
	<div class="box-content alerts">
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" onclick="window.history.go(-1)">×</button>
			<h4 class="alert-heading"><?php echo $text;?></h4>
			<p>将于<span class="red" id="s"><?php echo $time;?></span>秒后自动跳转到相应页面</p>
			<?php if ($url) { ?>
			<p><a href="<?php echo $url?>">如果您的浏览器没有自动跳转，请点击这里</a></p>
			<?php }else{?>
			<p><a href="#" onclick="window.history.go(-1)">点击这里立即跳转</a>，或<a href="/">跳转到首页</a></p>
			<?php } ?>
		</div>
	</div>
	</div>

	</div>
	<div class="span4"></div>
</div>


<?php box( 'bottom' );?>