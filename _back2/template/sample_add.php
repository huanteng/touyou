<?php box( 'top',  array( 'title' => '增加' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> <?=$table?>通用增加，请谨慎操作。每个字段要求必填。</h2>
			<div class="box-icon"><a href="#" onclick="history.back();" class="btn btn-round"><i class="icon-remove"></i></a></div>
		</div>
		<div class="box-content">
			<form onsubmit="return submitit(this, '<?=$table?>', 'add_save' );">
				<table class="table table-striped table-bordered">
					<?php
					$content = '';
					foreach( $info as $v )
					{
						$content .= '<tr>'
							. '<td><span class="left_title">' . $v . '</span></td>'
							. '<td><input name="' . $v . '" type="text" class="validate[required]" id="' . $v . '"></td>'
							. '<td>*</td>'
							. '</tr>';
					}
					echo $content;
					?>
				</table>
				<div class="form-actions">
				  <button type="submit" class="btn btn-primary">保存</button>
					<a href="#" onclick="history.back();" class="btn">返回</a>
				</div>
			</form>
		</div>
	</div><!--/span-->

</div><!--/row-->
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script>
<script src="js/jquery.validationEngine.min.js"></script>
<script>$("form").validationEngine();</script>
<?php box( 'bottom', '', 86400 ); ?>