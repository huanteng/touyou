<?php box( 'top',  array( 'title' => '列表' ), 86400 ); ?>
<?php include_once('_list.php'); ?>

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i><?=$table?></h2>
			<div class="box-icon"></div>
		</div>
		<div class="box-content">
			<table class="table table-striped table-bordered">
			 <?php
			 if( count( $data ) > 0 )
			 {
				 $content = '<thead><tr>';
				 $string = load( 'string' );
				 foreach( $data[ 0 ] as $k => $null )
				 {
					 $content .= '<th>' . up_down( $k, $k ) . '</th>';
				 }
				 $content .= '</tr></thead>';

				 foreach( $data as $info )
				 {
					 $content .= '<tr>';
					 foreach( $info as $v )
					 {
						 $content .= '<td>' . $v . '</td>';
					 }
					 $content .= '</tr>';
				 }

				 echo $content;
			 }
			 else
			 {
				 echo '<thead><tr><th>没有数据</th></tr></thead>';
			 }
			 ?>
		  </table>
		  <?php box( 'page', $nav, 0 ); ?>
		</div>
	</div><!--/span-->

	<fieldset>
		<legend>说明：</legend>
		<li></li>
	</fieldset>

</div><!--/row-->

<?php box( 'bottom', '', 86400 ); ?>
