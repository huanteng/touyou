<?php box( 'top',  array( 'title' => '帐单管理' ), 86400 ); ?>
<?php include_once('_list.php'); ?>
<script src="js/jquery.contextmenu.r2.packed.js"></script>
<script src="js/self.contextmenu.js"></script>

				
<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 账单管理</h2>
			<div class="box-icon"><a href="payment.php" class="btn btn-round"><i class="icon-repeat"></i></a></div>
		</div>
		<div class="box-content">
         <form action="payment.php" method="get" id="transForm" name="transForm">
                <div class="input-append">
                    关键字：
                    <input id="appendedInputButton" name="q" size="16" type="text" style="width:90px;">
                    开始日期：
					<input type="text" class="input-xlarge datepicker" name="start_date" id="start_date" style="width:90px;">
					结束日期：
					<input type="text" class="input-xlarge datepicker" name="end_date" id="end_date" style="width:90px;">
                    <button class="btn btn-success" type="submit">搜索</button>
                </div>
            </form>
            <fieldset>
                <legend></legend>
            </fieldset>
			<table class="table table-striped table-bordered">
			  <thead>
				  <tr>
					  <th><? up_down( 'id', 'ID' ) ?></th>
					  <th><? up_down( 'uid', '用户名' ) ?></th>
				      <th><? up_down( 'money', '金额' ) ?></th>
				      <th width="100"><? up_down( 'time', '创建时间' ) ?></th>
				      <th><? up_down( 'no', '帐单号' ) ?></th>
				      <th><? up_down( 'site', '子站' ) ?></th>
					  <th><? up_down( 'status', '状态' ) ?></th>
					  <th><? up_down( 'remark', '备注' ) ?></th>
					  <th>操作</th>
				  </tr>
			  </thead>
			  <tbody>
				<?php foreach($data as $v){?>
				<tr>
					<td><?=$v['id']?></td>
					<td class="center" context_menu_type="user" uid="<?= $v['uid'] ?>"><?=$v['username']?></td>
					<td class="center"><?=$v['money']?></td>
					<td class="center"><?=$v['created']?></td>
					<td class="center"><?=$v['no']?></td>
					<td class="center"><?=$v['site_name']?></td>
					<td class="center"><?=$status[$v['status']]?></td>
					<td class="center"><?=$v['memo']?></td>
					<td class="center">
					<?php if( $v['status'] == 0 ){ ?>
					    <a href="?method=edit&id=<?= $v['id'] ?>"><i class="icon-edit"></i>编辑</a>
					<?php } else { ?>
						<a href="#" onclick="post('payment', 'resend', {id:<?= $v['id'] ?>})">重发通知</a>
					<?php } ?>
					</td>
				</tr>
				<?php }?>  
			  </tbody>
		  </table> 
		  <?php box( 'page', $nav, 0 ); ?>
		</div>
	</div><!--/span-->

</div><!--/row-->

<?php box( 'bottom', '', 86400 ); ?>
