<?php box( 'top',  array( 'title' => '编辑菜单' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> 菜单管理</h2>
            <div class="box-icon"><a href="#" onclick="history.back()" class="btn btn-round"><i class="icon-remove"></i></a></div>
        </div>
        <div class="box-content">
			<form onsubmit="return submitit(this, 'admin_function', 'edit_save' );">
				<table class="table table-striped table-bordered">
					<tr><td>父级菜单</td><td><?= $parent_id ?></td><td>*</td></tr>
					<tr><td>菜单标题</td><td><input type="text" name="name" value="<?= $name ?>" class="validate[required]"></td><td>*</td></tr>
					<tr><td>菜单URL</td><td><input type="text" name="url" value="<?= $url ?>"></td><td></td></tr>
					<tr><td>菜单排序</td><td><input type="text" name="order" value="<?= $order ?>" class="validate[custom[integer],min[0]]"></td><td>数字越大排得越前</td></tr>
					<tr><td>菜单备注</td><td><input type="text" name="remark" value="<?= $remark ?>" /> </td><td></td></tr>
					<tr><td>是否显示</td><td><?= $display ?></td><td></td></tr>
				</table>
				<div class="form-actions">
					<input type="hidden" name="id" value="<?= $id ?>" >
					<button type="submit" class="btn btn-primary">保存</button>
					<a href="#" onclick="history.back()" class="btn">返回</a>
				</div>
			</form>
        </div>
    </div><!--/span-->

</div><!--/row-->
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script> 
<script src="js/jquery.validationEngine.min.js"></script>
<script>$("form").validationEngine();</script>
<?php box( 'bottom', '', 86400 ); ?>