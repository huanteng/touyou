<?php box( 'top',  array( 'title' => '编辑' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> <?=$table?>通用修改，请谨慎操作。每个字段要求必填。</h2>
			<div class="box-icon"><a href="#" onclick="history.back();" class="btn btn-round"><i class="icon-remove"></i></a></div>
		</div>
		<div class="box-content">
			<form onsubmit="return submitit(this, '<?=$table?>', 'edit_save' );">
				<table class="table table-striped table-bordered">
					<?php
					$content = '';
					foreach( $info as $k => $v )
					{
						$content .= '<tr>'
							. '<td><span class="left_title">' . $k . '</span></td>'
							. '<td><input name="' . $k . '" type="text" class="validate[required]" id="' . $k . '" value="' . $v . '"></td>'
							. '<td>*</td>'
							. '</tr>';
					}
					echo $content;
					?>
				</table>
				<div class="form-actions">
					<input type="submit" class="btn btn-primary" value="保存" />
					<a href="#" onclick="history.back();" class="btn">返回</a>
				</div>
			</form>
		</div>
	</div><!--/span-->

</div><!--/row-->
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script>
<script src="js/jquery.validationEngine.min.js"></script>
<script>$("form").validationEngine();</script>
<?php box( 'bottom', '', 86400 ); ?>