<?php box( 'top',  array( 'title' => '修改密码' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>
<script>
function success()
{
	alert('修改成功');
}
</script>
<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 修改密码</h2>
			<div class="box-icon"></div>
		</div>
		<div class="box-content">
			<form onsubmit="return submitit(this, 'password', 'edit_save', success );">
				<table class="table table-striped table-bordered">
					<tr><td>用户名</td><td><?= $admin['username'] ?></td></tr>
					<tr><td>原密码</td><td><input name="old_password" type="password" class="validate[required]"/></td></tr>
					<tr><td>新密码</td><td><input id="password" name="password" type="password" class="validate[required]"/></td></tr>
					<tr><td>确认新密码</td><td><input id="password2" name="password2" type="password" class="validate[required,equals[password]]"/></td></tr>
				</table>
				<div class="form-actions">
					<input type="hidden" name="id" value="<?= $admin['id'] ?>">
					<input type="hidden" name="method" value="edit_save">
					<input type="submit" class="btn btn-primary" value="保存"> 
					<a href="#" onclick="history.back();" class="btn">返回</a>
				</div>
			</form>
			</tbody>
			</table> 
		</div>
	</div><!--/span-->

</div><!--/row-->
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script>
<script src="js/jquery.validationEngine.min.js"></script>
<script>$("form").validationEngine();</script>
<?php box( 'bottom', '', 86400 ); ?>