<?php box( 'top',  array( 'title' => '日志管理' ), 86400 ); ?>
<?php include_once('_list.php'); ?>

<script src="js/jquery.contextmenu.r2.packed.js"></script>
<script type="text/javascript">
/**
 * 服务器处理后函数
 */
function after_post( log_id, data )
{
	if( data.code == 1 )
	{
		$('#tr' + log_id).hide();
		$('#tr' + log_id + '_2').hide();

		if( $('table').find('tbody').children(':visible').length == 0 )
		{
			reload();
		}
	}
	else
	{
		alert( '{memo}(code={code})'.format_key( data ) );
	}
}

/*统一的log post函数
 * 参数：
 * 	btn：当前按钮对象，由本对象获得log id
 * 	method：方法
 * 	data：发送数据，不必传入 log id，本函数会自动取
 */
function do_post_log( btn, data )
{
	var id = $(btn).closest('tr[log_id]').attr('log_id');
	data.id = id;
	var func = function( data )
	{
		after_post( id, data );
	}

	post( 'log', 'do_module', data, func )
}

// 右键时，当前行id
var context_menu_id = 0;

function banch_del( field )
{
	if( confirm( '将会执行批量删除，并且不可恢复。\n真的确认么？' ) )
	{
		post( 'log', 'banch_del', { field: field, id: context_menu_id }, reload );
	}
}

$( document ).ready( function() {
	$( "td[context_menu_type='log']" ).contextMenu( 'context_menu_log', {
		onContextMenu : function( e )
		{
			context_menu_id = $(e.target).closest('tr').children(':first').text();
			return true;
		},
		bindings :
		{
			'del_by_module' : function( t )
			{
				banch_del( 'module' );
			},
			'del_by_content' : function( t )
			{
				banch_del( 'content' );
			},

			'del_by_type' : function( t )
			{
				banch_del( 'type' );
			},
			'del_by_code' : function( t )
			{
				banch_del( 'code' );
			},
			'del_by_ip' : function( t )
			{
				banch_del( 'ip' );
			},
			'del_by_all' : function( t )
			{
				banch_del( 'all' );
			}
		}
	});
});
</script>

<div class="contextMenu hide" id="context_menu_log">
	<ul>
		<li id="del_by_module">按模块删除</li>
		<li id="del_by_content">按内容删除</li>
		<li id="del_by_type">按类型删除</li>
		<li id="del_by_code">按代码删除</li>
		<li id="del_by_ip">按IP删除</li>
		<li id="del_by_all">全部删除</li>
	</ul>
</div>

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> log管理</h2>
			<div class="box-icon"></div>
		</div>
		<div class="box-content">
	   	<form method="get">
		<div class="input-append">
			模块：<?=$module?>
			类型：<?=$type?>
			相关id：
			<input name="relate_id" type="text" id="relate_id" style="width:90px;" />
			代码：
			<input name="code" type="text" id="code" style="width:90px;" />
		  IP：
		  <input name="ip" type="text" id="ip" size="16" style="width:90px;" />
		  关键字：
		  <input name="q" size="16" type="text" style="width:90px;">
			<?php if( isset($hide) ) echo '<input name="hide" type="hidden">'; ?>
			<button class="btn btn-success" type="submit">搜索</button>
		 </div>
		  </form>

			<table class="table table-striped table-bordered">
			  <thead>
				  <tr>
					  <th><? up_down( 'id', 'ID' ) ?></th>
					  <th><? up_down( 'module', '模块' ) ?></th>
					  <th><? up_down( 'content', '内容' ) ?></th>
					  <th><? up_down( 'type', '类型' ) ?></th>
					  <th><? up_down( 'relate_id', '相关id' ) ?></th>
					  <th><? up_down( 'code', '代码' ) ?></th>
					  <th><? up_down( 'ip', 'IP' ) ?></th>
					  <th><? up_down( 'time', '时间' ) ?></th>
					  <th>操作</th>
				  </tr>
			  </thead>
			  <tbody>
				<?php foreach($data as $k=>$v){?>
				<tr id="tr<?=$v['id']?>">
					<td><?=$v['id']?></td>
					<td><?=$v['module']?></td>
					<td><?php
						echo $v['content'];

						if( !empty( $v['table'] ) )
						{
							echo '&nbsp;<a href="#" onclick="$(\'#tr' . $v[ 'id' ] . '_2\').toggle();return false">详情</a>';
						}
						?>
					</td>
					<td><?=$v['type']?></td>
					<td><a href="<?=$v['relate_url']?>" target="_blank"><?=$v['relate_id']?></a></td>
					<td><?=$v['code']?></td>
					<td><?=$v['ip']?></td>
					<td><?=$v['time']?></td>
					<td context_menu_type="log">
						<a href="#" onclick="return del( 'log', <?= $v['id'] ?>)" title="删除"><span class="icon-trash"></span></a>					</td>
				</tr>
				<?php if( isset( $v['table'] ) ) { ?>
				<tr<? if( isset($hide) ) echo ' class="hide"'; ?> id="tr<?= $v[ 'id' ] ?>_2" log_id="<?= $v[ 'id' ] ?>">
					<td colspan="9"><?= $v[ 'table' ] ?></pre></td>
				</tr>
				<?php } ?>
				<?php }?>
			  </tbody>
		  </table>
		  <?php box( 'page', $nav, 0 ); ?>
		</div>
	</div><!--/span-->

	<fieldset>
		<legend>说明：</legend>
		<li>在“操作”列右键，可以执行批量删除。</li>
	</fieldset>
</div><!--/row-->

<?php box( 'bottom', '', 86400 ); ?>