<?php box( 'top',  array( 'title' => '错误管理' ), 86400 ); ?>

<script type="text/javascript">
var is_stop = false;
var count = 0;

function view_data( data )
{
	var s = '';
	var l = data.length;

	for( var i = 0;i < l; ++i )
	{
		var d;
		d = data[i];
		s = '<hr><li>{0}, <a href="{1}" target="_blank">{1}</a></li>'.format( d.time, d.url );
		delete d.time;
		delete d.url;

		for( var k in d )
		{
			if( k == 'sql' )
			{
				for( var k_sql in d[k] )
				{
					if( typeof( d[k][k_sql] ) == 'object' )
					{
						s += '<ul>sql:{0}'.format( k_sql );
						var sql = d[k][k_sql];	// 批sql
						for( var k1_sql in sql )
							s += '<li>&nbsp;&nbsp;{0}：{1}</li>'.format( k1_sql, sql[k1_sql] );
						s += '</ul>';
					}
					else
					{
						s += '<li>sql:{0}, {1}</li>'.format( k_sql, d[k][k_sql] );
					}
				}
			}
			else
			s += '<li>{0}:{1}</li>'.format( k, JSON.stringify( d[k]) );
		}
		$("#body").prepend(s);
	}
}

function view()
{
	post( 'error', "view", {}, function(data) {
			data = data.data;
			if( data.length > 0 )
			{
				view_data( data );
			}
		}
	);
}

function clear()
{
	var func = function(data)
	{
		location.reload();
	}
	post( 'error', "clear", {}, func );
}

function get()
{
	if( is_stop ) return;

	$("#count").text("执行次数：" + count + "，运行久后，有些浏览器会变慢。如有必要请刷新。");
	++count;

	post( 'error', "get", {}, function(data) {
		data = data.data;
		if( data.length > 0 )
		{
			view_data( data );
		}
	} );
	setTimeout(get, 2000);
}
function start()
{
	is_stop = false;
	get();
}

function stop()
{
	is_stop = true;
}

function once()
{
	$('#body').html('');
	is_stop = false;
	get();
	is_stop = true;
}

$('#body').dblclick(function(){
	$(this).html('');
})

function save()
{
	var name_arr = [ 'cookie', 'url', 'postdata', 'content', 'sql' ];
	var data = {};

	for( var name in name_arr )
	{
		name = name_arr[ name ];
		var val = $('#'+name).val();
		if( val != '' )
		{
			data[ name ] = val;
		}
	}

	name_arr = [ 'log', 'save_postdata', 'save_content', 'save_sql' ];
	for( var name in name_arr )
	{
		name = name_arr[ name ];
		if($('#'+name+"1").attr("checked"))
		{
			data[ name ] = 1;
		}
	}

	post( 'error', "save", data,
		function (data)
		{
			alert(data.memo);
			location.reload();
		},function (jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		}
	);
}

function init()
{
	view();
}

$(function(){
	init();
});
</script>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> 错误管理</h2>
            <div class="box-icon"><a href="#" onclick="history.back();" class="btn btn-round"><i class="icon-remove"></i></a></div>
        </div>
        <div class="box-content">
            <form method="get">
                <div class="input-append">
					<a href="javascript:start();" class="btn btn-round">开始</a>
					<a href="javascript:stop();" class="btn btn-round">停止</a>
					<a href="javascript:once();" class="btn btn-round">执行一次</a>
					( <a href="javascript:view();">仅显示而不清空</a>  <a href="javascript:clear();">清空记录</a>）<div id="count"></div> <br>

					条件如下(多条件用半角逗号隔开)：<a href="javascript:save();" class="btn btn-round">保存</a><?php echo $log; ?><br><br>

					<?php
					$array = array( 'uid' => 'uid', 'url' => 'url', 'postdata' => 'postdata', 'content' => '内容', 'sql' => 'SQL' );

					$s = '';
					foreach ( $array as $name => $text ) {
						$s .= $text . '&nbsp&nbsp<input style="width:120px;" id="' . $name . '"';

						if( isset( $$name ) )
						{
							$s .= ' value="'. $$name .'"';
						}

						$s .= '/>&nbsp';
					}

					$s .= '<br><br>数据：';

					$s .= $save_postdata . $save_content . $save_sql;

					echo $s;
					?>

				</div>
            </form>

			<table class="table table-striped table-bordered">
				<tbody>
				<tr><td style="word-wrap:break-word"><div id='body' ondblclick="$(this).html('')"></div></td></tr>
				</tbody>
			</table>


        </div>
    </div><!--/span-->

</div><!--/row-->

<?php box( 'bottom', '', 86400 ); ?>