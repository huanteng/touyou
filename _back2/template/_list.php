<?php
/** 在表头显示向上向下箭头
 * @param $field：字段名
 * @param $text：显示文字
 */
function up_down( $field, $text )
{
	// 除orderby参数外的url
	static $base_url = '';

	if( $base_url == '' )
	{
		$temp = $_GET;
		$s = '';
		foreach( $temp as $k => $v )
		{
			if( $k != 'orderby' )
			{
				if( $s != '' )
				{
					$s .= '&';
				}

				$s .= $k . '=' . $v;
			}
		}
		$base_url = '?' . $s . ( $s == '' ? '' : '&' ) . 'orderby=' ;
	}

	// 旧排序
	$old = value( $_GET, 'orderby' );
	$old = explode( ' ', $old );

	// 新排序
	$new1 = explode( ' ', $field );

	if( $old[ 0 ] == $new1[ 0 ] )
	{
		if( isset( $old[ 1 ] ) )
		{
			$class = 'icon-arrow-down';
			$field = $old[ 0 ];
		}
		else
		{
			$class = 'icon-arrow-up';
			$field = $old[ 0 ] . ' desc';
		}
	}
	else
	{
		//$class = 'icon-resize-vertical';
		$class = '';
	}

	echo load('string')->format_key( '<a href="{base_url}{field}">{text}<i class="{class}"></i></a>',
		array( 'base_url' => $base_url, 'field' => $field, 'text' => $text, 'class' => $class )
	);
}
?>

<script type="text/javascript">
$(function(){
	<?php
	$s = '';
	foreach( $_GET as $k => $v )
	{
		$s .= "$('form:first').find('[name=\"$k\"]').val('" . addslashes( $v ) ."');";
	}
	echo $s;
	?>
});

function del( file, id )
{
	var func = function( data ) {
		if( data.code == 1 )
		{
			reload();
		}
		else
		{
			alert( '{memo}(代码：{code})'.format_key( data ) );
		}
	};

	if( confirm('真的删除吗？') )
	{
		post( file, 'del', { id: id }, func );
	}
}

function reload()
{
	location.reload();
}
</script>