<?php box( 'top',  array( 'title' => '帐单管理' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>
<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> 账单管理</h2>
            <div class="box-icon"><a href="#" onclick="history.back();" class="btn btn-round"><i class="icon-remove"></i></a></div>
        </div>
        <div class="box-content">
                <form action="payment.php" method="post" name="edit_form">
    				<input type="hidden" name="id" value="<?= $info['id'] ?>" />
					<input type="hidden" name="uid" value="<?= $info['uid'] ?>" />
					<input type="hidden"  name="money" value="<?= $info['money'] ?>" />
    				<input type="hidden" name="method" value="edit_save" />
                    <table class="table table-striped table-bordered">
						<tr><td>订单号</td><td><?= $info['billno'] ?></td><td>*</td></tr>
                        <tr><td>用户名</td><td><?= $info['username'] ?></td><td>*</td></tr>
                        <tr><td>money</td><td><?= $info['money'] ?></td><td>*</td></tr>
                        <tr><td>备注</td><td><input type="text" name="remark" value="<?= $info['remark'] ?>"/></td><td></td></tr>
					    <tr><td>显示</td><td><?= $info['status_form'] ?></td><td></td></tr>
                    </table> 
                    <div class="form-actions">
						<button type="submit" class="btn btn-primary">保存</button>
						<a href="payment.php" class="btn">返回</a>
					</div>  	
                </form>
        </div>
    </div><!--/span-->

</div><!--/row-->
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script> 
<script src="js/jquery.validationEngine.min.js"></script>
<script>$("form").validationEngine();</script>
<?php box( 'bottom', '', 86400 ); ?>