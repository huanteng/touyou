<div class="pagination pagination-centered">
<ul>
<?php 
if($page>1){?><li><a href="<?=$url ?>1">首页</a></li><li><a href="<?=$url . ($page>=2?$page-1:1) ?>">上一页</a></li>
<?php }
for($i=$start;$i<=$end;$i++){
?>
<li<?php if($page==$i)echo' class="active"';?>><a href="<?=$url . $i ?>"><?=$i?></a></li>
<?php } if($page < $pagecount){ ?>
<li><a href="<?=$url . ( $page+1<=$pagecount?$page+1:$pagecount )?>">下一页</a></li>
<?php }?>
<li><a href="<?=$url . $pagecount ?>">页数：<?=$pagecount?></a><a href="#">数量：<?=$total?></a></li>
<li>跳转到
   <input id="page" type="text" value="<?=$page?>" style="width:90px;"/>页
   <a href="#" id="go">GO</a></li>
</ul>
</div>
<script type="text/javascript">
	$('#go').click(function(){
		var page = $('#page').val();
		if( isNaN( page ) )
		{
			return false;
		}

		if( page < 1 )
		{
			page = 1;
		}
		else if( page > <?=$pagecount?> )
		{
			page = <?=$pagecount?>;
		}

		location = '<?=$url ?>' + page;

	});
</script>