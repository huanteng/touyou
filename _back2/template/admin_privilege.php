<?php box( 'top',  array( 'title' => '编辑权限' ), 86400 ); ?>
<!-- 定义相关函数 -->
<script type="text/javascript">
	$(function(){
		$("#checkAll").click(function(){		           
			$(":checkbox").attr("checked","checked");//全选    
		});
		$("#checkReturn").click(
			function(){        
			$(":checkbox").each(function(){
			if($(this).attr("checked"))
			{
				$(this).removeAttr("checked");                
			}
			else{
				$(this).attr("checked","checked"); 
			}
		});        
	});
	});
</script>
</head>
<body>
		
<div class="row-fluid">
	<form method="post" action="admin_privilege.php">
		<input type="hidden" name="admin" value="<?= $admin ?>">
		<input type="hidden" name="method" value="edit_save">
		
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> 权限编辑</h2>
            <div class="box-icon"><a href="admin.php" class="btn btn-success"><i class="icon-arrow-left"></i></a></div>
        </div>

		<div class="form-actions">
			<button type="button" class="btn btn-info" id="checkAll" >全选</button>
			<button type="button" class="btn btn-info"id="checkReturn"  >反选</button>
			&nbsp;&nbsp;
			<button type="submit" class="btn btn-primary">保存</button>
			<a href="admin.php" class="btn">返回</a>		
		</div>	
		<div class="span4 main-menu-span">
			<div class="well nav-collapse sidebar-nav">
				<?php foreach ($data as $k=>$v) {
					?>
					<ul class="nav nav-tabs nav-stacked main-menu">		
						<li><label class="checkbox"><input name="id[]" type="checkbox" value="<?= $v['info']['id'] ?>"<?= $v['info']['checked'] ? ' checked' : '' ?>><?= $v['info']['name'] ?>（ <?= $v['info']['id'] ?> ）</label></li>
						<?php foreach ($v['son'] as $v1) { ?>
							<li><label class="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="id[]" type="checkbox" value="<?= $v1['id'] ?>"<?= $v1['checked'] ? ' checked' : '' ?>><?= $v1['name'] ?>（ <?= $v1['id'] ?> ）</label></li>
						<?php } ?>
					</ul>
				<?php } ?>
			</div><!--/.well -->
		</div><!--/span-->
		
	</form>	
</div><!--/row-->
<?php box( 'bottom', '', 86400 ); ?>