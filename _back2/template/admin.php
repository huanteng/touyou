<?php box( 'top', array( 'title' => '管理员管理' ), 86400 );?>
<?php include_once('_list.php'); ?>
<script>
function copy( id )
{
	var func = function( data ) {
		if( data.code == 1 )
		{
			location = 'admin.php';
		}
		else
		{
			alert( '{memo}(代码：{code})'.format_key( data ) );
		}
	};

	if( confirm('真的复制这个管理员吗？') )
	{
		post( 'admin', 'copy', { id: id }, func );
	}
}	
</script>
<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 管理员管理</h2>
			<div class="box-icon"></div>
		</div>
		<div class="box-content">

			<table class="table table-striped table-bordered">
			  <thead>
				  <tr>
					  <th>ID</th>
					  <th>用户名</th>
				      <th width="100">最后登入时间</th>
					  <th>状态</th>
					  <th>操作</th>
				  </tr>
			  </thead>   
			  <tbody>
				<?php foreach($data as $k=>$v){?>  
				<tr>
					<td><?=$v['id']?></td>
					<td class="center"><?=$v['username']?></td>
					<td class="center"><?=date('Y-m-d H:i', $v['last_login_time'])?></td>
					<td class="center">
						<?=$v['status']==1?"正常":"关闭"?>
					</td>
					<td class="center">
						<a href="#" onclick="return copy(<?= $v['id'] ?>)"><i class="icon-edit"></i>复制</a>
						<a href="admin_privilege.php?id=<?=$v['id']?>"><i class="icon-edit"></i>权限</a>
					    <a href="?method=edit&id=<?= $v['id'] ?>"><i class="icon-edit "></i>编辑</a>
						<a href="#" onclick="return del( 'admin', <?= $v['id'] ?>)" title="删除"><i class="icon-trash"></i>删除</a>
					</td>
				</tr>
				<?php }?>  
			  </tbody>
		  </table>
			<?php box( 'page', $nav, 0 ); ?>
		</div>
	</div><!--/span-->

</div><!--/row-->

<?php box( 'bottom', '', 86400 ); ?>
