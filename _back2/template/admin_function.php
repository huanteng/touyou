<?php box( 'top',  array( 'title' => '菜单管理' ), 86400 ); ?>
<?php include_once('_list.php'); ?>

<script type="text/javascript">
function del_privilege( admin, privilege, username, privilege_name )
{
	if( confirm( '真要删除该用户权限么？\n用户名：' + username + '\n权限名：' + privilege_name ) )
	{
		var func = function(){
			location.reload();
		};
		post( 'admin_privilege', 'del_privilege', { admin: admin, privilege: privilege }, func );
	}
}

function delit( id )
{
	if( confirm('真的删除吗？') )
	{
		post( 'admin_function', 'del', { id: id }, reload );
	}

	return false;
}
</script>

<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 菜单管理</h2>

			<div class="box-icon"></div>
		</div>
		<div class="box-content">
            <form action="admin_function.php" method="get" id="transForm" name="transForm">
                <div class="input-append">
                    关键字：
                    <input id="appendedInputButton" name="q" size="16" type="text" style="width:90px;">
                    <button class="btn btn-success" type="submit">搜索</button>
                    <a href="?method=add" class="btn btn-info">增加</a>
                </div>
            </form>
            
            <table class="table table-striped table-bordered">
			  <thead>
				  <tr>
					  <th><? up_down( 'id', 'ID' ) ?></th>
					  <th><? up_down( 'parent_id', '父菜单' ) ?></th>
					  <th><? up_down( 'name', '菜单名称' ) ?></th>
				      <th><? up_down( 'url', 'URL' ) ?></th>
					  <th><? up_down( '`order` desc', '排序号' ) ?></th>
					  <th>上移</th>
					  <th>下移</th>
					  <th>允许用户</th>
					  <th>操作</th>
				  </tr>
			  </thead>   
			  <tbody>
				<?php foreach($data as $v){?>
				<tr>
					<td><?=$v['id']?></td>
					<td class="center"><a href="?parent_id=<?=$v['parent_id']?>"><?=$v['parent_name']?></a></td>
					<td class="center"><?=$v['name']?></td>
					<td class="center"><?=$v['url']?></td>
					<td class="center"><?=$v['order']?></td>
					<td class="center"><a href="#" onclick="return post( 'admin_function', 'updown', { id: <?=$v['id']?>, direct: 'up' } );"><i class="icon-arrow-up"></i></a></td>
					<td class="center"><a href="#" onclick="return post( 'admin_function', 'updown', { id: <?=$v['id']?>, direct: 'down' } );"><i class="icon-arrow-down"></i></a></td>
					<td class="center">
						<?php foreach( $v[ 'user' ] as $u ){?>
							<a href="#" onclick="return del_privilege( <?= $u['id'] ?>, <?= $v['id'] ?>, '<?= $u['username'] ?>', '<?=$v['name']?>' );"><?= $u['username'] ?></a>
						<?php }?>
					</td>
					<td class="center">
						<a href="?method=privilege&id=<?= $v['id'] ?>">分配权限</a>
						<a href="?method=edit&id=<?= $v['id'] ?>">
						<i class="icon-edit"></i>
						编辑	</a>
					 	<a href="#" onclick="return delit(<?= $v['id'] ?>)">
						<i class="icon-trash"></i>
						删除	</a>
					</td>
				</tr>
				<?php }?>  
			  </tbody>
		  </table>
			<?php box( 'page', $nav, 0 ); ?>
		</div>
	</div><!--/span-->

<fieldset>
                <legend></legend>
            </fieldset>
</div><!--/row-->

<?php box( 'bottom', '', 86400 ); ?>
