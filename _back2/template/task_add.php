<?php box( 'top',  array( 'title' => '增加任务' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th-list"></i> 添加任务</h2>
			<div class="box-icon"><a href="task.php" class="btn btn-round"><i class="icon-remove"></i></a></div>
		</div>
		<div class="box-content">
			<form onsubmit="return submitit(this, 'task', 'add_save' );">
				<table class="table table-striped table-bordered">
					<tr><td>类型</td><td><input type="text" name="type" class="validate[required]"></td><td></td></tr>
					<tr><td>标题</td><td><input type="text" name="title" class="validate[required]"></td><td>*标题至少要输入一个。</td></tr>
					<tr><td>最小间隔</td><td><input type="text" name="min_second" class="validate[custom[integer]]"></td>
						<td>* 请输入秒数</td>
					</tr>
					<tr><td>最大间隔</td><td><input type="text" name="max_second" class="validate[custom[integer]]"/> </td><td>* 请输入秒数</td></tr>
					<tr><td colspan="3">&nbsp;</td></tr>
					<tr><td>状态</td><td><?= $status ?></td>
						<td>&nbsp;</td>
					</tr>
					<tr><td>LOG级别</td><td><input type="text" name="log" value="9" class="validate[custom[integer]]" /> </td><td>运行时记录LOG。0表示不记录，9为最详尽。</td></tr>
					<tr><td>备注</td><td><input type="text" name="remark"></td>
						<td>&nbsp;</td>
					</tr>
				</table>
				<div class="form-actions">
				  <button type="submit" class="btn btn-primary">保存</button>
					<a href="#" onclick="history.back();" class="btn">返回</a>
				</div>
			</form>
		</div>
	</div><!--/span-->

</div><!--/row-->
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script>
<script src="js/jquery.validationEngine.min.js"></script>
<script>$("form").validationEngine();</script>
<?php box( 'bottom', '', 86400 ); ?>