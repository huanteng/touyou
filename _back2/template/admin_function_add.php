<?php box( 'top',  array( 'title' => '增加菜单' ), 86400 ); ?>
<?php include_once('_edit.php'); ?>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> 增加菜单</h2>
            <div class="box-icon"><a href="#" onclick="history.back()" class="btn btn-round"><i class="icon-remove"></i></a></div>
        </div>
        <div class="box-content">
			<form onsubmit="return submitit(this, 'admin_function', 'add_save' );">
				<table class="table table-striped table-bordered">
					<tr><td>父级菜单</td><td><?= $parent_id ?></td><td>*</td></tr>
					<tr><td>菜单标题</td><td><input type="text" name="name" class="validate[required]"></td><td>*</td></tr>
					<tr><td>菜单URL</td><td><input type="text" name="url"></td><td></td></tr>
					<tr><td>菜单排序</td><td><input type="text" name="order" class="validate[custom[integer],min[0]]" value="0"></td><td>数字越大排得越前</td></tr>
					<tr><td>菜单备注</td><td><input type="text" name="remark" /> </td><td></td></tr>
				</table>
				<div class="form-actions">
				  <button type="submit" class="btn btn-primary">保存</button>
					<a href="#" onclick="history.back()" class="btn">返回</a>
			  </div>
			</form>
        </div>
    </div><!--/span-->

</div><!--/row-->
<link rel="stylesheet" href="css/validationEngine.jquery.css">
<script src="js/jquery.validationEngine-zh-CN.js"></script> 
<script src="js/jquery.validationEngine.min.js"></script>
<script>$("form").validationEngine();</script>
<?php box( 'bottom', '', 86400 ); ?>