<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'shortcut';
		$this->privilege = '108';
	}

	function data()
	{
		// 定义可执行的快捷方式
		return array(
			'init_pwd' => '初始化登录密码',
			'clear_log' => '清空log，备份前可用',
			'clear_alert' => '清空alert',
		);
	}

	function home( $in )
	{
		//$this->check_privilege();
		$data = $this->data();

		return $this->out( array( 'data' => $data ) );
	}

	function doit( $in )
	{
		//$this->check_privilege();
		$key = $in[ 'key' ];
		$data = $this->data();

		if( !isset( $data[ $key ] ) )
		{
			return $this->ajax_out( -1, '【错误】未定义操作' );
		}

		$db = load('db');
		switch( $key )
		{
			case 'init_pwd':
				biz('admin')->set_by_term( 'password=\'' . biz('admin')->hash('111111') . '\'', 'username=\'dda\'' );
				break;

			case 'clear_log':
				$db->command( 'truncate table log' );
				break;

			case 'clear_alert':
				$db->command( 'truncate table alert' );
				break;


		}

		return $this->ajax_out( 1, '操作成功' );
	}
}

$action = new action();
$action->run();
?>
