<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'payment';
    }

    function home( $in )
    {
		$this->check_privilege( '176' );
		$in += array(
			'orderby' => 'created desc',
		);

        $payment = biz( 'payment' );
        $equal = array( 'id' );
        $like = array( );
        $q = array( 'username', 'billno' );
		$other = array();
        if( value( $in, 'start_date' ) != '' )
        {
			$start = strtotime( $in['start_date'] );
			$end = strtotime( $in['end_date'] );
			$other = array( 'created between ' . $start . ' and ' . $end );
        }

        $data = $payment->search( $in, $equal, $like, $q, array( 'term' => $other ) );

		//$users = biz( 'users' );
		$site = biz( 'site' );
		$time = load( 'time' );
		foreach( $data[ 'data' ] as &$v )
		{
			/*
			if( $v[ 'uid' ] == 0 )
			{
				$username = '';
			}
			else
			{
				$username = $users->get_name_from_id( $v[ 'uid' ] );
			}
			*/
			$v[ 'username' ] = '';//$username;

			if( $v[ 'site' ] == 0 )
			{
				$site_name = '';
			}
			else
			{
				$site_info = $site->info( $v[ 'site' ] );
				$site_name = value( $site_info, 'name', '错误' );
			}
			$v[ 'site_name' ] = $site_name;

			$v[ 'created' ] = $time->format( $v[ 'created' ] );
		}

		$data[ 'status' ] = $payment->status_dict();
			
        return $this->out( $data );
    }

    function edit( $in )
    {
		$this->check_privilege( '176' );
        $payment = biz( 'payment' );
        $data[ 'info' ] = biz( 'payment' )->get_from_id( $in[ 'id' ] );
		$status=$payment->status_dict();
        unset($status[1],$status[2]);
		$data[ 'info' ][ 'status_form' ] = load( 'form' )->radio( 'status', $data[ 'info' ][ 'status' ], $status );
        return $this->out( $data );
    }

    function edit_save( $in )
    {
		$this->check_privilege( '176' );
		$users=biz('users');
		if( $in[ 'status' ] == 3 && $in['money'] > 0 )
		{
			$remark = $in['remark'] != '' ? '('.$in['remark'].')' : '';
			$users->add_money( $in['uid'], 1, $in['money'], $in['id'],  '手工确认'.$remark );
		}
  
		$this->log( 'payment', $in['id'], '操作账单状态为'.$in['status'] ) ;
		$result = biz( 'payment' )->set( $in );
		$msg = ( $result == 1 ) ? '修改成功' : "修改失败,请检查！";
		$this->jump( $msg);	
    }

	/** 重发通知
	 * @param $in
	 */
	function resend( $in )
	{
		biz('payment')->send( $in[ 'id' ] );
		return $this->ajax_out( 'ok' );
	}

}

$action = new action();
$action->run();
?>
