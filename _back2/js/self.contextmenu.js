var _current_context_menu_id_ = 0;
var _current_context_menu_uid_ = 0;
var _current_context_menu_tipsid_ = 0;

$( document ).ready( function()
{
	$( document.body ).append( '<div class="contextMenu" id="context_menu_user"><ul><li id="copy_uid_1">复制uid</li><li id="user_index">用户首页</li><li id="list_tips">推荐列表</li><li id="tips_report">推荐报表</li><li id="modify_info">修改信息</li><li id="sell_tips">查看卖出的推荐</li><li id="buy_tips">查看买入的推荐</li><li id="gold">金币记录</li></ul></div>' );
	$( "#context_menu_user" ).hide();

	$( "a[context_menu_type='user'],td[context_menu_type='user']" ).each
	(
		function( t )
		{
			$( this ).contextMenu
			(
				'context_menu_user',

				{
					onContextMenu : function( e )
					{
						_current_context_menu_uid_ = $( e.target ).attr( 'uid' );
						$( '#copy_uid_1' ).html( '复制uid（' + _current_context_menu_uid_ + '）' );
						return true;
					},

					onShowMenu : function( e, menu )
					{
						_current_context_menu_uid_ = $( e.target ).attr( 'uid' );
						return menu;
					},

					bindings :
					{
						'copy_uid_1' : function( t )
						{
							window.clipboardData.setData( "Text", _current_context_menu_uid_ );
							alert( '复制成功' );
						},

						'user_index' : function( t )
						{
							window.open( 'http://www.tuijie.cc/u' + _current_context_menu_uid_ );
						},

						'list_tips' : function( t )
						{
							window.location.href = 'tips.php?uid=' + _current_context_menu_uid_;
						},

						'sell_tips' : function( t )
						{
							window.location.href = 'sell_log.php?uid=' + _current_context_menu_uid_;
						},

						'buy_tips' : function( t )
						{
							window.location.href = 'sell_log.php?buyer_uid=' + _current_context_menu_uid_;
						},

//						'tips_report' : function( t )
//						{
//							window.location.href = '../tips_report/detail.php?uid=' + _current_context_menu_uid_;
//						},

						'modify_info' : function( t )
						{
							window.location.href = 'users.php?method=edit&uid=' + _current_context_menu_uid_;
						},

						'gold' : function( t )
						{
							window.location.href = 'balance.php?uid=' + _current_context_menu_uid_;
						}
					}
				}
			);
		}
	);

}
);