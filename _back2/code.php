<?php

require 'backend.php';

class action extends backend
{

	function home()
	{
		$name = isset($this->input['name']) ? $this->input['name'] : 'login_code';
		$code = mt_rand(1000, 9999) . '';
		$cookie = load('cookie');
		$cookie->set($name, $code, true);
		load('tool')->validate_picture($code);
	}

}

$action = new action();
$action->run();
?>