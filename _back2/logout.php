<?php

session_start();
require 'backend.php';

class action extends backend
{
	function home()
	{
		biz('admin')->logout();
		echo '<script>top.location="_login.php";</script>';
	}
}

$action = new action();
$action->run();
?>