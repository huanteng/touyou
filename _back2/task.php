<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'task';
	}

	function home( $in )
	{
		$this->check_privilege( '196' );
		$arr = load('arr');
		$task = biz('task');

		$in = $arr->set_default( $in, array(
			'pagesize' => 15,
			'orderby' => 'status,run_time'
		) );
		$_GET = $in;

		$equal = array( 'id','status');
		$like = array('type');
		$q = array('title', 'remark');
		$data = $task->search( $in, $equal, $like, $q );

		$dataset = load( 'dataset' );
		$data[ 'data' ] = $dataset->format_time( $data[ 'data' ], array( 'run_time', 'time' ) );
		$data[ 'data' ] = $dataset->cut( $data[ 'data' ], array( 'data' => 40, 'remark' => 20 ) );

		$data['status'] = $task->status_dict();
		$data['status_select'] = load('form')->select( 'status', '', $data['status'], array('empty'=>true, 'attr'=>'style="width:90px;" ') );
		
		return $this->out( $data );
	}

	function add( $in )
	{
		$this->check_privilege( '196' );
		$info[ 'status' ] = load( 'form' )->radio( 'status', 1, biz('task')->status_dict() );
		return $this->out( $info );
	}

	function add_save( $in )
	{
		$this->check_privilege( '196' );
		extract( $in );

		$task = biz( 'task' );

		if( $task->exists( array( 'title'=>$title ) ) )
		{
			return $this->ajax_out( -1, '标题重复' );
		}
		else
		{
			$task->add( $in );
			return $this->ajax_out( 1, '添加成功' );
		}
	}
	
	function edit( $in )
	{
		$this->check_privilege( '196' );
		$task = biz( 'task' );
		$info = $task->get_from_id( $in[ 'id' ] );
		$info = load('arr')->html_encode( $info, array( 'data', 'remark' ) );
		$info[ 'status_radio' ] = load( 'form' )->radio( 'status', $info[ 'status' ], $task->status_dict() );
		return $this->out( $info );
	}

	function edit_save( $in )
	{
		$this->check_privilege( '196' );
		$in['run_time'] = strtotime($in['run_time']);
		$result = biz( 'task' )->set( $in );

		return $result == 1 ? $this->ajax_out( 1, '修改成功' ) : $this->ajax_out( -1, '修改失败,请检查！' );
	}
	
	function del( $in )
	{
		$this->check_privilege( '196' );
		$result = biz( 'task' )->del( $in[ 'id' ] );
		return $result == 1 ? $this->ajax_out( 1, '删除成功' ) : $this->ajax_out( -1, '删除失败,请检查！' );
	}
	
	function check( $in )
	{
		$this->check_privilege( '196' );
		biz('task')->execute( array( 'id' => $in['id'] ), true );
		return $this->ajax_out( 1, '成功' );
	}
}

$action = new action();
$action->run();
?>
