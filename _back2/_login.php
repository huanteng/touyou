<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = '_login';
	}

	function home($in)
	{
		return $this->out(array());
	}

	function login($in)
	{
		if (!isset($in['username']) || $in['username'] == '') {
			$this->jump('用户名不能空');
		} else if (!isset($in['password']) || $in['password'] == '') {
			$this->jump('密码不能空');
		} else if (!isset($in['code']) || $in['code'] == '') {
			$this->jump('验证码不能空');
		} else {
			$result = biz('admin')->login($in);

			if( $result[ 'code' ] == 1 )
			{
				load('tool')->qq_notice( $this->input['username'] . ' 登录99goad后台', '94406121', 1 );
				$this->jump(null, 'main.php', 0, 0);
			}
			else
			{
				$this->jump( $result[ 'memo' ] );
			}
		}
	}

}

$action = new action();
$action->run();
?>