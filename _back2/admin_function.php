<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'admin_function';
	}

	function home( $in )
	{
		$this->check_privilege( '153' );
		$arr = load('arr');
		$in = $arr->set_default( $in, array(
			'orderby' => '`order` desc',
			'pagesize' => 15
		) );

		$_GET = $arr->set_default( $_GET, array(
			'orderby' => '`order` desc'
		) );

		if( $in[ 'orderby' ] != '`order`' && $in[ 'orderby' ] != '`order` desc' )
		{
			$in[ 'orderby' ] .= ',`order` desc';
		}

		$admin_function = biz( 'admin_function' );
		$admin = biz( 'admin' );
		$admin_privilege = biz( 'admin_privilege' );
		$equal = array( 'parent_id' );
		$like = array( );
		$q = array( 'name', 'url' );
		$data = $admin_function->search( $in, $equal, $like, $q );

		foreach( $data[ 'data' ] as &$v )
		{
			$privilege_data = $admin_privilege->get( 'admin', array( 'privilege' => $v[ 'id' ] ) );

			if( count( $privilege_data ) > 0 )
			{
				$ids = $admin->implode2( $privilege_data, '' );
				$v[ 'user' ] = $admin->get( 'id,username', array( 'id in(' . $ids . ')' ) );
			}
			else
			{
				$v[ 'user' ] = array();
			}
		}

		$data[ 'data' ] = load( 'dataset' )->fill_name( $data[ 'data' ], $admin_function, 'parent_id', 'parent_name' );

		return $this->out( $data );
	}

	function edit( $in )
	{
		$this->check_privilege( '153' );
		$admin_function = biz( 'admin_function' );
		$form = load( 'form' );

		$data = $admin_function->get_from_id( $in[ 'id' ] );
		$data[ 'display' ] = $form->radio( 'display', $data[ 'display' ], $admin_function->display_dict() );

		$parent = $admin_function->parent();
		$data[ 'parent_id' ] = $form->select( 'parent_id', $data[ 'parent_id' ], $parent, true );
		return $this->out( $data );
	}

	function edit_save( $in )
	{
		$this->check_privilege( '153' );
		$result = biz( 'admin_function' )->set( $in );
		return $result == 1 ? $this->ajax_out( 1, '修改成功' ) : $this->ajax_out( -1, '修改失败,请检查！' );
	}

	function del( $in )
	{
		$this->check_privilege( '153' );
		$result = biz( 'admin_function' )->del( $in[ 'id' ] );
		return $result == 1 ? $this->ajax_out( 1, '删除成功' ) : $this->ajax_out( -1, '删除失败,请检查！' );
	}

	function add( $in )
	{
		$this->check_privilege( '153' );
		$form = load( 'form' );
		$parent = biz( 'admin_function' )->parent();
		$info = array( 'parent_id' => $form->select( 'parent_id', 0, $parent, true ) );
		return $this->out( $info );
	}

	function add_save( $in )
	{
		$this->check_privilege( '153' );
		$admin_function = biz( 'admin_function' );
		if( $admin_function->exists( array( 'parent_id' => $in[ "parent_id" ], 'name' => $in[ "name" ] ) ) )
		{
			return $this->ajax_out( -1, '名字重复' );
		}
		else
		{
			$admin_function->add( $in );
			return $this->ajax_out( 1, '操作成功' );
		}
	}

	function updown( $in )
	{
		$this->check_privilege( '153' );
		$result = biz( 'admin_function' )->updown( $in );
		return $result ? $this->ajax_out( 1, '修改成功' ) : $this->ajax_out( -1, '修改失败,请检查！' );
	}

	/** 分配权限
	 * @param $in：
	 * 	id：功能id
	 * @return mixed
	 */
	function privilege( $in )
	{
		$this->check_privilege( '152' );
		$id = $in[ 'id' ];

		$info = biz('admin_function')->get_from_id( $id );

		$data = biz('admin')->get( '*', array());

		$admin_privilege = biz( 'admin_privilege' );

		foreach ($data as &$v)
		{
			$v[ 'checked' ] = $admin_privilege->exists( array( 'admin=' . $v[ 'id' ], 'privilege=' . $id ) );
		}

		$out = array( 'info' => $info, 'data' => $data );
		return $this->out( $out );
	}
}

$action = new action();
$action->run();
?>
