<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'menu';
	}
	function home( $in )
	{
		$this->check_privilege();
		$admin = biz('admin');
		$result['admin'] = $admin->login_info();
		if( !empty($result['admin']) )
		{
			$result['menu'] = $admin->menu();
			return $this->out($result);
		}
	}
}

$action = new action();
$action->run();
?>