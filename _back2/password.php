<?php

require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'password';
	}

	function home( $in )
	{
		$this->check_privilege( '105' );
		$data[ 'admin' ] = biz( 'admin' )->login_info();
		return $this->out( $data );
	}

	function edit_save( $in )
	{
		$this->check_privilege( '105' );
		$admin = biz( 'admin' );
		$admin_info = $admin->login_info();

		$result = $admin->password( array( 'id' => $in[ 'id' ], 'password' => $in[ 'password' ], 'old_password' => $in[ 'old_password' ] ) );
		return $result == 1 ? $this->ajax_out( 1, '修改成功' ) : $this->ajax_out( -1, '旧密码不正确' );
	}

}

$action = new action();
$action->run();
?>