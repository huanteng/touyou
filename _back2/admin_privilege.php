<?php

require 'backend.php';

class action extends backend {

	function __construct()
	{
		parent::__construct();
		$this->table = 'admin_privilege';
	}

	function home( $in )
	{
		$this->check_privilege( '152' );
		$id = $in[ 'id' ];

		$data = biz('admin_privilege')->get( 'privilege', array('admin' => $id));
		$privilege = array();
		foreach( $data as $v )
		{
			$privilege[ $v[ 'privilege' ] ] = '';
		}

		$data = biz( 'admin_function' )->get( '*', array(), '`order` desc' );

		$arr = load( 'arr' );

		$out = array();
		foreach ($data as $v)
		{
			$v[ 'checked' ] = isset( $privilege[ $v['id'] ] );

			if( $v['parent_id'] == 0 )
			{
				$out = $arr->set_if_empty( $out, $v[ 'id' ], array( 'son' => array() ) );
				$out[ $v[ 'id' ] ][ 'info' ] = $v;
			}
			else
			{
				$out = $arr->set_if_empty( $out, $v['parent_id'], array( 'son' => array() ) );

				$out[ $v['parent_id'] ][ 'son' ][] = $v;
			}
		}

		$out = array( 'admin' => $id, 'data' => $out );
		return $this->out( $out );
	}

	function edit_save($in) {
		$this->check_privilege( '152' );
		$id = array();
		foreach( $in[ 'id' ] as $v )
		{
			$id[ $v ] = '';
		}

		$admin = $in[ 'admin' ];
		$admin_privilege = biz('admin_privilege');

		$data = $admin_privilege->get( 'privilege', array( 'admin' => $admin ) );
		foreach( $data as $v )
		{
			$privilege = $v[ 'privilege' ];
			if( isset( $id[ $privilege ] ) )
			{
				unset( $id[ $privilege ] );
			}
			else
			{
				$admin_privilege->del_by_term( array( 'admin' => $admin, 'privilege' => $privilege ) );
			}
		}

		foreach( $id as $privilege => $null )
		{
			$admin_privilege->add( array( 'admin' => $admin, 'privilege' => $privilege ) );
		}

		$this->jump('修改成功','admin_privilege.php?id='.$admin);
	}

	/** 按功能分配权限
	 * @param $in
	 */
	function save_by_function( $in )
	{
		$this->check_privilege( '152' );
		$admin = array();
		foreach( $in[ 'admin' ] as $v )
		{
			$admin[ $v ] = '';
		}

		$privilege = $in[ 'privilege' ];
		$admin_privilege = biz('admin_privilege');

		$data = $admin_privilege->get( 'admin', array( 'privilege' => $privilege ) );
		foreach( $data as $v )
		{
			$value = $v[ 'admin' ];
			if( isset( $admin[ $value ] ) )
			{
				unset( $admin[ $value ] );
			}
			else
			{
				$admin_privilege->del_by_term( array( 'admin' => $value, 'privilege' => $privilege ) );
			}
		}

		foreach( $admin as $id => $null )
		{
			$admin_privilege->add( array( 'admin' => $id, 'privilege' => $privilege ) );
		}

		$this->jump('修改成功','admin_function.php?method=privilege&id=' . $privilege );
	}

	function del_privilege( $in )
	{
		$this->check_privilege( '152' );
		$result = biz('admin_privilege')->del_by_term( $in );

		return $result ? $this->ajax_out( 1, '删除成功' ) : $this->ajax_out( -1, '删除失败,请检查！' );
	}

}

$action = new action();
$action->run();
?>