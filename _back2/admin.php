<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'admin';
	}

	function home( $in )
	{
		$this->check_privilege( '152' );
		
		$in[ 'pagesize' ] = 15;
		$admin = biz( 'admin' );
		$equal = array( );
		$like = array( );
		$q = array( );
		$data = $admin->search( $in, $equal, $like, $q );
		$data[ 'q' ] = value( $in, 'q' );
		return $this->out( $data );
	}

	function edit( $in )
	{
		$this->check_privilege( '152' );
		
		$data[ 'info' ] = biz( 'admin' )->get_from_id( $in[ 'id' ] );
		$data[ 'info' ][ 'status_form' ] = load( 'form' )->radio( 'status', $data[ 'info' ][ 'status' ], array( '1' => '正常', '0' => '关闭' ) );
		return $this->out( $data );
	}

	function edit_save( $in )
	{
		$this->check_privilege( '152' );
		
		$result = biz( 'admin' )->set( $in );
		$msg = ( $result == 1 ) ? '修改成功' : "修改失败,请检查！";
		$this->jump( $msg );
	}

	function del( $in )
	{
		$this->check_privilege( '152' );
		
		$result = biz( 'admin' )->del( $in[ 'id' ] );
		return $result == 1 ? $this->ajax_out( 1, '删除成功' ) : $this->ajax_out( -1, '删除失败,请检查！' );
	}

	function copy( $in )
	{
		$this->check_privilege( '152' );

		$result = biz( 'admin' )->copy( $in[ 'id' ] );
		return $result >= 1 ? $this->ajax_out( 1, '复制成功' ) : $this->ajax_out( -1, '复制失败' );
	}

}

$action = new action();
$action->run();
?>
