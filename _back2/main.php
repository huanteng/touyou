<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'main';
	}

	function home($in)
	{
		$title = config( 'site.name' ) . '后台';

		if( config( 'develop' ) )
		{
			$title .= '【测试】';
		}

		return $this->out( array( 'title' => $title ) );
	}
}

$action = new action();
$action->run();
?>