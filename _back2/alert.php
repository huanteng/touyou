<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'alert';
	}

	function home( $in )
	{
		$this->check_privilege( '182' );
		$in = load('arr')->set_default( $in, array(
			'pagesize' => 15
		) );

		$equal = array();
		$like = array();
		$q = array();
		$data = biz('alert')->search( $in, $equal, $like, $q );
		$data[ 'data' ] = load('dataset')->format_time( $data[ 'data' ], 'time' );

		// 半小时未及时执行的任务
		$data[ 'task' ] = biz('task')->count( array( 'status=1', 'run_time<' . ( time() - 1800 ) ) );

		return $this->out( $data );
	}

	function del( $in )
	{
		$this->check_privilege( '182' );
		$alert = biz( 'alert' );
		$task_id = $alert -> get_field_from_id( $in[ 'id' ], 'task_id' );
		$result = $alert->del( $in[ 'id' ] );
		if( $result == 1 ) biz( 'task' )->set( array( 'id' => $task_id, 'status' => 1 ) );
		return $result == 1 ? $this->ajax_out( 1, '删除成功' ) : $this->ajax_out( -1, '删除失败,请检查！' );
	}

}

$action = new action();
$action->run();
?>
