<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'error';
	}

	function home( $in )
	{
		$this->check_privilege( '259' );
		$form = load( 'form' );

		$data = load('data')->get( 'error_conf', 0, array() );

		$checkbox = array( 'log' => '监控', 'save_postdata' => 'postdata', 'save_content' => '内容', 'save_sql' => 'SQL' );

		foreach ( $checkbox as $name => $text ) {
			$data[ $name ] = $form->checkbox( $name, isset( $data[ $name ] ) ? '1' : '', array( '1' => $text ) );
		}

		return $this->out( $data );
	}

	function save( $in )
	{
		$this->check_privilege( '259' );
		$data = load('data');
		$key = 'error_conf';

		if( !isset( $in[ 'log' ] ) )
		{
			$data->del( $key );
		}
		else
		{
			unset( $in[ 'method' ] );
			$data->set( $key, $in );
		}

		return $this->ajax_out( '保存成功' );
	}

	/*
	 * 之所以加几个无用参数，是为了和base兼容，避免报错
	 */
	function get( $in,$a = '', $b = '', $c = '' )
	{
		$this->check_privilege( '259' );
		$error = biz('error');
		$error_data = $error->get();
		$error->clear();
		return $this->ajax_out( $error_data );
	}

	function view( $in )
	{
		$this->check_privilege( '259' );
		$error_data = biz('error')->get();

		return $this->ajax_out( $error_data );
	}

	function clear( $in )
	{
		$this->check_privilege( '259' );
		biz('error')->clear();
		return $this->ajax_out( '已经删除' );
	}
}

$action = new action();
$action->run();
?>