<?php
require_once 'backend.php';

class action_box extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'box';
	}
    // 页头
	function top( $in )
	{
		$in = load('arr')->set_default( $in, array(
			'title' => ''
		) );

		$name = config( 'site.name' );
		if( $in[ 'title' ] != '' )
		{
			$in['title'] = $in['title'] . '_' . $name . '后台';
		}
		else
		{
			$in['title'] = $name . '后台';
		}

		return $in;
	}

	// 翻页页码列表
	function page( $in )
	{
		$page = $in[ 'page' ];
		$start = $page-2 > 0 ? $page-2 : 1;
		$end = $start+4 < 5 ? 5 : $start+4;
		$end = min( $end, $in[ 'pagecount' ] );

		$url = '?';
		foreach( $_GET as $k => $v )
		{
			if( $k != 'page' )
			{
				$url .= $k . '=' . $v . '&';
			}
		}

		$url = $_SERVER['SCRIPT_NAME'] . $url . 'page=';

		$in[ 'start' ] = $start;
		$in[ 'end' ] = $end;
		$in[ 'url' ] = $url;

		return $in;
	}


}
?>