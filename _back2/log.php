<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'log';
	}

	function home( $in )
	{
		$this->check_privilege( '154' );
		$in = load('arr')->set_default( $in, array(
			'module' => '',
			'type' => '',
			'pagesize' => 15,
			'orderby' => 'id desc'
		) );
		$_GET = $in;

		$log = biz( 'log' );
		$form = load( 'form' );
		$time = load( 'time' );

		$equal = array( 'id', 'module', 'type', 'relate_id', 'ip', 'code' );
		$like = array();
		$q = array( 'data', 'content' );
		$data = $log->search( $in, $equal, $like, $q );

		$module = $log->module_dict();
		$data[ 'module' ] = $form->select( 'module', $in[ 'module' ], $module, array( 'empty' => true, 'attr' => 'style="width:90px;"' ) );

		$type = $log->type_dict();
		$data[ 'type' ] = $form->select( 'type', $in[ 'type' ], $type, array('empty'=>true, 'attr'=>'style="width:90px;" ') );

		// 默认链接
		$relate_url = array(
			'task' => 'task.php?id={0}',
			'tips_fake' => 'tips_fake.php?id={0}',
			'payment' => 'payment.php?id={0}',
			'vs' => 'vs.php?id={0}',
			'sell_log' => 'sell_log.php?id={0}',
			'news' => '/news.php?id={0}',
			'tips' => 'http://www.tuijie.cc/t{0}',
		);

		$string = load( 'string' );

		foreach( $data[ 'data' ] as &$v )
		{
			$v[ 'data' ]  = json_decode( $v[ 'data' ], true );
			$v[ 'time' ] = $time->format( $v[ 'time' ] );
			$v[ 'type' ] = $type[ $v[ 'type' ] ];

			$method = 'module_' . $v[ 'module' ];

			$table = array( array(), array(), '' );
			if( method_exists( $this, $method ) )
			{
				$table = $this->{$method}( $v );
			}

			// 如果没处理，则交给默认处理
			if( empty( $table[0] ) && empty( $table[1] ) && $table[2] == '' && !empty( $v[ 'data' ] ) )
			{
				$table = $this->module_default( $v );
			}

			$v = $this->join_table( $v, $table );

			if( !isset( $v[ 'relate_url' ] ) && isset( $relate_url[ $v[ 'module' ] ] ) )
			{
				$v[ 'relate_url' ] = $string->format( $relate_url[ $v[ 'module' ] ], $v[ 'relate_id' ] );
			}
			else
			{
				$v[ 'relate_url' ] = '';
			}
		}

		return $this->out( $data );
	}

	function del( $in )
	{
		$this->check_privilege( '154' );
		$result = biz( 'log' )->del( $in[ 'id' ] );

		return $result == 1 ? $this->ajax_out( 1, '删除成功' ) : $this->ajax_out( -1, '删除失败,请检查！' );
	}


	function do_module( $in )
	{
		$this->check_privilege( '154' );
		$out = biz( 'log' )->action( $in );
		return json_encode( $out );
	}
	/** 批量删除
	 * @param $in：field, id
	 */
	function banch_del( $in )
	{
		$this->check_privilege( '154' );
		$field = $in[ 'field' ];

		$log = biz( 'log' );
		if( $field == 'all' )
		{
			$log->db->command( 'truncate table log' );
			$this->log( 'log', 0, '全部清空' );
		}
		else
		{
			$info = $log->get_from_id( $in[ 'id' ] );

			$term = array( $field => $info[ $field ] );
			if( $field == 'content' || $field == 'type' || $field == 'code' )
			{
				$term[ 'module' ] = $info[ 'module' ];
			}

			$log->del_by_term( $term );

			$this->log( 'log', 0, '批量删除', array( 'data' => $term ) );
		}
	}

	function module_default( $info )
	{
		$thead = array();
		$tbody = array();
		$tfoot = '';

		if( isset( $info[ 'data' ][ 'data' ] )  )
		{
			$data = $info[ 'data' ][ 'data' ];
			if( is_array( $data ) && count( $data ) > 0 )
			{
				if( isset( $data[ 0 ] ) )
				{
					foreach( $data[ 0 ] as $k => $null )
					{
						$thead[] = $k;
					}
				}

				foreach( $data as $v )
				{
					$td = array();
					foreach( $thead as $v2 )
					{
						$temp = value($v, $v2);
						if( is_array( $temp ) )
						{
							$td[] = htmlspecialchars( join(' / ', $temp ) );
						}
						else
						{
							$td[] = htmlspecialchars( $temp );
						}
					}
					$tbody[ ] = $td;
				}
			}
		}

		foreach( $info[ 'data' ] as $k => $v )
		{
			if( $k != 'data' || !isset( $info[ 'data' ][ 'data' ][0] ) )
			{
				$tfoot .= $k . '：';
				if( is_array( $v ) )
				{
					$tfoot .= htmlspecialchars( join(' / ', $v ) );
				}
				else
				{
					$tfoot .= htmlspecialchars( $v );
				}
				$tfoot .= '<br>';
			}
		}

		return array( $thead, $tbody, $tfoot );
	}

	/** 串接table html，被module中使用
	 * @param $info：log info
	 * @param $table：表结构，包括三部分：
	 * 	thead：表头列表，json形式
	 * 	tbody：表体列表，二维数组
	 * 	tfoot：表脚，字符串形式
	 * 返回值：处理过后的log info，视情况追加合适的table属性
	 */
	function join_table( $info, $table )
	{
		$content = '';
		$thead = $table[0];
		$tbody = $table[1];
		$tfoot = $table[2];
		if( !empty( $thead ) || !empty( $tbody ) || $tfoot != '' )
		{
			$arr = load( 'arr' );
			$content = '<table class="table table-striped table-bordered">';

			if( !empty( $thead ) )
			{
				$content .= '<thead>' . $arr->implode( $thead, '<th>', '</th>', '' ) . '</thead>';
			}

			foreach( $tbody as $v )
			{
				$content .= '<tr>' . $arr->implode( $v, '<td>', '</td>', '' ) . '</tr>';
			}

			if( $tfoot != '' )
			{
				$count = count( $thead );
				if( $count == 0 && count( $tbody ))
				{
					$count = count( $tbody[ 0 ] );
				}

				if( $count != 0 )
				{
					$count = ' colspan="' . $count . '"';
				}

				$content .= '<tr><td' . $count . ' style="word-break:break-all">' . $tfoot . '</td></tr>';
			}

			$content .= '</table>';
		}

		if( !empty( $info[ 'data' ] ) )
		{
			$content .= '<span onclick="$(\'#data' . $info[ 'id' ] . '\').toggle()">原始数据+</span>' .
				'<span id="data'. $info[ 'id' ] . '" style="display:none"><pre>' . htmlspecialchars( print_r( $info[ 'data' ], true ) ) . '</pre></span>';
		}

		if( $content != '' )
		{
			$info[ 'table' ] = $content;
		}

		return $info;
	}

}

$action = new action();
$action->run();
?>
