<?php
require 'backend.php';

class action extends backend
{
	function __construct()
	{
		parent::__construct();
		$this->table = 'dashboard';
	}
	function home( $in )
	{	
		$this->check_privilege( '0' );
		$db = load('db');
		$result['user'] = 0;//$db->total('from users');
		$result['vip'] = 0;//$db->total('from users where is_npc = 0');
		$result['today_user'] = '---';//$db->total('from users where created >= '.strtotime(date('Y-m-d')));
		$result['today_vip'] = '---';//$db->total('from users where is_npc = 0 and created >= '.strtotime(date('Y-m-d')));

		$result['payment'] = 0;//$db->total('from users where is_npc = 0');
		$result['warning'] = $db->total('from alert');
		$result['today_warning'] = 1;//$db->total('from alert2 where time >= '.strtotime(date('Y-m-d')));
		
		//成长计算：
		$current_start_time = strtotime( date( 'Y-m-01' ) );
		$current_end_time = strtotime( date( 'Y-m-d' ) ) + 86399;

		$current_day = date( 'd' );
		$prev_month = date( 'm' ) - 1;
		$prev_year = date( 'Y' );

		if ( $prev_month == 0 )
		{
			$prev_month = 12;
			$prev_year = $prev_year - 1;
		}

		if ( $current_day > 28 )
		{
			$prev_standard_day = date('t', strtotime($prev_year . '-' . $prev_month . '-01'));
			if ( $current_day > $prev_standard_day ) $prev_day = $prev_standard_day;
			else $prev_day = $current_day;
		}
		else
		{
			$prev_day = $current_day;
		}

		$prev_start_time = strtotime( $prev_year . '-' . $prev_month . '-01' );
		$prev_end_time = strtotime( $prev_year . '-' . $prev_month . '-' . $prev_day ) + 86399;

		$current_sql = "select sum( `change` ) as money from balance where type in ( 7, 11 ) and dateline between " . $current_start_time . " and " . $current_end_time;

		$prev_sql = "select sum( `change` ) as money from balance where type in ( 7, 11 ) and dateline between " . $prev_start_time . " and " . $prev_end_time;

		$current = array();//$db->unique( $current_sql );
		$prev = array();//$db->unique( $prev_sql );
		$current['money'] = value($current,'money');
		$prev['money'] = value($prev,'money');
		$result['money'] = $current['money'] > 0 ? $current['money'] : 0;
		$result['percent'] = $prev['money'] == 0 ? '-' : ( ( round( ( $current['money'] - $prev['money'] ) * 100 / $prev['money'] ) ) . '%');
		//成长计算结束

		$login_info = biz('admin')->login_info();
		$result[ 'username' ] = $login_info[ 'username' ];
		$result[ 'hash' ] = $login_info[ 'password' ];

		return $this->out($result);
	}
}

$action = new action();
$action->run();
?>