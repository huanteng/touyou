<?php
	function get_or_post( $url, $post = array(), $referer = '', $cookie = '', $port = 80, $useragent = 'Mozilla/4.0 (compatible; MSIE 5.00; Windows 98)' )
	{
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_PORT, $port );
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_HEADER, 0 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
		curl_setopt( $ch, CURLOPT_USERAGENT, $useragent );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 0 );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 0 );

		if ( $referer != '' ) curl_setopt( $ch, CURLOPT_REFERER, $referer );
		if ( $cookie != '' ) curl_setopt( $ch, CURLOPT_COOKIE, $cookie );

		if ( count( $post ) > 0 )
		{
			$field = '';
			foreach( $post as $key => $value ) $field .= $key . '=' . urlencode( $value ) . '&';
			$field = substr( $field, 0, -1 );
			curl_setopt( $ch, CURLOPT_POST, 1 );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $field );
		}

		$data = curl_exec( $ch );
		curl_close( $ch );
		return $data;
	}

	if ( isset( $_GET['url'] ) ) echo get_or_post( $_GET['url'] );
?>