<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE )
	{
		if( $set_uid ) $in = $this->set_uid( $in );
		$q = load( 'biz.event' );
		return $q->$method( $in );
	}

	function ad( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}

	function num( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function execute( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
	function lists( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}
}

$action = new action();
$action->run();
?>