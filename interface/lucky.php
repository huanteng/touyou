<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = load( 'biz.lucky' );
		return $q->$method( $in );
	}
	function num( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function execute( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
	function lists( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}
	// <editor-fold defaultstate="collapsed" desc="20130110，已过期，保留一个月">
	/*function result()
	{
		return array( 'code' => -1, 'memo' => '你没有抽奖券了' );
	}
	 * 
	 */
	// </editor-fold>
}

$action = new action();
$action->run();
?>