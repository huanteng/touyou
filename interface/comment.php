<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = load( 'biz.comment' );
		return $q->$method( $in );
	}

	// 重载自MVC，如出现问题逐个修正
	function format( $s )
	{
		return $s;
	}

	function view( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
	function lists( $in )
	{
		if( !isset( $in[ 'uid' ] ) )
		{
			$in = $this->set_uid( $in, 'uid', true );
		}

		return biz('comment')->lists( $in );
	}

	function replys( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function say( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function answer( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
}

$action = new action();
$action->run();
?>