<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = load( 'biz.props' );
		return $q->$method( $in );
	}

	function payment_no( $in )
	{
		$in = $this->set_uid( $in, 'uid', TRUE );
		$code = biz('payment')->get_no( $in );
		return array( 'data' => $code, 'no' => $code );
	}

	function upgrade()
	{
		$props = load( 'biz.props' );
		return $props->upgrade( $this->input );
	}

	function detail( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function lists( $in )
	{
		$props = load( 'biz.props' );
		$kind = isset( $this->input['kind'] ) && is_numeric( $this->input['kind'] ) ? $this->input['kind'] : 2;
        $where = ' and kind = ' . $kind;
        if( $kind == 1 ) $where .= ' and id <> 1';

		$result = $props->lists( 1, $where, 1024 );

		return array( 'data' => $result['data'] );
	}

	function my( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function buy( $in )
	{		
		return $this->common( __FUNCTION__, $in );
	}

	function uses( $in )
	{
		// 内部切换变量名
		$in[ 'id' ] = $in[ 'props' ];
		$in[ 'count' ] = isset( $in[ 'quantity' ] ) ? $in[ 'quantity' ] : 1;

		return $this->common( 'use_props', $in );
	}

	function free( $in )
	{
		$in = $this->set_uid( $in );
		
		$props = load( 'biz.props' );
		$result = $props->gold( $in['uid'] );
		
		return array( 'code' => $result );
	}
	
	// 索要
	// 返回值{code, memo}
	// code为正数时，表示赠送单id
	function request( $in )
	{
		return array( 'code' => $this->common( __FUNCTION__, $in ) );
	}

	// 赠送
	// 返回值{code, memo}
	// code为正数时，表示赠送单id
	function send( $in )
	{
		return array( 'code' => $this->common( __FUNCTION__, $in ) );
	}

	// 赠送单详情
	// 返回json对象
	function request_info( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	// 拒绝索要
	// 返回json对象
	function request_refuse()
	{
		$props = load( 'biz.props' );
		$id = $props->value( $this->input, 'id' );
		return $props->request_refuse( $id );
	}

	function history( $in )
	{
		// 20130122，兼容旧程序，在此用data函数，过两个月后再统一
		//return $this->common( __FUNCTION__, $in );
		$data = $this->common( __FUNCTION__, $in );
		$base = load( 'biz.base' );
		return $base->out( $data );
	}
}

$action = new action();
$action->run();
?>