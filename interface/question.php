<?php
require 'front.php';

class action extends front
{	
	function detail( $in )
	{		
		$in = $this->set_uid( $in );
		
		$q = load( 'biz.question' );
		return $q->detail( $in );
	}

	function answer( $in )
	{		
		$in = $this->set_uid( $in );
		
		$q = load( 'biz.question' );
		return $q->answer( $in );
	}
	
}

$action = new action();
$action->run();
?>