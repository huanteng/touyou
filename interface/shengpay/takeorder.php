<?php
	require_once('config.php');
	require_once('core/core.php');

	$shengpay=new shengpay();

	$array=array(
		'Name'=>'B2CPayment',
		'Version'=>'V4.1.1.1.1',
		'Charset'=>'UTF-8',
		'MsgSender'=>$MsgSender,
		'SendTime'=>date('YmdHis'),
		'OrderTime'=>date('YmdHis'),
		'PayType'=>'',
		'InstCode'=>'',
		'PageUrl'=>$PageUrl,
		'NotifyUrl'=>$NotifyUrl,
		'ProductName'=>'元宝',
		'BuyerContact'=>'',
		'BuyerIp'=>'',
		'Ext1'=>'',
		'Ext2'=>'',
		'SignType'=>'MD5',
	);

	$shengpay->init( $array );
	$shengpay->setKey( $Key );

	$oid = isset( $_REQUEST['oid'] ) && is_numeric( $_REQUEST['oid'] ) ? $_REQUEST['oid'] : 0;
	$fee = isset( $_REQUEST['fee'] ) && is_numeric( $_REQUEST['fee'] ) ? $_REQUEST['fee'] : 0;

	if ( $oid > 0 && $fee > 0 ) $shengpay->takeOrder( $oid, $fee );
	else die( '参数错误' );
?>