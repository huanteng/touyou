<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = load( 'biz.message' );
		return $q->$method( $in );
	}
	
	function lists( $in )
	{
		$in = $this->set_uid( $in, 'owner' );
		$result = $this->common( __FUNCTION__, $in, FALSE );

		$new_order = array();
		$max = count( $result['data'] );
		for( $i = $max - 1; $i >= 0; $i-- ) $new_order[] = $result['data'][$i];
		$result['data'] = $new_order;

		return $result['data'];
	}

	// 20130201，好像没用到
	function detail()
	{
		biz('base')->error('message detail');
//		$message = load( 'biz.message' );
//		return $message->read( $this->input );
	}

	function send( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
}

$action = new action();
$action->run();
?>