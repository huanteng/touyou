<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = biz( 'room' );
		return $q->$method( $in );
	}

	function popup( $in )
	{
		$in = $this->set_uid( $in, 'uid', TRUE );
		return biz('room_pool')->popup( $in );
	}
	
	function lists( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}

	function quick_start( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function detail( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}


	function rank( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}

	function say( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
	function info( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
	function desk( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}
}

$action = new action();
$action->run();
?>