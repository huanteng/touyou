<?php
    ///////////////////页面功能说明///////////////////
    /// 客户端callback回调之后请求服务器callback_url
    /// 验签通过就给客户端返回2，不通过就返回1
    ////////////////////////////////////////////////

    require 'alipay_config.php';
    require 'alipay_function.php';

    //得到签名
    $sign = urldecode($_POST["sign"]);

    //得到待签名字符串
    $content = urldecode($_POST["content"]);

    //验签数据
    $isVerify = verify($content, $sign);

    //判断验签
    if ( $isVerify )
	{
		//验签通过
		echo("2");
    }
    else
	{
		//验签失败
		echo("1");
    }

?>