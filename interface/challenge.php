<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE )
	{
		if( $set_uid ) $in = $this->set_uid( $in );
		$q = load( 'biz.challenge' );
		return $q->$method( $in );
	}

	function no_continue( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function next( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function accept( $in )
	{
		return array( 'code' => $this->common( __FUNCTION__, $in ) );
	}

	function reject( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function send( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function cancel( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function win_rank( $in )
	{
		$in['types'] = 1;
		return array( 'data' => $this->common( 'rank', $in ) );
	}
	function friend_rank( $in )
	{
		$in['types'] = 3;
		return array( 'data' => $this->common( 'rank', $in ) );
	}
	
	function list_user( $in )
	{
		return array( 'data' => $this->common( __FUNCTION__, $in ) );
	}
}

$action = new action();
$action->run();
?>