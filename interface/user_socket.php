<?php
	require '../library/config.php';

	if ( isset( $_REQUEST['operate'] ) && $_REQUEST['operate'] != '' )
	{
		$database = load( 'database' );

		if ( isset( $_REQUEST['operate'] ) && $_REQUEST['operate'] == 'init' )
		{
			$database->command( 'truncate user_socket' );
		}
		else if ( isset( $_REQUEST['operate'] ) && $_REQUEST['operate'] == 'close' && isset( $_REQUEST['socket'] ) && is_numeric( $_REQUEST['socket'] ) )
		{
			$database->command( 'delete from user_socket where socket = ' . $_REQUEST['socket'] );
		}
		else if ( isset( $_REQUEST['operate'] ) && $_REQUEST['operate'] == 'heart' && isset( $_REQUEST['socket'] ) && is_numeric( $_REQUEST['socket'] ) )
		{
			$info = $database->unique( 'select user from user_socket where socket = ' . $_REQUEST['socket'] );

			if ( isset( $info['user'] ) )
			{
				$pool = load( 'biz.pool' );
				echo json_encode( $pool->popup( array( 'uid' => $info['user'] ) ) );
			}
		}
		else if ( isset( $_REQUEST['operate'] ) && $_REQUEST['operate'] == 'heart2' && isset( $_REQUEST['socket'] ) && is_numeric( $_REQUEST['socket'] ) )
		{
			$info = $database->unique( 'select user from user_socket where socket = ' . $_REQUEST['socket'] );

			if ( isset( $info['user'] ) )
			{
				$announce = load( 'biz.announce' );
				echo json_encode( $announce->popup( array( 'uid' => $info['user'], 'mode_not_in' => 0 ) ) );
			}
		}
		else if ( isset( $_REQUEST['operate'] ) && $_REQUEST['operate'] == 'handle' && isset( $_REQUEST['socket'] ) && is_numeric( $_REQUEST['socket'] ) && isset( $_REQUEST['data'] ) && $_REQUEST['data'] != '' )
		{
			$data = json_decode( $_REQUEST['data'], true );

			if ( isset( $data['operate'] ) )
			{
				if ( $data['operate'] == 'bind' && isset( $data['user'] ) && is_numeric( $data['user'] ) )
				{
					$database->command( 'delete from user_socket where user = ' . $data['user'] );
					$database->command( 'insert into user_socket ( socket, user ) values ( ' . $_REQUEST['socket'] . ' , ' . $data['user'] . ' )' );

					echo json_encode( array( 'code' => 0, 'info' => '' ) );
				}
			}
		}
	}
?>