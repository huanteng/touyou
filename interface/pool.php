<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = load( 'biz.pool' );
		return $q->$method( $in );
	}

	function popup( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function pop( $in )
	{
		return $this->common( __FUNCTION__, $in, false );
	}

}

$action = new action();
$action->run();
?>