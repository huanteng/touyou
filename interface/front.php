<?php
require '../library/controller.php';

class front extends controller
{
	var $db = '';
	
	function __construct()
	{
		parent::__construct();
		$this->db = load( 'database' );
	}

	function shows( $content )
	{
		include '../log.conf.php';
		if ( isset( $log['interface'] ) )
		{
			$is_log = false;
			$log = $log['interface'];
			
			$uid = 0;
			if( isset( $log['key']['uid'] ) )
			{
				if( isset($_REQUEST['sid']) )
				{
					$user = biz( 'user' );
					$uid = $user->get_id_from_sid( array( 'sid' => $_REQUEST['sid'] ) );
					
					$is_log = ( in_array( $uid, explode( ',', $log['key']['uid'] ) ) );
				}				
			}
			else
			{
				$is_log = true;
			}
			
			if( $is_log && !isset( $log['key']['heart'] ) )
			{
				$is_log = !$this->log_check_in( 'announce.php,pool.php', $_SERVER['REQUEST_URI'] )
					&& !$this->log_check_in( 'popup', var_export( $_REQUEST, true ) );
			}

			if( $is_log && isset( $log['key']['url'] ) )
			{
				$is_log = $this->log_check_in( $log['key']['url'], $_SERVER['REQUEST_URI'] );
			}
			if( $is_log && isset( $log['key']['input'] ) )
			{
				$is_log = $this->log_check_in( $log['key']['input'], var_export( $_REQUEST, true ) );
			}
			if( $is_log && isset( $log['key']['response'] ) )
			{
				$is_log = $this->log_check_in( $log['key']['response'], $content );
			}
			if( $is_log && isset( $log['key']['sql'] ) )
			{
				$is_log = $this->log_check_in( $log['key']['sql'], var_export( $this->db->sql, true ) );
			}

			if ( $is_log )
			{
				$data = array( 'uid' => $uid, 'url' => $_SERVER['REQUEST_URI'] );
				if(  isset( $log['save']['input'] ) ) $data['input'] = var_export( $_REQUEST, true );
				if(  isset( $log['save']['response'] ) ) $data['response'] = $content;
				if(  isset( $log['save']['sql'] ) ) $data['sql'] = var_export( $this->db->sql, true );
				
				$this->log( $data );
			}
		}
		echo $content;
	}

	function do_get()
	{
		$function = isset( $this->input['type'] ) && $this->input['type'] != '' ? $this->input['type'] : '';

		if ( $function != 'do_post' && method_exists( $this, $function ) )
		{
			$data = $this->input;
			unset( $data[ $function ] );
			$result = $this->$function( $data  );
		}
		else
		{
			biz('user')->error( '接口不存在：', $function );
			$result = array( 'code' => -999, 'data' => 'unknown method' );
		}

		$this->shows( json_encode( $result ) );
	}

	function do_post()
	{
		return $this->do_get();
	}
	
	// 记录log。
	// 注意，为了性能，尽量不要过多调用
	// 参数：uid, url, input, response,sql
	function log( $data )
	{		
		$log = load( 'biz.interface_log' );
		$log->add( $data );
	}

	// 主要用户log场景。$keys中设定多个过滤条件（用逗号分开），逐一检查是否处于$content中
	function log_check_in( $keys, $content )
	{
		if( $keys == '' ) return true;
		foreach( explode( ',', $keys ) as $key )
		{
			if( strpos( $content, $key ) !== false )
			{
				return true;
			}
		}
		return false;
	}
	
	/* 从$data['sid']中，获得uid，并设置$data
	 * 参数：
	 *   $data：数组，一般是 $this->value
	 *   $key：将获得的uid保存为什么key？默认为uid
	 *   $exist：如果sid不在线，是否退出，默认true
	 * 返回值：处理过后的数组
	 */
	function set_uid( $data, $key = 'uid', $exist = true )
	{
		$uid = biz('user')->get_id_from_sid( array( 'sid' => $data['sid'] ) );
		
		if( $uid == '' && $exist )
		{
			$result = array( 'code' => -100, 'data' => '用户不在线' );
			$this->shows( json_encode( $result ) );
			die;
		}

		unset( $data['sid'] );
		$data[ $key ] = $uid;
		return $data;
	}
	
	
}
?>
