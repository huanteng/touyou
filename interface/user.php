<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE )
	{
		if( $set_uid ) $in = $this->set_uid( $in );
		$q = load( 'biz.user' );
		return $q->$method( $in );
	}

	function logon( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}

	function moving( $in )
	{
		$user = load( 'biz.user' );
		
		$in = $this->set_uid( $in, 'temp' );
		$uid = $in['temp'];

		if( $user->value( $in, 'kind', 2 ) == 1 )
		{
			if( $user->value( $in, 'uid', '' ) == '' ) $in['uid'] = $uid;
		}
		
		$data = $user->moving( $in );
				
		return array( 'data' => $data );
	}

	function task_list( $in )
	{
		$in = $this->set_uid( $in );
		return biz( 'task' )->lists( $in );
	}
	function consume( $in )
	{ // 201318，新版不再使用本接口，而是直接使用props.history
		$in = $this->set_uid( $in );
		$in['props'] = 1;
		$props = load( 'biz.props' );
		return $props->history( $in );
	}

	function lists( $in )
	{
		return $this->common( 'all', $in );
	}

	function get_pass()
	{
		$user = load( 'biz.user' );
		return array( 'code' => $user->get_pass( $this->input ) );
	}

	function email_bind( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function check_email_code( $in )
	{
		$user = load( 'biz.user' );
		$user->check_email_code( $in );

		$template = load( 'template', array( 'dir' => '../frontend/template/' ) );
		echo $template->parse( 'check_email_ok.php' );die;
	}

	function email_code( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function setinfo( $in )
	{
		return array( 'code' => $this->common( __FUNCTION__, $in ) );
	}

	function detail( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function addfriend( $in )
	{
		$in = $this->set_uid( $in );
		$friend = load( 'biz.friend' );
		return $friend->create( $in );
	}

	function delfriend( $in )
	{
		$in = $this->set_uid( $in );
		$friend = load( 'biz.friend' );
		return $friend->remove( $in );
	}

	function my_focus( $in )//我关注的人=我添加X为好友
	{
		$friend = biz( 'friend' );
		$in['types'] = 'focus';
		$in = $this->set_uid( $in );
		// 在线的最前
		$data = $friend->lists2( $in );
		usort($data['data'], 'online');
		return array( 'data' => $data );
	}

	function my_fans( $in )//关注我的人=添加了我为X的好友
	{
		$friend = load( 'biz.friend' );
		$in['types'] = 'fans';
		$in = $this->set_uid( $in );
		$data = $friend->lists2( $in );
		usort($data['data'], 'online');
		return array( 'data' => $data );
	}

	function search()
	{
		$data = biz( 'friend' )->search( $this->input );
		//usort($data, 'online');
		return array( 'data' => $data );
	}

	function setpass( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function register( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}

	function quick_reg()
	{
		$imei = isset( $this->input['imei'] ) ? $this->input['imei'] : '';
		$channel = isset( $this->input['channel'] ) ? $this->input['channel'] : '';
		$user = load( 'biz.user' );
		return $user->quick_reg( $imei, $channel );
	}

	function login( $data )
	{
		$user = load( 'biz.user' );
		$out =  $user->login( $data );

		// 20130504，如果一个月内无调用，则删除
//		$user->error('有旧的login方法调用');

		if( $out['code'] > 0 ) $out['memo'] = '';
		return $out;
	}

	// 开放平台登录（旧版使用）
	function open_login( $in )
	{
		$user = load( 'biz.user' );
		return $user->open_login( $in );
	}

	function open_logon( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}

	// 开放平台绑定
	function open_bind( $in )
	{
		$user = load( 'biz.user' );
		$in = $this->set_uid( $in );
		return $user->open_bind( $in );
	}

	function logout( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
	
	/* 个人战绩
	 * 
	 */
	function score( $in )
	{
		$user = load( 'biz.user' );
		
		$uid2 = $user->value( $in, 'uid' );
		$in = $this->set_uid( $in );
		$in[ 'uid2' ] = ( $uid2 == '' ) ? $in[ 'uid' ] : $uid2;

		return $user->score( $in );
	}

	/* 个人战绩
	 *
	 */
	function today_score( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function money( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function invite( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function config( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
	function set_config( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
	function day_gold( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function data( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function connect( $in )
	{
		return $this->common( __FUNCTION__, $in, false );
	}
	function disconnect( $in )
	{
		return $this->common( __FUNCTION__, $in, false );
	}
	function disconnect_all( $in )
	{
		return $this->common( __FUNCTION__, $in, false );
	}

	function online( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
}

function online($a, $b)
{
	return $a['online']!=1;
}

$action = new action();
$action->run();
?>
