<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = load( 'biz.notification' );
		return $q->$method( $in );
	}
	function lists( $in )
	{
		$in = $this->set_uid( $in, 'receiver' );
		$out = $this->common( __FUNCTION__, $in, FALSE );

		return $out['data'];
	}

	function num( $in )
	{
		return $this->common( __FUNCTION__, $in, TRUE, FALSE );
	}

	function del( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function detail( $in )
	{
		return $this->common( 'read', $in );
	}


}

$action = new action();
$action->run();
?>