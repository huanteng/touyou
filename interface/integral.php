<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = load( 'biz.integral' );
		return $q->$method( $in );
	}
	
	function stat( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}

	function apply( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function signup( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function match( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function player_info()
	{
		$integral = load( 'biz.integral' );
		return $integral->player_info( $this->input );
	}

	function rank( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
}

$action = new action();
$action->run();
?>