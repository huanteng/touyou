<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE )
	{
		if( $set_uid ) $in = $this->set_uid( $in );
		$q = load( 'biz.match' );
		return $q->$method( $in );
	}
	function intrust()
	{
		$vs = load( 'biz.match' );
		$this->input['managed'] = 1;
		return array( 'code' => $vs->intrust( $this->input ) );
	}

	function cancel_intrust()
	{
		$vs = load( 'biz.match' );
		$this->input['managed'] = 0;
		return array( 'code' => $vs->intrust( $this->input ) );
	}

	function shout( $in )
	{
		return array( 'code' => $this->common( __FUNCTION__, $in ) );
	}

	function close( $in )
	{
		$in['old'] = 1;
		$out = $this->common( 'close', $in );

		if( !isset( $out[ 'code' ] ) ) $out = array( 'code' => $out );

		return $out;
	}

	function breaks( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function open( $in )
	{
		// 20130129临时使用这样写法，以后应该在close中返回json结构
		$out = $this->common( 'close', $in );

		if( !isset( $out[ 'code' ] ) ) $out = array( 'code' => $out );

		return $out;
	}

	// 测试
	function test()
	{
		$data = array( 'text' => 'good' );
		return $data;
	}
}

$action = new action();
$action->run();
?>