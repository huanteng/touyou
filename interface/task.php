<?php
require 'front.php';

class action extends front
{
	function common( $method, $in )
	{
		$in = $this->set_uid( $in );
		$task = load( 'biz.task' );
		return $task->$method( $in );
	}
	function num( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
	function lists( $in )
	{
		$in = $this->set_uid( $in );
		$q = load( 'biz.task' );
		return $q->lists( $in );
	}
	
	function detail( $in )
	{		
		$in = $this->set_uid( $in );
		
		$q = load( 'biz.task' );
		return $q->detail( $in );
	}

	function finish( $in )
	{		
		$in = $this->set_uid( $in );
		
		$q = load( 'biz.task' );
		return $q->finish( $in );
	}
}

$action = new action();
$action->run();
?>