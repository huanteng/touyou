<?php //搬迁去香港服务器前，因为不断有访问导致错误日志快速增大，关闭；2016 03 04
die();
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = load( 'biz.announce' );
		return $q->$method( $in );
	}

	function normal( $in )
	{
		$in['mode_in'] = 0;
		return $this->common( 'popup', $in, TRUE, FALSE );
	}

	function match( $in )
	{
		$in['mode_not_in'] = 0;
		return $this->common( 'popup', $in );
	}
}

$action = new action();
$action->run();
?>