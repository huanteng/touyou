<?php
	$type = 0;
	$now = time();
	$file = '/dev/shm/' . $type . '.run';

	if ( ! is_file( $file ) )
	{
		file_put_contents( $file, $type );

		require '../library/config.php';
		$task_queue = load( 'biz.task_queue' );
		$data = $task_queue->database->unique( 'select * from task_queue where type = 1016 limit 1' );

		if ( isset( $data['id'] ) && $data['time'] <= $now )
		{
			$time = $task_queue->handle_1016( $data );
			$task_queue->database->command( 'update task_queue set time = ' . $time . ' where id = ' . $data['id'] );
		}

		unlink( $file );
	}
?>