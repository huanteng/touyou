<?php
// 20130604，本文件临时用

require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = biz( 'room' );
		return $q->$method( $in );
	}

	function clear( $in )
	{
		$uid = $in[ 'uid' ];
		
		$room = biz('room');
		foreach ( array(1,2,3,4) as $v )
		{
			$data = $room->get1( '*', array( 'uid'. $v => $uid) );
			if( isset($data['id']) )
			{
				$room->set( array( 'id' => $data['id'], 'status' => 1, 'uid' . $v => 0 ) );
			}
		}
		echo 'ok';
	}
	
	function lists( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}

	function sit( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function quick_sit( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function detail( $in )
	{
		return $this->common( __FUNCTION__, $in,false );
	}

	function leave( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function begin( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function shout( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function open( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	// 临时测试用 sit & begin
	function sb( $in )
	{
		$m = $in;
		$this->sit($in);
		$this->begin($m);
	}

}

$action = new action();
$action->run();
?>