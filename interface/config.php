<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = load( 'biz.config' );
		return $q->$method( $in );
	}
	function get( $in )
	{
		return $this->common( __FUNCTION__, $in, TRUE, FALSE );
	}

}

$action = new action();
$action->run();
?>