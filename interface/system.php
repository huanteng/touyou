<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = biz( 'system' );
		return $q->$method( $in );
	}

	function version( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}

	function update_lua( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}

	function select( $in )
	{
		return $this->common( __FUNCTION__, $in, FALSE );
	}

}

$action = new action();
$action->run();
?>