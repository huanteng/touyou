<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = TRUE, $exist = true )
	{
		if( $set_uid ) $in = $this->set_uid( $in, 'uid', $exist );
		$q = load( 'biz.album' );
		return $q->$method( $in );
	}

	function set_logo( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function remove( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function upload( $in )
	{
		// 改名，以统一数据库结构
		$base = load( 'biz.base' );
		$in[ 'type' ] = $base->value( $in, 'kind', 0 );

		return $this->common( __FUNCTION__, $in );
	}

	function vote_score( $in )
	{
		return array( 'code' => $this->common( __FUNCTION__, $in ) );
	}

	function detail( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function list_all_new( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	// 20130228，本接口将放弃
	function list_someone_all( $in )
	{
		// 改名，以统一数据库结构
		$base = load( 'biz.base' );
		$in[ 'type' ] = $base->value( $in, 'kind', 0 );
		
		return array( 'data' => $this->common( __FUNCTION__, $in ) );
	}

	function small( $in )
	{
		$in[ 'type' ] = $in['kind'];
		return $this->common( __FUNCTION__, $in );
	}
	function large( $in )
	{
		$in[ 'type' ] = $in['kind'];
		return $this->common( __FUNCTION__, $in );
	}
	function real( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}
}

$action = new action();
$action->run();
?>