<?php
require 'front.php';

class action extends front
{
	function change_online()
	{
		$id = $this->input['id'];
		$uid = $this->input['uid'];
		$administrator = load( 'biz.administrator' );
		$data = $administrator->change_online( $id, $uid );
		
		return $data;
	}

}

$action = new action();
$action->run();
?>