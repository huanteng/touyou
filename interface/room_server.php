<?php
require 'front.php';

class action extends front
{
	function common( $method, $in, $set_uid = FALSE )
	{
		if( $set_uid ) $in = $this->set_uid( $in );
		$q = biz( 'room_server' );
		return $q->$method( $in );
	}

	function popup( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function start_server( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function pre_stop_server( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function stop_server( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function stop_service( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function add_gold( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function add_props( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function result( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function my_info( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function info( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function sit( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function leave( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function use_props( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function challenge( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function challenge_room( $in )
	{
		return $this->common( __FUNCTION__, $in );
	}

	function challenge_send( $in )
	{
		return biz( 'challenge_send' )->pop( $in );
	}
}

$action = new action();
$action->run();
?>