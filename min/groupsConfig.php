<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
    // 'js' => array('//js/file1.js', '//js/file2.js'),
    // 后台css
	'css' => array(
		'//_back2/css/bootstrap-cerulean.css',
		'//_back2/css/bootstrap-responsive.css',
		'//_back2/css/charisma-app.css',
		'//_back2/css/jquery-ui-1.8.21.custom.css',
		'//_back2/css/fullcalendar.css',
		'//_back2/css/fullcalendar.print.css',
		'//_back2/css/chosen.css',
		'//_back2/css/uniform.default.css',
		'//_back2/css/colorbox.css',
		'//_back2/css/jquery.cleditor.css',
		'//_back2/css/jquery.noty.css',
		'//_back2/css/noty_theme_default.css',
		'//_back2/css/elfinder.min.css',
		'//_back2/css/elfinder.theme.css',
		'//_back2/css/jquery.iphone.toggle.css',
		'//_back2/css/opa-icons.css',
		'//_back2/css/uploadify.css',
	),

	// 后台所有js
	'back' => array(
		'//_back2/js/jquery-1.7.2.min.js',
		'//_back2/../js/common.js',
		'//_back2/js/jquery-ui-1.8.21.custom.min.js',
		'//_back2/js/bootstrap-transition.js',
		'//_back2/js/bootstrap-alert.js',
		'//_back2/js/bootstrap-modal.js',
		'//_back2/js/bootstrap-dropdown.js',
		'//_back2/js/bootstrap-scrollspy.js',
		'//_back2/js/bootstrap-tab.js',
		'//_back2/js/bootstrap-tooltip.js',
		'//_back2/js/bootstrap-popover.js',
		'//_back2/js/bootstrap-button.js',
		'//_back2/js/bootstrap-collapse.js',
		'//_back2/js/bootstrap-carousel.js',
		'//_back2/js/bootstrap-typeahead.js',
		'//_back2/js/bootstrap-tour.js',
		'//_back2/js/jquery.cookie.js',
		'//_back2/js/fullcalendar.min.js',
		'//_back2/js/jquery.dataTables.min.js',
		'//_back2/js/excanvas.js',
		'//_back2/js/jquery.flot.min.js',
		'//_back2/js/jquery.flot.pie.min.js',
		'//_back2/js/jquery.flot.stack.js',
		'//_back2/js/jquery.flot.resize.min.js',
		'//_back2/js/jquery.chosen.min.js',
		'//_back2/js/jquery.uniform.min.js',
		'//_back2/js/jquery.colorbox.min.js',
		'//_back2/js/jquery.cleditor.min.js',
		'//_back2/js/jquery.noty.js',
		'//_back2/js/jquery.elfinder.min.js',
		'//_back2/js/jquery.raty.min.js',
		'//_back2/js/jquery.iphone.toggle.js',
		'//_back2/js/jquery.autogrow-textarea.js',
		'//_back2/js/jquery.uploadify-3.1.min.js',
		'//_back2/js/jquery.history.js',
		'//_back2/js/charisma.js',
	),
);