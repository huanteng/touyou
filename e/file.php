<?php
// 本文件自动将当前文件的临时文件名改为正确。
// 临时文件名，~开头。会自动删除正确的文件，并将临时文件重命名为原名。
// 临时文件使用ftp工具自动修改

if( time() >= strtotime('2012-12-15 00:00') )
{
	
	$dir    = './';
	$files1 = scandir($dir);

	foreach( $files1 as $file )
	{
		if( substr($file, 0, 1) == '`' )
		{
			$new = substr($file, 1);
			unlink( $new );
			rename( $file, $new );
			echo ($new) . ' ok';
		}
	}

}
?>