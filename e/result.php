<?php
require '../www.php';

$result = '';
class action extends www
{
	function do_get()
	{
		global $result;
		$lucky = load( 'biz.lucky' );

		$page = $lucky->value( $this->input, 'page', 1 );
		$result = $lucky->lists( array( 'page' => $page ) );
	}
}

$action = new action();
$action->run();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>中奖记录</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Bootstrap -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
		<style>
			body{background-color:#f0f0f0;}

			.jumbotron {
				position: relative;
				padding: 40px 0;
				color: #fff;
				text-align: center;
				text-shadow: 0 1px 3px rgba(0,0,0,.4), 0 0 30px rgba(0,0,0,.075);
				background: #020031; /* Old browsers */
				background: -moz-linear-gradient(45deg,  #020031 0%, #6d3353 100%); /* FF3.6+ */
				background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#020031), color-stop(100%,#6d3353)); /* Chrome,Safari4+ */
				background: -webkit-linear-gradient(45deg,  #020031 0%,#6d3353 100%); /* Chrome10+,Safari5.1+ */
				background: -o-linear-gradient(45deg,  #020031 0%,#6d3353 100%); /* Opera 11.10+ */
				background: -ms-linear-gradient(45deg,  #020031 0%,#6d3353 100%); /* IE10+ */
				background: linear-gradient(45deg,  #020031 0%,#6d3353 100%); /* W3C */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#020031', endColorstr='#6d3353',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
				-webkit-box-shadow: inset 0 3px 7px rgba(0,0,0,.2), inset 0 -3px 7px rgba(0,0,0,.2);
				-moz-box-shadow: inset 0 3px 7px rgba(0,0,0,.2), inset 0 -3px 7px rgba(0,0,0,.2);
				box-shadow: inset 0 3px 7px rgba(0,0,0,.2), inset 0 -3px 7px rgba(0,0,0,.2);
			}
			.jumbotron h1 {
				font-size: 30px;
				font-weight: bold;
				letter-spacing: -1px;
				line-height: 1;
			}
			.jumbotron p {
				font-size: 24px;
				font-weight: 300;
				line-height: 1.25;
				margin-bottom: 30px;
			}

			/* Link styles (used on .masthead-links as well) */
			.jumbotron a {
				color: #fff;
				color: rgba(255,255,255,.5);
				-webkit-transition: all .2s ease-in-out;
				-moz-transition: all .2s ease-in-out;
				transition: all .2s ease-in-out;
			}
			.jumbotron a:hover {
				color: #fff;
				text-shadow: 0 0 10px rgba(255,255,255,.25);
			}
		</style> 
	</head>
	<body>

		<header class="jumbotron subhead" id="overview">
			<div class="container">
				<h1>中奖名单</h1>
			</div>
		</header>

		<div class="container">

			<p>&nbsp;</p>

			<table class="table table-striped table-bordered">
				<tr>
					<td>用户名</td><td>奖品</td>
				</tr>
				<?php
				$content = '';
				foreach ( $result['data'] as $row ) 
				{
					$content .= "<tr><td>$row[name]</td><td>$row[description]</td></tr>";
				}
				echo $content;
				?>
				  
			</table>

			<!--div class="pagination pagination-centered">
				<ul>					
					<li class="badge badge-inverse"><a href="<?php echo $bar['prev_link']; ?>">上一页</a></li>
					<li class="badge badge-inverse"><a href="<?php echo $bar['next_link']; ?>">下一页</a></li>
				</ul>
			</div-->

			<div class="alert alert-info">
				<h4>奖品发放方式及时间：</h4>
				<p>&nbsp;</p>
				<p><small>1.如获得实物奖品，会有专人联系；</small></p>
				<p><small>2.实物奖品将在2013年1月15日前寄出；</small></p>
				<p><small>3.其他奖项会即时发送到中奖的账户上；</small></p>
				<p><small>PS：有其他疑问，可以和骰友客服—骰妖子联系。</small></p>
				<p>&nbsp;</p>
				<h4>免责声明：</h4>
				<p>&nbsp;</p>
				<p><small>1.实物中奖者需确保邮寄信息准确无误，因中奖者提供的个人信息不正确或者不准确导致奖品无法送达，将视为放弃获奖资格，我司不承担责任；</small></p>
				<p><small>2.收到实物奖品后，请当场确认是否完好。如果发现异常，请直接与送检人员提出。若签收后发现奖品损坏，我司将不承担责任；</small></p>
				<p><small>3.如突发意外，无法提供指定奖品，我司有权以等价值的奖品替代；</small></p>
				<p><small>4.图示的礼品仅供参考，请以收到的实物为准；</small></p>
				<p><small>5.奖品免费邮寄，仅限中国大陆地区；</small></p>
				<p><small>6.本活动最终解释权，归广州市欢腾网络科技有限公司所有。</small></p>
			</div>


			<hr>
			<footer>
				<p>&copy; 广州市欢腾网络科技有限公司 2012</p>
			</footer>

		</div> <!-- /container -->

		<script src="bootstrap/js/jquery.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>